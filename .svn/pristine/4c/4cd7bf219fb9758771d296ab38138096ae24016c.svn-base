﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using GFleetV3.Service;
using System.Web.Script.Serialization;

namespace GFleetV3.Controllers
{
    [ValidateOnlyIncomingValuesAttribute]
    public class VehicleDispatchController : Controller
    {
        #region DispatchGrid data

        [Authorize]
        public ActionResult GetDispatchData()
        {
            var vehicleDispatch = DispatchGridModel.getDispatchData();
            var data = Json(vehicleDispatch);
            return data;
        }
        [Authorize]
        public JsonResult getLatestDispatchData(string highest)
        {
            DateTime date;
            if (DateTime.TryParse(highest, out date))
            {
                var latestDataDispatch = DispatchGridModel.getLatestDispatchData(highest);
                var data = Json(latestDataDispatch);
                return data;
                //return Json(DispatchGridModel.GetLatestDispatchData(date));
            }
            else
            {
                return Json("Wrong Date: " + highest);
            }
        }
        #endregion

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return View("VehicleDispatchGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);

            return View(data);
        }


        public JsonResult getBookDispatchRoles()
        {

            Boolean isDispatchRoles = Utility.isDispatch();
            Boolean isBookRoles = Utility.isBook();
            if (isDispatchRoles)
            {
                return Json("Dispatch");

            }
            else if (isBookRoles)
            {
                return Json("Book");
            }
            else if (isBookRoles && isDispatchRoles)
            {
                return Json("Book&Dispatch");
            }
            else
            {
                return Json(" ");
            }

        }

        private PagedVehicleDispatchModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            const int pageSize = 500;
            var totalRows = vehicleDispatchModel.CountVehicleDispatch();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "DispatchId";

            var vehicleDispatch = vehicleDispatchModel.GetVehicleDispatchPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleDispatchModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                VehicleDispatch = vehicleDispatch
            };
            return data;
        }

        [Authorize]
        [Authorization]
        public ActionResult Details(int id)
        {
            return View();
        }

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateAccessibilityData();
            PopulatePaymentModeData();
            PopulateAssignedExtrasData();
            PopulateCustomerDropDownList();
            PopulateDriverDropDownList();
            PopulateVehicleDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
            }
            return View("VehicleDispatchGrid");
        }

        [HttpPost]
        [Authorize]
        //    [Authorization]
        public ActionResult CreateEditVehicleDispatch(DispatchBookModel mVehicleDispatch, string[] selectedExtras, string[] selectedAccessibility, string[] selectedPaymentMode, string Command, string IsSendSms, bool? SendToDriver, bool? SendToCustomer, bool? IsVehicleVisible)
        {
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Book")
            {
                if (selectedPaymentMode == null || selectedPaymentMode.Length < 1)
                {
                    ErrorModel errMod = new ErrorModel();
                    errMod.Errors.Add("Kindly select payment mode");
                    return View("_Error", errMod);
                }
                mVehicleDispatch.Dispatch.BookedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleDispatch.Dispatch.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.ParseExact(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime, "dd-MM-yyyy HH:mm", null);
                //mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.Parse(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime);

                mVehicleDispatch.Dispatch.PaymentModeId = int.Parse(selectedPaymentMode[0]);

                mVehicleDispatch.Dispatch.ClientId = Service.Utility.getClientId().Value;
                mVehicleDispatch.Dispatch.SMSStatus1 = -1;
                mVehicleDispatch.Dispatch.CallBackId = -1;
                mVehicleDispatch.Dispatch.DispatchStatusId = 2;
                mVehicleDispatch.Dispatch.PassengerOnBoardId = -1;


                UpdateDispatchExtras(selectedExtras, mVehicleDispatch.Dispatch);
                UpdateDispatchAccessibility(selectedAccessibility, mVehicleDispatch.Dispatch);

                ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();

                ResponseModel rm = vehicleDispatchModel.CreateVehicleDispatch(mVehicleDispatch.Dispatch);
                return this.Json(rm);
            }

            else if (Command == "BookDispatch")
            {
                mVehicleDispatch.Dispatch.BookedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleDispatch.Dispatch.DispatchedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleDispatch.Dispatch.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.ParseExact(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime, "dd-MM-yyyy HH:mm", null);
                //mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.Parse(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime);
                mVehicleDispatch.Dispatch.PaymentModeId = int.Parse(selectedPaymentMode[0]);
                mVehicleDispatch.Dispatch.ClientId = Service.Utility.getClientId().Value;
                mVehicleDispatch.Dispatch.PassengerOnBoardId = -1;
                mVehicleDispatch.Dispatch.CallBackId = -1;
                mVehicleDispatch.Dispatch.SMSToCustomer = SendToCustomer;
                mVehicleDispatch.Dispatch.SMSToDriver = SendToDriver;
                mVehicleDispatch.Dispatch.DispatchStatusId = 3;
                mVehicleDispatch.Dispatch.IsVehicleVisible = IsVehicleVisible.HasValue;

                if (IsSendSms == "0")
                {
                    mVehicleDispatch.Dispatch.SMSStatus1 = -1;
                }



                UpdateDispatchExtras(selectedExtras, mVehicleDispatch.Dispatch);
                UpdateDispatchAccessibility(selectedAccessibility, mVehicleDispatch.Dispatch);

                ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
                ResponseModel rm = vehicleDispatchModel.CreateVehicleDispatch(mVehicleDispatch.Dispatch);

                if (IsSendSms == "1")
                {
                    bool? SendToDriverCancelled = false;

                    int VehicleId = Convert.ToInt32(mVehicleDispatch.Dispatch.VehicleId);
                    List<Dispatch_SendDispatchMessages1_Result> result = vehicleDispatchModel.SendDispatchMessage(mVehicleDispatch.Dispatch.DispatchId, SendToDriver.Value, SendToCustomer.Value, SendToDriverCancelled, null).ToList<Dispatch_SendDispatchMessages1_Result>();
                }
                return this.Json(rm);
            }
            else if (Command == "Dispatch")
            {
                mVehicleDispatch.Dispatch.DispatchedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleDispatch.Dispatch.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.ParseExact(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime, "dd-MM-yyyy HH:mm", null);
                //mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.Parse(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime);
                mVehicleDispatch.Dispatch.PaymentModeId = int.Parse(selectedPaymentMode[0]);
                mVehicleDispatch.Dispatch.ClientId = Service.Utility.getClientId().Value;
                mVehicleDispatch.Dispatch.DispatchStatusId = 3;
                mVehicleDispatch.Dispatch.PassengerOnBoardId = -1;
                mVehicleDispatch.Dispatch.IsVehicleVisible = IsVehicleVisible.HasValue;

                if (IsSendSms == "0")
                {
                    mVehicleDispatch.Dispatch.SMSStatus1 = -1;
                }

                UpdateDispatchExtras(selectedExtras, mVehicleDispatch.Dispatch);
                UpdateDispatchAccessibility(selectedAccessibility, mVehicleDispatch.Dispatch);

                ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
                ResponseModel rm = vehicleDispatchModel.UpdateVehicleDispatch(mVehicleDispatch.Dispatch, SendToCustomer, SendToDriver);

                if (IsSendSms == "1")
                {
                    bool? SendToDriverCancelled = false;
                    int VehicleId = Convert.ToInt32(mVehicleDispatch.Dispatch.VehicleId);
                    List<Dispatch_SendDispatchMessages1_Result> result = vehicleDispatchModel.SendDispatchMessage(mVehicleDispatch.Dispatch.DispatchId, SendToDriver, SendToCustomer, SendToDriverCancelled, null).ToList<Dispatch_SendDispatchMessages1_Result>();
                }
                return this.Json(rm);

            }
            else if (Command == "BookEdit")
            {
                mVehicleDispatch.Dispatch.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.ParseExact(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime, "dd-MM-yyyy HH:mm", null);
                //mVehicleDispatch.Dispatch.PickUpDateTime = DateTime.Parse(mVehicleDispatch.Dispatch.PickUpDate + " " + mVehicleDispatch.Dispatch.PickUpTime);
                mVehicleDispatch.Dispatch.PaymentModeId = int.Parse(selectedPaymentMode[0]);
                mVehicleDispatch.Dispatch.ClientId = Service.Utility.getClientId().Value;
                mVehicleDispatch.Dispatch.SMSStatus1 = -1;
                mVehicleDispatch.Dispatch.CallBackId = -1;
                mVehicleDispatch.Dispatch.DispatchStatusId = 2;
                mVehicleDispatch.Dispatch.PassengerOnBoardId = -1;
                mVehicleDispatch.Dispatch.SMSToCustomer = SendToCustomer;
                mVehicleDispatch.Dispatch.SMSToDriver = SendToDriver;

                UpdateDispatchExtras(selectedExtras, mVehicleDispatch.Dispatch);
                UpdateDispatchAccessibility(selectedAccessibility, mVehicleDispatch.Dispatch);

                ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
                ResponseModel rm = vehicleDispatchModel.UpdateVehicleDispatch(mVehicleDispatch.Dispatch, SendToCustomer, SendToDriver);

                return this.Json(rm);

            }
            else
            {
                return Content("Unrecognized operation, kindly refresh browser and rety.");
            }

        }

        //
        // GET: /VehicleDispatch/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult UpdatePOB(int idno, string PaggengerOnBoardId, string Comment)
        {
            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            var data = vehicleDispatchModel.UpdatePOB(idno, PaggengerOnBoardId, Comment);

            return Json(data);
        }
        [Authorize]
        [Authorization]
        public ActionResult CancelBook(int idno, string PassengerOnBoardId,string Comment, bool SendToCustomer)
        {
            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            var data = vehicleDispatchModel.CanceBooking(idno, PassengerOnBoardId,Comment, SendToCustomer);

            return Json(data);
        }
        [Authorize]
        [Authorization]

        public ActionResult UpdateCallBack(int idno, string CallBackId, string Comment)
        {
            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            var data = vehicleDispatchModel.UpdateCallBack(idno, CallBackId, Comment);

            return Json(data);
        }
        [Authorize]
        [Authorization]

        public ActionResult EditDispatch(int DispatchId, string VehicleId, string DriverUserId, bool? SendToDriver, bool? SendToCustomer, bool? IsVehicleVisible)
        {
            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            bool? SendToDriverCancelled = true;
            var data = vehicleDispatchModel.EditDispatch(DispatchId, int.Parse(VehicleId), Guid.Parse(DriverUserId), IsVehicleVisible, SendToDriver, SendToCustomer, SendToDriverCancelled);




            return Json(data);
        }
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            var data = vehicleDispatchModel.GetVehicleDispatch(id, true);

            return Json(data);
        }

        //
        // GET: /VehicleDispatch/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        [Authorize]
        public ActionResult BookDispatch(string Vid = null)
        {
            if (Vid != null)
            {
                ViewBag.Vid = Vid.ToString();
            }
            DispatchBookModel vehicleDispatchModel = new DispatchBookModel();
            vehicleDispatchModel = DispatchBookModel.getDispatchBookModel();
            PopulateAccessibilityData();
            PopulatePaymentModeData();
            PopulateDriverDropDownList();
            PopulateAssignedExtrasData();
            PopulateVehicleDropDownList();

            //return View("BookDispatch", vehicleDispatchModel);
            return View("BookDispatchNew", vehicleDispatchModel);
        }

        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var DriverVehileAssignment = new GFleetModelContainer().vw_VehicleDetailsV3.Where(v => v.ClientId == ClientId)
                .Select(v => new
                {
                    VehicleId = v.VehicleId,
                    VehicleReg = v.VehicleRegCode
                });
            ViewBag.Vehicle = new SelectList(DriverVehileAssignment, "VehicleId", "VehicleReg", Vehicle);
        }
        private void PopulateDriverDropDownList(object Driver = null)
        {
            string ClientId = Service.Utility.getClientId().Value.ToString();
            var DriverQuery = from d in new GFleetModelContainer().vw_Driver.Where(v => v.ClientId.Equals(ClientId))
                                  //.getUserProfile(ClientId, 3)
                              orderby d.FirstName
                              select new
                              {
                                  PersonId = d.UserId,
                                  Name = d.DriverNameNumber
                              };
            //if (!DriverNo == null)
            //{
            //}
            ViewBag.Driver = new SelectList(DriverQuery.ToList(), "PersonId", "Name", Driver);

        }
        //private void PopulateDriverDropDownList(object Driver = null)
        //{
        //    int ClientId = Service.Utility.getClientId().Value;
        //    var DriverQuery = from d in new GFleetModelContainer().People.OfType<Driver>().Where(v => v.ClientId == ClientId)
        //                      orderby d.FirstName
        //                      select new
        //                      {
        //                          PersonId = d.PersonId,
        //                          Name = d.DriverNameNumber
        //                      };
        //    //if (!DriverNo == null)
        //    //{
        //    //}
        //    ViewBag.Driver = new SelectList(DriverQuery, "PersonId", "Name", Driver);

        //}

        private void PopulateCustomerDropDownList(object Customer = null)
        {
            var CustomerQuery = from d in new GFleetModelContainer().People.OfType<Customer>()
                                orderby d.FirstName
                                select d;
            ViewBag.Customer = new SelectList(CustomerQuery, "PersonId", "FirstName", Customer);
        }

        private void PopulateAssignedExtrasData(Dispatch dispatch = null)
        {
            var allExtras = new GFleetModelContainer().Extras;
            var dispatchExtras = dispatch != null ? new HashSet<int>(dispatch.DispatchExtras.Select(c => c.ExtrasId)) : new HashSet<int>();
            var viewModel = new List<DispatchExtras>();
            foreach (var extra in allExtras)
            {
                viewModel.Add(new DispatchExtras
                {
                    ExtrasID = extra.ExtrasId,
                    Extras = extra.Extras,
                    Assigned = dispatchExtras.Contains(extra.ExtrasId)
                });
            }
            ViewBag.Extras = viewModel;
        }

        private void UpdateDispatchExtras(string[] selectedExtras, Dispatch dispatchExtrasToUpdate)
        {
            if (selectedExtras == null)
            {
                dispatchExtrasToUpdate.DispatchExtras = null;
                return;
            }

            var selectedExtrasHS = new HashSet<string>(selectedExtras);
            var dispatchExtras = new HashSet<int>
                (dispatchExtrasToUpdate.DispatchExtras.Select(c => c.ExtrasId));
            foreach (var extra in new GFleetModelContainer().Extras)
            {
                if (selectedExtrasHS.Contains(extra.ExtrasId.ToString()))
                {
                    if (!dispatchExtras.Contains(extra.ExtrasId))
                    {
                        dispatchExtrasToUpdate.DispatchExtras.Add(new DispatchExtra()
                        {
                            DispatchId = dispatchExtrasToUpdate.DispatchId,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            ClientId = Service.Utility.getClientId().Value,
                            DateTime = DateTime.Now,
                            ExtrasId = extra.ExtrasId
                        });
                    }
                }
                else
                {
                    if (dispatchExtras.Contains(extra.ExtrasId))
                    {
                        dispatchExtrasToUpdate.DispatchExtras.Remove(new DispatchExtra()
                        {
                            DispatchId = dispatchExtrasToUpdate.DispatchId,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            DateTime = DateTime.Now,
                            ExtrasId = extra.ExtrasId
                        });
                    }
                }
            }
        }

        private void UpdateDispatchAccessibility(string[] selectedAccessibility, Dispatch dispatchToUpdate)
        {
            if (selectedAccessibility == null)
            {
                dispatchToUpdate.DispatchAccessibilities = null;
                return;
            }

            var selectedAccessibilityHS = new HashSet<string>(selectedAccessibility);
            var dispatchAccessibilities = new HashSet<int>
                (dispatchToUpdate.DispatchAccessibilities.Select(c => c.AccessibilityId));
            foreach (var access in new GFleetModelContainer().Accesibilities)
            {
                if (selectedAccessibilityHS.Contains(access.AccesibilityId.ToString()))
                {
                    if (!dispatchAccessibilities.Contains(access.AccesibilityId))
                    {
                        dispatchToUpdate.DispatchAccessibilities.Add(new DispatchAccessibility()
                        {
                            DispatchId = dispatchToUpdate.DispatchId,
                            Accesibility = null,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            DateModified = DateTime.Now,
                            AccessibilityId = access.AccesibilityId
                        });
                    }
                }
                else
                {
                    if (dispatchAccessibilities.Contains(access.AccesibilityId))
                    {
                        dispatchToUpdate.DispatchAccessibilities.Remove(new DispatchAccessibility()
                        {
                            DispatchId = dispatchToUpdate.DispatchId,
                            Accesibility = null,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            DateModified = DateTime.Now,
                            AccessibilityId = access.AccesibilityId
                        });
                    }
                }
            }
        }

        private void PopulateAccessibilityData(Dispatch dispatch = null)
        {
            var allAccessibility = new GFleetModelContainer().Accesibilities;
            var dispatchAccessibility = dispatch != null ? new HashSet<int>(dispatch.DispatchAccessibilities.Select(c => c.AccessibilityId)) : new HashSet<int>();
            var viewModel = new List<AccessibilityinDispatch>();
            foreach (var accessibility in allAccessibility)
            {
                viewModel.Add(new AccessibilityinDispatch
                {
                    AccessibilityId = accessibility.AccesibilityId,
                    Accessibility = accessibility.Accesibility1,
                    Assigned = dispatchAccessibility.Contains(accessibility.AccesibilityId)
                });
            }
            ViewBag.Accessibility = viewModel;
        }

        private void PopulatePaymentModeData(Dispatch dispatch = null)
        {
            var allPaymentMode = new GFleetModelContainer().PaymentModes;
            var dispatchPaymentMode = dispatch != null ? new HashSet<int>(dispatch.PaymentMode.Dispatches.Select(c => c.PaymentModeId)) : new HashSet<int>();
            var viewModel = new List<DispatchPaymentMode>();
            foreach (var paymentMode in allPaymentMode)
            {
                viewModel.Add(new DispatchPaymentMode
                {
                    PaymentModeId = paymentMode.PaymentModeId,
                    PaymentMode = paymentMode.PaymentMode1,
                    Assigned = dispatchPaymentMode.Contains(paymentMode.PaymentModeId)
                });
            }
            ViewBag.PaymentMode = viewModel;
        }

        private void PopulateCustomer(int id)
        {
            var CustomerName = new GFleetModelContainer().People.Where(m => m.PersonId == id).FirstOrDefault();
            ViewBag.CustomerName = CustomerName.FirstName + " " + CustomerName.LastName;
            ViewBag.CustomerEmail = CustomerName.Email1;
            ViewBag.CustomerPhone = CustomerName.Telephone1;
            ViewBag.CustomerOrganisation = CustomerName.Organisation.OrganisationName;
        }
        [Authorize]
        [Authorization]
        public ActionResult loadMap(MapModel data = null)
        {
            return PartialView("_mapDirections", data);
        }


        [Authorize]
        public JsonResult getNearVehicleData(string coordinates)
        {
            return Json(VehicleDispatch.getDispatchVehicle(coordinates));
        }
        [Authorize]
        [Authorization]
        public ActionResult VehicleDispatchGrid()
        {
            return View("VehicleDispatchGrid");
        }


    }



}

