﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Threading.Tasks;

namespace GFleetV3.Models
{
    public class MessageModel
    {
        public List<MessageParamObject> GridParam { get; set; }
        public string msgparam { get; set; }
    }

    public class MessageParamObject
    {
        public string UserId { get; set; }
        public string PersonId { get; set; }
        public string FullNames { get; set; }
        public string ContactType { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public string Text { get; set; }

        public async static Task<List<MessageParamObject>> getMessageParamObjectAsync(int ClientId)
        {
            List<MessageParamObject> mpo = new List<MessageParamObject>();
            string BrandName = Service.Utility.getClientBrandName();
            var driver = getDriverAsync(ClientId, BrandName);
            var Customer = getCustomerAsync(ClientId);
            var CompanyStaff = getCompanyStaffAsync(ClientId, BrandName);
            var SystemUser = getSystemUserAsync(ClientId, BrandName);

            await Task.WhenAll(driver, Customer, CompanyStaff);

            mpo.AddRange(driver.Result);
            mpo.AddRange(Customer.Result);
            mpo.AddRange(CompanyStaff.Result);
            mpo.AddRange(SystemUser.Result);


            return mpo;
        }

        async static Task<List<MessageParamObject>> getDriverAsync(int ClientId, string BrandName)
        {
            return (await Task.FromResult<List<MessageParamObject>>(getDriver(ClientId, BrandName)));
        }

        async static Task<List<MessageParamObject>> getSystemUserAsync(int ClientId, string BrandName)
        {
            return (await Task.FromResult<List<MessageParamObject>>(getSystemUser(ClientId, BrandName)));
        }

        async static Task<List<MessageParamObject>> getCustomerAsync(int ClientId)
        {
            return (await Task.FromResult<List<MessageParamObject>>(getCustomer(ClientId)));
        }
        async static Task<List<MessageParamObject>> getCompanyStaffAsync(int ClientId ,string BrandName)
        {
            return (await Task.FromResult<List<MessageParamObject>>(getCompanyStaff(ClientId, BrandName)));
        }

        public static List<MessageParamObject> getDriver(int ClientId ,string BrandName)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<MessageParamObject> result = new List<MessageParamObject>();
                string _ClientId = ClientId.ToString();
                result.AddRange(db.vw_Driver.Where(v => v.ClientId == _ClientId)
                    .ToList()
                    .Select(v => new MessageParamObject()
                    {
                        UserId = v.UserId.ToString(),
                        FullNames = v.DriverNameNumber,
                        PhoneNumber = v.Telephone1,
                        ContactType = "Driver",
                        Company = BrandName
                    })
                );

                return result;
            }
        }

        public static List<MessageParamObject> getCustomer(int ClientId)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<MessageParamObject> result = new List<MessageParamObject>();

                result.AddRange(db.People.OfType<Customer>().Where(v => v.ClientId == ClientId)
                    .ToList()
                    .Select(v => new MessageParamObject()
                    {
                        UserId = v.UserId.ToString(),
                        FullNames = v.FullNames,
                        PhoneNumber = v.Telephone1,
                        Company = v.Organisation.OrganisationName,
                        ContactType = "Customer"
                    })
                );

                return result;
            }
        }
        public static List<MessageParamObject> getCompanyStaff(int ClientId, string BrandName)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<MessageParamObject> result = new List<MessageParamObject>();

                result.AddRange(db.People.OfType<CompanyStaff>().Where(v => v.ClientId == ClientId)
                    .ToList()
                    .Select(v => new MessageParamObject()
                    {
                        UserId = v.UserId.ToString(),
                        FullNames = v.FullNames,
                        PhoneNumber = v.Telephone1,
                        ContactType = "CompanyStaff",
                         Company = BrandName
                    })
                );

                return result;
            }
        }

        public static List<MessageParamObject> getSystemUser(int ClientId, string BrandName)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<MessageParamObject> result = new List<MessageParamObject>();

                result.AddRange(db.People.OfType<SystemUser>().Where(v => v.ClientId == ClientId)
                    .ToList()
                    .Select(v => new MessageParamObject()
                    {
                        UserId = v.UserId.ToString(),
                        FullNames = v.FullNames,
                        PhoneNumber = v.Telephone1,
                        ContactType = "SystemUser",
                        Company = BrandName
                    })
                );

                return result;
            }
        }
    }

    public class Message
    {
        public int UserId { get; set; }
        public string PersonId { get; set;  }
        public string FullNames { get; set; }
        public string ContactType { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public string Text { get; set; }
    }


 


    public class PagedContactsModel
    {
        public IEnumerable<Person> Contacts { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class ModelContacts 
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public ResponseModel SendMessage(string PersonId,string msg)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                int ClientId = Service.Utility.getClientId().Value;
                var UserId = Guid.Parse(Service.Utility.getUserId().ToString());
                List<Outgoing_Insert_Result> response = entities.Outgoing_Insert(PersonId, msg, ClientId).ToList();
                _rm.Status = response[0].Success.Value;
                _rm.Message = response[0].Error;
                // "Message  successfully sent!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
    }
}