﻿using System;
using ExceptionHandler;
using System.Threading;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

namespace GFleetV3.Controllers
{
    public class BackUpController : Controller
    {
        [Authorize]
        [Authorization]
        public ActionResult Index()
        {
            try
            {
                string directoryPath = getDirectoryPath();
                if (!string.IsNullOrEmpty(directoryPath))
                {

                    string downloadSearchPattern = System.Configuration.ConfigurationManager.AppSettings["downloadSearchPattern"].ToString();

                    DirectoryInfo _DirectoryInfo = new DirectoryInfo(directoryPath);

                    if (_DirectoryInfo.Exists)
                    {

                        FileInfo[] _files = _DirectoryInfo.GetFiles(downloadSearchPattern /*"*.xls"*/, SearchOption.TopDirectoryOnly);

                        IEnumerable<BackUpGridData> files = _files.Select(r => new BackUpGridData()
                        {
                            FileDate = r.CreationTime,
                            FileName = r.Name,
                            FileSize = string.Format((r.Length / 1024f / 1024f).ToString() + "{0}", " MB")
                        });

                        BackUpData _BackUpData = new BackUpData() { BackUpGridData = files };

                        return View(_BackUpData);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }
            //return Json("Error: Error while reading files!!");
            ViewBag.Message = "Error: Error while reading files!!";
            return View(new BackUpData());

        }

        private static string getDirectoryPath()
        {
            try
            {
                int? clientId = Service.Utility.getClientId();
                if (clientId.HasValue)
                {

                    return getDirectoryPath(clientId.Value);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }
            return null;
        }

        private static string getDirectoryPath(int clientId)
        {
            try
            {
                ModelClient clientModel = new ModelClient();

                string brandName;
                string ftpHomeDirectory;
                //int clientId = Service.Utility.getClientId().Value;
                brandName = clientModel.GetClient(clientId).BrandName;

                ftpHomeDirectory = System.Configuration.ConfigurationManager.AppSettings["ftpHomeDirectory"].ToString();

                return ftpHomeDirectory + brandName + @"\";
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }
            return "";
        }

        [Authorize]
        [Authorization]
        public ActionResult downloadBackupFile(string filename)
        {
            string directoryPath = getDirectoryPath();

            if (!string.IsNullOrEmpty(directoryPath))
            {

                string filepath = directoryPath + filename;

                return new BackUpDownloadResult<object>
                (
                    ControllerContext,
                    filepath
                );
            }

            return Json("Invalid file for downlaod or session expired. Kindly login and rety!", JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [Authorization]
        [HttpPost]
        public ActionResult createBackUp(string startDate, string endDate, string startTime, string endTime)
        {
            DateTime sdate;
            DateTime edate;
            startDate = startDate + " " + startTime;
            endDate = endDate + " " + endTime;
            //return Json("Error: A start date must be selected!!");
            //validate start date
            if (string.IsNullOrEmpty(startDate))
            {
                return Json("Error: A start date must be selected!!");
            }
            if (!DateTime.TryParse(startDate, out sdate))
            {
                string fdate = startDate;
                string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                    + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : "");
                if (!DateTime.TryParse(otherDate, out sdate))
                {
                    return Json("Error: Invalid start date specified!!");
                }
            }
            //startDate = sdate.ToString("dd/MM/yyyy HH:mm:ss");

            //validate end date
            if (endDate == null)
            {
                return Json("Error: An end date must be selected!!");
            }
            if (!DateTime.TryParse(endDate, out edate))
            {
                string fdate = endDate;
                string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                    + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : " 23:59:59.998");
                if (!DateTime.TryParse(otherDate, out edate))
                {
                    return Json("Error: Invalid start date specified!!");
                }
            }
            int? clientId = Service.Utility.getClientId();
            if (clientId.HasValue)
            {
                string email = Membership.GetUser().Email;

                Thread t = new Thread(delegate()
                {
                    try
                    {
                        string virtualDirectoryPath = BackUpModel.createDataBackUp(sdate, edate, clientId.Value);
                        string startPath = virtualDirectoryPath;

                        string zipPath = getDirectoryPath(clientId.Value);
                        //ZipFile.CreateFromDirectory(startPath, zipPath.Substring(0,zipPath.LastIndexOf('\\')));

                        zipPath = virtualDirectoryPath.Substring(0, virtualDirectoryPath.LastIndexOf('\\')) + ".zip"; //zipPath.Substring(0, zipPath.LastIndexOf('\\'));
                        startPath = startPath.Substring(0, startPath.LastIndexOf('\\'));
                        try
                        {
                            FileInfo _zipPath = new FileInfo(zipPath);
                            _zipPath.Delete();
                        }
                        catch (Exception ex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
                        }

                        ZipFile.CreateFromDirectory(@startPath, @zipPath);

                        // send mail here
                        BackUpModel.sendBackUpMail(backupEmailSubject, backupEmailMessage, email);

                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
                    }
                });
                t.Start();

                return Json("Success: Back up has been started. You will receive an email notification shortly.");
            }
            else
            {
                return Json("Error: Error occurred. Kindly retry again.");
            }
        }

        private string backupEmailMessage
        {
            get
            {
                string message = "";

                message = "GTrack Backup <br />";

                message += "Backup operation completed. Kindly log on and access the backup page and download. <br /><br />";

                message += "Note: Files older then 7 days will be deleted immediately. <br /><br />";

                message += "Regards <br />";

                message += "Gtrack Support Team";

                return message;
            }
        }
        private string backupEmailSubject
        {
            get
            {
                string subject = "Gtrack Backup";

                return subject;
            }
        }
    }
}