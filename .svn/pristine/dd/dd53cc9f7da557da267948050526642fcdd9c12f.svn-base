﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Taksi.Models;
using System.Net.Mail;
using System.Web.Configuration;
using Newtonsoft.Json;

namespace Taksi.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            //return View();
            return RedirectToAction("Login");
        }
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Privacy()
        {
            return View("Privacy");
        }

        //[CaptchaMvc.Attributes.CaptchaVerify("Captcha Is Not valid")]
        [HttpPost]
        public ActionResult Login(LogOnModel model, string returnUrl)
        {
            var response = Request["g-recaptcha-response"];
            //secret that was generated in key value pair
            const string secret = "6LcQ2wUTAAAAAJnrZ7iCWNbQWJIsKYaqsbRdQXp-";

            var client = new System.Net.WebClient();
            var reply =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0) return View();

                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        ModelState.AddModelError("", "The captcha secret parameter is missing.");
                        break;
                    case ("invalid-input-secret"):
                        ModelState.AddModelError("", "The captcha secret parameter is invalid or malformed.");
                        break;

                    case ("missing-input-response"):
                        ModelState.AddModelError("", "The captcha response parameter is missing.");
                        break;
                    case ("invalid-input-response"):
                        ModelState.AddModelError("", "The captcha response parameter is invalid or malformed.");
                        break;

                    default:
                        ModelState.AddModelError("", "Error occured. Please try again");
                        break;
                }
            }
            else
            {
                if (ModelState.IsValid)
                {
                    model.UserName = model.UserName.Replace("+", "");
                    Boolean uservalid = Membership.ValidateUser(model.UserName, model.Password);

                    if (uservalid == false)
                    {
                        bool successfulUnlock = AutoUnlockUser(model.UserName);
                        if (successfulUnlock)
                        {
                            FormsService.SignIn(model.UserName, model.RememberMe);
                            if (Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Dashboard");
                            }
                        }
                    }
                    else //if (uservalid)
                    {
                        string[] validLoginRoles = WebConfigurationManager.AppSettings["validLoginRoles"].Split(',');
                        string[] roles = Roles.GetRolesForUser(model.UserName);
                        bool valid = false;
                        foreach (string role in validLoginRoles)
                        {
                            if (roles.Contains(role))
                            {
                                valid = true;
                            }
                        }
                        if (!valid)
                        {
                            ModelState.AddModelError("", "User not authorized on this platform.");
                            return View(model);
                        }

                        FormsService.SignIn(model.UserName, model.RememberMe);
                        if (Url.IsLocalUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Dashboard");
                        }
                    }

                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private bool AutoUnlockUser(string username)
        {
            string _passwordLockoutMinutes = System.Configuration.ConfigurationManager.AppSettings["PasswordLockoutMinutes"];
            MembershipUser mu = Membership.GetUser(username, false);
            if ((mu != null) && (mu.IsLockedOut) &&
                (mu.LastLockoutDate.ToUniversalTime().AddMinutes(int.Parse(_passwordLockoutMinutes)) < DateTime.UtcNow))
            {
                bool retval = mu.UnlockUser();
                if (retval)
                    return true;
                else
                    return false;    //something went wrong with the unlock
            }
            else
                return false;       //not locked out in the first place
            //or still in lockout period
        }

    }
}
