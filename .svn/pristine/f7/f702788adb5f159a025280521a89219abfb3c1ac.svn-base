﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using Newtonsoft.Json;

namespace GFleetV3.Controllers
{
    public class LiveController : Controller
    {

        public ActionResult test()
        {
            return View("test");
        }

        [Authorize]
        [Authorization]
        public ActionResult Index()
        {
            PopulateUnallocatedDevices();
            ViewBag.alertdata = LiveModel.getUserTrackConfig();
            return View();
        }
        [Authorize]
        public JsonResult getTree()
        {
            return Json(VehicleTreeModel.getVehicleTree1(), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult getGridData()
        {
            return Json(LiveModel.getWebTrackData());
        }

        [Authorize]
        public JsonResult getLatestData(string highest)
        {
            DateTime date;
            if (DateTime.TryParse(highest, out date))
            {
                return Json(LiveModel.getLatestTrackData(date));
            }
            else
            {
                return Json("Wrong Date: " + highest);
            }
        }

        [HttpPost]
        public JsonResult getLatestData1(string latestDataParams)
        {
            List<LiveLatestDataParams> data = String.IsNullOrEmpty(latestDataParams) ? null : JsonConvert.DeserializeObject<List<LiveLatestDataParams>>(latestDataParams);
            //ResponseModel rm;(latestDataParams==null)? null:latestDataParams.ToList<DispatchLatestDataParams>()
            return Json(LiveModel.GetLatestLiveDataAsync(data));
        }

        [Authorize]
        public JsonResult requestLocation(string vehicleId)
        {
            int _vehicleId;
            if (int.TryParse(vehicleId, out _vehicleId))
            {
                return Json(LiveModel.sendCommand(_vehicleId, "Locate", Request.UserHostAddress));
            }
            else
            {
                return Json("Unknown device");
            }
        }

        [Authorize]
        public JsonResult controlEngine(string vehicleId, Boolean isSwitchOn)
        {
            int _vehicleId;
            if (int.TryParse(vehicleId, out _vehicleId))
            {
                return Json(LiveModel.sendCommand(_vehicleId, isSwitchOn ? "startEngine" : "stopEngine", Request.UserHostAddress));
            }
            else
            {
                return Json("Unknown device");
            }
        }

        [Authorize]
        public JsonResult getLog()
        {
            return Json(LiveModel.getDevicesLogs(Request.UserHostAddress));
        }

        [Authorize]
        public JsonResult requestGetLog(string vehicleId)
        {
            int _vehicleId;
            if (int.TryParse(vehicleId, out _vehicleId))
            {
                return Json(LiveModel.sendCommand(_vehicleId, "GETLOG", Request.UserHostAddress));
            }
            else
            {
                return Json("Unknown device");
            }
        }

        [Authorize]
        public JsonResult getRouteList(string Registration, string startDate, string endDate)
        {
            DateTime sdate;
            DateTime edate;
            //return Json("Error: A start date must be selected!!");
            //validate start date
            if (string.IsNullOrEmpty(startDate))
            {
                return Json("Error: A start date must be selected!!");
            }
            if (!DateTime.TryParse(startDate, out sdate))
            {
                string fdate = startDate;
                string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                    + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : "");
                if (!DateTime.TryParse(otherDate, out sdate))
                {
                    return Json("Error: Invalid start date specified!!");
                }
            }
            //startDate = sdate.ToString("dd/MM/yyyy HH:mm:ss");

            //validate end date
            if (endDate == null)
            {
                return Json("Error: An end date must be selected!!");
            }
            if (!DateTime.TryParse(endDate, out edate))
            {
                string fdate = endDate;
                string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                    + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : " 23:59:59.999");
                if (!DateTime.TryParse(otherDate, out edate))
                {
                    return Json("Error: Invalid start date specified!!");
                }
            }
            //endDate = edate.ToString("dd/MM/yyyy HH:mm:ss");
            return Json(LiveModel.getRouteReport(Registration, sdate, edate));
        }

        [Authorize]
        public JsonResult getPlotRouteData(string Registration, string startDate, string endDate)
        {
            return Json(LiveModel.getPlotRouteReport(Registration, DateTime.Parse(startDate), DateTime.Parse(endDate)));
        }

        [Authorize]
        public JsonResult sendSMS(string Contacts, string Message)
        {
            SendSMSModel.SendSMS(Contacts, Message);
            return Json("sms queued for sending");
        }

        [Authorize]
        public JsonResult getGeofences()
        {
            return Json(LiveModel.getGeoFence());
        }

        public JsonResult liveGridTableHeight()
        {
            return Json(LiveModel.getGeoFence());
        }

        [Authorize]
        public JsonResult addGeofence(string name, string points)
        {
            return Json(LiveModel.addGeofence(name, points));
        }

        [Authorize]
        public JsonResult getAllgroups()
        {
            ResponseModel rm = new ResponseModel() { Data = new ModelGroup().GetGroupDetails() };
            return Json(rm);
        }

        [Authorize]
        public JsonResult updateGeofence(int id, string groupIds, int? speed, string name, Boolean geofenceactive, Boolean? reportonexit, int? reportonexitfreq,
                int? reportonexitnoofalerts, Boolean? reportonentry, int? reportonentryfreq, int? reportonentrynoofalerts, string email, string phone, Boolean Isgeofencespeed)
        {

            return Json(LiveModel.updateGeofence(id, groupIds, name, geofenceactive, reportonexit, reportonexitfreq, reportonexitnoofalerts,
                reportonentry, reportonentryfreq, reportonentrynoofalerts, email, phone, speed, Isgeofencespeed));
        }

        [Authorize]
        public JsonResult deleteGeofence(int id)
        {
            return Json(LiveModel.deleteGeofence(id));
        }

        [Authorize]
        public JsonResult updateAlert(int alertId, Boolean value)
        {
            return Json(LiveModel.updateAlertStatus(alertId, value, "User requested from web"));
        }

        [Authorize]
        public JsonResult getCompanySettings()
        {
            return Json(CompanySettingsModel.getCompanySettings_gridheight());
        }
        [Authorize]
        public JsonResult getClientTypeLogoUrl()
        {
            return Json(ModelClientType.getClientTypeLogoUrl());
        }

        [Authorize]
        public void setCompanySettings(string newValue)
        {
            CompanySettingsModel.setCompanySettings_height(newValue);
        }

        private void PopulateUnallocatedDevices(Vehicle device = null)
        {
            var allDevices = new GFleetModelContainer().vw_UnallocatedDevices;
            var availdevices = device != null ? new HashSet<int>(device.VehicleDevices.Select(c => c.DeviceId)) : new HashSet<int>();
            var viewModel = new List<Devices>();
            foreach (var devices in allDevices)
            {
                viewModel.Add(new Devices
                {
                    DeviceId = devices.DeviceId,
                    PhoneNo = devices.PhoneNo,
                    Assigned = availdevices.Contains(devices.DeviceId)
                });
            }
            ViewBag.Devices = viewModel;
        }
    }
}
