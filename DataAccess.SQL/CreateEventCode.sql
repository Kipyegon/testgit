USE [CarTrack]
GO

/****** Object:  Table [dbo].[EventCode]    Script Date: 04/26/2014 12:36:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EventCode](
	[DeviceTypeId] [int] NULL,
	[DeviceEventId] [int] NULL,
	[DeviceEventName] [varchar](50) NULL,
	[EventName] [varchar](100) NULL,
	[EventId] [int] NULL,
	[EventCodeId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 CONSTRAINT [PK_EventCode] PRIMARY KEY CLUSTERED 
(
	[EventCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


