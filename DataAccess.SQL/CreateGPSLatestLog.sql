USE [CarTrack]
GO

/****** Object:  Table [dbo].[GPSLatestLog]    Script Date: 04/26/2014 12:33:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GPSLatestLog](
	[ID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UnitID] [varchar](20) NULL,
	[SERIAL_NUM] [varchar](50) NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[GLB_TIME] [datetime] NULL,
	[Speed] [float] NULL,
	[Date] [datetime] NULL,
	[Time] [datetime] NULL,
	[Distance] [bigint] NULL,
	[Course] [float] NULL,
	[Direction] [varchar](10) NULL,
	[DeviceTypeId] [int] NULL,
	[VehicleId] [int] NULL,
	[DeviceId] [int] NULL,
	[DeviceEventId] [int] NULL,
	[HasLocationData] [bit] NULL,
	[GPSStatus] [bit] NULL,
	[Alarm] [int] NULL,
	[SequenceNo] [bigint] NULL,
	[EngineState] [bit] NULL,
	[HDOP] [float] NULL,
	[A2D1] [float] NULL,
	[A2D2] [float] NULL,
	[A2D3] [float] NULL,
	[IO_INPUT1] [bit] NULL,
	[IO_INPUT2] [bit] NULL,
	[IO_INPUT3] [bit] NULL,
	[IO_INPUT4] [bit] NULL,
	[IO_INPUT5] [bit] NULL,
	[IO_INPUT6] [bit] NULL,
	[IO_INPUT7] [bit] NULL,
	[IO_INPUT8] [bit] NULL,
	[IO_OUTPUT1] [bit] NULL,
	[IO_OUTPUT2] [bit] NULL,
	[IO_OUTPUT3] [bit] NULL,
	[IO_OUTPUT4] [bit] NULL,
	[IO_OUTPUT5] [bit] NULL,
	[IO_OUTPUT6] [bit] NULL,
	[IO_OUTPUT7] [bit] NULL,
	[IO_OUTPUT8] [bit] NULL,
	[Altitude] [float] NULL,
	[SLNAME] [varchar](400) NULL,
	[LOCNAME] [varchar](50) NULL,
	[DIVNAME] [varchar](50) NULL,
	[DISTNAME] [varchar](50) NULL,
	[PROVNAME] [varchar](50) NULL,
	[CONSTITUEN] [varchar](50) NULL,
	[TOWN] [varchar](400) NULL,
	[STREET] [varchar](400) NULL,
	[ImageUrl] [varchar](10) NULL,
	[SatellitesUsed] [int] NULL,
	[CELL_MCC] [varchar](10) NULL,
	[CELL_MNC] [varchar](10) NULL,
	[CELL_LAC] [varchar](10) NULL,
	[CELL_ID] [varchar](10) NULL,
	[GSM_RSSI] [varchar](50) NULL,
	[externalPowerState] [int] NULL,
	[externalPowerVoltage] [float] NULL,
	[batteryVoltage] [float] NULL,
 CONSTRAINT [PK_GPSLatestLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GPSLatestLog] ADD  CONSTRAINT [DF_GPSLatestLog_Date]  DEFAULT (getdate()) FOR [Date]
GO


