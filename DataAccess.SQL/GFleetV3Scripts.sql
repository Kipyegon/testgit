
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 04/08/2014 17:10:53
-- Generated from EDMX file: E:\FromD\Projects\GFleetV3V3\DataAccess.SQL\GFleetV3Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GFleetV3];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__286302EC]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__UserI__286302EC];
GO
IF OBJECT_ID(N'[dbo].[FK__Person__ClientId__320C68B7]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Person] DROP CONSTRAINT [FK__Person__ClientId__320C68B7];
GO
IF OBJECT_ID(N'[dbo].[FK__Vehicle_F__Clien__37C5420D]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleFuelManagement] DROP CONSTRAINT [FK__Vehicle_F__Clien__37C5420D];
GO
IF OBJECT_ID(N'[dbo].[FK__Vehicle_I__Clien__38B96646]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleInsuarance] DROP CONSTRAINT [FK__Vehicle_I__Clien__38B96646];
GO
IF OBJECT_ID(N'[dbo].[FK__Vehicle_L__Clien__39AD8A7F]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleLicenceDetail] DROP CONSTRAINT [FK__Vehicle_L__Clien__39AD8A7F];
GO
IF OBJECT_ID(N'[dbo].[FK__VehicleSe__Clien__3D7E1B63]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleServiceDetail] DROP CONSTRAINT [FK__VehicleSe__Clien__3D7E1B63];
GO
IF OBJECT_ID(N'[dbo].[FK__VehicleSt__Clien__3E723F9C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleStatus] DROP CONSTRAINT [FK__VehicleSt__Clien__3E723F9C];
GO
IF OBJECT_ID(N'[dbo].[FK_Accesibility_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Accesibility] DROP CONSTRAINT [FK_Accesibility_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertLog_AlertReceiverAlert]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertLog] DROP CONSTRAINT [FK_AlertLog_AlertReceiverAlert];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertReceiverAlert_AlertRecipient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertReceiverAlert] DROP CONSTRAINT [FK_AlertReceiverAlert_AlertRecipient];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertReceiverAlert_AlertType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertReceiverAlert] DROP CONSTRAINT [FK_AlertReceiverAlert_AlertType];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertReceiverAlert_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertReceiverAlert] DROP CONSTRAINT [FK_AlertReceiverAlert_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertRecipient_AlertRecipient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertRecipient] DROP CONSTRAINT [FK_AlertRecipient_AlertRecipient];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertRecipient_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertRecipient] DROP CONSTRAINT [FK_AlertRecipient_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertRecipient_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertRecipient] DROP CONSTRAINT [FK_AlertRecipient_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_AlertType_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlertType] DROP CONSTRAINT [FK_AlertType_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Client_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Client] DROP CONSTRAINT [FK_Client_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Client_ClientType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Client] DROP CONSTRAINT [FK_Client_ClientType];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientType_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClientType] DROP CONSTRAINT [FK_ClientType_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_CompanySettings_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CompanySettings] DROP CONSTRAINT [FK_CompanySettings_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_Controller_Controller]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Controller] DROP CONSTRAINT [FK_Controller_Controller];
GO
IF OBJECT_ID(N'[dbo].[FK_Controller_Module]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Controller] DROP CONSTRAINT [FK_Controller_Module];
GO
IF OBJECT_ID(N'[dbo].[FK_ControllerAction_Action]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ControllerAction] DROP CONSTRAINT [FK_ControllerAction_Action];
GO
IF OBJECT_ID(N'[dbo].[FK_ControllerAction_Controller]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ControllerAction] DROP CONSTRAINT [FK_ControllerAction_Controller];
GO
IF OBJECT_ID(N'[dbo].[FK_ControllerActionRole_aspnet_Roles]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ControllerActionRole] DROP CONSTRAINT [FK_ControllerActionRole_aspnet_Roles];
GO
IF OBJECT_ID(N'[dbo].[FK_ControllerActionRole_ControllerAction]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ControllerActionRole] DROP CONSTRAINT [FK_ControllerActionRole_ControllerAction];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerType_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerType] DROP CONSTRAINT [FK_CustomerType_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerType_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerType] DROP CONSTRAINT [FK_CustomerType_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_Device_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Device] DROP CONSTRAINT [FK_Device_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_Device_DeviceType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Device] DROP CONSTRAINT [FK_Device_DeviceType];
GO
IF OBJECT_ID(N'[dbo].[FK_DeviceModel_DeviceModel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DeviceModel] DROP CONSTRAINT [FK_DeviceModel_DeviceModel];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Accessibility_Accesibility]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchAccessibility] DROP CONSTRAINT [FK_Dispatch_Accessibility_Accesibility];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Accessibility_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchAccessibility] DROP CONSTRAINT [FK_Dispatch_Accessibility_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_aspnet_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_aspnet_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_aspnet_Users1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_aspnet_Users1];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Dispatch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_Dispatch];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_DispatchStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_DispatchStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_DriverVehicleAssignment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_DriverVehicleAssignment];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Extras_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchExtras] DROP CONSTRAINT [FK_Dispatch_Extras_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Extras_Extras]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchExtras] DROP CONSTRAINT [FK_Dispatch_Extras_Extras];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_PaymentMode]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_PaymentMode];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Person_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_Person_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispatch_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dispatch] DROP CONSTRAINT [FK_Dispatch_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_DispatchAccessibility_Dispatch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchAccessibility] DROP CONSTRAINT [FK_DispatchAccessibility_Dispatch];
GO
IF OBJECT_ID(N'[dbo].[FK_DispatchExtras_Dispatch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchExtras] DROP CONSTRAINT [FK_DispatchExtras_Dispatch];
GO
IF OBJECT_ID(N'[dbo].[FK_DispatchStatus_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchStatus] DROP CONSTRAINT [FK_DispatchStatus_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_Driver_DriverType_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverLicenceType] DROP CONSTRAINT [FK_Driver_DriverType_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Driver_Vehicle_Assignment_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverVehicleAssignment] DROP CONSTRAINT [FK_Driver_Vehicle_Assignment_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Driver_Vehicle_Assignment_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverVehicleAssignment] DROP CONSTRAINT [FK_Driver_Vehicle_Assignment_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_Driver_Vehicle_Assignment_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverVehicleAssignment] DROP CONSTRAINT [FK_Driver_Vehicle_Assignment_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_Driver_Vehicle_Assignment_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverVehicleAssignment] DROP CONSTRAINT [FK_Driver_Vehicle_Assignment_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverEndosment_aspnet_Membership1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverEndosment] DROP CONSTRAINT [FK_DriverEndosment_aspnet_Membership1];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverEndosment_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverEndosment] DROP CONSTRAINT [FK_DriverEndosment_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverEndosment_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverEndosment] DROP CONSTRAINT [FK_DriverEndosment_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverLicenceType_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverLicenceType] DROP CONSTRAINT [FK_DriverLicenceType_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverLicenceType_LicenceType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverLicenceType] DROP CONSTRAINT [FK_DriverLicenceType_LicenceType];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverLicenceType_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverLicenceType] DROP CONSTRAINT [FK_DriverLicenceType_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverStatus_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverStatus] DROP CONSTRAINT [FK_DriverStatus_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverStatus_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverStatus] DROP CONSTRAINT [FK_DriverStatus_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverType_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LicenceType] DROP CONSTRAINT [FK_DriverType_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Extras_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Extras] DROP CONSTRAINT [FK_Extras_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Extras_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Extras] DROP CONSTRAINT [FK_Extras_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_FuelType_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FuelType] DROP CONSTRAINT [FK_FuelType_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_GeoFence_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GeoFence] DROP CONSTRAINT [FK_GeoFence_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_GeoFenceVehicle_GeoFence]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GeoFenceVehicle] DROP CONSTRAINT [FK_GeoFenceVehicle_GeoFence];
GO
IF OBJECT_ID(N'[dbo].[FK_GeoFenceVehicle_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GeoFenceVehicle] DROP CONSTRAINT [FK_GeoFenceVehicle_Group];
GO
IF OBJECT_ID(N'[dbo].[FK_InsuaranceType_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InsuaranceType] DROP CONSTRAINT [FK_InsuaranceType_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_PaymentMode_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaymentMode] DROP CONSTRAINT [FK_PaymentMode_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Person_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Person] DROP CONSTRAINT [FK_Person_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_SentSMS_aspnet_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SentSMS] DROP CONSTRAINT [FK_SentSMS_aspnet_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_TrackAlertConfig_AlertConfig]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TrackAlertConfig] DROP CONSTRAINT [FK_TrackAlertConfig_AlertConfig];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Fuel_Management_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleFuelManagement] DROP CONSTRAINT [FK_Vehicle_Fuel_Management_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Fuel_Management_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleFuelManagement] DROP CONSTRAINT [FK_Vehicle_Fuel_Management_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Insuarance_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleInsuarance] DROP CONSTRAINT [FK_Vehicle_Insuarance_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Insuarance_Insuarance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleInsuarance] DROP CONSTRAINT [FK_Vehicle_Insuarance_Insuarance];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Insuarance_InsuaranceType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleInsuarance] DROP CONSTRAINT [FK_Vehicle_Insuarance_InsuaranceType];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Insuarance_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleInsuarance] DROP CONSTRAINT [FK_Vehicle_Insuarance_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_LicenceDetail_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleLicenceDetail] DROP CONSTRAINT [FK_Vehicle_LicenceDetail_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_LicenceDetail_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleLicenceDetail] DROP CONSTRAINT [FK_Vehicle_LicenceDetail_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_LicenceDetail_VehicleLicenceType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleLicenceDetail] DROP CONSTRAINT [FK_Vehicle_LicenceDetail_VehicleLicenceType];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK_Vehicle_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleDevice_Device]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleDevice] DROP CONSTRAINT [FK_VehicleDevice_Device];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleDevice_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleDevice] DROP CONSTRAINT [FK_VehicleDevice_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleFuelManagement_Person]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleFuelManagement] DROP CONSTRAINT [FK_VehicleFuelManagement_Person];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleGroup_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleGroup] DROP CONSTRAINT [FK_VehicleGroup_Group];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleGroup_Vehicle]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleGroup] DROP CONSTRAINT [FK_VehicleGroup_Vehicle];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleServiceDetail_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleServiceDetail] DROP CONSTRAINT [FK_VehicleServiceDetail_aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[FK_VehicleStatus_aspnet_Membership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VehicleStatus] DROP CONSTRAINT [FK_VehicleStatus_aspnet_Membership];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Accesibility]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Accesibility];
GO
IF OBJECT_ID(N'[dbo].[Action]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Action];
GO
IF OBJECT_ID(N'[dbo].[AlertConfigType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AlertConfigType];
GO
IF OBJECT_ID(N'[dbo].[AlertLog]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AlertLog];
GO
IF OBJECT_ID(N'[dbo].[AlertReceiverAlert]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AlertReceiverAlert];
GO
IF OBJECT_ID(N'[dbo].[AlertRecipient]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AlertRecipient];
GO
IF OBJECT_ID(N'[dbo].[AlertType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AlertType];
GO
IF OBJECT_ID(N'[dbo].[aspnet_Membership]', 'U') IS NOT NULL
    DROP TABLE [dbo].[aspnet_Membership];
GO
IF OBJECT_ID(N'[dbo].[aspnet_Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[aspnet_Roles];
GO
IF OBJECT_ID(N'[dbo].[aspnet_Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[aspnet_Users];
GO
IF OBJECT_ID(N'[dbo].[Client]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Client];
GO
IF OBJECT_ID(N'[dbo].[ClientDevice]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClientDevice];
GO
IF OBJECT_ID(N'[dbo].[ClientType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClientType];
GO
IF OBJECT_ID(N'[dbo].[CompanySettings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CompanySettings];
GO
IF OBJECT_ID(N'[dbo].[Controller]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Controller];
GO
IF OBJECT_ID(N'[dbo].[ControllerAction]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ControllerAction];
GO
IF OBJECT_ID(N'[dbo].[ControllerActionRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ControllerActionRole];
GO
IF OBJECT_ID(N'[dbo].[CustomerType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CustomerType];
GO
IF OBJECT_ID(N'[dbo].[Device]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Device];
GO
IF OBJECT_ID(N'[dbo].[DeviceModel]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeviceModel];
GO
IF OBJECT_ID(N'[dbo].[DeviceType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeviceType];
GO
IF OBJECT_ID(N'[dbo].[Dispatch]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Dispatch];
GO
IF OBJECT_ID(N'[dbo].[DispatchAccessibility]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DispatchAccessibility];
GO
IF OBJECT_ID(N'[dbo].[DispatchExtras]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DispatchExtras];
GO
IF OBJECT_ID(N'[dbo].[DispatchStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DispatchStatus];
GO
IF OBJECT_ID(N'[dbo].[DriverEndosment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DriverEndosment];
GO
IF OBJECT_ID(N'[dbo].[DriverLicenceType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DriverLicenceType];
GO
IF OBJECT_ID(N'[dbo].[DriverStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DriverStatus];
GO
IF OBJECT_ID(N'[dbo].[DriverVehicleAssignment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DriverVehicleAssignment];
GO
IF OBJECT_ID(N'[dbo].[Extras]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Extras];
GO
IF OBJECT_ID(N'[dbo].[FuelType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FuelType];
GO
IF OBJECT_ID(N'[dbo].[GeoFence]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GeoFence];
GO
IF OBJECT_ID(N'[dbo].[GeoFenceVehicle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GeoFenceVehicle];
GO
IF OBJECT_ID(N'[dbo].[Group]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Group];
GO
IF OBJECT_ID(N'[dbo].[Insuarance]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Insuarance];
GO
IF OBJECT_ID(N'[dbo].[InsuaranceType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InsuaranceType];
GO
IF OBJECT_ID(N'[dbo].[LicenceType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LicenceType];
GO
IF OBJECT_ID(N'[dbo].[Module]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Module];
GO
IF OBJECT_ID(N'[dbo].[PaymentMode]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaymentMode];
GO
IF OBJECT_ID(N'[dbo].[Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person];
GO
IF OBJECT_ID(N'[dbo].[SentSMS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SentSMS];
GO
IF OBJECT_ID(N'[dbo].[TrackAlertConfig]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TrackAlertConfig];
GO
IF OBJECT_ID(N'[dbo].[Vehicle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicle];
GO
IF OBJECT_ID(N'[dbo].[VehicleDevice]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleDevice];
GO
IF OBJECT_ID(N'[dbo].[VehicleFuelManagement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleFuelManagement];
GO
IF OBJECT_ID(N'[dbo].[VehicleGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleGroup];
GO
IF OBJECT_ID(N'[dbo].[VehicleInsuarance]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleInsuarance];
GO
IF OBJECT_ID(N'[dbo].[VehicleLicenceDetail]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleLicenceDetail];
GO
IF OBJECT_ID(N'[dbo].[VehicleLicenceType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleLicenceType];
GO
IF OBJECT_ID(N'[dbo].[VehicleServiceDetail]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleServiceDetail];
GO
IF OBJECT_ID(N'[dbo].[VehicleStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleStatus];
GO
IF OBJECT_ID(N'[dbo].[VehicleType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VehicleType];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_ClientTypeDetails]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_ClientTypeDetails];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_ControllerView]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_ControllerView];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_DriverAssignmentStatus]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_DriverAssignmentStatus];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_DriverLicenceTypeRpt]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_DriverLicenceTypeRpt];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_DriverStatus]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_DriverStatus];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_DriverStatusRpt]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_DriverStatusRpt];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_DriverVehicleAssignment]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_DriverVehicleAssignment];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_GeoFence]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_GeoFence];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_UnallocatedDevices]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_UnallocatedDevices];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_VehicleAssignment]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_VehicleAssignment];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_VehicleDetails]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_VehicleDetails];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_VehicleFuelMngtRpt]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_VehicleFuelMngtRpt];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_VehicleInsuranceRpt]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_VehicleInsuranceRpt];
GO
IF OBJECT_ID(N'[GFleetV3ModelStoreContainer].[vw_VehicleLicenceDetailRpt]', 'U') IS NOT NULL
    DROP TABLE [GFleetV3ModelStoreContainer].[vw_VehicleLicenceDetailRpt];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'aspnet_Roles'
CREATE TABLE [dbo].[aspnet_Roles] (
    [ApplicationId] uniqueidentifier  NOT NULL,
    [RoleId] uniqueidentifier  NOT NULL,
    [RoleName] nvarchar(256)  NOT NULL,
    [LoweredRoleName] nvarchar(256)  NOT NULL,
    [Description] nvarchar(256)  NULL
);
GO

-- Creating table 'DriverStatus'
CREATE TABLE [dbo].[DriverStatus] (
    [DriverStatusId] int IDENTITY(1,1) NOT NULL,
    [Description] varchar(50)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [IsAvailable] bit  NOT NULL,
    [ClientId] int  NOT NULL,
    [DriverStatusName] varchar(30)  NOT NULL
);
GO

-- Creating table 'VehicleServiceDetails'
CREATE TABLE [dbo].[VehicleServiceDetails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ServiceItem] varchar(30)  NULL,
    [Action] varchar(50)  NULL,
    [VehicleServiceId] int  NULL,
    [DateModified] datetime  NULL,
    [ModifiedBy] uniqueidentifier  NULL,
    [ClientId] int  NULL
);
GO

-- Creating table 'People'
CREATE TABLE [dbo].[People] (
    [PersonId] int IDENTITY(1,1) NOT NULL,
    [UserId] uniqueidentifier  NULL,
    [FirstName] nvarchar(50)  NULL,
    [LastName] nvarchar(50)  NULL,
    [Email1] varchar(50)  NULL,
    [Email2] varchar(50)  NULL,
    [IsMale] bit  NULL,
    [PostCode] nchar(10)  NULL,
    [Telephone1] varchar(30)  NULL,
    [Telephone2] varchar(30)  NULL,
    [FaxNumber] varchar(30)  NULL,
    [DOB] datetime  NULL,
    [IDNumber] nchar(10)  NULL,
    [Image] varbinary(max)  NULL,
    [EmployeeNo] nchar(10)  NULL,
    [NextOfKinName1] varchar(50)  NULL,
    [NextOfKinName2] varchar(50)  NULL,
    [NextOfKinPhone1] varchar(20)  NULL,
    [NextOfKinPhone2] varchar(20)  NULL,
    [Association1] varchar(30)  NULL,
    [Association2] varchar(30)  NULL,
    [NextOfKinEmail1] varchar(40)  NULL,
    [NextOfKinEmail2] varchar(40)  NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [Active] bit  NULL,
    [ClientId] int  NULL
);
GO

-- Creating table 'DriverEndosments'
CREATE TABLE [dbo].[DriverEndosments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PersonId] int  NOT NULL,
    [Endosment] varchar(30)  NULL,
    [UserId] uniqueidentifier  NULL,
    [Like] bit  NULL,
    [ModifiedBy] uniqueidentifier  NULL,
    [DateModified] datetime  NULL,
    [ClientId] int  NULL
);
GO

-- Creating table 'vw_ControllerView'
CREATE TABLE [dbo].[vw_ControllerView] (
    [ActionName] varchar(20)  NULL,
    [ControllerName] varchar(40)  NULL,
    [Url] varchar(40)  NULL,
    [ControllerActive] bit  NULL,
    [ModuleName] varchar(50)  NULL,
    [ControllerId] int  NOT NULL,
    [IsEnabled] bit  NOT NULL,
    [ControllerActionId] int  NOT NULL,
    [ActionId] int  NOT NULL,
    [RoleName] nvarchar(256)  NOT NULL,
    [ParentId] int  NULL,
    [ControllerActionRoleId] int  NOT NULL,
    [RoleId] uniqueidentifier  NOT NULL,
    [ModuleId] int  NOT NULL,
    [ControllerImgPath] varchar(200)  NULL,
    [Description] varchar(100)  NULL
);
GO

-- Creating table 'GFleetV3Controllers'
CREATE TABLE [dbo].[GFleetV3Controllers] (
    [ControllerId] int IDENTITY(1,1) NOT NULL,
    [ControllerName] varchar(40)  NULL,
    [Url] varchar(40)  NULL,
    [ModuleId] int  NOT NULL,
    [Active] bit  NOT NULL,
    [ParentId] int  NULL,
    [ControllerImgPath] varchar(200)  NULL,
    [Description] varchar(100)  NULL
);
GO

-- Creating table 'PaymentModes'
CREATE TABLE [dbo].[PaymentModes] (
    [PaymentModeId] int IDENTITY(1,1) NOT NULL,
    [PaymentMode1] varchar(30)  NOT NULL,
    [ModifiedBy] uniqueidentifier  NULL,
    [DateModified] datetime  NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'aspnet_Users'
CREATE TABLE [dbo].[aspnet_Users] (
    [ApplicationId] uniqueidentifier  NOT NULL,
    [UserId] uniqueidentifier  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL,
    [LoweredUserName] nvarchar(256)  NOT NULL,
    [MobileAlias] nvarchar(16)  NULL,
    [IsAnonymous] bit  NOT NULL,
    [LastActivityDate] datetime  NOT NULL
);
GO

-- Creating table 'DriverLicenceTypes'
CREATE TABLE [dbo].[DriverLicenceTypes] (
    [DriverLicenceTypeId] int IDENTITY(1,1) NOT NULL,
    [PersonId] int  NOT NULL,
    [LicenceTypeId] int  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'VehicleStatus'
CREATE TABLE [dbo].[VehicleStatus] (
    [VehicleStatusId] int IDENTITY(1,1) NOT NULL,
    [VehicleStatusName] varchar(30)  NOT NULL,
    [Description] varchar(50)  NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [CompanyId] int  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'VehicleTypes'
CREATE TABLE [dbo].[VehicleTypes] (
    [VehicleTypeId] int IDENTITY(1,1) NOT NULL,
    [VehicleTypeName] varchar(30)  NOT NULL,
    [Description] varchar(50)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'VehicleInsuarances'
CREATE TABLE [dbo].[VehicleInsuarances] (
    [VehicleInsuaranceId] int IDENTITY(1,1) NOT NULL,
    [VehicleId] int  NOT NULL,
    [InsuaranceId] int  NOT NULL,
    [InsuaranceTypeId] int  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NOT NULL,
    [PolicyNo] nchar(20)  NOT NULL,
    [InsuaranceCost] decimal(19,4)  NULL
);
GO

-- Creating table 'VehicleLicenceDetails'
CREATE TABLE [dbo].[VehicleLicenceDetails] (
    [VehicleLicenceDetailId] int IDENTITY(1,1) NOT NULL,
    [VehicleId] int  NOT NULL,
    [LicenceTypeId] int  NOT NULL,
    [RenewalDate] datetime  NOT NULL,
    [ExpireDate] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [RenewalCost] decimal(19,4)  NULL,
    [LicenceNo] nchar(20)  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'LicenceTypes'
CREATE TABLE [dbo].[LicenceTypes] (
    [LicenceTypeId] int IDENTITY(1,1) NOT NULL,
    [DriverLicenceType] varchar(80)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL
);
GO

-- Creating table 'Vehicles'
CREATE TABLE [dbo].[Vehicles] (
    [VehicleId] int IDENTITY(1,1) NOT NULL,
    [RegistrationNo] varchar(50)  NOT NULL,
    [Year] int  NOT NULL,
    [VehicleTypeId] int  NOT NULL,
    [Color] varchar(30)  NULL,
    [Make] varchar(20)  NULL,
    [Model] varchar(20)  NULL,
    [FuelTypeId] int  NOT NULL,
    [EngineNo] varchar(20)  NULL,
    [ChasisNo] varchar(20)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [VehicleStatusId] int  NOT NULL,
    [NickName] varchar(20)  NULL,
    [ClientId] int  NOT NULL,
    [AvgFuelConsumption] int  NOT NULL,
    [VehicleRegCode] varchar(73)  NULL
);
GO

-- Creating table 'Extras'
CREATE TABLE [dbo].[Extras] (
    [ExtrasId] int IDENTITY(1,1) NOT NULL,
    [Extras] varchar(30)  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'VehicleLicenceTypes'
CREATE TABLE [dbo].[VehicleLicenceTypes] (
    [VehicleLicenceTypeId] int IDENTITY(1,1) NOT NULL,
    [VehicleLicenceTypeName] varchar(30)  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'DispatchStatus'
CREATE TABLE [dbo].[DispatchStatus] (
    [DispatchStatusId] int IDENTITY(1,1) NOT NULL,
    [DispatchStatus] varchar(30)  NOT NULL,
    [Description] varchar(50)  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'DispatchExtras'
CREATE TABLE [dbo].[DispatchExtras] (
    [DispatchExtrasId] int IDENTITY(1,1) NOT NULL,
    [ExtrasId] int  NOT NULL,
    [DispatchId] int  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateTime] datetime  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'Accesibilities'
CREATE TABLE [dbo].[Accesibilities] (
    [AccesibilityId] int IDENTITY(1,1) NOT NULL,
    [Accesibility1] varchar(30)  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'CustomerTypes'
CREATE TABLE [dbo].[CustomerTypes] (
    [CustomerTypeId] int IDENTITY(1,1) NOT NULL,
    [CustomerTypeName] varchar(40)  NOT NULL,
    [Description] varchar(50)  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'DispatchAccessibilities'
CREATE TABLE [dbo].[DispatchAccessibilities] (
    [DispacthAccessibilityId] int IDENTITY(1,1) NOT NULL,
    [AccessibilityId] int  NOT NULL,
    [DispatchId] int  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'DriverVehicleAssignments'
CREATE TABLE [dbo].[DriverVehicleAssignments] (
    [DriverVehicleAssignmentId] int IDENTITY(1,1) NOT NULL,
    [PersonId] int  NOT NULL,
    [VehicleId] int  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [Notes] varchar(30)  NULL,
    [ClientId] int  NOT NULL,
    [StartGeoInfo] bigint  NULL,
    [EndGeoInfo] bigint  NULL,
    [ActualStartDate] datetime  NULL,
    [ActualEndDate] datetime  NULL
);
GO

-- Creating table 'FuelTypes'
CREATE TABLE [dbo].[FuelTypes] (
    [FuelTypeId] int IDENTITY(1,1) NOT NULL,
    [FuelTypeName] varchar(20)  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'VehicleFuelManagements'
CREATE TABLE [dbo].[VehicleFuelManagements] (
    [VehicleFuelManagementId] int IDENTITY(1,1) NOT NULL,
    [VehicleId] int  NOT NULL,
    [Date] datetime  NOT NULL,
    [PreviousMileage] int  NOT NULL,
    [CurrentMileage] int  NOT NULL,
    [Difference] int  NOT NULL,
    [NoOfLitres] int  NOT NULL,
    [PersonId] int  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NOT NULL,
    [AvgFuelCons] float  NULL
);
GO

-- Creating table 'Insuarances'
CREATE TABLE [dbo].[Insuarances] (
    [InsuaranceId] int IDENTITY(1,1) NOT NULL,
    [InsuaranceName] varchar(30)  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [Address] varchar(30)  NULL,
    [Telephone] varchar(30)  NULL,
    [Email] varchar(30)  NULL,
    [ContactPerson] varchar(30)  NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'InsuaranceTypes'
CREATE TABLE [dbo].[InsuaranceTypes] (
    [InsuaranceTypeId] int IDENTITY(1,1) NOT NULL,
    [InsuaranceTypeName] varchar(30)  NOT NULL,
    [Description] varchar(100)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'vw_DriverStatus'
CREATE TABLE [dbo].[vw_DriverStatus] (
    [DriverStatusId] int  NOT NULL,
    [DriverStatusName] varchar(30)  NOT NULL,
    [IsAvailable] bit  NOT NULL,
    [PersonId] int  NOT NULL,
    [Names] nvarchar(101)  NULL,
    [Telephone] varchar(61)  NULL
);
GO

-- Creating table 'vw_DriverVehicleAssignment'
CREATE TABLE [dbo].[vw_DriverVehicleAssignment] (
    [PersonId] int  NOT NULL,
    [Names] nvarchar(101)  NULL,
    [Telephones] varchar(61)  NULL,
    [VehicleId] int  NOT NULL,
    [VehicleRegCode] varchar(38)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [DriverVehicleAssignmentId] int  NOT NULL
);
GO

-- Creating table 'vw_VehicleAssignment'
CREATE TABLE [dbo].[vw_VehicleAssignment] (
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [VehicleRegCode] varchar(38)  NULL,
    [VehicleId] int  NOT NULL,
    [AssignmentStatus] bit  NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NULL,
    [DriverVehicleAssignmentStatusId] int  NULL,
    [ClientId] int  NOT NULL,
    [DriverVehicleAssignmentId] int  NOT NULL
);
GO

-- Creating table 'Groups'
CREATE TABLE [dbo].[Groups] (
    [GroupId] int IDENTITY(1,1) NOT NULL,
    [GroupName] varchar(50)  NOT NULL,
    [Active] bit  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ClientId] int  NULL
);
GO

-- Creating table 'VehicleDevices'
CREATE TABLE [dbo].[VehicleDevices] (
    [VehicleDeviceId] int IDENTITY(1,1) NOT NULL,
    [VehicleId] int  NOT NULL,
    [DeviceId] int  NOT NULL,
    [Status] bit  NOT NULL
);
GO

-- Creating table 'VehicleGroups'
CREATE TABLE [dbo].[VehicleGroups] (
    [VehicleGroupId] int IDENTITY(1,1) NOT NULL,
    [VehicleId] int  NOT NULL,
    [GroupId] int  NOT NULL,
    [Status] bit  NOT NULL
);
GO

-- Creating table 'Devices'
CREATE TABLE [dbo].[Devices] (
    [DeviceId] int IDENTITY(1,1) NOT NULL,
    [SerialNo] varchar(50)  NOT NULL,
    [PhoneNo] varchar(20)  NOT NULL,
    [DeviceModelId] int  NOT NULL,
    [ClientId] int  NOT NULL,
    [DeviceTypeId] int  NULL
);
GO

-- Creating table 'vw_UnallocatedDevices'
CREATE TABLE [dbo].[vw_UnallocatedDevices] (
    [PhoneNo] varchar(20)  NOT NULL,
    [DeviceId] int IDENTITY(1,1) NOT NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [ClientId] int IDENTITY(1,1) NOT NULL,
    [ClientName] varchar(50)  NOT NULL,
    [ClientAddress] varchar(50)  NULL,
    [ClientTelephone] varchar(50)  NOT NULL,
    [ClientEmail] varchar(50)  NOT NULL,
    [ClientPhysicalAddress] varchar(50)  NULL,
    [Active] bit  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ContactPerson] varchar(50)  NOT NULL,
    [ClientTypeId] int  NOT NULL
);
GO

-- Creating table 'ClientTypes'
CREATE TABLE [dbo].[ClientTypes] (
    [ClientTypeId] int IDENTITY(1,1) NOT NULL,
    [ClientTypeName] varchar(30)  NOT NULL,
    [Active] bit  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [LogoUrl] varchar(200)  NULL
);
GO

-- Creating table 'aspnet_Membership'
CREATE TABLE [dbo].[aspnet_Membership] (
    [ApplicationId] uniqueidentifier  NOT NULL,
    [UserId] uniqueidentifier  NOT NULL,
    [Password] nvarchar(128)  NOT NULL,
    [PasswordFormat] int  NOT NULL,
    [PasswordSalt] nvarchar(128)  NOT NULL,
    [MobilePIN] nvarchar(16)  NULL,
    [Email] nvarchar(256)  NULL,
    [LoweredEmail] nvarchar(256)  NULL,
    [PasswordQuestion] nvarchar(256)  NULL,
    [PasswordAnswer] nvarchar(128)  NULL,
    [IsApproved] bit  NOT NULL,
    [IsLockedOut] bit  NOT NULL,
    [CreateDate] datetime  NOT NULL,
    [LastLoginDate] datetime  NOT NULL,
    [LastPasswordChangedDate] datetime  NOT NULL,
    [LastLockoutDate] datetime  NOT NULL,
    [FailedPasswordAttemptCount] int  NOT NULL,
    [FailedPasswordAttemptWindowStart] datetime  NOT NULL,
    [FailedPasswordAnswerAttemptCount] int  NOT NULL,
    [FailedPasswordAnswerAttemptWindowStart] datetime  NOT NULL,
    [Comment] nvarchar(max)  NULL
);
GO

-- Creating table 'vw_DriverAssignmentStatus'
CREATE TABLE [dbo].[vw_DriverAssignmentStatus] (
    [Names] nvarchar(101)  NULL,
    [PersonId] int  NOT NULL,
    [VehicleId] int  NOT NULL,
    [RegistrationNo] varchar(15)  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [Notes] varchar(30)  NULL,
    [ClientId] int  NOT NULL,
    [AssignmentStatus] bit  NULL,
    [UserName] nvarchar(256)  NOT NULL,
    [DriverVehicleAssignmentId] int  NOT NULL,
    [SerialNo] varchar(50)  NOT NULL,
    [StartGeoInfo] bigint  NULL,
    [EndGeoInfo] bigint  NULL
);
GO

-- Creating table 'vw_DriverLicenceTypeRpt'
CREATE TABLE [dbo].[vw_DriverLicenceTypeRpt] (
    [PersonId] int  NOT NULL,
    [FirstName] nvarchar(50)  NULL,
    [LastName] nvarchar(50)  NULL,
    [Email1] varchar(50)  NULL,
    [Email2] varchar(50)  NULL,
    [IsMale] bit  NULL,
    [PostCode] nchar(10)  NULL,
    [ClientId] int  NULL,
    [CofCNo] varchar(20)  NULL,
    [IDNumber] nchar(10)  NULL,
    [SpecialConditions] varchar(50)  NULL,
    [LicenceDate] datetime  NULL,
    [DOB] datetime  NULL,
    [DriverStatusId] int  NULL,
    [LicenceTypeId] int  NOT NULL,
    [DriverLicenceType] varchar(80)  NULL,
    [Telephone1] varchar(30)  NULL,
    [Telephone2] varchar(30)  NULL
);
GO

-- Creating table 'vw_DriverStatusRpt'
CREATE TABLE [dbo].[vw_DriverStatusRpt] (
    [FirstName] nvarchar(50)  NULL,
    [LastName] nvarchar(50)  NULL,
    [Email1] varchar(50)  NULL,
    [Email2] varchar(50)  NULL,
    [IsMale] bit  NULL,
    [PostCode] nchar(10)  NULL,
    [Telephone1] varchar(30)  NULL,
    [Telephone2] varchar(30)  NULL,
    [SpecialConditions] varchar(50)  NULL,
    [IDNumber] nchar(10)  NULL,
    [CofCNo] varchar(20)  NULL,
    [DriverStatusId] int  NULL,
    [DriverStatusName] varchar(30)  NOT NULL,
    [ClientId] int  NULL,
    [PersonId] int  NOT NULL
);
GO

-- Creating table 'vw_VehicleFuelMngtRpt'
CREATE TABLE [dbo].[vw_VehicleFuelMngtRpt] (
    [VehicleId] int  NOT NULL,
    [Date] datetime  NOT NULL,
    [Difference] int  NOT NULL,
    [NoOfLitres] int  NOT NULL,
    [PersonId] int  NOT NULL,
    [VehicleRegCode] varchar(38)  NULL,
    [ClientId] int  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [AvgFuelCons] float  NULL
);
GO

-- Creating table 'vw_VehicleInsuranceRpt'
CREATE TABLE [dbo].[vw_VehicleInsuranceRpt] (
    [VehicleId] int  NOT NULL,
    [InsuaranceId] int  NOT NULL,
    [InsuaranceTypeId] int  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [PolicyNo] nchar(20)  NOT NULL,
    [InsuaranceCost] decimal(19,4)  NULL,
    [ClientId] int  NOT NULL,
    [InsuaranceName] varchar(30)  NOT NULL,
    [InsuaranceTypeName] varchar(30)  NOT NULL,
    [VehicleRegCode] varchar(38)  NULL
);
GO

-- Creating table 'vw_VehicleLicenceDetailRpt'
CREATE TABLE [dbo].[vw_VehicleLicenceDetailRpt] (
    [VehicleId] int  NOT NULL,
    [LicenceTypeId] int  NOT NULL,
    [RenewalDate] datetime  NOT NULL,
    [ExpireDate] datetime  NOT NULL,
    [RenewalCost] decimal(19,4)  NULL,
    [LicenceNo] nchar(20)  NOT NULL,
    [ClientId] int  NOT NULL,
    [VehicleLicenceTypeName] varchar(30)  NOT NULL,
    [VehicleRegCode] varchar(38)  NULL
);
GO

-- Creating table 'Actions'
CREATE TABLE [dbo].[Actions] (
    [ActionId] int IDENTITY(1,1) NOT NULL,
    [ActionName] varchar(20)  NOT NULL
);
GO

-- Creating table 'ControllerActions'
CREATE TABLE [dbo].[ControllerActions] (
    [ControllerActionId] int IDENTITY(1,1) NOT NULL,
    [ControllerId] int  NOT NULL,
    [ActionId] int  NOT NULL
);
GO

-- Creating table 'ControllerActionRoles'
CREATE TABLE [dbo].[ControllerActionRoles] (
    [ControllerActionRoleId] int IDENTITY(1,1) NOT NULL,
    [RoleId] uniqueidentifier  NOT NULL,
    [ControllerActionId] int  NOT NULL,
    [IsEnabled] bit  NOT NULL
);
GO

-- Creating table 'Modules'
CREATE TABLE [dbo].[Modules] (
    [ModuleId] int IDENTITY(1,1) NOT NULL,
    [ModuleName] varchar(50)  NULL
);
GO

-- Creating table 'AlertLogs'
CREATE TABLE [dbo].[AlertLogs] (
    [AlertLogId] int IDENTITY(1,1) NOT NULL,
    [AlertReceiverAlertId] int  NULL,
    [IsEmail] bit  NOT NULL,
    [IsPhone] bit  NOT NULL,
    [Message] varchar(500)  NOT NULL,
    [Forwarded] bit  NOT NULL,
    [Date] datetime  NOT NULL
);
GO

-- Creating table 'AlertReceiverAlerts'
CREATE TABLE [dbo].[AlertReceiverAlerts] (
    [AlertReceiverAlertId] int IDENTITY(1,1) NOT NULL,
    [AlertTypeId] int  NOT NULL,
    [AlertReceverId] int  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'AlertRecipients'
CREATE TABLE [dbo].[AlertRecipients] (
    [AlertRecipientId] int IDENTITY(1,1) NOT NULL,
    [Names] varchar(50)  NOT NULL,
    [ClientId] int  NOT NULL,
    [EmailAddress] varchar(50)  NOT NULL,
    [MobileNo] varchar(50)  NOT NULL,
    [Active] bit  NOT NULL,
    [ReceiveEmail] bit  NOT NULL,
    [ReceiveSMS] bit  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL
);
GO

-- Creating table 'AlertTypes'
CREATE TABLE [dbo].[AlertTypes] (
    [AlertTypeId] int IDENTITY(1,1) NOT NULL,
    [AlertType1] varchar(30)  NOT NULL,
    [Active] bit  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'vw_VehicleDetails'
CREATE TABLE [dbo].[vw_VehicleDetails] (
    [GroupId] int  NULL,
    [GroupName] varchar(50)  NULL,
    [VehicleGroupId] int  NULL,
    [VehicleId] int  NOT NULL,
    [RegistrationNo] varchar(15)  NOT NULL,
    [VehicleDeviceId] int  NULL,
    [DeviceId] int  NULL,
    [SerialNo] varchar(50)  NULL,
    [PhoneNo] varchar(20)  NULL,
    [ClientId] int  NOT NULL,
    [VehicleSerial] varchar(15)  NOT NULL,
    [VehicleRegCode] varchar(38)  NULL,
    [NickName] varchar(20)  NULL,
    [DeviceModelId] int  NULL,
    [DeviceMake] varchar(50)  NULL,
    [VehicleStatusId] int  NOT NULL,
    [DeviceTypeId] int  NULL,
    [DeviceTypeName] varchar(50)  NULL
);
GO

-- Creating table 'DeviceModels'
CREATE TABLE [dbo].[DeviceModels] (
    [DeviceModelId] int IDENTITY(1,1) NOT NULL,
    [Description] varchar(100)  NULL,
    [Manufacturer] varchar(100)  NULL,
    [DeviceMake] varchar(50)  NOT NULL,
    [ModifiedBy] uniqueidentifier  NULL,
    [DateModified] datetime  NOT NULL
);
GO

-- Creating table 'GeoFenceVehicles'
CREATE TABLE [dbo].[GeoFenceVehicles] (
    [FenceVehicleId] int IDENTITY(1,1) NOT NULL,
    [GeoFenceId] int  NOT NULL,
    [GroupId] int  NOT NULL,
    [ModifiedBy] uniqueidentifier  NULL,
    [ClientId] int  NOT NULL,
    [Active] bit  NOT NULL,
    [Date] datetime  NOT NULL
);
GO

-- Creating table 'SentSMS'
CREATE TABLE [dbo].[SentSMS] (
    [SentSMSId] int IDENTITY(1,1) NOT NULL,
    [Text] varchar(max)  NOT NULL,
    [Date] datetime  NOT NULL,
    [Sender] uniqueidentifier  NULL,
    [PhoneNo] varchar(max)  NULL,
    [Response] varchar(max)  NULL,
    [ClientId] int  NOT NULL
);
GO

-- Creating table 'AlertConfigTypes'
CREATE TABLE [dbo].[AlertConfigTypes] (
    [AlertConfigTypeId] int IDENTITY(1,1) NOT NULL,
    [AlertConfigName] varchar(50)  NOT NULL
);
GO

-- Creating table 'TrackAlertConfigs'
CREATE TABLE [dbo].[TrackAlertConfigs] (
    [TrackAlertConfigId] int IDENTITY(1,1) NOT NULL,
    [AlertConfigTypeId] int  NOT NULL,
    [UserId] uniqueidentifier  NOT NULL,
    [ClientId] int  NOT NULL,
    [IsEnabled] bit  NOT NULL,
    [Date] datetime  NOT NULL,
    [Reason] varchar(50)  NOT NULL
);
GO

-- Creating table 'CompanySettings'
CREATE TABLE [dbo].[CompanySettings] (
    [CompanySettingsId] int IDENTITY(1,1) NOT NULL,
    [ClientId] int  NOT NULL,
    [TableGridHeight] varchar(30)  NOT NULL
);
GO

-- Creating table 'GeoFences'
CREATE TABLE [dbo].[GeoFences] (
    [GeoFenceId] int IDENTITY(1,1) NOT NULL,
    [GeoFenceName] varchar(50)  NOT NULL,
    [Active] bit  NOT NULL,
    [Date] datetime  NOT NULL,
    [ClientId] int  NOT NULL,
    [ReportOnExit] bit  NULL,
    [FreqInMin_Exit] int  NULL,
    [ExitActive] bit  NULL,
    [NoOfAlerts_Exit] int  NULL,
    [ReportOnEntry] bit  NULL,
    [FreqInMin_Entry] int  NULL,
    [NoOfAlerts_Entry] int  NULL,
    [EntryActive] bit  NULL,
    [PhoneNumber] varchar(max)  NULL,
    [Email] varchar(max)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [Speed] int  NULL,
    [IsSpeedGeoFence] bit  NOT NULL
);
GO

-- Creating table 'DeviceTypes'
CREATE TABLE [dbo].[DeviceTypes] (
    [DeviceTypeId] int IDENTITY(1,1) NOT NULL,
    [DeviceTypeName] varchar(50)  NULL
);
GO

-- Creating table 'vw_ClientTypeDetails'
CREATE TABLE [dbo].[vw_ClientTypeDetails] (
    [ClientId] int  NOT NULL,
    [ClientTypeName] varchar(30)  NOT NULL,
    [LogoUrl] varchar(200)  NULL
);
GO

-- Creating table 'vw_GeoFence'
CREATE TABLE [dbo].[vw_GeoFence] (
    [ReportOnExit] bit  NULL,
    [FreqInMin_Exit] int  NULL,
    [ExitActive] bit  NULL,
    [NoOfAlerts_Exit] int  NULL,
    [ReportOnEntry] bit  NULL,
    [FreqInMin_Entry] int  NULL,
    [NoOfAlerts_Entry] int  NULL,
    [EntryActive] bit  NULL,
    [PhoneNumber] varchar(max)  NULL,
    [Email] varchar(max)  NULL,
    [ModifiedBy] uniqueidentifier  NOT NULL,
    [GroupIds] varchar(max)  NULL,
    [GeoFenceName] varchar(50)  NOT NULL,
    [Active] bit  NOT NULL,
    [Geom] varchar(max)  NULL,
    [Speed] int  NULL,
    [IsSpeedGeoFence] bit  NOT NULL,
    [Date] datetime  NOT NULL,
    [GeoFenceId] int IDENTITY(1,1) NOT NULL,
    [ClientId] int  NOT NULL,
    [FenceVehicleId] varchar(max)  NULL
);
GO

-- Creating table 'Dispatches'
CREATE TABLE [dbo].[Dispatches] (
    [DispatchId] int IDENTITY(1,1) NOT NULL,
    [To] varchar(150)  NOT NULL,
    [From] varchar(150)  NOT NULL,
    [PickUpPointName] varchar(30)  NOT NULL,
    [DropOffPointName] varchar(50)  NOT NULL,
    [PickUpDateTime] datetime  NOT NULL,
    [DispatchedBy] uniqueidentifier  NULL,
    [BookedBy] uniqueidentifier  NOT NULL,
    [DateModified] datetime  NOT NULL,
    [Amount] decimal(19,4)  NULL,
    [Distance] int  NOT NULL,
    [ExpectedCompletionTime] datetime  NULL,
    [TimeCompleted] datetime  NULL,
    [CustomerId] int  NOT NULL,
    [ReturnId] int  NULL,
    [DispatchedStateId] int  NULL,
    [NoOfPassengers] int  NOT NULL,
    [NoOfLuggage] int  NULL,
    [Isreccurent] bit  NOT NULL,
    [ReccurentMinutes] int  NULL,
    [PaymentModeId] int  NOT NULL,
    [Notes] varchar(50)  NULL,
    [ClientId] int  NOT NULL,
    [DriverVehicleAssignmentId] int  NULL,
    [VehicleId] int  NULL
);
GO

-- Creating table 'ClientDevices'
CREATE TABLE [dbo].[ClientDevices] (
    [ClientDeviceId] int IDENTITY(1,1) NOT NULL,
    [ClientId] int  NOT NULL,
    [DeviceId] int  NOT NULL,
    [Date] datetime  NOT NULL
);
GO

-- Creating table 'People_Driver'
CREATE TABLE [dbo].[People_Driver] (
    [LicenceDate] datetime  NULL,
    [SpecialConditions] varchar(50)  NULL,
    [CofCNo] varchar(20)  NULL,
    [DriverStatusId] int  NULL,
    [PersonId] int  NOT NULL
);
GO

-- Creating table 'People_Customer'
CREATE TABLE [dbo].[People_Customer] (
    [CreditLimit] decimal(19,4)  NULL,
    [CustomerTypeId] int  NULL,
    [PersonId] int  NOT NULL
);
GO

-- Creating table 'People_SystemUser'
CREATE TABLE [dbo].[People_SystemUser] (
    [PersonId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [RoleId] in table 'aspnet_Roles'
ALTER TABLE [dbo].[aspnet_Roles]
ADD CONSTRAINT [PK_aspnet_Roles]
    PRIMARY KEY CLUSTERED ([RoleId] ASC);
GO

-- Creating primary key on [DriverStatusId] in table 'DriverStatus'
ALTER TABLE [dbo].[DriverStatus]
ADD CONSTRAINT [PK_DriverStatus]
    PRIMARY KEY CLUSTERED ([DriverStatusId] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleServiceDetails'
ALTER TABLE [dbo].[VehicleServiceDetails]
ADD CONSTRAINT [PK_VehicleServiceDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [PersonId] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [PK_People]
    PRIMARY KEY CLUSTERED ([PersonId] ASC);
GO

-- Creating primary key on [Id] in table 'DriverEndosments'
ALTER TABLE [dbo].[DriverEndosments]
ADD CONSTRAINT [PK_DriverEndosments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ControllerId], [IsEnabled], [ControllerActionId], [ActionId], [RoleName], [ControllerActionRoleId], [RoleId], [ModuleId] in table 'vw_ControllerView'
ALTER TABLE [dbo].[vw_ControllerView]
ADD CONSTRAINT [PK_vw_ControllerView]
    PRIMARY KEY CLUSTERED ([ControllerId], [IsEnabled], [ControllerActionId], [ActionId], [RoleName], [ControllerActionRoleId], [RoleId], [ModuleId] ASC);
GO

-- Creating primary key on [ControllerId] in table 'GFleetV3Controllers'
ALTER TABLE [dbo].[GFleetV3Controllers]
ADD CONSTRAINT [PK_GFleetV3Controllers]
    PRIMARY KEY CLUSTERED ([ControllerId] ASC);
GO

-- Creating primary key on [PaymentModeId] in table 'PaymentModes'
ALTER TABLE [dbo].[PaymentModes]
ADD CONSTRAINT [PK_PaymentModes]
    PRIMARY KEY CLUSTERED ([PaymentModeId] ASC);
GO

-- Creating primary key on [UserId] in table 'aspnet_Users'
ALTER TABLE [dbo].[aspnet_Users]
ADD CONSTRAINT [PK_aspnet_Users]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [DriverLicenceTypeId] in table 'DriverLicenceTypes'
ALTER TABLE [dbo].[DriverLicenceTypes]
ADD CONSTRAINT [PK_DriverLicenceTypes]
    PRIMARY KEY CLUSTERED ([DriverLicenceTypeId] ASC);
GO

-- Creating primary key on [VehicleStatusId] in table 'VehicleStatus'
ALTER TABLE [dbo].[VehicleStatus]
ADD CONSTRAINT [PK_VehicleStatus]
    PRIMARY KEY CLUSTERED ([VehicleStatusId] ASC);
GO

-- Creating primary key on [VehicleTypeId] in table 'VehicleTypes'
ALTER TABLE [dbo].[VehicleTypes]
ADD CONSTRAINT [PK_VehicleTypes]
    PRIMARY KEY CLUSTERED ([VehicleTypeId] ASC);
GO

-- Creating primary key on [VehicleInsuaranceId] in table 'VehicleInsuarances'
ALTER TABLE [dbo].[VehicleInsuarances]
ADD CONSTRAINT [PK_VehicleInsuarances]
    PRIMARY KEY CLUSTERED ([VehicleInsuaranceId] ASC);
GO

-- Creating primary key on [VehicleLicenceDetailId] in table 'VehicleLicenceDetails'
ALTER TABLE [dbo].[VehicleLicenceDetails]
ADD CONSTRAINT [PK_VehicleLicenceDetails]
    PRIMARY KEY CLUSTERED ([VehicleLicenceDetailId] ASC);
GO

-- Creating primary key on [LicenceTypeId] in table 'LicenceTypes'
ALTER TABLE [dbo].[LicenceTypes]
ADD CONSTRAINT [PK_LicenceTypes]
    PRIMARY KEY CLUSTERED ([LicenceTypeId] ASC);
GO

-- Creating primary key on [VehicleId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [PK_Vehicles]
    PRIMARY KEY CLUSTERED ([VehicleId] ASC);
GO

-- Creating primary key on [ExtrasId] in table 'Extras'
ALTER TABLE [dbo].[Extras]
ADD CONSTRAINT [PK_Extras]
    PRIMARY KEY CLUSTERED ([ExtrasId] ASC);
GO

-- Creating primary key on [VehicleLicenceTypeId] in table 'VehicleLicenceTypes'
ALTER TABLE [dbo].[VehicleLicenceTypes]
ADD CONSTRAINT [PK_VehicleLicenceTypes]
    PRIMARY KEY CLUSTERED ([VehicleLicenceTypeId] ASC);
GO

-- Creating primary key on [DispatchStatusId] in table 'DispatchStatus'
ALTER TABLE [dbo].[DispatchStatus]
ADD CONSTRAINT [PK_DispatchStatus]
    PRIMARY KEY CLUSTERED ([DispatchStatusId] ASC);
GO

-- Creating primary key on [DispatchExtrasId] in table 'DispatchExtras'
ALTER TABLE [dbo].[DispatchExtras]
ADD CONSTRAINT [PK_DispatchExtras]
    PRIMARY KEY CLUSTERED ([DispatchExtrasId] ASC);
GO

-- Creating primary key on [AccesibilityId] in table 'Accesibilities'
ALTER TABLE [dbo].[Accesibilities]
ADD CONSTRAINT [PK_Accesibilities]
    PRIMARY KEY CLUSTERED ([AccesibilityId] ASC);
GO

-- Creating primary key on [CustomerTypeId] in table 'CustomerTypes'
ALTER TABLE [dbo].[CustomerTypes]
ADD CONSTRAINT [PK_CustomerTypes]
    PRIMARY KEY CLUSTERED ([CustomerTypeId] ASC);
GO

-- Creating primary key on [DispacthAccessibilityId] in table 'DispatchAccessibilities'
ALTER TABLE [dbo].[DispatchAccessibilities]
ADD CONSTRAINT [PK_DispatchAccessibilities]
    PRIMARY KEY CLUSTERED ([DispacthAccessibilityId] ASC);
GO

-- Creating primary key on [DriverVehicleAssignmentId] in table 'DriverVehicleAssignments'
ALTER TABLE [dbo].[DriverVehicleAssignments]
ADD CONSTRAINT [PK_DriverVehicleAssignments]
    PRIMARY KEY CLUSTERED ([DriverVehicleAssignmentId] ASC);
GO

-- Creating primary key on [FuelTypeId] in table 'FuelTypes'
ALTER TABLE [dbo].[FuelTypes]
ADD CONSTRAINT [PK_FuelTypes]
    PRIMARY KEY CLUSTERED ([FuelTypeId] ASC);
GO

-- Creating primary key on [VehicleFuelManagementId] in table 'VehicleFuelManagements'
ALTER TABLE [dbo].[VehicleFuelManagements]
ADD CONSTRAINT [PK_VehicleFuelManagements]
    PRIMARY KEY CLUSTERED ([VehicleFuelManagementId] ASC);
GO

-- Creating primary key on [InsuaranceId] in table 'Insuarances'
ALTER TABLE [dbo].[Insuarances]
ADD CONSTRAINT [PK_Insuarances]
    PRIMARY KEY CLUSTERED ([InsuaranceId] ASC);
GO

-- Creating primary key on [InsuaranceTypeId] in table 'InsuaranceTypes'
ALTER TABLE [dbo].[InsuaranceTypes]
ADD CONSTRAINT [PK_InsuaranceTypes]
    PRIMARY KEY CLUSTERED ([InsuaranceTypeId] ASC);
GO

-- Creating primary key on [DriverStatusId], [DriverStatusName], [IsAvailable], [PersonId] in table 'vw_DriverStatus'
ALTER TABLE [dbo].[vw_DriverStatus]
ADD CONSTRAINT [PK_vw_DriverStatus]
    PRIMARY KEY CLUSTERED ([DriverStatusId], [DriverStatusName], [IsAvailable], [PersonId] ASC);
GO

-- Creating primary key on [PersonId], [VehicleId], [ModifiedBy], [StartDate], [EndDate], [DateModified], [DriverVehicleAssignmentId] in table 'vw_DriverVehicleAssignment'
ALTER TABLE [dbo].[vw_DriverVehicleAssignment]
ADD CONSTRAINT [PK_vw_DriverVehicleAssignment]
    PRIMARY KEY CLUSTERED ([PersonId], [VehicleId], [ModifiedBy], [StartDate], [EndDate], [DateModified], [DriverVehicleAssignmentId] ASC);
GO

-- Creating primary key on [StartDate], [EndDate], [VehicleId], [FirstName], [ClientId], [DriverVehicleAssignmentId] in table 'vw_VehicleAssignment'
ALTER TABLE [dbo].[vw_VehicleAssignment]
ADD CONSTRAINT [PK_vw_VehicleAssignment]
    PRIMARY KEY CLUSTERED ([StartDate], [EndDate], [VehicleId], [FirstName], [ClientId], [DriverVehicleAssignmentId] ASC);
GO

-- Creating primary key on [GroupId] in table 'Groups'
ALTER TABLE [dbo].[Groups]
ADD CONSTRAINT [PK_Groups]
    PRIMARY KEY CLUSTERED ([GroupId] ASC);
GO

-- Creating primary key on [VehicleDeviceId] in table 'VehicleDevices'
ALTER TABLE [dbo].[VehicleDevices]
ADD CONSTRAINT [PK_VehicleDevices]
    PRIMARY KEY CLUSTERED ([VehicleDeviceId] ASC);
GO

-- Creating primary key on [VehicleGroupId] in table 'VehicleGroups'
ALTER TABLE [dbo].[VehicleGroups]
ADD CONSTRAINT [PK_VehicleGroups]
    PRIMARY KEY CLUSTERED ([VehicleGroupId] ASC);
GO

-- Creating primary key on [DeviceId] in table 'Devices'
ALTER TABLE [dbo].[Devices]
ADD CONSTRAINT [PK_Devices]
    PRIMARY KEY CLUSTERED ([DeviceId] ASC);
GO

-- Creating primary key on [PhoneNo], [DeviceId], [ClientId] in table 'vw_UnallocatedDevices'
ALTER TABLE [dbo].[vw_UnallocatedDevices]
ADD CONSTRAINT [PK_vw_UnallocatedDevices]
    PRIMARY KEY CLUSTERED ([PhoneNo], [DeviceId], [ClientId] ASC);
GO

-- Creating primary key on [ClientId] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([ClientId] ASC);
GO

-- Creating primary key on [ClientTypeId] in table 'ClientTypes'
ALTER TABLE [dbo].[ClientTypes]
ADD CONSTRAINT [PK_ClientTypes]
    PRIMARY KEY CLUSTERED ([ClientTypeId] ASC);
GO

-- Creating primary key on [UserId] in table 'aspnet_Membership'
ALTER TABLE [dbo].[aspnet_Membership]
ADD CONSTRAINT [PK_aspnet_Membership]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [DriverVehicleAssignmentId], [SerialNo] in table 'vw_DriverAssignmentStatus'
ALTER TABLE [dbo].[vw_DriverAssignmentStatus]
ADD CONSTRAINT [PK_vw_DriverAssignmentStatus]
    PRIMARY KEY CLUSTERED ([DriverVehicleAssignmentId], [SerialNo] ASC);
GO

-- Creating primary key on [PersonId], [LicenceTypeId] in table 'vw_DriverLicenceTypeRpt'
ALTER TABLE [dbo].[vw_DriverLicenceTypeRpt]
ADD CONSTRAINT [PK_vw_DriverLicenceTypeRpt]
    PRIMARY KEY CLUSTERED ([PersonId], [LicenceTypeId] ASC);
GO

-- Creating primary key on [DriverStatusName], [PersonId] in table 'vw_DriverStatusRpt'
ALTER TABLE [dbo].[vw_DriverStatusRpt]
ADD CONSTRAINT [PK_vw_DriverStatusRpt]
    PRIMARY KEY CLUSTERED ([DriverStatusName], [PersonId] ASC);
GO

-- Creating primary key on [VehicleId], [Date], [Difference], [NoOfLitres], [PersonId], [ClientId], [DateModified] in table 'vw_VehicleFuelMngtRpt'
ALTER TABLE [dbo].[vw_VehicleFuelMngtRpt]
ADD CONSTRAINT [PK_vw_VehicleFuelMngtRpt]
    PRIMARY KEY CLUSTERED ([VehicleId], [Date], [Difference], [NoOfLitres], [PersonId], [ClientId], [DateModified] ASC);
GO

-- Creating primary key on [VehicleId], [InsuaranceId], [InsuaranceTypeId], [StartDate], [EndDate], [PolicyNo], [ClientId], [InsuaranceName], [InsuaranceTypeName] in table 'vw_VehicleInsuranceRpt'
ALTER TABLE [dbo].[vw_VehicleInsuranceRpt]
ADD CONSTRAINT [PK_vw_VehicleInsuranceRpt]
    PRIMARY KEY CLUSTERED ([VehicleId], [InsuaranceId], [InsuaranceTypeId], [StartDate], [EndDate], [PolicyNo], [ClientId], [InsuaranceName], [InsuaranceTypeName] ASC);
GO

-- Creating primary key on [VehicleId], [LicenceTypeId], [RenewalDate], [ExpireDate], [LicenceNo], [ClientId], [VehicleLicenceTypeName] in table 'vw_VehicleLicenceDetailRpt'
ALTER TABLE [dbo].[vw_VehicleLicenceDetailRpt]
ADD CONSTRAINT [PK_vw_VehicleLicenceDetailRpt]
    PRIMARY KEY CLUSTERED ([VehicleId], [LicenceTypeId], [RenewalDate], [ExpireDate], [LicenceNo], [ClientId], [VehicleLicenceTypeName] ASC);
GO

-- Creating primary key on [ActionId] in table 'Actions'
ALTER TABLE [dbo].[Actions]
ADD CONSTRAINT [PK_Actions]
    PRIMARY KEY CLUSTERED ([ActionId] ASC);
GO

-- Creating primary key on [ControllerActionId] in table 'ControllerActions'
ALTER TABLE [dbo].[ControllerActions]
ADD CONSTRAINT [PK_ControllerActions]
    PRIMARY KEY CLUSTERED ([ControllerActionId] ASC);
GO

-- Creating primary key on [ControllerActionRoleId] in table 'ControllerActionRoles'
ALTER TABLE [dbo].[ControllerActionRoles]
ADD CONSTRAINT [PK_ControllerActionRoles]
    PRIMARY KEY CLUSTERED ([ControllerActionRoleId] ASC);
GO

-- Creating primary key on [ModuleId] in table 'Modules'
ALTER TABLE [dbo].[Modules]
ADD CONSTRAINT [PK_Modules]
    PRIMARY KEY CLUSTERED ([ModuleId] ASC);
GO

-- Creating primary key on [AlertLogId] in table 'AlertLogs'
ALTER TABLE [dbo].[AlertLogs]
ADD CONSTRAINT [PK_AlertLogs]
    PRIMARY KEY CLUSTERED ([AlertLogId] ASC);
GO

-- Creating primary key on [AlertReceiverAlertId] in table 'AlertReceiverAlerts'
ALTER TABLE [dbo].[AlertReceiverAlerts]
ADD CONSTRAINT [PK_AlertReceiverAlerts]
    PRIMARY KEY CLUSTERED ([AlertReceiverAlertId] ASC);
GO

-- Creating primary key on [AlertRecipientId] in table 'AlertRecipients'
ALTER TABLE [dbo].[AlertRecipients]
ADD CONSTRAINT [PK_AlertRecipients]
    PRIMARY KEY CLUSTERED ([AlertRecipientId] ASC);
GO

-- Creating primary key on [AlertTypeId] in table 'AlertTypes'
ALTER TABLE [dbo].[AlertTypes]
ADD CONSTRAINT [PK_AlertTypes]
    PRIMARY KEY CLUSTERED ([AlertTypeId] ASC);
GO

-- Creating primary key on [VehicleId], [RegistrationNo], [ClientId], [VehicleStatusId] in table 'vw_VehicleDetails'
ALTER TABLE [dbo].[vw_VehicleDetails]
ADD CONSTRAINT [PK_vw_VehicleDetails]
    PRIMARY KEY CLUSTERED ([VehicleId], [RegistrationNo], [ClientId], [VehicleStatusId] ASC);
GO

-- Creating primary key on [DeviceModelId] in table 'DeviceModels'
ALTER TABLE [dbo].[DeviceModels]
ADD CONSTRAINT [PK_DeviceModels]
    PRIMARY KEY CLUSTERED ([DeviceModelId] ASC);
GO

-- Creating primary key on [FenceVehicleId] in table 'GeoFenceVehicles'
ALTER TABLE [dbo].[GeoFenceVehicles]
ADD CONSTRAINT [PK_GeoFenceVehicles]
    PRIMARY KEY CLUSTERED ([FenceVehicleId] ASC);
GO

-- Creating primary key on [SentSMSId] in table 'SentSMS'
ALTER TABLE [dbo].[SentSMS]
ADD CONSTRAINT [PK_SentSMS]
    PRIMARY KEY CLUSTERED ([SentSMSId] ASC);
GO

-- Creating primary key on [AlertConfigTypeId] in table 'AlertConfigTypes'
ALTER TABLE [dbo].[AlertConfigTypes]
ADD CONSTRAINT [PK_AlertConfigTypes]
    PRIMARY KEY CLUSTERED ([AlertConfigTypeId] ASC);
GO

-- Creating primary key on [TrackAlertConfigId] in table 'TrackAlertConfigs'
ALTER TABLE [dbo].[TrackAlertConfigs]
ADD CONSTRAINT [PK_TrackAlertConfigs]
    PRIMARY KEY CLUSTERED ([TrackAlertConfigId] ASC);
GO

-- Creating primary key on [CompanySettingsId] in table 'CompanySettings'
ALTER TABLE [dbo].[CompanySettings]
ADD CONSTRAINT [PK_CompanySettings]
    PRIMARY KEY CLUSTERED ([CompanySettingsId] ASC);
GO

-- Creating primary key on [GeoFenceId] in table 'GeoFences'
ALTER TABLE [dbo].[GeoFences]
ADD CONSTRAINT [PK_GeoFences]
    PRIMARY KEY CLUSTERED ([GeoFenceId] ASC);
GO

-- Creating primary key on [DeviceTypeId] in table 'DeviceTypes'
ALTER TABLE [dbo].[DeviceTypes]
ADD CONSTRAINT [PK_DeviceTypes]
    PRIMARY KEY CLUSTERED ([DeviceTypeId] ASC);
GO

-- Creating primary key on [ClientId], [ClientTypeName] in table 'vw_ClientTypeDetails'
ALTER TABLE [dbo].[vw_ClientTypeDetails]
ADD CONSTRAINT [PK_vw_ClientTypeDetails]
    PRIMARY KEY CLUSTERED ([ClientId], [ClientTypeName] ASC);
GO

-- Creating primary key on [ModifiedBy], [GeoFenceName], [Active], [IsSpeedGeoFence], [Date], [GeoFenceId], [ClientId] in table 'vw_GeoFence'
ALTER TABLE [dbo].[vw_GeoFence]
ADD CONSTRAINT [PK_vw_GeoFence]
    PRIMARY KEY CLUSTERED ([ModifiedBy], [GeoFenceName], [Active], [IsSpeedGeoFence], [Date], [GeoFenceId], [ClientId] ASC);
GO

-- Creating primary key on [DispatchId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [PK_Dispatches]
    PRIMARY KEY CLUSTERED ([DispatchId] ASC);
GO

-- Creating primary key on [ClientDeviceId] in table 'ClientDevices'
ALTER TABLE [dbo].[ClientDevices]
ADD CONSTRAINT [PK_ClientDevices]
    PRIMARY KEY CLUSTERED ([ClientDeviceId] ASC);
GO

-- Creating primary key on [PersonId] in table 'People_Driver'
ALTER TABLE [dbo].[People_Driver]
ADD CONSTRAINT [PK_People_Driver]
    PRIMARY KEY CLUSTERED ([PersonId] ASC);
GO

-- Creating primary key on [PersonId] in table 'People_Customer'
ALTER TABLE [dbo].[People_Customer]
ADD CONSTRAINT [PK_People_Customer]
    PRIMARY KEY CLUSTERED ([PersonId] ASC);
GO

-- Creating primary key on [PersonId] in table 'People_SystemUser'
ALTER TABLE [dbo].[People_SystemUser]
ADD CONSTRAINT [PK_People_SystemUser]
    PRIMARY KEY CLUSTERED ([PersonId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [PersonId] in table 'DriverEndosments'
ALTER TABLE [dbo].[DriverEndosments]
ADD CONSTRAINT [FK_DriverEndosment_Person]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverEndosment_Person'
CREATE INDEX [IX_FK_DriverEndosment_Person]
ON [dbo].[DriverEndosments]
    ([PersonId]);
GO

-- Creating foreign key on [PersonId] in table 'DriverLicenceTypes'
ALTER TABLE [dbo].[DriverLicenceTypes]
ADD CONSTRAINT [FK_DriverLicenceType_Person]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverLicenceType_Person'
CREATE INDEX [IX_FK_DriverLicenceType_Person]
ON [dbo].[DriverLicenceTypes]
    ([PersonId]);
GO

-- Creating foreign key on [LicenceTypeId] in table 'DriverLicenceTypes'
ALTER TABLE [dbo].[DriverLicenceTypes]
ADD CONSTRAINT [FK_DriverLicenceType_LicenceType]
    FOREIGN KEY ([LicenceTypeId])
    REFERENCES [dbo].[LicenceTypes]
        ([LicenceTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverLicenceType_LicenceType'
CREATE INDEX [IX_FK_DriverLicenceType_LicenceType]
ON [dbo].[DriverLicenceTypes]
    ([LicenceTypeId]);
GO

-- Creating foreign key on [VehicleId] in table 'VehicleInsuarances'
ALTER TABLE [dbo].[VehicleInsuarances]
ADD CONSTRAINT [FK_Vehicle_Insuarance_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_Insuarance_Vehicle'
CREATE INDEX [IX_FK_Vehicle_Insuarance_Vehicle]
ON [dbo].[VehicleInsuarances]
    ([VehicleId]);
GO

-- Creating foreign key on [VehicleId] in table 'VehicleLicenceDetails'
ALTER TABLE [dbo].[VehicleLicenceDetails]
ADD CONSTRAINT [FK_Vehicle_LicenceDetail_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_LicenceDetail_Vehicle'
CREATE INDEX [IX_FK_Vehicle_LicenceDetail_Vehicle]
ON [dbo].[VehicleLicenceDetails]
    ([VehicleId]);
GO

-- Creating foreign key on [VehicleStatusId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_VehicleStatus]
    FOREIGN KEY ([VehicleStatusId])
    REFERENCES [dbo].[VehicleStatus]
        ([VehicleStatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_VehicleStatus'
CREATE INDEX [IX_FK_Vehicle_VehicleStatus]
ON [dbo].[Vehicles]
    ([VehicleStatusId]);
GO

-- Creating foreign key on [VehicleTypeId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_VehicleType]
    FOREIGN KEY ([VehicleTypeId])
    REFERENCES [dbo].[VehicleTypes]
        ([VehicleTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_VehicleType'
CREATE INDEX [IX_FK_Vehicle_VehicleType]
ON [dbo].[Vehicles]
    ([VehicleTypeId]);
GO

-- Creating foreign key on [LicenceTypeId] in table 'VehicleLicenceDetails'
ALTER TABLE [dbo].[VehicleLicenceDetails]
ADD CONSTRAINT [FK_Vehicle_LicenceDetail_VehicleLicenceType]
    FOREIGN KEY ([LicenceTypeId])
    REFERENCES [dbo].[VehicleLicenceTypes]
        ([VehicleLicenceTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_LicenceDetail_VehicleLicenceType'
CREATE INDEX [IX_FK_Vehicle_LicenceDetail_VehicleLicenceType]
ON [dbo].[VehicleLicenceDetails]
    ([LicenceTypeId]);
GO

-- Creating foreign key on [ExtrasId] in table 'DispatchExtras'
ALTER TABLE [dbo].[DispatchExtras]
ADD CONSTRAINT [FK_Dispatch_Extras_Extras]
    FOREIGN KEY ([ExtrasId])
    REFERENCES [dbo].[Extras]
        ([ExtrasId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Extras_Extras'
CREATE INDEX [IX_FK_Dispatch_Extras_Extras]
ON [dbo].[DispatchExtras]
    ([ExtrasId]);
GO

-- Creating foreign key on [AccessibilityId] in table 'DispatchAccessibilities'
ALTER TABLE [dbo].[DispatchAccessibilities]
ADD CONSTRAINT [FK_Dispatch_Accessibility_Accesibility]
    FOREIGN KEY ([AccessibilityId])
    REFERENCES [dbo].[Accesibilities]
        ([AccesibilityId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Accessibility_Accesibility'
CREATE INDEX [IX_FK_Dispatch_Accessibility_Accesibility]
ON [dbo].[DispatchAccessibilities]
    ([AccessibilityId]);
GO

-- Creating foreign key on [PersonId] in table 'DriverVehicleAssignments'
ALTER TABLE [dbo].[DriverVehicleAssignments]
ADD CONSTRAINT [FK_Driver_Vehicle_Assignment_Person]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Driver_Vehicle_Assignment_Person'
CREATE INDEX [IX_FK_Driver_Vehicle_Assignment_Person]
ON [dbo].[DriverVehicleAssignments]
    ([PersonId]);
GO

-- Creating foreign key on [VehicleId] in table 'DriverVehicleAssignments'
ALTER TABLE [dbo].[DriverVehicleAssignments]
ADD CONSTRAINT [FK_Driver_Vehicle_Assignment_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Driver_Vehicle_Assignment_Vehicle'
CREATE INDEX [IX_FK_Driver_Vehicle_Assignment_Vehicle]
ON [dbo].[DriverVehicleAssignments]
    ([VehicleId]);
GO

-- Creating foreign key on [FuelTypeId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_FuelType]
    FOREIGN KEY ([FuelTypeId])
    REFERENCES [dbo].[FuelTypes]
        ([FuelTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_FuelType'
CREATE INDEX [IX_FK_Vehicle_FuelType]
ON [dbo].[Vehicles]
    ([FuelTypeId]);
GO

-- Creating foreign key on [PersonId] in table 'VehicleFuelManagements'
ALTER TABLE [dbo].[VehicleFuelManagements]
ADD CONSTRAINT [FK_VehicleFuelManagement_Person]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleFuelManagement_Person'
CREATE INDEX [IX_FK_VehicleFuelManagement_Person]
ON [dbo].[VehicleFuelManagements]
    ([PersonId]);
GO

-- Creating foreign key on [VehicleId] in table 'VehicleFuelManagements'
ALTER TABLE [dbo].[VehicleFuelManagements]
ADD CONSTRAINT [FK_Vehicle_Fuel_Management_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_Fuel_Management_Vehicle'
CREATE INDEX [IX_FK_Vehicle_Fuel_Management_Vehicle]
ON [dbo].[VehicleFuelManagements]
    ([VehicleId]);
GO

-- Creating foreign key on [InsuaranceId] in table 'VehicleInsuarances'
ALTER TABLE [dbo].[VehicleInsuarances]
ADD CONSTRAINT [FK_Vehicle_Insuarance_Insuarance]
    FOREIGN KEY ([InsuaranceId])
    REFERENCES [dbo].[Insuarances]
        ([InsuaranceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_Insuarance_Insuarance'
CREATE INDEX [IX_FK_Vehicle_Insuarance_Insuarance]
ON [dbo].[VehicleInsuarances]
    ([InsuaranceId]);
GO

-- Creating foreign key on [InsuaranceTypeId] in table 'VehicleInsuarances'
ALTER TABLE [dbo].[VehicleInsuarances]
ADD CONSTRAINT [FK_Vehicle_Insuarance_InsuaranceType]
    FOREIGN KEY ([InsuaranceTypeId])
    REFERENCES [dbo].[InsuaranceTypes]
        ([InsuaranceTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_Insuarance_InsuaranceType'
CREATE INDEX [IX_FK_Vehicle_Insuarance_InsuaranceType]
ON [dbo].[VehicleInsuarances]
    ([InsuaranceTypeId]);
GO

-- Creating foreign key on [GroupId] in table 'VehicleGroups'
ALTER TABLE [dbo].[VehicleGroups]
ADD CONSTRAINT [FK_VehicleGroup_Group]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[Groups]
        ([GroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleGroup_Group'
CREATE INDEX [IX_FK_VehicleGroup_Group]
ON [dbo].[VehicleGroups]
    ([GroupId]);
GO

-- Creating foreign key on [VehicleId] in table 'VehicleDevices'
ALTER TABLE [dbo].[VehicleDevices]
ADD CONSTRAINT [FK_VehicleDevice_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleDevice_Vehicle'
CREATE INDEX [IX_FK_VehicleDevice_Vehicle]
ON [dbo].[VehicleDevices]
    ([VehicleId]);
GO

-- Creating foreign key on [VehicleId] in table 'VehicleGroups'
ALTER TABLE [dbo].[VehicleGroups]
ADD CONSTRAINT [FK_VehicleGroup_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleGroup_Vehicle'
CREATE INDEX [IX_FK_VehicleGroup_Vehicle]
ON [dbo].[VehicleGroups]
    ([VehicleId]);
GO

-- Creating foreign key on [DeviceId] in table 'VehicleDevices'
ALTER TABLE [dbo].[VehicleDevices]
ADD CONSTRAINT [FK_VehicleDevice_Device]
    FOREIGN KEY ([DeviceId])
    REFERENCES [dbo].[Devices]
        ([DeviceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleDevice_Device'
CREATE INDEX [IX_FK_VehicleDevice_Device]
ON [dbo].[VehicleDevices]
    ([DeviceId]);
GO

-- Creating foreign key on [VehicleId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ClientId] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [FK__Person__ClientId__320C68B7]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Person__ClientId__320C68B7'
CREATE INDEX [IX_FK__Person__ClientId__320C68B7]
ON [dbo].[People]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK__Vehicle__ClientI__36D11DD4]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Vehicle__ClientI__36D11DD4'
CREATE INDEX [IX_FK__Vehicle__ClientI__36D11DD4]
ON [dbo].[Vehicles]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'VehicleFuelManagements'
ALTER TABLE [dbo].[VehicleFuelManagements]
ADD CONSTRAINT [FK__Vehicle_F__Clien__37C5420D]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Vehicle_F__Clien__37C5420D'
CREATE INDEX [IX_FK__Vehicle_F__Clien__37C5420D]
ON [dbo].[VehicleFuelManagements]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'VehicleInsuarances'
ALTER TABLE [dbo].[VehicleInsuarances]
ADD CONSTRAINT [FK__Vehicle_I__Clien__38B96646]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Vehicle_I__Clien__38B96646'
CREATE INDEX [IX_FK__Vehicle_I__Clien__38B96646]
ON [dbo].[VehicleInsuarances]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'VehicleLicenceDetails'
ALTER TABLE [dbo].[VehicleLicenceDetails]
ADD CONSTRAINT [FK__Vehicle_L__Clien__39AD8A7F]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Vehicle_L__Clien__39AD8A7F'
CREATE INDEX [IX_FK__Vehicle_L__Clien__39AD8A7F]
ON [dbo].[VehicleLicenceDetails]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'VehicleServiceDetails'
ALTER TABLE [dbo].[VehicleServiceDetails]
ADD CONSTRAINT [FK__VehicleSe__Clien__3D7E1B63]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__VehicleSe__Clien__3D7E1B63'
CREATE INDEX [IX_FK__VehicleSe__Clien__3D7E1B63]
ON [dbo].[VehicleServiceDetails]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'VehicleStatus'
ALTER TABLE [dbo].[VehicleStatus]
ADD CONSTRAINT [FK__VehicleSt__Clien__3E723F9C]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__VehicleSt__Clien__3E723F9C'
CREATE INDEX [IX_FK__VehicleSt__Clien__3E723F9C]
ON [dbo].[VehicleStatus]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'CustomerTypes'
ALTER TABLE [dbo].[CustomerTypes]
ADD CONSTRAINT [FK_CustomerType_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerType_Client'
CREATE INDEX [IX_FK_CustomerType_Client]
ON [dbo].[CustomerTypes]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'Devices'
ALTER TABLE [dbo].[Devices]
ADD CONSTRAINT [FK_Device_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Device_Client'
CREATE INDEX [IX_FK_Device_Client]
ON [dbo].[Devices]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'DispatchStatus'
ALTER TABLE [dbo].[DispatchStatus]
ADD CONSTRAINT [FK_DispatchStatus_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DispatchStatus_Client'
CREATE INDEX [IX_FK_DispatchStatus_Client]
ON [dbo].[DispatchStatus]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'DriverVehicleAssignments'
ALTER TABLE [dbo].[DriverVehicleAssignments]
ADD CONSTRAINT [FK_Driver_Vehicle_Assignment_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Driver_Vehicle_Assignment_Client'
CREATE INDEX [IX_FK_Driver_Vehicle_Assignment_Client]
ON [dbo].[DriverVehicleAssignments]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'DriverEndosments'
ALTER TABLE [dbo].[DriverEndosments]
ADD CONSTRAINT [FK_DriverEndosment_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverEndosment_Client'
CREATE INDEX [IX_FK_DriverEndosment_Client]
ON [dbo].[DriverEndosments]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'DriverLicenceTypes'
ALTER TABLE [dbo].[DriverLicenceTypes]
ADD CONSTRAINT [FK_DriverLicenceType_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverLicenceType_Client'
CREATE INDEX [IX_FK_DriverLicenceType_Client]
ON [dbo].[DriverLicenceTypes]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'DriverStatus'
ALTER TABLE [dbo].[DriverStatus]
ADD CONSTRAINT [FK_DriverStatus_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverStatus_Client'
CREATE INDEX [IX_FK_DriverStatus_Client]
ON [dbo].[DriverStatus]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'Extras'
ALTER TABLE [dbo].[Extras]
ADD CONSTRAINT [FK_Extras_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Extras_Client'
CREATE INDEX [IX_FK_Extras_Client]
ON [dbo].[Extras]
    ([ClientId]);
GO

-- Creating foreign key on [ClientTypeId] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [FK_Client_ClientType]
    FOREIGN KEY ([ClientTypeId])
    REFERENCES [dbo].[ClientTypes]
        ([ClientTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Client_ClientType'
CREATE INDEX [IX_FK_Client_ClientType]
ON [dbo].[Clients]
    ([ClientTypeId]);
GO

-- Creating foreign key on [ModifiedBy] in table 'Accesibilities'
ALTER TABLE [dbo].[Accesibilities]
ADD CONSTRAINT [FK_Accesibility_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Accesibility_aspnet_Membership'
CREATE INDEX [IX_FK_Accesibility_aspnet_Membership]
ON [dbo].[Accesibilities]
    ([ModifiedBy]);
GO

-- Creating foreign key on [UserId] in table 'aspnet_Membership'
ALTER TABLE [dbo].[aspnet_Membership]
ADD CONSTRAINT [FK__aspnet_Me__UserI__286302EC]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[aspnet_Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ModifiedBy] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [FK_Client_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Client_aspnet_Membership'
CREATE INDEX [IX_FK_Client_aspnet_Membership]
ON [dbo].[Clients]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'ClientTypes'
ALTER TABLE [dbo].[ClientTypes]
ADD CONSTRAINT [FK_ClientType_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientType_aspnet_Membership'
CREATE INDEX [IX_FK_ClientType_aspnet_Membership]
ON [dbo].[ClientTypes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'CustomerTypes'
ALTER TABLE [dbo].[CustomerTypes]
ADD CONSTRAINT [FK_CustomerType_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerType_aspnet_Membership'
CREATE INDEX [IX_FK_CustomerType_aspnet_Membership]
ON [dbo].[CustomerTypes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'DispatchAccessibilities'
ALTER TABLE [dbo].[DispatchAccessibilities]
ADD CONSTRAINT [FK_Dispatch_Accessibility_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Accessibility_aspnet_Membership'
CREATE INDEX [IX_FK_Dispatch_Accessibility_aspnet_Membership]
ON [dbo].[DispatchAccessibilities]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'DispatchExtras'
ALTER TABLE [dbo].[DispatchExtras]
ADD CONSTRAINT [FK_Dispatch_Extras_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Extras_aspnet_Membership'
CREATE INDEX [IX_FK_Dispatch_Extras_aspnet_Membership]
ON [dbo].[DispatchExtras]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'DriverLicenceTypes'
ALTER TABLE [dbo].[DriverLicenceTypes]
ADD CONSTRAINT [FK_Driver_DriverType_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Driver_DriverType_aspnet_Membership'
CREATE INDEX [IX_FK_Driver_DriverType_aspnet_Membership]
ON [dbo].[DriverLicenceTypes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'DriverVehicleAssignments'
ALTER TABLE [dbo].[DriverVehicleAssignments]
ADD CONSTRAINT [FK_Driver_Vehicle_Assignment_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Driver_Vehicle_Assignment_aspnet_Membership'
CREATE INDEX [IX_FK_Driver_Vehicle_Assignment_aspnet_Membership]
ON [dbo].[DriverVehicleAssignments]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'DriverEndosments'
ALTER TABLE [dbo].[DriverEndosments]
ADD CONSTRAINT [FK_DriverEndosment_aspnet_Membership1]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverEndosment_aspnet_Membership1'
CREATE INDEX [IX_FK_DriverEndosment_aspnet_Membership1]
ON [dbo].[DriverEndosments]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'DriverStatus'
ALTER TABLE [dbo].[DriverStatus]
ADD CONSTRAINT [FK_DriverStatus_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverStatus_aspnet_Membership'
CREATE INDEX [IX_FK_DriverStatus_aspnet_Membership]
ON [dbo].[DriverStatus]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'LicenceTypes'
ALTER TABLE [dbo].[LicenceTypes]
ADD CONSTRAINT [FK_DriverType_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverType_aspnet_Membership'
CREATE INDEX [IX_FK_DriverType_aspnet_Membership]
ON [dbo].[LicenceTypes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'Extras'
ALTER TABLE [dbo].[Extras]
ADD CONSTRAINT [FK_Extras_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Extras_aspnet_Membership'
CREATE INDEX [IX_FK_Extras_aspnet_Membership]
ON [dbo].[Extras]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'FuelTypes'
ALTER TABLE [dbo].[FuelTypes]
ADD CONSTRAINT [FK_FuelType_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FuelType_aspnet_Membership'
CREATE INDEX [IX_FK_FuelType_aspnet_Membership]
ON [dbo].[FuelTypes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'InsuaranceTypes'
ALTER TABLE [dbo].[InsuaranceTypes]
ADD CONSTRAINT [FK_InsuaranceType_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_InsuaranceType_aspnet_Membership'
CREATE INDEX [IX_FK_InsuaranceType_aspnet_Membership]
ON [dbo].[InsuaranceTypes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'PaymentModes'
ALTER TABLE [dbo].[PaymentModes]
ADD CONSTRAINT [FK_PaymentMode_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PaymentMode_aspnet_Membership'
CREATE INDEX [IX_FK_PaymentMode_aspnet_Membership]
ON [dbo].[PaymentModes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [UserId] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [FK_Person_aspnet_Membership]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person_aspnet_Membership'
CREATE INDEX [IX_FK_Person_aspnet_Membership]
ON [dbo].[People]
    ([UserId]);
GO

-- Creating foreign key on [ModifiedBy] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_aspnet_Membership'
CREATE INDEX [IX_FK_Vehicle_aspnet_Membership]
ON [dbo].[Vehicles]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'VehicleFuelManagements'
ALTER TABLE [dbo].[VehicleFuelManagements]
ADD CONSTRAINT [FK_Vehicle_Fuel_Management_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_Fuel_Management_aspnet_Membership'
CREATE INDEX [IX_FK_Vehicle_Fuel_Management_aspnet_Membership]
ON [dbo].[VehicleFuelManagements]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'VehicleInsuarances'
ALTER TABLE [dbo].[VehicleInsuarances]
ADD CONSTRAINT [FK_Vehicle_Insuarance_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_Insuarance_aspnet_Membership'
CREATE INDEX [IX_FK_Vehicle_Insuarance_aspnet_Membership]
ON [dbo].[VehicleInsuarances]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'VehicleLicenceDetails'
ALTER TABLE [dbo].[VehicleLicenceDetails]
ADD CONSTRAINT [FK_Vehicle_LicenceDetail_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_LicenceDetail_aspnet_Membership'
CREATE INDEX [IX_FK_Vehicle_LicenceDetail_aspnet_Membership]
ON [dbo].[VehicleLicenceDetails]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'VehicleServiceDetails'
ALTER TABLE [dbo].[VehicleServiceDetails]
ADD CONSTRAINT [FK_VehicleServiceDetail_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleServiceDetail_aspnet_Membership'
CREATE INDEX [IX_FK_VehicleServiceDetail_aspnet_Membership]
ON [dbo].[VehicleServiceDetails]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ModifiedBy] in table 'VehicleStatus'
ALTER TABLE [dbo].[VehicleStatus]
ADD CONSTRAINT [FK_VehicleStatus_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleStatus_aspnet_Membership'
CREATE INDEX [IX_FK_VehicleStatus_aspnet_Membership]
ON [dbo].[VehicleStatus]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ActionId] in table 'ControllerActions'
ALTER TABLE [dbo].[ControllerActions]
ADD CONSTRAINT [FK_ControllerAction_Action]
    FOREIGN KEY ([ActionId])
    REFERENCES [dbo].[Actions]
        ([ActionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ControllerAction_Action'
CREATE INDEX [IX_FK_ControllerAction_Action]
ON [dbo].[ControllerActions]
    ([ActionId]);
GO

-- Creating foreign key on [RoleId] in table 'ControllerActionRoles'
ALTER TABLE [dbo].[ControllerActionRoles]
ADD CONSTRAINT [FK_ControllerActionRole_aspnet_Roles]
    FOREIGN KEY ([RoleId])
    REFERENCES [dbo].[aspnet_Roles]
        ([RoleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ControllerActionRole_aspnet_Roles'
CREATE INDEX [IX_FK_ControllerActionRole_aspnet_Roles]
ON [dbo].[ControllerActionRoles]
    ([RoleId]);
GO

-- Creating foreign key on [ModuleId] in table 'GFleetV3Controllers'
ALTER TABLE [dbo].[GFleetV3Controllers]
ADD CONSTRAINT [FK_Controller_Module]
    FOREIGN KEY ([ModuleId])
    REFERENCES [dbo].[Modules]
        ([ModuleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Controller_Module'
CREATE INDEX [IX_FK_Controller_Module]
ON [dbo].[GFleetV3Controllers]
    ([ModuleId]);
GO

-- Creating foreign key on [ControllerId] in table 'ControllerActions'
ALTER TABLE [dbo].[ControllerActions]
ADD CONSTRAINT [FK_ControllerAction_Controller]
    FOREIGN KEY ([ControllerId])
    REFERENCES [dbo].[GFleetV3Controllers]
        ([ControllerId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ControllerAction_Controller'
CREATE INDEX [IX_FK_ControllerAction_Controller]
ON [dbo].[ControllerActions]
    ([ControllerId]);
GO

-- Creating foreign key on [ControllerActionId] in table 'ControllerActionRoles'
ALTER TABLE [dbo].[ControllerActionRoles]
ADD CONSTRAINT [FK_ControllerActionRole_ControllerAction]
    FOREIGN KEY ([ControllerActionId])
    REFERENCES [dbo].[ControllerActions]
        ([ControllerActionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ControllerActionRole_ControllerAction'
CREATE INDEX [IX_FK_ControllerActionRole_ControllerAction]
ON [dbo].[ControllerActionRoles]
    ([ControllerActionId]);
GO

-- Creating foreign key on [ParentId] in table 'GFleetV3Controllers'
ALTER TABLE [dbo].[GFleetV3Controllers]
ADD CONSTRAINT [FK_Controller_Controller]
    FOREIGN KEY ([ParentId])
    REFERENCES [dbo].[GFleetV3Controllers]
        ([ControllerId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Controller_Controller'
CREATE INDEX [IX_FK_Controller_Controller]
ON [dbo].[GFleetV3Controllers]
    ([ParentId]);
GO

-- Creating foreign key on [AlertReceiverAlertId] in table 'AlertLogs'
ALTER TABLE [dbo].[AlertLogs]
ADD CONSTRAINT [FK_AlertLog_AlertReceiverAlert]
    FOREIGN KEY ([AlertReceiverAlertId])
    REFERENCES [dbo].[AlertReceiverAlerts]
        ([AlertReceiverAlertId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertLog_AlertReceiverAlert'
CREATE INDEX [IX_FK_AlertLog_AlertReceiverAlert]
ON [dbo].[AlertLogs]
    ([AlertReceiverAlertId]);
GO

-- Creating foreign key on [AlertReceverId] in table 'AlertReceiverAlerts'
ALTER TABLE [dbo].[AlertReceiverAlerts]
ADD CONSTRAINT [FK_AlertReceiverAlert_AlertRecipient]
    FOREIGN KEY ([AlertReceverId])
    REFERENCES [dbo].[AlertRecipients]
        ([AlertRecipientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertReceiverAlert_AlertRecipient'
CREATE INDEX [IX_FK_AlertReceiverAlert_AlertRecipient]
ON [dbo].[AlertReceiverAlerts]
    ([AlertReceverId]);
GO

-- Creating foreign key on [AlertTypeId] in table 'AlertReceiverAlerts'
ALTER TABLE [dbo].[AlertReceiverAlerts]
ADD CONSTRAINT [FK_AlertReceiverAlert_AlertType]
    FOREIGN KEY ([AlertTypeId])
    REFERENCES [dbo].[AlertTypes]
        ([AlertTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertReceiverAlert_AlertType'
CREATE INDEX [IX_FK_AlertReceiverAlert_AlertType]
ON [dbo].[AlertReceiverAlerts]
    ([AlertTypeId]);
GO

-- Creating foreign key on [ModifiedBy] in table 'AlertReceiverAlerts'
ALTER TABLE [dbo].[AlertReceiverAlerts]
ADD CONSTRAINT [FK_AlertReceiverAlert_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertReceiverAlert_aspnet_Membership'
CREATE INDEX [IX_FK_AlertReceiverAlert_aspnet_Membership]
ON [dbo].[AlertReceiverAlerts]
    ([ModifiedBy]);
GO

-- Creating foreign key on [AlertRecipientId] in table 'AlertRecipients'
ALTER TABLE [dbo].[AlertRecipients]
ADD CONSTRAINT [FK_AlertRecipient_AlertRecipient]
    FOREIGN KEY ([AlertRecipientId])
    REFERENCES [dbo].[AlertRecipients]
        ([AlertRecipientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ModifiedBy] in table 'AlertRecipients'
ALTER TABLE [dbo].[AlertRecipients]
ADD CONSTRAINT [FK_AlertRecipient_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertRecipient_aspnet_Membership'
CREATE INDEX [IX_FK_AlertRecipient_aspnet_Membership]
ON [dbo].[AlertRecipients]
    ([ModifiedBy]);
GO

-- Creating foreign key on [ClientId] in table 'AlertRecipients'
ALTER TABLE [dbo].[AlertRecipients]
ADD CONSTRAINT [FK_AlertRecipient_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertRecipient_Client'
CREATE INDEX [IX_FK_AlertRecipient_Client]
ON [dbo].[AlertRecipients]
    ([ClientId]);
GO

-- Creating foreign key on [ModifiedBy] in table 'AlertTypes'
ALTER TABLE [dbo].[AlertTypes]
ADD CONSTRAINT [FK_AlertType_aspnet_Membership]
    FOREIGN KEY ([ModifiedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AlertType_aspnet_Membership'
CREATE INDEX [IX_FK_AlertType_aspnet_Membership]
ON [dbo].[AlertTypes]
    ([ModifiedBy]);
GO

-- Creating foreign key on [DeviceModelId] in table 'Devices'
ALTER TABLE [dbo].[Devices]
ADD CONSTRAINT [FK_Device_DeviceModel]
    FOREIGN KEY ([DeviceModelId])
    REFERENCES [dbo].[DeviceModels]
        ([DeviceModelId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Device_DeviceModel'
CREATE INDEX [IX_FK_Device_DeviceModel]
ON [dbo].[Devices]
    ([DeviceModelId]);
GO

-- Creating foreign key on [DeviceModelId] in table 'DeviceModels'
ALTER TABLE [dbo].[DeviceModels]
ADD CONSTRAINT [FK_DeviceModel_DeviceModel]
    FOREIGN KEY ([DeviceModelId])
    REFERENCES [dbo].[DeviceModels]
        ([DeviceModelId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [GroupId] in table 'GeoFenceVehicles'
ALTER TABLE [dbo].[GeoFenceVehicles]
ADD CONSTRAINT [FK_GeoFenceVehicle_Group]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[Groups]
        ([GroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GeoFenceVehicle_Group'
CREATE INDEX [IX_FK_GeoFenceVehicle_Group]
ON [dbo].[GeoFenceVehicles]
    ([GroupId]);
GO

-- Creating foreign key on [Sender] in table 'SentSMS'
ALTER TABLE [dbo].[SentSMS]
ADD CONSTRAINT [FK_SentSMS_aspnet_Users]
    FOREIGN KEY ([Sender])
    REFERENCES [dbo].[aspnet_Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SentSMS_aspnet_Users'
CREATE INDEX [IX_FK_SentSMS_aspnet_Users]
ON [dbo].[SentSMS]
    ([Sender]);
GO

-- Creating foreign key on [AlertConfigTypeId] in table 'TrackAlertConfigs'
ALTER TABLE [dbo].[TrackAlertConfigs]
ADD CONSTRAINT [FK_TrackAlertConfig_AlertConfig]
    FOREIGN KEY ([AlertConfigTypeId])
    REFERENCES [dbo].[AlertConfigTypes]
        ([AlertConfigTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TrackAlertConfig_AlertConfig'
CREATE INDEX [IX_FK_TrackAlertConfig_AlertConfig]
ON [dbo].[TrackAlertConfigs]
    ([AlertConfigTypeId]);
GO

-- Creating foreign key on [ClientId] in table 'CompanySettings'
ALTER TABLE [dbo].[CompanySettings]
ADD CONSTRAINT [FK_CompanySettings_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CompanySettings_Client'
CREATE INDEX [IX_FK_CompanySettings_Client]
ON [dbo].[CompanySettings]
    ([ClientId]);
GO

-- Creating foreign key on [ClientId] in table 'GeoFences'
ALTER TABLE [dbo].[GeoFences]
ADD CONSTRAINT [FK_GeoFence_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GeoFence_Client'
CREATE INDEX [IX_FK_GeoFence_Client]
ON [dbo].[GeoFences]
    ([ClientId]);
GO

-- Creating foreign key on [GeoFenceId] in table 'GeoFenceVehicles'
ALTER TABLE [dbo].[GeoFenceVehicles]
ADD CONSTRAINT [FK_GeoFenceVehicle_GeoFence]
    FOREIGN KEY ([GeoFenceId])
    REFERENCES [dbo].[GeoFences]
        ([GeoFenceId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GeoFenceVehicle_GeoFence'
CREATE INDEX [IX_FK_GeoFenceVehicle_GeoFence]
ON [dbo].[GeoFenceVehicles]
    ([GeoFenceId]);
GO

-- Creating foreign key on [DeviceTypeId] in table 'Devices'
ALTER TABLE [dbo].[Devices]
ADD CONSTRAINT [FK_Device_DeviceType]
    FOREIGN KEY ([DeviceTypeId])
    REFERENCES [dbo].[DeviceTypes]
        ([DeviceTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Device_DeviceType'
CREATE INDEX [IX_FK_Device_DeviceType]
ON [dbo].[Devices]
    ([DeviceTypeId]);
GO

-- Creating foreign key on [DriverStatusId] in table 'People_Driver'
ALTER TABLE [dbo].[People_Driver]
ADD CONSTRAINT [FK_DriverStatuDriver]
    FOREIGN KEY ([DriverStatusId])
    REFERENCES [dbo].[DriverStatus]
        ([DriverStatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverStatuDriver'
CREATE INDEX [IX_FK_DriverStatuDriver]
ON [dbo].[People_Driver]
    ([DriverStatusId]);
GO

-- Creating foreign key on [DispatchedBy] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_aspnet_Membership]
    FOREIGN KEY ([DispatchedBy])
    REFERENCES [dbo].[aspnet_Membership]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_aspnet_Membership'
CREATE INDEX [IX_FK_Dispatch_aspnet_Membership]
ON [dbo].[Dispatches]
    ([DispatchedBy]);
GO

-- Creating foreign key on [BookedBy] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_aspnet_Users]
    FOREIGN KEY ([BookedBy])
    REFERENCES [dbo].[aspnet_Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_aspnet_Users'
CREATE INDEX [IX_FK_Dispatch_aspnet_Users]
ON [dbo].[Dispatches]
    ([BookedBy]);
GO

-- Creating foreign key on [DispatchedBy] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_aspnet_Users1]
    FOREIGN KEY ([DispatchedBy])
    REFERENCES [dbo].[aspnet_Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_aspnet_Users1'
CREATE INDEX [IX_FK_Dispatch_aspnet_Users1]
ON [dbo].[Dispatches]
    ([DispatchedBy]);
GO

-- Creating foreign key on [ClientId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_Client]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Client'
CREATE INDEX [IX_FK_Dispatch_Client]
ON [dbo].[Dispatches]
    ([ClientId]);
GO

-- Creating foreign key on [ReturnId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_Dispatch]
    FOREIGN KEY ([ReturnId])
    REFERENCES [dbo].[Dispatches]
        ([DispatchId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Dispatch'
CREATE INDEX [IX_FK_Dispatch_Dispatch]
ON [dbo].[Dispatches]
    ([ReturnId]);
GO

-- Creating foreign key on [DispatchedStateId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_DispatchStatus]
    FOREIGN KEY ([DispatchedStateId])
    REFERENCES [dbo].[DispatchStatus]
        ([DispatchStatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_DispatchStatus'
CREATE INDEX [IX_FK_Dispatch_DispatchStatus]
ON [dbo].[Dispatches]
    ([DispatchedStateId]);
GO

-- Creating foreign key on [DriverVehicleAssignmentId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_DriverVehicleAssignment]
    FOREIGN KEY ([DriverVehicleAssignmentId])
    REFERENCES [dbo].[DriverVehicleAssignments]
        ([DriverVehicleAssignmentId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_DriverVehicleAssignment'
CREATE INDEX [IX_FK_Dispatch_DriverVehicleAssignment]
ON [dbo].[Dispatches]
    ([DriverVehicleAssignmentId]);
GO

-- Creating foreign key on [PaymentModeId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_PaymentMode]
    FOREIGN KEY ([PaymentModeId])
    REFERENCES [dbo].[PaymentModes]
        ([PaymentModeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_PaymentMode'
CREATE INDEX [IX_FK_Dispatch_PaymentMode]
ON [dbo].[Dispatches]
    ([PaymentModeId]);
GO

-- Creating foreign key on [CustomerId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_Person_Customer]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Person_Customer'
CREATE INDEX [IX_FK_Dispatch_Person_Customer]
ON [dbo].[Dispatches]
    ([CustomerId]);
GO

-- Creating foreign key on [VehicleId] in table 'Dispatches'
ALTER TABLE [dbo].[Dispatches]
ADD CONSTRAINT [FK_Dispatch_Vehicle]
    FOREIGN KEY ([VehicleId])
    REFERENCES [dbo].[Vehicles]
        ([VehicleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch_Vehicle'
CREATE INDEX [IX_FK_Dispatch_Vehicle]
ON [dbo].[Dispatches]
    ([VehicleId]);
GO

-- Creating foreign key on [DispatchId] in table 'DispatchAccessibilities'
ALTER TABLE [dbo].[DispatchAccessibilities]
ADD CONSTRAINT [FK_DispatchAccessibility_Dispatch]
    FOREIGN KEY ([DispatchId])
    REFERENCES [dbo].[Dispatches]
        ([DispatchId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DispatchAccessibility_Dispatch'
CREATE INDEX [IX_FK_DispatchAccessibility_Dispatch]
ON [dbo].[DispatchAccessibilities]
    ([DispatchId]);
GO

-- Creating foreign key on [DispatchId] in table 'DispatchExtras'
ALTER TABLE [dbo].[DispatchExtras]
ADD CONSTRAINT [FK_DispatchExtras_Dispatch]
    FOREIGN KEY ([DispatchId])
    REFERENCES [dbo].[Dispatches]
        ([DispatchId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DispatchExtras_Dispatch'
CREATE INDEX [IX_FK_DispatchExtras_Dispatch]
ON [dbo].[DispatchExtras]
    ([DispatchId]);
GO

-- Creating foreign key on [PersonId] in table 'People_Driver'
ALTER TABLE [dbo].[People_Driver]
ADD CONSTRAINT [FK_Driver_inherits_Person]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PersonId] in table 'People_Customer'
ALTER TABLE [dbo].[People_Customer]
ADD CONSTRAINT [FK_Customer_inherits_Person]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PersonId] in table 'People_SystemUser'
ALTER TABLE [dbo].[People_SystemUser]
ADD CONSTRAINT [FK_SystemUser_inherits_Person]
    FOREIGN KEY ([PersonId])
    REFERENCES [dbo].[People]
        ([PersonId])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO