USE [CarTrack]
GO
/****** Object:  Trigger [dbo].[LatestLogInsert]    Script Date: 04/23/2014 17:32:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[LatestLogInsert] 
   ON  [dbo].[GPSLatestLog]
   INSTEAD OF INSERT,UPDATE
AS 
BEGIN
	DECLARE @Cinfo VARBINARY(128)  
	SELECT @Cinfo = Context_Info()  
	IF @Cinfo = 0x55555
	begin	
		print 'return'
		--return;
	end
	
	print 'execute'
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
    
    /*
		check whether unitid exists on this table
		if exists, update row and insert new in gpsloghistroy
		else insert new in both this table and gpsloghistroy
		
		check also for time whether difference greater than 30 minutes from now
		if greater, insert in gpsloghistroy only
		else insert new in both this table and gpsloghistroy
    */
    
    declare @isCorrectTime bit=0
    declare @IsUnitIdExist bit=0
    declare @isUpdate bit=0
    declare @currentDate datetime=getdate()
    declare @correctTimeDiff int=30
    
    select @isCorrectTime= case when datediff(mi,time,@currentDate)>@correctTimeDiff then 0 else 1 end 
		from inserted
    
    select @IsUnitIdExist= case when g.unitId is null then 0 else 1 end 
		from dbo.GPSLatestLog g, inserted i where g.unitid=i.unitid and g.vehicleid=i.vehicleid
		
	select @isUpdate=case when g.unitId is null then 0 else 1 end 
		from deleted g, inserted i where g.unitid=i.unitid and g.vehicleid=i.vehicleid
		
	select * 
		from deleted g, inserted i where g.unitid=i.unitid and g.vehicleid=i.vehicleid
	
	print @isCorrectTime
	print @IsUnitIdExist
	print @isUpdate
		
	if(@isCorrectTime=1 and @IsUnitIdExist=1 and @isUpdate=1)
	begin
		/* update record in GPSLatestLog and insert new record in dbo.GPSHistoryLog */
		
		UPDATE [CarTrack].[dbo].[GPSLatestLog] SET [UnitID]=i.UnitID,[SERIAL_NUM]=i.SERIAL_NUM,
			[Longitude]=i.Longitude,[Latitude]=i.Latitude,[GLB_TIME]=i.GLB_TIME,[Speed]=i.Speed,
			[Date]=@currentDate,[Time]=i.Time,[Distance]=i.Distance,[Course]=i.Course,
			[Direction]=i.Direction,[DeviceTypeId]=i.DeviceTypeId,[VehicleId]=i.VehicleId,
			[DeviceId]=i.DeviceId,[DeviceEventId]=i.DeviceEventId,
			[HasLocationData]=i.HasLocationData,[GPSStatus]=i.GPSStatus,[Alarm]=i.Alarm,
			[SequenceNo]=i.SequenceNo,[EngineState]=i.EngineState,[HDOP]=i.HDOP,[A2D1]=i.A2D1,
			[A2D2]=i.A2D2,[A2D3]=i.A2D3,[IO_INPUT1]=i.IO_INPUT1,[IO_INPUT2]=i.IO_INPUT2,
			[IO_INPUT3]=i.IO_INPUT3,[IO_INPUT4]=i.IO_INPUT4,[IO_INPUT5]=i.IO_INPUT5,
			[IO_INPUT6]=i.IO_INPUT6,[IO_INPUT7]=i.IO_INPUT7,[IO_INPUT8]=i.IO_INPUT8,
			[IO_OUTPUT1]=i.IO_OUTPUT1,[IO_OUTPUT2]=i.IO_OUTPUT2,[IO_OUTPUT3]=i.IO_OUTPUT3,
			[IO_OUTPUT4]=i.IO_OUTPUT4,[IO_OUTPUT5]=i.IO_OUTPUT5,[IO_OUTPUT6]=i.IO_OUTPUT6,
			[IO_OUTPUT7]=i.IO_OUTPUT7,[IO_OUTPUT8]=i.IO_OUTPUT8,[Altitude]=i.Altitude,
			[SLNAME]=i.SLNAME,[LOCNAME]=i.LOCNAME,[DIVNAME]=i.DIVNAME,[DISTNAME]=i.DISTNAME,
			[PROVNAME]=i.PROVNAME,[CONSTITUEN]=i.CONSTITUEN,[TOWN]=i.TOWN,[STREET]=i.STREET,
			[ImageUrl]=i.ImageUrl,[SatellitesUsed]=i.SatellitesUsed,[CELL_MCC]=i.CELL_MCC,
			[CELL_MNC]=i.CELL_MNC,[CELL_LAC]=i.CELL_LAC,[CELL_ID]=i.CELL_ID,[GSM_RSSI]=i.GSM_RSSI,
			[externalPowerState]=i.externalPowerState,[externalPowerVoltage]=i.externalPowerVoltage,
			[batteryVoltage]=i.batteryVoltage from inserted i 
			WHERE [CarTrack].[dbo].[GPSLatestLog].unitid=i.unitid and 
				[CarTrack].[dbo].[GPSLatestLog].vehicleid=i.vehicleid
		
		INSERT INTO [CarTrack].[dbo].[GPSHistoryLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
		
		select 1
	end
	else if(@isCorrectTime=0 and @IsUnitIdExist=1 and @isUpdate=1)
	begin
		/* only insert new record in dbo.GPSHistoryLog */
		
		INSERT INTO [CarTrack].[dbo].[GPSHistoryLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
			
		select 2
	end
	else if(@isCorrectTime=0 and @IsUnitIdExist=0 and @isUpdate=1) --or if(@isCorrectTime=1 and @IsUnitIdExist=0)
	begin
		/* 
			if unit id doesnt exist, you must insert in both tables.
			insert new record in GPSLatestLog and insert new record in dbo.GPSHistoryLog 
		*/
		
		INSERT INTO [CarTrack].[dbo].[GPSLatestLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
			
		INSERT INTO [CarTrack].[dbo].[GPSHistoryLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
		
		select 3
	end
	else if(@isCorrectTime=0 and @IsUnitIdExist=0 and @isUpdate=0) --or if(@isCorrectTime=1 and @IsUnitIdExist=0)
	begin
		/* 
			if unit id doesnt exist and its an insert you must insert in both tables.
			insert new record in GPSLatestLog and insert new record in dbo.GPSHistoryLog 
		*/
		
		INSERT INTO [CarTrack].[dbo].[GPSLatestLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
			
		INSERT INTO [CarTrack].[dbo].[GPSHistoryLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
		
		select 4
	end
	else if(@isCorrectTime=0 and @IsUnitIdExist=1 and @isUpdate=0) --or if(@isCorrectTime=1 and @IsUnitIdExist=0)
	begin
		/* 
			if unit id exist, time isnt correct and its an insert you must insert in both tables.
			insert new record in GPSLatestLog and insert new record in dbo.GPSHistoryLog 
		*/
					
		INSERT INTO [CarTrack].[dbo].[GPSHistoryLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
		
		select 5
	end
	else if(@isCorrectTime=1 and @IsUnitIdExist=1 and @isUpdate=0) --or if(@isCorrectTime=1 and @IsUnitIdExist=0)
	begin
		/* 
			if unit id exist, time is correct and its an insert you must insert in both tables.
			insert new record in GPSLatestLog and insert new record in dbo.GPSHistoryLog 
		*/
					
		UPDATE [CarTrack].[dbo].[GPSLatestLog] SET [UnitID]=i.UnitID,[SERIAL_NUM]=i.SERIAL_NUM,
			[Longitude]=i.Longitude,[Latitude]=i.Latitude,[GLB_TIME]=i.GLB_TIME,[Speed]=i.Speed,
			[Date]=@currentDate,[Time]=i.Time,[Distance]=i.Distance,[Course]=i.Course,
			[Direction]=i.Direction,[DeviceTypeId]=i.DeviceTypeId,[VehicleId]=i.VehicleId,
			[DeviceId]=i.DeviceId,[DeviceEventId]=i.DeviceEventId,
			[HasLocationData]=i.HasLocationData,[GPSStatus]=i.GPSStatus,[Alarm]=i.Alarm,
			[SequenceNo]=i.SequenceNo,[EngineState]=i.EngineState,[HDOP]=i.HDOP,[A2D1]=i.A2D1,
			[A2D2]=i.A2D2,[A2D3]=i.A2D3,[IO_INPUT1]=i.IO_INPUT1,[IO_INPUT2]=i.IO_INPUT2,
			[IO_INPUT3]=i.IO_INPUT3,[IO_INPUT4]=i.IO_INPUT4,[IO_INPUT5]=i.IO_INPUT5,
			[IO_INPUT6]=i.IO_INPUT6,[IO_INPUT7]=i.IO_INPUT7,[IO_INPUT8]=i.IO_INPUT8,
			[IO_OUTPUT1]=i.IO_OUTPUT1,[IO_OUTPUT2]=i.IO_OUTPUT2,[IO_OUTPUT3]=i.IO_OUTPUT3,
			[IO_OUTPUT4]=i.IO_OUTPUT4,[IO_OUTPUT5]=i.IO_OUTPUT5,[IO_OUTPUT6]=i.IO_OUTPUT6,
			[IO_OUTPUT7]=i.IO_OUTPUT7,[IO_OUTPUT8]=i.IO_OUTPUT8,[Altitude]=i.Altitude,
			[SLNAME]=i.SLNAME,[LOCNAME]=i.LOCNAME,[DIVNAME]=i.DIVNAME,[DISTNAME]=i.DISTNAME,
			[PROVNAME]=i.PROVNAME,[CONSTITUEN]=i.CONSTITUEN,[TOWN]=i.TOWN,[STREET]=i.STREET,
			[ImageUrl]=i.ImageUrl,[SatellitesUsed]=i.SatellitesUsed,[CELL_MCC]=i.CELL_MCC,
			[CELL_MNC]=i.CELL_MNC,[CELL_LAC]=i.CELL_LAC,[CELL_ID]=i.CELL_ID,[GSM_RSSI]=i.GSM_RSSI,
			[externalPowerState]=i.externalPowerState,[externalPowerVoltage]=i.externalPowerVoltage,
			[batteryVoltage]=i.batteryVoltage from inserted i 
			WHERE [CarTrack].[dbo].[GPSLatestLog].unitid=i.unitid and 
				[CarTrack].[dbo].[GPSLatestLog].vehicleid=i.vehicleid
		
		INSERT INTO [CarTrack].[dbo].[GPSHistoryLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
		
		select 6
	end
	else if(@isCorrectTime=1 and @IsUnitIdExist=0 and @isUpdate=0) --or if(@isCorrectTime=1 and @IsUnitIdExist=0)
	begin
		/* 
			if unit id doesnt exist, time is correct and its an insert you must insert in both tables.
			insert new record in GPSLatestLog and insert new record in dbo.GPSHistoryLog 
		*/
		
		SET Context_Info 0x55555;
					
		--INSERT INTO [CarTrack].[dbo].[GPSLatestLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
		--	[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
		--	[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
		--	[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
		--	[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
		--	[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
		--	[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
		--	[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
		--	[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		--select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
		--	[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
		--	[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
		--	[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
		--	[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
		--	[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
		--	[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
		--	[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
		--	[externalPowerVoltage],[batteryVoltage] from inserted
			
		INSERT INTO [CarTrack].[dbo].[GPSHistoryLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Date],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select [UnitID],[SERIAL_NUM],[Longitude],[Latitude],[GLB_TIME],[Speed],@currentDate,[Time],
			[Distance],[Course],[Direction],[DeviceTypeId],[VehicleId],[DeviceId],[DeviceEventId],
			[HasLocationData],[GPSStatus],[Alarm],[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],
			[A2D3],[IO_INPUT1],[IO_INPUT2],[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],
			[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],
			[IO_OUTPUT6],[IO_OUTPUT7],[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],
			[DISTNAME],[PROVNAME],[CONSTITUEN],[TOWN],[STREET],[ImageUrl],[SatellitesUsed],
			[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],[GSM_RSSI],[externalPowerState],
			[externalPowerVoltage],[batteryVoltage] from inserted
		
		select 7
	end

END
/*
	select * from gpslatestlog
	select top 40 * from [GPSHistoryLog] order by id desc
	
	update gpslatestlog set speed=41,unitid='34534543',deviceid=8,
		devicetypeid=1,vehicleid=9,serial_num='34534543',deviceeventid=7,sequenceno=34545
	
	----delete from gpslatestlog where id>4
*/