﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Text;

namespace DataAccess.SQL
{
    public class MenuDa
    {      
        public static IEnumerable<vw_ControllerView> getController(string[] RoleNames)
        {
            try
            {
                GFleetModelContainer gfleet = new GFleetModelContainer();
                return gfleet.vw_ControllerView.Where<vw_ControllerView>(w => RoleNames.Contains(w.RoleName));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex.InnerException == null ? ex : ex.InnerException);
            }
        }

        public static IEnumerable<vw_ControllerView> getSubController(int ContollerParentId)
        {
            try
            {
                GFleetModelContainer gfleet = new GFleetModelContainer();
                var cats = from c in gfleet.vw_ControllerView
                           where (c.ParentId == ContollerParentId)
                           //group 
                           select c;
                return cats.GroupBy(c => c.ControllerName).Select(c => c.FirstOrDefault()).ToList<vw_ControllerView>();
                //return gfleet.vw_ControllerView.Where<vw_ControllerView>(w => w.ParentId == ContollerParentId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex.InnerException == null ? ex : ex.InnerException);
            }
        }
        public static List<vw_ControllerView> getSubController(string controllerName)
        {
            try
            {
                using (GFleetModelContainer gfleet = new GFleetModelContainer())
                {
                    //vw_ControllerView vw = gfleet.vw_ControllerView.FirstOrDefault(v => v.ControllerName.Equals(controllerName));
                    //int controllerId = vw.ParentId.HasValue ? vw.ParentId.Value : vw.ControllerId;
                    //List<vw_ControllerView> data = gfleet.vw_ControllerView.Where(c => (c.ParentId == controllerId || c.ControllerId == controllerId)
                    //    && !c.ControllerName.Equals(controllerName)).GroupBy(c => c.ControllerName).Select(c => c.FirstOrDefault()).ToList<vw_ControllerView>();

                    //return data;
                    return gfleet.getSubController(controllerName)
                        .GroupBy(p => p.Url)
                        .Select(g => g.First())
                        .ToList();
                    //.ToList<vw_ControllerView>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex.InnerException == null ? ex : ex.InnerException);
            }
        }

        public static IEnumerable<vw_ClientTypeController> getClientTypeController(string ClientType)
        {
            try
            {
                GFleetModelContainer gfleet = new GFleetModelContainer();
                return gfleet.vw_ClientTypeController.Where<vw_ClientTypeController>(w => ClientType.Contains(w.ClientTypeName));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex.InnerException == null ? ex : ex.InnerException);
            }
        }
    }
}
