﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
//using EFTracingProvider;

namespace DataAccess.SQL
{
    #region enable entity tracing
    //public class NorthwindContext : GFleetModelContainer
    //{
    //    public NorthwindContext()
    //        : base(EFTracingProviderUtils.CreateTracedEntityConnection("GFleetModelContainer"))
    //    {
    //        this.EnableTracing();
    //    }
    //}
    #endregion

    #region Vehicle Validation
    [MetadataType(typeof(VehicleValidation))]
    public partial class Vehicle
    {
        public static bool VehicleExists(string RegistrationNo)
        {
            var vehicle = new GFleetModelContainer().Vehicles.FirstOrDefault(d => d.RegistrationNo == RegistrationNo);
            return (vehicle != null);
        }
        public static bool ChasisNoExists(string ChasisNo)
        {
            if (string.IsNullOrEmpty(ChasisNo))
                return false;

            var vehicle = new GFleetModelContainer().Vehicles.FirstOrDefault(d => d.ChasisNo == ChasisNo);
            return (vehicle != null);
        }

        public static bool EngineNoExists(string EngineNo)
        {
            if (string.IsNullOrEmpty(EngineNo))
                return false;

            var vehicle = new GFleetModelContainer().Vehicles.FirstOrDefault(d => d.EngineNo == EngineNo);
            return (vehicle != null);
        }

        //public string VehicleRegCode { get { return RegistrationNo + " (" + NickName + ")"; } }
    }
    public class VehicleValidation
    {
        [ScaffoldColumn(false)]
        public Int32 VehicleId { get; set; }

        [StringLength(40, MinimumLength = 2)]
        [Remote("IsReg_Available", "Validation", AdditionalFields = "isEditing")]
        [Required(ErrorMessage = "Vehicle Registration Required")]
        [Display(Name = "Vehicle Registration:")]
        public String RegistrationNo { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle Type")]
        [Display(Name = "Vehicle Type:")]
        public VehicleTypeId VehicleTypeId { get; set; }

        [Required(ErrorMessage = "Vehicle Color Required")]
        [Display(Name = "Vehicle Color:")]
        public String Color { get; set; }

        [Required(ErrorMessage = "Vehicle Make Required")]
        [Display(Name = "Vehicle Make:")]
        public String Make { get; set; }

        [Required(ErrorMessage = "Vehicle Model Required")]
        [Display(Name = "Vehicle Model:")]
        public String Model { get; set; }

        [Display(Name = "Vehicle Description:")]
        public String VehicleDesc { get; set; }

        [Required(ErrorMessage = "Year of Manufacture Required")]
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        [Display(Name = "Manufacture Year:")]
        public Int32 Year { get; set; }

        [Remote("IsEngine_Available", "Validation", AdditionalFields = "isEditing")]
        [Required(ErrorMessage = "Engine No. Required")]
        [Display(Name = "Engine No:")]
        public String EngineNo { get; set; }

        [Remote("IsChasis_Available", "Validation", AdditionalFields = "isEditing")]
        [Required(ErrorMessage = "Chasis No. Required")]
        [Display(Name = "Chasis No:")]
        public String ChasisNo { get; set; }

        [Required(ErrorMessage = "Select Vehicle Status")]
        [Display(Name = "Vehicle Status:")]
        public VehicleStatusId VehicleStatusId { get; set; }

        [Required(ErrorMessage = "Select Fuel Type")]
        [Display(Name = "Gasoline Type:")]
        public FuelTypeId FuelTypeId { get; set; }

        //[Required(ErrorMessage = "Please Enter Avg Fuel Consumption")]
        [Display(Name = "Avg Fuel Consumption(/Km):")]
        public int AvgFuelConsumption { get; set; }

        //[Required(ErrorMessage = "Please Enter Special Condition")]
        [Display(Name = "Vehicle Code:")]
        public String NickName { get; set; }

    }
    #endregion

    #region VehicleTypeId Validation
    [MetadataType(typeof(VehicleTypeIdValidation))]
    public partial class VehicleTypeId
    { }
    public class VehicleTypeIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Vehicle Type")]
        public int VehicleTypeId { get; set; }
        public string VehicleTypeName { get; set; }
    }
    #endregion

    #region FuelTypeId Validation
    [MetadataType(typeof(FuelTypeValidation))]
    public partial class FuelTypeId
    { }
    public class FuelTypeValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Fuel Type")]
        public int FuelTypeId { get; set; }
        public string FuelTypeName { get; set; }
    }
    #endregion

    #region SmsType Validation
    [MetadataType(typeof(SmsTypeValidation))]
    public partial class SMSTypeId
    { }
    public class SmsTypeValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Sms Type")]
        public int SMSTypeId { get; set; }
        public string SmsTypeName { get; set; }
    }
    #endregion
    
    #region DeviceType Validation
    [MetadataType(typeof(DeviceTypeValidation))]
    public partial class DeviceType
    { }
    public class DeviceTypeValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Fuel Type")]
        public int DeviceTypeId { get; set; }
        public string DeviceTypeName { get; set; }
    }
    #endregion

    #region VehicleStatusId Validation
    [MetadataType(typeof(VehicleStatusIdValidation))]
    public partial class VehicleStatusId
    { }
    public class VehicleStatusIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Vehicle Status")]
        public int Id { get; set; }
        public string VehicleStatu { get; set; }
    }
    #endregion

    #region Selected attribute validation
    public class MustBeSelectedAttribute : ValidationAttribute, IClientValidatable // IClientValidatable for client side Validation
    {
        public override bool IsValid(object value)
        {
            if (value == null || (int)value == 0)
                return false;
            else
                return true;
        }
        // Implement IClientValidatable for client side Validation
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new ModelClientValidationRule[] { new ModelClientValidationRule { ValidationType = "dropdown", ErrorMessage = this.ErrorMessage } };
        }
    }
    #endregion

    #region Date range validation
    public class DateRangeAttribute : ValidationAttribute
    {
        private const string DateFormat = "yyyy/MM/dd";
        private const string DefaultErrorMessage =
     "'{0}' must be a date between {1:d} and {2:d}.";

        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }

        public DateRangeAttribute(string minDate, string maxDate)
            : base(DefaultErrorMessage)
        {
            MinDate = ParseDate(minDate);
            MaxDate = ParseDate(maxDate);
        }

        public override bool IsValid(object value)
        {
            if (value == null || !(value is DateTime))
            {
                return true;
            }
            DateTime dateValue = (DateTime)value;
            return MinDate <= dateValue && dateValue <= MaxDate;
        }
        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture, ErrorMessageString, name, MinDate, MaxDate);
        }

        private static DateTime ParseDate(string dateValue)
        {
            return DateTime.ParseExact(dateValue, DateFormat, CultureInfo.InvariantCulture);
        }
    }
    #endregion

    #region checkbox validation
    public class MustBeTrueAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            return value is bool && (bool)value;
        }
        //// Implement IClientValidatable for client side Validation 
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new ModelClientValidationRule[] { new ModelClientValidationRule {
                ValidationType = "checkboxtrue", ErrorMessage = this.ErrorMessage } };
        }
    }
    #endregion

    #region Company Validation
    [MetadataType(typeof(CompanyValidation))]
    public partial class Company
    { }
    public class CompanyValidation
    {
        [ScaffoldColumn(false)]
        public Int32 Id { get; set; }

        //[Required(ErrorMessage = "Company")]
        //[Display(Name = "Company Name:")]
        //public String Company1 { get; set; }
        [Required(ErrorMessage = "Please Enter Postal Address")]
        [Display(Name = "Postal Address:")]
        public String PostalAddress { get; set; }
        [Required(ErrorMessage = "Please Enter Physical Address")]
        [Display(Name = "Physical Address:")]
        public String PhysicalAddress { get; set; }
        [Required(ErrorMessage = "Please Enter Phone Number")]
        [Display(Name = "Telephone No:")]
        public String TelephoneNo { get; set; }
        [Required(ErrorMessage = "Please enter company email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter Contact Person")]
        [Display(Name = "Contact Person:")]
        public String ContactPerson { get; set; }
        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Role name")]
        [ValidRoleNameAttribute(ErrorMessage = "Please Select Role name")]
        public UserRole UserRole { get; set; }

    }
    #endregion

    #region Password Length Validation
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class ValidatePasswordLengthAttribute : ValidationAttribute, IClientValidatable
    {
        private const string _defaultErrorMessage = "'{0}' must be at least {1} characters long.";
        private readonly int _minCharacters = Membership.Provider.MinRequiredPasswordLength;

        public ValidatePasswordLengthAttribute()
            : base(_defaultErrorMessage)
        {
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture, ErrorMessageString,
                name, _minCharacters);
        }

        public override bool IsValid(object value)
        {
            string valueAsString = value as string;
            return (valueAsString != null && valueAsString.Length >= _minCharacters);
        }
        public IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[]{
                new System.Web.Mvc.ModelClientValidationStringLengthRule(FormatErrorMessage(metadata.GetDisplayName()), _minCharacters, int.MaxValue)
            };
        }
    }
    #endregion

    #region Role name Validation
    public class ValidRoleNameAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (string.IsNullOrEmpty(((UserRole)value).RoleName))
                return false;
            else
                return true;
        }
    }
    #endregion

    #region user role validation
    public class UserRole
    {
        public string Role { get; set; }
        public string RoleName { get; set; }
        public Boolean Selected { get; set; }
    }
    #endregion

    #region OrganisationType Validation
    [MetadataType(typeof(OrganisationTypeValidation))]
    public partial class OrganisationType
    { }
    public class OrganisationTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 OrganisationTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter Customer Type")]
        [Display(Name = "Organisation Type:")]
        public String OrganisationTypeName { get; set; }
        [Required(ErrorMessage = "Please Enter Description")]
        [Display(Name = "Description:")]
        public String Description { get; set; }
    }
    #endregion

    #region CustomerType Validation
    [MetadataType(typeof(CustomerTypeValidation))]
    public partial class CustomerType
    { }
    public class CustomerTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 CustomerTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter Customer Type")]
        [Display(Name = "Organisation Type:")]
        public String CustomerTypeName { get; set; }
        [Required(ErrorMessage = "Please Enter Description")]
        [Display(Name = "Description:")]
        public String Description { get; set; }
    }
    #endregion

    #region SMSFormat Validation
    [MetadataType(typeof(DispatchSMSFormatValidation))]
    public partial class DispatchSMSFormat
    { }
    public class DispatchSMSFormatValidation
    {
        [ScaffoldColumn(false)]
        public Int32 SMSFormatId { get; set; }

        [Required(ErrorMessage = "Please Enter SMS Format")]
        [Display(Name = "SMS Format:")]
        public String SMSFormat { get; set; }

        [Display(Name = "Active:")]
        public Boolean Active { get; set; }

        //[Display(Name = "IsDriver:")]
        //public Boolean IsDriver { get; set; }

        [Required(ErrorMessage = "Select Sms Type")]
        [Display(Name = "Sms Type:")]
        public SMSTypeId SMSTypeId { get; set; }
    }
    #endregion

    #region SMSSetting Validation
    [MetadataType(typeof(SMSSettingValidation))]
    public partial class SMSSetting
    { }
    public class SMSSettingValidation
    {
        [ScaffoldColumn(false)]
        public Int32 SMSSettingsId { get; set; }

        [Required(ErrorMessage = "Please Enter A/c Username")]
        [Display(Name = "Username:")]
        public String Username { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [Display(Name = "Password:")]
        public String Password { get; set; }

        [Required(ErrorMessage = "Please Enter SenderId")]
        [Display(Name = "SenderId:")]
        public String SenderId { get; set; }

    }
    #endregion

    #region Driver Validation
    [MetadataType(typeof(DriverValidation))]
    public partial class Driver : Person
    {
        public string FullNames { get { return FirstName + " " + LastName; } }

        public string Telephones { get { return Telephone1 + ", " + Telephone2; } }

        public string Emails { get { return Email1 + ", " + Email2; } }

        public String DriverStatus
        {

            get { return DriverStatu == null ? "" : DriverStatu.DriverStatusName; }
        }
    }
    public class DriverValidation
    {
        [ScaffoldColumn(false)]
        public Int32 PersonId { get; set; }

        [Required(ErrorMessage = "Please Enter First Name")]
        [Display(Name = "First Name:")]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Last Name")]
        [Display(Name = "Last Name:")]
        public String LastName { get; set; }
        //[Required(ErrorMessage = "Please Enter Email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email1:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email1 { get; set; }
        [Required(ErrorMessage = "Please Enter Phone Number")]
        [Display(Name = "Phone1:")]
        public String Telephone1 { get; set; }
        //[Required(ErrorMessage = "Please Enter Email2")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Other Email:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email2 { get; set; }
        //[Required(ErrorMessage = "Please Enter Alternative Phone Number")]
        [Display(Name = "Phone2:")]
        public String Telephone2 { get; set; }
        [Display(Name = "Gender:")]
        [Required(ErrorMessage = "Please select Gender")]
        public Boolean IsMale { get; set; }
        //[Required(ErrorMessage = "Please Enter Post Code")]
        [Display(Name = "Post Code:")]
        public String PostCode { get; set; }

        //[Required(ErrorMessage = "Please Enter Fax Number")]
        [Display(Name = "Fax Number:")]
        public String FaxNumber { get; set; }

        //[Required(ErrorMessage = "Please Enter Driver Number")]
        [Display(Name = "Driver Number:")]
        public String DriverNo { get; set; }


        //[Required(ErrorMessage = "Please Enter Driver RFID")]
        [Display(Name = "Driver RFID:")]
        public String DriverRFID { get; set; }

        //[Required(ErrorMessage = "Please Enter Date Of Birth")]
        [Display(Name = "D.O.B:")]
        public String DOB { get; set; }
        [Display(Name = "IDNumber")]
        [Required(ErrorMessage = "Please Enter IDNumber")]
        public String IDNumber { get; set; }
        //Image ,
        [Display(Name = "NextOfKinName1:")]
        public String NextOfKinName1 { get; set; }
        [Display(Name = "NextOfKinName2:")]
        public String NextOfKinName2 { get; set; }
        [Display(Name = "NextOfKinPhone1:")]
        public String NextOfKinPhone1 { get; set; }
        [Display(Name = "NextOfKinPhone2:")]
        public String NextOfKinPhone2 { get; set; }
        [Display(Name = "Association1:")]
        public String Association1 { get; set; }
        [Display(Name = "Association2:")]
        public String Association2 { get; set; }
        [Display(Name = "NextOfKinEmail1:")]
        public String NextOfKinEmail1 { get; set; }
        [Display(Name = "NextOfKinEmail2:")]
        public String NextOfKinEmail2 { get; set; }
        [Display(Name = "Active:")]
        public Boolean Active { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        [Display(Name = "Licence Date")]
        [Required(ErrorMessage = "Please Enter Licence Date")]
        public String LicenceDate { get; set; }

        [Display(Name = "Special Conditions:")]
        public String SpecialConditions { get; set; }

        [Display(Name = "CofCNo:")]
        public String CofCNo { get; set; }

        [Display(Name = "Driver Status:")]
        public DriverStatusId DriverStatusId { get; set; }



    }
    #endregion

    #region CompanyStaff Validation
    [MetadataType(typeof(CompanyStaffValidation))]
    public partial class CompanyStaff : Person
    {
        public string FullNames { get { return FirstName + " " + LastName; } }

        public string Telephones { get { return Telephone1 + ", " + Telephone2; } }

        public string Emails { get { return Email1 + ", " + Email2; } }
    }
    public class CompanyStaffValidation
    {
        [ScaffoldColumn(false)]
        public Int32 PersonId { get; set; }

        [Required(ErrorMessage = "Please Enter First Name")]
        [Display(Name = "First Name:")]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Last Name")]
        [Display(Name = "Last Name:")]
        public String LastName { get; set; }
        //[Required(ErrorMessage = "Please Enter Email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email1:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email1 { get; set; }
        [Required(ErrorMessage = "Please Enter Phone Number")]
        [Display(Name = "Phone1:")]
        public String Telephone1 { get; set; }
        //[Required(ErrorMessage = "Please Enter Email2")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Other Email:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email2 { get; set; }
        //[Required(ErrorMessage = "Please Enter Alternative Phone Number")]
        [Display(Name = "Phone2:")]
        public String Telephone2 { get; set; }
        [Display(Name = "Gender:")]
        [Required(ErrorMessage = "Please select Gender")]
        public Boolean IsMale { get; set; }

        //[Required(ErrorMessage = "Please Enter Date Of Birth")]
        [Display(Name = "D.O.B:")]
        public String DOB { get; set; }

        [Display(Name = "Employee No.:")]
        public String EmployeeNo { get; set; }

        [Display(Name = "IDNumber")]
        //[Required(ErrorMessage = "Please Enter IDNumber")]
        public String IDNumber { get; set; }
        //Image ,
        [Display(Name = "NextOfKinName1:")]
        public String NextOfKinName1 { get; set; }
        [Display(Name = "NextOfKinName2:")]
        public String NextOfKinName2 { get; set; }
        [Display(Name = "NextOfKinPhone1:")]
        public String NextOfKinPhone1 { get; set; }
        [Display(Name = "NextOfKinPhone2:")]
        public String NextOfKinPhone2 { get; set; }
        [Display(Name = "Association1:")]
        public String Association1 { get; set; }
        [Display(Name = "Association2:")]
        public String Association2 { get; set; }
        [Display(Name = "NextOfKinEmail1:")]
        public String NextOfKinEmail1 { get; set; }
        [Display(Name = "NextOfKinEmail2:")]
        public String NextOfKinEmail2 { get; set; }
        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
    }
    #endregion

    #region Customer Validation
    [MetadataType(typeof(CustomerValidation))]
    public partial class Customer : Person
    {
        public string FullNames { get { return FirstName + " " + LastName; } }

        public string Telephones { get { return Telephone1 + ", " + Telephone2; } }

        public string Emails { get { return Email1 + ", " + Email2; } }

    }
    public class CustomerValidation
    {
        [ScaffoldColumn(false)]
        public Int32 PersonId { get; set; }

        [Required(ErrorMessage = "Please Enter First Name")]
        [Display(Name = "First Name:")]
        public String FirstName { get; set; }

        [Required(ErrorMessage = "Please Enter Last Name")]
        [Display(Name = "Last Name:")]
        public String LastName { get; set; }

        //[Required(ErrorMessage = "Please Enter Email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email1:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email1 { get; set; }

        [Required(ErrorMessage = "Please Enter Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone:")]
        [RegularExpression(@"^(?=\+)(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?^.{10,}$", ErrorMessage = "Please enter correct Phone Number")]
        public String Telephone1 { get; set; }

        //[Required(ErrorMessage = "Please Enter Email2")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Other Email:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email2 { get; set; }

        //[Required(ErrorMessage = "Please Enter Alternative Phone Number")]
        [Display(Name = "Phone2:")]
        public String Telephone2 { get; set; }

        [Display(Name = "Gender:")]
        [Required(ErrorMessage = "Please select Gender")]
        public Boolean IsMale { get; set; }

        //[Required(ErrorMessage = "Please Enter Post Code")]
        [Display(Name = "Post Code:")]
        public String PostCode { get; set; }
        //[Required(ErrorMessage = "Please Enter Fax Number")]

        [Display(Name = "Fax Number:")]
        public String FaxNumber { get; set; }

        //[Required(ErrorMessage = "Please Enter Date Of Birth")]
        [Display(Name = "D.O.B:")]
        public String DOB { get; set; }

        //[Required(ErrorMessage = "Please Enter Credit Limit")]
        [Display(Name = "Credit Limit:")]
        public String CreditLimit { get; set; }

        //Image ,
        [Display(Name = "NextOfKinName1:")]
        public String NextOfKinName1 { get; set; }

        [Display(Name = "NextOfKinName2:")]
        public String NextOfKinName2 { get; set; }

        [Display(Name = "NextOfKinPhone1:")]
        public String NextOfKinPhone1 { get; set; }

        [Display(Name = "NextOfKinPhone2:")]
        public String NextOfKinPhone2 { get; set; }

        [Display(Name = "Association1:")]
        public String Association1 { get; set; }

        [Display(Name = "Association2:")]
        public String Association2 { get; set; }

        [Display(Name = "NextOfKinEmail1:")]
        public String NextOfKinEmail1 { get; set; }

        [Display(Name = "NextOfKinEmail2:")]
        public String NextOfKinEmail2 { get; set; }


        [Display(Name = "Customer Type:")]
        public CustomerTypeId CustomerTypeId { get; set; }

        [Display(Name = "Upload Map:")]
        public String CustomerDirUrl { get; set; }

        [Display(Name = "Organisation:")]
        [Required(ErrorMessage = "Please select Organisation")]
        public OrganisationId OrganisationId { get; set; }

        [Display(Name = "Department:")]
        [Required(ErrorMessage = "Please select Department")]
        public OrganisationDeptId OrganisationDeptId { get; set; }

        [Display(Name = "Bookings Need Approval?:")]
        public Boolean IsApproved { get; set; }

        [Display(Name = "IsVIP:")]
        public Boolean IsVIP { get; set; }

        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
    }
    #endregion

    #region SystemUser Validation
    [MetadataType(typeof(SystemUserValidation))]
    public partial class SystemUser : Person
    {
        public string FullNames { get { return (FirstName != null ? FirstName : "_") + " " + (LastName != null ? LastName : "_"); } }

        public string Telephones { get { return (Telephone1 != null ? Telephone1 : "") + ", " + (Telephone2 != null ? Telephone2 : ""); } }

        public string Emails { get { return (Email1 != null ? Email1 : "") + ", " + (Email2 != null ? Email2 : ""); } }
    }
    public class SystemUserValidation
    {
        [ScaffoldColumn(false)]
        public Int32 PersonId { get; set; }

        [Required(ErrorMessage = "Please Enter First Name")]
        [Display(Name = "First Name:")]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Last Name")]
        [Display(Name = "Last Name:")]
        public String LastName { get; set; }
        [Required(ErrorMessage = "Please enter your email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email1:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email1 { get; set; }
        [Required(ErrorMessage = "Please Enter Phone Number")]
        [Display(Name = "Phone1:")]
        public String Telephone1 { get; set; }
        //[Required(ErrorMessage = "Please Enter Email2")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Other Email:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email2 { get; set; }
        //[Required(ErrorMessage = "Please Enter Alternative Phone Number")]
        [Display(Name = "Phone2:")]
        public String Telephone2 { get; set; }

        [Display(Name = "Gender:")]
        [Required(ErrorMessage = "Please select Gender")]
        public Boolean IsMale { get; set; }

        //[Required(ErrorMessage = "Please Enter Post Code")]
        [Display(Name = "Post Code:")]
        public String PostCode { get; set; }
        //[Required(ErrorMessage = "Please Enter Fax Number")]

        [Display(Name = "Fax Number:")]
        public String FaxNumber { get; set; }

        //[Required(ErrorMessage = "Please Enter Date Of Birth")]
        [Display(Name = "D.O.B:")]
        public String DOB { get; set; }
        //Image ,
        [Display(Name = "NextOfKinName1:")]
        public String NextOfKinName1 { get; set; }

        [Display(Name = "NextOfKinName2:")]
        public String NextOfKinName2 { get; set; }

        [Display(Name = "NextOfKinPhone1:")]
        public String NextOfKinPhone1 { get; set; }

        [Display(Name = "NextOfKinPhone2:")]
        public String NextOfKinPhone2 { get; set; }

        [Display(Name = "Association1:")]
        public String Association1 { get; set; }

        [Display(Name = "Association2:")]
        public String Association2 { get; set; }

        [Display(Name = "NextOfKinEmail1:")]
        public String NextOfKinEmail1 { get; set; }

        [Display(Name = "NextOfKinEmail2:")]
        public String NextOfKinEmail2 { get; set; }

        [Display(Name = "Department:")]
        [Required(ErrorMessage = "Please select Department")]
        public OrganisationDeptId OrganisationDeptId { get; set; }

    }
    #endregion

    #region CustomerTypeId Validation
    [MetadataType(typeof(CustomerTypeIdValidation))]
    public partial class CustomerTypeId
    { }
    public class CustomerTypeIdValidation
    {
        [ScaffoldColumn(false)]
        public Int32 CustomerTypeId { get; set; }

        [Required(ErrorMessage = "Please Select Customer Type")]
        [Display(Name = "Customer Type:")]
        public String CustomerTypeName { get; set; }
    }
    #endregion

    #region OrganisationId Validation
    [MetadataType(typeof(OrganisationIdValidation))]
    public partial class OrganisationId
    { }
    public class OrganisationIdValidation
    {
        [ScaffoldColumn(false)]
        public Int32 OrganisationId { get; set; }

        [Required(ErrorMessage = "Please Select Organisation")]
        [Display(Name = "Organisation:")]
        public String OrganisationName { get; set; }
    }
    #endregion

    #region OrganisationDeptId Validation
    [MetadataType(typeof(OrganisationDeptIdValidation))]
    public partial class OrganisationDeptId
    { }
    public class OrganisationDeptIdValidation
    {
        [ScaffoldColumn(false)]
        public Int32 OrganisationDeptId { get; set; }

        [Required(ErrorMessage = "Please Select Department")]
        [Display(Name = "Organisation:")]
        public String DepartmentName { get; set; }
    }
    #endregion

    #region DriverLicenceType Validation
    [MetadataType(typeof(LicenceTypeValidation))]
    public partial class LicenceType
    { }
    public class LicenceTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 LicenceTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter Licence Type")]
        [Display(Name = "Licence Type:")]
        public String DriverLicenceType { get; set; }
    }
    #endregion

    #region DriverStatus Validation
    [MetadataType(typeof(DriverStatusValidation))]
    public partial class DriverStatu
    { }
    public class DriverStatusValidation
    {
        [ScaffoldColumn(false)]
        public Int32 DriverStatusId { get; set; }

        [Required(ErrorMessage = "Please Enter Driver Status")]
        [Display(Name = "Driver Status:")]
        public String DriverStatusName { get; set; }
        [Required(ErrorMessage = "Please Enter Description")]
        [Display(Name = "Description:")]
        public String Description { get; set; }
        [Display(Name = "Active:")]
        public Boolean IsAvailable { get; set; }
    }
    #endregion

    #region DriverStatusId Validation
    [MetadataType(typeof(DriverStatusIdValidation))]
    public partial class DriverStatusId
    { }
    public class DriverStatusIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Driver Status")]
        public int DriverStatusId { get; set; }
        public string DriverStatusName { get; set; }
    }
    #endregion

    #region DriverVehicleAssignment Validation
    [MetadataType(typeof(DriverVehicleAssignmentValidation))]
    public partial class DriverVehicleAssignment
    {
        String _StartDateDate;
        public String StartDateDate
        {
            get
            {
                return string.IsNullOrEmpty(_StartDateDate) ? String.Format("{0:d}", StartDate) : _StartDateDate;
            }
            set { _StartDateDate = value; }
        }

        String _EndDateDate;
        public String EndDateDate
        {
            get
            {
                return string.IsNullOrEmpty(_EndDateDate) ? String.Format("{0:d}", EndDate) : _EndDateDate;
            }
            set { _EndDateDate = value; }
        }

        String _StartDateTime;
        public String StartDateTime
        {
            get
            {
                return string.IsNullOrEmpty(_StartDateTime) ? String.Format("{0:t}", StartDate) : _StartDateTime;
            }
            set { _StartDateTime = value; }
        }

        String _EndDateTime;
        public String EndDateTime
        {
            get
            {
                return string.IsNullOrEmpty(_EndDateTime) ? String.Format("{0:t}", EndDate) : _EndDateTime;
            }
            set { _EndDateTime = value; }
        }

    }
    public class DriverVehicleAssignmentValidation
    {
        [ScaffoldColumn(false)]
        public Int32 DriverVehicleAssignmentId { get; set; }

        [Required(ErrorMessage = "Please Select Driver")]
        [Display(Name = "Driver:")]
        public PersonId PersonId { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public VehicleId VehicleId { get; set; }

        [Required(ErrorMessage = "Please Select  Start Date")]
        [DisplayFormat(DataFormatString = "{0:d}", ConvertEmptyStringToNull = true)]
        [Display(Name = "Start Date:")]
        public String StartDateDate { get; set; }

        [Display(Name = "Start Time:")]
        [DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String StartDateTime { get; set; }

        [Required(ErrorMessage = "Please Select End Date")]
        [DisplayFormat(DataFormatString = "{0:d}", ConvertEmptyStringToNull = true)]
        [Display(Name = "End Date:")]
        public String EndDateDate { get; set; }

        [Display(Name = "End Time:")]
        [DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String EndDateTime { get; set; }

        [Required(ErrorMessage = "Please Enter Notes")]
        [Display(Name = "Notes:")]
        public String Notes { get; set; }
    }
    #endregion

    #region PersonId Validation
    [MetadataType(typeof(PersonIdValidation))]
    public partial class PersonId
    { }
    public class PersonIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Driver")]
        public int PersonId { get; set; }
        public string Surname { get; set; }
    }
    #endregion

    #region VehicleId Validation
    [MetadataType(typeof(VehicleIdValidation))]
    public partial class VehicleId
    { }
    public class VehicleIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Vehicle")]
        public int VehicleId { get; set; }
        public string RegistrationNo { get; set; }
    }
    #endregion

    #region DriverEndorsment Validation
    [MetadataType(typeof(DriverEndorsmentValidation))]
    public partial class DriverEndorsmentAssignment
    { }
    public class DriverEndorsmentValidation
    {
        [ScaffoldColumn(false)]
        public Int32 Id { get; set; }

        [Required(ErrorMessage = "Please Select Driver")]
        [Display(Name = "Driver:")]
        public PersonId DriverUserId { get; set; }
        [Required(ErrorMessage = "Please Select Enter Endosment Message")]
        [Display(Name = "Endosment:")]
        public String Endosment { get; set; }
        [Display(Name = "Like:")]
        public Boolean Like { get; set; }
    }
    #endregion

    #region VehicleDispatch Validation
    [MetadataType(typeof(VehicleDispatchValidation))]
    public partial class Dispatch
    {
        String _ReturnDate;
        public String ReturnDate
        {
            get
            {
                return string.IsNullOrEmpty(_ReturnDate) ? String.Format("{0:d}", ExpectedCompletionTime) : _ReturnDate;
            }
            set { _ReturnDate = value; }
        }
        String _PickUpDate;
        public String PickUpDate
        {
            get
            {
                return string.IsNullOrEmpty(_PickUpDate) ? String.Format("{0:d}",
                    PickUpDateTime == DateTime.MinValue ? null : PickUpDate) : _PickUpDate;
            }
            set { _PickUpDate = value; }
        }


        String _PickUpTime;
        public String PickUpTime
        {
            get
            {
                return string.IsNullOrEmpty(_PickUpTime) ? String.Format("{0:t}",
                    PickUpDateTime == DateTime.MinValue ? null : PickUpTime) : _PickUpTime;
                DateTime Time = Convert.ToDateTime(PickUpDateTime);
                Time.ToString("HH:mm:ss");
            }
            set { _PickUpTime = value; }
        }

        String _ReturnTime;
        public String ReturnTime
        {
            get
            {
                return string.IsNullOrEmpty(_ReturnTime) ? String.Format("{0:t}", ExpectedCompletionTime) : _ReturnTime;
            }
            set { _ReturnTime = value; }
        }

        public String PassengerNames
        {//return Person.FirstName + " " + Person.LastName;
            get { return Person == null ? "" : Person.FirstName + " " + Person.LastName; }
            ///get { return ""; /*Customer.FirstName + " " + Customer.LastName;*/ }
        }
        public String PassengerOrganisation
        {
            get { return Person == null ? "" : Person.Organisation.OrganisationName; }
        }
        public String PassengerTelephone
        {
            get { return Person == null ? "" : Person.Telephone1; }
        }
        public String VehicleDispatched
        {

            get { return Vehicle == null ? "" : Vehicle.VehicleRegCode; }
        }
    }
    public class VehicleDispatchValidation
    {
        [ScaffoldColumn(false)]
        public Int32 DispatchId { get; set; }


        [Required(ErrorMessage = "Please Enter Location From")]
        [Display(Name = "Pick Up:")]
        public String PickUpPointName { get; set; }

        //[Required(ErrorMessage = "Please Enter Location To")]
        [Display(Name = "Drop Off:")]
        public String DropOffPointName { get; set; }

        [Display(Name = "Distance")]
        //[Required(ErrorMessage = "Please Enter Distance")]
        public String Distance { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public int DriverVehicleAssignmentId { get; set; }

        //[Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public int VehicleId { get; set; }

        [Required(ErrorMessage = "Please Select Passenger")]
        [Display(Name = "Passenger:")]
        public int CustomerId { get; set; }

        //[Required(ErrorMessage = "Please Enter Amount")]
        [Display(Name = "Amount(/Km)")]
        public String Amount { get; set; }


        //[Required(ErrorMessage = "Please Enter Special Instructions")]
        [Display(Name = "Special Instructions")]
        public String SpecialInstructions { get; set; }


        //[Required(ErrorMessage = "Please Enter Bookers Phone")]
        [Display(Name = "Bookers Phone")]
        public String BookersPhone { get; set; }


        [Required(ErrorMessage = "Please Enter BookingRequestedBy")]
        [Display(Name = "Booking RequestedBy")]
        public String BookingRequestedBy { get; set; }

        [Display(Name = "Return Time:")]
        [DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String ReturnTime { get; set; }

        [Display(Name = "Return Date:")]
        [DisplayFormat(DataFormatString = "{0:d}", ConvertEmptyStringToNull = true)]
        public String ReturnDate { get; set; }

        [Required(ErrorMessage = "Please Enter PickUp Date")]
        [Display(Name = "PickUp Date:")]
        [DisplayFormat(DataFormatString = "{0:d}", ConvertEmptyStringToNull = true)]
        public String PickUpDate { get; set; }

        [Required(ErrorMessage = "Please Enter PickUp Time")]
        [Display(Name = "PickUp Time:")]
        [DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String PickUpTime { get; set; }

        [Required(ErrorMessage = "Please Enter No. Of Passengers")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Please enter correct email")]
        [Display(Name = "No. Of Passengers:")]
        public int NoOfPassengers { get; set; }

        [Display(Name = "No Of Luggage:")]
        public int NoOfLuggage { get; set; }


        [Display(Name = "Recurrent:")]
        public Boolean Isreccurent { get; set; }

        [Display(Name = "Driver Name")]
        public Guid DriverUserId { get; set; }
    }
    #endregion

    #region CustomerId Validation
    [MetadataType(typeof(CustomerIdValidation))]
    public partial class CustomerId
    { }
    public class CustomerIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Customer")]
        public int CustomerId { get; set; }
        public string CustomerNames { get; set; }
    }
    #endregion

    #region VehicleFuelManagement Validation
    [MetadataType(typeof(VehicleFuelManagementValidation))]
    public partial class VehicleFuelManagement
    {
        String _FueledDate;
        public String FueledDate
        {
            get
            {
                return string.IsNullOrEmpty(_FueledDate) ? String.Format("{0:d}", Date) : _FueledDate;
            }
            set { _FueledDate = value; }
        }
        String _FueledTime;
        public String FueledTime
        {
            get
            {
                return string.IsNullOrEmpty(_FueledTime) ? String.Format("{0:t}", Date) : _FueledTime;
            }
            set { _FueledTime = value; }
        }
        public string VehicleRegCode { get { return Vehicle.RegistrationNo + " (" + Vehicle.NickName + ")"; } }
    }
    public class VehicleFuelManagementValidation
    {
        [ScaffoldColumn(false)]
        public Int32 VehicleFuelManagementId { get; set; }

        [Required(ErrorMessage = "Please Select Driver")]
        [Display(Name = "Fueled By:")]
        public String DriverUserId { get; set; }

        [Required(ErrorMessage = "Please Enter Time")]
        [DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String FueledTime { get; set; }

        [Required(ErrorMessage = "Please Select Date")]
        [DisplayFormat(DataFormatString = "{0:d}", ConvertEmptyStringToNull = true)]
        [Display(Name = "Fueling Date:")]
        public String FueledDate { get; set; }

        [Required(ErrorMessage = "Please Enter Previous Mileage")]
        [Display(Name = "Previous Mileage:")]
        public String PreviousMileage { get; set; }

        [Required(ErrorMessage = "Please Enter CurrentMileage")]
        [Display(Name = "Current Mileage:")]
        public String CurrentMileage { get; set; }

        [Required(ErrorMessage = "Please Enter Difference")]
        [Display(Name = "Difference:")]
        public int Difference { get; set; }

        [Display(Name = "No. Of Litres")]
        [Required(ErrorMessage = "Please Enter No. Of Litres")]
        public double? NoOfLitres { get; set; }

        [Required(ErrorMessage = "Please Enter Cost")]
        [Display(Name = "Cost:")]
        public double? Cost { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public VehicleId VehicleId { get; set; }

    }
    #endregion

    #region VehicleService Validation
    [MetadataType(typeof(VehicleServiceValidation))]
    public partial class VehicleService
    { }
    public class VehicleServiceValidation
    {
        [ScaffoldColumn(false)]
        public Int32 Id { get; set; }

        [Required(ErrorMessage = "Please Select Service Type")]
        [Display(Name = "Service Type:")]
        public ServiceTypeId ServiceTypeId { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public VehicleId VehicleId { get; set; }

        [Required(ErrorMessage = "Please Enter Current Odometer Read")]
        [Display(Name = "Current Odometer Read:")]
        public String CurrentOdometerRead { get; set; }

        [Required(ErrorMessage = "Please Enter Requirement")]
        [Display(Name = "Requirement:")]
        public String Requirement { get; set; }

        [Required(ErrorMessage = "Please Enter Cost")]
        [Display(Name = "Cost:")]
        public String Cost { get; set; }

        [Display(Name = "NextServiceAfter")]
        [Required(ErrorMessage = "Please Enter Next Service After")]
        public String NextServiceAfter { get; set; }

        [Required(ErrorMessage = "Please Enter Service Date")]
        [Display(Name = "Service Date:")]
        public String ServiceDate { get; set; }
    }
    #endregion

    #region ServiceTypeId Validation
    [MetadataType(typeof(ServiceTypeIdValidation))]
    public partial class ServiceTypeId
    { }
    public class ServiceTypeIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Service Type")]
        public int ServiceTypeId { get; set; }
        public string ServiceType { get; set; }
    }
    #endregion

    #region ServiceType Validation
    [MetadataType(typeof(ServiceTypeValidation))]
    public partial class ServiceType
    { }
    public class ServiceTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 Id { get; set; }

        [Required(ErrorMessage = "Please Enter Service Type")]
        [Display(Name = "Service Type:")]
        public String ServiceType { get; set; }

        [Required(ErrorMessage = "Please Enter Notes")]
        [Display(Name = "Notes:")]
        public String Notes { get; set; }
    }
    #endregion

    #region VehicleServiceDetail Validation
    [MetadataType(typeof(VehicleServiceDetailValidation))]
    public partial class VehicleServiceDetail
    { }
    public class VehicleServiceDetailValidation
    {
        [ScaffoldColumn(false)]
        public Int32 Id { get; set; }


        [Required(ErrorMessage = "Please Enter Service Item")]
        [Display(Name = "Service Item:")]
        public String ServiceItem { get; set; }

        [Required(ErrorMessage = "Please Enter Action")]
        [Display(Name = "Action:")]
        public String Action { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle Service")]
        [Display(Name = "Vehicle Service:")]
        public VehicleServiceId VehicleServiceId { get; set; }
    }
    #endregion

    #region VehicleServiceId Validation
    [MetadataType(typeof(VehicleServiceIdValidation))]
    public partial class VehicleServiceId
    { }
    public class VehicleServiceIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Vehicle Service")]
        public int VehicleServiceId { get; set; }
        public string Requirement { get; set; }
    }
    #endregion

    #region VehicleLicenceType Validation
    [MetadataType(typeof(VehicleLicenceTypeValidation))]
    public partial class VehicleLicenceType
    { }
    public class VehicleLicenceTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 VehicleLicenceTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter Vehicle Licence Type")]
        [Display(Name = "Licence Type:")]
        public String VehicleLicenceTypeName { get; set; }
    }
    #endregion

    #region InsuaranceType Validation
    [MetadataType(typeof(InsuaranceTypeValidation))]
    public partial class InsuaranceType
    { }
    public class InsuaranceTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 InsuaranceTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter InsuaranceType")]
        [Display(Name = "Insuarance Type:")]
        public String InsuaranceTypeName { get; set; }

        [Required(ErrorMessage = "Please Enter Description")]
        [Display(Name = "Description:")]
        public String Description { get; set; }

        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
    }
    #endregion

    #region Insuarance Validation
    [MetadataType(typeof(InsuaranceValidation))]
    public partial class Insuarance
    { }
    public class InsuaranceValidation
    {
        [ScaffoldColumn(false)]
        public Int32 InsuaranceId { get; set; }

        [Required(ErrorMessage = "Please Enter Insuarance")]
        [Display(Name = "Insuarance:")]
        public String InsuaranceName { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [Display(Name = "Address:")]
        public String Address { get; set; }

        [Required(ErrorMessage = "Please Enter Telephone")]
        [Display(Name = "Telephone:")]
        public String Telephone { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [Display(Name = "Email:")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Please Enter Contact Person")]
        [Display(Name = "Contact Person:")]
        public String ContactPerson { get; set; }

        [Display(Name = "Active:")]
        public String Active { get; set; }
    }
    #endregion

    #region Client Validation
    [MetadataType(typeof(ClientValidation))]
    public partial class Client
    { }
    public class ClientValidation
    {
        [ScaffoldColumn(false)]
        public Int32 ClientId { get; set; }

        [Required(ErrorMessage = "Please Enter Client Name")]
        [Display(Name = "Client:")]
        public String ClientName { get; set; }

        [Required(ErrorMessage = "Please Enter BrandName")]
        [RegularExpression(@"^[\w]+[A-Za-z]$", ErrorMessage = "Please enter correct BrandName, special characters including spaces are not allowed")]
        [Display(Name = "Brand Name:")]
        public String BrandName { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [Display(Name = "Address(Box):")]
        public String ClientAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Telephone No.")]
        [Display(Name = "Telephone:")]
        public String ClientTelephone { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [Display(Name = "Email:")]
        public String ClientEmail { get; set; }

        [Required(ErrorMessage = "Please Enter Physical Address")]
        [Display(Name = "Physical Address:")]
        public String ClientPhysicalAddress { get; set; }

        [Required(ErrorMessage = "Please Select Client Type")]
        [Display(Name = "Client Type:")]
        public Int32 ClientTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter Contact Person")]
        [Display(Name = "Contact Person:")]
        public String ContactPerson { get; set; }

        [Display(Name = "Active:")]
        public String Active { get; set; }
    }
    #endregion

    #region Device Validation
    [MetadataType(typeof(DeviceValidation))]
    public partial class Device
    {

    }
    public class DeviceValidation
    {
        [ScaffoldColumn(false)]
        public Int32 DeviceId { get; set; }

        [Required(ErrorMessage = "Please select  Device Model")]
        [Display(Name = "Device Model:")]
        public Int32 DeviceModelId { get; set; }

        [Required(ErrorMessage = "Please Enter Serial Number")]
        [Display(Name = "Serial No.:")]
        public String SerialNo { get; set; }

        [Required(ErrorMessage = "Please Enter Telephone")]
        [Display(Name = "Phone No:")]
        public String PhoneNo { get; set; }

        [Required(ErrorMessage = "Select Device Type")]
        [Display(Name = "Device Type:")]
        public Int32 DeviceTypeId { get; set; }
    }
    #endregion

    #region Group Validation
    [MetadataType(typeof(GroupValidation))]
    public partial class Group
    { }
    public class GroupValidation
    {
        [ScaffoldColumn(false)]
        public Int32 GroupId { get; set; }

        [Required(ErrorMessage = "Please Enter Group Name")]
        [Display(Name = "Group:")]
        public String GroupName { get; set; }

        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
    }
    #endregion

    #region MaintainanceComponent Validation
    [MetadataType(typeof(MaintainanceComponentValidation))]
    public partial class MaintainanceComponent
    { }
    public class MaintainanceComponentValidation
    {
        [ScaffoldColumn(false)]
        public Int32 MaintainanceComponentId { get; set; }

        [Required(ErrorMessage = "Please Enter Spare Part")]
        [Display(Name = "Spare Part:")]
        public String ComponentName { get; set; }

        [Display(Name = "Spare Part Number :")]
        public Boolean ComponentNumber { get; set; }
    }
    #endregion

    #region Maintainance Validation
    [MetadataType(typeof(MaintainanceValidation))]
    public partial class Maintainance
    { }
    public class MaintainanceValidation
    {
        [ScaffoldColumn(false)]
        public Int32 MaintainanceId { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public int VehicleId { get; set; }

        [Required(ErrorMessage = "Please Select Maintainance Type")]
        [Display(Name = "Maintainance Type:")]
        public int MaintainanceTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter Description")]
        [Display(Name = "Description:")]
        public String Description { get; set; }

        [Required(ErrorMessage = "Please Enter Driver name")]
        [Display(Name = "Driver:")]
        public String DriverName { get; set; }

        [Required(ErrorMessage = "Please Enter Driver Telephone")]
        [Display(Name = "Driver Telephone:")]
        public String DriverTelephone { get; set; }

        [Required(ErrorMessage = "Please Enter Vehicle Station")]
        [Display(Name = "Vehicle Station:")]
        public String VehicleStation { get; set; }

    }
    #endregion

    #region OrganisationDepartment Validation
    [MetadataType(typeof(OrganisationDepartmentValidation))]
    public partial class OrganisationDepartment
    { }
    public class OrganisationDepartmentValidation
    {
        [ScaffoldColumn(false)]
        public Int32 OrganisationDeptId { get; set; }

        [Required(ErrorMessage = "Please Enter Department Name")]
        [Display(Name = "Department:")]
        public String DepartmentName { get; set; }

        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
    }
    #endregion

    #region Organisation Validation
    [MetadataType(typeof(OrganisationValidation))]
    public partial class Organisation
    { }
    public class OrganisationValidation
    {
        [ScaffoldColumn(false)]
        public Int32 OrganisationId { get; set; }

        [Required(ErrorMessage = "Please Enter Organisation Name")]
        [Display(Name = "Organisation:")]
        public String OrganisationName { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [Display(Name = "Address(Box):")]
        public String OrganisationAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Telephone No.")]
        [Display(Name = "Telephone:")]
        public String OrganisationTelephone { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [Display(Name = "Email:")]
        public String OrganisationEmail { get; set; }

        [Required(ErrorMessage = "Please Enter Physical Address")]
        [Display(Name = "Physical Address:")]
        public String OrganisationPhysicalAddress { get; set; }

        [Required(ErrorMessage = "Please Select Organisation Type")]
        [Display(Name = "Organisation Type:")]
        public Int32 OrganisationTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter Contact Person")]
        [Display(Name = "Contact Person:")]
        public String ContactPerson { get; set; }

        [Display(Name = "Active:")]
        public String Active { get; set; }

        [Display(Name = "Bookings Need Approval?:")]
        public String IsApproved { get; set; }
    }
    #endregion

    #region VehicleInsuarance Validation
    [MetadataType(typeof(VehicleInsuaranceValidation))]
    public partial class VehicleInsuarance
    {
        public string VehicleRegCode { get { return Vehicle.RegistrationNo + " (" + Vehicle.NickName + ")"; } }
    }
    public class VehicleInsuaranceValidation
    {
        [ScaffoldColumn(false)]
        public Int32 VehicleInsuaranceId { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public VehicleId VehicleId { get; set; }

        [Required(ErrorMessage = "Please Select Insuarance")]
        [Display(Name = "Insuarance:")]
        public InsuaranceId InsuaranceId { get; set; }

        [Required(ErrorMessage = "Please Select Insuarance Type")]
        [Display(Name = "Insuarance Type:")]
        public InsuaranceTypeId InsuaranceTypeId { get; set; }

        [Required(ErrorMessage = "Please Select Start Date")]
        [Display(Name = "Renewal Date:")]
        [CompareDates("EndDate", false, ErrorMessage = "Expiry date cannot be before Renewal date")]
        public String StartDate { get; set; }

        [Required(ErrorMessage = "Please Select End Date")]
        [Display(Name = "Expiry Date:")]
        [CompareDates("StartDate", false, ErrorMessage = "Expiry date cannot be before Renewal date")]
        public String EndDate { get; set; }

        [Required(ErrorMessage = "Please Enter Policy Number")]
        [Display(Name = "Policy No.")]
        public String PolicyNo { get; set; }

        [Required(ErrorMessage = "Please Enter Insuarance Cost")]
        [Display(Name = "Cost:")]
        public String InsuaranceCost { get; set; }

    }
    #endregion

    #region InsuaranceId Validation
    [MetadataType(typeof(InsuaranceIdValidation))]
    public partial class InsuaranceId
    { }
    public class InsuaranceIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Insuarance")]
        public int InsuaranceId { get; set; }
        public string InsuaranceName { get; set; }
    }
    #endregion

    #region InsuaranceTypeId Validation
    [MetadataType(typeof(InsuaranceTypeIdValidation))]
    public partial class InsuaranceTypeId
    { }
    public class InsuaranceTypeIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Insuarance Type")]
        public int InsuaranceTypeId { get; set; }
        public string InsuaranceTypeName { get; set; }
    }
    #endregion

    #region VehicleLicenceDetail Validation
    [MetadataType(typeof(VehicleLicenceDetailValidation))]
    public partial class VehicleLicenceDetail
    {
        public String PickUpDate
        {
            get
            {
                return String.Format("{0:d}", RenewalDate);
            }
            set { }
        }
        public string VehicleRegCode { get { return Vehicle.RegistrationNo + " (" + Vehicle.NickName + ")"; } }
    }
    public class VehicleLicenceDetailValidation
    {
        [ScaffoldColumn(false)]
        public Int32 VehicleLicenceDetailId { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "Vehicle:")]
        public VehicleId VehicleId { get; set; }

        [Required(ErrorMessage = "Please Select Licence Type")]
        [Display(Name = "Licence Type:")]
        public LicenceTypeId LicenceTypeId { get; set; }

        [Required(ErrorMessage = "Please Select Renewal Date")]
        [Display(Name = "Renewal Date:")]
        [CompareDates("ExpireDate", false, ErrorMessage = "Renewal date cannot be after Expiry & today date")]
        public String RenewalDate { get; set; }

        [Required(ErrorMessage = "Please Enter Expire Date")]
        [Display(Name = "Expiry Date:")]
        [CompareDates("RenewalDate", false, ErrorMessage = "Expiry date cannot be before Renewal date")]
        public String ExpireDate { get; set; }

        [Required(ErrorMessage = "Please Enter Renewal Cost")]
        [Display(Name = "Renewal Cost:")]
        public decimal RenewalCost { get; set; }

        [Required(ErrorMessage = "Please Enter Licence No")]
        [Display(Name = "Licence No:")]
        public String LicenceNo { get; set; }
    }
    #endregion

    #region LicenceTypeId Validation
    [MetadataType(typeof(LicenceTypeIdValidation))]
    public partial class LicenceTypeId
    { }
    public class LicenceTypeIdValidation
    {
        [MustBeSelectedAttribute(ErrorMessage = "Please Select Licence Type")]
        public int Id { get; set; }
        public string LicenceType { get; set; }
    }
    #endregion

    #region start, end date validation
    /// <summary>
    /// Compares two dates to each other, ensuring that one is larger than the other
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CompareDatesAttribute : ValidationAttribute, IClientValidatable
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareDatesAttribute"/> class.
        /// </summary>
        /// <param name="otherPropertyName">Name of the compare to date property.</param>
        /// <param name="allowEquality">if set to <c>true</c> equal dates are allowed.</param>
        public CompareDatesAttribute(string otherPropertyName, //bool startDate = true, 
            bool allowEquality = true)
        {
            AllowEquality = allowEquality;
            OtherPropertyName = otherPropertyName;
            //StartDate = startDate;
        }

        #region Properties

        /// <summary>
        /// Gets the name of the  property to compare to
        /// </summary>
        public string OtherPropertyName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether dates could be the same
        /// </summary>
        public bool AllowEquality { get; private set; }

        /// <summary>
        /// Gets a value indicating whether its the start date
        /// </summary>
        public bool StartDate { get; private set; }

        #endregion

        /// <summary>
        /// Validates the specified value with respect to the current validation attribute.
        /// </summary>
        /// <param name="value">The value to validate.</param>
        /// <param name="validationContext">The context information about the validation operation.</param>
        /// <returns>
        /// An instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationResult"/> class.
        /// </returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var result = ValidationResult.Success;
            var otherValue = validationContext.ObjectType.GetProperty(OtherPropertyName)
                .GetValue(validationContext.ObjectInstance, null);
            if (value != null)
            {
                if (value is DateTime)
                {
                    if (otherValue != null)
                    {
                        if (otherValue is DateTime)
                        {
                            if (!OtherPropertyName.ToLower().Contains("start") && !OtherPropertyName.ToLower().Contains("renewal"))
                            //if (!StartDate)
                            {
                                if ((DateTime)value > (DateTime)otherValue || (DateTime)value < DateTime.Now.Date)
                                {
                                    result = new ValidationResult(ErrorMessage);
                                }
                            }
                            else
                            {
                                if ((DateTime)value < (DateTime)otherValue || (DateTime)value < DateTime.Now.Date)
                                {
                                    result = new ValidationResult(ErrorMessage);
                                }
                            }
                            if ((DateTime)value == (DateTime)otherValue && !AllowEquality)
                            {
                                result = new ValidationResult(ErrorMessage);
                            }
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// When implemented in a class, returns client validation rules for that class.
        /// </summary>
        /// <param name="metadata">The model metadata.</param>
        /// <param name="context">The controller context.</param>
        /// <returns>
        /// The client validation rules for this validator.
        /// </returns>
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = ErrorMessage,
                ValidationType = "comparedates"
            };
            rule.ValidationParameters["otherpropertyname"] = OtherPropertyName;
            rule.ValidationParameters["allowequality"] = AllowEquality ? "true" : "";
            yield return rule;
        }
    }
    #endregion

    #region Price Validation
    [MetadataType(typeof(PriceValidation))]
    public partial class Price
    {

    }
    public class PriceValidation
    {
        [ScaffoldColumn(false)]
        public Int32 PriceId { get; set; }

        [Required(ErrorMessage = "Please Enter Price")]
        [Display(Name = "Price Name.:")]
        public String PriceName { get; set; }

        [Display(Name = "Start Time:")]
        //[DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String StartTime { get; set; }

        [Display(Name = "End Time:")]
        //[DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String EndTime { get; set; }

        [Required(ErrorMessage = "Please Enter Amount")]
        [Display(Name = "Amount(/Km):")]
        public String Amount { get; set; }

        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
    }
    #endregion

    #region AlertType Validation
    [MetadataType(typeof(AlertTypeValidation))]
    public partial class AlertType
    { }
    public class AlertTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 AlertTypeId { get; set; }

        [Required(ErrorMessage = "Please Enter AlertType")]
        [Display(Name = "Alert Type:")]
        public String AlertTypeName { get; set; }

        [Display(Name = "Active:")]
        public Boolean Active { get; set; }
    }
    #endregion

    #region AlertRecipient Validation
    [MetadataType(typeof(AlertRecipientValidation))]
    public partial class AlertRecipient
    { }
    public class AlertRecipientValidation
    {
        [ScaffoldColumn(false)]
        public Int32 AlertRecipientId { get; set; }

        [Required(ErrorMessage = "Please Enter Names")]
        [Display(Name = "Names:")]
        public String Names { get; set; }

        [Required(ErrorMessage = "Please Enter EmailAddress")]
        [Display(Name = "Email Address:")]
        public String EmailAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Telephone")]
        [Display(Name = "Telephone:")]
        public String MobileNo { get; set; }

        [Display(Name = "Receive Email?:")]
        public Boolean ReceiveEmail { get; set; }

        [Display(Name = "Receive SMS?:")]
        public Boolean ReceiveSMS { get; set; }

        [Display(Name = "Active:")]
        public String Active { get; set; }
    }
    #endregion

    #region AlertReceiverAlert Validation
    [MetadataType(typeof(AlertReceiverAlertTypeValidation))]
    public partial class AlertReceiverAlert
    { }
    public class AlertReceiverAlertTypeValidation
    {
        [ScaffoldColumn(false)]
        public Int32 AlertReceiverAlertId { get; set; }

        [Display(Name = "Alert Type:")]
        [Required(ErrorMessage = "Please select AlertType")]
        public AlertTypeId AlertTypeId { get; set; }

        [Display(Name = "Alert Recepient:")]
        [Required(ErrorMessage = "Please select Alert Recepient")]
        public AlertRecipientId AlertRecipientId { get; set; }
    }
    #endregion

    #region AlertTypeId Validation
    [MetadataType(typeof(AlertTypeIdValidation))]
    public partial class AlertTypeId
    { }
    public class AlertTypeIdValidation
    {
        [ScaffoldColumn(false)]
        public Int32 AlertTypeId { get; set; }

        [Required(ErrorMessage = "Please Select AlertType")]
        [Display(Name = "Alert Type:")]
        public String AlertTypeName { get; set; }
    }
    #endregion

    #region AlertRecipientId Validation
    [MetadataType(typeof(AlertRecipientIdValidation))]
    public partial class AlertRecipientId
    { }
    public class AlertRecipientIdValidation
    {
        [ScaffoldColumn(false)]
        public Int32 AlertRecipientId { get; set; }

        [Required(ErrorMessage = "Please Select Alert Recipient")]
        [Display(Name = "Alert Recipient:")]
        public String Names { get; set; }
    }
    #endregion

    public class UserValidation
    {
        [ScaffoldColumn(false)]
        public Guid UserId { get; set; }
        public string FullNames { get; set; }

        [Required(ErrorMessage = "Please Enter First Name")]
        [Display(Name = "First Name:")]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter Last Name")]
        [Display(Name = "Last Name:")]
        public String LastName { get; set; }
        //[Required(ErrorMessage = "Please Enter Email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email1:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email1 { get; set; }
        [Required(ErrorMessage = "Please Enter Phone Number")]
        [Display(Name = "Phone1:")]
        public String Telephone1 { get; set; }
        //[Required(ErrorMessage = "Please Enter Email2")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Other Email:")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email2 { get; set; }
        //[Required(ErrorMessage = "Please Enter Alternative Phone Number")]
        [Display(Name = "Phone2:")]
        public String Telephone2 { get; set; }
        [Display(Name = "Gender:")]
        [Required(ErrorMessage = "Please select Gender")]
        public Boolean IsMale { get; set; }
        //[Required(ErrorMessage = "Please Enter Post Code")]
        [Display(Name = "Post Code:")]
        public String PostCode { get; set; }

        //[Required(ErrorMessage = "Please Enter Fax Number")]
        [Display(Name = "Fax Number:")]
        public String FaxNumber { get; set; }

        //[Required(ErrorMessage = "Please Enter Date Of Birth")]
        [Display(Name = "D.O.B:")]
        public String DOB { get; set; }
        [Display(Name = "IDNumber")]
        [Required(ErrorMessage = "Please Enter IDNumber")]
        public String IDNumber { get; set; }
        //Image ,
        [Display(Name = "NextOfKinName1:")]
        public String NextOfKinName1 { get; set; }
        [Display(Name = "NextOfKinName2:")]
        public String NextOfKinName2 { get; set; }
        [Display(Name = "NextOfKinPhone1:")]
        public String NextOfKinPhone1 { get; set; }
        [Display(Name = "NextOfKinPhone2:")]
        public String NextOfKinPhone2 { get; set; }
        [Display(Name = "Association1:")]
        public String Association1 { get; set; }
        [Display(Name = "Association2:")]
        public String Association2 { get; set; }
        [Display(Name = "NextOfKinEmail1:")]
        public String NextOfKinEmail1 { get; set; }
        [Display(Name = "NextOfKinEmail2:")]
        public String NextOfKinEmail2 { get; set; }
        [Display(Name = "Active:")]
        public Boolean Active { get; set; }

        public int DiscriminatorId { get; set; }

        [Display(Name = "Driver Number:")]
        public String DriverNo { get; set; }

        //[Required(ErrorMessage = "Please Enter Driver RFID")]
        [Display(Name = "Driver RFID:")]
        public String DriverRFID { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}")]
        [Display(Name = "Licence Date")]
        [Required(ErrorMessage = "Please Enter Licence Date")]
        public String LicenceDate { get; set; }

        [Display(Name = "Special Conditions:")]
        public String SpecialConditions { get; set; }

        [Display(Name = "CofCNo:")]
        public String CofCNo { get; set; }

        [Display(Name = "Driver Status:")]
        public int DriverStatusId { get; set; }
    }

}
