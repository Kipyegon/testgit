USE [CarTrack]
GO
/****** Object:  StoredProcedure [dbo].[insertGPS]    Script Date: 04/26/2014 12:30:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[insertGPS]
	@UnitID varchar(20),@SERIAL_NUM varchar(50),@Longitude float,@Latitude float,@GLB_TIME datetime,
	@Speed float,@Time datetime,@Distance bigint,@Course float,@DeviceTypeId int,
	@VehicleId int,@DeviceId int,@DeviceEventId int,@HasLocationData bit,@GPSStatus bit,@Alarm int,
	@SequenceNo bigint,@EngineState bit,@HDOP float,@A2D1 float,@A2D2 float,@A2D3 float,
	@IO_INPUT1 bit,@IO_INPUT2 bit,@IO_INPUT3 bit,@IO_INPUT4 bit,@IO_INPUT5 bit,@IO_INPUT6 bit,
	@IO_INPUT7 bit,@IO_INPUT8 bit,@IO_OUTPUT1 bit,@IO_OUTPUT2 bit,@IO_OUTPUT3 bit,@IO_OUTPUT4 bit,
	@IO_OUTPUT5 bit,@IO_OUTPUT6 bit,@IO_OUTPUT7 bit,@IO_OUTPUT8 bit,@Altitude float,
	@SLNAME varchar(400),@TOWN varchar(400),@STREET varchar(400),@SatellitesUsed int,
	@CELL_MCC varchar(10),@CELL_MNC varchar(10),@CELL_LAC varchar(10),@CELL_ID varchar(10),
	@GSM_RSSI varchar(50),@externalPowerState int,@externalPowerVoltage float,@batteryVoltage float,
	@isGetLocationFailed bit
as
begin
	SET NOCOUNT ON;
	set @UnitID=LTRIM(RTRIM(@UnitID))
    set @SERIAL_NUM=LTRIM(RTRIM(@SERIAL_NUM))
	declare @Direction varchar(10),@imageUrl varchar(10)
	
	set @Direction=dbo.get_direction(@Course)
	set @imageUrl=dbo.get_image(@Course)
	
	if (DATEDIFF(YEAR,getdate(),@time)>1)
	begin
		set @Time=GETDATE()
	end
	
	declare @LOCNAME varchar(50),@DIVNAME varchar(50),
		@DISTNAME varchar(50),@PROVNAME varchar(50),@CONSTITUEN varchar(50)
	
	if (@isGetLocationFailed=1)
	begin
		declare @point varchar(50),@g Geometry
		set @point=	'POINT('+ ltrim(rtrim(Str(@Longitude,25,16)))+' '+ ltrim(rtrim(Str(@Latitude,25,16)))+')'
		
		SET @g = Geometry::STGeomFromText(@point,4326)			
		set @STREET=dbo.GetRoad(@g)
		set @town=dbo.GetTown(@g)
		select @SLNAME=SLNAME,@LOCNAME=LOCNAME,@DIVNAME=DIVNAME,@DISTNAME=DISTNAME,
			@PROVNAME=PROVNAME,@CONSTITUEN=CONSTITUEN from dbo.GetAdministrativeLoc(@g)
	end
	
	INSERT INTO [CarTrack].[dbo].[GPSLatestLog]([UnitID],[SERIAL_NUM],[Longitude],[Latitude],
			[GLB_TIME],[Speed],[Time],[Distance],[Course],[Direction],[DeviceTypeId],
			[VehicleId],[DeviceId],[DeviceEventId],[HasLocationData],[GPSStatus],[Alarm],
			[SequenceNo],[EngineState],[HDOP],[A2D1],[A2D2],[A2D3],[IO_INPUT1],[IO_INPUT2],
			[IO_INPUT3],[IO_INPUT4],[IO_INPUT5],[IO_INPUT6],[IO_INPUT7],[IO_INPUT8],[IO_OUTPUT1],
			[IO_OUTPUT2],[IO_OUTPUT3],[IO_OUTPUT4],[IO_OUTPUT5],[IO_OUTPUT6],[IO_OUTPUT7],
			[IO_OUTPUT8],[Altitude],[SLNAME],[LOCNAME],[DIVNAME],[DISTNAME],[PROVNAME],[CONSTITUEN],
			[TOWN],[STREET],[ImageUrl],[SatellitesUsed],[CELL_MCC],[CELL_MNC],[CELL_LAC],[CELL_ID],
			[GSM_RSSI],[externalPowerState],[externalPowerVoltage],[batteryVoltage])
		select @UnitID,@SERIAL_NUM,@Longitude,@Latitude,@GLB_TIME,@Speed,@Time,
			@Distance,@Course,@Direction,@DeviceTypeId,@VehicleId,@DeviceId,@DeviceEventId,
			@HasLocationData,@GPSStatus,@Alarm,@SequenceNo,@EngineState,@HDOP,@A2D1,@A2D2,
			@A2D3,@IO_INPUT1,@IO_INPUT2,@IO_INPUT3,@IO_INPUT4,@IO_INPUT5,@IO_INPUT6,
			@IO_INPUT7,@IO_INPUT8,@IO_OUTPUT1,@IO_OUTPUT2,@IO_OUTPUT3,@IO_OUTPUT4,@IO_OUTPUT5,
			@IO_OUTPUT6,@IO_OUTPUT7,@IO_OUTPUT8,@Altitude,@SLNAME,@LOCNAME,@DIVNAME,
			@DISTNAME,@PROVNAME,@CONSTITUEN,@TOWN,@STREET,@ImageUrl,@SatellitesUsed,
			@CELL_MCC,@CELL_MNC,@CELL_LAC,@CELL_ID,@GSM_RSSI,@externalPowerState,
			@externalPowerVoltage,@batteryVoltage
	
	--select 1	
end

/*
	select top 1 * from gpslog order by id desc
	select top 10 * from dbo.GPSLatestLog order by id desc
	select top 10 * from dbo.GPSHistoryLog order by id desc
	
	
	EXEC [dbo].[insertGPS]
		@UnitID = N'234567',
		@SERIAL_NUM = N'234567',
		@Longitude = 36.812442779541,
		@Latitude = -1.23415994644165,
		@GLB_TIME = NULL,
		@Speed = 30,
		@Time = N'2014-04-08 12:08:30.157',
		@Distance = 46453,
		@Course = 56,
		@DeviceTypeId = 1,
		@VehicleId = 34,
		@DeviceId = 145,
		@DeviceEventId = 110,
		@HasLocationData = 1,
		@GPSStatus = 1,
		@Alarm = 0,
		@SequenceNo = NULL,
		@EngineState = 1,
		@HDOP = 2345,
		@A2D1 = 0.0,
		@A2D2 = NULL,
		@A2D3 = NULL,
		@IO_INPUT1 = 1,
		@IO_INPUT2 = NULL,
		@IO_INPUT3 = NULL,
		@IO_INPUT4 = NULL,
		@IO_INPUT5 = NULL,
		@IO_INPUT6 = NULL,
		@IO_INPUT7 = NULL,
		@IO_INPUT8 = NULL,
		@IO_OUTPUT1 = NULL,
		@IO_OUTPUT2 = NULL,
		@IO_OUTPUT3 = NULL,
		@IO_OUTPUT4 = NULL,
		@IO_OUTPUT5 = NULL,
		@IO_OUTPUT6 = NULL,
		@IO_OUTPUT7 = NULL,
		@IO_OUTPUT8 = NULL,
		@Altitude = 1476,
		@SLNAME = N' Subukia',
		@TOWN = N' Subukia / Subukia',
		@STREET = N'B5',
		@SatellitesUsed = 10,
		@CELL_MCC = NULL,
		@CELL_MNC = NULL,
		@CELL_LAC = NULL,
		@CELL_ID = NULL,
		@GSM_RSSI = NULL,
		@externalPowerState = 1,
		@externalPowerVoltage = 12.45,
		@batteryVoltage = 6.76,
		@isGetLocationFailed = 1
*/