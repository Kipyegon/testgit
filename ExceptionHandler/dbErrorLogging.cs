﻿using System;
using System.Diagnostics;
using System.Web;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExceptionHandler
{
    public class LogObject
    {
        public Exception oEx { get; set; }
        public HttpContext ctxObject { get; set; }
    }

    public class dbErrorLogging
    {
        public dbErrorLogging()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public static void LogError(Exception oEx, HttpContext ctxObject)
        {
            LogObject data = new LogObject() { ctxObject = ctxObject, oEx = oEx };
            Thread thread = new Thread(new ParameterizedThreadStart(new dbErrorLogging()._LogError));
            thread.Start((data));
        }

        private void _LogError(object callback)
        {
            LogObject data = (LogObject)callback;
            bool blLogCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLog"].ToString());
            bool blLogEmailCheck = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableLogEmail"].ToString());
            if (blLogCheck)
            {
                HandleException(data.oEx, data.ctxObject);
            }
            if (blLogEmailCheck)
            {
                SendExceptionMail(data.oEx);
            }
        }

        public static void HandleException(Exception ex, HttpContext ctxObject)
        {
            //HttpContext ctxObject = HttpContext.Current;
            string strLogConnString = System.Configuration.ConfigurationManager.AppSettings["dbErrorLog"].ToString();
            string logDateTime = DateTime.Now.ToString("g");
            string strReqURL = String.Empty;

            try
            {
                strReqURL = (ctxObject != null && ctxObject.Request != null && ctxObject.Request.Url != null) ? ctxObject.Request.Url.ToString() : String.Empty;
            }
            catch (Exception _ex) { }
            string strReqQS = String.Empty;
            try
            {
                strReqQS = (ctxObject != null && ctxObject.Request != null && ctxObject.Request.QueryString != null) ? ctxObject.Request.QueryString.ToString() : String.Empty;
            }
            catch (Exception _ex) { }

            string strServerName = String.Empty;
            try
            {
                if (ctxObject != null && ctxObject.Request != null && ctxObject.Request.ServerVariables["HTTP_REFERER"] != null)
                {
                    strServerName = ctxObject.Request.ServerVariables["HTTP_REFERER"].ToString();
                }
            }
            catch (Exception _ex) { }
            string strUserAgent = String.Empty;
            try
            {
                strUserAgent = (ctxObject != null && ctxObject.Request != null && ctxObject.Request.UserAgent != null) ? ctxObject.Request.UserAgent : String.Empty;
            }
            catch (Exception _ex) { }
            string strUserIP = String.Empty;
            try
            {
                strUserIP = (ctxObject != null && ctxObject.Request != null && ctxObject.Request.UserHostAddress != null) ? ctxObject.Request.UserHostAddress : String.Empty;
            }
            catch (Exception _ex) { }
            string strUserAuthen = (ctxObject != null && ctxObject.User != null && ctxObject.User.Identity.IsAuthenticated.ToString() != null) ? ctxObject.User.Identity.IsAuthenticated.ToString() : String.Empty;
            
            string strUserName = (ctxObject != null && ctxObject.User != null && ctxObject.User.Identity.Name != null) ? ctxObject.User.Identity.Name : String.Empty;
            string strMessage = string.Empty, strSource = string.Empty, strTargetSite = string.Empty, strStackTrace = string.Empty;
            while (ex != null)
            {
                strMessage += string.IsNullOrEmpty(strMessage) ? ex.Message : " ------ " + ex.Message;
                strSource += string.IsNullOrEmpty(strSource) ? ex.Source : " ------ " + ex.Source;
                
                strTargetSite += string.IsNullOrEmpty(strTargetSite) ? ex.TargetSite != null ? ex.TargetSite.ToString() : " ------ " + ex.TargetSite.ToString() : "";
                strStackTrace += string.IsNullOrEmpty(strStackTrace) ? ex.StackTrace != null ? ex.StackTrace : " ------ " + ex.StackTrace : "";
                ex = ex.InnerException;
            }
            if (strLogConnString.Length > 0)
            {
                SqlCommand strSqlCmd = new SqlCommand();
                strSqlCmd.CommandType = CommandType.StoredProcedure;
                strSqlCmd.CommandText = "sp_LogExceptionToDB";
                SqlConnection sqlConn = new SqlConnection(strLogConnString);
                strSqlCmd.Connection = sqlConn;
                sqlConn.Open();
                try
                {
                    strSqlCmd.Parameters.Add(new SqlParameter("@Source", strSource));
                    strSqlCmd.Parameters.Add(new SqlParameter("@LogDateTime", logDateTime));
                    strSqlCmd.Parameters.Add(new SqlParameter("@Message", strMessage));
                    strSqlCmd.Parameters.Add(new SqlParameter("@QueryString", strReqQS));
                    strSqlCmd.Parameters.Add(new SqlParameter("@TargetSite", strTargetSite));
                    strSqlCmd.Parameters.Add(new SqlParameter("@StackTrace", strStackTrace));
                    strSqlCmd.Parameters.Add(new SqlParameter("@ServerName", strServerName));
                    strSqlCmd.Parameters.Add(new SqlParameter("@RequestURL", string.IsNullOrEmpty(strReqURL) ? "" : strReqURL));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserAgent", strUserAgent));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserIP", strUserIP));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserAuthentication", strUserAuthen));
                    strSqlCmd.Parameters.Add(new SqlParameter("@UserName", strUserName));
                    SqlParameter outParm = new SqlParameter("@EventId", SqlDbType.Int);
                    outParm.Direction = ParameterDirection.Output;
                    strSqlCmd.Parameters.Add(outParm);
                    strSqlCmd.ExecuteNonQuery();
                    strSqlCmd.Dispose();
                    sqlConn.Close();
                }
                catch (Exception exc)
                {
                    EventLog.WriteEntry(exc.Source, "Database Error From Exception Log!", EventLogEntryType.Error, 65535);
                }
                finally
                {
                    strSqlCmd.Dispose();
                    sqlConn.Close();
                }
            }
        }
        protected static string FormatExceptionDescription(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            HttpContext context = HttpContext.Current;
            if (context != null)
            {
                sb.Append("<b>Time of Error: </b>" + DateTime.Now.ToString("g") + "<br />");
                sb.Append("<b>URL:</b> " + context.Request.Url + "<br />");
                sb.Append("<b>QueryString: </b> " + context.Request.QueryString.ToString() + "<br />");
                sb.Append("<b>Server Name: </b> " + context.Request.ServerVariables["SERVER_NAME"] + "<br />");
                sb.Append("<b>User Agent: </b>" + context.Request.UserAgent + "<br />");
                sb.Append("<b>User IP: </b>" + context.Request.UserHostAddress + "<br />");
                sb.Append("<b>User Host Name: </b>" + context.Request.UserHostName + "<br />");
                sb.Append("<b>User is Authenticated: </b>" + context.User.Identity.IsAuthenticated.ToString() + "<br />");
                sb.Append("<b>User Name: </b>" + context.User.Identity.Name + "<br />");
            }
            while (e != null)
            {
                sb.Append("<b>Message: </b>" + e.Message + "<br />");
                sb.Append("<b> Source: </b>" + e.Source + "<br />");
                sb.Append("<b>TargetSite: </b>" + e.TargetSite + "<br />");
                sb.Append("<b>StackTrace: </b>" + e.StackTrace + "<br />");
                sb.Append(Environment.NewLine + "<br />");
                e = e.InnerException;
            }
            sb.Append("--------------------------------------------------------" + "<br />");
            sb.Append("Regards," + "<br />");
            sb.Append("Admin");
            return sb.ToString();
        }
        public static void SendExceptionMail(Exception e)
        {
            string strEmails = System.Configuration.ConfigurationManager.AppSettings["LogToEmail"].ToString();
            string Subject = System.Configuration.ConfigurationManager.AppSettings["LogToSubject"].ToString();
            string emailFrom = System.Configuration.ConfigurationManager.AppSettings["LogFromEmail"].ToString();

            string mailServer = System.Configuration.ConfigurationManager.AppSettings["mailServer"].ToString();
            string emailFromPassword = System.Configuration.ConfigurationManager.AppSettings["emailFromPassword"].ToString();

            if (strEmails.Length > 0)
            {
                string[] arEmails = strEmails.Contains('|') ? strEmails.Split(Convert.ToChar("|")) : strEmails.Split(',');
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.To.Add(new MailAddress(arEmails[0]));
                for (int i = 1; i < arEmails.Length; i++)
                    mail.CC.Add(new MailAddress(arEmails[i]));

                mail.From = new MailAddress(emailFrom);
                mail.Subject = Subject + DateTime.Now + " !";
                string sExceptionDescription = FormatExceptionDescription(e);
                mail.Body = sExceptionDescription;

                SmtpClient smtp = new SmtpClient(mailServer);
                NetworkCredential credential = new NetworkCredential(emailFrom, emailFromPassword);
                smtp.Credentials = credential;
                try
                {
                    smtp.Send(mail);
                }
                catch (Exception _ex)
                {
                    EventLog.WriteEntry(_ex.Source, "Send Email Error From Exception Log!", EventLogEntryType.Error, 65535);
                }
            }
            else
            {
                return;
            }
        }
    }
}
