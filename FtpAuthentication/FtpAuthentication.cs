﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Web.FtpServer;
using System.Linq;
using System.Web;
using System.Web.Security;

using DataAccess.SQL;

namespace FtpAuthentication
{
    public class FtpAuthentication : BaseProvider,
        IFtpAuthenticationProvider,
        IFtpRoleProvider,
        IFtpHomeDirectoryProvider
    {
        // Create strings to store the paths to the XML files that store the user and role data.
        //private string _xmlUsersFileName;
        //private string _xmlRolesFileName;

        // Create a string to store the FTP home directory path.
        //private string _ftpHomeDirectory;

        // Create a file system watcher object for change notifications.
        //private FileSystemWatcher _xmlFileWatch;

        // Create a dictionary to hold user data.
        //private Dictionary<string, XmlUserData> _XmlUserData =
        //  new Dictionary<string, XmlUserData>(
        //    StringComparer.InvariantCultureIgnoreCase);

        // Override the Initialize method to retrieve the configuration settings.
        //protected override void Initialize(StringDictionary config)
        //{
        //     Retrieve the paths from the configuration dictionary.
        //    _xmlUsersFileName = config[@"xmlUsersFileName"];
        //    _xmlRolesFileName = config[@"xmlRolesFileName"];
        //    _ftpHomeDirectory = config[@"ftpHomeDirectory"];

        //     Test if the path to the users or roles XML file is empty.
        //    if ((string.IsNullOrEmpty(_xmlUsersFileName)) || (string.IsNullOrEmpty(_xmlRolesFileName)))
        //    {
        //         Throw an exception if the path is missing or empty.
        //        throw new ArgumentException(@"Missing xmlUsersFileName or xmlRolesFileName value in configuration.");
        //    }
        //    else
        //    {
        //         Test if the XML files exist.
        //        if ((File.Exists(_xmlUsersFileName) == false) || (File.Exists(_xmlRolesFileName) == false))
        //        {
        //             Throw an exception if the file does not exist.
        //            throw new ArgumentException(@"The specified XML file does not exist.");
        //        }
        //    }

        //    try
        //    {
        //         Create a file system watcher object for the XML file.
        //        _xmlFileWatch = new FileSystemWatcher();
        //         Specify the folder that contains the XML file to watch.
        //        _xmlFileWatch.Path = _xmlUsersFileName.Substring(0, _xmlUsersFileName.LastIndexOf(@"\"));
        //         Filter events based on the XML file name.
        //        _xmlFileWatch.Filter = @"*.xml";
        //         Filter change notifications based on last write time and file size.
        //        _xmlFileWatch.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.Size;
        //         Add the event handler.
        //        _xmlFileWatch.Changed += new FileSystemEventHandler(this.XmlFileChanged);
        //         Enable change notification events.
        //        _xmlFileWatch.EnableRaisingEvents = true;
        //    }
        //    catch (Exception ex)
        //    {
        //         Raise an exception if an error occurs.
        //        throw new ProviderException(ex.Message, ex.InnerException);
        //    }
        //}

        // Define the event handler for changes to the XML files.
        //public void XmlFileChanged(object sender, FileSystemEventArgs e)
        //{
        //    // Verify that the changed file is one of the XML data files.
        //    if ((e.FullPath.Equals(_xmlUsersFileName,
        //        StringComparison.OrdinalIgnoreCase)) ||
        //        (e.FullPath.Equals(_xmlRolesFileName,
        //        StringComparison.OrdinalIgnoreCase)))
        //    {
        //        // Clear the contents of the existing user dictionary.
        //        _XmlUserData.Clear();
        //        // Repopulate the user dictionary.
        //        ReadXmlDataStore();
        //    }
        //}

        // Override the Dispose method to dispose of objects.
        protected override void Dispose(bool IsDisposing)
        {
            if (IsDisposing)
            {
                //_xmlFileWatch.Dispose();
                //_XmlUserData.Clear();
            }
        }

        // Define the AuthenticateUser method.
        bool IFtpAuthenticationProvider.AuthenticateUser(
               string sessionId,
               string siteName,
               string userName,
               string userPassword,
               out string canonicalUserName)
        {
            // Define the canonical user name.
            canonicalUserName = userName;

            // Validate that the user name and password are not empty.
            if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(userPassword))
            {
                // Return false (authentication failed) if either are empty.
                return false;
            }
            else
            {
                try
                {
                    // Retrieve the user/role data from the XML file.
                    //ReadXmlDataStore();
                    //// Create a user object.
                    //XmlUserData user = null;
                    //// Test if the user name is in the dictionary of users.
                    //if (_XmlUserData.TryGetValue(userName, out user))
                    //{
                    //    // Retrieve a sequence of bytes for the password.
                    //    var passwordBytes = Encoding.UTF8.GetBytes(userPassword);
                    //    // Retrieve a SHA256 object.
                    //    using (HashAlgorithm sha256 = new SHA256Managed())
                    //    {
                    //        // Hash the password.
                    //        sha256.TransformFinalBlock(passwordBytes, 0, passwordBytes.Length);
                    //        // Convert the hashed password to a Base64 string.
                    //        string passwordHash = Convert.ToBase64String(sha256.Hash);
                    //        // Perform a case-insensitive comparison on the password hashes.
                    //        if (String.Compare(user.Password, passwordHash, true) == 0)
                    //        {
                    //            // Return true (authentication succeeded) if the hashed passwords match.
                    //            return true;
                    //        }
                    //    }
                    //}

                    MembershipProvider pmp = Membership.Provider;
                    return pmp.ValidateUser(userName, userPassword);

                }
                catch (Exception ex)
                {
                    // Raise an exception if an error occurs.
                    throw new ProviderException(ex.Message, ex.InnerException);
                }
            }
            // Return false (authentication failed) if authentication fails to this point.
            //return false;
        }

        // Define the IsUserInRole method.
        bool IFtpRoleProvider.IsUserInRole(
             string sessionId,
             string siteName,
             string userName,
             string userRole)
        {
            // Validate that the user and role names are not empty.
            if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(userRole))
            {
                // Return false (role lookup failed) if either are empty.
                return false;
            }
            else
            {
                try
                {
                    // Retrieve the user/role data from the XML file.
                    //ReadXmlDataStore();
                    //// Create a user object.
                    //XmlUserData user = null;
                    //// Test if the user name is in the dictionary of users.
                    //if (_XmlUserData.TryGetValue(userName, out user))
                    //{
                    //    // Search for the role in the list.
                    //    string roleFound = user.Roles.Find(item => item == userRole);
                    //    // Return true (role lookup succeeded) if the role lookup was successful.
                    //    if (!String.IsNullOrEmpty(roleFound)) return true;
                    //}
                    return Roles.IsUserInRole(userRole);
                }
                catch (Exception ex)
                {
                    // Raise an exception if an error occurs.
                    throw new ProviderException(ex.Message, ex.InnerException);
                }
            }
            // Return false (role lookup failed) if role lookup fails to this point.
            //return false;
        }

        // Define the GetUserHomeDirectoryData method.
        public string GetUserHomeDirectoryData(string sessionId, string siteName, string userName)
        {
            // Test if the path to the home directory is empty.
            //if (string.IsNullOrEmpty(_ftpHomeDirectory))
            //{
            //    // Throw an exception if the path is missing or empty.
            //    throw new ArgumentException(@"Missing ftpHomeDirectory value in configuration.");
            //}
            string ftpHomeDirectory = System.Configuration.ConfigurationManager.AppSettings["ftpHomeDirectory"].ToString();
            string foldername;

            //using (GFleetModelContainer gfleet = new GFleetModelContainer())
            //{
            foldername = getClientBrandName(userName);
            //}

            // Return the path to the home directory.
            return ftpHomeDirectory + foldername;
        }

        public string getClientBrandName(string userName)
        {
            MembershipProvider pmp = Membership.Provider;
            object UserId = pmp.GetUser(userName, false).ProviderUserKey;
            string brandName="";
            if (UserId != null)
            {
                GFleetModelContainer entities = new GFleetModelContainer();
                Guid userId = Guid.Parse(UserId.ToString());
                Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                if (per != null)
                {
                    int? clientId = per.ClientId;
                    brandName = entities.Clients.SingleOrDefault(s => s.ClientId == clientId).ClientName;
                }                
            }
            return String.IsNullOrEmpty(brandName) ? "unKnown" : brandName;
        }

        // Retrieve the user/role data from the XML files.
        //private void ReadXmlDataStore()
        //{
        //    // Lock the provider while the data is retrieved.
        //    lock (this)
        //    {
        //        try
        //        {
        //            // Test if the dictionary already has data.
        //            if (_XmlUserData.Count == 0)
        //            {
        //                // Create an XML document object and load the user data XML file
        //                XPathDocument xmlUsersDocument = GetXPathDocument(_xmlUsersFileName);
        //                // Create a navigator object to navigate through the XML file.
        //                XPathNavigator xmlNavigator = xmlUsersDocument.CreateNavigator();
        //                // Loop through the users in the XML file.
        //                foreach (XPathNavigator userNode in xmlNavigator.Select("/Users/User"))
        //                {
        //                    // Retrieve a user name.
        //                    string userName = GetInnerText(userNode, @"UserName");
        //                    // Retrieve the user's password.
        //                    string password = GetInnerText(userNode, @"Password");
        //                    // Test if the data is empty.
        //                    if ((String.IsNullOrEmpty(userName) == false) && (String.IsNullOrEmpty(password) == false))
        //                    {
        //                        // Create a user data class.
        //                        XmlUserData userData = new XmlUserData(password);
        //                        // Store the user data in the dictionary.
        //                        _XmlUserData.Add(userName, userData);
        //                    }
        //                }

        //                // Create an XML document object and load the role data XML file
        //                XPathDocument xmlRolesDocument = GetXPathDocument(_xmlRolesFileName);
        //                // Create a navigator object to navigate through the XML file.
        //                xmlNavigator = xmlRolesDocument.CreateNavigator();
        //                // Loop through the roles in the XML file.
        //                foreach (XPathNavigator roleNode in xmlNavigator.Select(@"/roles/role"))
        //                {
        //                    // Retrieve a role name.
        //                    string roleName = GetInnerText(roleNode, @"name");
        //                    // Loop through the users for the role.
        //                    foreach (XPathNavigator userNode in roleNode.Select(@"users/user"))
        //                    {
        //                        // Retrieve a user name.
        //                        string userName = userNode.Value;
        //                        // Create a user object.
        //                        XmlUserData user = null;
        //                        // Test if the user name is in the dictionary of users.
        //                        if (_XmlUserData.TryGetValue(userName, out user))
        //                        {
        //                            // Add the role name for the user.
        //                            user.Roles.Add(roleName);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            // Raise an exception if an error occurs.
        //            throw new ProviderException(ex.Message, ex.InnerException);
        //        }
        //    }
        //}

        // Retrieve an XPathDocument object from a file path.
        private static XPathDocument GetXPathDocument(string path)
        {
            Exception _ex = null;
            // Specify number of attempts to create an XPathDocument.
            for (int i = 0; i < 8; ++i)
            {
                try
                {
                    // Create an XPathDocument object and load the user data XML file
                    XPathDocument xPathDocument = new XPathDocument(path);
                    // Return the XPathDocument if successful. 
                    return xPathDocument;
                }
                catch (Exception ex)
                {
                    // Save the exception for later.
                    _ex = ex;
                    // Pause for a brief interval.
                    System.Threading.Thread.Sleep(250);
                }
            }
            // Throw the last exception if the function fails to this point.
            throw new ProviderException(_ex.Message, _ex.InnerException);
        }

        // Retrieve data from an XML element.
        private static string GetInnerText(XPathNavigator xmlNode, string xmlElement)
        {
            string xmlText = string.Empty;
            try
            {
                // Test if the XML element exists.
                if (xmlNode.SelectSingleNode(xmlElement) != null)
                {
                    // Retrieve the text in the XML element.
                    xmlText = xmlNode.SelectSingleNode(xmlElement).Value.ToString();
                }
            }
            catch (Exception ex)
            {
                // Raise an exception if an error occurs.
                throw new ProviderException(ex.Message, ex.InnerException);
            }
            // Return the element text.
            return xmlText;
        }
    }

    // Define the user data class.
    internal class XmlUserData
    {
        // Create a private string to hold a user's password.
        private string _password = string.Empty;
        // Create a private string array to hold a user's roles.
        private List<String> _roles = null;

        // Define the class constructor requiring a user's password.
        public XmlUserData(string Password)
        {
            this.Password = Password;
            this.Roles = new List<String>();
        }

        // Define the password property.
        public string Password
        {
            get { return _password; }
            set
            {
                try { _password = value; }
                catch (Exception ex)
                {
                    throw new ProviderException(ex.Message, ex.InnerException);
                }
            }
        }

        // Define the roles property.
        public List<String> Roles
        {
            get { return _roles; }
            set
            {
                try { _roles = value; }
                catch (Exception ex)
                {
                    throw new ProviderException(ex.Message, ex.InnerException);
                }
            }
        }
    }
}