﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using GFleetV3.Models;
using System.Net.Mail;
using GFleetV3.Service;
using System.Web.Configuration;
using Recaptcha;
using Newtonsoft.Json;

//using DotNetOpenAuth.Messaging;
//using DotNetOpenAuth.OpenId;
//using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
//using DotNetOpenAuth.OpenId.RelyingParty;
//using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;

namespace GFleetV3.Controllers
{
    //Felix---code edit//
    //public class CustomMembershipProvider : MembershipProvider
    //{
    //    public override MembershipUser CreateUser(string username,
    //       string password, string email, string passwordQuestion,
    //       string passwordAnswer, bool isApproved,
    //       object providerUserKey, out MembershipCreateStatus status)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override MembershipUser GetUser(string username, bool userIsOnline)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override bool ValidateUser(string username, string password)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override int MinRequiredPasswordLength
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override bool RequiresUniqueEmail
    //    {
    //        get { throw new NotImplementedException(); }
    //    }
    //}
    //end<Felix-code edit//
    public class AccountController : Controller
    {
        //private static OpenIdRelyingParty openid = new OpenIdRelyingParty();

        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        private void bindRoles(UserRole usr)
        {
            string[] roles = System.Web.Security.Roles.GetAllRoles();
            List<UserRole> _role = new List<UserRole>();
            _role.Add(new UserRole { RoleName = null, Role = "Select" });

            foreach (string role in roles)
            {
                _role.Add(new UserRole { RoleName = role, Role = role });
            }

            if (usr == null)
            {
                _role.Single(v => v.RoleName == null).Selected = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(usr.RoleName))
                    _role.SingleOrDefault(v => v.Role == usr.RoleName).Selected = true;
            }
            ViewBag.UserRole = _role;
        }

        // **************************************
        // URL: /Account/LogOn
        // **************************************

        public ActionResult LogOn()
        {
            return View();
        }

        public ActionResult SMSLogOn()
        {
            return View();
        }


        //[CaptchaMvc.Attributes.CaptchaVerify("Captcha Is Not valid")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[RecaptchaControlMvc.CaptchaValidator]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            FormsService.SignOut();
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            var response = Request["g-recaptcha-response"];
            //secret that was generated in key value pair
            const string secret = "6LcQ2wUTAAAAAJnrZ7iCWNbQWJIsKYaqsbRdQXp-";

            var client = new System.Net.WebClient();
            var reply =client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0) return View();

                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        ModelState.AddModelError("", "The captcha secret parameter is missing.");
                        break;
                    case ("invalid-input-secret"):
                        ModelState.AddModelError("", "The captcha secret parameter is invalid or malformed.");
                        break;

                    case ("missing-input-response"):
                        ModelState.AddModelError("", "The captcha response parameter is missing.");
                        break;
                    case ("invalid-input-response"):
                        ModelState.AddModelError("", "The captcha response parameter is invalid or malformed.");
                        break;

                    default:
                        ModelState.AddModelError("", "Error occured. Please try again");
                        break;
                }
            }
            else
            {
                if (ModelState.IsValid)
                {
                    //if(captchaValid)
                    //{
                    Boolean uservalid = Membership.ValidateUser(model.UserName, model.Password);
                    if (uservalid == false)
                    {
                        bool successfulUnlock = AutoUnlockUser(model.UserName);
                        if (successfulUnlock)
                        {
                            FormsService.SignIn(model.UserName, model.RememberMe);
                            if (Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "ClientSelect");
                            }
                        }
                    }
                    else //if (uservalid)
                    {
                        string[] InvalidLoginRoles = WebConfigurationManager.AppSettings["InvalidLoginRoles"].Split(',');

                        foreach (string role in Roles.GetRolesForUser(model.UserName))
                        {
                            if (InvalidLoginRoles.Contains(role))
                            {
                                ModelState.AddModelError("", "User not authorized on this platform.");
                                return View(model);
                            }
                        }

                        FormsService.SignIn(model.UserName, model.RememberMe);
                        if (Url.IsLocalUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "ClientSelect");
                        }
                    }

                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    //}
                    //ModelState.AddModelError("", captchaErrorMessage);
                }
            }


            // If we got this far, something failed, redisplay form
            return View(model);
        }



        private bool AutoUnlockUser(string username)
        {
            string _passwordLockoutMinutes = System.Configuration.ConfigurationManager.AppSettings["PasswordLockoutMinutes"];
            MembershipUser mu = Membership.GetUser(username, false);
            if ((mu != null) && (mu.IsLockedOut) &&
                (mu.LastLockoutDate.ToUniversalTime().AddMinutes(int.Parse(_passwordLockoutMinutes)) < DateTime.UtcNow))
            {
                bool retval = mu.UnlockUser();
                if (retval)
                    return true;
                else
                    return false;    //something went wrong with the unlock
            }
            else
                return false;       //not locked out in the first place
            //or still in lockout period
        }
        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public ActionResult LogOff()
        {
            FormsService.SignOut();
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
            //if (url.Contains(AccessUrls.ToString()))
            if (AccessUrls.Contains(url))
            {
                return RedirectToAction("Logon", "Account"); // new { returnUrl = Request.Url.PathAndQuery });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // **************************************
        // URL: /Account/Register
        //    Administrator
        //    Company Admin
        //    Company Staff
        //    Super Admin
        // **************************************

        [Authorize]
       [Authorization]
        public ActionResult Register(string OpenID)
        {
            Boolean isSystemRightsRoles = Utility.isAdmin();

            if (isSystemRightsRoles)
            {
                ViewBag.PasswordLength = MembershipService.MinPasswordLength;
                ViewBag.OpenID = OpenID;
                bindRoles(null);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }
        }

        [Authorize]
        [Authorization]
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            Boolean isSystemRightsRoles = Utility.isAdmin();

            if (isSystemRightsRoles)
            {
                if (ModelState.IsValid)
                {
                    // Attempt to register the user
                    MembershipCreateStatus createStatus = MembershipService.CreateUser(model.Email, model.Password, model.Email, model.OpenID);

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        Roles.AddUserToRole(model.Email, model.UserRole.RoleName);
                        FormsService.SignIn(model.Email, false /* createPersistentCookie */);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                    }
                }

                // If we got this far, something failed, redisplay form
                ViewBag.PasswordLength = MembershipService.MinPasswordLength;
                bindRoles(model.UserRole);
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {//model.NewPassword
                    string emailid = Membership.GetUser(User.Identity.Name).Email;
                    string subject = "Password reset Successful";
                    var resetLink = "<a href='" + Url.Action("logOn", "Account") + "'>Login</a>";
                    string body = "Your Password has been changed to: <b>" + model.NewPassword + "</b><br/>";
                    body += "Please login " + resetLink;
                    try
                    {
                        SendEmail.SendUserMngtEmail(emailid, subject, body);
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
                    }

                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************
        [Authorize]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult Authenticate(string returnUrl)
        {
            //var response = openid.GetResponse();
            //if (response == null)
            //{
            //Let us submit the request to OpenID provider
            //Identifier id;
            //if (Identifier.TryParse(Request.Form["openid_identifier"], out id))
            //{
            //    try
            //    {
            //        var request = openid.CreateRequest(Request.Form["openid_identifier"]);
            //        return request.RedirectingResponse.AsActionResult();
            //    }
            //    catch (ProtocolException ex)
            //    {
            //        ViewBag.Message = ex.Message;
            //        return View("LogOn");
            //    }
            //}

            ViewBag.Message = "Invalid identifier";
            return View("LogOn");
            // }

            //Let us check the response
            //switch (response.Status)
            //{

            //    case AuthenticationStatus.Authenticated:
            //        LogOnModel lm = new LogOnModel();
            //        lm.OpenID = response.ClaimedIdentifier;
            //        //check if user exist
            //        MembershipUser user = MembershipService.GetUser(lm.OpenID);
            //        if (user != null)
            //        {
            //            lm.UserName = user.UserName;
            //            FormsService.SignIn(user.UserName, false);
            //        }

            //        return View("LogOn", lm);

            //    case AuthenticationStatus.Canceled:
            //        ViewBag.Message = "Canceled at provider";
            //        return View("LogOn");
            //    case AuthenticationStatus.Failed:
            //        ViewBag.Message = response.Exception.Message;
            //        return View("LogOn");
            //}

            //return new EmptyResult();
        }


        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string UserName)
        {
            //check user existence
            var user = this.getUser(UserName);

            if (user == null)
            {
                TempData["Message"] = "Please enter your Username....";
            }
            else
            {

                //generate password token
                var token = SendEmail.HashResetParams(UserName, user.ProviderUserKey.ToString());

                //create url with above token
                var resetLink = "<a href='" + Url.Action("ResetPassword", "Account", new { username = UserName, reset = token }, "http") + "'>Reset Password</a>";

                var emailid = user.Email;


                //send mail
                string subject = "Password Reset Token";
                string body = "<b>Please click the link below to find your Password Reset Token</b><br/>" + resetLink; //edit it
                try
                {
                    SendEmail.SendUserMngtEmail(emailid, subject, body);
                    TempData["Message"] = "Your Password reset token has been sent to your email. Check your mail for more instructions.";
                }
                catch (Exception ex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
                    TempData["Message"] = "Error occured while sending email." + ex.Message;
                    return View();
                }

            }

            return RedirectToAction("LogOn");
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string reset, string username)
        {
            if ((reset != null) && (username != null))
            {
                MembershipUser currentUser = this.getUser(username);
                if (SendEmail.HashResetParams(currentUser.UserName, currentUser.ProviderUserKey.ToString()) == reset)
                {
                    //if (currentUser.ChangePassword(currentUser.ResetPassword(), newPass))
                    //{
                    //    success = "Password for user " + username + " successfully changed!";
                    //}

                    currentUser.UnlockUser();

                    string newpass = currentUser.ResetPassword();
                    ViewBag.newPass = newpass;
                    ViewBag.userName = username;

                    string emailid = currentUser.Email;
                    string subject = "Password reset";
                    var resetLink = "<a href='" + Url.Action("logOn", "Account") + "'>Login</a>";
                    string body = "Your Password has been changed to: <b>" + newpass + "</b><br/>";
                    body += "Please login " + resetLink;
                    SendEmail.SendUserMngtEmail(emailid, subject, body);

                    TempData["Message"] = "Your Password was reset and sent to your email. Check new password and login";

                    return RedirectToAction("LogOn");
                }
            }

            return View();
        }

        private MembershipUser getUser(string emailName)
        {
            MembershipUser currentUser = Membership.Providers["MembershipProviderOther"].GetUser(emailName, false);

            if (currentUser == null)
            {
                string userName = Membership.Providers["MembershipProviderOther"].GetUserNameByEmail(emailName);
                if (userName != null)
                {
                    currentUser = Membership.Providers["MembershipProviderOther"].GetUser(userName, false);
                }
            }

            return currentUser;
        }
        [HttpPost]
        public ActionResult ValidateCaptcha()
        {
            var response = Request["g-recaptcha-response"];
            //secret that was generated in key value pair
            const string secret = "6LecJwUTAAAAALne594n_HSNPbqoIvzNA-06OvFE";

            var client = new System.Net.WebClient();
            var reply =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0) return View();

                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        ViewBag.Message = "The secret parameter is missing.";
                        break;
                    case ("invalid-input-secret"):
                        ViewBag.Message = "The secret parameter is invalid or malformed.";
                        break;

                    case ("missing-input-response"):
                        ViewBag.Message = "The response parameter is missing.";
                        break;
                    case ("invalid-input-response"):
                        ViewBag.Message = "The response parameter is invalid or malformed.";
                        break;

                    default:
                        ViewBag.Message = "Error occured. Please try again";
                        break;
                }
            }
            else
            {
                ViewBag.Message = "Valid";
            }

            return ViewBag.Message;
        }
    }
}
