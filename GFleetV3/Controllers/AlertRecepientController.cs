﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

namespace GFleetV3.Controllers
{
    public class AlertRecepientController : Controller
    {
        //
        // GET: /AlertRecepient/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_AlertRecepientGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedAlertRecepientModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelAlertRecipient AlertRecepientModel = new ModelAlertRecipient();
            const int pageSize = 25;
            var totalRows = AlertRecepientModel.CountAlertRecipient();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "AlertRecipientId";

            var AlertRecepient = AlertRecepientModel.GetAlertRecipientPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedAlertRecepientModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                AlertRecipient = AlertRecepient
            };
            return data;
        }

        //
        // GET: /AlertRecepient/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_AlertRecepient");
            }
            else
            {
                return View("_AlertRecepient");
            }
        }

        //
        // POST: /AlertRecepient/Create

        [HttpPost]
        [Authorize]
        public ActionResult CreateEditAlertRecepient(AlertRecipient mAlertRecepient, string Command)
        {
            ModelAlertRecipient AlertRecepientModel = new ModelAlertRecipient();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mAlertRecepient.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mAlertRecepient.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mAlertRecepient.ClientId = Service.Utility.getClientId().Value;

                ResponseModel rm = AlertRecepientModel.CreateAlertRecipient(mAlertRecepient);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mAlertRecepient.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mAlertRecepient.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mAlertRecepient.ClientId = Service.Utility.getClientId().Value;
                return this.Json(AlertRecepientModel.UpdateAlertRecipient(mAlertRecepient));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /AlertRecepient/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelAlertRecipient AlertRecepientModel = new ModelAlertRecipient();
            var data = AlertRecepientModel.GetAlertRecipient(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_AlertRecepient", data);
            }
            else

                return View(data);
        }

        //
        // GET: /AlertRecepient/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /AlertRecepient/Delete/5

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}