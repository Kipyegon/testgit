﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class AlertTypeController : Controller
    {


        //
        // GET: /AlertType/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_AlertTypeGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedAlertTypeModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelAlertType AlertTypeModel = new ModelAlertType();
            const int pageSize = 100;
            var totalRows = AlertTypeModel.CountAlertType();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "AlertTypeId";

            var AlertType = AlertTypeModel.GetAlertTypePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedAlertTypeModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                AlertType = AlertType
            };
            return data;
        }

        //
        // GET: /AlertType/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_AlertType");
            }
            else
            {
                return View("_AlertType");
            }
        }

        //
        // POST: /AlertType/Create

        [HttpPost]
        [Authorize]
       // [Authorization]
        public ActionResult CreateEditAlertType(AlertType mAlertType, string Command)
        {
            ModelAlertType AlertTypeModel = new ModelAlertType();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mAlertType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mAlertType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = AlertTypeModel.CreateAlertType(mAlertType);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mAlertType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mAlertType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(AlertTypeModel.UpdateAlertType(mAlertType));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }

        //
        // GET: /AlertType/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelAlertType AlertTypeModel = new ModelAlertType();
            var data = AlertTypeModel.GetAlertType(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_AlertType", data);
        }


        //
        // GET: /AlertType/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

    }
}