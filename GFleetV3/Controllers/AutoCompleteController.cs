﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using DataAccess.SQL;
using GFleetV3.Models;

namespace GFleetV3.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class AutoCompleteController : Controller
    {
        public JsonResult CustomerNameData(string query, string field)
        {
            int ClientId = Service.Utility.getClientId().Value;
            List<string> listData = null;

            if (string.IsNullOrEmpty(query))
                return Json(new { Data = listData });

            return Json(new
            {
                Data = new GFleetModelContainer().People.OfType<Customer>()
                    .Where(m => field.Equals("name") ? m.FirstName.Contains(query) || m.LastName.Contains(query) :
                       field.Equals("email") ? m.Email1.Contains(query) || m.Email2.Contains(query) :
                       field.Equals("Phone") ? m.Telephone1.Contains(query) || m.Telephone2.Contains(query) :
                       m.Organisation.OrganisationName.Contains(query))

                    .OrderBy(r => Guid.NewGuid()).Where(m => m.ClientId == ClientId).Take(5).Select(v => new
                    {
                        Name = (v.FirstName + (string.IsNullOrEmpty(v.LastName) ? "" : ", " + v.LastName)),
                        v.PersonId,
                        Email = (v.Email1 + (string.IsNullOrEmpty(v.Email2) ? "" : ", " + v.Email2)),
                        Telephone = (v.Telephone1 + (string.IsNullOrEmpty(v.Telephone2) ? "" : ", " + v.Telephone2)),
                        Organisation = v.Organisation.OrganisationName,
                        CustomerDirUrl = v.CustomerDirUrl,
                        OrganisationId = v.OrganisationId
                    })
                    .ToList()
            });
        }

        public JsonResult VehicleMileageData(int vehicleId)
        {
            return Json(new
            {
                Data = new GFleetModelContainer().VehicleFuelManagements.Where(v => v.VehicleId == vehicleId)
                .OrderByDescending(v => v.VehicleFuelManagementId).Take(1).Select(v => new
                {
                    Prev = v.CurrentMileage,
                    NoOfLts = v.NoOfLitres
                })
            });
        }

        private void DriverUserId(int vehicleId)
        {
            //var PersonId = new GFleetModelContainer().DriverVehicleAssignments.Where(v => v.VehicleId == vehicleId)
            //.OrderByDescending(v => v.DriverVehicleAssignmentId).Take(1).Select(v => new
            //{
            //    DriverUserId = v.PersonId
            //});
            //return PersonId;
        }

        public JsonResult DriverVehicleInfo(string vehicleId, string Pickupdatetime)
        {
            ModelVehicle vehicleModel = new ModelVehicle();
            //var data = vehicleModel.GetDriverUserId(vehicleId,Pickupdatetime).ToArray();
            List<getDriverVehicleAssignment_Result> data = vehicleModel.GetDriverUserId(int.Parse(vehicleId), Pickupdatetime);
            if (data.Count != 0)
            {
                int PersonId = data[0].PersonId;
                return Json(new
                {
                    Data = new GFleetModelContainer().People.OfType<Driver>().Where(v => v.PersonId == PersonId)
                    .Select(v => new
                    {
                        PersonId = v.PersonId,
                        Name = v.FirstName + " " + v.LastName

                    })
                });
            }
            else
            {
                return Json(null);
            }
        }

        public JsonResult CurrentFare(int OrganisationId)
        {
            return Json(new
            {
                Data = new GFleetModelContainer().Organisations.Where(v => v.OrganisationId == OrganisationId)
                .Select(v => new
                {
                    Amount = v.Pricing
                })
            });
        }
    }
}
