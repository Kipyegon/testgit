﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;

namespace GFleetV3.Controllers
{
    public class ClientController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        //
        //// GET: /Company/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC")
        {
            ModelClient clientModel = new ModelClient();
            const int pageSize = 200;
            var totalRows = clientModel.CountClient();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "ClientId";

            var client = clientModel.GetClientPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedClientModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Client = client
            };
            return View(data);
        }

        //
        // GET: /Client/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateRoleDropDownList();
            PopulateClientTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_Client");
            }
            else
            {
                PopulateRoleDropDownList();
                return View("_Client");
            }
        }

        //
        // POST: /Client/Create

        //[HttpPost]
        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditClient(ClientRegisterModel mClientReg, string Command)
        {
            ModelClient clientModel = new ModelClient();
            if (!ModelState.IsValid)
            {

                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                return Json(clientModel.CreateClientAdminUser(mClientReg));
            }

            else if (Command == "Update")
            {
                mClientReg.ClientModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mClientReg.ClientModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(clientModel.UpdateClient(mClientReg.ClientModel));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /Client/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            var data = ClientRegisterModel.getClientRegisterModel(id);

            PopulateRoleDropDownList();
            PopulateClientTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_Client", data);
            }
            else

                return View("_Client", data);
        }
        //
        // GET: /Client/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Client/Delete/5

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] ClientAdminRoles = System.Configuration.ConfigurationManager.AppSettings["ClientAdminRoles"].ToString().Split(',');
            var RoleRemove = Roles.GetRolesForUser();
            var RoleTypes = new GFleetModelContainer().aspnet_Roles.Where(m => ClientAdminRoles.Contains(m.RoleName));

            ViewBag.RoleName = new SelectList(RoleTypes, "RoleName", "RoleName", RoleName);
        }

        private void PopulateClientTypeDropDownList(object ClientType = null)
        {
            var ClientTypeQuery = from d in new GFleetModelContainer().ClientTypes
                                  orderby d.ClientTypeName
                                  select d;
            ViewBag.ClientType = new SelectList(ClientTypeQuery, "ClientTypeId", "ClientTypeName", ClientType);
        }
    }
}
