﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{

    public class ClientSelectController : Controller
    {
        //
        // GET: /ClientSelect/
        [Authorize]
        // [Authorization]
        public ActionResult Index()
        {
            string UserClientType = Service.Utility.getClientTypeName();
            if (UserClientType != null)
            {
                if (UserClientType == "Private")
                {
                    return RedirectToAction("Index", "Live");
                }
                else
                {
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            else
            {
                PopulateClientDropDownList();
                return View();
            }
            //if (Roles.IsUserInRole("Super Admin") || Roles.IsUserInRole("Sharp Admin"))
            //{
            //}
            //else if (Roles.IsUserInRole("Lease Admin"))
            //{
            //    return RedirectToAction("Index", "Maintainance");
            //}
            //else
            //{
            //    return RedirectToAction("Index", "Dashboard");
            //    //return null;
            //}
        }

        private void PopulateClientDropDownList(object Clients = null)
        {
            if (Roles.IsUserInRole("Super Admin"))
            {
                var ClientQuery = from d in new GFleetModelContainer().Clients
                                  orderby d.ClientName
                                  select d;
                ViewBag.Clients = new SelectList(ClientQuery, "ClientId", "ClientName", Clients);
            }
            else
            {
                try
                {
                    string[] SharpClientIds = System.Configuration.ConfigurationManager.AppSettings["SharpClientIds"].ToString().Split(',');
                    List<int> list = new List<int>();

                    foreach (string id in SharpClientIds)
                    {
                        list.Add(int.Parse(id));
                    }

                    var ClientQuery = from d in new GFleetModelContainer().Clients.Where(v => list.Contains(v.ClientId))
                                      orderby d.ClientName
                                      select d;
                    ViewBag.Clients = new SelectList(ClientQuery, "ClientId", "ClientName", Clients);
                }
                catch (Exception mex)
                {

                }
            }
        }

        //private void PopulateClientDropDownList(Roles. object Clients = null)
        //{
        //    var ClientQuery = from d in new GFleetModelContainer().Clients
        //                      orderby d.ClientName
        //                      select d;
        //    ViewBag.Clients = new SelectList(ClientQuery, "ClientId", "ClientName", Clients);
        //}

        [Authorize]
        [Authorization]
        public ActionResult Client(int ClientId)
        {
            System.Web.HttpContext.Current.Session["ClientId"] = ClientId;
            return RedirectToAction("Index", "Dashboard");
        }
    }
}
