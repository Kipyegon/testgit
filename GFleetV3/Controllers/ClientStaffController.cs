﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;

namespace GFleetV3.Controllers
{
    public class ClientStaffController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        //
        // GET: /Company/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC")
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            const int pageSize = 200;
            var totalRows = clientStaffModel.CountClientStaff();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "UserId";

            var clientStaff = clientStaffModel.GetClientStaffPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedClientStaffModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                ClientStaff = clientStaff
            };
            return View(data);
        }

        //
        // GET: /ClientStaff/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateRoleDropDownList();
            PopulateOrganisationDeptDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_ClientStaff");
            }
            else
            {
                PopulateRoleDropDownList();
                return View("_ClientStaff");
            }
        }

        //
        // POST: /ClientStaff/Create


        [HttpPost]
        [Authorize]
       // [Authorization]
        public ActionResult CreateEditClientStaff(ClientStaffRegisterModel mClientStaff, string[] roles, string Command)
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                if (mClientStaff.RegisterStaffModel.UserRole != null)
                {
                    return Json(clientStaffModel.CreateClientStaff(mClientStaff, roles));
                }
            }

            else if (Command == "Update")
            {
                //mClientStaff.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                //mClientStaff.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //return this.Json(clientStaffModel.UpdateClientStaff(mClientStaff));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }


        [HttpPost]
        [Authorize]
        public ActionResult EditSystemUser(SystemUser mSystemUser, string[] roles, string Command)
        {
            ModelCustomer SystemUserModel = new ModelCustomer();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else
            {
                mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mSystemUser.UserId = mSystemUser.UserId;
                mSystemUser.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mSystemUser.ClientId = Service.Utility.getClientId().Value;
                return this.Json(SystemUserModel.UpdateSystemUser(mSystemUser,roles));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /ClientStaff/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(Guid id)
        {
            ModelCustomer customerModel = new ModelCustomer();
            var data = customerModel.GetSystemUser(id);
            PopulatePersonTitle(data.Title);
            PopulateOrganisationDeptDropDownList(data.OrganisationDeptId);
            //if (Request.IsAjaxRequest())
            //{
            ViewBag.IsUpdate = true;
            return View("_ClientstaffUpdate", data);
            //}
            //else

            //    return View(data);
        }
        //
        // GET: /ClientStaff/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
        [Authorize]
        [Authorization]
        public ActionResult DisapproveUser(string username)
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            return Json(clientStaffModel.DisapproveUser(username));
        }

        [Authorize]
        [Authorization]
        public ActionResult ApproveUser(string username)
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            return Json(clientStaffModel.ApproveUser(username));
        }

        [Authorize]
        [Authorization]
        public ActionResult UnLockUser(string username)
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            return Json(clientStaffModel.UnLockUserDetails(username));
        }


        [Authorize]
        [Authorization]
        public ActionResult ChangePassword(PagedClientStaffModel mClientStaff)
        {
            string newPass, confirmPass, username;

            newPass = mClientStaff.ClientRegStaffModel.RegisterStaffModel.Password;
            confirmPass = mClientStaff.ClientRegStaffModel.RegisterStaffModel.ConfirmPassword;
            username = mClientStaff.ClientRegStaffModel.RegisterStaffModel.Email;
            if (newPass.Equals(confirmPass, StringComparison.Ordinal))
            {
                ModelClientStaff clientStaffModel = new ModelClientStaff();
                return Json(clientStaffModel.ChangeUserPassword(username, mClientStaff));
            }
            else
            {
                return Json(new ResponseModel()
                {
                    Status = false,
                    Message = "Unrecognized operation, kindly refresh browser and retry."
                });
            }
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] ClientStaffRoles = System.Configuration.ConfigurationManager.AppSettings["ClientStaffRoles"].ToString().Split(',');
            var RolesQuery = from d in new GFleetModelContainer().aspnet_Roles.Where(m => ClientStaffRoles.Contains(m.RoleName))
                             orderby d.RoleName
                             select d;
            ViewBag.RoleName = new SelectList(RolesQuery, "RoleName", "RoleName", RoleName);
        }

        private void PopulatePersonTitle(object title = null)
        {
            List<string> PersonTitle = System.Configuration.ConfigurationManager.AppSettings["personTitle"].ToString().Split(',').ToList<string>();

            List<SelectListItem> sli = new List<SelectListItem>();

            foreach (string item in PersonTitle)
            {
                sli.Add(new SelectListItem()
                {
                    Value = item,
                    Text = item
                });
            }

            ViewBag.PersonTitle = new SelectList(sli, "Value", "Text", title);
        }
        private void PopulateOrganisationDeptDropDownList(object OrganisationDepartment = null)
        {
            var DepartmentQuery = from d in new GFleetModelContainer().OrganisationDepartments
                                  orderby d.DepartmentName
                                  select d;
            ViewBag.Department = new SelectList(DepartmentQuery, "OrganisationDeptId", "DepartmentName", OrganisationDepartment);
        }
    }
}
