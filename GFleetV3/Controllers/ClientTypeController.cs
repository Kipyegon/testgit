﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class ClientTypeController : Controller
    {
        //
        // GET: /ClientType/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_ClientTypeGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        

        private PagedClientTypeModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelClientType clientTypeModel = new ModelClientType();
            const int pageSize = 25;
            var totalRows = clientTypeModel.CountClientType();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "ClientTypeId";

            var clientType = clientTypeModel.GetClientTypePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedClientTypeModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                ClientType = clientType
            };
            return data;
        }
        
         //GET:/ClientType/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_ClientType");
            }
            else
            {
                return View("_ClientType");
            }
        }

        //
        // POST: /ClientType/Create

        [HttpPost]
        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditClientType(ClientType mClientType, string Command)
        {
            ModelClientType clientTypeModel = new ModelClientType();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mClientType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
               // mClientType.ClientId = Service.Utility.getClientId().Value;
                mClientType.DateModified = DateTime.Parse(DateTime.Now.ToString());

                ResponseModel rm = clientTypeModel.CreateClientType(mClientType);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mClientType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mClientType.DateModified = DateTime.Parse(DateTime.Now.ToString());
               // mClientType.ClientId = Service.Utility.getClientId().Value;
                return this.Json(clientTypeModel.UpdateClientType(mClientType));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /ClientType/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelClientType clientTypeModel = new ModelClientType();
            var data = clientTypeModel.GetClientType(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }

            return View("_ClientType", data);
        }

        //
        // GET: /ClientType/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ClientType/Delete/5

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
