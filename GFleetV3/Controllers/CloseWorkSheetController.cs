﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Configuration;
using System.Web.Routing;
using System.Web.Security;

namespace GFleetV3.Controllers
{
    public class CloseWorkSheetController : Controller
    {
        [Authorize]
        [Authorization]
        public ActionResult Index()
        {
            PopulateVehicleDropDownList();
            return View();
        }
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateVehicleDropDownList();
            PopulateOrganisationDropDownList();
            PopulateCustomerDropDownList();
            PopulateDriverDropDownList();
            return PartialView("ParameterControls");
        }
        [Authorize]
        public ActionResult GenerateReport(DispatchRptParamModel mReportParam)
        {
            return View("Worksheetreport", DispatchReportModel.GetDispatchRpt(mReportParam));

        }
        [Authorize]
        //[Authorization]
        public ActionResult TripDetails(string VehicleId, string StartDateTime, string EndDateTime)
        {
            CloseWorkSheetModel mjourneydata = new CloseWorkSheetModel();
            try
            {
                mjourneydata = CloseWorkSheetModel.getJourneyData(VehicleId, StartDateTime, EndDateTime);
                var Response = Json(mjourneydata);
                return Response;

            }
            catch (Exception ex)
            {
                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

                return Json(mjourneydata);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult closeWorkSheet(string StartMileage, string Amount, string EndMileage, string DispatchId)
        {
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }

            CloseWorkSheetModel vehicleDispatchModel = new CloseWorkSheetModel();
            ResponseModel rm = vehicleDispatchModel.CloseWorkSheet(StartMileage, Amount, EndMileage, DispatchId);
            return this.Json(rm);

        }
        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var VehicleQuery = from d in new GFleetModelContainer().ClientVehicles.Where(v => v.ClientId == ClientId)
                               orderby d.Vehicle.RegistrationNo
                               select new { VehicleId = d.VehicleId, RegistrationNo = (d.Vehicle.RegistrationNo + " (" + d.Vehicle.NickName + ")") };
            ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "RegistrationNo", Vehicle);
        }

        private void PopulateOrganisationDropDownList(object Organisation = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var OrganisationQuery = from d in new GFleetModelContainer().Organisations.Where(v => v.ClientId == ClientId)
                                    orderby d.OrganisationName
                                    select d;
            ViewBag.Organisation = new SelectList(OrganisationQuery, "OrganisationId", "OrganisationName", Organisation);
        }

        private void PopulateCustomerDropDownList(object Customer = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var CustomerQuery = from d in new GFleetModelContainer().People.OfType<Customer>().Where(v => v.ClientId == ClientId)
                                orderby d.FirstName
                                select new { PersonId = d.PersonId, Name = d.FirstName + d.LastName };
            ViewBag.Customer = new SelectList(CustomerQuery, "PersonId", "Name", Customer);
        }

        private void PopulateDriverDropDownList(object Driver = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var DriverQuery = from d in new GFleetModelContainer().People.OfType<Driver>().Where(v => v.ClientId == ClientId)
                              orderby d.FirstName
                              select new { PersonId = d.PersonId, Name = d.DriverNameNumber };
            ViewBag.Driver = new SelectList(DriverQuery, "PersonId", "Name", Driver);
        }
    }
}