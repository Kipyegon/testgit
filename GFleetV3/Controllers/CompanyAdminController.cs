﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;
using GFleetV3.Service;

using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;

namespace GFleetV3.Controllers
{
    public class CompanyAdminController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        //
        // GET: /Company/
        [Authorize]
        [Authorization]
        //[Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC")
        {
            Boolean isSystemRightsRoles = Utility.isAdmin();

            //if (isSystemRightsRoles)
            //{
                ModelClientStaff clientStaffModel = new ModelClientStaff();
                const int pageSize = 15;
                var totalRows = clientStaffModel.CountClientStaff();

                sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
                sort = "UserId";

                var clientStaff = clientStaffModel.GetClientAdminPage(page, pageSize, "it." + sort + " " + sortDir);

                var data = new PagedClientStaffModel()
                {
                    TotalRows = totalRows,
                    PageSize = pageSize,
                    ClientStaff = clientStaff
                };
              return View(data);
            //}
            //else
            //{
            //    return RedirectToAction("Index", "Dashboard");
            //}            
        }

        //
        // GET: /ClientStaff/Create
        [Authorize]
        [Authorization]
        //[Authorization]
        public ActionResult Create()
        {
            Boolean isSystemRightsRoles = Utility.isAdmin();

            if (isSystemRightsRoles)
            {
                PopulateRoleDropDownList();
                if (Request.IsAjaxRequest())
                {
                    ViewBag.IsUpdate = false;
                    return View("_CompanyAdmin");
                }
                else
                {
                    PopulateRoleDropDownList();
                    return View("_CompanyAdmin");
                }
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }     
        }

        //
        // POST: /ClientStaff/Create


        [HttpPost]
        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditClientStaff(ClientStaffRegisterModel mClientStaff, string Command)
        {
            Boolean isSystemRightsRoles = Utility.isAdmin();

            if (isSystemRightsRoles)
            {
                ModelClientStaff clientStaffModel = new ModelClientStaff();
                if (!ModelState.IsValid)
                {
                    ErrorModel errMod = new ErrorModel();
                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            errMod.Errors.Add(error.ErrorMessage);
                        }
                    }
                    return View("_Error", errMod);
                }
                else if (Command == "Save")
                {
                    if (mClientStaff.RegisterStaffModel.UserRole != null)
                    {
                        return Json(clientStaffModel.CreateClientStaff(mClientStaff,null));
                    }
                }

                else if (Command == "Update")
                {
                    //mClientStaff.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                    //mClientStaff.DateModified = DateTime.Parse(DateTime.Now.ToString());
                    //return this.Json(clientStaffModel.UpdateClientStaff(mClientStaff));
                }

                return Content("Unrecognized operation, kindly refresh browser and retry.");
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }                 
        }

        //
        // GET: /ClientStaff/Edit/5
        [Authorize]
        [Authorization]
        //[Authorization]
        public ActionResult Edit(int id)
        {
            Boolean isSystemRightsRoles = Utility.isAdmin();

            if (isSystemRightsRoles)
            {
                ModelClientStaff clientStaffModel = new ModelClientStaff();
                var data = clientStaffModel.GetClientStaff(id);
                PopulateRoleDropDownList();
                if (Request.IsAjaxRequest())
                {
                    ViewBag.IsUpdate = true;
                    return View("_CompanyAdmin", data);
                }
                else

                    return View(data);
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }       
        }
        //
        // GET: /ClientStaff/Delete/5
        [Authorize]
        [Authorization]
        //[Authorization]
        public ActionResult Delete(int id)
        {
            Boolean isSystemRightsRoles = Utility.isAdmin();

            if (isSystemRightsRoles)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }   
        }

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult DisapproveUser(string username)
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            return Json(clientStaffModel.DisapproveUser(username));
        }
        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult ApproveUser(string username)
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            return Json(clientStaffModel.ApproveUser(username));
        }
        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult UnLockUser(string username)
        {
            ModelClientStaff clientStaffModel = new ModelClientStaff();
            return Json(clientStaffModel.UnLockUserDetails(username));
        }
        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult ChangePassword(PagedClientStaffModel mClientStaff)
        {
            string newPass, confirmPass, username;

            newPass = mClientStaff.ClientRegStaffModel.RegisterStaffModel.Password;
            confirmPass = mClientStaff.ClientRegStaffModel.RegisterStaffModel.ConfirmPassword;
            username = mClientStaff.ClientRegStaffModel.RegisterStaffModel.Email;
            if (newPass.Equals(confirmPass, StringComparison.Ordinal))
            {
                ModelClientStaff clientStaffModel = new ModelClientStaff();
                return Json(clientStaffModel.ChangeUserPassword(username, mClientStaff));
            }
            else
            {
                return Json(new ResponseModel()
                {
                    Status = false,
                    Message = "Unrecognized operation, kindly refresh browser and retry."
                });
            }
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] ClientAdminRoles = System.Configuration.ConfigurationManager.AppSettings["ClientAdminRole"].ToString().Split(',');
            var RolesQuery = from d in new GFleetModelContainer().aspnet_Roles.Where(m => ClientAdminRoles.Contains(m.RoleName))
                             orderby d.RoleName
                             select d;
            ViewBag.RoleName = new SelectList(RolesQuery, "RoleName", "RoleName", RoleName);
        }
    }
}