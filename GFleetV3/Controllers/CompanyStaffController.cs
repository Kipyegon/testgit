﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

namespace GFleetV3.Controllers
{
    public class CompanyStaffController : Controller
    {
        //
        // GET: /CompanyStaff/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_CompanyStaffGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedCompanyStaffModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelCompanyStaff CompanyStaffModel = new ModelCompanyStaff();
            const int pageSize = 100;
            var totalRows = CompanyStaffModel.CountCompanyStaff();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "PersonId";

            var CompanyStaff = CompanyStaffModel.GetCompanyStaffPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedCompanyStaffModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                CompanyStaff = CompanyStaff
            };
            return data;
        }

        //
        // GET: /CompanyStaff/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
            }
            PopulatePersonTitle();
            return View("CompanyStaff");
        }

        //
        // POST: /CompanyStaff/Create

        [HttpPost]
        [Authorize]
     //   [Authorization]
        public ActionResult CreateEditCompanyStaff(CompanyStaff mCompanyStaff, string Command)
        {
            ModelCompanyStaff CompanyStaffModel = new ModelCompanyStaff();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mCompanyStaff.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mCompanyStaff.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mCompanyStaff.ClientId = Service.Utility.getClientId().Value;

                ResponseModel rm = CompanyStaffModel.CreateCompanyStaff(mCompanyStaff);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mCompanyStaff.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mCompanyStaff.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mCompanyStaff.ClientId = Service.Utility.getClientId().Value;
                return this.Json(CompanyStaffModel.UpdateCompanyStaff(mCompanyStaff));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /CompanyStaff/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelCompanyStaff CompanyStaffModel = new ModelCompanyStaff();
            var data = CompanyStaffModel.GetCompanyStaff(id);
            PopulatePersonTitle(data.Title);
            //if (Request.IsAjaxRequest())
            //{
                ViewBag.IsUpdate = true;
                return View("CompanyStaff", data);
            //}
            //else

            //    return View(data);
        }

        //
        // GET: /CompanyStaff/Delete/5
        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            ModelCompanyStaff CompanyStaffModel = new ModelCompanyStaff();

            ResponseModel rm = CompanyStaffModel.DeleteCompanyStaff(id);
            return this.Json(rm);
        }

        private void PopulatePersonTitle(object title = null)
        {
            List<string> PersonTitle = System.Configuration.ConfigurationManager.AppSettings["personTitle"].ToString().Split(',').ToList<string>();

            List<SelectListItem> sli = new List<SelectListItem>();

            foreach (string item in PersonTitle)
            {
                sli.Add(new SelectListItem()
                {
                    Value = item,
                    Text = item
                });
            }

            ViewBag.PersonTitle = new SelectList(sli, "Value", "Text", title);
        }
    }
}