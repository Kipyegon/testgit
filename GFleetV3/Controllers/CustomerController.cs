﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Configuration;
using System.Web.Routing;
using System.Web.Security;

namespace GFleetV3.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_CustomerGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedCustomerModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelCustomer customerModel = new ModelCustomer();
            const int pageSize = 100;
            var totalRows = customerModel.CountCustomer();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "PersonId";

            var customer = customerModel.GetCustomerPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedCustomerModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Customer = customer
            };
            return data;
        }

        //
        // GET: /Customer/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                ViewBag.IsAcUpdate = false;
            }
            PopulateCustomerTypeDropDownList();
            PopulateOrganisationDropDownList();
            PopulateOrganisationDeptDropDownList();
            PopulatePersonTitle();
            PopulateRoleDropDownList();
            return View("_Customer");
        }

        //
        // POST: /Customer/Create

        [HttpPost]
        [Authorize]
     //   [Authorization]
        public ActionResult CreateEditCustomer(OrganisationRegisterModel mCustomerReg, string Command, string ChksendSms, string ChksendEmail)
        {
            bool SendEmail = false;
            bool SendSms = false;
            if (ChksendEmail == "on")
            {
                SendEmail = true;
            }
            if (ChksendSms=="on")
            {
                SendSms = true;
            }
            HttpPostedFileBase File = Request.Files["CustomerDirUrl"];//mCustomer.CustomerDirUrl.ToString(); 
            ModelCustomer customerModel = new ModelCustomer();
            ModelOrganisation OrganisationModel = new ModelOrganisation();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mCustomerReg.OrganisationEndUserModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mCustomerReg.OrganisationEndUserModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mCustomerReg.OrganisationEndUserModel.ClientId = Service.Utility.getClientId().Value;

                if (File != null)
                {
                    if (File.ContentLength > 10240000)
                    {
                        ModelState.AddModelError("file", "The size of the file should not exceed 10 MB");
                        return View();
                    }
                }

                //ResponseModel rm = customerModel.customerModel(mCustomer,File);
               // return this.Json();
                ResponseModel rm = OrganisationModel.CreateOrganisationUser(mCustomerReg, File, SendSms, SendEmail);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mCustomerReg.OrganisationEndUserModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mCustomerReg.OrganisationEndUserModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mCustomerReg.OrganisationEndUserModel.ClientId = Service.Utility.getClientId().Value;
                return this.Json(customerModel.UpdateCustomer(mCustomerReg.OrganisationEndUserModel, File));
            }
            else if (Command == "UpdateAccount")
            {
                mCustomerReg.OrganisationEndUserModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mCustomerReg.OrganisationEndUserModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mCustomerReg.OrganisationEndUserModel.ClientId = Service.Utility.getClientId().Value;
                return this.Json(customerModel.UpdateCustomerAccount(mCustomerReg, File));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /Customer/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelCustomer customerModel = new ModelCustomer();
            var data = OrganisationRegisterModel.getEndUser(id);
            //var data = customerModel.GetCustomer(id);
            PopulatePersonTitle(data.OrganisationEndUserModel.Title);
            PopulateCustomerTypeDropDownList(data.OrganisationEndUserModel.CustomerTypeId);
            PopulateOrganisationDropDownList(data.OrganisationEndUserModel.OrganisationId);
            PopulateOrganisationDropDownList(data.OrganisationEndUserModel.OrganisationDeptId);
            PopulateOrganisationDeptDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_Customer", data);
            }
            else

                return View("_Customer", data);
        }
        [Authorize]
        [Authorization]
        public ActionResult CreateEndUserAc(int id)
        {
            ModelCustomer customerModel = new ModelCustomer();
            var data = OrganisationRegisterModel.getEndUser(id);
            //var data = customerModel.GetCustomer(id);
            PopulatePersonTitle(data.OrganisationEndUserModel.Title);
            PopulateCustomerTypeDropDownList(data.OrganisationEndUserModel.CustomerTypeId);
            PopulateOrganisationDropDownList(data.OrganisationEndUserModel.OrganisationId);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsAcUpdate = true;
                ViewBag.IsUpdate = false;
                return View("_Customer", data);
            }
            else

                return View("_Customer", data);
        }
        //
        // GET: /Customer/Delete/5
        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            ModelCustomer customerModel = new ModelCustomer();

            ResponseModel rm = customerModel.DeleteCustomer(id);
            return this.Json(rm);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ResetPassword(string Username)
        {
            //string Username = Telephone1;
            ResponseModel response = ModelOrganisation.ResetPassword(Username);

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        private void PopulateCustomerTypeDropDownList(object CustomerType = null)
        {
            var CustomerTypeQuery = from d in new GFleetModelContainer().CustomerTypes
                                    orderby d.CustomerTypeName
                                    select d;
            ViewBag.CustomerType = new SelectList(CustomerTypeQuery, "CustomerTypeId", "CustomerTypeName", CustomerType);
        }

        private void PopulateOrganisationDropDownList(object Organisation = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var OrganisationQuery = from d in new GFleetModelContainer().Organisations.Where(v => v.ClientId == ClientId)
                                    orderby d.OrganisationName
                                    select d;
            ViewBag.Organisation = new SelectList(OrganisationQuery, "OrganisationId", "OrganisationName", Organisation);
        }

        private void PopulateOrganisationDeptDropDownList(object OrganisationDepartment = null)
        {
            var DepartmentQuery = from d in new GFleetModelContainer().OrganisationDepartments
                                    orderby d.DepartmentName
                                    select d;
            ViewBag.Department = new SelectList(DepartmentQuery, "OrganisationDeptId", "DepartmentName", OrganisationDepartment);
        }

        private void PopulatePersonTitle(object title = null)
        {
            List<string> PersonTitle = System.Configuration.ConfigurationManager.AppSettings["personTitle"].ToString().Split(',').ToList<string>();

            List<SelectListItem> sli = new List<SelectListItem>();

            foreach (string item in PersonTitle)
            {
                sli.Add(new SelectListItem()
                {
                    Value = item,
                    Text = item
                });
            }

            ViewBag.PersonTitle = new SelectList(sli, "Value", "Text", title);
        }

        public FileResult GetCustomerMap(int CustId)
        {
            Customer mCustomer = new ModelCustomer().GetCustomer(CustId);
            if (mCustomer != null && mCustomer.CustomerDirUrl != null)
            {
                string CustomerDir = WebConfigurationManager.AppSettings["CustomerDir"];
                string filepath = System.Web.HttpContext.Current.Server.MapPath(CustomerDir);
                string Filename = mCustomer.CustomerDirUrl;

                string fileextension = Path.GetExtension(filepath + Filename);

                byte[] pdfByte = Service.Helper.GetBytesFromFile(filepath + Filename);
                return File(pdfByte, "application/" + fileextension.Replace(".", ""));
            }
            else
            {
                return null;
            }
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] OrganisationRoles = System.Configuration.ConfigurationManager.AppSettings["OrganisationRole"].ToString().Split(',');
            var RolesQuery = from d in new GFleetModelContainer().aspnet_Roles.Where(m => OrganisationRoles.Contains(m.RoleName))
                             orderby d.RoleName
                             select d;
            ViewBag.RoleName = new SelectList(RolesQuery, "RoleName", "RoleName", RoleName);
        }
    }
}
