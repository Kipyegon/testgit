﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
 using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class DashBoardController: Controller
    {
        //
        // GET: /DashBoard/
        [Authorize]
        [Authorization]
        public ActionResult Index()
        {
            return View();
            //"_DispatchClientPerformance"
        }

        public JsonResult VehicleData()
        {
            int ClientId = Service.Utility.getClientId().Value;
            DateTime start = DateTime.Today;
            DateTime end = DateTime.Today.AddDays(1).AddMilliseconds(-1);

            //DateTime start = DateTime.Parse("2013-07-17 00:00:00.000");
            //DateTime end = DateTime.Parse("2013-07-30 23:00:00.000");

            List<getDistancetravelled_V3_Result> data = new GFleetModelContainer().getDistancetravelled_V3("all", start, end, ClientId).ToList();

            return Json(new
            {
                top10 = data.OrderByDescending(v => v.Distance)
                    .Take(10)
                    .Select(v => new
                    {
                        Vehicles = v.Reg + " (" + v.VehicleCode + ")",
                        Distance = v.Distance
                    }),
                bottom10 = data.OrderBy(v => v.Distance)
                    .Take(10)
                    .Select(v => new
                    {
                        Vehicles = v.Reg + " (" + v.VehicleCode + ")",
                        Distance = v.Distance
                    })
            });
        }

        public JsonResult DispatchClientPerformanceData()
        {
            int ClientId = Service.Utility.getClientId().Value;
            DateTime start = DateTime.Today;
            DateTime end = DateTime.Today.AddDays(1).AddMilliseconds(-1);
            DateTime myEndTime = DateTime.Now;
            DateTime myStartTime = myEndTime.Subtract(myEndTime.TimeOfDay);
            //TimeSpan myStartTime = new TimeSpan(0, 0, 0); //12 midnight
            //TimeSpan myEndTime = DateTime.Now.TimeOfDay;

            List<DispatchOrgPerformance_Result> data = DashBoardModel.getDispatchOrgPerf(myStartTime, myEndTime, ClientId, "-1");
                //new GFleetModelContainer().DispatchOrgPerformance(ClientId, myStartTime, myEndTime).ToList<DispatchOrgPerformance_Result>();

                List<Object> orgGraph = new List<object>();
                foreach (DispatchOrgPerformance_Result _data in data)
                {
                    //{ "unit": "a", "status": "Transport", "val": 5.1425000000000000 }
                    orgGraph.Add(new graphData() { status = "Dispatched", unit = _data.OrganisationName, val = _data.Dispatched });
                    orgGraph.Add(new graphData() { status = "Completed", unit = _data.OrganisationName, val = _data.Completed });
                    orgGraph.Add(new graphData() { status = "Cancelled", unit = _data.OrganisationName, val = _data.Cancelled });
                }
                return Json(orgGraph);
            }

        public PartialViewResult VehicleDataBottomTen()
        {
            return PartialView("_VehicleDataBottomTen");
        }

        public PartialViewResult VehicleDataTopTen()
        {
            return PartialView("_VehicleDataTopTen");
        }

        public PartialViewResult DispatchClientPerformance()
        {
            return PartialView("_DispatchClientPerformance");
        }

        public PartialViewResult DisconnectedView()
        {
            return PartialView("_Disconnected", DashBoardModel.getDisconnectedData());
        }
        
    }

    public class organisationBarGraph
    {
        public int? dispatched { get; set; }
        public int? completed { get; set; }
        public int? cancelled { get; set; }
    }

    public class organisationGraph
    {
        public string organisationName { get; set; }
        public organisationBarGraph organisationBarGraph { get; set; }
    }

    public class graphData
    {
        public string unit { get; set; }
        public string status { get; set; }
        public int? val { get; set; }
    }
}
