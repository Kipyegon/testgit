﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
namespace GFleetV3.Controllers
{
    public class DealerController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        //
        //// GET: /Dealer/
        [Authorize]
        [Authorization]
        //[Authorization]
        public ActionResult Index()
        {
            var edealers = DealerModel.GetDealers();
            ViewBag.Label = "Dealers";
            return View(edealers.ToList());
        }

        //
        // GET: /Dealer/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateRoleDropDownList();
            PopulateClientTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_Dealer");
            }
            else
            {
                PopulateRoleDropDownList();
                return View("_Dealer");
            }
        }

        //
        // POST: /Dealer/Create

        //[HttpPost]
        [Authorize]
     //   [Authorization]
        public ActionResult CreateEditDealer(DealerRegisterModel mDealerReg, string Command)
        {
            DealerModel clientModel = new DealerModel();
            if (!ModelState.IsValid)
            {

                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                return Json(clientModel.CreateDealerAdminUser(mDealerReg));
            }

            else if (Command == "Update")
            {
                //mDealerReg.DealerModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                //mDealerReg.DealerModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //return this.Json(clientModel.UpdateDealer(mDealerReg.DealerModel));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /Dealer/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            var data = DealerRegisterModel.getDealerRegisterModel(id);

            PopulateRoleDropDownList();
            PopulateClientTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_Dealer", data);
            }
            else

                return View("_Dealer", data);
        }
        //
        // GET: /Dealer/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Dealer/Delete/5

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] DealerAdminRoles = System.Configuration.ConfigurationManager.AppSettings["DealerAdminRoles"].ToString().Split(',');
            var RoleRemove = Roles.GetRolesForUser();
            var RoleTypes = new GFleetModelContainer().aspnet_Roles.Where(m => DealerAdminRoles.Contains(m.RoleName));

            ViewBag.RoleName = new SelectList(RoleTypes, "RoleName", "RoleName", RoleName);
        }
        private void PopulateClientTypeDropDownList(object ClientType = null)
        {
            var ClientTypeQuery = from d in new GFleetModelContainer().ClientTypes
                                  orderby d.ClientTypeName
                                  select d;
            ViewBag.ClientType = new SelectList(ClientTypeQuery, "ClientTypeId", "ClientTypeName", ClientType);
        }
    }
}