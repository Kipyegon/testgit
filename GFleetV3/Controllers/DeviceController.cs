﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;


namespace GFleetV3.Controllers
{
    public class DeviceController : Controller
    {

        //
        // GET: /Device/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DeviceGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedDeviceModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelDevice deviceModel = new ModelDevice();
            const int pageSize = 100;
            var totalRows = deviceModel.CountDevice();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "DeviceId";

            var device = deviceModel.GetDevicePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedDeviceModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Device = device
            };
            return data;
        }

        //
        // GET: /Device/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateDeviceModelDropDownList();
            PopulateDeviceTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_Device");
            }
            else
            {
                return View("_Device");
            }
        }

        //
        // POST: /Device/Create

        [HttpPost]
        [Authorize]
       // [Authorization]
        public ActionResult createEditDevice(Device mDevice, string Command)
        {
            ModelDevice deviceModel = new ModelDevice();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mDevice.ClientId = Service.Utility.getClientId().Value;
                ResponseModel rm = deviceModel.CreateDevice(mDevice);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mDevice.ClientId = Service.Utility.getClientId().Value;
                return this.Json(deviceModel.UpdateDevice(mDevice));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }

        //
        // GET: /Device/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelDevice deviceModel = new ModelDevice();
            PopulateDeviceModelDropDownList();
            var data = deviceModel.GetDevice(id);
            PopulateDeviceTypeDropDownList(data);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_Device", data);
        }


        //
        // GET: /Device/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
        private void PopulateDeviceModelDropDownList(object DeviceModel = null)
        {
            var DeviceModelQuery = from d in new GFleetModelContainer().DeviceModels
                                    orderby d.DeviceMake
                                    select d;
            ViewBag.DeviceModel = new SelectList(DeviceModelQuery, "DeviceModelId", "DeviceMake", DeviceModel);
        }

        private void PopulateDeviceTypeDropDownList(object DeviceType = null)
        {
            var DeviceTypeQuery = from d in new GFleetModelContainer().DeviceTypes
                                  orderby d.DeviceTypeName
                                  select d;
            ViewBag.DeviceType = new SelectList(DeviceTypeQuery, "DeviceTypeId", "DeviceTypeName", DeviceType);
        }

        //private void PopulateClientsData(Client client = null)
        //{
        //    var allClients = new GFleetModelContainer().Clients;
        //    var deviceClients = client != null ? new HashSet<int>(client.ClientDevices.Select(c => c.ClientId)) : new HashSet<int>();
        //    var viewModel = new List<Clients>();
        //    foreach (var clients in allClients)
        //    {
        //        viewModel.Add(new Clients
        //        {
        //            ClientId = clients.ClientId,
        //            ClientName = clients.ClientName,
        //            Assigned = deviceClients.Contains(clients.ClientId)
        //        });
        //    }
        //    ViewBag.Clients = viewModel;
        //}

        //private void UpdateClientDevice(string[] selectedClients, Device clientDevices)
        //{
        //    if (selectedClients == null)
        //    {
        //        clientDevices.ClientDevices = null;
        //        return;
        //    }

        //    var selectedClientsHS = new HashSet<string>(selectedClients);
        //    var deviceClients = new HashSet<int>
        //        (clientDevices.ClientDevices.Select(c => c.ClientDeviceId));
        //    foreach (var clients in new GFleetModelContainer().Clients)
        //    {
        //        if (selectedClientsHS.Contains(clients.ClientId.ToString()))
        //        {
        //            if (!deviceClients.Contains(clients.ClientId))
        //            {
        //                clientDevices.ClientDevices.Add(new ClientDevice()
        //                {
        //                    DeviceId = clientDevices.DeviceId,
        //                    Date = DateTime.Now,
        //                    ClientId = clients.ClientId
        //                });
        //            }
        //        }
        //        else
        //        {
        //            if (deviceClients.Contains(clients.ClientId))
        //            {
        //                clientDevices.ClientDevices.Remove(new ClientDevice()
        //                {
        //                    DeviceId = clientDevices.DeviceId,
        //                    Date = DateTime.Now,
        //                    ClientId = clients.ClientId
        //                });
        //            }
        //        }
        //    }
        //}
    }
}
