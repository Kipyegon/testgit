﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class DispatchedVehicleDailyController : Controller
    {

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DispatchedVehiclesGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }
        private PagedVehicleDispatchModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelDispatchedVehiclesDaily DispatchedVehiclesDailyModel = new ModelDispatchedVehiclesDaily();
            const int pageSize = 100;
            var totalRows = DispatchedVehiclesDailyModel.CountDispatchedVehicles();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "DispatchId";

            var vehicleDispatch = DispatchedVehiclesDailyModel.GetDispatchedVehilcesDailyPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleDispatchModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize
                //,VehicleDispatch = vehicleDispatch
            };
            return data;
        }

        //
        // GET: /DispatchedVehicleDaily/Details/5
        [Authorize]
        [Authorization]
        public ActionResult Details(int id)
        {
            ModelDispatchedVehiclesDaily DispatchedVehiclesDailyModel = new ModelDispatchedVehiclesDaily();
            var data = DispatchedVehiclesDailyModel.GetDispatchedVehilce(id);
            PopulateAccessibilityData();
            PopulatePaymentModeData();
            PopulateAssignedExtrasData();
            PopulateCustomerDropDownList();
            PopulateDriverDropDownList();
            PopulateVehicleDropDownList();
            PopulateCustomer(data.CustomerId);

            return View("_DispatchedVehiclesDaily", data);
        }

        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            var VehicleQuery = from d in new GFleetModelContainer().Vehicles
                               orderby d.RegistrationNo
                               select new { VehicleId = d.VehicleId, RegistrationNo = (d.RegistrationNo + " (" + d.NickName + ")") };
            ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "RegistrationNo", Vehicle);
        }

        private void PopulateDriverDropDownList(object Driver = null)
        {
            var DriverQuery = from d in new GFleetModelContainer().People.OfType<Driver>()
                              orderby d.FirstName
                              select d;
            ViewBag.Driver = new SelectList(DriverQuery, "PersonId", "FirstName", Driver);
        }

        private void PopulateCustomerDropDownList(object Customer = null)
        {
            var CustomerQuery = from d in new GFleetModelContainer().People.OfType<Customer>()
                                orderby d.FirstName
                                select d;
            ViewBag.Customer = new SelectList(CustomerQuery, "PersonId", "FirstName", Customer);
        }

        private void PopulateAssignedExtrasData(Dispatch dispatch = null)
        {
            var allExtras = new GFleetModelContainer().Extras;
            var dispatchExtras = dispatch != null ? new HashSet<int>(dispatch.DispatchExtras.Select(c => c.ExtrasId)) : new HashSet<int>();
            var viewModel = new List<DispatchExtras>();
            foreach (var extra in allExtras)
            {
                viewModel.Add(new DispatchExtras
                {
                    ExtrasID = extra.ExtrasId,
                    Extras = extra.Extras,
                    Assigned = dispatchExtras.Contains(extra.ExtrasId)
                });
            }
            ViewBag.Extras = viewModel;
        }

        private void UpdateDispatchExtras(string[] selectedExtras, Dispatch dispatchToUpdate)
        {
            if (selectedExtras == null)
            {
                dispatchToUpdate.DispatchExtras = null;
                return;
            }

            var selectedExtrasHS = new HashSet<string>(selectedExtras);
            var dispatchExtras = new HashSet<int>
                (dispatchToUpdate.DispatchExtras.Select(c => c.ExtrasId));
            foreach (var extra in new GFleetModelContainer().DispatchExtras)
            {
                if (selectedExtrasHS.Contains(extra.ExtrasId.ToString()))
                {
                    if (!dispatchExtras.Contains(extra.ExtrasId))
                    {
                        dispatchToUpdate.DispatchExtras.Add(new DispatchExtra()
                        {
                            DispatchId = dispatchToUpdate.DispatchId,
                            Extra = null,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            ExtrasId = extra.ExtrasId
                        });
                    }
                }
                else
                {
                    if (dispatchExtras.Contains(extra.ExtrasId))
                    {
                        dispatchToUpdate.DispatchExtras.Remove(new DispatchExtra()
                        {
                            DispatchId = dispatchToUpdate.DispatchId,
                            Extra = null,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            ExtrasId = extra.ExtrasId
                        });
                    }
                }
            }
        }

        private void PopulateAccessibilityData(Dispatch dispatch = null)
        {
            var allAccessibility = new GFleetModelContainer().Accesibilities;
            var dispatchAccessibility = dispatch != null ? new HashSet<int>(dispatch.DispatchAccessibilities.Select(c => c.AccessibilityId)) : new HashSet<int>();
            var viewModel = new List<AccessibilityinDispatch>();
            foreach (var accessibility in allAccessibility)
            {
                viewModel.Add(new AccessibilityinDispatch
                {
                    AccessibilityId = accessibility.AccesibilityId,
                    Accessibility = accessibility.Accesibility1,
                    Assigned = dispatchAccessibility.Contains(accessibility.AccesibilityId)
                });
            }
            ViewBag.Accessibility = viewModel;
        }

        private void PopulatePaymentModeData(Dispatch dispatch = null)
        {
            var allPaymentMode = new GFleetModelContainer().PaymentModes;
            var dispatchPaymentMode = dispatch != null ? new HashSet<int>(dispatch.PaymentMode.Dispatches.Select(c => c.PaymentModeId)) : new HashSet<int>();
            var viewModel = new List<DispatchPaymentMode>();
            foreach (var paymentMode in allPaymentMode)
            {
                viewModel.Add(new DispatchPaymentMode
                {
                    PaymentModeId = paymentMode.PaymentModeId,
                    PaymentMode = paymentMode.PaymentMode1,
                    Assigned = dispatchPaymentMode.Contains(paymentMode.PaymentModeId)
                });
            }
            ViewBag.PaymentMode = viewModel;
        }

        private void PopulateCustomer(int id)
        {
            var CustomerName =  new GFleetModelContainer().People.Where(m =>m.PersonId==id).FirstOrDefault();
            ViewBag.CustomerName = CustomerName.FirstName + " " + CustomerName.LastName;
            ViewBag.CustomerEmail = CustomerName.Email1;
            ViewBag.CustomerPhone = CustomerName.Telephone1;
            ViewBag.CustomerOrganisation = CustomerName.Organisation.OrganisationName;
        }
        [Authorize]
        [Authorization]
        public ActionResult loadMap(MapModel data = null)
        {
            return PartialView("_mapDirections", data);
        }
    }
}
