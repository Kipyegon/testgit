﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using GFleetV3.Service;
using System.Web.Script.Serialization;

namespace GFleetV3.Controllers
{
    public class DisplayBookingDataController : Controller
    {
        // GET: DisplayBookingData
        [Authorize]
        //[Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DisplayBookingDataGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);

            return View(data);
        }

        private PagedVehicleDispatchModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            const int pageSize = 500;
            var totalRows = vehicleDispatchModel.CountVehicleDispatch();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "DispatchId";

            var vehicleDispatch = vehicleDispatchModel.GetVehicleDispatchPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleDispatchModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                VehicleDispatch = vehicleDispatch
            };
            return data;
        }

    }
}