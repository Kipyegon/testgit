﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Security;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Threading;



namespace GFleetV3.Controllers
{
    public class DriverController : Controller
    {
        //
        // GET: /Driver/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DriverGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ResetPassword(string Username)
        {
            ResponseModel response = ModelOrganisation.ResetPassword(Username);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [Authorization]
        private PagedDriverModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelDriver driverModel = new ModelDriver();
            const int pageSize = 1000;
            var totalRows = driverModel.CountDriver();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "PersonId";

            var driver = driverModel.GetDriverPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedDriverModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Driver = driver
            };
            return data;
        }
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                PopulateLicenceTypesData();
                PopulateDriverStatusDropDownList();
                ViewBag.IsUpdate = false;
                return View("_Driver");
            }
            else
            {
                PopulateLicenceTypesData();
                ViewBag.IsUpdate = false;
                PopulateDriverStatusDropDownList();
                return View("_Driver");
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult CreateDriverThread()
        {
            ModelDriver driverModel = new ModelDriver();
            ResponseModel rm = driverModel.CreateDriverUserThread();
            return this.Json(rm);

            //string gh = "254725 554 014,254720 332 535/0755633139";


            //string[] fg = gh.Split(',');
            //foreach (string k in fg)
            //{
            //    new Thread(() =>
            //    {
            //        try
            //        {
            //            Membership.Providers["MembershipProviderOther"].DeleteUser(k, true);
            //        }
            //        catch (Exception mex)
            //        {
            //            //ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Request);
            //            //throw new Exception(ex.Message.ToString());
            //        }
            //    }).Start();
            //}
            //return Json(new object());
        }

        //
        // POST: /Driver/Create

        [HttpPost]
        [Authorize]
        //  [Authorization]
        public ActionResult CreateEditDriver(DriverRegisterModel mDriver, string[] selectedLicenceTypes, string Command, string ChksendSms, string ChksendEmail)
        {
            bool SendEmail = false;
            bool SendSms = false;
            if (ChksendEmail == "on")
            {
                SendEmail = true;
            }
            if (ChksendSms == "on")
            {
                SendSms = true;
            }
            ModelDriver driverModel = new ModelDriver();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                //mDriver.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                //mDriver.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //mDriver.ClientId = Service.Utility.getClientId().Value;
                // UpdateDriverLicenceTypes(selectedLicenceTypes, mDriver);
                ResponseModel rm = driverModel.CreateDriverUser(mDriver, SendEmail, SendSms);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                //mDriver.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                //mDriver.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //mDriver.ClientId = Service.Utility.getClientId().Value;
                //UpdateDriverLicenceTypes(selectedLicenceTypes, mDriver);
                return this.Json(driverModel.UpdateDriver(mDriver));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /Driver/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(string UserId)
        {
            //var data = AccountProfile.getDriver(UserId);
            var data = DriverRegisterModel.getDriver(UserId);
            //var obj = new
            //{
            //    someOtherProperty = "hello",
            //    view = RenderHelper.PartialView(this, "_Driver", rm)
            //};

            //return Json(obj);
            AccountProfile driverModel = new AccountProfile();
            //return this.Json(rm);
            // return Json(rm, JsonRequestBehavior.AllowGet);
            //var data = driverModel.(UserId);
            ////.GetDriver(Guid.Parse(UserId));
            PopulateLicenceTypesData();
            PopulateDriverStatusDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_Driver", data);
            }
            else
                return View("_Driver", data);
        }

        //
        // GET: /Driver/Delete/5
        [Authorize]
        [Authorization]
        [HttpPost]
        public ActionResult Delete(string UserId)
        {
            AccountProfile userModel = new AccountProfile();
            ResponseModel rm = AccountProfile.DeleteDriver(UserId); //customerModel.DeleteCustomer(id);
            return this.Json(rm);
        }


        //public ActionResult ChangePassword(PagedDriverModel mDriver)
        //{
        //    string newPass, confirmPass, username;

        //    newPass = mDriver.DriverRegModel.RegisterDriverUserModel.Username;
        //    //newPass = mDriver.PagedDriverModel.RegisterDriverUserModel.Password;
        //    //confirmPass=mDriver.DriverRegModel.DriverModel.
        //    //confirmPass = mDriver.PagedDriverModel.RegisterDriverUserModel.ConfirmPassword;
        //    //username = mDriver.PagedDriverModel.RegisterDriverUserModel.Email;
        //    if (newPass.Equals(confirmPass, StringComparison.Ordinal))
        //    {
        //        ModelClientStaff driverModel = new ModelClientStaff();
        //        return Json(driverModel.ChangeUserPassword(username, mDriver));
        //    }
        //    else
        //    {
        //        return Json(new ResponseModel()
        //        {
        //            Status = false,
        //            Message = "Unrecognized operation, kindly refresh browser and retry."
        //        });
        //    }
        //}
        public static class RenderHelper
        {
            public static string PartialView(Controller controller, string viewName, object model)
            {
                controller.ViewData.Model = model;

                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                    viewResult.View.Render(viewContext, sw);
                    viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                    return sw.ToString();
                }
            }
        }

        private void PopulateDriverStatusDropDownList(object DriverStatus = null)
        {
            var DriverStatusQuery = from d in new GFleetModelContainer().DriverStatus
                                    orderby d.DriverStatusName
                                    select d;
            ViewBag.DriverStatus = new SelectList(DriverStatusQuery, "DriverStatusId", "DriverStatusName", DriverStatus);
        }

        private void PopulateLicenceTypesData(Driver driver = null)
        {
            var alllicenceTypes = new GFleetModelContainer().LicenceTypes;
            var driverlicenceTypes = driver != null ? new HashSet<int>(driver.DriverLicenceTypes.Select(c => c.LicenceTypeId)) : new HashSet<int>();
            var viewModel = new List<LicenceTypes>();
            foreach (var licencetypes in alllicenceTypes)
            {
                viewModel.Add(new LicenceTypes
                {
                    LicenceTypeId = licencetypes.LicenceTypeId,
                    DriverLicenceType = licencetypes.DriverLicenceType,
                    Assigned = driverlicenceTypes.Contains(licencetypes.LicenceTypeId)
                });
            }
            ViewBag.LicenceTypes = viewModel;
        }

        private void UpdateDriverLicenceTypes(string[] selectedLicenceTypes, Driver licenceTypesUpdate)
        {
            if (selectedLicenceTypes == null)
            {
                licenceTypesUpdate.DriverLicenceTypes = null;
                return;
            }

            var selectedLicenceTypesHS = new HashSet<string>(selectedLicenceTypes);
            var driverLicenceTypes = new HashSet<int>
                (licenceTypesUpdate.DriverLicenceTypes.Select(c => c.LicenceTypeId));
            foreach (var licenceType in new GFleetModelContainer().LicenceTypes)
            {
                if (selectedLicenceTypesHS.Contains(licenceType.LicenceTypeId.ToString()))
                {
                    if (!driverLicenceTypes.Contains(licenceType.LicenceTypeId))
                    {
                        licenceTypesUpdate.DriverLicenceTypes.Add(new DriverLicenceType()
                        {
                            PersonId = licenceTypesUpdate.PersonId,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            ClientId = Service.Utility.getClientId().Value,
                            DateModified = DateTime.Now,
                            LicenceTypeId = licenceType.LicenceTypeId
                        });
                    }
                }
                else
                {
                    if (driverLicenceTypes.Contains(licenceType.LicenceTypeId))
                    {
                        licenceTypesUpdate.DriverLicenceTypes.Remove(new DriverLicenceType()
                        {
                            PersonId = licenceTypesUpdate.PersonId,
                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                            LicenceTypeId = licenceType.LicenceTypeId
                        });
                    }
                }
            }
        }

        public object pageNumber { get; set; }

        public object pageSize { get; set; }
    }
}
