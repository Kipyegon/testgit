﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class DriverEndorsementController : Controller
    {
        //
        // GET: /DriverEndorsement/


        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DriverEndorsementGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedDriverEndorsmentModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelDriverEndorsment driverEndorsmentModel = new ModelDriverEndorsment();
            const int pageSize = 100;
            var totalRows = driverEndorsmentModel.CountDriverEndorsment();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            //var validColumns = new[] { "custid", "name", "address", "contactno" };
            //if (!validColumns.Any(c => c.Equals(sort, StringComparison.CurrentCultureIgnoreCase)))
            sort = "Id";

            var driverEndorsment = driverEndorsmentModel.GetDriverEndorsmentPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedDriverEndorsmentModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                DriverEndorsment = driverEndorsment
            };
            return data;
        }


        //
        // GET: /DriverEndorsement/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                PopulateDriverDropDownList();
                ViewBag.IsUpdate = false;
                return View("_DriverEndorsement");
            }
            else
            {
                PopulateDriverDropDownList();
                return View("_DriverEndorsement");
            }
        }

        //
        // POST: /DriverEndorsement/Create

        [HttpPost]
        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditDriverEndorsement(DriverEndosment mDriverEndorsement, string Command)
        {
            ModelDriverEndorsment driverEndorsmentModel = new ModelDriverEndorsment();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mDriverEndorsement.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());

                ResponseModel rm = driverEndorsmentModel.CreateDriverEndosment(mDriverEndorsement);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                return this.Json(driverEndorsmentModel.UpdateDriverEndosment(mDriverEndorsement));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /DriverEndorsement/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelDriverEndorsment driverEndorsmentModel = new ModelDriverEndorsment();
            PopulateDriverDropDownList();
            var data = driverEndorsmentModel.GetDriverEndorsment(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_DriverEndorsement", data);
            }
            else

                return View(data);
        }
        //
        // POST: /DriverEndorsement/Edit/5


        //
        // GET: /DriverEndorsement/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
        private void PopulateDriverDropDownList(object Driver = null)
        {
            var DriverQuery = from d in new GFleetModelContainer().People.OfType<Driver>()
                              orderby d.FirstName
                              select d;
            ViewBag.Driver = new SelectList(DriverQuery, "PersonId", "FirstName", Driver);
        }
    }
}
