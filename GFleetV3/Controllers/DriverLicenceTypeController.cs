﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class DriverLicenceTypeController : Controller
    {
        //
        // GET: /DriverLicenceType/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DriverLicenceTypeGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedDriverLicenceTypeModel gridData(int page, ref string sort, ref string sortDir)
        {
           ModelDriverLicenceType driverLicenceTypeModel = new ModelDriverLicenceType();
            const int pageSize = 15;
            var totalRows = driverLicenceTypeModel.CountDriverLicenceType();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "LicenceTypeId";

            var driverLicenceType = driverLicenceTypeModel.GetDriverLicenceTypePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedDriverLicenceTypeModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                LicenceType = driverLicenceType
            };
            return data;
        }

        //
        // GET: /DriverLicenceType/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_DriverLicenceType");
            }
            else
            {
                return View("_DriverLicenceType");
            }
        }

        //
        // POST: /DriverLicenceType/Create

        [HttpPost]
        [Authorize]
     //   [Authorization]
        public ActionResult CreateEditDriverLicenceType(LicenceType mDriverLicenceType, string Command)
        {
            ModelDriverLicenceType driverLicenceTypeModel = new ModelDriverLicenceType();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mDriverLicenceType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDriverLicenceType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = driverLicenceTypeModel.CreateLicenceType(mDriverLicenceType);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mDriverLicenceType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDriverLicenceType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(driverLicenceTypeModel.UpdateLicenceType(mDriverLicenceType));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /DriverLicenceType/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelDriverLicenceType driverLicenceTypeModel = new ModelDriverLicenceType();
            var data = driverLicenceTypeModel.GetDriverLicenceType(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_DriverLicenceType", data);
            }
            else

                return View(data);
        }

        //
        // GET: /DriverLicenceType/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

    }
}
