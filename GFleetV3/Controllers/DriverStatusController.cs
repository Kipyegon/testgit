﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class DriverStatusController : Controller
    {
        //
        // GET: /DriverStatus/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DriverStatusGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedDriverStatusModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelDriverStatus driverStatusModel = new ModelDriverStatus();
            const int pageSize = 100;
            var totalRows = driverStatusModel.CountDriverStatus();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            //var validColumns = new[] { "custid", "name", "address", "contactno" };
            //if (!validColumns.Any(c => c.Equals(sort, StringComparison.CurrentCultureIgnoreCase)))
            sort = "DriverStatusId";

            var driverStatus = driverStatusModel.GetDriverStatussPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedDriverStatusModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                DriverStatus = driverStatus
            };
            return data;
        }

        //
        // GET: /DriverStatus/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_DriverStatus");
            }
            else
            {
                return View("_DriverStatus");
            }
        }

        //
        // POST: /DriverStatus/Create

        [HttpPost]

        [Authorize]
       // [Authorization]
        public ActionResult CreateEditDriverStatus(DriverStatu mDriverStatus, string Command)
        {
            ModelDriverStatus driverStatusModel = new ModelDriverStatus();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mDriverStatus.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDriverStatus.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mDriverStatus.ClientId = Service.Utility.getClientId().Value;


                ResponseModel rm = driverStatusModel.CreateDriverStatus(mDriverStatus);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mDriverStatus.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDriverStatus.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mDriverStatus.ClientId = Service.Utility.getClientId().Value;

                return this.Json(driverStatusModel.UpdateDriverStatus(mDriverStatus));
            }

            return Content("Unrecognized operation, kindly refresh browser and rety.");
        }

        //
        // GET: /DriverStatus/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelDriverStatus driverStatusModel = new ModelDriverStatus();
            var data = driverStatusModel.GetDriverStatus(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_DriverStatus", data);
            }
            else

                return View(data);
        }

        //
        // GET: /DriverStatus/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
    }
}
