﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class DriverVehicleAssignController : Controller
    {
        //
        // GET: /DriverVehicleAssign/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_DriverVehicleAssignGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedDriverVehicleAssignmentModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelDriverVehicleAssignment driverVehicleAssignmentModel = new ModelDriverVehicleAssignment();
            const int pageSize = 100;
            var totalRows = driverVehicleAssignmentModel.CountDriverVehicleAssignment();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            //var validColumns = new[] { "custid", "name", "address", "contactno" };
            //if (!validColumns.Any(c => c.Equals(sort, StringComparison.CurrentCultureIgnoreCase)))
            sort = "DriverVehicleAssignmentId";

            var driverVehicleAssignment = driverVehicleAssignmentModel.GetDriverVehicleAssignmentPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedDriverVehicleAssignmentModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                DriverVehicleAssignment = driverVehicleAssignment
            };
            return data;
        }

        //
        // GET: /DriverVehicleAssign/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                PopulateVehicleDropDownList();
                PopulateDriverDropDownList();
                ViewBag.IsUpdate = false;
                return View("_DriverVehicleAssignment");
            }
            else
            {
                PopulateVehicleDropDownList();
                PopulateDriverDropDownList();
                return View("_DriverVehicleAssignment");
            }
        }

        //
        // POST: /DriverVehicleAssign/Create

        [HttpPost]

        [Authorize]
       // [Authorization]
        public ActionResult CreateEditDriverVehicleAssignment(DriverVehicleAssignment mDriverVehicleAssignment, string Command)
        {
            ModelDriverVehicleAssignment driverVehicleAssignmentModel = new ModelDriverVehicleAssignment();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mDriverVehicleAssignment.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDriverVehicleAssignment.StartDate = DateTime.Parse(mDriverVehicleAssignment.StartDateDate + " " + mDriverVehicleAssignment.StartDateTime);
                mDriverVehicleAssignment.EndDate = DateTime.Parse(mDriverVehicleAssignment.EndDateDate + " " + mDriverVehicleAssignment.EndDateTime);
                mDriverVehicleAssignment.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mDriverVehicleAssignment.ClientId = Service.Utility.getClientId().Value;

                ResponseModel rm = driverVehicleAssignmentModel.CreateDriverVehicleAssignment(mDriverVehicleAssignment);

                JsonResult result = this.Json(rm);
                return result;
            }

            else if (Command == "Update")
            {
                mDriverVehicleAssignment.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDriverVehicleAssignment.StartDate = DateTime.Parse(mDriverVehicleAssignment.StartDateDate + " " + mDriverVehicleAssignment.StartDateTime);
                mDriverVehicleAssignment.EndDate = DateTime.Parse(mDriverVehicleAssignment.EndDateDate + " " + mDriverVehicleAssignment.EndDateTime);
                mDriverVehicleAssignment.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mDriverVehicleAssignment.ClientId = Service.Utility.getClientId().Value;
                return this.Json(driverVehicleAssignmentModel.UpdateDriverVehicleAssignment(mDriverVehicleAssignment));
            }

            return Content("Unrecognized operation, kindly refresh browser and rety.");
        }

        //
        // GET: /DriverVehicleAssign/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelDriverVehicleAssignment driverVehicleAssignmentModel = new ModelDriverVehicleAssignment();
            PopulateVehicleDropDownList();
            PopulateDriverDropDownList();
            var data = driverVehicleAssignmentModel.GetDriverVehicleAssignment(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_DriverVehicleAssignment", data);
            }
            else

                return View(data);
        }


        //
        // GET: /DriverVehicleAssign/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //[Authorize]
        //[Authorization]
        public JsonResult SignIn(int id)
        {
            ModelDriverVehicleAssignment driverVehicleAssignmentModel = new ModelDriverVehicleAssignment();
            ResponseModel rm = driverVehicleAssignmentModel.DriverSignIn(id);
            JsonResult result = this.Json(rm);
            return result;            
        }

        //[Authorize]
        //[Authorization]
        public JsonResult SignOut(int id)
        {
            ModelDriverVehicleAssignment driverVehicleAssignmentModel = new ModelDriverVehicleAssignment();
            ResponseModel rm = driverVehicleAssignmentModel.DriverSignOut(id);
            JsonResult result = this.Json(rm);
            return result;   
        }

        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var VehicleQuery = new GFleetModelContainer().vw_VehicleDetailsV3.Where(v => v.ClientId == ClientId)
               .Select(v => new
               {
                   VehicleId = v.VehicleId,
                   VehicleRegCode = v.VehicleRegCode
               });
            ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "VehicleRegCode", Vehicle);
        }
        private void PopulateDriverDropDownList(object Driver = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var DriverQuery = from d in new GFleetModelContainer().People.OfType<Driver>().Where(v => v.ClientId == ClientId)
                              orderby d.FirstName
                              //select d;
                              select new { PersonId = d.PersonId, Name = (d.FirstName + " " + d.LastName) };
            ViewBag.Driver = new SelectList(DriverQuery, "PersonId", "Name", Driver);
        }
    }
}
