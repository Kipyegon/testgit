﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using GFleetV3.Models;
using System.Net.Mail;
using System.Threading;
using DataAccess.SQL;
using GFleetV3.Service;

namespace GFleetV3.Controllers
{
    public class GetLocationController : Controller
    {
        public ActionResult sendLocateCommand()
        {
            string commandType = "locate";
            string IP = "162.13.137.130";
            string userId = "322041FC-4AF9-4328-93AF-6D1C99FACA15";

            new Thread(() =>
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    var ClientIds = from d in new GFleetModelContainer().Clients
                                    select d.ClientId;
                    foreach (var client in ClientIds)
                    {
                        foreach (var vehicle in new GFleetModelContainer().getVehiclesToLocate(client))
                        {
                            new Thread(() =>
                            {
                                LiveModel.sendCommand(vehicle.VehicleId.Value, commandType, IP, System.Web.HttpContext.Current, Guid.Parse(userId));
                            }).Start();
                        }
                    }
                }
            }).Start();
            return null;
        }
    }

}