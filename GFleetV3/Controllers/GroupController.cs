﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;
namespace GFleetV3.Controllers
{
    public class GroupController : Controller
    {
        
        //
        // GET: /Group/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_GroupGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedGroupModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelGroup groupModel = new ModelGroup();
            const int pageSize = 100;
            var totalRows = groupModel.CountGroup();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "GroupId";

            var group = groupModel.GetGroupPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedGroupModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Group = group
            };
            return data;
        }

        //
        // GET: /Group/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_Group");
            }
            else
            {
                return View("_Group");
            }
        }

        //
        // POST: /Group/Create

        [HttpPost]

        [Authorize]
     //   [Authorization]
        public ActionResult createEditGroup(Group mGroup, string Command)
        {
            ModelGroup groupModel = new ModelGroup();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mGroup.ClientId = Service.Utility.getClientId().Value;
                mGroup.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mGroup.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = groupModel.CreateGroup(mGroup);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mGroup.ClientId = Service.Utility.getClientId().Value;
                mGroup.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mGroup.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(groupModel.UpdateGroup(mGroup));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
        //
        // GET: /Group/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelGroup groupModel = new ModelGroup();
            var data = groupModel.GetGroup(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_Group", data);
        }

        //
        // GET: /Group/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
    }
}
