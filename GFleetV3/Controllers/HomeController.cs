﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;

namespace GFleetV3.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            //ViewBag.Message = "Welcome to ASP.NET MVC!";
            string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
            if (AccessUrls.Contains(url))
            {
                return RedirectToAction("LogOn", "Account");
            }
            else
            {
                //return Redirect("http://taksi.geeckoltd.com/");
                return View();
            }
        }
       
        public ActionResult About()
        {
            return View();
        }
    
        //public ActionResult Contact()
        //{
        //    return View();
        //}

   
        //public ActionResult sendContactMail(string name, string email, string comment)
        //{
        //    Service.SendEmail.SendUserMngtEmail("info@geeckoltd.com", "Contact Page email from " + name, email + " - " + comment);
        //    ModelState.AddModelError("", "Email sent successfully.");
        //    return View("Index");
        //}
    }
}
