﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class InsuaranceController : Controller
    {
        
        //
        // GET: /Insuarance/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_InsuaranceGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedInsuranceModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelInsuarance insuranceModel = new ModelInsuarance();
            const int pageSize = 100;
            var totalRows = insuranceModel.CountInsuarance();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "InsuaranceId";

            var insuarance = insuranceModel.GetInsuarancePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedInsuranceModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Insuarance = insuarance
            };
            return data;
        }

        //
        // GET: /Insuarance/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_Insuarance");
            }
            else
            {
                return View("_Insuarance");
            }
        }
        //
        // POST: /Insuarance/Create

        [HttpPost]

        [Authorize]
     //   [Authorization]
        public ActionResult createEditInsuarance(Insuarance mInsuarance, string Command)
        {
            ModelInsuarance insuranceModel = new ModelInsuarance();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mInsuarance.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = insuranceModel.CreateInsuarance(mInsuarance);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mInsuarance.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(insuranceModel.UpdateInsuarance(mInsuarance));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }

        //
        // GET: /Insuarance/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelInsuarance insuranceModel = new ModelInsuarance();
            var data = insuranceModel.GetInsuarance(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_Insuarance", data);
        }

        //
        // GET: /Insuarance/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
    }
}
