﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class InsuaranceTypeController : Controller
    {
        
        //
        // GET: /InsuaranceType/


        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_InsuaranceTypeGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedInsuranceTypeModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelInsuaranceType insuaranceTypeModel = new ModelInsuaranceType();
            const int pageSize = 100;
            var totalRows = insuaranceTypeModel.CountInsuaranceType();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "InsuaranceTypeId";

            var insuaranceType = insuaranceTypeModel.GetInsuaranceTypePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedInsuranceTypeModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                InsuaranceType = insuaranceType
            };
            return data;
        }

        //
        // GET: /InsuaranceType/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            //Thread.Sleep(2000);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_InsuaranceType");
            }
            else
            {
                return View("_InsuaranceType");
            }
        }

        //
        // POST: /InsuaranceType/Create

        [HttpPost]

        [Authorize]
       // [Authorization]
        public ActionResult CreateEditInsuaranceType(InsuaranceType mInsuaranceType, string Command)
        {
            ModelInsuaranceType insuaranceTypeModel = new ModelInsuaranceType();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mInsuaranceType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mInsuaranceType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = insuaranceTypeModel.CreateInsuaranceType(mInsuaranceType);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mInsuaranceType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mInsuaranceType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(insuaranceTypeModel.UpdateInsuaranceType(mInsuaranceType));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }

        //
        // GET: /InsuaranceType/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelInsuaranceType insuaranceTypeModel = new ModelInsuaranceType();
            var data = insuaranceTypeModel.GetInsuaranceType(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_InsuaranceType", data);
        }
        //
        // GET: /InsuaranceType/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

    }
}
