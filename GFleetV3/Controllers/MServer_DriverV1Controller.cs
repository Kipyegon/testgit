﻿using DataAccess.SQL;
using GFleetV3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace GFleetV3.Controllers
{
    public class MServer_DriverV1Controller : Controller
    {

        ////ResponseModel rm = new ResponseModel();
        //[HttpPost]
        //public ActionResult RegisterDriverDeviceWithRegNo(string Device_token,string DeviceNumber)
        //{
        //    MServer_DriverV1Model mregistertoken = new MServer_DriverV1Model();
        //    ResponseModel mregistered = mregistertoken.RegisterDriverDevice(Device_token,DeviceNumber);
        //    return Json(mregistered);
        //}

        [HttpPost]
        public ActionResult RegisterDriverDevice(string Device_token, string DeviceNumber)
        {
            MServer_DriverV1Model mregistertoken = new MServer_DriverV1Model();
            ResponseModel mregistered = mregistertoken.RegisterDriverDevice(Device_token, DeviceNumber);
            return Json(mregistered);
        }
        [HttpPost]
        public ActionResult GetDriverDispatch(int DispatchId, int ClientId)
        {
            MServer_DriverV1Model mdispatch = new MServer_DriverV1Model();
            ResponseModel mdriverdispatch = mdispatch.GetVehicleDispatch(DispatchId, ClientId);
            return Json(mdriverdispatch);
        }
        //ResponseModel _rm = new ResponseModel();
        [HttpPost]
        public ActionResult Login(string Username, string Password, string Device_token)
        {
            MServer_DriverV1Model mlogin = new MServer_DriverV1Model();
            try
            {
                Boolean uservalid = System.Web.Security.Membership.ValidateUser(Username, Password);
                System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(Username);

                if (!uservalid)
                {
                    mlogin.IsAuthenticated = false;

                    if (user != null)
                    {
                        if (!user.IsApproved)
                        {
                            mlogin.AuthMessage = "Your Account is not Approved";
                        }
                        else if (user.IsLockedOut)
                        {
                            mlogin.AuthMessage = "Your Account is locked";
                        }
                        else
                        {
                            mlogin.AuthMessage = "Invalid Username or Password";
                        }
                    }
                    else
                    {
                        mlogin.AuthMessage = "Invalid Username or Password";
                    }
                    return Json(mlogin);
                }
                else
                {
                    string[] InvalidLoginRoles = WebConfigurationManager.AppSettings["InvalidLoginRoles"].Split(',');

                    foreach (string role in System.Web.Security.Roles.GetRolesForUser(Username))
                    {
                        if (InvalidLoginRoles.Contains(role))
                        {
                            mlogin.IsAuthenticated = false;
                            mlogin.AuthMessage = "User not authorized on this platform.";
                            return Json(mlogin);
                        }
                    }
                    mlogin = MServer_DriverV1Model.getValidatedUser(Guid.Parse(user.ProviderUserKey.ToString()), Username);

                    if (mlogin == null)
                    {
                        mlogin = new MServer_DriverV1Model();
                        mlogin.IsAuthenticated = false;
                        mlogin.AuthMessage = "Credentials used is not a contact type. Kindly contact the administrator";

                        return Json(mlogin);
                    }
                    else
                    {

                        mlogin.IsAuthenticated = true;
                        mlogin.AuthMessage = "Login Successfull";

                        var Response = Json(mlogin);
                        return Response;
                    }

                }
            }
            catch (Exception ex)
            {
                mlogin.AuthMessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                mlogin.IsAuthenticated = false;

                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

                return Json(mlogin);
            }

        }
        [HttpPost]
        public ActionResult GetDriverDispatches(string UserId)
        {
            MServer_DriverV1Model mdispatch = new MServer_DriverV1Model();
            mdispatch = MServer_DriverV1Model.getDriverDispatches(Guid.Parse(UserId));
            return Json(mdispatch);
        }
        [HttpPost]
        public JsonResult PushDispatchToDriver(int DispatchId)
        {
            MessageResponseModel response = MServer_DriverV1Model.PushDriverDispatch(DispatchId);

            return Json(response);
        }
        [HttpPost]
        public ActionResult EditDriverDetails(DriverDetailData driverdata)
        {
            ResponseModel rm;
            try
            {

                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();

                return this.Json(driverModel.UpdateMobileDriver(driverdata));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpPost]
        public ActionResult AcceptDeclineJob(int DispatchStatusId, int DispatchId, bool IsAccept)
        {
            ResponseModel rm;
            try
            {

                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();

                return this.Json(driverModel.AcceptDeclineDispatch(DispatchId, DispatchStatusId, IsAccept));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpPost]
        public ActionResult CloseDriverTrip(TripDetails tripdata)
        {
            ResponseModel rm;
            try
            {

                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();

                return this.Json(driverModel.CloseTrip(tripdata.StartTime, tripdata.StartJobPosition, tripdata.EnrouteDisatnce,
                    tripdata.ClientOnBoardTime, tripdata.ClientOnBoardPosition, tripdata.DropOffTime, tripdata.DropOffPosition,
                    tripdata.DropOffMileage, tripdata.DispatchId));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpPost]
        public ActionResult StartTrip(TripDetails tripdata)
        {
            ResponseModel rm;
            try
            {

                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();

                return this.Json(driverModel.CloseTrip(tripdata.StartTime, tripdata.StartJobPosition, null,
                    null, null, null, null,
                    null, tripdata.DispatchId));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpPost]
        public ActionResult EnrouteTrip(TripDetails tripdata)
        {
            ResponseModel rm;
            try
            {

                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();

                return this.Json(driverModel.CloseTrip(tripdata.StartTime, tripdata.StartJobPosition, tripdata.EnrouteDisatnce,
                    tripdata.ClientOnBoardTime, tripdata.ClientOnBoardPosition, tripdata.DropOffTime, tripdata.DropOffPosition,
                    tripdata.DropOffMileage, tripdata.DispatchId));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpPost]
        public ActionResult ClientBoardTrip(TripDetails tripdata)
        {
            ResponseModel rm;
            try
            {

                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();

                return this.Json(driverModel.CloseTrip(tripdata.StartTime, tripdata.StartJobPosition, tripdata.EnrouteDisatnce,
                    tripdata.ClientOnBoardTime, tripdata.ClientOnBoardPosition, tripdata.DropOffTime, tripdata.DropOffPosition,
                    tripdata.DropOffMileage, tripdata.DispatchId));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpPost]
        public ActionResult DropClientTrip(TripDetails tripdata)
        {
            ResponseModel rm;
            try
            {

                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();

                return this.Json(driverModel.CloseTrip(tripdata.StartTime, tripdata.StartJobPosition, tripdata.EnrouteDisatnce,
                    tripdata.ClientOnBoardTime, tripdata.ClientOnBoardPosition, tripdata.DropOffTime, tripdata.DropOffPosition,
                    tripdata.DropOffMileage, tripdata.DispatchId));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpPost]
        public ActionResult WaitingCustomer(WaitingDetails waitdata)
        {
            ResponseModel rm;
            try
            {
                MServer_DriverV1Model driverModel = new MServer_DriverV1Model();
                return this.Json(driverModel.WaitCustomer(waitdata.DispatchId, waitdata.StartWaitTime, waitdata.EndWaitTime,
                    waitdata.WaitingPosition));
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }
        [HttpGet]
        public ActionResult felixDriver()
        {
            int DispatchId = 7401;
            MessageResponseModel response = MServer_DriverV1Model.PushDriverDispatch(DispatchId);

            return Json(response,JsonRequestBehavior.AllowGet);
        }
    }
}