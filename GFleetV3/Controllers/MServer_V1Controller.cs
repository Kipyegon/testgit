﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using GFleetV3.Models;
using System.Threading.Tasks;
using System.Threading;
using System.Xml.Serialization;
using System.IO;
using System.Web.Configuration;

namespace GFleetV3.Controllers
{
    public class MServer_V1Controller : Controller
    {
        ResponseModel _rm = new ResponseModel();
        [HttpPost]
        public ActionResult Login(string Username, string Password, string Phone_token)
        {
            MServer_V1Model mlogin = new MServer_V1Model();
            try
            {
                Boolean uservalid = Membership.ValidateUser(Username, Password);
                MembershipUser user = Membership.GetUser(Username);

                if (!uservalid)
                {
                    mlogin.IsAuthenticated = false;

                    if (user != null)
                    {
                        if (!user.IsApproved)
                        {
                            mlogin.AuthMessage = "Your Account is not Approved";
                        }
                        else if (user.IsLockedOut)
                        {
                            mlogin.AuthMessage = "Your Account is locked";
                        }
                        else
                        {
                            mlogin.AuthMessage = "Invalid Username or Password";
                        }
                    }
                    else
                    {
                        mlogin.AuthMessage = "Invalid Username or Password";
                    }
                    return Json(mlogin);
                }
                else
                {
                    string[] InvalidLoginRoles = WebConfigurationManager.AppSettings["InvalidLoginRoles"].Split(',');

                    foreach (string role in Roles.GetRolesForUser(Username))
                    {
                        if (InvalidLoginRoles.Contains(role))
                        {
                            mlogin.IsAuthenticated = false;
                            mlogin.AuthMessage = "User not authorized on this platform.";
                            return Json(mlogin);
                        }
                    }


                    Client_Phone.RegisterDevice(Phone_token, Guid.Parse(user.ProviderUserKey.ToString()));
                    mlogin = MServer_V1Model.getValidatedUser(Guid.Parse(user.ProviderUserKey.ToString()), Username);

                    if (mlogin == null)
                    {
                        mlogin = new MServer_V1Model();
                        mlogin.IsAuthenticated = false;
                        mlogin.AuthMessage = "Credentials used is not a contact type. Kindly contact the administrator";

                        return Json(mlogin);
                    }
                    else
                    {

                        mlogin.IsAuthenticated = true;
                        mlogin.AuthMessage = "Login Successfull";

                        var Response = Json(mlogin);
                        return Response;
                    }

                }
            }
            catch (Exception ex)
            {
                mlogin.AuthMessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                mlogin.IsAuthenticated = false;

                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

                return Json(mlogin);
            }

        }

        [HttpPost]
        public ActionResult getClientVehicles(int ClientId)
        {
            List<VehicleData> vData = null;
            try
            {
                vData = MServer_V1Model.GetVehicleData(ClientId);
            }
            catch (Exception ex)
            {
                vData = new List<VehicleData>();
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

            }
            return Json(vData);
        }
        [HttpPost]

        public ActionResult getLatestVehicleData(string VehicleIds, int? ClientId, string latestdate)
        {
            MServer_V1ModelLatest mlatestdata = new MServer_V1ModelLatest();
            try
            {
                if (ClientId.HasValue)
                {
                    DateTime date;
                    if (DateTime.TryParse(latestdate, out date))
                    {
                        mlatestdata = MServer_V1Model.getLatest(VehicleIds, ClientId.Value, date);
                        return Json(mlatestdata);
                    }
                    else
                    {
                        mlatestdata.isSuccess = false;
                        mlatestdata.Message = "Wrong Date: " + latestdate;

                        return Json(mlatestdata);
                    }
                }
                else
                {
                    mlatestdata.isSuccess = false;
                    mlatestdata.Message = "Client Not Known!";

                    return Json(mlatestdata);
                }
            }
            catch (Exception ex)
            {
                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);


                mlatestdata.isSuccess = false;
                mlatestdata.Message = "Error Occured";

                return Json(mlatestdata);
            }

        }


        [HttpPost]
        public JsonResult controlEngineMobile(string vehicleId, string isSwitchOn, string userId)
        {
            int _vehicleId;
            if (int.TryParse(vehicleId, out _vehicleId))
            {
                return Json(LiveModel.sendCommand(_vehicleId, bool.Parse(isSwitchOn) ? "startEngine" : "stopEngine", Request.UserHostAddress, System.Web.HttpContext.Current, Guid.Parse(userId)));
            }
            else
            {
                return Json("Unknown device");
            }
        }

        [HttpPost]
        public JsonResult requestLocation(string vehicleId, string userId)
        {
            int _vehicleId;
            HttpContext current = System.Web.HttpContext.Current;
            requestLocationReply rlr = new requestLocationReply() { IsSuccess = false };
            if (int.TryParse(vehicleId, out _vehicleId))
            {
                try
                {
                    rlr.Message = LiveModel.sendCommand(_vehicleId, "Locate", Request.UserHostAddress, current, Guid.Parse(userId));
                    if (!rlr.Message.StartsWith("Error:")) rlr.IsSuccess = true;

                    return Json(rlr);
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, System.Web.HttpContext.Current);
                    _rm.Status = false;
                    _rm.Message = "Invalid UserId";
                    return Json(_rm);
                }
            }
            else
            {
                rlr.Message = "Unknown device";
                return Json(rlr);
            }
        }


        [HttpPost]
        public ActionResult getFuelData(int ClientId, int VehicleId)
        {
            MServer_V1Fuel mFuelData = new MServer_V1Fuel();
            try
            {
                mFuelData = MServer_V1Fuel.GetVehicleFuelData(ClientId, VehicleId);
                return Json(mFuelData);
            }
            catch (Exception ex)
            {
                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);


                mFuelData.Message = "Error Occured";

                return Json(mFuelData);
            }

        }

        [HttpPost]
        public ActionResult saveFuelData(int VehicleId, int ClientId, double AvgFuelCons, string FuelDate, string DriverUserId, int Diffrence,
            int CurrentMileage, int PreviuosMileage, string UserId, double NoOfLitres, double Cost)
        {
            MServer_V1Fuel vehicleFuelManagementModel = new MServer_V1Fuel();

            ResponseModel rm = vehicleFuelManagementModel.saveFuelManagement(VehicleId, ClientId, AvgFuelCons, DateTime.Parse(FuelDate), Guid.Parse(DriverUserId),
                 CurrentMileage, PreviuosMileage, Guid.Parse(UserId), NoOfLitres, Diffrence, Cost);
            return this.Json(rm);
        }

    }

    public class requestLocationReply
    {
        public string Message { set; get; }
        public Boolean IsSuccess { set; get; }
    }
}