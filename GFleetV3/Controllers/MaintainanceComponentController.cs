﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

namespace GFleetV3.Controllers
{
    public class MaintainanceComponentController : Controller
    {
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("MaintainanceComponentGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }
        private PagedMaintainanceComponentModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelMaintainanceComponent maitainancacoponentModel = new ModelMaintainanceComponent();
            const int pageSize = 100;
            var totalRows = maitainancacoponentModel.CountMaintainanceComponent();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "MaintainanceComponentId";

            var maintainancecomponent = maitainancacoponentModel.GetMaitainanceComponentPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedMaintainanceComponentModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                MaintainanceComponent = maintainancecomponent
            };
            return data;
        }
        //public ActionResult Index()
        //{
        //    //var ecomponents =  ModelMaintainanceComponent.GetMaintainanceComponent();
        //    //ViewBag.Label = "Spare Parts";
        //    //return View(ecomponents.ToList());
        //}

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_MaintainanceComponent");
            }

            return View("_MaintainanceComponent");
        }
        // POST: /MaintananceComponent/Create

        [HttpPost]

        [Authorize]
      //  [Authorization]
        public ActionResult createEditMaintananceComponent(MaintainanceComponent mMaintananceComponent, string Command)
        {
            ModelMaintainanceComponent componentModel = new ModelMaintainanceComponent();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mMaintananceComponent.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mMaintananceComponent.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = componentModel.CreateMaintananceComponent(mMaintananceComponent);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mMaintananceComponent.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mMaintananceComponent.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(componentModel.UpdateMaintananceComponent(mMaintananceComponent));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
        //
        // GET: /ModelMaintainanceComponent/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelMaintainanceComponent componentModel = new ModelMaintainanceComponent();
            var data = componentModel.GetMaintananceComponent(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_MaintainanceComponent",data);
        }

        //
        // GET: /MaintananceComponent/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
    }
}