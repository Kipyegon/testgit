﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;
using System.Collections;

namespace GFleetV3.Controllers
{
    public class MaintainanceController : Controller
    {
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("MaintainanceGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }
        private PagedMaintainanceModel gridData(int page, ref string sort, ref string sortDir)
        {
            MaintainanceModel maitainanceModel = new MaintainanceModel();
            const int pageSize = 100;
            var totalRows = maitainanceModel.CountMaintainance();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "MaintainanceId";

            var maintainance = maitainanceModel.GetMaitainancePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedMaintainanceModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Maintainance = maintainance
            };
            return data;
        }
        [Authorize]
        [Authorization]
        public ActionResult GetFilteredData(string MaintainanceTypeId, string MaintainanceStatusId,
            string VehicleId)
        {
            var emaintainances = MaintainanceModel.GetFilteredDealerMaintainance(MaintainanceTypeId, MaintainanceStatusId, VehicleId);
            ViewBag.Label = "Maintainance Scheduled";
            return PartialView("_MaintainanceDetailsGrid",emaintainances.ToList());
        }
        [Authorize]
        [Authorization]
        public ActionResult GetIndex()
        {
            var ecomponents = ModelMaintainanceComponent.GetMaintainanceComponent()
                .Select(v => new
                    {
                        ComponentName = v.ComponentName,
                        ComponentNumber = v.ComponentNumber
                    });
            ViewBag.Label = "Maintainance Scheduled";

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Converters.Add(new ListMaintainaceConverter());
            settings.Converters.Add(new IsoDateTimeConverter { DateTimeFormat = "dd-MMM-yyyy" });
            settings.Formatting = Formatting.Indented;

            string json = JsonConvert.SerializeObject(ecomponents, settings);
            Console.WriteLine(json);
            return Json(json);
        }
        public class MaintainanceComponentData
        {
            //public string ComponentName { get; set; }
        }
        public class spareparts
        {
            public string ComponentName { get; set; }
            public string ComponentNumber { get; set; }
        }

        public class ListMaintainaceConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                // We only want to convert lists of non-enumerable class types (including string)
                if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    Type itemType = objectType.GetGenericArguments().Single();
                    if (itemType.IsClass && !typeof(IEnumerable).IsAssignableFrom(itemType))
                    {
                        return true;
                    }
                }
                return false;
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                JArray array = new JArray();
                IList list = (IList)value;
                if (list.Count > 0)
                {
                    JArray keys = new JArray();

                    JObject first = JObject.FromObject(list[0], serializer);
                    foreach (JProperty prop in first.Properties())
                    {
                        keys.Add(new JValue(prop.Name));
                    }
                    array.Add(keys);

                    foreach (object item in list)
                    {
                        JObject obj = JObject.FromObject(item, serializer);
                        JArray itemValues = new JArray();
                        foreach (JProperty prop in obj.Properties())
                        {
                            itemValues.Add(prop.Value);
                        }
                        array.Add(itemValues);
                    }
                }
                array.WriteTo(writer);
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                IList list = (IList)Activator.CreateInstance(objectType);  // List<T>
                JArray array = JArray.Load(reader);
                if (array.Count > 0)
                {
                    Type itemType = objectType.GetGenericArguments().Single();

                    JArray keys = (JArray)array[0];
                    foreach (JArray itemValues in array.Children<JArray>().Skip(1))
                    {
                        JObject item = new JObject();
                        for (int i = 0; i < keys.Count; i++)
                        {
                            item.Add(new JProperty(keys[i].ToString(), itemValues[i]));
                        }

                        list.Add(item.ToObject(itemType, serializer));
                    }
                }
                return list;
            }
        }
        [Authorize]
        [Authorization]
        public ActionResult Create(bool? edit)
        {
            PopulateMaintainanceTypeDropDownList();
            PopulateVehicleDropDownList();
            PopulateMaintainanceComponentDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
            }
            var data = 0;
            if (edit.HasValue && edit.Value)
            {
                data = 1;
                ViewData["heading"] = "Edit Maintainance";
            }
            else
            {
                ViewData["heading"] = "New Maintainance";
            }

            return View("_Maintainance", new MaintainanceDetailView());
        }
        [Authorize]
        [Authorization]
        public ActionResult CreateAuthorize(int id)
        {
            MaintainanceModel maintainanceModel = new MaintainanceModel();
            var data = maintainanceModel.GetMaintainance(id);
            return View("_Authorize", data);
        }
        [Authorize]
        [Authorization]
        public ActionResult CreateComplete(int id)
        {
            MaintainanceModel maintainanceModel = new MaintainanceModel();
            var data = maintainanceModel.GetMaintainance(id);
            return View("_Complete", data);
        }
        [Authorize]
        [Authorization]
        public ActionResult AuthorizeMaintainance(Maintainance mMaintainance)
        {
            MaintainanceModel maintainanceModel = new MaintainanceModel();
            mMaintainance.MaintainanceStatusId = 4;
            return this.Json(maintainanceModel.AuthCompleteMaintainance(mMaintainance));
        }
        [Authorize]
        [Authorization]
        public ActionResult CompleteMaintainance(Maintainance mMaintainance)
        {
            MaintainanceModel maintainanceModel = new MaintainanceModel();
            mMaintainance.MaintainanceStatusId = 3;
            return this.Json(maintainanceModel.AuthCompleteMaintainance(mMaintainance));
        }
        //
        // POST: /Maintainance/Create

        [HttpPost]

        [Authorize]
       // [Authorization]
        public ActionResult createEditMaintainance(MaintainanceDetailView mMaintainance, string Command, string[] item_quantity,
                string[] item_cost, string[] MaintainanceComponentId, string[] MaintainanceId, string[] MaintainanceDetailId)
        {
            MaintainanceModel maintainanceModel = new MaintainanceModel();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mMaintainance.MainTable.RequestedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mMaintainance.MainTable.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mMaintainance.MainTable.DealerId = Service.Utility.getClientId().Value;
                mMaintainance.MainTable.MaintainanceStatusId = 1;
                //UpdateMaintainaceDetailComponent(MaintainanceComponentId, item_quantity, item_cost, MaintainanceDetailId, mMaintainance.MainTable);
                //ResponseModel rm = maintainanceModel.CreateMaintainance(mMaintainance);
                return this.Json(UpdateMaintainaceDetailComponent(MaintainanceComponentId, item_quantity, item_cost, MaintainanceDetailId, mMaintainance.MainTable, mMaintainance.MainTable));
            }

            else if (Command == "Update")
            {
                mMaintainance.MainTable.RequestedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mMaintainance.MainTable.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //UpdateMaintainaceDetailComponent(MaintainanceComponentId, item_quantity, item_cost, MaintainanceDetailId, mMaintainance.MainTable);
                return this.Json(UpdateMaintainaceDetailComponent(MaintainanceComponentId, item_quantity, item_cost, MaintainanceDetailId, mMaintainance.MainTable, mMaintainance.MainTable));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
        //
        // GET: /ModelMaintainance/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            PopulateMaintainanceTypeDropDownList();
            PopulateVehicleDropDownList();
            PopulateMaintainanceComponentDropDownList();
            ViewBag.IsUpdate = true;
            return View("_Maintainance", MaintainanceDetailView.MaintainanceDetailViewGet(id));
        }

        //
        // GET: /Maintainance/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
        private void PopulateMaintainanceStatusDropDownList(object MaintainanceStatus = null)
        {
            var MaintainanceStatusQuery = from d in new GFleetModelContainer().MaintainanceStatus
                                          orderby d.MaintainanceStatusName
                                          select d;
            ViewBag.MaintainanceStatus = new SelectList(MaintainanceStatusQuery, "MaintainanceStatusId", "MaintainanceStatusName", MaintainanceStatus);
        }
        private void PopulateMaintainanceTypeDropDownList(object MaintainanceType = null)
        {
            var MaintainanceTypeQuery = from d in new GFleetModelContainer().MaintainanceTypes
                                        orderby d.MaintainanceType1
                                        select d;
            ViewBag.MaintainanceType = new SelectList(MaintainanceTypeQuery, "MaintainanceTypeId", "MaintainanceType1", MaintainanceType);
        }
        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            int DealerId = Service.Utility.getClientId().Value;
            var VehicleQuery = from d in new GFleetModelContainer().vw_VehicleDetailsV3.
                                        Where(v => v.ClientId == DealerId)
                               orderby d.VehicleRegCode
                               select d;
            ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "VehicleRegCode", Vehicle);
        }
        private void PopulateMaintainanceComponentDropDownList(object MaintainanceComponent = null)
        {
            var MaintainanceComponentQuery = from d in new GFleetModelContainer().MaintainanceComponents
                                             orderby d.ComponentName
                                             select d;
            ViewBag.MaintainanceComponent = new SelectList(MaintainanceComponentQuery, "MaintainanceComponentId", "ComponentName", MaintainanceComponent);
        }

        private ResponseModel UpdateMaintainaceDetailComponent(string[] selectedComponent, string[] selectedQuantity,
           string[] selectedCost, string[] MaintainanceDetailId, Maintainance mMaintainance, Maintainance mMaintainances)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {

                if (mMaintainance.MaintainanceId > 0)
                {

                    //mMaintainance.MaintainanceDetails.Load();// = entities.MaintainanceDetails.Where(v => v.MaintainanceId.Equals(mMaintainance.MaintainanceId)).;
                    //Maintainance data
                    mMaintainances = entities.Maintainances.SingleOrDefault(v => v.MaintainanceId.Equals(mMaintainance.MaintainanceId));
                    //mMaintainance.MaintainanceDetails = entities.MaintainanceDetails.SingleOrDefault(v => v.MaintainanceDetailId.Equals(mMaintainance.));


                }
                if (selectedComponent != null)
                {
                    for (int i = 0; i < selectedComponent.Count(); i++)
                    {
                        int main_detailId = (MaintainanceDetailId == null || MaintainanceDetailId.Count() <= i || String.IsNullOrEmpty(MaintainanceDetailId[i].ToString())) ? -1 : int.Parse(MaintainanceDetailId[i]);

                        if (main_detailId == -1)
                        {

                            mMaintainances.MaintainanceDetails.Add(new MaintainanceDetail()
                            {
                                MaintainanceId = mMaintainance.MaintainanceId,
                                ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                Datemodified = DateTime.Parse(DateTime.Now.ToString()),
                                MaintainanceComponentId = int.Parse(selectedComponent[i]),
                                Quantity = int.Parse(selectedQuantity[i]),
                                Cost = decimal.Parse(selectedCost[i])
                            });
                        }
                        else if (mMaintainances.MaintainanceDetails.Any(v => v.MaintainanceDetailId.Equals(main_detailId)))
                        {
                            MaintainanceDetail data = mMaintainance.MaintainanceDetails.Where(v => v.MaintainanceDetailId.Equals(main_detailId)).FirstOrDefault();
                            if (data != null)
                            {
                                data.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                data.Datemodified = DateTime.Parse(DateTime.Now.ToString());
                                data.MaintainanceComponentId = int.Parse(selectedComponent[i]);
                                data.Quantity = int.Parse(selectedQuantity[i]);
                                data.Cost = decimal.Parse(selectedCost[i]);
                            }
                        }
                    }

                    HashSet<int> MaintainanceDetailIdsHS = new HashSet<int>();
                    if (MaintainanceDetailId != null)
                    {
                        foreach (string k in MaintainanceDetailId)
                        {
                            if (!String.IsNullOrEmpty(k) || !k.Equals("0"))
                                MaintainanceDetailIdsHS.Add(int.Parse(k));
                        }
                        HashSet<int> MaintainanceDetailIds = new HashSet<int>
                            (mMaintainances.MaintainanceDetails.Select(c => c.MaintainanceDetailId));

                        HashSet<int> to_Reomve = new HashSet<int>(MaintainanceDetailIds.Except(MaintainanceDetailIdsHS));

                        foreach (int h in to_Reomve)
                        {
                            if (h != 0)
                            {
                                entities.DeleteObject(mMaintainances.MaintainanceDetails.SingleOrDefault(v => v.MaintainanceDetailId.Equals(h)));
                                //mMaintainance.MaintainanceDetails.Remove(mMaintainance.MaintainanceDetails.SingleOrDefault(v => v.MaintainanceDetailId.Equals(h)));
                            }
                        }
                    }
                }
                ResponseModel _rm = new ResponseModel() { IsUpdate = true };
                try
                {
                    bool IsSave = false;
                    if (mMaintainance.MaintainanceId > 0)
                    {
                        mMaintainance = (Maintainance)entities.Maintainances.ApplyCurrentValues(mMaintainance);
                        IsSave = true;
                        entities.SaveChanges();
                    }
                    else
                    {
                        entities.Maintainances.AddObject(mMaintainance);
                        entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    }

                    _rm.Status = true;
                    _rm.Message = IsSave ? "Maintanance for vehicle " + mMaintainance.Vehicle.RegistrationNo + " successfully updated!" : "Maintanance for vehicle successfully saved!";

                    return _rm;
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, System.Web.HttpContext.Current);
                    _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                    return _rm;
                }

            }
        }

    }
}