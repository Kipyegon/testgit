﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;
using System.Threading.Tasks;

namespace GFleetV3.Controllers
{
    public class MessagesController : Controller
    {
        [Authorize]
        public async Task<ActionResult> SendMessage()
        {
            int ClientId = Service.Utility.getClientId().Value;

            return View("SendMessage", await MessageParamObject.getMessageParamObjectAsync(ClientId));
        }

        private GFleetModelContainer db = new GFleetModelContainer();


        [HttpPost]
        public JsonResult Message(string [] ContactId, string Message)
        {
            ModelContacts mm = new ModelContacts();
            ResponseModel response = new ResponseModel();
            string PersonId = string.Join(",", ContactId);

            response = mm.SendMessage(PersonId, Message);
            return Json(response);
        }
    }
}