﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class OrganisationController : Controller
    {


        //
        // GET: /Organisation/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_OrganisationGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedOrganisationModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelOrganisation OrganisationModel = new ModelOrganisation();
            const int pageSize = 1000;
            var totalRows = OrganisationModel.CountOrganisation();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "OrganisationId";

            var Organisation = OrganisationModel.GetOrganisationPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedOrganisationModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Organisation = Organisation
            };
            return data;
        }

        //
        // GET: /Organisation/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateRoleDropDownList();
            PopulateOrgTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                ViewBag.HasAc = false;
                return View("_Organisation");
            }
            else
            {
                PopulateRoleDropDownList();
                return View("_Organisation");
            }
        }

        //
        // POST: /Organisation/Create

        [HttpPost]
        [Authorize]
        //[Authorization]
        public ActionResult CreateEditOrganisation(OrganisationRegisterModel mOrganisationReg, string Command)
        {
            ModelOrganisation OrganisationModel = new ModelOrganisation();
            if (!ModelState.IsValid)
            {

                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                return Json(OrganisationModel.CreateOrganisationAdminUser(mOrganisationReg));
            }

            //else if (Command == "Update")
            //{
            //    mOrganisationReg.OrganisationModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
            //    mOrganisationReg.OrganisationModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
            //    mOrganisationReg.OrganisationModel.ClientId = Service.Utility.getClientId().Value;
            //    return this.Json(OrganisationModel.UpdateOrganisation(mOrganisationReg.OrganisationModel));
            //}
            else if (Command == "Update")
            {
                mOrganisationReg.OrganisationModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mOrganisationReg.OrganisationModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mOrganisationReg.OrganisationModel.ClientId = Service.Utility.getClientId().Value;
                return this.Json(OrganisationModel.UpdateOrgAdminAccount(mOrganisationReg));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }
        //
        // GET: /Organisation/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            var data = OrganisationRegisterModel.getOrganisationRegisterModel(id);

            PopulateRoleDropDownList();
            PopulateOrgTypeDropDownList();

            if (Request.IsAjaxRequest())
            {
                if(ModelOrganisation.orgHasAdmin(id))
                {
                    ViewBag.HasAc = ModelOrganisation.orgHasAdmin(id);
                    ViewBag.IsUpdate = true;
                }
                else{
                    ViewBag.HasAc = false;
                    ViewBag.IsUpdate = true;
                }
               
                return View("_Organisation", data);
            }
            else

                return View("_Organisation", data);
        }

        [Authorize]
        [Authorization]
        public ActionResult EditOrgAc(int id)
        {
            var data = OrganisationRegisterModel.getOrganisationRegisterModel(id);

            PopulateRoleDropDownList();
            PopulateOrgTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                //ViewBag.IsUpdate = ModelOrganisation.orgHasAdmin(id);
                ViewBag.HasAc = ModelOrganisation.orgHasAdmin(id);
                ViewBag.IsUpdate = false;
                return View("_Organisation", data);
            }
            else

                return View("_Organisation", data);
        }
        //
        // GET: /Organisation/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult DeleteOrg(int id)
        {
            ModelOrganisation OrganisationModel = new ModelOrganisation();
            var data = OrganisationRegisterModel.getOrganisationRegisterModel(id);
           // var data = OrganisationModel.DeleteOrganisation(id);

            return View("DeletePop",data);
        }
        //[Authorization]
        public ActionResult Delete(OrganisationRegisterModel mOrganisationReg)
        {
            ModelOrganisation OrganisationModel = new ModelOrganisation();
            var data = OrganisationModel.DeleteOrganisation(mOrganisationReg.OrganisationModel.OrganisationId);

            return Json(data);
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] OrganisationRoles = System.Configuration.ConfigurationManager.AppSettings["OrganisationRole"].ToString().Split(',');
            var RolesQuery = from d in new GFleetModelContainer().aspnet_Roles.Where(m => OrganisationRoles.Contains(m.RoleName))
                             orderby d.RoleName
                             select d;
            ViewBag.RoleName = new SelectList(RolesQuery, "RoleName", "RoleName", RoleName);
        }

        private void PopulateOrgTypeDropDownList(object OrganisationType = null)
        {
            var OrgTypeQuery = from d in new GFleetModelContainer().OrganisationTypes
                               orderby d.OrganisationTypeName
                               select d;
            ViewBag.OrgType = new SelectList(OrgTypeQuery, "OrganisationTypeId", "OrganisationTypeName", OrganisationType);
        }
    }
}