﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class OrganisationDepartmentController : Controller
    {

        //
        // GET: /OrganisationDepartment/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_OrganisationDepartmentGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedOrganisationDepartmentModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelOrganisationDepartment groupModel = new ModelOrganisationDepartment();
            const int pageSize = 100;
            var totalRows = groupModel.CountOrganisationDepartment();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "OrganisationDeptId";

            var group = groupModel.GetOrganisationDepartmentPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedOrganisationDepartmentModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                OrganisationDepartment = group
            };
            return data;
        }

        //
        // GET: /OrganisationDepartment/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_OrganisationDepartment");
            }
            else
            {
                return View("_OrganisationDepartment");
            }
        }

        //
        // POST: /OrganisationDepartment/Create

        [HttpPost]
        [Authorize]
       // [Authorization]
        public ActionResult createEditOrganisationDepartment(OrganisationDepartment mOrganisationDepartment, string Command)
        {
            ModelOrganisationDepartment groupModel = new ModelOrganisationDepartment();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mOrganisationDepartment.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mOrganisationDepartment.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = groupModel.CreateOrganisationDepartment(mOrganisationDepartment);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mOrganisationDepartment.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mOrganisationDepartment.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(groupModel.UpdateOrganisationDepartment(mOrganisationDepartment));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
        //
        // GET: /OrganisationDepartment/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelOrganisationDepartment groupModel = new ModelOrganisationDepartment();
            var data = groupModel.GetOrganisationDepartment(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_OrganisationDepartment", data);
        }

        //
        // GET: /OrganisationDepartment/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
    }
}