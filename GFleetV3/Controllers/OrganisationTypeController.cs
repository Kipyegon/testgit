﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class OrganisationTypeController : Controller
    {
        //
        // GET: /OrganisationType/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_OrganisationTypeGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedOrganisationTypeModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelOrganisationType OrganisationTypeModel = new ModelOrganisationType();
            const int pageSize = 25;
            var totalRows = OrganisationTypeModel.CountOrganisationType();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "OrganisationTypeId";

            var OrganisationType = OrganisationTypeModel.GetOrganisationTypePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedOrganisationTypeModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                OrganisationType = OrganisationType
            };
            return data;
        }
        //
        // GET: /OrganisationType/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_OrganisationType");
            }
            else
            {
                return View("_OrganisationType");
            }
        }

        //
        // POST: /OrganisationType/Create

        [HttpPost]
        [Authorize]
       // [Authorization]
        public ActionResult CreateEditOrganisationType(OrganisationType mOrganisationType, string Command)
        {
            ModelOrganisationType OrganisationTypeModel = new ModelOrganisationType();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mOrganisationType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mOrganisationType.ClientId = Service.Utility.getClientId().Value;
                mOrganisationType.DateModified = DateTime.Parse(DateTime.Now.ToString());

                ResponseModel rm = OrganisationTypeModel.CreateOrganisationType(mOrganisationType);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mOrganisationType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mOrganisationType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mOrganisationType.ClientId = Service.Utility.getClientId().Value;
                return this.Json(OrganisationTypeModel.UpdateOrganisationType(mOrganisationType));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /OrganisationType/Edit/5
       [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelOrganisationType OrganisationTypeModel = new ModelOrganisationType();
            var data = OrganisationTypeModel.GetOrganisationType(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }

            return View("_OrganisationType", data);
        }

        //
        // GET: /CustomerType/Delete/5
          [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /CustomerType/Delete/5

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}