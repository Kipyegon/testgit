﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class PriceController : Controller
    {

        //
        // GET: /Price/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_PriceGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedPriceModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelPrice PriceModel = new ModelPrice();
            const int pageSize = 100;
            var totalRows = PriceModel.CountPrice();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "PriceId";

            var Price = PriceModel.GetPricePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedPriceModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Price = Price
            };
            return data;
        }

        //
        // GET: /Price/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_Price");
            }
            else
            {
                return View("_Price");
            }
        }

        //
        // POST: /Price/Create

        [HttpPost]
        [Authorize]
       // [Authorization]
        public ActionResult createEditPrice(Price mPrice, string Command)
        {
            ModelPrice PriceModel = new ModelPrice();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mPrice.ClientId = Service.Utility.getClientId().Value;
                mPrice.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mPrice.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = PriceModel.CreatePrice(mPrice);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mPrice.ClientId = Service.Utility.getClientId().Value;
                mPrice.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mPrice.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(PriceModel.UpdatePrice(mPrice));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }

        //
        // GET: /Price/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelPrice PriceModel = new ModelPrice();
            var data = PriceModel.GetPrice(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_Price", data);
        }


        //
        // GET: /Price/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
    }
}