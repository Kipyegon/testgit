﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Web.Routing;
using System.Web.Security;

namespace GFleetV3.Controllers
{
    public class RecepientAlertTypeController : Controller
    {
        //
        // GET: /RecepientAlertType/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_RecepientAlertTypeGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedRecepientAlertTypeModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelRecepientAlertType RecepientAlertTypeModel = new ModelRecepientAlertType();
            const int pageSize = 25;
            var totalRows = RecepientAlertTypeModel.CountRecepientAlertType();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "AlertReceiverAlertId";

            var RecepientAlertType = RecepientAlertTypeModel.GetRecepientAlertTypePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedRecepientAlertTypeModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                RecepientAlertType = RecepientAlertType
            };
            return data;
        }

        //
        // GET: /RecepientAlertType/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateAlertTypeDropDownList();
            PopulateAlertRecipientDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_RecepientAlertType");
            }
            else
            {
                return View("_RecepientAlertType");
            }
        }

        //
        // POST: /RecepientAlertType/Create

        [HttpPost]
        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditRecepientAlertType(AlertReceiverAlert mRecepientAlertType, string Command)
        {
            ModelRecepientAlertType RecepientAlertTypeModel = new ModelRecepientAlertType();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mRecepientAlertType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mRecepientAlertType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mRecepientAlertType.ClientId = Service.Utility.getClientId().Value;

                ResponseModel rm = RecepientAlertTypeModel.CreateRecepientAlertType(mRecepientAlertType);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mRecepientAlertType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mRecepientAlertType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mRecepientAlertType.ClientId = Service.Utility.getClientId().Value;
                return this.Json(RecepientAlertTypeModel.UpdateRecepientAlertType(mRecepientAlertType));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /RecepientAlertType/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelRecepientAlertType RecepientAlertTypeModel = new ModelRecepientAlertType();
            var data = RecepientAlertTypeModel.GetRecepientAlertType(id);
            PopulateAlertTypeDropDownList(data);
            PopulateAlertRecipientDropDownList(data);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_RecepientAlertType", data);
            }
            else

                return View(data);
        }

        //
        // GET: /RecepientAlertType/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /RecepientAlertType/Delete/5

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        private void PopulateAlertTypeDropDownList(object AlertType = null)
        {
            var AlertTypeQuery = from d in new GFleetModelContainer().AlertTypes
                                 orderby d.AlertTypeName
                                 select d;
            ViewBag.AlertType = new SelectList(AlertTypeQuery, "AlertTypeId", "AlertTypeName", AlertType);
        }
        private void PopulateAlertRecipientDropDownList(object AlertRecipient = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var AlertRecipientQuery = from d in new GFleetModelContainer().AlertRecipients.Where(v => v.ClientId == ClientId)
                                      orderby d.Names
                                      select d;
            ViewBag.AlertRecipient = new SelectList(AlertRecipientQuery, "AlertRecipientId", "Names", AlertRecipient);
        }
    }
}