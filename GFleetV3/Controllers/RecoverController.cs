﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class RecoverController : Controller
    {
        //
        // GET: /Recover/

        [Authorize]
        [Authorization]
        public ActionResult Index()
        {
            PopulateDriverNVehicleDropDownList();
            return View();
        }

        [Authorize]
        public JsonResult getGridData(int recover, int lost)
        {
            return Json(RecoverModel.getWebRecoverData(recover, lost));
        }

        [Authorize]
        public JsonResult getLatestData(string highest, int recover, int lost)
        {
            return Json(RecoverModel.getLatestRecoverData(DateTime.Parse(highest), recover, lost));
        }

        [Authorize]
        public JsonResult getLostData(int recover)
        {
            return Json(RecoverModel.getLostVehicle(recover));
        }

        [Authorize]
        private void PopulateDriverNVehicleDropDownList(object vehicle = null)
        {

            int clientId = Service.Utility.getClientId().Value;

            var vehicleQuery = from d in new GFleetModelContainer().vw_VehicleDetailsV3
                               where d.ClientId == clientId
                               orderby d.VehicleRegCode ascending
                               select new { d.VehicleRegCode, d.VehicleId };
            ViewBag.vehicle = new SelectList(vehicleQuery, "VehicleId", "VehicleRegCode", vehicle);
        }
    }
}
