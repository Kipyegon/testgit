﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/

        //public ActionResult Index()
        //{
        //    return View(gridData());
        //}

        //private DriverStatusReport gridData()
        //{
        //    var report = ReportModel.GetDriverStatusRpt();

        //    var data = new DriverStatusReport()
        //    {
        //        //TotalRows = totalRows,
        //        //PageSize = pageSize,
        //        vw_DriverStatus = report
        //    };
        //    return data;
        //}

        #region DriverAssignmentStatus Report

        //[Authorize(Roles = "super admin,Administrator,company admin,Company staff")]
        //public ActionResult DriverAssignmentStatus()
        //{
        //    PopulateDriverNVehicleDropDownList();

        //    return View("DriverAssignmentStatus",null);
        //}


        [Authorize]
        [Authorization]
        public ActionResult DriverAssignmentStatus(int page = 1, string sort = "Reg", string sortDir = "ASC", string driver = null, string vehicle = null,
            string startdate = null, string enddate = null)
        {

            PopulateDriverNVehicleDropDownList();

            if (string.IsNullOrEmpty(startdate) || string.IsNullOrEmpty(enddate))
            {
                List<vw_DriverAssignmentStatus> data = new List<vw_DriverAssignmentStatus>();
                return View("DriverAssignmentStatus", data);
            }

            const int pageSize = 25;
            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "PersonId";
            string orderCriteria = "it." + sort + " " + sortDir;


            return View("DriverAssignmentStatus", ReportModel.getDriverAssignmentStatus(page, pageSize, orderCriteria,
                driver, vehicle, startdate, enddate));
        }


        [Authorize]
        [Authorization]
        public ActionResult DriverStatus(int page = 1, string sort = "Reg", string sortDir = "ASC", string DriverStatus = null)
        {

            PopulateDriverStatusDropDownList();

            if (string.IsNullOrEmpty(DriverStatus))
            {
                List<vw_DriverStatusRpt> data = new List<vw_DriverStatusRpt>();
                return View("DriverStatus", data);
            }

            const int pageSize = 25;
            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "PersonId";
            string orderCriteria = "it." + sort + " " + sortDir;


            return View("DriverStatus", ReportModel.GetDriverStatusRpt(page, pageSize, orderCriteria, DriverStatus));
        }


        [Authorize]
        [Authorization]
        public ActionResult DriverLicenceType(int page = 1, string sort = "Reg", string sortDir = "ASC", string DriverLicenceType = null)
        {

            PopulateDriverLicenceTypeDropDownList();

            if (string.IsNullOrEmpty(DriverLicenceType))
            {
                List<vw_DriverLicenceTypeRpt> data = new List<vw_DriverLicenceTypeRpt>();
                return View("DriverLicenceType", data);
            }

            const int pageSize = 25;
            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "PersonId";
            string orderCriteria = "it." + sort + " " + sortDir;


            return View("DriverLicenceType", ReportModel.GetDriverLicenceRpt(page, pageSize, orderCriteria, DriverLicenceType));
        }


        [Authorize]
        [Authorization]
        public ActionResult VehicleFuelDetails(int page = 1, string sort = "Reg", string sortDir = "ASC", string vehicle = null,
            string startdate = null, string enddate = null)
        {

            PopulateVehicleDropDownList();

            if (string.IsNullOrEmpty(startdate) || string.IsNullOrEmpty(enddate))
            {
                List<vw_VehicleFuelMngtRpt> data = new List<vw_VehicleFuelMngtRpt>();
                return View("VehicleFuelDetails", data);
            }

            const int pageSize = 25;
            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "VehicleId";
            string orderCriteria = "it." + sort + " " + sortDir;

            var datas =ReportModel.GetVehicleFuelRpt(page, pageSize, orderCriteria, vehicle, startdate, enddate);
            return View("VehicleFuelDetails", datas);
        }


        [Authorize]
        [Authorization]
        public ActionResult VehicleLicenceDetails(int page = 1, string sort = "Reg", string sortDir = "ASC", string VehicleLicenceType = null)
        {

            PopulateVehicleLicenceDropDownList();

            if (string.IsNullOrEmpty(VehicleLicenceType))
            {
                List<vw_VehicleLicenceDetailRpt> data = new List<vw_VehicleLicenceDetailRpt>();
                return View("VehicleLicenceDetails", data);
            }

            const int pageSize = 25;
            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "LicenceTypeId";
            string orderCriteria = "it." + sort + " " + sortDir;


            return View("VehicleLicenceDetails", ReportModel.GetVehicleLicenceDetails(page, pageSize, orderCriteria, VehicleLicenceType));
        }
                
        private void PopulateDriverNVehicleDropDownList(object vehicle = null, object driver = null)
        {

            int clientId = Service.Utility.getClientId().Value;

            var vehicleQuery = from d in new GFleetModelContainer().vw_VehicleDetailsV3
                               where d.ClientId == clientId
                               orderby d.VehicleRegCode ascending
                               select new { d.VehicleRegCode, d.VehicleId };
            ViewBag.vehicle = new SelectList(vehicleQuery, "VehicleId", "VehicleRegCode", vehicle);

            var driverQuery = from d in new GFleetModelContainer().People.OfType<Driver>()
                              where d.ClientId == clientId
                              orderby d.FirstName ascending
                              select new
                              {
                                  FName = string.IsNullOrEmpty(d.FirstName) ? "" : d.FirstName
                                      + (string.IsNullOrEmpty(d.LastName) ? "" : " " + d.LastName),
                                  d.PersonId
                              };
            ViewBag.driver = new SelectList(driverQuery, "PersonId", "FName", driver);
        }

        private void PopulateDriverStatusDropDownList(object DriverStatus = null)
        {
            var DriverStatusQuery = from d in new GFleetModelContainer().DriverStatus
                                    orderby d.DriverStatusName
                                    select new { d.DriverStatusName, d.DriverStatusId };
            ViewBag.DriverStatus = new SelectList(DriverStatusQuery, "DriverStatusId", "DriverStatusName", DriverStatus);
        }

        private void PopulateDriverLicenceTypeDropDownList(object DriverLicenceType = null)
        {
            var DriverLicenceTypesQuery = from d in new GFleetModelContainer().LicenceTypes
                                          orderby d.DriverLicenceType
                                          select d;
            ViewBag.DriverLicenceType = new SelectList(DriverLicenceTypesQuery, "LicenceTypeId", "DriverLicenceType", DriverLicenceType);
        }

        private void PopulateVehicleDropDownList(object vehicle = null)
        {

            int clientId = Service.Utility.getClientId().Value;

            var vehicleQuery = from d in new GFleetModelContainer().vw_VehicleDetailsV3
                               where d.ClientId == clientId
                               orderby d.VehicleRegCode ascending
                               select new { d.VehicleRegCode, d.VehicleId };
            ViewBag.vehicle = new SelectList(vehicleQuery, "VehicleId", "VehicleRegCode", vehicle);
        }

        private void PopulateVehicleLicenceDropDownList(object VehicleLicenceTypes = null)
        {


            var VehicleLicenceQuery = from d in new GFleetModelContainer().VehicleLicenceTypes
                                      orderby d.VehicleLicenceTypeName ascending
                                      select d;
            ViewBag.VehicleLicence = new SelectList(VehicleLicenceQuery, "VehicleLicenceTypeId", "VehicleLicenceTypeName", VehicleLicenceTypes);
        }
        #endregion
    }
}
