﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class SMSFormatController : Controller
    {
        //
        // GET: /SMSFormat/
        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_SMSFormatGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedSMSFormatModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelSMSFormat smsformatmodel = new ModelSMSFormat();
            const int pageSize = 25;
            var totalRows = smsformatmodel.CountSMSFormat();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "SMSFormatId";

            var smsformat = smsformatmodel.GetSMSFormatPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedSMSFormatModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                SMSFormat = smsformat
            };
            return data;
        }
        //
        // GET: /SMSFormat/Create
        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
            }
            PopulateDispatchSMSField();
            PopulateSmsTypeDropDownList();
            return View("_DispatchSMSFormat");
        }

        //
        // POST: /SMSFormat/Create

        [HttpPost]
        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditDispatchSMSFormat(DispatchSMSFormat mSMSFormat, string Command)
        {
            ModelSMSFormat smsformatmodel = new ModelSMSFormat();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mSMSFormat.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mSMSFormat.ClientId = Service.Utility.getClientId().Value;
                mSMSFormat.DateModified = DateTime.Parse(DateTime.Now.ToString());

                ResponseModel rm = smsformatmodel.CreateSMSFormat(mSMSFormat);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mSMSFormat.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mSMSFormat.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mSMSFormat.ClientId = Service.Utility.getClientId().Value;
                return this.Json(smsformatmodel.UpdateSMSFormat(mSMSFormat));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /SMSFormat/Edit/5
        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelSMSFormat smsformatmodel = new ModelSMSFormat();
            var data = smsformatmodel.GetSMSFormat(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            PopulateDispatchSMSField();
            PopulateSmsTypeDropDownList(data);
            return View("_DispatchSMSFormat", data);
        }

        //
        // GET: /SMSFormat/Delete/5
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /SMSFormat/Delete/5

        [HttpPost]
        [Authorize]
        [Authorization]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void PopulateDispatchSMSField(object DispatchSMSField = null)
        {
            var viewModel = new List<DispatchSMSField>();
            viewModel.AddRange(new GFleetModelContainer().DispatchSMSFields.ToList<DispatchSMSField>());
            ViewBag.DispatchSMSField = viewModel;
        }

        private void PopulateSmsTypeDropDownList(object SmsType = null)
        {
            var SmsTypeQuery = from d in new GFleetModelContainer().SmsTypes
                               orderby d.SmsTypeName
                               select d;
            ViewBag.SmsType = new SelectList(SmsTypeQuery, "SmsTypeId", "SmsTypeName", SmsType);
        }
    }
}