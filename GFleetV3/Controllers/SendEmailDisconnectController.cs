﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class SendEmailDisconnectController : Controller
    {
        public void sendVehicleDisconnectEmail()
        {
            new Thread(() =>
            {
                var ClientIds = from d in new GFleetModelContainer().Clients
                                select d.ClientId;

                foreach (int ClientId in ClientIds)
                {
                    SendEmailDisconnectModel.sendEmailDisconnect(ClientId);
                }
            }).Start();
        }
    }
}