﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    [Authorize(Roles = "Super Admin")]
    public class SystemAuthorizationController : Controller
    {
        private GFleetModelContainer db = new GFleetModelContainer();

        #region ControllerActionClientType
        //
        // GET: /ControllerActionRole/
       
        public ActionResult ControllerActionClientTypeIndex()
        {
            var controlleractionclienttypes = db.ControllerActionClientTypes.Include("ClientType").Include("ControllerAction");
            return View(controlleractionclienttypes.ToList());
        }

        //
        // GET: /ControllerActionClientType/Details/5
        
        public ActionResult ControllerActionClientTypeDetails(int id = 0)
        {
            ControllerActionClientType controlleractionclienttype = db.ControllerActionClientTypes.Single(c => c.ControllerActionClientTypeId == id);
            if (controlleractionclienttype == null)
            {
                return HttpNotFound();
            }
            return View(controlleractionclienttype);
        }

        //
        // GET: /ControllerActionClientType/Create
     
        public ActionResult ControllerActionClientTypeCreate()
        {
            PopulateControllerActionData();
            ViewBag.ClientTypeId = new SelectList(db.ClientTypes, "ClientTypeId", "ClientTypeName");
            return View("ControllerActionClientTypeCreate");
        }

        //
        // POST: /ControllerActionClientType/Create

       
        public ActionResult CreateEditControllerActionClientType(string[] selectedControllerActions, ControllerActionClientType controlleractionclienttype, int ControllerId, int ActionId, string Command)
        {
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                ControllerActionClientType aar = db.ControllerActionClientTypes.Single(v => v.ControllerAction.ControllerId.Equals
                    (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId));
                controlleractionclienttype.ControllerActionId = aar.ControllerActionId;
                db.ControllerActionClientTypes.AddObject(controlleractionclienttype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            else if (Command == "Update")
            {
                controlleractionclienttype.ControllerActionClientTypeId = new GFleetModelContainer().ControllerActionClientTypes.Single(v => v.ControllerAction.ControllerId.Equals
                       (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId)).ControllerActionClientTypeId;
                db.ControllerActionClientTypes.Attach(controlleractionclienttype);
                db.ObjectStateManager.ChangeObjectState(controlleractionclienttype, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
      
        public ActionResult CreateControllerActionClientType(string selectedControllerActions, string ClientTypeId, Boolean chkIsEnabled)
        {
            return Json(SystemAuthorizationModel.createControllerActionClientTypes(selectedControllerActions, ClientTypeId, chkIsEnabled));
        }
       
        public ActionResult CreateClientTypeControllerAction(int selectedControllers, string selectedAction)
        {
            return Json(SystemAuthorizationModel.createControllerAction(selectedControllers, selectedAction));
        }

        [HttpPost]
     
        public ActionResult ControllerActionClientTypeCreate(ControllerActionClientType controlleractionclienttype, int ControllerId, int ActionId)
        {
            if (ModelState.IsValid)
            {
                ControllerActionClientType aar = db.ControllerActionClientTypes.SingleOrDefault(v => v.ControllerAction.ControllerId.Equals
                    (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId));
                controlleractionclienttype.ControllerActionId = aar.ControllerActionId;
                db.ControllerActionClientTypes.AddObject(controlleractionclienttype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientTypeId = new SelectList(db.ClientTypes, "ClientTypeId", "ClientTypeName", controlleractionclienttype.ClientTypeId);
            ViewBag.ControllerId = new SelectList(db.GFleetControllers, "ControllerId", "ControllerName", controlleractionclienttype.ControllerAction.Controller.ControllerId);
            ViewBag.ActionId = new SelectList(db.Actions, "ActionId", "ActionName", controlleractionclienttype.ControllerAction.Action.ActionId);
            return View(controlleractionclienttype);
        }

        //
        // GET: /ControllerActionClientType/Edit/5
      
        public ActionResult ControllerActionClientTypeEdit(int id)
        {
            ControllerActionClientType controlleractionclienttype = db.ControllerActionClientTypes.Single(c => c.ControllerActionClientTypeId == id);
            if (controlleractionclienttype == null)
            {
                return HttpNotFound();
            }
            ViewBag.IsUpdate = true;
            ViewBag.ClientTypeId = new SelectList(db.ClientTypes, "ClientTypeId", "ClientTypeName", controlleractionclienttype.ClientTypeId);
            //ViewBag.ControllerActionId = new SelectList(db.ControllerActions, "ControllerActionId", "ControllerActionId", controlleractionclienttype.ControllerActionId);
            ViewBag.ControllerId = new SelectList(db.GFleetControllers, "ControllerId", "ControllerName", controlleractionclienttype.ControllerAction.Controller.ControllerId);
            ViewBag.ActionId = new SelectList(db.Actions, "ActionId", "ActionName", controlleractionclienttype.ControllerAction.Action.ActionId);
            return View("Create", controlleractionclienttype);
        }

        //
        // POST: /ControllerActionClientType/Edit/5

        //[HttpPost]
        //public ActionResult Edit(ControllerActionClientType controlleractionclienttype, int ControllerId, int ActionId)
        //{
        //    if (ModelState.IsValid)
        //    {

        //        controlleractionclienttype.ControllerActionId = new GFleetModelContainer().ControllerActionClientTypes.SingleOrDefault(v => v.ControllerAction.ControllerId.Equals
        //               (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId)).ControllerActionId;

        //        db.ControllerActionClientTypes.Attach(controlleractionclienttype);

        //        db.ObjectStateManager.ChangeObjectState(controlleractionclienttype, EntityState.Modified);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", controlleractionclienttype.RoleId);
        //    ViewBag.ControllerId = new SelectList(db.GFleetControllers, "ControllerId", "ControllerName", controlleractionclienttype.ControllerAction.Controller.ControllerId);
        //    ViewBag.ActionId = new SelectList(db.Actions, "ActionId", "ActionName", controlleractionclienttype.ControllerAction.Action.ActionId);
        //    return View(controlleractionclienttype);
        //}

        //
        // GET: /ControllerActionClientType/Delete/5
       
        public ActionResult ControllerActionClientTypeDelete(int id = 0)
        {
            ControllerActionClientType controlleractionclienttype = db.ControllerActionClientTypes.Single(c => c.ControllerActionClientTypeId == id);
            if (controlleractionclienttype == null)
            {
                return HttpNotFound();
            }
            return View(controlleractionclienttype);
        }

        //
        // POST: /ControllerActionClientType/Delete/5

        [HttpPost, ActionName("Delete")]
    
        public ActionResult ControllerActionClientTypeDeleteConfirmed(int id)
        {
            ControllerActionClientType controlleractionclienttype = db.ControllerActionClientTypes.Single(c => c.ControllerActionClientTypeId == id);
            db.ControllerActionClientTypes.DeleteObject(controlleractionclienttype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion


        #region ControllerActionRole
        //
        // GET: /ControllerActionRole/
     
        public ActionResult Index()
        {
            var controlleractionroles = db.ControllerActionRoles.Include("aspnet_Roles").Include("ControllerAction");
            return View(controlleractionroles.ToList());
        }

        //
        // GET: /ControllerActionRole/Details/5
       
        public ActionResult Details(int id = 0)
        {
            ControllerActionRole controlleractionrole = db.ControllerActionRoles.Single(c => c.ControllerActionRoleId == id);
            if (controlleractionrole == null)
            {
                return HttpNotFound();
            }
            return View(controlleractionrole);
        }

        //
        // GET: /ControllerActionRole/Create
     
        public ActionResult Create()
        {
            PopulateControllerActionData();
            ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName");
            return View("Create");
        }

        //
        // POST: /ControllerActionRole/Create

      
        public ActionResult CreateEditControllerActionRole(string[] selectedControllerActions, ControllerActionRole controlleractionrole, int ControllerId, int ActionId, string Command)
        {
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                ControllerActionRole aar = db.ControllerActionRoles.Single(v => v.ControllerAction.ControllerId.Equals
                    (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId));
                controlleractionrole.ControllerActionId = aar.ControllerActionId;
                db.ControllerActionRoles.AddObject(controlleractionrole);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            else if (Command == "Update")
            {
                controlleractionrole.ControllerActionRoleId = new GFleetModelContainer().ControllerActionRoles.Single(v => v.ControllerAction.ControllerId.Equals
                       (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId)).ControllerActionRoleId;
                db.ControllerActionRoles.Attach(controlleractionrole);
                db.ObjectStateManager.ChangeObjectState(controlleractionrole, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
       
        public ActionResult CreateControllerActionRole(string selectedControllerActions, string RoleId, Boolean chkIsEnabled)
        {
            return Json(SystemAuthorizationModel.createControllerActionRoles(selectedControllerActions, RoleId, chkIsEnabled));
        }
       
        public ActionResult CreateControllerAction(int selectedControllers, string selectedAction)
        {
            return Json(SystemAuthorizationModel.createControllerAction(selectedControllers, selectedAction));
        }

        [HttpPost]
       
        public ActionResult Create(ControllerActionRole controlleractionrole, int ControllerId, int ActionId)
        {
            if (ModelState.IsValid)
            {
                ControllerActionRole aar = db.ControllerActionRoles.SingleOrDefault(v => v.ControllerAction.ControllerId.Equals
                    (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId));
                controlleractionrole.ControllerActionId = aar.ControllerActionId;
                db.ControllerActionRoles.AddObject(controlleractionrole);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", controlleractionrole.RoleId);
            ViewBag.ControllerId = new SelectList(db.GFleetControllers, "ControllerId", "ControllerName", controlleractionrole.ControllerAction.Controller.ControllerId);
            ViewBag.ActionId = new SelectList(db.Actions, "ActionId", "ActionName", controlleractionrole.ControllerAction.Action.ActionId);
            return View(controlleractionrole);
        }

        //
        // GET: /ControllerActionRole/Edit/5
        
        public ActionResult Edit(int id)
        {
            ControllerActionRole controlleractionrole = db.ControllerActionRoles.Single(c => c.ControllerActionRoleId == id);
            if (controlleractionrole == null)
            {
                return HttpNotFound();
            }
            ViewBag.IsUpdate = true;
            ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", controlleractionrole.RoleId);
            //ViewBag.ControllerActionId = new SelectList(db.ControllerActions, "ControllerActionId", "ControllerActionId", controlleractionrole.ControllerActionId);
            ViewBag.ControllerId = new SelectList(db.GFleetControllers, "ControllerId", "ControllerName", controlleractionrole.ControllerAction.Controller.ControllerId);
            ViewBag.ActionId = new SelectList(db.Actions, "ActionId", "ActionName", controlleractionrole.ControllerAction.Action.ActionId);
            return View("Create", controlleractionrole);
        }

        //
        // POST: /ControllerActionRole/Edit/5

        //[HttpPost]
        //public ActionResult Edit(ControllerActionRole controlleractionrole, int ControllerId, int ActionId)
        //{
        //    if (ModelState.IsValid)
        //    {

        //        controlleractionrole.ControllerActionId = new GFleetModelContainer().ControllerActionRoles.SingleOrDefault(v => v.ControllerAction.ControllerId.Equals
        //               (ControllerId) && v.ControllerAction.ActionId.Equals(ActionId)).ControllerActionId;

        //        db.ControllerActionRoles.Attach(controlleractionrole);

        //        db.ObjectStateManager.ChangeObjectState(controlleractionrole, EntityState.Modified);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.RoleId = new SelectList(db.aspnet_Roles, "RoleId", "RoleName", controlleractionrole.RoleId);
        //    ViewBag.ControllerId = new SelectList(db.GFleetControllers, "ControllerId", "ControllerName", controlleractionrole.ControllerAction.Controller.ControllerId);
        //    ViewBag.ActionId = new SelectList(db.Actions, "ActionId", "ActionName", controlleractionrole.ControllerAction.Action.ActionId);
        //    return View(controlleractionrole);
        //}

        //
        // GET: /ControllerActionRole/Delete/5
       
        public ActionResult Delete(int id = 0)
        {
            ControllerActionRole controlleractionrole = db.ControllerActionRoles.Single(c => c.ControllerActionRoleId == id);
            if (controlleractionrole == null)
            {
                return HttpNotFound();
            }
            return View(controlleractionrole);
        }

        //
        // POST: /ControllerActionRole/Delete/5

        [HttpPost, ActionName("Delete")]
       
        public ActionResult DeleteConfirmed(int id)
        {
            ControllerActionRole controlleractionrole = db.ControllerActionRoles.Single(c => c.ControllerActionRoleId == id);
            db.ControllerActionRoles.DeleteObject(controlleractionrole);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        #endregion

        #region Action

        //
        // GET: /Action/
     
        public ActionResult ActionIndex()
        {
            return View("ActionIndex", db.Actions.ToList());
        }

        //
        // GET: /Action/Details/5
      
        public ActionResult ActionDetails(int id = 0)
        {
            DataAccess.SQL.Action action = db.Actions.Single(a => a.ActionId == id);
            if (action == null)
            {
                return HttpNotFound();
            }
            return View("ActionDetails", action);
        }

        //
        // GET: /Action/Create
       
        public ActionResult ActionCreate()
        {
            return View("ActionCreate");
        }

        //
        // POST: /Action/Create

        [HttpPost]
       
        public ActionResult ActionCreate(DataAccess.SQL.Action action, string ActionName)
        {
            //if (ModelState.IsValid)
            try
            {
                action = new DataAccess.SQL.Action() { ActionName = ActionName };
                db.Actions.AddObject(action);
                db.SaveChanges();
                return RedirectToAction("ActionIndex");
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, System.Web.HttpContext.Current);
            }

            return View("ActionCreate", action);
        }

        //
        // GET: /Action/Edit/5
      
        public ActionResult ActionEdit(int id)
        {
            DataAccess.SQL.Action action = db.Actions.Single(a => a.ActionId == id);
            if (action == null)
            {
                return HttpNotFound();
            }
            ViewBag.IsUpdate = true;
            return View("ActionCreate", action);
        }

        //
        // POST: /Action/Edit/5

        [HttpPost]
      
        public ActionResult ActionEdit(int ActionId, string ActionName)
        {
            DataAccess.SQL.Action action = new DataAccess.SQL.Action()
            {
                ActionId = ActionId,
                ActionName = ActionName
            };
            //if (ModelState.IsValid)
            try
            {
                db.Actions.Attach(action);
                db.ObjectStateManager.ChangeObjectState(action, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("ActionIndex");
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
                var sms = ex.Message;
            }
            return View("ActionEdit", action);
        }

        //
        // GET: /Action/Delete/5
      
        public ActionResult ActionDelete(int id = 0)
        {
            DataAccess.SQL.Action action = db.Actions.Single(a => a.ActionId == id);
            if (action == null)
            {
                return HttpNotFound();
            }
            return View("ActionDelete", action);
        }

        //
        // POST: /Action/Delete/5
      
        [HttpPost, ActionName("ActionDelete")]
        public ActionResult ActionDeleteConfirmed(int id)
        {
            DataAccess.SQL.Action action = db.Actions.Single(a => a.ActionId == id);
            db.Actions.DeleteObject(action);
            db.SaveChanges();
            return RedirectToAction("ActionIndex");
        }

        #endregion

        #region Module
      
        public ActionResult ModuleIndex()
        {
            return View("ModuleIndex", db.Modules.ToList());
        }

        //
        // GET: /Module/Details/5
       
        public ActionResult ModuleDetails(int id = 0)
        {
            DataAccess.SQL.Module module = db.Modules.Single(m => m.ModuleId == id);
            if (module == null)
            {
                return HttpNotFound();
            }
            return View("ModuleDetails", module);
        }

        //
        // GET: /Module/Create
       
        public ActionResult ModuleCreate()
        {
            return View("ModuleCreate");
        }

        //
        // POST: /Module/Create

        [HttpPost]
       
        public ActionResult ModuleCreate(DataAccess.SQL.Module module)
        {
            if (ModelState.IsValid)
            {
                db.Modules.AddObject(module);
                db.SaveChanges();
                return RedirectToAction("ModuleIndex");
            }

            return View("ModuleCreate", module);
        }

        //
        // GET: /Module/Edit/5
      
        public ActionResult ModuleEdit(int id)
        {
            DataAccess.SQL.Module module = db.Modules.Single(m => m.ModuleId == id);
            if (module == null)
            {
                return HttpNotFound();
            }
            ViewBag.IsUpdate = true;
            return View("ModuleCreate", module);
        }

        //
        // POST: /Module/Edit/5

        [HttpPost]
     
        public ActionResult ModuleEdit(DataAccess.SQL.Module module)
        {
            if (ModelState.IsValid)
            {
                db.Modules.Attach(module);
                db.ObjectStateManager.ChangeObjectState(module, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("ModuleIndex");
            }
            return View("ModuleCreate", module);
        }

        //
        // GET: /Module/Delete/5
      
        public ActionResult ModuleDelete(int id = 0)
        {
            DataAccess.SQL.Module module = db.Modules.Single(m => m.ModuleId == id);
            if (module == null)
            {
                return HttpNotFound();
            }
            return View("ModuleDelete", module);
        }

        //
        // POST: /Module/Delete/5
       
        [HttpPost, ActionName("ModuleDelete")]
        public ActionResult ModuleDeleteConfirmed(int id)
        {
            DataAccess.SQL.Module module = db.Modules.Single(m => m.ModuleId == id);
            db.Modules.DeleteObject(module);
            db.SaveChanges();
            return RedirectToAction("ModuleIndex");
        }

        #endregion

        #region GfleetController
      
        public ActionResult GfleetControllerIndex()
        {
            var gfleetcontrollers = db.GFleetControllers.Include("Module");
            return View("GfleetControllerIndex", gfleetcontrollers.ToList().OrderBy(m => m.Description));
        }

        //
        // GET: /GfleetController/Details/5
      
        public ActionResult GfleetControllerDetails(int id = 0)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            if (gfleetcontroller == null)
            {
                return HttpNotFound();
            }
            return View("GfleetControllerDetails", gfleetcontroller);
        }

        //
        // GET: /GfleetController/Create
       
        public ActionResult GfleetControllerCreate()
        {
            ViewBag.ParentId = new SelectList(db.GFleetControllers.Where(v => !v.ParentId.HasValue), "ControllerId", "ControllerName");
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleId", "ModuleName");
            return View("GfleetControllerCreate");
        }
        //
        // POST: /GfleetController/Create

        [HttpPost]
    
        public ActionResult GfleetControllerCreate(GFleetController gfleetcontroller)
        {
            if (ModelState.IsValid)
            {
                db.GFleetControllers.AddObject(gfleetcontroller);
                db.SaveChanges();
                return RedirectToAction("GfleetControllerIndex");
            }

            ViewBag.ParentId = new SelectList(db.GFleetControllers.Where(v => !v.ParentId.HasValue), "ControllerId", "ControllerName", gfleetcontroller.ParentId);
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleId", "ModuleName", gfleetcontroller.ModuleId);
            return View("GfleetControllerCreate", gfleetcontroller);
        }

        //
        // GET: /GfleetController/Edit/5
       
        public ActionResult GfleetControllerEdit(int id = 0)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            if (gfleetcontroller == null)
            {
                return HttpNotFound();
            }

            ViewBag.ParentId = new SelectList(db.GFleetControllers.Where(v => !v.ParentId.HasValue), "ControllerId", "ControllerName", gfleetcontroller.ParentId);
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleId", "ModuleName", gfleetcontroller.ModuleId);
            return View("GfleetControllerEdit", gfleetcontroller);
        }

        //
        // POST: /GfleetController/Edit/5

        [HttpPost]
     
        public ActionResult GfleetControllerEdit(GFleetController gfleetcontroller)
        {
            if (ModelState.IsValid)
            {
                db.GFleetControllers.Attach(gfleetcontroller);
                db.ObjectStateManager.ChangeObjectState(gfleetcontroller, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("GfleetControllerIndex");
            }
            ViewBag.ParentId = new SelectList(db.GFleetControllers.Where(v => !v.ParentId.HasValue), "ControllerId", "ControllerName", gfleetcontroller.ParentId);
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleId", "ModuleName", gfleetcontroller.ModuleId);
            return View("GfleetControllerEdit", gfleetcontroller);
        }

        //
        // GET: /GfleetController/Delete/5
       
        public ActionResult GfleetControllerDelete(int id = 0)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            if (gfleetcontroller == null)
            {
                return HttpNotFound();
            }
            return View("GfleetControllerDelete", gfleetcontroller);
        }

        //
        // POST: /GfleetController/Delete/5
       
        [HttpPost, ActionName("GfleetControllerDelete")]
        public ActionResult GfleetControllerDeleteConfirmed(int id)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            db.GFleetControllers.DeleteObject(gfleetcontroller);
            db.SaveChanges();
            return RedirectToAction("GfleetControllerIndex");
        }


        #endregion

        #region GfleetControllerAction
       
        public ActionResult GfleetControllerActionIndex()
        {
            var gfleetcontrolleractions = db.ControllerActions.Include("Controller");
            return View("GfleetControllerActionIndex", gfleetcontrolleractions.ToList());
        }

        //
        // GET: /GfleetController/Details/5
     
        public ActionResult GfleetControllerActionDetails(int id = 0)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            if (gfleetcontroller == null)
            {
                return HttpNotFound();
            }
            return View("GfleetControllerActionDetails", gfleetcontroller);
        }

        //
        // GET: /GfleetControllerAction/Create
      
        public ActionResult GfleetControllerActionCreate()
        {
            //ViewBag.ParentId = new SelectList(db.GFleetControllers.Where(v => !v.ParentId.HasValue), "ControllerId", "ControllerName");
            //ViewBag.ActionId = new SelectList(db.Actions, "ActionId", "ActionName");

            PopulateActionData();
            PopulateControllerData();
            return View("GfleetControllerActionCreate");
        }
        //
        // POST: /GfleetControllerAction/Create

        //[HttpPost]
        //public ActionResult GfleetControllerActionCreate(ControllerAction controllerAction)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ControllerActions.AddObject(controllerAction);
        //        db.SaveChanges();
        //        return RedirectToAction("GfleetControllerActionIndex");
        //    }
        //    PopulateActionData();
        //    PopulateControllerData();
        //    return View("GfleetControllerActionCreate", controllerAction);
        //}

        [HttpPost]
       
        public ActionResult GfleetControllerActionCreate(string[] selectedControllers, string[] selectedAction)
        {
            if (ModelState.IsValid)
            {
                //db.ControllerActions.AddObject(controllerAction);
                //db.SaveChanges();
                return RedirectToAction("GfleetControllerActionIndex");
            }
            PopulateActionData();
            PopulateControllerData();
            return View("GfleetControllerActionCreate");
        }

        //
        // GET: /GfleetController/Edit/5
       
        public ActionResult GfleetControllerActionEdit(int id = 0)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            if (gfleetcontroller == null)
            {
                return HttpNotFound();
            }

            ViewBag.ParentId = new SelectList(db.GFleetControllers.Where(v => !v.ParentId.HasValue), "ControllerId", "ControllerName", gfleetcontroller.ParentId);
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleId", "ModuleName", gfleetcontroller.ModuleId);
            return View("GfleetControllerEdit", gfleetcontroller);
        }

        //
        // POST: /GfleetController/Edit/5

        [HttpPost]
       
        public ActionResult GfleetControllerActionEdit(GFleetController gfleetcontroller)
        {
            if (ModelState.IsValid)
            {
                db.GFleetControllers.Attach(gfleetcontroller);
                db.ObjectStateManager.ChangeObjectState(gfleetcontroller, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("GfleetControllerIndex");
            }
            ViewBag.ParentId = new SelectList(db.GFleetControllers.Where(v => !v.ParentId.HasValue), "ControllerId", "ControllerName", gfleetcontroller.ParentId);
            ViewBag.ModuleId = new SelectList(db.Modules, "ModuleId", "ModuleName", gfleetcontroller.ModuleId);
            return View("GfleetControllerEdit", gfleetcontroller);
        }

        //
        // GET: /GfleetController/Delete/5
       
        public ActionResult GfleetControllerActionDelete(int id = 0)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            if (gfleetcontroller == null)
            {
                return HttpNotFound();
            }
            return View("GfleetControllerDelete", gfleetcontroller);
        }

        //
        // POST: /GfleetController/Delete/5
       
        [HttpPost, ActionName("GfleetControllerDelete")]
        public ActionResult GfleetControllerActionDeleteConfirmed(int id)
        {
            GFleetController gfleetcontroller = db.GFleetControllers.Single(g => g.ControllerId == id);
            db.GFleetControllers.DeleteObject(gfleetcontroller);
            db.SaveChanges();
            return RedirectToAction("GfleetControllerIndex");
        }


        #endregion

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private void PopulateControllerData()
        {
            var allControllers = new GFleetModelContainer().GFleetControllers.OrderBy(M => M.Module.ModuleName);
            //var gfleetControllers = controller != null ? new HashSet<int>(controller.ControllerName.Select(c => c.)) : new HashSet<int>();
            var viewModel = new List<GFleetV3.Models.GFleetAllControllers>();
            foreach (var controllers in allControllers)
            {
                viewModel.Add(new GFleetV3.Models.GFleetAllControllers
                {
                    ControllerId = controllers.ControllerId,
                    ControllerName = controllers.ControllerName,
                    ModuleName = controllers.Module.ModuleName,
                    ModuleControllerName = "{" + controllers.Module.ModuleName + "}" + controllers.ControllerName
                });
            }
            ViewBag.GFleetControllers = viewModel;
        }
        private void PopulateActionData()
        {
            var allActions = new GFleetModelContainer().Actions;
            //var gfleetControllers = controller != null ? new HashSet<int>(controller.ControllerName.Select(c => c.)) : new HashSet<int>();
            var viewModel = new List<GFleetAction>();
            foreach (var actions in allActions)
            {
                viewModel.Add(new GFleetAction
                {
                    ActionId = actions.ActionId,
                    ActionName = actions.ActionName
                });
            }
            ViewBag.GFleetAction = viewModel;
        }
        private void PopulateControllerActionData()
        {
            var allControllerActions = new GFleetModelContainer().ControllerActions.OrderBy(m => m.Controller.ControllerName);
            //var gfleetControllers = controller != null ? new HashSet<int>(controller.ControllerName.Select(c => c.)) : new HashSet<int>();
            var viewModel = new List<GFleetV3.Models.GFleetControllerActions>();
            foreach (var controlleractions in allControllerActions)
            {
                viewModel.Add(new GFleetV3.Models.GFleetControllerActions
                {
                    ControllerActionId = controlleractions.ControllerActionId,
                    ControllerName = controlleractions.Controller.ControllerName,
                    ActionName = controlleractions.Action.ActionName,
                    ControllerActionName = controlleractions.Controller.ControllerName + "(" + controlleractions.Action.ActionName + ")"
                });
            }
            ViewBag.GFleetControllerActions = viewModel;
        }

        private void InsertControllerAction(string[] selectedControllerActions, ControllerAction controlleractonRoleUpdate)
        {
            if (selectedControllerActions == null)
            {
                controlleractonRoleUpdate.ControllerActionRoles = null;
                return;
            }

            var controllerActonsHS = new HashSet<string>(selectedControllerActions);
            var controllerActons = new HashSet<int>
                (controlleractonRoleUpdate.ControllerActionRoles.Select(c => c.ControllerActionId));
            foreach (var controlleractons in new GFleetModelContainer().ControllerActions)
            {
                if (controllerActonsHS.Contains(controlleractons.ControllerActionId.ToString()))
                {
                    if (!controllerActons.Contains(controlleractons.ControllerActionId))
                    {
                        controlleractonRoleUpdate.ControllerActionRoles.Add(new ControllerActionRole()
                        {
                            ControllerActionId = controlleractonRoleUpdate.ControllerActionId
                        });
                    }
                }
                else
                {
                    if (!controllerActons.Contains(controlleractons.ControllerActionId))
                    {
                        controlleractonRoleUpdate.ControllerActionRoles.Add(new ControllerActionRole()
                        {
                            ControllerActionId = controlleractonRoleUpdate.ControllerActionId
                        });
                    }
                }
            }
        }

    }
}
