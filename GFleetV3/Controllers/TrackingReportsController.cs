﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class TrackingReportsController : Controller
    {
        //
        // GET: /TrackingReports/

        //[Authorize(Roles = "super admin,Administrator,company admin,Company staff,")]
        [Authorize]
        //[Authorization]
        public ActionResult Index(string vehicledetailid, string type, string startDate, string endDate, string regNo, string Speed)
        {
            if (type == "overdue")
            {
                return View("Overdue", Overdue.getOverdue());
            }
            DateTime sdate;
            DateTime edate;

            if (!validateReportInput(startDate, endDate, regNo, out  sdate, out  edate, out regNo))
            {
                return View();
            }

            switch (type)
            {
                case "detail":
                    {
                        return View("Detailed", DetailedHistory.getDetailedHistory(sdate, edate, regNo));
                    }
                case "distance":
                    {
                        return View("Distance", DistanceReport.getDistanceReport(vehicledetailid, sdate, edate));
                    }
                case "travel":
                    {
                        return View("Journey", JourneyReport.getJourneyReport(sdate, edate, regNo));
                    }
                case "travel_Move":
                    {
                        return View("Journey", JourneyReport.getJourneyReportDist(sdate, edate, regNo));
                    }
                case "stop":
                    {
                        return View("Stop", StopReport.getStopReport(sdate, edate, regNo));
                    }
                case "overspeed":
                    {
                        return View("Overspeed", Overspeed.getOverspeed(regNo, sdate, edate, int.Parse(Speed)));
                    }
                default:
                    {
                        ViewBag.message = "unknown report type!!";
                        return View();
                    }

            }

        }
        //[Authorize(Roles = "super admin,Administrator,company admin")]
        [Authorize]
        //[Authorization]
        public ActionResult export(string reportType, string type, string startDate, string endDate, string regNo, string vehicleDetailId = "")
        {
            DateTime sdate;
            DateTime edate;
            vehicleDetailId = HttpUtility.UrlDecode(vehicleDetailId);

            if (!validateReportInput(startDate, endDate, regNo, out  sdate, out  edate, out regNo))
            {
                return View();
            }

            string spacer = " " + Environment.NewLine + " ";

            List<object> data = new List<object>();

            switch (type)
            {
                case "detail":
                    {
                        List<DetailedHistoryPayload> values = DetailedHistory.getDetailedHistory(sdate, edate, regNo).data;

                        foreach (DetailedHistoryPayload _value in values)
                        {
                            data.AddRange(_value.Reports.Select(m => new
                            {
                                Reg = m.Reg,
                                Time = m.time,
                                Driver = m.Driver,
                                Direction = m.Direction,
                                Latitude = m.Latitude,
                                Longitude = m.Longitude,
                                Speed = m.Speed,
                                Odometer = m.distance,
                                Event = m.EventName,
                                Street = m.street,
                                Town = m.town
                            }));
                        }


                        break;
                    }
                case "travel":
                    {
                        List<JourneyPayload> values = JourneyReport.getJourneyReport(sdate, edate, regNo).data;

                        foreach (JourneyPayload _value in values)
                        {
                            data.AddRange(_value.JourneyReports.Select(m => new
                            {
                                Time = m.sTime + spacer + m.eTime,
                                Event = m.sEvent + spacer + m.eEvent,
                                Street = m.sStreet + spacer + m.eStreet,
                                Longitude = m.sTown + spacer + m.eTown,
                                Distance = m.Distance,
                                Odometer = m.sOdometer + spacer + m.eOdometer,
                                TimeElapsed = m.TimeElapsed
                            }));
                        }


                        break;
                    }
                case "stop":
                    {
                        List<StopPayload> values = StopReport.getStopReport(sdate, edate, regNo).data;

                        foreach (StopPayload _value in values)
                        {
                            data.AddRange(_value.StopReports.Select(m => new
                            {
                                Event = m.Event,
                                DateFrom = m.DateFrom,
                                DateTo = m.DateTo,
                                Latitude = m.Latitude,
                                Longitude = m.Longitude,
                                Duration = m.Duration,
                                Town = m.Town,
                                Street = m.Street
                            }));
                        }


                        break;
                    }
                case "distance":
                    {
                        List<DistanceReportPayload> values = DistanceReport.getDistanceReport(vehicleDetailId, sdate, edate).data;

                        foreach (DistanceReportPayload _value in values)
                        {
                            data.AddRange(_value.DistanceReports.Select(m => new
                            {
                                Registration = m.Reg,
                                TimeElapsed = m.TimeElapsed,
                                Distance = m.Distance,
                                IgnitionOff = m.IgnitionOff,
                                IgnitionOn = m.IgnitionOn,
                                DoorOpened = m.DoorOpened,
                                DoorClosed = m.DoorClosed
                            }));
                        }


                        break;
                    }
                default:
                    {
                        ViewBag.message = "unknown report type!!";
                        return View();
                    }

            }
            return new ExportResult<object>
                (
                    ControllerContext,
                    type + " report (" + sdate.ToString() + ") - (" + edate.ToString() + ")",
                    data,
                    reportType
                );
        }

        [Authorize]
        public ActionResult ReportDetailed(int id)
        {
            return View("Detailed", DetailedHistory.getDetailedHistory(id));
        }
        [Authorize]
        public ActionResult ReportJourney(int id)
        {
            return View("Journey", JourneyReport.getJourneyReport(id));
        }
        [Authorize]
        public ActionResult ReportStop(int id)
        {
            return View("Stop", StopReport.getStopReport(id));
        }

        private Boolean validateReportInput(string startDate, string endDate, string _regNo, out DateTime sdate, out DateTime edate, out string regNo)
        {
            sdate = DateTime.Now;
            edate = DateTime.Now;

            if (_regNo == null)
            {
                regNo = null;
            }
            else
            {
                regNo = _regNo.Replace("+", " ");
            }


            endDate = HttpUtility.UrlDecode(endDate);     //endDate = endDate.Replace("+", ""); 
            startDate = HttpUtility.UrlDecode(startDate);   //startDate = startDate.Replace("+", "");

            //validate start date
            if (string.IsNullOrEmpty(startDate))
            {
                ViewBag.message = "A start date must be selected!!";
                return false;
            }
            if (!DateTime.TryParse(startDate, out sdate))
            {
                string fdate = startDate;
                string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                    + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : "");
                if (!DateTime.TryParse(otherDate, out sdate))
                {
                    ViewBag.message = "Invalid start date specified!!";
                    return false;
                }
            }
            startDate = sdate.ToString("dd/MM/yyyy HH:mm:ss");

            //validate end date
            if (string.IsNullOrEmpty(endDate))
            {
                ViewBag.message = "An end date must be selected!!";
                return false;
            }
            if (!DateTime.TryParse(endDate, out edate))
            {
                string fdate = endDate;
                string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                    + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : " 23:59:59.999");
                if (!DateTime.TryParse(otherDate, out edate))
                {
                    ViewBag.message = "Invalid start date specified!!";
                    return false;
                }
            }
            //edate = edate.Add(TimeSpan.FromDays(1)).AddMilliseconds(-1);

            return true;
        }
        [Authorize]
        [Authorization]
        public ActionResult ManageReportDash()
        {
            PopulateVehicleDropDownList();
            return View("ManageReportDash");
        }

        private void PopulateVehicleDropDownList(object vehicle = null)
        {
            int clientId = Service.Utility.getClientId().Value;
            var vehicleQuery = from d in new GFleetModelContainer().vw_VehicleDetailsV3
                               where d.ClientId == clientId
                               orderby d.VehicleRegCode ascending
                               select new { d.RegistrationNo, d.VehicleId };
            ViewBag.vehicle = new SelectList(vehicleQuery, "VehicleId", "RegistrationNo", vehicle);
        }
    }
}
