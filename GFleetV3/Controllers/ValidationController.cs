﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ValidationController : Controller
    {
        public JsonResult IsReg_Available(string RegistrationNo, string isEditing)
        {
            isEditing = string.IsNullOrEmpty(isEditing) ? "false" : isEditing;
            Boolean isEdit; Boolean.TryParse(isEditing, out isEdit);

            if (isEdit)
                return Json(true, JsonRequestBehavior.AllowGet);

            if (!Vehicle.VehicleExists(RegistrationNo))
                return Json(true, JsonRequestBehavior.AllowGet);

            string suggestedUID = String.Format(CultureInfo.InvariantCulture,
                "{0} already in the system.", RegistrationNo);
            return Json(suggestedUID, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsChasis_Available(string ChasisNo, string isEditing)
        {
            isEditing = string.IsNullOrEmpty(isEditing) ? "false" : isEditing;
            Boolean isEdit; Boolean.TryParse(isEditing, out isEdit);

            if (isEdit)
                return Json(true, JsonRequestBehavior.AllowGet);

            if (!Vehicle.ChasisNoExists(ChasisNo))
                return Json(true, JsonRequestBehavior.AllowGet);

            string suggestedUID = String.Format(CultureInfo.InvariantCulture,
                "{0} chasis No. already in the system.", ChasisNo);
            return Json(suggestedUID, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsEngine_Available(string EngineNo, string isEditing)
        {
            isEditing = string.IsNullOrEmpty(isEditing) ? "false" : isEditing;
            Boolean isEdit; Boolean.TryParse(isEditing, out isEdit);

            if (isEdit)
                return Json(true, JsonRequestBehavior.AllowGet);

            if (!Vehicle.EngineNoExists(EngineNo))
                return Json(true, JsonRequestBehavior.AllowGet);

            string suggestedUID = String.Format(CultureInfo.InvariantCulture,
                "{0} engine No. already in the system.", EngineNo);
            return Json(suggestedUID, JsonRequestBehavior.AllowGet);
        }
    }
}
