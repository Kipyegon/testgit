﻿using GFleetV3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GFleetV3.Controllers
{
    public class VehicleConsumptionAnalysisController : Controller
    {
        // GET: VehicleConsumptionAnalysis
        [Authorize]
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }
    }
}