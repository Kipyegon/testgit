﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    public class VehicleController : Controller
    {

        //
        // GET: /Vehicle/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_VehicleGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedVehicleModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelVehicle vehicleModel = new ModelVehicle();
            const int pageSize = 100;
            var totalRows = vehicleModel.CountVehicle();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "vehicleId";

            var vehicle = vehicleModel.GetVehiclePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Vehicle = vehicle
            };
            return data;
        }

        // GET: /Vehicle/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateFuelTypeDropDownList();
            PopulateVehicleTypeDropDownList();
            PopulateVehicleStatusDropDownList();
            PopulateUnallocatedDevices();
            PopulateVehicleGroups();
            PopulateClient();
            //Thread.Sleep(2000);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_Vehicle");
            }
            else
            {
                return View("_Vehicle");
            }
        }

        //
        // POST: /Vehicle/Create

        [HttpPost]

        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditVehicle(Vehicle mVehicle, string[] selectedDevices, string[] selectedGroups, string[] selectedClients, string Command)
        {
            ModelVehicle vehicleModel = new ModelVehicle();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mVehicle.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicle.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //mVehicle.ClientId = Service.Utility.getClientId().Value;
                UpdateVehicleDevices(selectedDevices, mVehicle);
                UpdateVehicleGroups(selectedGroups, mVehicle);
                UpdateClientVehicles(selectedClients, mVehicle);
                ResponseModel rm = vehicleModel.CreateVehicle(mVehicle);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mVehicle.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicle.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //mVehicle.ClientId = Service.Utility.getClientId().Value;
                UpdateVehicleDevices(selectedDevices, mVehicle);
                UpdateVehicleGroups(selectedGroups, mVehicle);
                UpdateClientVehicles(selectedClients, mVehicle);
                return this.Json(vehicleModel.UpdateVehicle(mVehicle));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }

        //
        // GET: /Vehicle/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelVehicle vehicleModel = new ModelVehicle();
            var data = vehicleModel.GetVehicle(id);
            PopulateFuelTypeDropDownList(data);
            PopulateVehicleTypeDropDownList(data);
            PopulateVehicleStatusDropDownList(data);
            PopulateDevices(data);
            PopulateVehicleGroups(data);
            PopulateClient(data);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_Vehicle", data);
            }
            else

                return View(data);
        }
        //
        // GET: /Vehicle/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        private void PopulateVehicleTypeDropDownList(object VehicleType = null)
        {
            var VehicleTypeQuery = from d in new GFleetModelContainer().VehicleTypes
                                   orderby d.VehicleTypeName
                                   select d;
            ViewBag.VehicleType = new SelectList(VehicleTypeQuery, "VehicleTypeId", "VehicleTypeName", VehicleType);
        }

        private void PopulateFuelTypeDropDownList(object FuelType = null)
        {
            var FuelTypeQuery = from d in new GFleetModelContainer().FuelTypes
                                orderby d.FuelTypeName
                                select d;
            ViewBag.FuelType = new SelectList(FuelTypeQuery, "FuelTypeId", "FuelTypeName", FuelType);
        }

        private void PopulateVehicleStatusDropDownList(object VehicleStatus = null)
        {
            var VehicleStatusQuery = from d in new GFleetModelContainer().VehicleStatus
                                     orderby d.VehicleStatusName
                                     select d;
            ViewBag.VehicleStatus = new SelectList(VehicleStatusQuery, "VehicleStatusId", "VehicleStatusName", VehicleStatus);
        }

        private void PopulateVehicleGroups(Vehicle group = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var allGroups = new GFleetModelContainer().Groups.Where(v => v.ClientId == ClientId);

            var availgroups = group != null ? new HashSet<int>(group.VehicleGroups.Select(c => c.GroupId)) : new HashSet<int>();
            var viewModel = new List<Groups>();
            foreach (var groups in allGroups)
            {
                viewModel.Add(new Groups
                {
                    GroupId = groups.GroupId,
                    GroupName = groups.GroupName,
                    Assigned = availgroups.Contains(groups.GroupId)
                });
            }
            ViewBag.Groups = viewModel;
        }

        private void UpdateVehicleGroups(string[] selectedGroups, Vehicle vehilceGroupsUpdate)
        {
            if (selectedGroups == null)
            {
                vehilceGroupsUpdate.VehicleGroups = null;
                return;
            }
            var selectedGroupsHS = new HashSet<string>(selectedGroups);
            var vehicleGroup = new HashSet<int>
                (vehilceGroupsUpdate.VehicleGroups.Select(c => c.GroupId));
            foreach (var group in new GFleetModelContainer().Groups)
            {
                if (selectedGroupsHS.Contains(group.GroupId.ToString()))
                {
                    if (!vehicleGroup.Contains(group.GroupId))
                    {
                        vehilceGroupsUpdate.VehicleGroups.Add(new VehicleGroup()
                        {
                            VehicleId = vehilceGroupsUpdate.VehicleId,
                            GroupId = group.GroupId,
                            Status = true
                        });
                    }
                }
                else
                {
                    if (vehicleGroup.Contains(group.GroupId))
                    {
                        vehilceGroupsUpdate.VehicleGroups.Remove(new VehicleGroup()
                        {
                            VehicleId = vehilceGroupsUpdate.VehicleId,
                            GroupId = group.GroupId,
                            Status = true
                        });
                    }
                }
            }
        }

        private void PopulateUnallocatedDevices(Vehicle device = null)
        {
            var allDevices = new GFleetModelContainer().vw_UnallocatedDevices;
            var availdevices = device != null ? new HashSet<int>(device.VehicleDevices.Select(c => c.DeviceId)) : new HashSet<int>();
            var viewModel = new List<Devices>();
            foreach (var devices in allDevices)
            {
                viewModel.Add(new Devices
                {
                    DeviceId = devices.DeviceId,
                    PhoneNo = devices.PhoneNo,
                    Assigned = availdevices.Contains(devices.DeviceId)
                });
            }
            ViewBag.Devices = viewModel;
        }

        private void PopulateDevices(Vehicle device)
        {
            ModelVehicle vehicleModel = new ModelVehicle();
            var data = vehicleModel.GetVehicle(device.VehicleId);

            string DeviceIds = "";

            for (int i = 0; i < data.VehicleDevices.Count; i++)
            {
                DeviceIds += "," + data.VehicleDevices.ToList()[i].DeviceId;
            }

            var allDevices = new GFleetModelContainer().unallocatedlocateddevices_SelectAll(DeviceIds);
            var availdevices = device != null ? new HashSet<int>(device.VehicleDevices.Select(c => c.DeviceId)) : new HashSet<int>();
            var viewModel = new List<Devices>();
            foreach (var devices in allDevices)
            {
                viewModel.Add(new Devices
                {
                    DeviceId = devices.DeviceId,
                    PhoneNo = devices.PhoneNo,
                    Assigned = availdevices.Contains(devices.DeviceId)
                });
            }
            ViewBag.Devices = viewModel;

        }

        private void UpdateVehicleDevices(string[] selectedDevices, Vehicle vehilceDeviceUpdate)
        {
            if (selectedDevices == null)
            {
                vehilceDeviceUpdate.VehicleDevices = null;
                return;
            }

            var selectedDevicesHS = new HashSet<string>(selectedDevices);
            var vehicleDevice = new HashSet<int>
                (vehilceDeviceUpdate.VehicleDevices.Select(c => c.DeviceId));
            foreach (var device in new GFleetModelContainer().Devices)
            {
                if (selectedDevicesHS.Contains(device.DeviceId.ToString()))
                {
                    if (!vehicleDevice.Contains(device.DeviceId))
                    {
                        vehilceDeviceUpdate.VehicleDevices.Add(new VehicleDevice()
                        {
                            VehicleId = vehilceDeviceUpdate.VehicleId,
                            DeviceId = device.DeviceId,
                            Status = true
                        });
                    }
                }
                else
                {
                    if (vehicleDevice.Contains(device.DeviceId))
                    {
                        vehilceDeviceUpdate.VehicleDevices.Remove(new VehicleDevice()
                        {
                            DeviceId = device.DeviceId,
                            VehicleId = vehilceDeviceUpdate.VehicleId,
                            Status = true
                        });
                    }
                }
            }
        }

        private void PopulateClient(Vehicle client = null)
        {
            if (Roles.IsUserInRole("Super Admin"))
            {
                //int ClientId = Service.Utility.getClientId().Value;
                var allClients = new GFleetModelContainer().Clients;

                var availclients = client != null ? new HashSet<int>(client.ClientVehicles.Select(c => c.ClientId)) : new HashSet<int>();
                var viewModel = new List<Clients>();
                foreach (var clients in allClients)
                {
                    viewModel.Add(new Clients
                    {
                        ClientId = clients.ClientId,
                        ClientName = clients.ClientName,
                        Assigned = availclients.Contains(clients.ClientId)
                    });
                }
                ViewBag.Clients = viewModel;
            }
            else if (Roles.IsUserInRole("Sharp Admin"))
            {
                string[] SharpClientIds = System.Configuration.ConfigurationManager.AppSettings["SharpClientIds"].ToString().Split(',');
                List<int> list = new List<int>();

                foreach (string id in SharpClientIds)
                {
                    list.Add(int.Parse(id));
                }

                var allClients = new GFleetModelContainer().Clients.Where(v => list.Contains(v.ClientId));

                var availclients = client != null ? new HashSet<int>(client.ClientVehicles.Select(c => c.ClientId)) : new HashSet<int>();
                var viewModel = new List<Clients>();
                foreach (var clients in allClients)
                {
                    viewModel.Add(new Clients
                    {
                        ClientId = clients.ClientId,
                        ClientName = clients.ClientName,
                        Assigned = availclients.Contains(clients.ClientId)
                    });
                }
                ViewBag.Clients = viewModel;
            }
            else
            {
                int ClientId = Service.Utility.getClientId().Value;
                var allClients = new GFleetModelContainer().Clients.Where(v => v.ClientId == ClientId);

                var availclients = client != null ? new HashSet<int>(client.ClientVehicles.Select(c => c.ClientId)) : new HashSet<int>();
                var viewModel = new List<Clients>();
                foreach (var clients in allClients)
                {
                    viewModel.Add(new Clients
                    {
                        ClientId = clients.ClientId,
                        ClientName = clients.ClientName,
                        Assigned = availclients.Contains(clients.ClientId)
                    });
                }
                ViewBag.Clients = viewModel;
            }
        }

        private void UpdateClientVehicles(string[] selectedClients, Vehicle ClientvehilceUpdate)
        {
            if (selectedClients == null)
            {
                ClientvehilceUpdate.ClientVehicles = null;
                return;
            }

            var selectedClientsHS = new HashSet<string>(selectedClients);
            var Clientvehicle = new HashSet<int>
                (ClientvehilceUpdate.ClientVehicles.Select(c => c.ClientId));
            foreach (var client in new GFleetModelContainer().Clients)
            {
                if (selectedClientsHS.Contains(client.ClientId.ToString()))
                {
                    if (!Clientvehicle.Contains(client.ClientId))
                    {
                        ClientvehilceUpdate.ClientVehicles.Add(new ClientVehicle()
                        {
                            VehicleId = ClientvehilceUpdate.VehicleId,
                            ClientId = client.ClientId
                        });
                    }
                }
                else
                {
                    if (Clientvehicle.Contains(client.ClientId))
                    {
                        ClientvehilceUpdate.ClientVehicles.Remove(new ClientVehicle()
                        {
                            VehicleId = ClientvehilceUpdate.VehicleId,
                            ClientId = client.ClientId
                        });
                    }
                }
            }
        }
    }
}
