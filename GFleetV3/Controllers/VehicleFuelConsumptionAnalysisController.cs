﻿using DataAccess.SQL;
using GFleetV3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GFleetV3.Controllers
{
    public class VehicleFuelConsumptionAnalysisController : Controller
    {
        //
        // GET: /DashBoard/*\\

        //public static Disconnected getDisconnectedData()
        //{
        //    using (GFleetModelContainer gfleet = new GFleetModelContainer())
        //    {
        //        int clientId = Service.Utility.getClientId().Value;

        //        string diconDate = DateTime.Now.Date.ToString("dd/MM/yy");

        //        List<getWebTrackData_V3_Result> data = gfleet.getWebTrackData_V3(clientId).ToList<getWebTrackData_V3_Result>();

        //        List<object> DisconnectedData = new List<object>();

        //        DisconnectedData.AddRange(data.Where(v => v.connected == 0
        //            && v.Date != null && v.Date.CompareTo(diconDate) < 0).OrderBy(v => v.timestamp)
        //        .Select(v => new
        //        {
        //            Reg = v.Reg + " (" + v.VehicleCode + ")",
        //            Date = string.IsNullOrEmpty(v.timestamp) ? "No Device" : v.timestamp,
        //            Place = v.Street + " - " + v.Town
        //        }));

        //        return new Disconnected()
        //        {
        //            TotalCount = data.Count,
        //            DisconnectedCount = DisconnectedData.Count(),
        //            DisconnectedData = DisconnectedData
        //        };
        //    }
        //}
        [Authorize]
        [Authorization]
        public ActionResult Index()
        {
            PopulateVehicles();
            return View();
        }
        //public ActionResult VehicleConsumptionAnalysis(int page = 1, string sort = "Reg", string sortDir = "ASC", string vehicle = null,
        //   string startdate = null, string enddate = null)
        //{

        //    PopulateVehicleDropDownList();

        //    if (string.IsNullOrEmpty(startdate) || string.IsNullOrEmpty(enddate))
        //    {
        //        List<vw_VehicleFuelMngtRpt> data = new List<vw_VehicleFuelMngtRpt>();
        //        return View("VehicleFuelDetails", data);
        //    }

        //    const int pageSize = 25;
        //    sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
        //    sort = "VehicleId";
        //    string orderCriteria = "it." + sort + " " + sortDir;

        //    var datas = ReportModel.GetVehicleFuelRpt(page, pageSize, orderCriteria, vehicle, startdate, enddate);
        //    return View("VehicleFuelDetails", datas);
        //}

        //private void PopulateVehicleDropDownList()
        //{
        //    throw new NotImplementedException();
        //}


        public JsonResult VehicleFuelAnalysisData(string[] VehicleIds, string StartDate, string EndDate)
        {
           string selectedVehicles = string.Join(",", VehicleIds);

           List<object> data = VehicleFuelConsumptionAnalysisModel.getFuelConsumptionAnalysis(selectedVehicles, StartDate, EndDate);

            // IEnumerable<IGrouping<string, VehicleFuelConsumptionAnalysis_Result>> data = new GFleetModelContainer().VehicleFuelConsumptionAnalysis(ClientId, Now, p7DaysAgo)
            //.OrderBy(c => c.FuelConsumption).GroupBy(v => v.VehicleRegCode);



            return Json(data);
        }
        [Authorize]
        [Authorization]

        public ActionResult VehicleFuelConsumptionAnalysis()
        {
            //        VehicleFuelConsumptionAnalysisModel model= new VehicleFuelConsumptionAnalysisModel();

            //if (model. == null)
            //{
            //    return RedirectToAction("VehicleFuelManagement");
            //}else


            return View("VehicleFuelConsumptionAnalysis");
        }

        private void PopulateVehicles(vw_VehicleDetailsV3 vehicle = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var allVehicle = new GFleetModelContainer().vw_VehicleDetailsV3.Where(v => v.ClientId == ClientId);

            var viewModel = new List<Vehicles>();
            foreach (var vehicles in allVehicle)
            {
                viewModel.Add(new Vehicles
                {
                    VehicleId = vehicles.VehicleId,
                    VehicleRegCode = vehicles.VehicleRegCode
                });
            }
            ViewBag.Vehicles = viewModel;
        }
    }
}
