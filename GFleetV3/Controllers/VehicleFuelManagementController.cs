﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class VehicleFuelManagementController : Controller
    {

        //
        // GET: /VehicleFuelManagement/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_VehicleFuelManagementGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedVehicleFuelManagementModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelVehicleFuelManagement vehicleFuelManagementModel = new ModelVehicleFuelManagement();
            const int pageSize = 100;
            var totalRows = vehicleFuelManagementModel.CountVehicleFuelManagement();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "VehicleFuelManagementId";

            var vehicleFuelManagement = vehicleFuelManagementModel.GetVehicleFuelManagementPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleFuelManagementModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                VehicleFuelManagement = vehicleFuelManagement
            };
            return data;
        }

        //
        // GET: /VehicleFuelManagement/Create


        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            //Thread.Sleep(2000);
            if (Request.IsAjaxRequest())
            {
                PopulateDriverDropDownList();
                PopulateVehicleDropDownList();
                ViewBag.IsUpdate = false;
                return View("_VehicleFuelManagement");
            }
            else
            {
                PopulateDriverDropDownList();
                PopulateVehicleDropDownList();
                return View("_VehicleFuelManagement");
            }
        }

        //
        // POST: /VehicleFuelManagement/Create

        [HttpPost]

        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditVehicleFuelManagement(VehicleFuelManagement mVehicleFuelManagement, string Command)
        {
            ModelVehicleFuelManagement vehicleFuelManagementModel = new ModelVehicleFuelManagement();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mVehicleFuelManagement.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleFuelManagement.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleFuelManagement.ClientId = Service.Utility.getClientId().Value;
                mVehicleFuelManagement.Date = DateTime.ParseExact(mVehicleFuelManagement.FueledDate + " " + mVehicleFuelManagement.FueledTime, "dd-MM-yyyy HH:mm", null);

                ResponseModel rm = vehicleFuelManagementModel.CreateVehicleFuelManagement(mVehicleFuelManagement);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mVehicleFuelManagement.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleFuelManagement.Date = DateTime.Parse(mVehicleFuelManagement.FueledDate + " " + mVehicleFuelManagement.FueledTime);
                mVehicleFuelManagement.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleFuelManagement.ClientId = Service.Utility.getClientId().Value;
                return this.Json(vehicleFuelManagementModel.UpdateVehicleFuelManagement(mVehicleFuelManagement));
            }

            return Content("Unrecognized operation, kindly refresh browser and rety.");
        }

        //
        // GET: /VehicleFuelManagement/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelVehicleFuelManagement vehicleFuelManagementModel = new ModelVehicleFuelManagement();
            PopulateDriverDropDownList();
            PopulateVehicleDropDownList();
            var data = vehicleFuelManagementModel.GetVehicleFuelManagement(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_VehicleFuelManagement", data);
            }
            else

                return View(data);
        }

        //
        // GET: /VehicleFuelManagement/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /VehicleFuelManagement/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var VehicleQuery = from d in new GFleetModelContainer().ClientVehicles.Where(v => v.ClientId == ClientId)
                               orderby d.Vehicle.RegistrationNo
                               select new { VehicleId = d.VehicleId, RegistrationNo = (d.Vehicle.RegistrationNo + " (" + d.Vehicle.NickName + ")") };
            ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "RegistrationNo", Vehicle);
        }
        private void PopulateDriverDropDownList(object Driver = null)
        {
            string ClientId = Service.Utility.getClientId().Value.ToString();
            var DriverQuery = from d in new GFleetModelContainer().vw_Driver.Where(v => v.ClientId.Equals(ClientId))
                              //.People.OfType<Driver>().Where(v => v.ClientId == ClientId)
                              orderby d.FirstName
                              //select d;
                              select new { DriverUserId = d.UserId, Name = (d.FirstName + " " + d.LastName) };
            ViewBag.Driver = new SelectList(DriverQuery, "DriverUserId", "Name", Driver);
        }

        //public JsonResult vehicleFuelAnalysisData()
        //{
        //    int ClientId = Service.Utility.getClientId().Value;
        //    DateTime start = DateTime.Today;
        //    DateTime end = DateTime.Today.AddDays(1).AddMilliseconds(-1);

        //   // List<VehicleFuelConsumptionAnalysis_Result> data = new GFleetModelContainer().VehicleFuelConsumptionAnalysis(ClientId,Now, DaysAgo).ToList();
        //    return Json(new
        //    {
        //        OrgData = data
        //            .Select(d => new
        //            {
        //                AvgFuelConsumption = d.AvgFuelConsumption,
        //                CurrentMileage = d.CurrentMileage,
        //                FuelConsumption = d.FuelConsumption

        //            })
        //    });
        //}
    }
}

      //  [Authorize]
        //public ActionResult FuelAnalysis()
    //    {
    //        return View("FuelAnalysis");
    //    }
    //}

