﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Controllers
{
    [ValidateOnlyIncomingValuesAttribute]
    public class VehicleInsuaranceController : Controller
    {
        //
        // GET: /VehicleInsuarance/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_VehicleInsuaranceGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedVehicleInsuaranceModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelVehicleInsuarance vehicleInsuaranceModel = new ModelVehicleInsuarance();
            const int pageSize = 100;
            var totalRows = vehicleInsuaranceModel.CountVehicleInsuarance();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";

            sort = "VehicleInsuaranceId";

            var vehicleInsuarance = vehicleInsuaranceModel.GetVehicleInsuarancePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleInsuaranceModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                VehicleInsuarance = vehicleInsuarance
            };
            return data;
        }

        //
        // GET: /VehicleInsuarance/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            PopulateVehicleDropDownList();
            PopulateInsuaranceDropDownList();
            PopulateInsuaranceTypeDropDownList();
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_VehicleInsuarance");
            }
            else
            {
                return View("_VehicleInsuarance");
            }
        }

        //
        // POST: /VehicleInsuarance/Create

        [HttpPost]

        [Authorize]
      //  [Authorization]
        public ActionResult CreateEditVehicleInsuarance(VehicleInsuarance mVehicleInsuarance, string Command)
        {
            ModelVehicleInsuarance vehicleInsuaranceModel = new ModelVehicleInsuarance();
            DateTime StartDate = mVehicleInsuarance.StartDate;
            DateTime EndDate = mVehicleInsuarance.EndDate;

            if (StartDate > EndDate)
            {
                ErrorModel errMod = new ErrorModel();

                errMod.Errors.Add("Expiry date should not be before the Renewal Date!");
                return View("_Error", errMod);
            }
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mVehicleInsuarance.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleInsuarance.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleInsuarance.ClientId = Service.Utility.getClientId().Value;
                ResponseModel rm = vehicleInsuaranceModel.CreateVehicleInsuarance(mVehicleInsuarance);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mVehicleInsuarance.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleInsuarance.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleInsuarance.ClientId = Service.Utility.getClientId().Value;
                return this.Json(vehicleInsuaranceModel.UpdateVehicleInsuarance(mVehicleInsuarance));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
        //
        // GET: /VehicleInsuarance/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelVehicleInsuarance vehicleInsuaranceModel = new ModelVehicleInsuarance();
            var data = vehicleInsuaranceModel.GetVehicleInsuarance(id);
            PopulateVehicleDropDownList(data);
            PopulateInsuaranceDropDownList(data);
            PopulateInsuaranceTypeDropDownList(data);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_VehicleInsuarance", data);
        }

        //
        // GET: /VehicleInsuarance/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var VehicleQuery = from d in new GFleetModelContainer().ClientVehicles.Where(v => v.ClientId == ClientId)
                               orderby d.Vehicle.RegistrationNo
                               select new { VehicleId = d.VehicleId, RegistrationNo = (d.Vehicle.RegistrationNo + " (" + d.Vehicle.NickName + ")") };
            ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "RegistrationNo", Vehicle);
        }

        private void PopulateInsuaranceTypeDropDownList(object InsuaranceType = null)
        {
            var InsuaranceTypeQuery = from d in new GFleetModelContainer().InsuaranceTypes
                                  orderby d.InsuaranceTypeName
                                  select d;
            ViewBag.InsuaranceType = new SelectList(InsuaranceTypeQuery, "InsuaranceTypeId", "InsuaranceTypeName", InsuaranceType);
        }

        private void PopulateInsuaranceDropDownList(object Insuarance = null)
        {
            var InsuaranceQuery = from d in new GFleetModelContainer().Insuarances
                               orderby d.InsuaranceName
                               select d;
            ViewBag.Insuarance = new SelectList(InsuaranceQuery, "InsuaranceId", "InsuaranceName", Insuarance);
        }

    }
}
