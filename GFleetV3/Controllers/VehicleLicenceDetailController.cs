﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;


namespace GFleetV3.Controllers
{
    [ValidateOnlyIncomingValuesAttribute]
    public class VehicleLicenceDetailController : Controller
    {
       
        //
        // GET: /VehicleLicenceDetail/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_VehicleLicenceDetailGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedVehicleLicenceDetailModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelVehicleLicenceDetail vehicleLicenceDetailModel = new ModelVehicleLicenceDetail();
            const int pageSize = 25;
            var totalRows = vehicleLicenceDetailModel.CountVehicleLicenceDetail();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "VehicleLicenceDetailId";

            var vehicleLicenceDetail = vehicleLicenceDetailModel.GetVehicleLicenceDetailPage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleLicenceDetailModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                VehicleLicenceDetail = vehicleLicenceDetail
            };
            return data;
        }

        //
        // GET: /VehicleLicenceDetail/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                PopulateVehicleDropDownList();
                PopulateVehicleLicenceTypeDropDownList();
                ViewBag.IsUpdate = false;
                return View("_VehicleLicenceDetail");
            }
            else
            {
                PopulateVehicleDropDownList();
                PopulateVehicleLicenceTypeDropDownList();
                return View("_VehicleLicenceDetail");
            }
        }

        //
        // POST: /VehicleLicenceDetail/Create

        [HttpPost]

        [Authorize]
        //[Authorization]
        public ActionResult CreateEditVehicleLicenceDetail(VehicleLicenceDetail mVehicleLicenceDetail, string Command)
        {
            ModelVehicleLicenceDetail vehicleLicenceDetailModel = new ModelVehicleLicenceDetail();
            DateTime RenewalDate = mVehicleLicenceDetail.RenewalDate;
            DateTime ExpireDate = mVehicleLicenceDetail.ExpireDate;

            if (RenewalDate > ExpireDate)
            {
                ErrorModel errMod = new ErrorModel();

                errMod.Errors.Add("Expiry date should not be before the Renewal Date!");
                return View("_Error", errMod);
            }
            
            if (!ModelState.IsValid) 
                //ExpireDate >RenewalDate )
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mVehicleLicenceDetail.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleLicenceDetail.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleLicenceDetail.ClientId = Service.Utility.getClientId().Value;

                ResponseModel rm = vehicleLicenceDetailModel.CreateVehicleLicenceDetail(mVehicleLicenceDetail);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mVehicleLicenceDetail.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleLicenceDetail.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mVehicleLicenceDetail.ClientId = Service.Utility.getClientId().Value;
                return this.Json(vehicleLicenceDetailModel.UpdateVehicleLicenceDetail(mVehicleLicenceDetail));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /VehicleLicenceDetail/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelVehicleLicenceDetail vehicleLicenceDetailModel = new ModelVehicleLicenceDetail();
            PopulateVehicleDropDownList();
            PopulateVehicleLicenceTypeDropDownList();
            var data = vehicleLicenceDetailModel.GetVehicleLicenceDetail(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_VehicleLicenceDetail", data);
            }
            else

                return View(data);
        }


        //
        // GET: /VehicleLicenceDetail/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
        private void PopulateVehicleDropDownList(object Vehicle = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var VehicleQuery = from d in new GFleetModelContainer().ClientVehicles.Where(v => v.ClientId == ClientId)
                               orderby d.Vehicle.RegistrationNo
                               select new { VehicleId = d.VehicleId, RegistrationNo = (d.Vehicle.RegistrationNo + " (" + d.Vehicle.NickName + ")") };
            ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "RegistrationNo", Vehicle);
        }
        private void PopulateVehicleLicenceTypeDropDownList(object VehicleLicenceType = null)
        {
            var LicenceTypeQuery = from d in new GFleetModelContainer().VehicleLicenceTypes
                                     orderby d.VehicleLicenceTypeName
                                     select d;
            ViewBag.LicenceType = new SelectList(LicenceTypeQuery, "VehicleLicenceTypeId", "VehicleLicenceTypeName", VehicleLicenceType);
        }
    }
}
