﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GFleetV3.Models;
using DataAccess.SQL;

namespace GFleetV3.Controllers
{
    public class VehicleLicenceTypeController : Controller
    {
        
        //
        // GET: /VehicleLicenceType/

        [Authorize]
        [Authorization]
        public ActionResult Index(int page = 1, string sort = "Reg", string sortDir = "ASC", string dataGrid = "false")
        {
            Boolean isDatagrid;
            if (Boolean.TryParse(dataGrid, out isDatagrid))
            {
                if (isDatagrid)
                {
                    return PartialView("_VehicleLicenceTypeGrid", gridData(page, ref sort, ref sortDir));
                }
            }
            var data = gridData(page, ref sort, ref sortDir);
            return View(data);
        }

        private PagedVehicleLicenceTypeModel gridData(int page, ref string sort, ref string sortDir)
        {
            ModelVehicleLicenceType vehicleLicenceTypeModel = new ModelVehicleLicenceType();
            const int pageSize = 5;
            var totalRows = vehicleLicenceTypeModel.CountVehicleLicenceType();

            sortDir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? sortDir : "asc";
            sort = "VehicleLicenceTypeId";

            var vehicleLicenceType = vehicleLicenceTypeModel.GetVehicleLicenceTypePage(page, pageSize, "it." + sort + " " + sortDir);

            var data = new PagedVehicleLicenceTypeModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                VehicleLicenceType = vehicleLicenceType
            };
            return data;
        }

        //
        // GET: /VehicleLicenceType/Create

        [Authorize]
        [Authorization]
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
                return View("_VehicleLicenceType");
            }
            else
            {
                return View("_VehicleLicenceType");
            }
        }


        //
        // POST: /VehicleLicenceType/Create


        [HttpPost]

        [Authorize]
        public ActionResult CreateEditVehicleLicenceType(VehicleLicenceType mVehicleLicenceType, string Command)
        {
            ModelVehicleLicenceType vehicleLicenceTypeModel = new ModelVehicleLicenceType();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mVehicleLicenceType.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mVehicleLicenceType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = vehicleLicenceTypeModel.CreateVehicleLicenceType(mVehicleLicenceType);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mVehicleLicenceType.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(vehicleLicenceTypeModel.UpdateVehicleLicenceType(mVehicleLicenceType));
            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //
        // GET: /VehicleLicenceType/Edit/5

        [Authorize]
        [Authorization]
        public ActionResult Edit(int id)
        {
            ModelVehicleLicenceType vehicleLicenceTypeModel = new ModelVehicleLicenceType();
            var data = vehicleLicenceTypeModel.GetVehicleLicenceType(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
                return View("_VehicleLicenceType", data);
            }
            else

                return View(data);
        }
        //
        // GET: /VehicleLicenceType/Delete/5

        [Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }

    }
}
