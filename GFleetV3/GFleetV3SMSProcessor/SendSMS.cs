﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.Configuration;

namespace GFleetV3SMSProcessor
{
    public class SendSMS
    {


        public static string SendMessage(string Recipients, string Message, string Sender)
        {
            string InfoBipUrl = System.Configuration.ConfigurationManager.AppSettings["InfobipUrl"].ToString();
            string username = System.Configuration.ConfigurationManager.AppSettings["InfobipUserName"].ToString();
            string password = System.Configuration.ConfigurationManager.AppSettings["InfobipPassword"].ToString();
            string gateway = System.Configuration.ConfigurationManager.AppSettings["gateway"].ToString();
            //Sender = "Geecko";
            string resp2 = "";
            if (Recipients.StartsWith("+971"))
            {
                Sender = "CapitalList";
            }
            if (gateway == "two")
            {

                resp2 = SendMessage2(Recipients, Message, Sender);
                return resp2;
            }

            try
            {
                Message = HttpUtility.UrlEncode(Message);
                InfoBipUrl = InfoBipUrl + "&password=" + password + "&sender=" + Sender.Trim() + "&SMSText=" + Message.Trim() + "&GSM=" + Recipients.Trim();
                if (Message.Length > 160)
                {
                    InfoBipUrl = InfoBipUrl + "&type=LongSMS";
                }
                WebRequest request = (WebRequest)WebRequest.Create(InfoBipUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string resultmsg = responseReader.ReadToEnd();
                responseReader.Close();
                response.Close();
                return resultmsg.Trim();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public static string SendMessage2(string MobileNo, string Message, string SenderId)
        {

            try
            {
                if (MobileNo.StartsWith("0"))
                {
                    int len = MobileNo.Length;
                    MobileNo = MobileNo.Substring(1, len - 1);
                    MobileNo = "254" + MobileNo.Trim();
                }
                string InfoBipUrl2 = "http://bulksms.w2wts.com/API_SendSMS.aspx?User=gghleuro&passwd=gghl2013&mobilenumber=" + MobileNo + "&message=" + Message.Trim() + "&sid=" + SenderId.Trim() + "&mtype=N&DR=Y";
                // InfoBipUrl = InfoBipUrl + "&password=" + password + "&sender=" + SenderId.Trim ( ) + "&SMSText=" + Message.Trim ( ) + "&GSM=" + MobileNo.Trim ( );
                if (Message.Length > 160)
                {
                    InfoBipUrl2 = InfoBipUrl2 + "&type=LongSMS";
                }
                WebRequest request = (WebRequest)WebRequest.Create(InfoBipUrl2);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader responseReader = new StreamReader(response.GetResponseStream());
                string resultmsg = responseReader.ReadToEnd();
                responseReader.Close();
                response.Close();
                return resultmsg.Trim();
            }
            catch (Exception)
            {
                return "Error";
            }
        }
    }
}
