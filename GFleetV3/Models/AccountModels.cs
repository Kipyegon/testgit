﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web.Profile;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Models
{
    #region Models

    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnModel
    {
        [Display(Name = "OpenID")]
        public string OpenID { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterStaffModel
    {

        [Display(Name = "OpenID")]
        public string OpenID { get; set; }

        //[Required]
        //[Display(Name = "UserName (Email Address)")]
        //public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserName (Email Address)")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Role name")]
        [ValidRoleNameAttribute(ErrorMessage = "Please Select Role name")]
        public UserRole UserRole { get; set; }
    }

    public class RegisterModel
    {

        [Display(Name = "OpenID")]
        public string OpenID { get; set; }

        //[Required]
        //[Display(Name = "UserName (Email Address)")]
        //public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserName (Email Address)")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter correct Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Username (Telephone):")]
        [RegularExpression(@"^(?=\+)(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?^.{10,}$", ErrorMessage = "Please enter correct Phone Number")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Role name")]
        [ValidRoleNameAttribute(ErrorMessage = "Please Select Role name")]
        public UserRole UserRole { get; set; }
    }

    public class RegisterClientModel
    {

        [Display(Name = "OpenID")]
        public string OpenID { get; set; }

        //[Required]
        //[Display(Name = "UserName (Email Address)")]
        //public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserName (Email Address)")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Role name")]
        [ValidRoleNameAttribute(ErrorMessage = "Please Select Role name")]
        public UserRole UserRole { get; set; }
    }

    public class RegisterOrganisationAdminModel
    {

        [Display(Name = "OpenID")]
        public string OpenID { get; set; }

        //[Required]
        //[Display(Name = "UserName (Email Address)")]
        //public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserName (Email Address)")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterEndUserPhoneModel
    {
        [Required(ErrorMessage = "Please enter correct Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Username (Telephone):")]
        [RegularExpression(@"^(?=\+)(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?^.{10,}$", ErrorMessage = "Please enter correct Phone Number")]
        public string Username { get; set; }
    }


    public class UserRole
    {
        public string Role { get; set; }
        public string RoleName { get; set; }
        public Boolean Selected { get; set; }
    }

    #endregion

    #region Services
    // The FormsAuthentication type is sealed and contains static members, so it is difficult to
    // unit test code that calls its members. The interface and helper class below demonstrate
    // how to create an abstract wrapper around such a type in order to make the AccountController
    // code unit testable.

    public interface IMembershipService
    {
        int MinPasswordLength { get; }
        bool ValidateUser(string userName, string password);
        MembershipCreateStatus CreateUser(string userName, string password, string email, string OpenID);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
        MembershipUser GetUser(string OpenID);
    }

    public class AccountMembershipService : IMembershipService
    {
        private readonly MembershipProvider _provider;

        public AccountMembershipService()
            : this(null)
        {
        }

        public AccountMembershipService(MembershipProvider provider)
        {
            _provider = provider ?? Membership.Provider;
        }

        public int MinPasswordLength
        {
            get
            {
                return _provider.MinRequiredPasswordLength;
            }
        }

        public bool ValidateUser(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

            return _provider.ValidateUser(userName, password);
        }
        public Guid StringToGUID(string value)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5.Create();
            MD5 md5Hasher = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            return new Guid(data);
        }
        public MembershipCreateStatus CreateUser(string userName, string password, string email, string OpenID)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");

            MembershipCreateStatus status;
            if (!string.IsNullOrEmpty(OpenID))
                _provider.CreateUser(userName, password, email, null, null, true, StringToGUID(OpenID), out status);
            else
                _provider.CreateUser(userName, password, email, null, null, true, null, out status);

            return status;
        }
        public MembershipUser GetUser(string OpenID)
        {
            return _provider.GetUser(StringToGUID(OpenID), true);
        }
        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(oldPassword)) throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
            if (String.IsNullOrEmpty(newPassword)) throw new ArgumentException("Value cannot be null or empty.", "newPassword");

            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                MembershipUser currentUser = _provider.GetUser(userName, true /* userIsOnline */);
                return currentUser.ChangePassword(oldPassword, newPassword);
            }
            catch (ArgumentException mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
            catch (MembershipPasswordException mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }


        public MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            throw new NotImplementedException();
        }
    }

    public interface IFormsAuthenticationService
    {
        void SignIn(string userName, bool createPersistentCookie);
        void SignOut();
    }

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        public void SignIn(string userName, bool createPersistentCookie)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");

            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
    #endregion

    #region Validation

    public static class AccountValidation
    {
        public static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class ValidatePasswordLengthAttribute : ValidationAttribute, IClientValidatable
    {
        private const string _defaultErrorMessage = "'{0}' must be at least {1} characters long.";
        private readonly int _minCharacters = Membership.Provider.MinRequiredPasswordLength;

        public ValidatePasswordLengthAttribute()
            : base(_defaultErrorMessage)
        {
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture, ErrorMessageString,
                name, _minCharacters);
        }

        public override bool IsValid(object value)
        {
            string valueAsString = value as string;
            return (valueAsString != null && valueAsString.Length >= _minCharacters);
        }

        public IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[]{
                new System.Web.Mvc.ModelClientValidationStringLengthRule(FormatErrorMessage(metadata.GetDisplayName()), _minCharacters, int.MaxValue)
            };
        }
    }
    public class ValidRoleNameAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
                return false;
            if (string.IsNullOrEmpty(((UserRole)value).RoleName))
                return false;
            else
                return true;
        }
    }

    #endregion

    public class AccountProfile : ProfileBase
    {

        public static AccountProfile CurrentUser
        {
            get
            {
                return (AccountProfile)
                       (ProfileBase.Create(Membership.GetUser().UserName));
            }
        }

        public string FullName
        {
            get { return ((string)(base["FullName"])); }
            set { base["FullName"] = value; Save(); }
        }
        public String FirstName
        {
            get
            {
                return ((string)(this.GetPropertyValue("FirstName")));
            }
            set
            {
                this.SetPropertyValue("FirstName", value);
            }
        }
        public String LastName
        {
            get
            {
                return ((string)(this.GetPropertyValue("LastName")));
            }
            set
            {
                this.SetPropertyValue("LastName", value);
            }
        }
        public String Email1
        {
            get
            {
                return ((string)(this.GetPropertyValue("Email1")));
            }
            set
            {
                this.SetPropertyValue("Email1", value);
            }
        }
        public String Telephone1
        {
            get
            {
                return ((string)(this.GetPropertyValue("Telephone1")));
            }
            set
            {
                this.SetPropertyValue("Telephone1", value);
            }
        }
        public String Email2
        {
            get
            {
                return ((string)(this.GetPropertyValue("Email2")));
            }
            set
            {
                this.SetPropertyValue("Email2", value);
            }
        }
        public String Telephone2
        {
            get
            {
                return ((string)(this.GetPropertyValue("Telephone2")));
            }
            set
            {
                this.SetPropertyValue("Telephone2", value);
            }
        }
        public bool? IsMale
        {
            get
            {
                return ((bool)(this.GetPropertyValue("IsMale")));
            }
            set
            {
                this.SetPropertyValue("IsMale", value);
            }
        }
        public int ClientId
        {
            get
            {
                return ((int)(this.GetPropertyValue("ClientId")));
            }
            set
            {
                this.SetPropertyValue("ClientId", value);
            }
        }
        public String PostCode
        {
            get
            {
                return ((string)(this.GetPropertyValue("PostCode")));
            }
            set
            {
                this.SetPropertyValue("PostCode", value);
            }
        }
        public String FaxNumber
        {
            get
            {
                return ((string)(this.GetPropertyValue("FaxNumber")));
            }
            set
            {
                this.SetPropertyValue("FaxNumber", value);
            }
        }
        public String DriverNo
        {
            get
            {
                return ((string)(this.GetPropertyValue("DriverNo")));
            }
            set
            {
                this.SetPropertyValue("DriverNo", value);
            }
        }
        public String DriverRFID
        {
            get
            {
                return ((string)(this.GetPropertyValue("DriverRFID")));
            }
            set
            {
                this.SetPropertyValue("DriverRFID", value);
            }
        }
        public String DOB
        {
            get
            {
                return ((string)(this.GetPropertyValue("DOB")));
            }
            set
            {
                this.SetPropertyValue("DOB", value);
            }
        }
        public String IDNumber
        {
            get
            {
                return ((string)(this.GetPropertyValue("IDNumber")));
            }
            set
            {
                this.SetPropertyValue("IDNumber", value);
            }
        }
        public String NextOfKinName1
        {
            get
            {
                return ((string)(this.GetPropertyValue("NextOfKinName1")));
            }
            set
            {
                this.SetPropertyValue("NextOfKinName1", value);
            }
        }
        public String NextOfKinPhone1
        {
            get
            {
                return ((string)(this.GetPropertyValue("NextOfKinPhone1")));
            }
            set
            {
                this.SetPropertyValue("NextOfKinPhone1", value);
            }
        }
        public String NextOfKinName2
        {
            get
            {
                return ((string)(this.GetPropertyValue("NextOfKinName2")));
            }
            set
            {
                this.SetPropertyValue("NextOfKinName2", value);
            }
        }
        public String NextOfKinPhone2
        {
            get
            {
                return ((string)(this.GetPropertyValue("NextOfKinPhone2")));
            }
            set
            {
                this.SetPropertyValue("NextOfKinPhone2", value);
            }
        }
        public String Association1
        {
            get
            {
                return ((string)(this.GetPropertyValue("Association1")));
            }
            set
            {
                this.SetPropertyValue("Association1", value);
            }
        }
        public String Association2
        {
            get
            {
                return ((string)(this.GetPropertyValue("Association2")));
            }
            set
            {
                this.SetPropertyValue("Association2", value);
            }
        }
        public String NextOfKinEmail1
        {
            get
            {
                return ((string)(this.GetPropertyValue("NextOfKinEmail1")));
            }
            set
            {
                this.SetPropertyValue("NextOfKinEmail1", value);
            }
        }
        public String NextOfKinEmail2
        {
            get
            {
                return ((string)(this.GetPropertyValue("NextOfKinEmail2")));
            }
            set
            {
                this.SetPropertyValue("NextOfKinEmail2", value);
            }
        }
        public bool Active
        {
            get
            {
                return ((bool)(this.GetPropertyValue("Active")));
            }
            set
            {
                this.SetPropertyValue("Active", value);
            }
        }
        public String LicenceDate
        {
            get
            {
                return ((string)(this.GetPropertyValue("LicenceDate")));
            }
            set
            {
                this.SetPropertyValue("LicenceDate", value);
            }
        }
        public String SpecialConditions
        {
            get
            {
                return ((string)(this.GetPropertyValue("SpecialConditions")));
            }
            set
            {
                this.SetPropertyValue("SpecialConditions", value);
            }
        }
        public String CofCNo
        {
            get
            {
                return ((string)(this.GetPropertyValue("CofCNo")));
            }
            set
            {
                this.SetPropertyValue("CofCNo", value);
            }
        }

        public static int CompanystaffDiscriminator
        {
            get { return 1; }
        }
        public static int SystemuserDiscriminator
        {
            get { return 2; }
        }
        public static int Driverdiscriminator
        {
            get { return 3; }
        }
        public static int CustomerDiscriminator
        {
            get { return 4; }
        }
        public int DiscriminatorId
        {
            /*
                1. Company staff 
             *  2. System user
             *  3. Driver
             *  4. Customer
             */
            get
            {
                return ((int)(this.GetPropertyValue("DiscriminatorId")));
            }
            set
            {
                this.SetPropertyValue("DiscriminatorId", value);
            }
        }
        #region driver
        public int? DriverStatusId
        {
            get
            {
                return ((int)(this.GetPropertyValue("DriverStatusId")));
            }
            set
            {
                this.SetPropertyValue("DriverStatusId", value);
            }
        }

        #endregion
        public static AccountProfile GetProfile(string username)
        {
            return Create(username) as AccountProfile;
        }
        public static IEnumerable<getUserProfile_Result> GetDriver(int ClientId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return entities.getUserProfile(ClientId, Driverdiscriminator)
                    //.Where(v => v.ClientId == ClientId.ToString())
                   .ToList();
            }
        }

        public static IEnumerable<getUserProfile_Result> GetCustomer(int ClientId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return entities.getUserProfile(ClientId, Driverdiscriminator)
                    //.Where(v => v.ClientId == ClientId.ToString())
                   .ToList();
            }
        }

        public static ResponseModel CreateDriverUser(DriverRegisterModel mDriver, int ClientId, bool SendEmail, bool SendSms)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                ResponseModel _rm = new ResponseModel();
                MembershipProvider pmp = null;
                RegisterEndUserPhoneModel mRegister = mDriver.RegisterDriverUserModel;

                try
                {
                    pmp = Membership.Providers["MembershipProviderOther"]; //.Provider;
                    if (pmp.GetUser(mRegister.Username, false) == null)
                    {
                        string username = mRegister.Username.TrimStart('+');
                        string password = Service.Utility.GeneratePass().ToString();

                        MembershipCreateStatus createStatus = new MembershipCreateStatus();
                        MembershipUser pms = pmp.CreateUser(username, password, mDriver.DriverModel.Email1, "Question",
                                            "Answer", true, null, out createStatus);
                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            Roles.AddUserToRole(username, "Driver");
                            //Roles.AddUserToRole(username, "Driver");
                            // AccountProfile profile = AccountProfile.CurrentUser;
                            //MembershipUser user = Membership.GetUser();
                            AccountProfile profile = AccountProfile.Create(username, true) as AccountProfile;

                            profile.ClientId = ClientId;
                            profile.FullName = mDriver.DriverModel.FullNames;
                            profile.FirstName = mDriver.DriverModel.FirstName;
                            profile.LastName = mDriver.DriverModel.LastName;
                            profile.Email1 = mDriver.DriverModel.Email1;
                            profile.Telephone1 = mDriver.DriverModel.Telephone1;
                            profile.Email2 = mDriver.DriverModel.Email2;
                            profile.DriverStatusId = mDriver.DriverModel.DriverStatusId;
                            profile.Telephone2 = mDriver.DriverModel.Telephone2;
                            profile.IsMale = mDriver.DriverModel.IsMale;
                            profile.PostCode = mDriver.DriverModel.PostCode;
                            profile.FaxNumber = mDriver.DriverModel.FaxNumber;
                            profile.DriverNo = mDriver.DriverModel.DriverNo;
                            profile.LicenceDate = mDriver.DriverModel.LicenceDate;
                            profile.DriverRFID = mDriver.DriverModel.DriverRFID;
                            profile.DOB = mDriver.DriverModel.DOB;
                            profile.IDNumber = mDriver.DriverModel.IDNumber;
                            profile.NextOfKinName1 = mDriver.DriverModel.NextOfKinName1;
                            profile.NextOfKinName2 = mDriver.DriverModel.NextOfKinName2;
                            profile.NextOfKinPhone1 = mDriver.DriverModel.NextOfKinPhone1;
                            profile.NextOfKinPhone2 = mDriver.DriverModel.NextOfKinPhone2;
                            profile.Association1 = mDriver.DriverModel.Association1;
                            profile.Association2 = mDriver.DriverModel.Association2;
                            profile.NextOfKinEmail1 = mDriver.DriverModel.NextOfKinEmail1;
                            profile.NextOfKinEmail2 = mDriver.DriverModel.NextOfKinEmail2;
                            profile.Active = mDriver.DriverModel.Active;
                            profile.DiscriminatorId = 3;
                            profile.Save();
                        }
                        else
                        {
                            _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                            return _rm;
                        }


                        try
                        {

                            string BrandName = Service.Utility.getClientBrandName();
                            string RepytoAddress = Service.Utility.getReplyToAddress();
                            RepytoAddress = RepytoAddress + "," + Membership.GetUser();

                            string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

                            emailBody = emailBody.Replace("[newline]", "</br>");

                            string solnaddress = BrandName + System.Configuration.ConfigurationManager.AppSettings["taksihostname"].ToString();

                            emailBody = String.Format(emailBody, mRegister.Username.TrimStart('+'), password, solnaddress);

                            string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();

                            string smsBody = "Welcome to GFleet, Your Username is {0} and Password is {1}. Kindly Check your email for further Instructions";

                            smsBody = String.Format(smsBody, mRegister.Username.TrimStart('+'), password);

                            if (mDriver.DriverModel.Email1 != null)
                            {

                                EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
                                if (ET != null)
                                {
                                    emailBody = String.Format(ET.Body, mRegister.Username.TrimStart('+'), password, solnaddress);
                                    emailBody = ET.Header + emailBody + ET.Footer;
                                }
                                if (SendEmail)
                                {
                                    new Thread(() =>
                                    {
                                        try
                                        {
                                            Service.SendEmail.SendUserMngtEmail(mDriver.DriverModel.Email1, emailSubject, emailBody, RepytoAddress);
                                        }
                                        catch (Exception mex)
                                        {
                                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                            //throw new Exception(ex.Message.ToString());
                                        }
                                    }).Start();
                                }
                            }
                            //Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                            {
                                if (SendSms)
                                {
                                    new Thread(() =>
                                    {
                                        try
                                        {
                                            GFleetV3SMSProcessor.SendSMS.SendMessage(mRegister.Username, smsBody, BrandName);
                                        }
                                        catch (Exception mex)
                                        {
                                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                            //throw new Exception(ex.Message.ToString());
                                        }
                                    }).Start();
                                }
                                ////string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();
                                ////ModelCustomer.sendEnduserCreateSMSEmail(mCustEndUser.Email1, mRegister.Username, password, emailBody, "EndUserRegistration");
                                _rm.Status = true; _rm.Message = "Driver saved";// +mCustEndUser.FullNames + " successfully saved!";
                            }
                        }
                        catch (Exception ex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                pmp.DeleteUser(username, true);
                                _rm.Status = false; _rm.Message = "User not created.";

                            }
                        }
                        return _rm;
                    }

                    else
                    {
                        _rm.Message = "user already exists"; _rm.Status = false;
                        return _rm;
                    }
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    if (pmp != null)
                    {
                        pmp.DeleteUser(mRegister.Username, true);
                    }

                    _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                    return _rm;
                }
            }
        }


        public static ResponseModel DeleteUser(string UserId, int Discriminator)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true, Status = false };
            try
            {
                Guid userKey;// = new Guid(UserId);
                if (Guid.TryParse(UserId, out userKey))
                {
                    MembershipUser mu = Membership.GetUser(userKey);
                    if (mu != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            int ClientId = Service.Utility.getClientId().Value;
                            List<getUserProfile_Result> data = entities.getUserProfile(ClientId, Discriminator).Where(v => v.UserId == userKey).ToList();
                            if (data != null)
                            {

                                string userName = mu.UserName;
                                Membership.DeleteUser(userName, true);
                                _rm.Message = "The user" + userName + " successfully deleted";
                                _rm.Status = true;
                            }
                            else
                            {
                                _rm.Message = "The user does not exist in your system";
                            }
                        }
                    }
                    else
                    {
                        _rm.Message = "The user does not exist";
                    }
                }
                else
                {
                    _rm.Message = "The user ID is incorrect!";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);

                _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
            }


            return _rm;
        }

        public static UserValidation getUser(string UserId, int Discriminator)
        {
            try
            {
                Guid userKey;
                if (Guid.TryParse(UserId, out userKey))
                {
                    MembershipUser mu = Membership.GetUser(userKey);
                    if (mu != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            int ClientId = Service.Utility.getClientId().Value;
                            var data = (from p in entities.getUserProfile(ClientId, Discriminator).Where(v => v.UserId == userKey)
                                        select new UserValidation
                                        {
                                            FirstName = p.FirstName,
                                            LastName = p.LastName,
                                            IDNumber = p.IDNumber,
                                            Telephone1 = p.Telephone1,
                                            Telephone2 = p.Telephone2,
                                            Email1 = p.Email1,
                                            Email2 = p.Email2,
                                            NextOfKinEmail1 = p.NextOfKinEmail1,
                                            NextOfKinEmail2 = p.NextOfKinEmail2,
                                            NextOfKinName1 = p.NextOfKinName1,
                                            NextOfKinName2 = p.NextOfKinName2,
                                            NextOfKinPhone1 = p.NextOfKinPhone1,
                                            NextOfKinPhone2 = p.NextOfKinPhone2,
                                            PostCode = p.PostCode,
                                            Association1 = p.Association1,
                                            Association2 = p.Association2,
                                            DriverNo = p.DriverNo,
                                            DriverRFID = p.DriverRFID,
                                            LicenceDate = p.LicenceDate,
                                            IsMale = Boolean.Parse(p.IsMale),
                                            DriverStatusId = int.Parse(p.DriverStatusId),
                                            UserId = p.UserId
                                        }).FirstOrDefault();
                            //getUserProfile_Result data = entities.getUserProfile(ClientId, Discriminator).SingleOrDefault(v => v.UserId == userKey);
                            return data;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);

                return null;
            }

        }

        //public static getUserProfile_Result getUser(string UserId, int Discriminator)
        //{
        //    try
        //    {
        //        Guid userKey;
        //        if (Guid.TryParse(UserId, out userKey))
        //        {
        //            MembershipUser mu = Membership.GetUser(userKey);
        //            if (mu != null)
        //            {
        //                using (GFleetModelContainer entities = new GFleetModelContainer())
        //                {
        //                    int ClientId = Service.Utility.getClientId().Value;
        //                    getUserProfile_Result data = entities.getUserProfile(ClientId, Discriminator).SingleOrDefault(v => v.UserId == userKey);
        //                    return data;
        //                }
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (Exception mex)
        //    {
        //        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);

        //        return null;
        //    }

        //}

        public static ResponseModel DeleteDriver(String UserId)
        {
            return DeleteUser(UserId, Driverdiscriminator);
        }

        public static UserValidation getDriver(String UserId)
        {
            var data = getUser(UserId, Driverdiscriminator);

            return data;
        }

        //  return new ResponseModel()
        //{
        //        Status = data != null? true: false, Data = data, IsUpdate = true, Message = data != null? "user found " : "user not found"};
        //}
        //    using (GFleetModelContainer entities = new GFleetModelContainer())
        //    {
        //        int ClientId = Service.Utility.getClientId().Value;
        //        Driver data = entities.People.OfType<Driver>().Where(m => m.PersonId == UserId && m.ClientId == ClientId).FirstOrDefault();

        //        if (data != null)
        //        {
        //            data.DeletedBy = Guid.Parse(Service.Utility.getUserId().ToString());
        //            data.DeletedDate = DateTime.Parse(DateTime.Now.ToString());
        //            data.IsDelete = true;
        //            //data = (Person)
        //            entities.People.ApplyCurrentValues(data);
        //            entities.SaveChanges();
        //            _rm.Status = true; _rm.Message = "Driver deleted";// +data.FirstName + " " + data.LastName + " successfully deleted!";
        //            return _rm;
        //        }
        //        else
        //        {
        //            _rm.Status = false; _rm.Message = "Driver Not deleted!";
        //            return _rm;
        //        }
        //    }
        //        catch (Exception mex)
        //        {
        //            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
        //            _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
        //            return _rm;
        //        }
        //    }
        //}

        //public String LastName { get; set; }
        //public string Email1 { get; set; }
        //public String Telephone1 { get; set; }
        //public string Email2 { get; set; }
        //public String Telephone2 { get; set; }
        //public bool  IsMale { get; set; }
        //public String PostCode { get; set; }
        //public String FaxNumber { get; set; }
        //public String DriverNo { get; set; }
        //public String DriverRFID { get; set; }
        //public DateTime DOB { get; set; }
        //public String IDNumber { get; set; }
        //public String NextOfKinName1 { get; set; }
        //public String NextOfKinName2 { get; set; }
        //public String NextOfKinPhone1 { get; set; }
        //public String NextOfKinPhone2 { get; set; }
        //public String Association1 { get; set; }
        //public String Association2 { get; set; }
        //public String NextOfKinEmail1 { get; set; }
        //public String NextOfKinEmail2 { get; set; }
        //public Boolean Active { get; set; }
        //public String LicenceDate { get; set; }
        //public String SpecialConditions { get; set; }
        //public String CofCNo { get; set; }
        //public int? DriverStatusId { get; set; }
        //add additional properties here

        private ResponseModel RedirectToAction(string p)
        {
            throw new NotImplementedException();
        }


        public static ResponseModel CreateDriverUserThread()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                string driverpho = "254701471754,254702 394 597,254702441813,254702488983,254704756121,254704882983,254705 065 085,254705977017,254706431145,254706763400,254706765244,254707884652,254708005454,254708770519,254710 904 410,254710306868,254710734099,254710827360,254711322216,254711708578,254711957098,254711979305,254712192760,254712238155,254712327182,254714342863,254715299378,254715368188,254716158364,254716281501,254716312269,254716360590,254716883350,254717450379,254718469936,254718602219,254720 320 672,254720 344 051,254720 346 771,254720 654 834,254720 672 631,254720065956,254720098906,254720144178,254720204481,254720234543,254720268729,254720273834,254720299625,254720332535,254720402088,254720409032,254720414148,254720417984,254720421623,254720433223,254720465712,254720572746,254720597493,254720618577,254720675979,254720828953,254720920203,254721 489 953,254721 549 026,254721 706 183,254721 949 243,254721220495,254721223389,254721248502,254721378315,254721429054,254721482347,254721550942,254721637025,254721684260,254721735048,254721800717,254721918394,254721926167,254721975760,254721990246,254722 168 209,254722 392 275,254722 654 516,254722371472,254722476032,254722494942,254722505781,254722509148,254722546970,254722552051,254722585605,254722591411,254722691499,254722755037,254722819133,254722820073,254722884997,254722902756,254722912127,254723 522 272,254723 863 788,254723107217,254723304180,254723320427,254723410301,254723439558,254723590707,254723783904,254723859918,254723989905,254724 274 004,254724 373 225,254724 501 261,254724 595 721,254724158976,254724304306,254724389697,254724471605,254724516755,254724680110,254724772929,254724806741,254724867371,254724943057,254725 157 476,254725 433 567,254725 554 014,254725 765 616,254725068586,254725156158,254725559958,254725644640,254725783030,254725795637,254725840418,254725841167,254725919193,254725942254,254725970645,254726 327 529,254726 488 864,254726018642,254726394692,254727 563 605,254727 939 183,254727271249,254727419475,254727616470,254727660842,254727717482,254727721095,254727752407,254727900400,254728348508,254728446022,254729 516 600,254729282604,254729753534,254729850209,254729974265,254729997035,254732943775,254733175782,254733250373,254733417945,254733629619,254733702750,254733939950,254734808842,254750 841 055,25475642587,254771255442,254773761260,254775806186,254787180887,254788718317,254789331123";
                string[] driver = driverpho.Split(',');


                foreach (var data in new GFleetModelContainer().People.OfType<Driver>()
                    .Where(m => driver.Contains(m.Telephone1)))
                {
                    //Person dperson = entities.People.OfType<Driver>().Where(m=>m.);
                    //Driver data = entities.People.OfType<Driver>().FirstOrDefault();
                    //ResponseModel _rm = new ResponseModel();
                    MembershipProvider pmp = null;

                    try
                    {
                        pmp = Membership.Providers["MembershipProviderOther"]; //.Provider;

                        new Thread(() =>
                        {
                            try
                            {

                                if (pmp.GetUser(data.Telephone1.Replace(" ", ""), false) == null)
                                {
                                    //string username = mRegister.Username.TrimStart('+');
                                    string username = data.Telephone1.Replace(" ", "");
                                    string password = Service.Utility.GeneratePass().ToString();

                                    MembershipCreateStatus createStatus = new MembershipCreateStatus();
                                    MembershipUser pms = pmp.CreateUser(username, password, data.Email1, "Question",
                                                        "Answer", true, null, out createStatus);
                                    if (createStatus == MembershipCreateStatus.Success)
                                    {
                                        Roles.AddUserToRole(username, "Driver");
                                        //Roles.AddUserToRole(username, "Driver");
                                        // AccountProfile profile = AccountProfile.CurrentUser;
                                        //MembershipUser user = Membership.GetUser();
                                        AccountProfile profile = AccountProfile.Create(username, true) as AccountProfile;

                                        profile.ClientId = data.ClientId.Value;
                                        profile.FullName = data.FullNames;
                                        profile.FirstName = data.FirstName;
                                        profile.LastName = data.LastName;
                                        profile.Email1 = data.Email1;
                                        profile.Telephone1 = data.Telephone1.Replace(" ", "");
                                        profile.Email2 = data.Email2;
                                        profile.DriverStatusId = data.DriverStatusId.Value;
                                        profile.Telephone2 = data.Telephone2;
                                        profile.IsMale = data.IsMale;
                                        profile.PostCode = data.PostCode;
                                        profile.FaxNumber = data.FaxNumber;
                                        profile.DriverNo = data.DriverNo;
                                        profile.LicenceDate = String.Format("{0:yyyy-MM-dd}", data.LicenceDate.ToString());
                                        profile.DriverRFID = data.DriverRFID;
                                        profile.DOB = data.DOB.ToString();
                                        profile.IDNumber = data.IDNumber;
                                        profile.NextOfKinName1 = data.NextOfKinName1;
                                        profile.NextOfKinName2 = data.NextOfKinName2;
                                        profile.NextOfKinPhone1 = data.NextOfKinPhone1;
                                        profile.NextOfKinPhone2 = data.NextOfKinPhone2;
                                        profile.Association1 = data.Association1;
                                        profile.Association2 = data.Association2;
                                        profile.NextOfKinEmail1 = data.NextOfKinEmail1;
                                        profile.NextOfKinEmail2 = data.NextOfKinEmail2;
                                        profile.Active = true;
                                        profile.DiscriminatorId = 3;
                                        profile.Save();
                                    }
                                    else
                                    {
                                        //_rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                                        //return _rm;
                                    }


                                }
                                else
                                {
                                    //_rm.Message = "user already exists"; _rm.Status = false;
                                    string uhuh = "user already exists";
                                    //return _rm;
                                }
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        if (pmp != null)
                        {
                            // pmp.DeleteUser(username, true);
                        }

                        // _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                        //return _rm;
                    }
                }
                return new ResponseModel(); ;
            }
        }
    }
}