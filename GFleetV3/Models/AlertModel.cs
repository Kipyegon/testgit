﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class PagedAlertTypeModel
    {
        public IEnumerable<AlertType> AlertType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedAlertRecepientModel
    {
        public IEnumerable<AlertRecipient> AlertRecipient { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedRecepientAlertTypeModel
    {
        public IEnumerable<AlertReceiverAlert> RecepientAlertType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class ModelAlertType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<AlertType> GetAlertType()
        {
            return entities.AlertTypes.ToList();
        }

        //For Custom Paging

        public IEnumerable<AlertType> GetAlertTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.AlertTypes
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountAlertType()
        {
            return entities.AlertTypes.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public AlertType GetAlertType(int mAlertTypeId)
        {
            return entities.AlertTypes.Where(m => m.AlertTypeId == mAlertTypeId).FirstOrDefault();
        }
        public AlertType GetVehicleAlertType(int mAlertTypeId)
        {
            return entities.AlertTypes.Where(m => m.AlertTypeId == mAlertTypeId).FirstOrDefault();
        }

        public ResponseModel CreateAlertType(AlertType mAlertType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                mAlertType.AlertTypeId = mAlertType.AlertTypeId;
                entities.AlertTypes.AddObject(mAlertType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "AlertType saved ";// +mAlertType.AlertTypeName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateAlertType(AlertType mAlertType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    AlertType data = entities.AlertTypes.Where(m => m.AlertTypeId == mAlertType.AlertTypeId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mAlertType;

                        mAlertType = (AlertType)entities.AlertTypes.ApplyCurrentValues(mAlertType);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "AlertType updated";// +mAlertType.AlertTypeName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "AlertType " + mAlertType.AlertTypeName + " is not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteAlertType(int mAlertTypeID)
        {
            try
            {
                AlertType data = entities.AlertTypes.Where(m => m.AlertTypeId == mAlertTypeID).FirstOrDefault();
                entities.AlertTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelAlertRecipient : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<AlertRecipient> GetAlertRecipient()
        {
            return entities.AlertRecipients.Where(v => v.ClientId == ClientId).ToList();
        }

        public IEnumerable<AlertRecipient> GetAlertRecipientPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.AlertRecipients
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountAlertRecipient()
        {
            return entities.AlertRecipients.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public AlertRecipient GetAlertRecipient(int mAlertRecipientId)
        {
            return entities.AlertRecipients.Where(m => m.AlertRecipientId == mAlertRecipientId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateAlertRecipient(AlertRecipient mAlertRecipient)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.AlertRecipients.AddObject(mAlertRecipient);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Alert Recipient saved ";// +mAlertRecipient.Names + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateAlertRecipient(AlertRecipient mAlertRecipient)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    AlertRecipient data = entities.AlertRecipients.Where(m => m.AlertRecipientId == mAlertRecipient.AlertRecipientId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mAlertRecipient;

                        mAlertRecipient = (AlertRecipient)entities.AlertRecipients.ApplyCurrentValues(mAlertRecipient);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Alert  Recipient updated ";// +mAlertRecipient.Names + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Alert  Recipient " + mAlertRecipient.Names + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteAlertRecipient(int mAlertRecipientID)
        {
            try
            {
                AlertRecipient data = entities.AlertRecipients.Where(m => m.AlertRecipientId == mAlertRecipientID && m.ClientId == ClientId).FirstOrDefault();
                entities.AlertRecipients.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelRecepientAlertType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<AlertReceiverAlert> GetRecepientAlertType()
        {
            return entities.AlertReceiverAlerts.Where(v => v.ClientId == ClientId).ToList();
        }

        public IEnumerable<AlertReceiverAlert> GetRecepientAlertTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.AlertReceiverAlerts
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountRecepientAlertType()
        {
            return entities.AlertReceiverAlerts.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public AlertReceiverAlert GetRecepientAlertType(int mRecepientAlertTypeId)
        {
            return entities.AlertReceiverAlerts.Where(m => m.AlertReceiverAlertId == mRecepientAlertTypeId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateRecepientAlertType(AlertReceiverAlert mRecepientAlertType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.AlertReceiverAlerts.AddObject(mRecepientAlertType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Alert Recipient pegged ";// + mRecepientAlertType.AlertRecipient.Names + " successfully pegged to Alert Type" + mRecepientAlertType.AlertType.AlertTypeName;
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateRecepientAlertType(AlertReceiverAlert mRecepientAlertType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    AlertReceiverAlert data = entities.AlertReceiverAlerts.Where(m => m.AlertReceiverAlertId == mRecepientAlertType.AlertReceiverAlertId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mRecepientAlertType;

                        mRecepientAlertType = (AlertReceiverAlert)entities.AlertReceiverAlerts.ApplyCurrentValues(mRecepientAlertType);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Alert Recipient pegged ";// + mRecepientAlertType.AlertRecipient.Names + " successfully pegged to Alert Type" + mRecepientAlertType.AlertType.AlertTypeName; ;
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Alert  Recipient " + mRecepientAlertType.AlertRecipient.Names + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteRecepientAlertType(int mRecepientAlertTypeID)
        {
            try
            {
                AlertReceiverAlert data = entities.AlertReceiverAlerts.Where(m => m.AlertReceiverAlertId == mRecepientAlertTypeID && m.ClientId == ClientId).FirstOrDefault();
                entities.AlertReceiverAlerts.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }

    public class SendEmailDisconnectModel
    {
        public static void sendEmailDisconnect(int ClientId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                try
                {
                    entities.SendDisconnectionEmail(ClientId);
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                }
            }
        }
    }

}