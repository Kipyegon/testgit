﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Objects;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.Security;
using System.Threading;
using DataAccess.SQL;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GFleetV3.Models
{
    public class BackUpData
    {
        public IEnumerable<BackUpGridData> BackUpGridData { get; set; }

        [Required(ErrorMessage = "Start Date Required")]
        [Display(Name = "Start Date:")]
        [DisplayFormat(DataFormatString = "{0:d}", ConvertEmptyStringToNull = true)]
        public string startDate { get; set; }

        [Required(ErrorMessage = "End Date Required")]
        [Display(Name = "End Date:")]
        [DisplayFormat(DataFormatString = "{0:d}", ConvertEmptyStringToNull = true)]
        public string endDate { get; set; }

        [Required(ErrorMessage = "Start Time Required")]
        [Display(Name = "Start Time:")]
        [DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String startTime { get; set; }

        [Required(ErrorMessage = "End Time Required")]
        [Display(Name = "End Time:")]
        [DisplayFormat(DataFormatString = "{0:t}", ConvertEmptyStringToNull = true)]
        public String endTime { get; set; }

        public BackUpData()
        {
            BackUpGridData = new List<BackUpGridData>();
        }
    }

    public class BackUpGridData
    {
        public string FileName { get; set; }
        public DateTime FileDate { get; set; }
        public string FileSize { get; set; }

        public BackUpGridData()
        {
            //FileName = "";
        }
    }
    public class BackUpModel
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public static string createDataBackUp(DateTime startdate, DateTime endDate, int clientId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                //int clientId = Service.Utility.getClientId().Value;
                ObjectParameter virtualDirectoryPath = new ObjectParameter("virtualDirectoryPath", typeof(String));
                int result = entities.dataBackUp(clientId, startdate, endDate, virtualDirectoryPath);

                return virtualDirectoryPath.Value.ToString();
            }
        }

        public static string createDataBackUp(DateTime startdate, DateTime endDate)
        {
            int clientId = Service.Utility.getClientId().Value;

            return createDataBackUp(startdate, endDate, clientId);
        }

        public static void sendBackUpMail(string subject, string body, string email)
        {
            Service.SendEmail.SendUserMngtEmail(email, subject, body);
        }
    }

    public class BackUpDownloadResult<Model> : ActionResult
    {
        string _fileName;
        ControllerContext _context;

        public BackUpDownloadResult(ControllerContext context, string fileName)
        {
            this._context = context;
            this._fileName = fileName;
        }

        void WriteFile(string content = null)
        {
            HttpContext context = HttpContext.Current;

            string fname = this._fileName.Split('\\').Last().ToString();

            context.Response.Buffer = false;
            context.Response.ContentType = "application/octet-stream";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fname + "\"");

            context.Response.TransmitFile(_fileName);

            #region Clear Buffer
            //context.Response.Clear();
            #endregion

            #region Close Buffer
            context.Response.End();
            #endregion
        }

        public override void ExecuteResult(ControllerContext context)
        {
            //string content = this.RenderViewToString();
            this.WriteFile();
        }
    }
}