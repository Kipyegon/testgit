﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class PagedClientModel
    {
        public IEnumerable<Client> Client { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedClientTypeModel
    {
        public IEnumerable<ClientType> ClientType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedClientStaffModel
    {
        public IEnumerable<aspnet_Membership> ClientStaff { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
        public ClientStaffRegisterModel ClientRegStaffModel { get; set; }
    }

    public class ModelClient : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public IEnumerable<Client> GetClient()
        {
            return entities.Clients.ToList();
        }

        //For Custom Paging

        public IEnumerable<Client> GetClientPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;
            if (Roles.IsUserInRole("Super Admin"))
            {
                return entities.Clients
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            }
            //else
            //{
            string[] SharpClientIds = System.Configuration.ConfigurationManager.AppSettings["SharpClientIds"].ToString().Split(',');
            List<int> list = new List<int>();

            foreach (string id in SharpClientIds)
            {
                list.Add(int.Parse(id));
            }

            return entities.Clients
              .OrderBy(orderCriteria)
              .Where(v => list.Contains(v.ClientId))
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        //}
        public int CountClient()
        {
            return entities.Clients.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Client GetClient(int mClientId)
        {
            return entities.Clients.Where(m => m.ClientId == mClientId).FirstOrDefault();
        }

        public ResponseModel CreateClientAdminUser(ClientRegisterModel mClientReg)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            SystemUser mSystemUser = mClientReg.ClientAdminStaffModel;
            Client mClient = mClientReg.ClientModel;
            RegisterClientModel mRegister = mClientReg.RegisterModel;
            try
            {

                if (!entities.Clients.Any(v => v.ClientName.Equals(mClient.ClientName) && v.ClientEmail.Equals(mClient.ClientEmail)))
                {

                    if (!string.IsNullOrEmpty(mRegister.UserRole.RoleName))
                    {

                        pmp = Membership.Provider;
                        if (pmp.GetUser(mRegister.Email, false) == null)
                        {
                            MembershipCreateStatus createStatus = new MembershipCreateStatus();
                            MembershipUser pms = pmp.CreateUser(mRegister.Email, mRegister.Password, mRegister.Email, "Question",
                                                "Answer", true, null, out createStatus);
                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                Roles.AddUserToRole(mRegister.Email, mRegister.UserRole.RoleName);
                                if (mSystemUser == null)
                                {
                                    mSystemUser = new SystemUser()
                                    {
                                        ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                        DateModified = DateTime.Now,
                                        Client = null,
                                        UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                                    };
                                }
                                else
                                {
                                    mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                    mSystemUser.DateModified = DateTime.Now;
                                    mSystemUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                                }

                                //string emailBody = "Welcome to Gfleet! </br> </br>";
                                //emailBody += "Your Credentials are as below: </br>";
                                //emailBody += "Username: {0} </br>";
                                //emailBody += "Password: {1} </br> </br>";
                                //emailBody += "Kindly logon in to the system via {2} </br>";
                                //emailBody += "For any further queries please do not hesitate to make contact. </br> </br>";
                                //emailBody += "Regards. </br>";
                                //emailBody += "Gtrack Team. </br>";
                                //emailBody += "support@geeckoltd.com";
                                string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBody"].ToString();

                                emailBody = emailBody.Replace("[newline]", "</br>");

                                string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
                                string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                                string newportsolnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();
                                string solnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();

                                if (url.Contains(AccessUrls.ToString()))
                                {
                                    emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, newportsolnaddress);
                                }
                                else
                                {
                                    emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, solnaddress);
                                }
                                //string emailSubject = "Sharp Account registration.";
                                string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubject"].ToString();

                                Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                            }
                            else
                            {
                                _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                                return _rm;
                            }

                            mClient.DateModified = DateTime.Now;
                            mClient.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());

                            mSystemUser.Client = mClient;
                            entities.People.AddObject(mSystemUser);
                            entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                            _rm.Status = true; _rm.Message = "Client saved";// +mClient.ClientName + " successfully saved!";
                            return _rm;
                        }
                        else
                        {
                            _rm.Message = "user already exists"; _rm.Status = false;
                            return _rm;
                        }
                    }
                    else
                    {
                        _rm.Message = "No Role name specified"; _rm.Status = false;
                        return _rm;
                    }
                }
                else
                {
                    _rm.Message = "The client already exists"; _rm.Status = false;
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateClient(Client mClient)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Client data = entities.Clients.Where(m => m.ClientId == mClient.ClientId).FirstOrDefault();
                    data = mClient;

                    mClient = (Client)entities.Clients.ApplyCurrentValues(mClient);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Client updated";// +mClient.ClientName + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteCompany(int mClientID)
        {
            try
            {
                Client data = entities.Clients.Where(m => m.ClientId == mClientID).FirstOrDefault();
                entities.Clients.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelClientStaff : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<SystemUser> GetClientStaff()
        {
            return entities.People.OfType<SystemUser>().Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<aspnet_Membership> GetClientStaffPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            int clientId = Service.Utility.getClientId().Value;

            //return entities.aspnet_Membership
            //  .OrderBy(orderCriteria)
            //  .Where(v => v.People.Any(n => n.ClientId == clientId) &&
            //              v.aspnet_Users.aspnet_Roles.Any(n => n.RoleName == "Company Staff"))
            //  .Skip((pageNumber - 1) * pageSize)
            //  .Take(pageSize)
            //  .ToList();
            return entities.aspnet_Membership
              .OrderBy(orderCriteria)
              .Where(v => v.People.Any(n => n.ClientId == clientId) &&
                          v.aspnet_Users.aspnet_Roles.Any(n => n.RoleName == "Company Staff" || n.RoleName == "Company Admin" || n.RoleName == "Company Supervisor"))
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public IEnumerable<aspnet_Membership> GetClientAdminPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            int clientId = Service.Utility.getClientId().Value;

            return entities.aspnet_Membership
              .OrderBy(orderCriteria)
              .Where(v => v.People.Any(n => n.ClientId == clientId) &&
                          v.aspnet_Users.aspnet_Roles.Any(n => n.RoleName == "Company Admin"))
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountClientStaff()
        {
            int clientId = Service.Utility.getClientId().Value;
            return entities.aspnet_Membership.Where(v => v.People.Any(n => n.ClientId == clientId) &&
                                                    v.aspnet_Users.aspnet_Roles.Any(n => n.RoleName == "Company Staff" && n.RoleName == "Company Admin" && n.RoleName == "Company Supervisor")).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public SystemUser GetClientStaff(int mClientStaffId)
        {
            return entities.People.OfType<SystemUser>().Where(m => m.PersonId == mClientStaffId && m.ClientId == ClientId).FirstOrDefault();
        }

        public aspnet_Users GetClientStaffRoles(Guid mClientStaffId)
        {
            return entities.aspnet_Users.Where(m => m.UserId == mClientStaffId).FirstOrDefault();
        }

        public ResponseModel CreateClientStaff(ClientStaffRegisterModel mClientStaff, string[] roles)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            SystemUser mSystemUser = mClientStaff.ClientStaffModel;
            RegisterStaffModel mRegister = mClientStaff.RegisterStaffModel;
            try
            {
                if (!string.IsNullOrEmpty(mRegister.UserRole.RoleName))
                {

                    pmp = Membership.Provider;
                    if (pmp.GetUser(mRegister.Email, false) == null)
                    {
                        MembershipCreateStatus createStatus = new MembershipCreateStatus();
                        MembershipUser pms = pmp.CreateUser(mRegister.Email, mRegister.Password, mRegister.Email, "Question",
                                            "Answer", true, null, out createStatus);
                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            Roles.AddUserToRole(mRegister.Email, mRegister.UserRole.RoleName);
                            if (roles != null)
                            {
                                foreach (string role in roles)
                                {
                                    Roles.AddUserToRole(mRegister.Email, role);
                                }
                            }
                            if (mSystemUser == null)
                            {
                                mSystemUser = new SystemUser()
                                {
                                    ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                    DateModified = DateTime.Now,
                                    Client = null,
                                    UserId = Guid.Parse(pms.ProviderUserKey.ToString()),
                                    ClientId = ClientId
                                };
                            }
                            else
                            {
                                mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                mSystemUser.DateModified = DateTime.Now;
                                mSystemUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                            }

                            string emailBody = "Welcome to Gfleet! </br> </br>";
                            emailBody += "Your Credentials are as below: </br>";
                            emailBody += "Username: {0} </br>";
                            emailBody += "Password: {1} </br> </br>";
                            emailBody += "Kindly logon in to the system via {2} </br>";
                            emailBody += "For any further queries please do not hesitate to make contact. </br> </br>";
                            emailBody += "Regards. </br>";
                            emailBody += "Gtrack Team. </br>";
                            emailBody += "support@geeckoltd.com";


                            string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
                            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                            string newportsolnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();
                            string solnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();

                            if (url.Contains(AccessUrls.ToString()))
                            {
                                emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, newportsolnaddress);
                            }
                            else
                            {
                                emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, solnaddress);
                            }

                            string emailSubject = "Gfleet Account registration.";

                            Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);

                        }
                        else
                        {
                            _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                            return _rm;
                        }

                        entities.People.AddObject(mSystemUser);
                        entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        _rm.Status = true; _rm.Message = "User saved";// +mRegister.Email + " successfully saved!";
                        return _rm;
                    }
                    else
                    {
                        _rm.Message = "user already exists";
                        _rm.Status = false;
                        return _rm;
                    }
                }
                else
                {
                    _rm.Message = "No Role name specified";
                    _rm.Status = false;
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel DisapproveUser(string username)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            try
            {
                MembershipUser muUser = Membership.GetUser(username);
                if (entities.People.Any(b => b.ClientId == ClientId && b.UserId == userId))
                {
                    muUser.IsApproved = false;
                    Membership.UpdateUser(muUser);
                    string success = "User disapproved";// +username + " disapproved from the system!";
                    _rm.Message = success; _rm.Status = true;
                }
                else
                {
                    string success = "User " + username + " not in your company!";
                    _rm.Message = success; _rm.Status = false;
                }
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    //pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel ApproveUser(string username)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            try
            {
                MembershipUser muUser = Membership.GetUser(username);
                if (entities.People.Any(b => b.ClientId == ClientId && b.UserId == userId))
                {
                    muUser.IsApproved = true;
                    Membership.UpdateUser(muUser);
                    string success = "User approved";// +username + " approved to use the system!";
                    _rm.Message = success; _rm.Status = true;
                }
                else
                {
                    string success = "User " + username + " not in your company!";
                    _rm.Message = success; _rm.Status = false;
                }
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    //pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UnLockUserDetails(string username)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            try
            {
                MembershipUser muUser = Membership.GetUser(username);
                if (entities.People.Any(b => b.ClientId == ClientId && b.UserId == userId))
                {
                    muUser.UnlockUser();
                    //.IsApproved = true;
                    //Membership.getUser(muUser);
                    string success = "User unlocked";// +username + " UnLocked from the system!";
                    _rm.Message = success; _rm.Status = true;
                }
                else
                {
                    string success = "User " + username + " not in your company!";
                    _rm.Message = success; _rm.Status = false;
                }
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    //pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel ChangeUserPassword(string username, PagedClientStaffModel mClientStaff)
        {
            ResponseModel _rm = new ResponseModel();
            string newPass;
            MembershipProvider pmp = null;
            string success;
            newPass = mClientStaff.ClientRegStaffModel.RegisterStaffModel.Password;
            try
            {
                MembershipUser muUser = Membership.GetUser(username);
                if (entities.People.Any(b => b.ClientId == ClientId))
                {
                    MembershipUser mu = Membership.Providers["MembershipProviderOther"].GetUser(username, false);

                    if (mu.ChangePassword(mu.ResetPassword(), newPass))
                    {
                        success = "Password for user changed";// +username + " successfully changed!";
                        _rm.Message = success; _rm.Status = true;
                    }
                    else
                    {
                        success = "error";
                        _rm.Message = success; _rm.Status = true;
                    }

                }
                else
                {
                    success = "User " + username + " not in your company!";
                    _rm.Message = success; _rm.Status = false;
                }
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    //pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateClientStaff(Person mClientStaff)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Person data = entities.People.Where(m => m.PersonId == mClientStaff.PersonId && m.ClientId == ClientId).FirstOrDefault();
                    if (data != null)
                    {
                        data = mClientStaff;

                        mClientStaff = (Person)entities.People.ApplyCurrentValues(mClientStaff);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Staff updated";// +mClientStaff.FirstName + " " + mClientStaff.LastName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Staff " + mClientStaff.FirstName + " " + mClientStaff.LastName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteClientStaff(int mClientStaffID)
        {
            try
            {
                SystemUser data = entities.People.OfType<SystemUser>().Where(m => m.PersonId == mClientStaffID && m.ClientId == ClientId).FirstOrDefault();
                entities.People.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelClientType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        // private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<ClientType> GetClientType()
        {
            return entities.ClientTypes.ToList();
        }

        //For Custom Paging

        public IEnumerable<ClientType> GetClientTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.ClientTypes
              .OrderBy(orderCriteria)
                // .Where(v => v.ClientTypeId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }

        public int CountClientType()
        {
            return entities.ClientTypes.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public ClientType GetClientType(int mClientTypeId)
        {
            return entities.ClientTypes.FirstOrDefault();
        }


        public ResponseModel CreateClientType(ClientType mClientType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.ClientTypes.AddObject(mClientType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "Client Type saved";// +mClientType.ClientTypeName + " successfully saved!";
                _rm.Data = new
                {
                    ClientTypeName = mClientType.ClientTypeName,
                    //Description = mClientType.Description,
                    Id = mClientType.ClientTypeId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateClientType(ClientType mClientType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    ClientType data = entities.ClientTypes.Where(m => m.ClientTypeId == mClientType.ClientTypeId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mClientType;

                        mClientType = (ClientType)entities.ClientTypes.ApplyCurrentValues(mClientType);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Client Type updated";// +mClientType.ClientTypeName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Client Type " + mClientType.ClientTypeName + " is not in you company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public static string getClientTypeLogoUrl()
        {
            int? ClientId = Service.Utility.getClientId();
            if (ClientId.HasValue)
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    try
                    {
                        vw_ClientTypeDetails _cl = entities.vw_ClientTypeDetails.SingleOrDefault(v => v.ClientId == ClientId.Value);

                        return _cl != null ? _cl.LogoUrl : null;
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        return null;
                    }
                }
            } return null;
        }

        public bool DeleteClientType(int mClientTypeID)
        {
            try
            {
                ClientType data = entities.ClientTypes.Where(m => m.ClientTypeId == mClientTypeID).FirstOrDefault();
                entities.ClientTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ClientStaffRegisterModel
    {
        public RegisterStaffModel RegisterStaffModel { get; set; }
        public SystemUser ClientStaffModel { get; set; }
        public List<string> isInDispatchBooking { get; set; }
    }
    public class ClientRegisterModel
    {
        public RegisterClientModel RegisterModel { get; set; }
        public Client ClientModel { get; set; }
        public SystemUser ClientAdminStaffModel { get; set; }

        public static ClientRegisterModel getClientRegisterModel(int clientId)
        {
            ClientRegisterModel crm = new ClientRegisterModel();
            crm.ClientModel = new ModelClient().GetClient(clientId);

            return crm;
        }
    }


}