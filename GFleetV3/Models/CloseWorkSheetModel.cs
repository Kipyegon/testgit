﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DataAccess.SQL;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Reflection;
using System.Web.Security;
using System.Text;


namespace GFleetV3.Models
{
    public class CloseWorkSheetModel
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();
        public class JourneyData
        {
            public string Customername { get; set; }
            public string VehicleReg { get; set; }
            public long? StartMileage { get; set; }
            public long? EndMileage { get; set; }
            public decimal? Amount { get; set; }
            public string PickUpDateTime { get; set; }
            public DateTime PickUpNDateTime { get; set; }
            public DateTime DateModified { get; set; }
            public string Organisation { get; set; }
            public string PickUpdate { get; set; }
            public int? RefNo { get; set; }
            public string POB { get; set; }
            public string strDModified
            {
                get
                {
                    return DateModified.ToString("yyyy-MM-dd HH:mm:ss.fff");
                }
            }
        }
        public List<JourneyData> journeyData { get; set; }
        public static CloseWorkSheetModel getJourneyData(string VehicleId, string startdate, string enddate)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                CloseWorkSheetModel bookdata = new CloseWorkSheetModel();
                bookdata.journeyData = GetJourneyData(VehicleId, startdate, enddate);
                //bookdata.UserRole = getRoleId();
                if (bookdata.journeyData == null)
                {
                    bookdata.journeyData = new List<JourneyData>();
                }
                return bookdata;
            }

        }

        public static List<JourneyData> GetJourneyData(string VehicleId, string startdate, string enddate)
        {

            DateTime sdate; DateTime edate;
            Boolean goodDate = false;

            enddate = HttpUtility.UrlDecode(enddate);     //endDate = endDate.Replace("+", ""); 
            startdate = HttpUtility.UrlDecode(startdate);   //startDate = startDate.Replace("+", "");

           // enddate += " 23:59:59.999";

            if (DateTime.TryParse(startdate, out sdate))
            {
                if (DateTime.TryParse(enddate, out edate))
                {
                    goodDate = true;
                    int ClientId = Service.Utility.getClientId().Value;
                    try
                    {

                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            var data = (from p in entities.getJourneyData(int.Parse(VehicleId), ClientId)
                                        select new JourneyData
                                        {
                                            Customername = p.Customername,
                                            Organisation = p.Organisationname,
                                            VehicleReg = p.VehicleRegCode,
                                            RefNo = p.DispatchId,
                                            PickUpDateTime = p.PickUpDateTime,
                                            POB = p.POBNumber,
                                            EndMileage = p.EndMileage,
                                            StartMileage = p.StartMileage,
                                            Amount = p.Amount,
                                            PickUpNDateTime=p.PickUpDateNTime.Value,
                                        }).Where(v => v.PickUpNDateTime >= sdate && v.PickUpNDateTime <= edate).ToList();
                            return data;
                        }
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        return null;
                    }
                }
                return null;
            }
            return null;
        }

        public ResponseModel CloseWorkSheet(string StartMileage, string Amount, string EndMileage, string DispatchId)
        {
            int ClientId = Service.Utility.getClientId().Value;
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.CloseWorkSheet(int.Parse(DispatchId), int.Parse(StartMileage), int.Parse(EndMileage), ClientId, int.Parse(Amount));
                _rm.Status = true;
                _rm.Message = "Work Sheet successfully closed!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
    }
}