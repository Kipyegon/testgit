﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class CompanySettingsModel
    {
        public static string getCompanySettings_gridheight()
        {
            int? ClientId = Service.Utility.getClientId();
            if (ClientId.HasValue)
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    try
                    {
                        CompanySetting company = entities.CompanySettings.SingleOrDefault(v => v.ClientId == ClientId);

                        return company != null ? company.TableGridHeight : null;
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        return null;
                    }
                }
            } return null;
        }
        public static string getCompanySettings_gridheighit()
        {
            int? ClientId = Service.Utility.getClientId();
            if (ClientId.HasValue)
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    try
                    {
                        CompanySetting company = entities.CompanySettings.SingleOrDefault(v => v.ClientId == ClientId);

                        return company != null ? company.TableGridHeight : null;
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        return null;
                    }
                }
            } return null;
        }

        public static void setCompanySettings_height(string newValue)
        {
            int? ClientId = Service.Utility.getClientId();
            if (ClientId.HasValue)
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    try
                    {
                        CompanySetting company = entities.CompanySettings.SingleOrDefault(v => v.ClientId == ClientId);
                        if (company != null)
                            company.TableGridHeight = newValue;

                        entities.SaveChanges();
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    }
                }
            }
        }


    }
}