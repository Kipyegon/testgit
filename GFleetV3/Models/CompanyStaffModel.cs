﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{

    public class PagedCompanyStaffModel
    {
        public IEnumerable<CompanyStaff> CompanyStaff { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }


    public class ModelCompanyStaff : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<CompanyStaff> GetCompanyStaff()
        {
            int ClientId = Service.Utility.getClientId().Value;
            return entities.People.OfType<CompanyStaff>().Where(v => v.ClientId == ClientId && v.IsDelete == false).ToList();
        }

        public IEnumerable<CompanyStaff> GetCompanyStaffPage(int pageNumber, int pageSize, string orderCriteria)
        {
            int ClientId = Service.Utility.getClientId().Value;
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.People.OfType<CompanyStaff>()
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId && v.IsDelete == false)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }

        public int CountCompanyStaff()
        {
            int ClientId = Service.Utility.getClientId().Value;
            return entities.People.OfType<CompanyStaff>().Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        public CompanyStaff GetCompanyStaff(int mCompanyStaffId)
        {
            int ClientId = Service.Utility.getClientId().Value;
            return entities.People.OfType<CompanyStaff>().Where(m => m.PersonId == mCompanyStaffId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateCompanyStaff(CompanyStaff mCompanyStaff)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                entities.People.AddObject(mCompanyStaff);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "CompanyStaff saved";// +mCompanyStaff.FirstName + " " + mCompanyStaff.LastName + " successfully saved!"; _rm.Data = mCompanyStaff;
                _rm.Data = new
                {
                    FullNames = mCompanyStaff.FullNames,
                    Telephones = mCompanyStaff.Telephones,
                    IDNumber = mCompanyStaff.IDNumber,
                    Id = mCompanyStaff.PersonId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateCompanyStaff(Person mCompanyStaff)
        {
            int ClientId = Service.Utility.getClientId().Value;
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Person data = entities.People.Where(m => m.PersonId == mCompanyStaff.PersonId && m.ClientId == ClientId).FirstOrDefault();


                    data = mCompanyStaff;

                    mCompanyStaff = (Person)entities.People.ApplyCurrentValues(mCompanyStaff);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "CompanyStaff updated";// +mCompanyStaff.FirstName + " " + mCompanyStaff.LastName + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel DeleteCompanyStaff(int mCompanyStaffID)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    int ClientId = Service.Utility.getClientId().Value;
                    CompanyStaff data = entities.People.OfType<CompanyStaff>().Where(m => m.PersonId == mCompanyStaffID && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data.DeletedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        data.DeletedDate = DateTime.Parse(DateTime.Now.ToString());
                        data.IsDelete = true;
                        //data = (Person)
                        entities.People.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "CompanyStaff deleted";// +data.FirstName + " " + data.LastName + " successfully deleted!";
                        return _rm;
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "CompanyStaff Not deleted!";
                        return _rm;
                    }
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
    }
}