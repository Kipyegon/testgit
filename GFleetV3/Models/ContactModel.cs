﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{

    public class PagedContactModel
    {
        public IEnumerable<Contact> Contact { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class PagedContactTypeModel
    {
        public IEnumerable<ContactType> ContactType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class ModelContact : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Contact> GetContact()
        {
            return entities.Contacts.Where(v => v.ClientId == ClientId).ToList();
        }

        public IEnumerable<object> GetContactDetails()
        {
            ResponseModel _rm = new ResponseModel();
            return entities.Contacts.Where(b => b.ClientId == ClientId).Select(b => new
            {
                GroupId = b.ContactId,
                GroupName = b.ContactNames
            }).ToList();
        }

        //For Custom Paging

        public IEnumerable<Contact> GetContactPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.Contacts
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }

        public int CountContact()
        {
            return entities.Contacts.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Contact GetContact(int mContactId)
        {
            return entities.Contacts.Where(m => m.ContactId == mContactId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateContact(Contact mContact)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.Contacts.AddObject(mContact);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Contact saved";// +mContact.ContactNames + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateContact(Contact mContact)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Contact data = entities.Contacts.Where(m => m.ContactId == mContact.ContactId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mContact;
                        mContact = (Contact)entities.Contacts.ApplyCurrentValues(mContact);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Contact updated";// +mContact.ContactNames + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Contact " + mContact.ContactNames + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteContact(int mContactID)
        {
            try
            {
                Contact data = entities.Contacts.Where(m => m.ContactId == mContactID && m.ClientId == ClientId).FirstOrDefault();
                entities.Contacts.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }

    public class ModelContactType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<ContactType> GetContactType()
        {
            return entities.ContactTypes.Where(v => v.ClientId == ClientId).ToList();
        }

        public IEnumerable<object> GetContactTypeDetails()
        {
            ResponseModel _rm = new ResponseModel();
            return entities.ContactTypes.Where(b => b.ClientId == ClientId).Select(b => new
            {
                GroupId = b.ContactTypeId,
                GroupName = b.ContactTypeName
            }).ToList();
        }

        //For Custom Paging

        public IEnumerable<ContactType> GetContactTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.ContactTypes
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountContactType()
        {
            return entities.ContactTypes.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public ContactType GetContactType(int mContactTypeId)
        {
            return entities.ContactTypes.Where(m => m.ContactTypeId == mContactTypeId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateContactType(ContactType mContactType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.ContactTypes.AddObject(mContactType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Contact Type saved";// +mContactType.ContactTypeName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateContactType(ContactType mContactType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    ContactType data = entities.ContactTypes.Where(m => m.ContactTypeId == mContactType.ContactTypeId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mContactType;

                        mContactType = (ContactType)entities.ContactTypes.ApplyCurrentValues(mContactType);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Contact Type updated";// +mContactType.ContactTypeName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Contact Type" + mContactType.ContactTypeName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteContactType(int mContactTypeID)
        {
            try
            {
                ContactType data = entities.ContactTypes.Where(m => m.ContactTypeId == mContactTypeID && m.ClientId == ClientId).FirstOrDefault();
                entities.ContactTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}