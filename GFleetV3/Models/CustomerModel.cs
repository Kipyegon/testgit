﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Configuration;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Models
{
    public class PagedCustomerModel
    {
        public IEnumerable<Person> Customer { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedCustomerTypeModel
    {
        public IEnumerable<CustomerType> CustomerType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class Customerr
    {
        public Int32 PersonId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public string Email1 { get; set; }

        public String Telephone1 { get; set; }

        public string Email2 { get; set; }

        public String Telephone2 { get; set; }

        public Boolean IsMale { get; set; }

        public String PostCode { get; set; }

        public String FaxNumber { get; set; }
        public String DOB { get; set; }
        public String CreditLimit { get; set; }

        public String NextOfKinName1 { get; set; }

        public String NextOfKinName2 { get; set; }

        public String NextOfKinPhone1 { get; set; }

        public String NextOfKinPhone2 { get; set; }

        public String Association1 { get; set; }

        public String Association2 { get; set; }

        public String NextOfKinEmail1 { get; set; }

        public String NextOfKinEmail2 { get; set; }

        public CustomerTypeId CustomerTypeId { get; set; }

        public OrganisationId OrganisationId { get; set; }
    }


    public class MyViewModel
    {
        public HttpPostedFileBase MyFile { get; set; }
    }

    public class ModelCustomer : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Customer> GetCustomer()
        {
            return entities.People.OfType<Customer>().Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<Customer> GetCustomerPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.People.OfType<Customer>()
              .OrderBy(orderCriteria).OrderBy(v => v.FirstName)
              .Where(v => v.ClientId == ClientId && v.IsDelete == false)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountCustomer()
        {
            return entities.People.OfType<Customer>().Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Customer GetCustomer(int mCustomerId)
        {
            return entities.People.OfType<Customer>().Where(m => m.PersonId == mCustomerId && m.ClientId == ClientId).FirstOrDefault();
        }
        public SystemUser GetSystemUser(Guid mSystemUserId)
        {
            return entities.People.OfType<SystemUser>().Where(m => m.UserId == mSystemUserId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateCustomer(Customer mCustomer, HttpPostedFileBase File)
        {
            ResponseModel _rm = new ResponseModel();

            string Filename = ""; string filepath = "";
            try
            {
                if (File != null)
                {
                    try
                    {
                        string CustomerDir = WebConfigurationManager.AppSettings["CustomerDir"];
                        filepath = System.Web.HttpContext.Current.Server.MapPath(CustomerDir);
                        Filename = "Client_" + mCustomer.Telephone1 + "_map" + Path.GetExtension(File.FileName);

                        mCustomer.CustomerDirUrl = Filename;

                        File.SaveAs(Path.Combine(filepath, Filename));
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        mCustomer.CustomerDirUrl = null;
                    }
                }
                entities.People.AddObject(mCustomer);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);

                _rm.Status = true;
                _rm.Message = "Customer saved";// +mCustomer.FirstName + " " + mCustomer.LastName + " successfully saved!";
                _rm.Data = new
                {
                    FullNames = mCustomer.FullNames,
                    Email1 = mCustomer.Email1,
                    Telephone1 = mCustomer.Telephone1,
                    PostCode = mCustomer.PostCode,
                    Organisation = mCustomer.Organisation.OrganisationName,
                    IsMale = mCustomer.IsMale,
                    FaxNumber = mCustomer.FaxNumber,
                    CreditLimit = mCustomer.CreditLimit,
                    PersonId = mCustomer.PersonId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                if (File != null)
                {
                    try
                    {
                        System.IO.File.Delete(filepath + Filename);
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);

                    }
                }
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateCustomerAccount(OrganisationRegisterModel mOrganisationReg, HttpPostedFileBase File)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            
            SystemUser mSystemUser = mOrganisationReg.OrganisationAdminStaffModel;
            Customer mCustEndUser = mOrganisationReg.OrganisationEndUserModel;
            RegisterEndUserPhoneModel mRegister = mOrganisationReg.RegisterEndUserModel;
            try
            {
                pmp = Membership.Provider;
                if (pmp.GetUser(mRegister.Username, false) == null)
                {
                    string username = mRegister.Username.TrimStart('+');
                    string password = Service.Utility.GeneratePass().ToString();

                    MembershipCreateStatus createStatus = new MembershipCreateStatus();
                    MembershipUser pms = pmp.CreateUser(username, password, mCustEndUser.Email1, "Question",
                                        "Answer", true, null, out createStatus);
                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        Roles.AddUserToRole(username, "End User");

                        if (mCustEndUser == null)
                        {
                            mCustEndUser = new Customer()
                            {
                                ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                DateModified = DateTime.Now,
                                Organisation = null,
                                UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                            };
                        }
                        else
                        {
                            mCustEndUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                            mCustEndUser.DateModified = DateTime.Now;
                            mCustEndUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                        }

                    }
                    else
                    {
                        _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;

                        return _rm;
                    }
                    _rm = UpdateCustomer(mCustEndUser, File);
                    if (_rm.Status)
                    {
                        string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();
                        sendEnduserCreateSMSEmail(mCustEndUser.Email1, mRegister.Username, password, emailBody, "EndUserRegistration");
                    }
                    else 
                    {
                        if (pmp != null)
                        {
                            pmp.DeleteUser(mRegister.Username, true);
                        }
                    }
                    return _rm;
                }
                else
                {
                    _rm.Message = "user already exists"; _rm.Status = false;
                    return _rm;
                }

            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    pmp.DeleteUser(mRegister.Username, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public static void sendEnduserCreateSMSEmail(string email, string username, string password, string emailBody, string emailtemplate)
        {
            //string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

            emailBody = emailBody.Replace("[newline]", "</br>");

            string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

            string newportsolnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();
            string solnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();

            if (url.Contains(AccessUrls.ToString()))
            {
                emailBody = String.Format(emailBody, username.TrimStart('+'), password, newportsolnaddress);
            }
            else
            {
                emailBody = String.Format(emailBody, username.TrimStart('+'), password, solnaddress);
            }
            string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();
            string smsBody = "Welcome to Taksi, Your Username is {0} and Password is {1}. Kindly Check your email for further Instructions";
            smsBody = String.Format(smsBody, username.TrimStart('+'), password);


            //EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
            EmailTemplate ET = new GFleetModelContainer().EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals(emailtemplate));
            if (ET != null)
            {
                if (url.Contains(AccessUrls.ToString()))
                {
                    emailBody = String.Format(ET.Body, username.TrimStart('+'), password, newportsolnaddress);
                }
                else
                {
                    emailBody = String.Format(ET.Body, username.TrimStart('+'), password, solnaddress);
                }
                emailBody = ET.Header + emailBody + ET.Footer;
            }
            //Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
            new Thread(() =>
            {
                try
                {
                    GFleetV3SMSProcessor.SendSMS.SendMessage(username, smsBody, "");
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    //throw new Exception(ex.Message.ToString());
                }
            }).Start();
            new Thread(() =>
            {
                try
                {
                    Service.SendEmail.SendUserMngtEmail(email, emailSubject, emailBody);
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    //throw new Exception(ex.Message.ToString());
                }
            }).Start();
        }

        public ResponseModel UpdateCustomer(Customer mCustEndUser, HttpPostedFileBase File)
        {
            string Filename = ""; string filepath = "";
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Person data = entities.People.Where(m => m.PersonId == mCustEndUser.PersonId && m.ClientId == ClientId).FirstOrDefault();

                    if (File != null)
                    {
                        try
                        {
                            string CustomerDir = WebConfigurationManager.AppSettings["CustomerDir"];
                            filepath = System.Web.HttpContext.Current.Server.MapPath(CustomerDir);
                            Filename = "Client_" + mCustEndUser.Telephone1 + "_map" + Path.GetExtension(File.FileName);

                            mCustEndUser.CustomerDirUrl = Filename;

                            File.SaveAs(Path.Combine(filepath, Filename));
                        }
                        catch (Exception mex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                            mCustEndUser.CustomerDirUrl = null;
                        }
                    }
                    else
                    {
                        mCustEndUser.CustomerDirUrl = data.CustomerDirUrl;
                    }


                    data = mCustEndUser;


                    mCustEndUser = (Customer)entities.People.ApplyCurrentValues(mCustEndUser);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Account Of Customer updated";// +mCustEndUser.FirstName + " " + mCustEndUser.LastName + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                if (File != null)
                {
                    try
                    {
                        System.IO.File.Delete(filepath + Filename);
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                    }
                }
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel UpdateSystemUser(SystemUser mSystemUser, string[] selectedRoles)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Person data = entities.People.Where(m => m.PersonId == mSystemUser.PersonId && m.ClientId == ClientId).FirstOrDefault();


                    data = mSystemUser;

                    mSystemUser = (SystemUser)entities.People.ApplyCurrentValues(mSystemUser);
                    entities.SaveChanges();

                    if (selectedRoles != null)
                    {
                        MembershipUser mu = Membership.GetUser(data.UserId);

                        if (mu != null)
                        {

                            string[] existingRoles = Roles.GetRolesForUser(mu.UserName).Where(v => v == "Booking" || v == "Dispatch").ToArray<string>();

                            foreach (string role in selectedRoles)
                            {
                                if (!existingRoles.Contains(role))
                                {
                                    Roles.AddUserToRole(mu.UserName, role);
                                }

                            }

                            foreach (string role in existingRoles)
                            {
                                if (!selectedRoles.Contains(role))
                                {
                                    Roles.RemoveUserFromRole(mu.UserName, role);
                                }
                            }
                        }

                    }
                    _rm.Status = true; _rm.Message = "System " + mSystemUser.FirstName + " " + mSystemUser.LastName + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel DeleteCustomer(int mCustomerID)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Customer data = entities.People.OfType<Customer>().Where(m => m.PersonId == mCustomerID && m.ClientId == ClientId).FirstOrDefault();
                    MembershipProvider pmp = Membership.Provider;
                    if (data != null)
                    {
                        data.DeletedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        data.DeletedDate = DateTime.Parse(DateTime.Now.ToString());
                        data.IsDelete = true;
                        //data = (Person)
                        entities.People.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        data.Telephone1.Replace("+", "");
                        pmp.DeleteUser(data.Telephone1, true);
                        _rm.Status = true; _rm.Message = "Customer deleted";// +data.FirstName + " " + data.LastName + " successfully deleted!";
                        return _rm;
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Customer Not deleted!";
                        return _rm;
                    }
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }



        public class Uploader
        {
            public HttpPostedFileBase MyFile { get; set; }

            public static bool uploadsaved(string filename, int clientid)
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    try
                    {
                        Customer client = entities.People.OfType<Customer>().SingleOrDefault(u => u.ClientId == clientid);
                        Customer updated = client;
                        updated.CustomerDirUrl = filename;
                        client = (Customer)entities.People.ApplyCurrentValues(updated);
                        entities.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }

                }
            }
        }

    }
    public class ModelCustomerType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<CustomerType> GetCustomerType()
        {
            return entities.CustomerTypes.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<CustomerType> GetCustomerTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.CustomerTypes
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountCustomerType()
        {
            return entities.CustomerTypes.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public CustomerType GetCustomerType(int mCustomerTypeId)
        {
            return entities.CustomerTypes.Where(m => m.CustomerTypeId == mCustomerTypeId && m.ClientId == ClientId).FirstOrDefault();
        }


        public ResponseModel CreateCustomerType(CustomerType mCustomerType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.CustomerTypes.AddObject(mCustomerType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "Customer Type saved";// +mCustomerType.CustomerTypeName + " successfully saved!";
                _rm.Data = new
                {
                    CustomerTypeName = mCustomerType.CustomerTypeName,
                    Description = mCustomerType.Description,
                    Id = mCustomerType.CustomerTypeId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateCustomerType(CustomerType mCustomerType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    CustomerType data = entities.CustomerTypes.Where(m => m.CustomerTypeId == mCustomerType.CustomerTypeId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mCustomerType;

                        mCustomerType = (CustomerType)entities.CustomerTypes.ApplyCurrentValues(mCustomerType);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Customer Type updated";// +mCustomerType.CustomerTypeName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Customer Type " + mCustomerType.CustomerTypeName + " is not in you company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteCustomerType(int mCustomerTypeID)
        {
            try
            {
                CustomerType data = entities.CustomerTypes.Where(m => m.CustomerTypeId == mCustomerTypeID && m.ClientId == ClientId).FirstOrDefault();
                entities.CustomerTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}