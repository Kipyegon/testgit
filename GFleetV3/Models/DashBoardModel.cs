﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
//using System.Globalization.CultureInfo;

namespace GFleetV3.Models
{
    public class Disconnected
    {
        public int TotalCount { get; set; }
        public int DisconnectedCount { get; set; }
        public List<object> DisconnectedData { get; set; }
    }
    public class DashBoardModel
    {
        public static List<DispatchOrgPerformance_Result> getDispatchOrgPerf(DateTime myStartTime, DateTime myEndTime, int ClientId, string organisationId)
        {
            try
            {
                using (GFleetModelContainer gfleet = new GFleetModelContainer())
                {
                    return gfleet.DispatchOrgPerformance(ClientId, myStartTime, myEndTime).ToList<DispatchOrgPerformance_Result>();
                }
            }
            catch (Exception ex) {
                return null;
            }
        }
        public static Disconnected getDisconnectedData()
        {
            using (GFleetModelContainer gfleet = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;

                string diconDate = DateTime.Now.Date.ToString("dd/MM/yy");

                List<getWebTrackData_V3_Result> data = gfleet.getWebTrackData_V3(clientId).ToList<getWebTrackData_V3_Result>();

                List<object> DisconnectedData = new List<object>();

                DisconnectedData.AddRange(data.Where(v => v.connected == 0
                    && v.Date != null && v.Date.CompareTo(diconDate) < 0).OrderBy(v => v.timestamp)
                .Select(v => new
                {
                    Reg = v.Reg + " (" + v.VehicleCode + ")",
                    Date = string.IsNullOrEmpty(v.timestamp) ? "No Device" : v.timestamp,
                    Place = v.Street + " - " + v.Town
                }));

                return new Disconnected()
                {
                    TotalCount = data.Count,
                    DisconnectedCount = DisconnectedData.Count(),
                    DisconnectedData = DisconnectedData
                };
            }
        }
    }
}