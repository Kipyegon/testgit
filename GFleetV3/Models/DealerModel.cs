﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Web.Security;

namespace GFleetV3.Models
{
    public class DealerModel
    {
        public static List<Client> GetDealers()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<Client> mc = new List<Client>();

                mc = db.Clients
                    .Where(v => v.IsLeaseDealer == true)
                    .ToList();
                return mc;
            }
        }
        public ResponseModel CreateDealerAdminUser(DealerRegisterModel mDealerReg)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                ResponseModel _rm = new ResponseModel();
                MembershipProvider pmp = null;
                SystemUser mSystemUser = mDealerReg.DealerAdminStaffModel;
                Client mDealer = mDealerReg.DealerModel;
                RegisterClientModel mRegister = mDealerReg.RegisterModel;
                try
                {

                    if (!entities.Clients.Any(v => v.ClientName.Equals(mDealer.ClientName) && v.ClientEmail.Equals(mDealer.ClientEmail)))
                    {

                        if (!string.IsNullOrEmpty(mRegister.UserRole.RoleName))
                        {

                            pmp = Membership.Provider;
                            if (pmp.GetUser(mRegister.Email, false) == null)
                            {
                                MembershipCreateStatus createStatus = new MembershipCreateStatus();
                                MembershipUser pms = pmp.CreateUser(mRegister.Email, mRegister.Password, mRegister.Email, "Question",
                                                    "Answer", true, null, out createStatus);
                                if (createStatus == MembershipCreateStatus.Success)
                                {
                                    Roles.AddUserToRole(mRegister.Email, mRegister.UserRole.RoleName);
                                    if (mSystemUser == null)
                                    {
                                        mSystemUser = new SystemUser()
                                        {
                                            ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                            DateModified = DateTime.Now,
                                            Client = null,
                                            UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                                        };
                                    }
                                    else
                                    {
                                        mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                        mSystemUser.DateModified = DateTime.Now;
                                        mSystemUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                                    }
                                    string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBody"].ToString();

                                    emailBody = emailBody.Replace("[newline]", "</br>");

                                    string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
                                    string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                                    string newportsolnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();
                                    string solnaddress = System.Configuration.ConfigurationManager.AppSettings["solnaddress"].ToString();

                                    if (url.Contains(AccessUrls.ToString()))
                                    {
                                        emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, newportsolnaddress);
                                    }
                                    else
                                    {
                                        emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, solnaddress);
                                    }
                                    //string emailSubject = "Sharp Account registration.";
                                    string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubject"].ToString();

                                    Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                                }
                                else
                                {
                                    _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                                    return _rm;
                                }

                                mDealer.DateModified = DateTime.Now;
                                mDealer.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                mDealer.IsLeaseDealer = true;

                                mSystemUser.Client = mDealer;
                                entities.People.AddObject(mSystemUser);
                                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                                _rm.Status = true; _rm.Message = "Dealer " + mDealer.ClientName + " successfully saved!";
                                return _rm;
                            }
                            else
                            {
                                _rm.Message = "user already exists"; _rm.Status = false;
                                return _rm;
                            }
                        }
                        else
                        {
                            _rm.Message = "No Role name specified"; _rm.Status = false;
                            return _rm;
                        }
                    }
                    else
                    {
                        _rm.Message = "The client already exists"; _rm.Status = false;
                        return _rm;
                    }
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    if (pmp != null)
                    {
                        pmp.DeleteUser(mRegister.Email, true);
                    }

                    _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                    return _rm;
                }
            }
        }
    }
    public class DealerStaffRegisterModel
    {
        public RegisterStaffModel RegisterStaffModel { get; set; }
        public SystemUser ClientStaffModel { get; set; }
        public List<string> isInDispatchBooking { get; set; }
    }
    public class DealerRegisterModel
    {
        public RegisterClientModel RegisterModel { get; set; }
        public Client DealerModel { get; set; }
        public SystemUser DealerAdminStaffModel { get; set; }

        public static DealerRegisterModel getDealerRegisterModel(int clientId)
        {
            DealerRegisterModel crm = new DealerRegisterModel();
            crm.DealerModel = new ModelClient().GetClient(clientId);

            return crm;
        }
    }
}