﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class PagedDeviceModel
    {
        public IEnumerable<Device> Device { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }


    public class ModelDevice : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Device> GetDevice()
        {
            return entities.Devices.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<Device> GetDevicePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.Devices
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountDevice()
        {
            return entities.Devices.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }
        //For Edit 
        public Device GetDevice(int mDeviceId)
        {
            return entities.Devices.Where(m => m.DeviceId == mDeviceId && m.ClientId == ClientId).FirstOrDefault();
        }
        public VehicleDevice GetVehicleDevice(int mDeviceId)
        {
            return entities.VehicleDevices.Where(m => m.DeviceId == mDeviceId && m.Device.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateDevice(Device mDevice)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                mDevice.DeviceTypeId = mDevice.DeviceTypeId;
                mDevice.DeviceType = null;
                entities.Devices.AddObject(mDevice);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Device saved";//with SerialNo" + mDevice.SerialNo + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateDevice(Device mDevice)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Device data = entities.Devices.Where(m => m.DeviceId == mDevice.DeviceId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mDevice;

                        mDevice = (Device)entities.Devices.ApplyCurrentValues(mDevice);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Device updated";//with SerialNo" + mDevice.SerialNo + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Device with SerialNo" + mDevice.SerialNo + " is not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteDevice(int mDeviceID)
        {
            try
            {
                Device data = entities.Devices.Where(m => m.DeviceId == mDeviceID && m.ClientId == ClientId).FirstOrDefault();
                entities.Devices.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}