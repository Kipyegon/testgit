﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GFleetV3.Models
{
    public class DispatchExtras
    {
        public int ExtrasID { get; set; }
        public string Extras { get; set; }
        public bool Assigned { get; set; }
    }
    public class AccessibilityinDispatch
    {
        public int AccessibilityId { get; set; }
        public string Accessibility { get; set; }
        public bool Assigned { get; set; }
    }
    public class DispatchPaymentMode
    {
        public int PaymentModeId { get; set; }
        public string PaymentMode { get; set; }
        public bool Assigned { get; set; }
    }
    public class LicenceTypes
    {
        public int LicenceTypeId { get; set; }
        public string DriverLicenceType { get; set; }
        public bool Assigned { get; set; }
    }
    public class Devices
    {
        public int DeviceId { get; set; }
        public string PhoneNo { get; set; }
        public bool Assigned { get; set; }
    }
    public class Groups
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public bool Assigned { get; set; }
    }

    public class Clients
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public bool Assigned { get; set; }
    }
    public class Vehicles
    {
        public int VehicleId { get; set; }
        public string VehicleRegCode { get; set; }
    }
}