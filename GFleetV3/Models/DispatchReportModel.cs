﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class DispatchReportModel
    {
        public static List<vw_DispatchReport> GetDispatchRpt(DispatchRptParamModel mParams)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                DateTime StartDateTime = mParams.StartDateTime;
                DateTime EndDateTime = mParams.EndDateTime;
                var Report= from p in entities.vw_DispatchReport.Where(v =>v.ClientId == clientId
                                && v.DateModified >= StartDateTime && v.DateModified <= EndDateTime).OrderByDescending(m => m.DateModified)
                                select p;

                if (mParams.Dispactched == "true")
                {
                    Report = Report.Where(v => v.DispatchedBy != null);
                }
                 else if (mParams.Dispactched == null)
                {
                   
                }
                else
                {
                    Report = Report.Where(v => v.DispatchedBy == null);
                }
                if (mParams.OrgId>0){
                    Report = Report.Where(v => v.OrganisationId == mParams.OrgId);
                }
                if (mParams.CustId>0){
                    Report = Report.Where(v => v.CustomerId == mParams.CustId);
                }
                if (mParams.VehId>0){
                    Report = Report.Where(v => v.VehicleId == mParams.VehId);
                }
                if (mParams.SmsStatId > 0)
                {
                    Report = Report.Where(v => v.StatusId == mParams.SmsStatId);
                }
                if (mParams.POBId > 0)
                {
                    Report = Report.Where(v => v.PassengerOnBoardId == mParams.POBId);
                }
                if (mParams.CBId > 0)
                {
                    Report = Report.Where(v => v.CallBackId == mParams.CBId);
                }
                if (!mParams.DrivId.Equals("0"))
                {
                    Report = Report.Where(v => v.DriverUserId == mParams.DrivId);
                }

                return Report.ToList<vw_DispatchReport>();
            }
        }
        public static List<vw_DispatchReport> GetDispatchRptDownload(DateTime StartDate, DateTime EndDate, string OrganisationId,
            string CustomerId, string VehicleId, bool IsNotDispactchedChk, bool IsDispactchedChk, string SmsStatusId, string PassengerOnBoardId, string CallBackId)
        {
            int OrgId = string.IsNullOrEmpty(OrganisationId) ? 0 : int.Parse(OrganisationId);
            int CustId = string.IsNullOrEmpty(CustomerId) ? 0 : int.Parse(CustomerId);
            int VehId = string.IsNullOrEmpty(VehicleId) ? 0 : int.Parse(VehicleId);
            int SmsStatId = string.IsNullOrEmpty(SmsStatusId) ? 0 : int.Parse(SmsStatusId);
            int POBId = string.IsNullOrEmpty(PassengerOnBoardId) ? 0 : int.Parse(PassengerOnBoardId);
            int CBId = string.IsNullOrEmpty(CallBackId) ? 0 : int.Parse(CallBackId);
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                var Report = from p in entities.vw_DispatchReport.Where(v => v.ClientId == clientId
                                 && v.DateModified >= StartDate && v.DateModified <= EndDate).OrderByDescending(m=>m.DateModified)
                             select p;

                if (IsDispactchedChk && IsNotDispactchedChk)
                {

                }
                else if (!IsDispactchedChk && !IsNotDispactchedChk)
                {

                }
                else if (IsDispactchedChk && !IsNotDispactchedChk)
                {
                    Report = Report.Where(v => v.DispatchedBy != null);
                }
                else
                {
                    Report = Report.Where(v => v.DispatchedBy == null);
                }

                if (OrgId > 0)
                {
                    Report = Report.Where(v => v.OrganisationId == OrgId);
                }
                if (CustId > 0)
                {
                    Report = Report.Where(v => v.CustomerId == CustId);
                }
                if (VehId > 0)
                {
                    Report = Report.Where(v => v.VehicleId == VehId);
                }
                if (SmsStatId > 0)
                {
                    Report = Report.Where(v => v.StatusId == SmsStatId);
                }
                if (POBId > 0)
                {
                    Report = Report.Where(v => v.PassengerOnBoardId == POBId);
                }
                if (CBId > 0)
                {
                    Report = Report.Where(v => v.CallBackId == CBId);
                }

                return Report.ToList<vw_DispatchReport>();
            }
        }
    }
}