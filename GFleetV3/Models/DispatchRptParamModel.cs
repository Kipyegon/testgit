﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GFleetV3.Models
{
    public class DispatchRptParamModel
    {
        public string OrganisationId { get; set; }
        public string DriverUserId { get; set; }
        public string CustomerId { get; set; }
        public string VehicleId { get; set; }
        public string StartDate { get; set; }
        public string PasserngerOnBoardId { get; set; }
        public string SmsStatusId { get; set; }
        public string StartTime { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        public string Dispactched { get; set; }
        public string CallBackId { get; set; }
        public string IsNotDispactchedChk { get; set; }

        public DateTime StartDateTime
        {
            get
            {
                DateTime result;
                string date = StartDate + ' ' + StartTime;
                DateTime.TryParse(date, out result);
                return result;
            }
        }

        public DateTime EndDateTime
        {
            get
            {
                DateTime result;
                string date = EndDate + ' ' + EndTime;
                DateTime.TryParse(date, out result);
                return result;
            }
        }

        public int OrgId { get { return string.IsNullOrEmpty(OrganisationId) ? 0 : int.Parse(OrganisationId); } }

        public int POBId { get { return string.IsNullOrEmpty(PasserngerOnBoardId) ? 0 : int.Parse(PasserngerOnBoardId); } }

        public int CBId { get { return string.IsNullOrEmpty(CallBackId) ? 0 : int.Parse(CallBackId); } }

        public int CustId { get { return string.IsNullOrEmpty(CustomerId) ? 0 : int.Parse(CustomerId); } }

        //public string DrivId { get { return string.IsNullOrEmpty(DriverUserId) ? "0" : DriverUserId.ToString(); } }

        public string DrivId { get { return string.IsNullOrEmpty(DriverUserId) ? "0" : DriverUserId.ToString(); } }

        public int VehId { get { return string.IsNullOrEmpty(VehicleId) ? 0 : int.Parse(VehicleId); } }

        public int SmsStatId { get { return string.IsNullOrEmpty(SmsStatusId) ? 0 : int.Parse(SmsStatusId); } }
    }
}