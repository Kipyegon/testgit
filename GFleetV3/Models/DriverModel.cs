﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Web.Security;
using System.Web.Configuration;
using System.IO;
using System.Threading;
using System.Web.Profile;
//using System.Web.Profile.ProfileBase;

namespace GFleetV3.Models
{
    public class PagedDriverModel
    {
        public IEnumerable<getUserProfile_Result> Driver { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }

        

        public DriverRegisterModel DriverRegModel { get; set; }
    }
    public class PagedDriverLicenceTypeModel
    {
        public IEnumerable<LicenceType> LicenceType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedDriverStatusModel
    {
        public IEnumerable<DriverStatu> DriverStatus { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedDriverVehicleAssignmentModel
    {
        public IEnumerable<DriverVehicleAssignment> DriverVehicleAssignment { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedDriverEndorsmentModel
    {
        public IEnumerable<DriverEndosment> DriverEndorsment { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }


    public class ModelDriver : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        //private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Driver> GetDriver()
        {
            int ClientId = Service.Utility.getClientId().Value;
            MembershipUserCollection users = Membership.GetAllUsers();
            string email = users["Telephone1"].Email;
            return entities.People.OfType<Driver>().Where(v => v.ClientId == ClientId && v.IsDelete == false).ToList();
        }

        public IEnumerable<getUserProfile_Result> GetDriverPage(int pageNumber, int pageSize, string orderCriteria)
        {
            int ClientId = Service.Utility.getClientId().Value;
            //if (pageNumber < 1)
            //    pageNumber = 1;

            return AccountProfile.GetDriver(ClientId);

            //   entities.getUserProfile()
            //.Where(v => v.ClientId == ClientId.ToString())
            //.Skip((pageNumber - 1) * pageSize)
            //.Take(pageSize)
            //.ToList();
            //return entities.People.OfType<Driver>()
            //  .OrderBy(orderCriteria)
            //  .Where(v => v.ClientId == ClientId && v.IsDelete == false)
            //  .Skip((pageNumber - 1) * pageSize)
            //  .Take(pageSize)
            //  .ToList();
        }

        public int CountDriver()
        {
            int ClientId = Service.Utility.getClientId().Value;
            return entities.People.OfType<Driver>().Where(v => v.ClientId == ClientId).Count();
        }

       

        public void Dispose()
        {
            entities.Dispose();
        }

        public getUserProfile_Result GetDriver(Guid UserId)
        {
            int ClientId = Service.Utility.getClientId().Value;
            return AccountProfile.GetDriver(ClientId)
                .Where(v => v.UserId == UserId).FirstOrDefault();
            //entities.getUserProfile().Where(v => v.ClientId == ClientId.ToString() && v.UserId==UserId).FirstOrDefault();
            //return entities.People.OfType<Driver>().Where(m => m.PersonId == mDriverUserId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateDriver(Driver mDriver)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                entities.People.AddObject(mDriver);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "Driver saved"; // +mDriver.FirstName + " " + mDriver.LastName + " successfully saved!";
                _rm.Data = mDriver;
                _rm.Data = new
                {
                    FullNames = mDriver.FullNames,
                    Telephones = mDriver.Telephones,
                    IDNumber = mDriver.IDNumber,
                    COfCNo = mDriver.CofCNo,
                    //DriverStatusName = mDriver.DriverStatu.DriverStatusName,
                    LicenceDate = mDriver.LicenceDate,
                    Id = mDriver.PersonId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel CreateDriverUser(DriverRegisterModel mDriver, bool SendEmail, bool SendSms)
        {
            int ClientId = Service.Utility.getClientId().Value;

            return AccountProfile.CreateDriverUser(mDriver, ClientId, SendEmail, SendSms);
        }
        public ResponseModel CreateDriverUserThread()
        {
            int ClientId = Service.Utility.getClientId().Value;
            return AccountProfile.CreateDriverUserThread();
        }

        //public static IEnumerable<Driver> GetUserInfos()
        //{
        //MembershipUserCollection users = Membership.GetAllUsers();
        //string email = users["some_username"].Email;
        //var members = Membership.GetAllUsers();

        //    var profiles = ProfileManager.GetAllProfiles(ProfileAuthenticationOption.All);

        //    foreach (MembershipUser member in members)
        //    {
        //        var profile = profiles[member.UserName];

        //        ProfileCommon driverProfile = System.Web.Profile.GetProfile(profile.UserName);

        //        if (driverProfile != null)
        //        {
        //            yield return new Driver
        //            {
        //                UserId = (Guid)member.ProviderUserKey,

        //                FirstName = driverProfile.FirstName,

        //                LastName = driverProfile.LastName
        //            };
        //        }
        //    }
        //}

        //public ResponseModel UpdateDriver(Driver mDriver)
        //{
        //    ResponseModel _rm = new ResponseModel() { IsUpdate = true };
        //    try
        //    {
        //        using (GFleetModelContainer entities = new GFleetModelContainer())
        //        {
        //            int ClientId = Service.Utility.getClientId().Value;
        //            Person data = entities.People.Where(m => m.PersonId == mDriver.PersonId && m.ClientId == ClientId).FirstOrDefault();

        //            if (data != null)
        //            {
        //                data = (Driver)entities.People.ApplyCurrentValues(mDriver);

        //                HashSet<int> dltLicenseType = new HashSet<int>(mDriver.DriverLicenceTypes.Select(c => c.LicenceTypeId));

        //                entities.DriverLicenceTypes.Where(w => w.PersonId == mDriver.PersonId && !dltLicenseType.Contains(w.LicenceTypeId))
        //                                    .ToList()
        //                                    .ForEach(entities.DriverLicenceTypes.DeleteObject);

        //                foreach (DriverLicenceType dlt in mDriver.DriverLicenceTypes)
        //                {
        //                    if (!entities.DriverLicenceTypes.Any(v => v.PersonId == dlt.PersonId && v.LicenceTypeId == dlt.LicenceTypeId))
        //                    {
        //                        entities.DriverLicenceTypes.AddObject(new DriverLicenceType()
        //                        {
        //                            ClientId = dlt.ClientId,
        //                            DateModified = dlt.DateModified,
        //                            LicenceTypeId = dlt.LicenceTypeId,
        //                            ModifiedBy = dlt.ModifiedBy,
        //                            PersonId = dlt.PersonId
        //                        });
        //                    }
        //                }

        //                entities.SaveChanges();
        //                _rm.Status = true;
        //                _rm.Message = "Driver updated";// +mDriver.FirstName + " " + mDriver.LastName + " successfully updated!"; //_rm.Data = mDriver;
        //            }
        //            else
        //            {
        //                _rm.Message = "Driver " + mDriver.FirstName + " " + mDriver.LastName + " not in your company!"; //_rm.Data = mDriver;
        //            }
        //            return _rm;
        //        }
        //    }
        //    catch (Exception mex)
        //    {
        //        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
        //        _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
        //        return _rm;
        //    }
        //}

        public ResponseModel UpdateDriver(DriverRegisterModel mDriver)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    int ClientId = Service.Utility.getClientId().Value;
                    getUserProfile_Result data = entities.getUserProfile(ClientId, AccountProfile.Driverdiscriminator).Where(m => m.UserId == mDriver.DriverModel.UserId && m.ClientId == ClientId.ToString()).FirstOrDefault();

                    if (data != null)
                    {
                        //data = (Driver)entities.People.ApplyCurrentValues(mDriver);

                        //HashSet<int> dltLicenseType = new HashSet<int>(mDriver.DriverLicenceTypes.Select(c => c.LicenceTypeId));

                        //entities.DriverLicenceTypes.Where(w => w.PersonId == mDriver.PersonId && !dltLicenseType.Contains(w.LicenceTypeId))
                        //                    .ToList()
                        //                    .ForEach(entities.DriverLicenceTypes.DeleteObject);

                        //foreach (DriverLicenceType dlt in mDriver.DriverLicenceTypes)
                        //{
                        //    if (!entities.DriverLicenceTypes.Any(v => v.PersonId == dlt.PersonId && v.LicenceTypeId == dlt.LicenceTypeId))
                        //    {
                        //        entities.DriverLicenceTypes.AddObject(new DriverLicenceType()
                        //        {
                        //            ClientId = dlt.ClientId,
                        //            DateModified = dlt.DateModified,
                        //            LicenceTypeId = dlt.LicenceTypeId,
                        //            ModifiedBy = dlt.ModifiedBy,
                        //            PersonId = dlt.PersonId
                        //        });
                        //    }
                        //}

                        MembershipUser user = Membership.GetUser();
                        MembershipUser mu = Membership.GetUser(mDriver.DriverModel.UserId);
                        string username = mu.UserName;
                       
                        AccountProfile profile = AccountProfile.Create(username, true) as AccountProfile;

                       // profile.ClientId = ClientId;
                        profile.FullName = mDriver.DriverModel.FullNames;
                        profile.FirstName = mDriver.DriverModel.FirstName;
                        profile.LastName = mDriver.DriverModel.LastName;
                        profile.Email1 = mDriver.DriverModel.Email1;
                        profile.Telephone1 = mDriver.DriverModel.Telephone1;
                        profile.Email2 = mDriver.DriverModel.Email2;
                        profile.DriverStatusId = mDriver.DriverModel.DriverStatusId;
                        profile.Telephone2 = mDriver.DriverModel.Telephone2;
                        profile.IsMale = mDriver.DriverModel.IsMale;
                        profile.PostCode = mDriver.DriverModel.PostCode;
                        profile.FaxNumber = mDriver.DriverModel.FaxNumber;
                        profile.DriverNo = mDriver.DriverModel.DriverNo;
                        profile.LicenceDate = mDriver.DriverModel.LicenceDate;
                        profile.DriverRFID = mDriver.DriverModel.DriverRFID;
                        profile.DOB = mDriver.DriverModel.DOB;
                        profile.IDNumber = mDriver.DriverModel.IDNumber;
                        profile.NextOfKinName1 = mDriver.DriverModel.NextOfKinName1;
                        profile.NextOfKinName2 = mDriver.DriverModel.NextOfKinName2;
                        profile.NextOfKinPhone1 = mDriver.DriverModel.NextOfKinPhone1;
                        profile.NextOfKinPhone2 = mDriver.DriverModel.NextOfKinPhone2;
                        profile.Association1 = mDriver.DriverModel.Association1;
                        profile.Association2 = mDriver.DriverModel.Association2;
                        profile.NextOfKinEmail1 = mDriver.DriverModel.NextOfKinEmail1;
                        profile.NextOfKinEmail2 = mDriver.DriverModel.NextOfKinEmail2;
                        //profile.Active = mDriver.DriverModel.Active;
                        profile.DiscriminatorId = 3;
                        profile.Save();
                        _rm.Status = true;
                        _rm.Message = "Driver updated";// +mDriver.FirstName + " " + mDriver.LastName + " successfully updated!"; //_rm.Data = mDriver;
                    }
                    else
                    {
                        _rm.Message = "Driver " + mDriver.DriverModel.FirstName + " " + mDriver.DriverModel.LastName + " not in your company!"; //_rm.Data = mDriver;
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
            
    }

    public class DriverRegisterModel
    {
        public RegisterEndUserPhoneModel RegisterDriverUserModel { get; set; }
        public UserValidation DriverModel { get; set; }
        

        public static DriverRegisterModel getDriver(String UserId)
        {
            DriverRegisterModel crm = new DriverRegisterModel();
            crm.DriverModel = AccountProfile.getDriver(UserId);

            return crm;
        }
        public static OrganisationRegisterModel getEndUser(int CustomerId)
        {
            OrganisationRegisterModel crm = new OrganisationRegisterModel();
            crm.OrganisationEndUserModel = new ModelCustomer().GetCustomer(CustomerId);

            return crm;
        }
    }

    public class ModelDriverLicenceType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public IEnumerable<LicenceType> GetDriverLicenceType()
        {
            return entities.LicenceTypes.ToList();
        }

        //For Custom Paging

        public IEnumerable<LicenceType> GetDriverLicenceTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.LicenceTypes
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountDriverLicenceType()
        {
            return entities.LicenceTypes.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public LicenceType GetDriverLicenceType(int mLicenceTypeId)
        {
            return entities.LicenceTypes.Where(m => m.LicenceTypeId == mLicenceTypeId).FirstOrDefault();
        }

        public ResponseModel CreateLicenceType(LicenceType mLicenceType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.LicenceTypes.AddObject(mLicenceType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "Driver LicenceType saved";// +mLicenceType.DriverLicenceType + " successfully saved!";
                _rm.Data = new
                {
                    DriverLicenceType = mLicenceType.DriverLicenceType,
                    LicenceTypeId = mLicenceType.LicenceTypeId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateLicenceType(LicenceType mLicenceType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    LicenceType data = entities.LicenceTypes.Where(m => m.LicenceTypeId == mLicenceType.LicenceTypeId).FirstOrDefault();


                    data = mLicenceType;

                    mLicenceType = (LicenceType)entities.LicenceTypes.ApplyCurrentValues(mLicenceType);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Driver LicenceType updated";// +mLicenceType.DriverLicenceType + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteDriverLicenceType(int mLicenceTypeID)
        {
            try
            {
                LicenceType data = entities.LicenceTypes.Where(m => m.LicenceTypeId == mLicenceTypeID).FirstOrDefault();
                entities.LicenceTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelDriverStatus : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<DriverStatu> GetDriverStatus()
        {
            return entities.DriverStatus.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<DriverStatu> GetDriverStatussPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.DriverStatus
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountDriverStatus()
        {
            return entities.DriverStatus.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public DriverStatu GetDriverStatus(int mDriverStatusId)
        {
            return entities.DriverStatus.Where(m => m.DriverStatusId == mDriverStatusId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateDriverStatus(DriverStatu mDriverStatus)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.DriverStatus.AddObject(mDriverStatus);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "Status saved";// +mDriverStatus.DriverStatusName + " successfully saved!";
                _rm.Data = new
                {
                    DriverStatusName = mDriverStatus.DriverStatusName,
                    Description = mDriverStatus.Description,
                    DriverStatusId = mDriverStatus.DriverStatusId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateDriverStatus(DriverStatu mDriverStatus)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    DriverStatu data = entities.DriverStatus.Where(m => m.DriverStatusId == mDriverStatus.DriverStatusId && m.ClientId == ClientId).FirstOrDefault();
                    if (data != null)
                    {

                        data = mDriverStatus;

                        mDriverStatus = (DriverStatu)entities.DriverStatus.ApplyCurrentValues(mDriverStatus);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Status updated";// +mDriverStatus.DriverStatusName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Status " + mDriverStatus.DriverStatusName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteDriverStatus(int mDriverStatusID)
        {
            try
            {
                DriverStatu data = entities.DriverStatus.Where(m => m.DriverStatusId == mDriverStatusID && m.ClientId == ClientId).FirstOrDefault();
                entities.DriverStatus.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelDriverVehicleAssignment : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<DriverVehicleAssignment> GetDriverVehicleAssignment()
        {
            return entities.DriverVehicleAssignments.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<DriverVehicleAssignment> GetDriverVehicleAssignmentPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            DateTime nowdate = DateTime.Now;

            return entities.DriverVehicleAssignments
                .OrderBy(orderCriteria)
                .OrderByDescending(v => v.StartDate)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
              .Where(v => v.StartDate.Year == nowdate.Year && v.StartDate.Month == nowdate.Month && v.StartDate.Day == nowdate.Day && v.ClientId == ClientId)
              .ToList();
        }
        public int CountDriverVehicleAssignment()
        {
            return entities.DriverVehicleAssignments.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public DriverVehicleAssignment GetDriverVehicleAssignment(int mDriverVehicleAssignmentId)
        {
            return entities.DriverVehicleAssignments.Where(m => m.DriverVehicleAssignmentId == mDriverVehicleAssignmentId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateDriverVehicleAssignment(DriverVehicleAssignment mDriverVehicleAssignment)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                entities.DriverVehicleAssignments.AddObject(mDriverVehicleAssignment);
                entities.SaveChanges();
                _rm.Status = true;
                _rm.Message = "Vehicle assigned";// +mDriverVehicleAssignment.Vehicle.RegistrationNo + " Successfully Assigned!";
                _rm.Data = new
                {
                    RegistrationNo = mDriverVehicleAssignment.Vehicle.RegistrationNo,
                    FirstName = mDriverVehicleAssignment.Person.FirstName,
                    StartDate = mDriverVehicleAssignment.StartDate.ToString("G"),
                    EndDate = mDriverVehicleAssignment.EndDate.ToString("G"),
                    Notes = mDriverVehicleAssignment.Notes,
                    Id = mDriverVehicleAssignment.DriverVehicleAssignmentId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateDriverVehicleAssignment(DriverVehicleAssignment mDriverVehicleAssignment)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    DriverVehicleAssignment data = entities.DriverVehicleAssignments.Where(m =>
                        m.DriverVehicleAssignmentId == mDriverVehicleAssignment.DriverVehicleAssignmentId
                        && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mDriverVehicleAssignment;
                        mDriverVehicleAssignment = (DriverVehicleAssignment)entities.DriverVehicleAssignments.ApplyCurrentValues(mDriverVehicleAssignment);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Vehicle Assignment updated ";// +mDriverVehicleAssignment.Vehicle.RegistrationNo + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Vehicle Assignment of " + mDriverVehicleAssignment.Vehicle.RegistrationNo + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel DriverSignOut(int id)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    entities.driverSignOut(id, ClientId);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Driver Signed Out";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel DriverSignIn(int id)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    entities.driverSignIn(id, ClientId);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Driver Signed In";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteDriverVehicleAssignment(int mDriverVehicleAssignmentID)
        {
            try
            {
                DriverVehicleAssignment data = entities.DriverVehicleAssignments.Where(m =>
                    m.DriverVehicleAssignmentId == mDriverVehicleAssignmentID
                    && m.ClientId == ClientId).FirstOrDefault();
                entities.DriverVehicleAssignments.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelDriverEndorsment : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<DriverEndosment> GetDriverEndorsment()
        {
            return entities.DriverEndosments.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<DriverEndosment> GetDriverEndorsmentPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.DriverEndosments
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountDriverEndorsment()
        {
            return entities.DriverEndosments.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public DriverEndosment GetDriverEndorsment(int mDriverEndorsmentId)
        {
            return entities.DriverEndosments.Where(m => m.Id == mDriverEndorsmentId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateDriverEndosment(DriverEndosment mDriverEndosment)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.DriverEndosments.AddObject(mDriverEndosment);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Driver endorsed";// +mDriverEndosment.PersonId + " Successfully endorsed!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateDriverEndosment(DriverEndosment mDriverEndosment)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    DriverEndosment data = entities.DriverEndosments.Where(m => m.Id == mDriverEndosment.Id && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mDriverEndosment;

                        mDriverEndosment = (DriverEndosment)entities.DriverEndosments.ApplyCurrentValues(mDriverEndosment);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Emdorsment updated";// +mDriverEndosment.Endosment + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Emdorsment " + mDriverEndosment.Endosment + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        //creating a list of roles
        public class RoleUserVM
        {
            public UserRole Role { get; set; }
            public ICollection<Driver> Users { get; set; }
        }
        public bool DeleteDriverEndorsment(int mDriverEndorsmentID)
        {
            try
            {
                DriverEndosment data = entities.DriverEndosments.Where(m => m.Id == mDriverEndorsmentID && m.ClientId == ClientId).FirstOrDefault();
                entities.DriverEndosments.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}