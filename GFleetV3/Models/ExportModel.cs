﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf.events;
using iTextSharp.text.pdf.interfaces;
using iTextSharp.text.pdf.intern;
using iTextSharp.text.pdf.collection;
using iTextSharp.text.xml.xmp;
using Org.BouncyCastle.X509;
using iTextSharp.text.error_messages;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.log;
using Ext.Net;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace GFleetV3.Models
{
    public class ExportModel
    {
    }

    public class ExportResult<Model> : ActionResult
    {
        string _exportType;
        string _fileName;
        //string _viewPath;
        Model _model;
        ControllerContext _context;

        public ExportResult(ControllerContext context, string fileName, Model model, string exportType)
        {
            this._context = context;
            this._fileName = fileName;
            //this._viewPath = viewPath;
            this._model = model;
            this._exportType = exportType;
        }
        //protected string RenderViewToString()
        //{
        //    using (var writer = new StringWriter())
        //    {
        //        var view = new WebFormView(_context, _viewPath);
        //        var vdd = new ViewDataDictionary<Model>(_model);
        //        var viewCxt = new ViewContext(_context, view, vdd, new TempDataDictionary(), writer);
        //        viewCxt.View.Render(viewCxt, writer);
        //        return writer.ToString();
        //    }
        //}
        void WriteFile(string content = null)
        {
            #region Convert Data JSON
            string o = Newtonsoft.Json.JsonConvert.SerializeObject(_model);
            //object o = JSON.Deserialize<object>(JSON.Serialize(_model));
            //object or = JSON.Deserialize<object>(json);
            //var jobj = JsonConvert.DeserializeObject(json);
            //object o = new JavaScriptSerializer().Deserialize<object>(json);
            #endregion

            #region JSON for use XML
            StringBuilder SB = new StringBuilder();
            SB.Append("{");
            SB.Append("\"?xml\":");
            SB.Append("{");
            SB.Append("\"@version\": \"1.0\",");
            SB.Append("\"@standalone\": \"no\"");
            SB.Append("},");
            SB.Append("\"records\":");
            SB.Append("{");
            SB.Append("\"record\":");
            //SB.Append(o.ToString());
            //SB.Append(_exportType.ToLower().Equals("html") ? o.ToString().Replace("\r\n", "<br/>") : o.ToString());
            SB.Append(_exportType.ToLower().Equals("html") ? o.ToString().Replace("\\r\\n", "<br>") : o.ToString().Replace("\\r\\n", "\r\n"));
            //SB.Append(_exportType.ToLower().Equals("html") ? o.ToString() : o.ToString());
            SB.Append("}}");
            #endregion

            #region Convert JSON to XML
            XmlDocument XD = (XmlDocument)JsonConvert.DeserializeXmlNode(@SB.ToString());
            XmlNode XN = XD as XmlNode;
            #endregion

            HttpContext context = HttpContext.Current;

            #region Clear Buffer
            context.Response.Clear();
            #endregion
            switch (_exportType.ToLower())
            {
                #region Document Type XML
                case "xml":
                    string strXml = XN.OuterXml;
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + _fileName + ".xml");
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    context.Response.AddHeader("Content-Length", strXml.Length.ToString());
                    context.Response.ContentType = "application/xml";
                    context.Response.Write(strXml);
                    break;
                #endregion

                #region Document Type XLS
                case "xls":
                    context.Response.ContentType = "application/vnd.ms-excel";
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + _fileName + ".xls");
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    XslCompiledTransform XCT1 = new XslCompiledTransform();
                    string excelPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(@"~/xls/"), "excel.xslt");
                    XCT1.Load(excelPath);
                    XCT1.Transform(XN, null, context.Response.OutputStream);
                    break;
                #endregion

                #region Document Type CSV
                case "csv":
                    context.Response.ContentType = "application/octet-stream";
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + _fileName + ".csv");
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    XslCompiledTransform XCT2 = new XslCompiledTransform();
                    string xsltPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(@"~/xls/"), "csv.xslt");
                    XCT2.Load(xsltPath);
                    XCT2.Transform(XN, null, context.Response.OutputStream);
                    break;
                #endregion

                #region Document Type PDF
                case "pdf":
                    using (XmlNodeReader XNR = new XmlNodeReader(XN))
                    {
                        using (DataSet DS = new DataSet())
                        {
                            DS.ReadXml(XNR);

                            using (System.Web.UI.WebControls.GridView GridView1 = new System.Web.UI.WebControls.GridView())
                            {
                                GridView1.AllowPaging = false;
                                //GridView1..AlternatingRowStyle=tablei
                                GridView1.DataSource = DS.Tables[0];
                                GridView1.DataBind();

                                context.Response.ContentType = "application/pdf";
                                context.Response.AddHeader("content-disposition", "attachment;filename=" + _fileName + ".pdf");
                                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

                                using (StringWriter SW = new StringWriter())
                                {
                                    using (HtmlTextWriter hw = new HtmlTextWriter(SW))
                                    {
                                        GridView1.RenderControl(hw);

                                        using (StringReader sr = new StringReader(SW.ToString()))
                                        {
                                            using (Document D = new Document(PageSize.A4, 10f, 10f, 10f, 0f))
                                            {
                                                using (HTMLWorker HW = new HTMLWorker(D))
                                                {
                                                    using (PdfWriter.GetInstance(D, context.Response.OutputStream))
                                                    {
                                                        D.Open();
                                                        HW.Parse(sr);
                                                        D.Close();
                                                        context.Response.Write(D);
                                                    }
                                                };
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region Document Type DOC
                case "doc":
                    using (XmlNodeReader XNR = new XmlNodeReader(XN))
                    {
                        using (DataSet DS = new DataSet())
                        {
                            DS.ReadXml(XNR);

                            using (System.Web.UI.WebControls.GridView GridView1 = new System.Web.UI.WebControls.GridView())
                            {
                                GridView1.AllowPaging = false;
                                GridView1.DataSource = DS.Tables[0];
                                GridView1.DataBind();

                                context.Response.ContentType = "application/vnd.ms-word";
                                context.Response.AddHeader("content-disposition", "attachment;filename=" + _fileName + ".doc");
                                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

                                using (StringWriter SW = new StringWriter())
                                {
                                    using (HtmlTextWriter HW = new HtmlTextWriter(SW))
                                    {
                                        GridView1.RenderControl(HW);
                                        context.Response.Output.Write(SW.ToString());
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion

                #region Document Type HTML
                case "html":
                    using (XmlNodeReader XNR = new XmlNodeReader(XN))
                    {
                        using (DataSet DS = new DataSet())
                        {
                            DS.ReadXml(XNR);

                            using (System.Web.UI.WebControls.GridView GridView1 = new System.Web.UI.WebControls.GridView())
                            {
                                GridView1.AllowPaging = false;
                                GridView1.DataSource = DS.Tables[0];
                                GridView1.DataBind();

                                context.Response.ContentType = "text/html";
                                context.Response.AddHeader("content-disposition", "attachment;filename=" + _fileName + ".html");
                                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

                                using (StringWriter SW = new StringWriter())
                                {
                                    using (HtmlTextWriter HW = new HtmlTextWriter(SW))
                                    {
                                        GridView1.RenderControl(HW);
                                        context.Response.Output.Write(SW.ToString());
                                    }
                                }
                            }
                        }
                    }
                    break;
                #endregion
            }

            #region Close Buffer
            context.Response.End();
            #endregion
        }

        public override void ExecuteResult(ControllerContext context)
        {
            //string content = this.RenderViewToString();
            this.WriteFile();
        }
    }
}