﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class PagedGroupModel
    {
        public IEnumerable<Group> Group { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class ModelGroup : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Group> GetGroup()
        {
            return entities.Groups.Where(v => v.ClientId == ClientId).ToList();
        }

        public IEnumerable<object> GetGroupDetails()
        {
            ResponseModel _rm = new ResponseModel();
            return entities.Groups.Where(b => b.ClientId == ClientId).Select(b => new
            {
                GroupId = b.GroupId,
                GroupName = b.GroupName
            }).ToList();
        }

        //For Custom Paging

        public IEnumerable<Group> GetGroupPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.Groups
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountGroup()
        {
            return entities.Groups.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Group GetGroup(int mGroupId)
        {
            return entities.Groups.Where(m => m.GroupId == mGroupId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateGroup(Group mGroup)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.Groups.AddObject(mGroup);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Group saved";// +mGroup.GroupName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateGroup(Group mGroup)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Group data = entities.Groups.Where(m => m.GroupId == mGroup.GroupId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mGroup;

                        mGroup = (Group)entities.Groups.ApplyCurrentValues(mGroup);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Group updated";// +mGroup.GroupName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Group " + mGroup.GroupName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteGroup(int mGroupID)
        {
            try
            {
                Group data = entities.Groups.Where(m => m.GroupId == mGroupID && m.ClientId == ClientId).FirstOrDefault();
                entities.Groups.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}