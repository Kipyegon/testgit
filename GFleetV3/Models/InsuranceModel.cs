﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{

    public class PagedInsuranceModel
    {
        public IEnumerable<Insuarance> Insuarance { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedInsuranceTypeModel
    {
        public IEnumerable<InsuaranceType> InsuaranceType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedVehicleInsuaranceModel
    {
        public IEnumerable<VehicleInsuarance> VehicleInsuarance { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class ModelInsuarance : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public IEnumerable<Insuarance> GetInsuarance()
        {
            return entities.Insuarances.ToList();
        }

        //For Custom Paging

        public IEnumerable<Insuarance> GetInsuarancePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.Insuarances
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountInsuarance()
        {
            return entities.Insuarances.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Insuarance GetInsuarance(int mInsuaranceId)
        {
            return entities.Insuarances.Where(m => m.InsuaranceId == mInsuaranceId).FirstOrDefault();
        }

        public ResponseModel CreateInsuarance(Insuarance mInsuarance)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.Insuarances.AddObject(mInsuarance);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Data = new
                {
                    InsuaranceName = mInsuarance.InsuaranceName,
                    ContactPerson = mInsuarance.ContactPerson,
                    Email = mInsuarance.Email,
                    Telephone = mInsuarance.Telephone,
                    Address = mInsuarance.Address,
                    InsuaranceId = mInsuarance.InsuaranceId
                };
                _rm.Message = "Insuarance saved";// +mInsuarance.InsuaranceName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateInsuarance(Insuarance mInsuarance)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Insuarance data = entities.Insuarances.Where(m => m.InsuaranceId == mInsuarance.InsuaranceId).FirstOrDefault();


                    data = mInsuarance;

                    mInsuarance = (Insuarance)entities.Insuarances.ApplyCurrentValues(mInsuarance);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Insuarance updated";// +mInsuarance.InsuaranceName + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteInsuarance(int mInsuaranceID)
        {
            try
            {
                Insuarance data = entities.Insuarances.Where(m => m.InsuaranceId == mInsuaranceID).FirstOrDefault();
                entities.Insuarances.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class ModelInsuaranceType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public IEnumerable<InsuaranceType> GetInsuaranceType()
        {
            return entities.InsuaranceTypes.ToList();
        }

        //For Custom Paging

        public IEnumerable<InsuaranceType> GetInsuaranceTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.InsuaranceTypes
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountInsuaranceType()
        {
            return entities.InsuaranceTypes.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public InsuaranceType GetInsuaranceType(int mInsuaranceTypeId)
        {
            return entities.InsuaranceTypes.Where(m => m.InsuaranceTypeId == mInsuaranceTypeId).FirstOrDefault();
        }

        public ResponseModel CreateInsuaranceType(InsuaranceType mInsuaranceType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.InsuaranceTypes.AddObject(mInsuaranceType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Insuarance Type saved";// +mInsuaranceType.InsuaranceTypeName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateInsuaranceType(InsuaranceType mInsuaranceType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    InsuaranceType data = entities.InsuaranceTypes.Where(m => m.InsuaranceTypeId == mInsuaranceType.InsuaranceTypeId).FirstOrDefault();


                    data = mInsuaranceType;

                    mInsuaranceType = (InsuaranceType)entities.InsuaranceTypes.ApplyCurrentValues(mInsuaranceType);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Insuarance Type updated";// +mInsuaranceType.InsuaranceTypeName + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteInsuaranceType(int mInsuaranceTypeID)
        {
            try
            {
                InsuaranceType data = entities.InsuaranceTypes.Where(m => m.InsuaranceTypeId == mInsuaranceTypeID).FirstOrDefault();
                entities.InsuaranceTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }

    public class ModelVehicleInsuarance : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<VehicleInsuarance> GetVehicleInsuarance()
        {
            return entities.VehicleInsuarances.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<VehicleInsuarance> GetVehicleInsuarancePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.VehicleInsuarances
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .GroupBy(c => c.VehicleId)
              .Select(c => c.FirstOrDefault())
              .ToList();
        }
        public int CountVehicleInsuarance()
        {
            return entities.VehicleInsuarances.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public VehicleInsuarance GetVehicleInsuarance(int mVehicleInsuaranceId)
        {
            return entities.VehicleInsuarances.Where(m => m.VehicleInsuaranceId == mVehicleInsuaranceId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateVehicleInsuarance(VehicleInsuarance mVehicleInsuarance)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                mVehicleInsuarance.Insuarance = null;
                mVehicleInsuarance.InsuaranceType = null;

                entities.VehicleInsuarances.AddObject(mVehicleInsuarance);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Insuarance Details saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateVehicleInsuarance(VehicleInsuarance mVehicleInsuarance)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    VehicleInsuarance data = entities.VehicleInsuarances.Where(m =>
                        m.VehicleInsuaranceId == mVehicleInsuarance.VehicleInsuaranceId
                         && m.ClientId == ClientId).FirstOrDefault();
                    if (data != null)
                    {
                        mVehicleInsuarance.InsuaranceId = mVehicleInsuarance.Insuarance.InsuaranceId;
                        mVehicleInsuarance.Insuarance = null;

                        mVehicleInsuarance.InsuaranceTypeId = mVehicleInsuarance.InsuaranceType.InsuaranceTypeId;
                        mVehicleInsuarance.InsuaranceType = null;

                        data = mVehicleInsuarance;

                        mVehicleInsuarance = (VehicleInsuarance)entities.VehicleInsuarances.ApplyCurrentValues(mVehicleInsuarance);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Insuarance Details updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Insuarance Details not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteVehicleInsuarance(int mVehicleInsuaranceID)
        {
            try
            {
                VehicleInsuarance data = entities.VehicleInsuarances.Where(m => m.VehicleInsuaranceId == mVehicleInsuaranceID && m.ClientId == ClientId).FirstOrDefault();
                entities.VehicleInsuarances.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}