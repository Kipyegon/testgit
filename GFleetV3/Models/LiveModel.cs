﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using DataAccess.SQL;
using System.Reflection;
using System.Threading.Tasks;

namespace GFleetV3.Models
{
    public class TrackAlert
    {
        public Boolean IsEnabled;
        public int TrackAlertConfigId;
        public string AlertConfigName;
        public int AlertConfigTypeId;
    }

    public class LiveLatestDataParams
    {
        public LiveLatestDataParams(DateTime vehicleDateTime, int vehicleId)
        {
            this.VehicleDateTime = vehicleDateTime;
            this.VehicleId = vehicleId;
        }
        public DateTime? VehicleDateTime { get; set; }
        public int VehicleId { get; set; }
    }

    public class VehicleTreeModel
    {
        public string id { get; set; }
        public string label { get; set; }
        public Boolean isFolder { get; set; }
        public Boolean open { get; set; }
        public string icon { get; set; }
        public string vehicleid { get; set; }
        public string groupname { get; set; }
        public List<VehicleTreeModel> childs { get; set; }

        //public string icon { get; set; }
        //public string icon { get; set; }

        public VehicleTreeModel()
        {
            isFolder = false;
            open = true;
            //childs = new List<VehicleTreeModel>();
        }

        public static List<VehicleTreeModel> getVehicleTree()
        {
            List<VehicleTreeModel> vtm = new List<VehicleTreeModel>();

            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                IEnumerable<IGrouping<string, vw_VehicleDetailsV3>> vehicles = entities.vw_VehicleDetailsV3.Where(v => v.ClientId == clientId)
                    .GroupBy(v => v.GroupName);

                int defaultValue = 0;

                foreach (IGrouping<string, vw_VehicleDetailsV3> _vehicle in vehicles)
                {
                    VehicleTreeModel tree = new VehicleTreeModel()
                    {
                        label = string.IsNullOrEmpty(_vehicle.Key) ? "Default" + (++defaultValue).ToString() : _vehicle.Key,
                        isFolder = true,
                        groupname = _vehicle.Key
                    };

                    foreach (vw_VehicleDetailsV3 vehicle in _vehicle)
                    {
                        if (string.IsNullOrEmpty(tree.id))
                            tree.id = string.IsNullOrEmpty(vehicle.GroupId.ToString()) ? defaultValue.ToString() : vehicle.GroupId.ToString();

                        VehicleTreeModel node = new VehicleTreeModel()
                        {
                            id = vehicle.VehicleId.ToString(),
                            label = vehicle.RegistrationNo,
                            vehicleid = vehicle.RegistrationNo
                        };
                        if (tree.childs == null)
                            tree.childs = new List<VehicleTreeModel>();
                        tree.childs.Add(node);
                    }

                    vtm.Add(tree);
                }
            }

            return vtm;
        }

        public static Object getVehicleTree1()
        {
            List<Object> vtm = new List<Object>();

            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                IEnumerable<IGrouping<string, vw_VehicleDetailsV3>> vehicles = entities.vw_VehicleDetailsV3.Where(v => v.ClientId == clientId)
                    .GroupBy(v => v.GroupName);

                int defaultValue = 0;

                foreach (IGrouping<string, vw_VehicleDetailsV3> _vehicle in vehicles)
                {
                    Object tree = new
                    {
                        open = true,
                        label = string.IsNullOrEmpty(_vehicle.Key) ? "Default" + (++defaultValue).ToString() : _vehicle.Key,
                        id = defaultValue.ToString(),
                        isFolder = true,
                        groupname = _vehicle.Key,
                        childs = getchilds(defaultValue, _vehicle)
                    };

                    vtm.Add(tree);
                }
            }

            return vtm;
        }

        private static Object getchilds(int defaultValue, IGrouping<string, vw_VehicleDetailsV3> _vehicle)
        {
            List<Object> tree = new List<object>();
            foreach (vw_VehicleDetailsV3 vehicle in _vehicle)
            {
                //if (string.IsNullOrEmpty(tree.id))
                //    tree.id = string.IsNullOrEmpty(vehicle.GroupId.ToString()) ? defaultValue.ToString() : vehicle.GroupId.ToString();

                Object node = new
                {
                    isFolder = false,
                    //open = true,
                    id = vehicle.VehicleId.ToString(),
                    label = vehicle.RegistrationNo,
                    groupname = _vehicle.Key,
                    vehicleid = vehicle.RegistrationNo
                };
                tree.Add(node);
            }
            return tree.Count > 0 ? tree : null;
        }
    }

    public class LiveModel
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();
        public static List<getWebTrackData_V3_Result> getWebTrackData()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int? clientId = Service.Utility.getClientId();
                return clientId.HasValue ? entities.getWebTrackData_V3(clientId.Value).ToList<getWebTrackData_V3_Result>()
                    : new List<getWebTrackData_V3_Result>();
            }
        }

        public static List<getLatestTrackData_V3_Result> getLatestTrackData(DateTime highestdate)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int? clientId = Service.Utility.getClientId();
                return clientId.HasValue ? entities.getLatestTrackData_V3(clientId.Value, highestdate).ToList<getLatestTrackData_V3_Result>()
                    : new List<getLatestTrackData_V3_Result>();
            }
        }

        public static List<object> getRouteReport(string Registration, DateTime startDate, DateTime endDate)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int? clientId = Service.Utility.getClientId();
                if (clientId.HasValue)
                {
                    List<object> data = new List<object>();
                    List<getRouteReport_V2_Result> report = new List<getRouteReport_V2_Result>();

                    IEnumerable<IGrouping<string, getRouteReport_V2_Result>> availableData = entities.getRouteReport_V2(Registration, clientId.Value, startDate, endDate).GroupBy(r => r.start_date);
                    int rec = 0;
                    foreach (IGrouping<string, getRouteReport_V2_Result> rhr in availableData)
                    {
                        if (!string.IsNullOrEmpty(rhr.Key))
                        {
                            rec = rec + 1;
                            string dow = DateTime.Parse(rhr.Key.ToString()).DayOfWeek.ToString();
                            string date = rhr.Key.ToString();
                            string dt = rhr.First().start_datetime;

                            data.Add(new
                            {
                                dow = dow,
                                date = date,
                                dt = dt,
                                data = rhr.ToArray()
                            });
                        }
                    }
                    return data;
                }
                else
                {
                    return new List<object>();
                }
            }
        }

        public static List<getPlotRouteReport_V3_Result> getPlotRouteReport(string Registration, DateTime startDate, DateTime endDate)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int? clientId = Service.Utility.getClientId();
                return clientId.HasValue ? entities.getPlotRouteReport_V3(clientId.Value, startDate, endDate, Registration).ToList<getPlotRouteReport_V3_Result>()
                    : new List<getPlotRouteReport_V3_Result>();
            }
        }

        public static string sendCommand(int vehiclId, string commandType, string IP)
        {
            Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());
            HttpContext current = HttpContext.Current;

            return sendCommand(vehiclId, commandType, IP, current, userId);
            /*
                string result = "";
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    try
                    {
                        Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

                        List<getCommandString_Result> data = entities.getCommandString(vehiclId, commandType).ToList<getCommandString_Result>();
                        if (data.Count > 0)
                        {
                            string cmdIp = data[0].cmdIp;
                            int cmdPort = data[0].cmdPort.Value;
                            //int cmdPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["X1CommandPort"]);
                            //string cmdIp = System.Configuration.ConfigurationManager.AppSettings["X1CommandIP"];

                            if (!string.IsNullOrEmpty(data[0].CommandString) && !string.IsNullOrEmpty(data[0].UnitId))
                            {
                                TcpClient tcpClient = new TcpClient();
                                try
                                {

                                    tcpClient.Connect(cmdIp, cmdPort);
                                    NetworkStream outStream = tcpClient.GetStream();
                                    //Let's set up the command!
                                    byte[] cmd = Encoding.ASCII.GetBytes("id:" + data[0].UnitId + ",cmd:" + data[0].CommandString);
                                    outStream.Write(cmd, 0, cmd.Length);

                                    Thread.Sleep(2000);
                                    if (outStream.CanRead)
                                    {
                                        byte[] myReadBuffer = new byte[1024];
                                        StringBuilder myCompleteMessage = new StringBuilder();
                                        int numberOfBytesRead = 0;

                                        // Incoming message may be larger than the buffer size.
                                        do
                                        {
                                            numberOfBytesRead = outStream.Read(myReadBuffer, 0, myReadBuffer.Length);

                                            myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                                        }
                                        while (outStream.DataAvailable);


                                        // Print out the received message to the console.
                                        result = myCompleteMessage.ToString();
                                    }
                                    else
                                    {
                                        //Console.WriteLine("Sorry. You cannot read from this NetworkStream.");
                                        result = "Request for location of  received at server";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    result = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                                }
                                finally
                                {
                                    entities.createSendCommand(data[0].CommandString, IP, result, data[0].UnitId, userId);
                                    tcpClient.Close();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                        //throw new Exception(ex.Message.ToString());
                    }
                    return result;
                }
             */
        }

        public static string sendCommand(int vehiclId, string commandType, string IP, HttpContext current, Guid userId)
        {
            string result = "";
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                try
                {
                    List<getCommandString_Result> data = entities.getCommandString(vehiclId, commandType).ToList<getCommandString_Result>();
                    if (data.Count > 0)
                    {
                        string cmdIp = data[0].cmdIp;
                        int cmdPort = data[0].cmdPort.Value;
                        //int cmdPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["X1CommandPort"]);
                        //string cmdIp = System.Configuration.ConfigurationManager.AppSettings["X1CommandIP"];

                        if (!string.IsNullOrEmpty(data[0].CommandString) && !string.IsNullOrEmpty(data[0].UnitId))
                        {
                            TcpClient tcpClient = new TcpClient();
                            try
                            {

                                tcpClient.Connect(cmdIp, cmdPort);
                                NetworkStream outStream = tcpClient.GetStream();
                                //Let's set up the command!
                                byte[] cmd = Encoding.ASCII.GetBytes("id:" + data[0].UnitId + ",cmd:" + data[0].CommandString);
                                outStream.Write(cmd, 0, cmd.Length);

                                Thread.Sleep(2000);
                                if (outStream.CanRead)
                                {
                                    byte[] myReadBuffer = new byte[1024];
                                    StringBuilder myCompleteMessage = new StringBuilder();
                                    int numberOfBytesRead = 0;

                                    // Incoming message may be larger than the buffer size.
                                    do
                                    {
                                        numberOfBytesRead = outStream.Read(myReadBuffer, 0, myReadBuffer.Length);

                                        myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                                    }
                                    while (outStream.DataAvailable);


                                    // Print out the received message to the console.
                                    result = myCompleteMessage.ToString();

                                    if (!result.ToLower().StartsWith("command") &&
                                        (commandType.ToLower().Contains("startengine") ||
                                        commandType.ToLower().Contains("stopengine")))
                                    {
                                        sendEngineControlSMS(vehiclId, commandType);
                                    }
                                }
                                else
                                {
                                    //Console.WriteLine("Sorry. You cannot read from this NetworkStream.");
                                    //result = "Error: Request for location of  received at server";
                                    result = "Error: Unknown whether device received request";
                                }
                            }
                            catch (Exception ex)
                            {
                                result = "Error: " + ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, current);
                            }
                            finally
                            {
                                entities.createSendCommand(data[0].CommandString, IP, result, data[0].UnitId, userId);
                                tcpClient.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = "Error: " + ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, current);
                    //throw new Exception(ex.Message.ToString());
                }
                return result;
            }
        }

        public static void sendEngineControlSMS(int vehicleId, string Command)
        {
            new Thread(() =>
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<getCommandSms_Result> data = entities.getCommandSms(vehicleId, Command).ToList<getCommandSms_Result>();

                    if (data.Count > 0)
                    {
                        string phoneNumber = data[0].PhoneNumber;
                        string sms = data[0].CommandString;

                        if (phoneNumber.Length > 0 && sms.Length > 0)
                        {
                            entities.SendSMS(phoneNumber, sms);
                        }
                    }
                }
            }).Start();
        }

        public static List<string> getDevicesLogs(string IP)
        {
            List<vw_VehicleDetailsV3> vehicles; List<string> result = new List<string>();
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                vehicles = entities.vw_VehicleDetailsV3.Where(v => v.DeviceModelId == 1 || v.DeviceModelId == 2).ToList<vw_VehicleDetailsV3>();
            }

            foreach (vw_VehicleDetailsV3 vw in vehicles)
            {
                result.Add(sendCommand(vw.VehicleId, "GETLOG", IP));
            }

            return result;
        }

        public static ResponseModel getGeoFence()
        {
            ResponseModel _rm = new ResponseModel();
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int? clientId = Service.Utility.getClientId();
                if (clientId.HasValue)
                {
                    _rm.Data = entities.vw_GeoFence.Where(b => b.ClientId == clientId.Value).Select(b => new
                    {
                        Active = b.Active,
                        ClientId = b.ClientId,
                        Speed = b.Speed,
                        IsSpeedGeoFence = b.IsSpeedGeoFence,
                        Date = b.Date,
                        Email = b.Email,
                        EntryActive = b.EntryActive,
                        ExitActive = b.ExitActive,
                        FreqInMin_Entry = b.FreqInMin_Entry,
                        FreqInMin_Exit = b.FreqInMin_Exit,
                        GeoFenceId = b.GeoFenceId,
                        GeoFenceName = b.GeoFenceName,
                        Geom = b.Geom,
                        GroupIds = b.GroupIds,
                        NoOfAlerts_Entry = b.NoOfAlerts_Entry,
                        NoOfAlerts_Exit = b.NoOfAlerts_Exit,
                        PhoneNumber = b.PhoneNumber,
                        ReportOnEntry = b.ReportOnEntry,
                        ReportOnExit = b.ReportOnExit
                    }).ToList<object>();

                    _rm.Status = true;
                    return _rm;
                }
                else
                {
                    _rm.Status = false; _rm.Message = "Client not Known";
                    return _rm;
                }
            }
        }

        public static ResponseModel addGeofence(string name, string points)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    entities.GeofenceInsert(Service.Utility.getClientId().Value, name, points, Guid.Parse(Service.Utility.getUserId().ToString()));
                    _rm.Status = true; _rm.Message = "Geofence " + name + " successfully saved!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public static ResponseModel updateGeofence(int id, string groupIds, string name, Boolean geofenceactive, Boolean? reportonexit, int? reportonexitfreq,
                int? reportonexitnoofalerts, Boolean? reportonentry, int? reportonentryfreq, int? reportonentrynoofalerts, string email, string phone, int? speed, Boolean Isgeofencespeed)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    int? clientId = Service.Utility.getClientId();
                    if (!clientId.HasValue)
                    {
                        _rm.Status = false; _rm.Message = "Unknown client during operation";
                        return _rm;
                    }
                    Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

                    GeoFence toUpdate = entities.GeoFences.Single(v => v.GeoFenceId == id && v.ClientId == clientId.Value);
                    if (toUpdate != null)
                    {
                        toUpdate.Active = geofenceactive; toUpdate.Date = DateTime.Now;
                        toUpdate.Email = email; toUpdate.EntryActive = reportonentry;
                        toUpdate.ExitActive = reportonexit; toUpdate.FreqInMin_Entry = reportonentryfreq;
                        toUpdate.FreqInMin_Exit = reportonexitfreq; toUpdate.GeoFenceName = name;
                        toUpdate.ModifiedBy = userId; toUpdate.NoOfAlerts_Entry = reportonentrynoofalerts;
                        toUpdate.NoOfAlerts_Exit = reportonexitnoofalerts; toUpdate.PhoneNumber = phone;
                        toUpdate.ReportOnEntry = reportonentry; toUpdate.ReportOnExit = reportonexit;
                        toUpdate.Speed = speed; toUpdate.IsSpeedGeoFence = Isgeofencespeed;

                        HashSet<int> dltGroup = new HashSet<int>(toUpdate.GeoFenceGroups.Select(c => c.GeoFenceGroupId));

                        entities.GeoFenceGroups.Where(w => w.GeoFenceId == id && dltGroup.Contains(w.GeoFenceGroupId))
                                            .ToList()
                                            .ForEach(entities.GeoFenceGroups.DeleteObject);

                        foreach (string groupId in groupIds.Split(','))
                        {
                            if (!String.IsNullOrEmpty(groupId))
                            {
                                toUpdate.GeoFenceGroups.Add(new GeoFenceGroup()
                                {
                                    ClientId = clientId.Value,
                                    GroupId = int.Parse(groupId),
                                    Date = DateTime.Now,
                                    GeoFence = null
                                });
                            }
                        }

                        entities.SaveChanges();
                    }
                    _rm.Data = LiveModel.getGeoFence();
                    _rm.Status = true; _rm.Message = "Geofence " + name + " successfully saved!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }

        }
        public static ResponseModel deleteGeofence(int id)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    int? ClientId = Service.Utility.getClientId();
                    if (ClientId.HasValue)
                    {
                        GeoFence data = entities.GeoFences.Where(m => m.GeoFenceId == id && m.ClientId == ClientId.Value).FirstOrDefault();

                        HashSet<int> dltGroup = new HashSet<int>(data.GeoFenceGroups.Select(c => c.GeoFenceGroupId));

                        entities.GeoFenceGroups.Where(w => w.GeoFenceId == id && dltGroup.Contains(w.GeoFenceGroupId))
                                                .ToList()
                                                .ForEach(entities.GeoFenceGroups.DeleteObject);

                        entities.GeoFences.DeleteObject(data);
                        entities.SaveChanges();

                        _rm.Data = LiveModel.getGeoFence();
                        _rm.Status = true; _rm.Message = "Geofence " + data.GeoFenceName + " successfully deleted!";
                        return _rm;
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Unknown client during operation";
                        return _rm;
                    }
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public static List<TrackAlert> getUserTrackConfig()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                List<TrackAlert> _rm = entities.AlertConfigTypes
                    .Select(v => new TrackAlert()
                    {
                        IsEnabled = true,
                        TrackAlertConfigId = -1,
                        AlertConfigName = v.AlertConfigName,
                        AlertConfigTypeId = v.AlertConfigTypeId
                    }).ToList();

                foreach (TrackAlert ta in _rm)
                {
                    updateAlertStatus(ta.AlertConfigTypeId, ta.IsEnabled, "refresh or log in");
                }

                return _rm;
            }
        }

        public static ResponseModel updateAlertStatus(int alertId, Boolean val, string reason)
        {
            ResponseModel _rm = new ResponseModel();

            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                try
                {
                    int ClientId = Service.Utility.getClientId().Value;
                    Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());
                    TrackAlertConfig track = new TrackAlertConfig()
                    {
                        AlertConfigTypeId = alertId,
                        AlertConfigType = null,
                        ClientId = ClientId,
                        Date = DateTime.Now,
                        IsEnabled = val,
                        UserId = userId,
                        Reason = reason
                    };
                    entities.TrackAlertConfigs.AddObject(track);
                    entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    _rm.Status = true; _rm.Message = "Group " + track.AlertConfigType.AlertConfigName + " successfully updated!";
                    return _rm;
                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                    return _rm;
                }
            }
        }

        public async static Task<List<getVehicleLatestTrackData_Result>> ProcessLiveDataAsync(int ClientId, DateTime VehicleDateTime, int VehicleId)
        {
            List<getVehicleLatestTrackData_Result> returndata = new List<getVehicleLatestTrackData_Result>();
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    returndata.AddRange(entities.getVehicleLatestTrackData(ClientId, VehicleDateTime, VehicleId).ToList());
                    //return returndata;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                //return null;
            }
            return returndata;
        }
        public async static Task<List<getVehicleLatestTrackData_Result>> GetLatestLiveDataAsync(List<LiveLatestDataParams> latestDataParams)
        {
            try
            {
                List<getVehicleLatestTrackData_Result> returndata = new List<getVehicleLatestTrackData_Result>();

                int? clientId = Service.Utility.getClientId();

                if (clientId.HasValue)
                {

                    var xyz = new List<Task<List<getVehicleLatestTrackData_Result>>>();

                    foreach (LiveLatestDataParams param in latestDataParams)
                    {
                        if (param.VehicleDateTime.HasValue)
                        {
                            xyz.Add(ProcessLiveDataAsync(clientId.Value, param.VehicleDateTime.Value, param.VehicleId));
                        }
                    }
                    //Task xyz = latestDataParams.Select(c => ProcessDispatchDataAsync(OrganizationId, CustId, ClientId, c.DispatchId, c.DispatchDateTime, null));
                    //xyz.Add(ProcessLiveDataAsync(OrganizationId, CustId, ClientId, null, latestDataParams.OrderBy(c => c.VehicleDateTime).FirstOrDefault().VehicleDateTime, String.Join(",", latestDataParams.Select(v => v.VehicleId))));

                    //await Task.WhenAll(xyz);
                    foreach (var xyztask in await Task.WhenAll(xyz))
                    {
                        returndata.AddRange(xyztask);
                    }
                }

                //string picktoday = DateTime.Now.ToString("yyyy.MM.dd");

                return returndata;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }


        //public IEnumerable<CompanySetting> GetDriver()
        //{
        //    return entities.CompanySettings.OfType<Driver>().Where(v => v.ClientId == ClientId).ToList();
        //}
    }
}