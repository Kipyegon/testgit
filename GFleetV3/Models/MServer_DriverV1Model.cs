﻿using DataAccess.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Data.Entity;
using ServerNotification;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace GFleetV3.Models
{
    public class MServer_DriverV1Model
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public class ClientData
        {
            public int? ClientId { get; set; }
            public string ClientName { get; set; }
        }

        public class devicedata
        {
            public DateTime DateTime { get; set; }
            public string DeviceToken { get; set; }

        }
        public class pushdispatch
        {
            public int DispatchId { get; set; }
            public string Message { get; set; }

        }
        public class DispatchData
        {
            public int? DispatchId { get; set; }
            public string PickUpDate { get; set; }
            public string CustomerName { get; set; }
            public string OrganisationName { get; set; }
            public string PickUpPointName { get; set; }
            public string DropOffPointName { get; set; }
            public double StartMileage { get; set; }
            public double EndMileage { get; set; }
            public string VehicleRegCode { get; set; }
            public string DriverName { get; set; }
            public double Amount { get; set; }
        }

        public List<DispatchData> DispatchedData { get; set; }

        public DriverDetailData DriverDetails { get; set; }

        public static MServer_DriverV1Model getDispatchData(Guid UserId)
        {
            MServer_DriverV1Model dispatches = new MServer_DriverV1Model();
            using (GFleetModelContainer db = new GFleetModelContainer())
            {

                dispatches.DispatchedData = getDispatches(UserId);
                if (dispatches.DispatchedData == null)
                {
                    dispatches.DispatchedData = new List<DispatchData>();
                }
            }
            return dispatches;
        }

        public static List<DispatchData> getDispatches(Guid UserId)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                return (from p in db.getDispatchDriverData(UserId)
                        select new DispatchData
                        {
                            DispatchId = p.DispatchId,
                            PickUpDate = p.PickUpDate,
                            CustomerName = p.Customername,
                            PickUpPointName = p.Pickuppointname,
                            OrganisationName = p.Organisationname,
                            DropOffPointName = p.Dropoffpointname,
                            VehicleRegCode = p.VehicleRegCode,
                            DriverName = p.Drivername,
                        }).ToList();
            }
        }

        public static DriverDetailData getDriverDetails(Guid UserId)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                return (from p in db.getSingleUserProfile(UserId.ToString(), 3).Where(v => v.UserId == UserId)
                        select new DriverDetailData
                        {
                            DriverUserId = p.UserId.ToString(),
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            Telephone1 = p.Telephone1,
                            DriverNo = p.DriverNo,
                            IsMale = p.IsMale,
                            Email1 = p.Email1,
                            IDNumber = p.IDNumber,
                            ClientId = int.Parse(p.ClientId)
                        }).FirstOrDefault();
            }
        }

        public string DeviceId { get; set; }
        public string Device_token { get; set; }
        public int DriverDeviceId { get; set; }
        public string DriverDeviceVehicleId { get; set; }
        public int VehicleId { get; set; }

        public ResponseModel RegisterDevice(string Device_token, string RegistrationNo)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer db = new GFleetModelContainer())
                {
                    Vehicle data = db.Vehicles.Where(m => m.RegistrationNo.Trim().Replace(" ", "") == RegistrationNo.Trim().Replace(" ", "")).FirstOrDefault();
                    if (data != null)
                    {
                        if (db.Vehicles.Any(v => v.RegistrationNo == RegistrationNo && v.Device_token == null))
                        {
                            data.Device_token = Device_token;
                            db.Vehicles.ApplyCurrentValues(data);
                            db.SaveChanges();
                            _rm.Status = true;
                            _rm.Message = "Driver Device successfully pegged to " + RegistrationNo;
                            return _rm;
                        }
                        else
                        {
                            _rm.Status = true;
                            _rm.Message = RegistrationNo + " already has a device";
                            return _rm;
                        }
                    }
                    else
                    {
                        _rm.Status = true;
                        _rm.Message = RegistrationNo + " does not exist in your company";
                        return _rm;
                    }
                }

            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel RegisterDriverDevice(string Device_token, string DeviceNumber)
        {
            ResponseModel rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer db = new GFleetModelContainer())
                {
                    if (Device_token != null)
                    {
                        DriverDevice data = db.DriverDevices.Where(d => d.DeviceToken == Device_token).FirstOrDefault();
                        DriverDevice datanumber = db.DriverDevices.Where(d => d.DeviceNumber == DeviceNumber).FirstOrDefault();
                        if (datanumber == null)
                        {
                            if (data == null)
                            {
                                //data.DeviceToken = Device_token;
                                //data.DateModified = DateTime.Now;
                                var devicedata = new DriverDevice
                                {
                                    DateModified = DateTime.Now,
                                    DeviceToken = Device_token,
                                    DeviceNumber = DeviceNumber,
                                };
                                db.DriverDevices.AddObject(devicedata);
                                db.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                                int DriverDeviceId = devicedata.DriverDeviceId;
                                rm.Status = true;
                                rm.DriverDeviceId = DriverDeviceId;
                                rm.Message = "Device successfully registered!";
                                return rm;
                            }
                            else
                            {
                                rm.Status = false;
                                rm.Message = "Device token already exists !!!!";
                                return rm;
                                //vw_VehicleDetailsV3 vehdata = db.vw_VehicleDetailsV3.Where(d => d.RegistrationNo == RegistrationNo).FirstOrDefault();
                                //Vehicle vehdata = db.Vehicles.Where(m => m.RegistrationNo.Trim().Replace(" ", "") == RegistrationNo.Trim().Replace(" ", "")).FirstOrDefault();
                                //if (vehdata != null)
                                //{
                                //    if (!db.DriverDeviceVehicles.Any(v => v.VehicleId == vehdata.VehicleId))
                                //    {
                                //        var devicevehicledata = new DriverDeviceVehicle
                                //        {
                                //            VehicleId = vehdata.VehicleId,
                                //            DriverDeviceId = data.DriverDeviceId
                                //        };
                                //        db.DriverDeviceVehicles.AddObject(devicevehicledata);
                                //        db.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                                //        rm.Status = true;
                                //        rm.Message = "Driver Device successfully pegged to " + RegistrationNo;
                                //        return rm;
                                //    }
                                //    else
                                //    {
                                //        rm.Status = false;
                                //        rm.Message = RegistrationNo + " already has a device";
                                //        return rm;
                                //    }
                                //}
                                //else
                                //{
                                //    rm.Status = false;
                                //    rm.Message = RegistrationNo + " does not exist in your company";
                                //    return rm;
                                //}
                            }
                        }
                        else
                        {
                            rm.Status = false;
                            rm.Message = "Device number exists!";
                            return rm;
                        }
                    }
                    else
                    {
                        rm.Status = false;
                        rm.Message = "Device token cannot be null!";
                        return rm;
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                rm.Status = false; rm.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return rm;
            }
        }
        public ResponseModel RegisterDriverDeviceVehicle(int DriverDeviceVehicleId, int VehicleId, int DriverDeviceId)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer db = new GFleetModelContainer())
                {
                    //string VehicleId = Convert.ToInt(VehicleId);
                    //DriverDeviceVehicle data db.DriverDeviceVehicles.Any(VehicleId.ToString())
                    DriverDeviceVehicle data = db.DriverDeviceVehicles.Where(d => d.VehicleId.Equals(VehicleId)).First();
                    if (data != null)
                    {
                        if (db.DriverDevices.Any(d => d.DriverDeviceId != null))
                        {
                            data.VehicleId = VehicleId;
                            data.DriverDeviceId = DriverDeviceId;
                            db.DriverDeviceVehicles.AddObject(data);
                            //db.DriverDeviceVehicle.Add(data);
                            db.SaveChanges();
                            _rm.Status = true;
                            _rm.VehicleId = VehicleId;
                            _rm.DriverDeviceId = DriverDeviceId;
                            _rm.DriverDeviceVehicleId = DriverDeviceVehicleId;
                            _rm.Message = "Successful";
                            return _rm;
                        }
                        else
                        {
                            _rm.Status = true;
                            _rm.Message = "No device registered!";
                            return _rm;
                        }
                    }
                    else
                    {
                        _rm.Status = true;
                        _rm.Message = "No vehicle registered!";
                        return _rm;
                    }
                }

            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public static MServer_DriverV1Model getValidatedUser(Guid UserId, string Username)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                MServer_DriverV1Model loggedIn;
                int? ClientId = Service.Utility.getDriverClientId(Username);

                loggedIn = new MServer_DriverV1Model();
                loggedIn.Username = Username;
                loggedIn.UserId = UserId;
                loggedIn.DriverDetails = getDriverDetails(UserId);
                return loggedIn;
            }

        }
        public static MServer_DriverV1Model getDriverDispatches(Guid UserId)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                MServer_DriverV1Model dispatches;

                dispatches = new MServer_DriverV1Model();
                dispatches.DispatchedData = getDispatches(UserId);
                return dispatches;
            }

        }
        public static List<DispatchData> GetDispatchBookData()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return (from p in entities.Dispatches
                        select new DispatchData
                        {
                            DispatchId = p.DispatchId
                        }).ToList();
            }
        }

        public string Username { get; set; }

        public Guid UserId { get; set; }

        public bool IsDriver { get; set; }

        public bool IsAuthenticated { get; set; }

        public string AuthMessage { get; set; }

        public ResponseModel GetVehicleDispatch(int mVehicleDispatchId, int ClientId)
        {
            ResponseModel _rm = new ResponseModel();
            _rm.Data = entities.Dispatches.Where(m => m.DispatchId == mVehicleDispatchId && m.ClientId == ClientId)
                .ToList()
                .Select(v => new
                {
                    v.Amount,
                    v.BookersPhone,
                    v.BookingRequestedBy,
                    PassengersName = v.Person.FirstName + " " + v.Person.LastName,
                    v.Person.Telephone1,
                    v.Person.Email1,
                    v.CustomerId,
                    DispatchAccessibilities = new HashSet<int>(v.DispatchAccessibilities.Select(c => c.AccessibilityId)),
                    v.DispatchedBy,
                    v.DispatchStatusId,
                    DispatchExtras = new HashSet<int>(v.DispatchExtras.Select(c => c.ExtrasId)),
                    v.DispatchId,
                    v.Distance,
                    v.DropOffPointName,
                    v.ExpectedCompletionTime,
                    v.From,
                    v.MoreDetails,
                    v.NoOfLuggage,
                    v.NoOfPassengers,
                    v.PaymentModeId,
                    PickUpDate = string.Format("{0:dd-MM-yyyy}", v.PickUpDateTime),
                    PickUpTime = string.Format("{0:HH:mm}", v.PickUpDateTime),
                    v.PickUpPointName,
                    v.SpecialInstructions,
                    v.To,
                    v.VehicleId,
                    v.Person.Organisation.OrganisationName,
                    v.Person.CustomerDirUrl
                })
                .FirstOrDefault();

            return _rm;
        }

        public static MessageResponseModel PushDriverDispatch(int DispatchId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                try
                {
                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == DispatchId).FirstOrDefault();
                    if (data != null)
                    {
                        var Device = entities.DriverDeviceVehicles
                            .Where(v => v.VehicleId == data.VehicleId).FirstOrDefault();
                        if (Device != null)
                        {
                            var Device_Token = entities.DriverDevices
                                .Where(v => v.DriverDeviceId == Device.DriverDeviceId).FirstOrDefault();
                            //string message = "Dispatch has been made Kindky Tap here to view details.";
                            var pdata = new pushdispatch
                                    {
                                        Message = "Dispatch has been made Kindly Tap here to view details.",
                                        DispatchId = DispatchId
                                    };
                            // var pushdata = new JavaScriptSerializer().Serialize(pdata);
                            //pushdispatch a = new pushdispatch();
                            //a.DispatchId = DispatchId;
                            //a.Message=message;
                            Notify _notify = GlobalNotify.notify;
                            //Guid ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                            Guid ModifiedBy = Guid.Parse("902EB716-D059-4731-88E5-E27D6972C6EC");
                            _notify.sendAndroidNotifications(Device_Token.DeviceToken, DispatchId, ModifiedBy, 1);
                            
                        }
                        return new MessageResponseModel()
                        {
                            Status = true,
                            Error = Device == null ? "Dispatched not yet arrived to vehicle" : "Dispatched arrived to vehicle"
                            //Error = Device.Count() == 0 ? "User is not logged in" : Userlist.Count().ToString() + " user(s) have been polled";

                        };
                    }
                    return new MessageResponseModel()
                    {
                        Status = true,
                        Message = "Dispatched not yet arrived to vehicle"
                        //Error = Device.Count() == 0 ? "User is not logged in" : Userlist.Count().ToString() + " user(s) have been polled";

                    };
                }
                catch (Exception ex)
                {
                    return new MessageResponseModel()
                    {
                        Status = false,
                        Error = "Error occured: " + ex.InnerException == null ? ex.Message : ex.InnerException.Message
                    };
                }
            }
        }
        public ResponseModel UpdateMobileDriver(DriverDetailData mDriver)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {


                    MembershipUser user = Membership.GetUser();
                    MembershipUser mu = Membership.GetUser(Guid.Parse(mDriver.DriverUserId));
                    string username = mu.UserName;
                    if (username != null)
                    {
                        int ClientId = Service.Utility.getDriverClientId(username).Value;
                        getUserProfile_Result data = entities.getUserProfile(ClientId, AccountProfile.Driverdiscriminator).Where(m => m.UserId == Guid.Parse(mDriver.DriverUserId) && m.ClientId == ClientId.ToString()).FirstOrDefault();
                        if (data != null)
                        {
                            AccountProfile profile = AccountProfile.Create(username, true) as AccountProfile;
                            profile.FirstName = mDriver.FirstName;
                            profile.LastName = mDriver.LastName;
                            profile.Email1 = mDriver.Email1;
                            profile.Telephone1 = mDriver.Telephone1;
                            profile.DriverNo = mDriver.DriverNo;
                            profile.IDNumber = mDriver.IDNumber;
                            profile.IsMale = Boolean.Parse(mDriver.IsMale);
                            profile.DiscriminatorId = 3;
                            profile.Save();
                            _rm.Status = true;
                            _rm.Message = "Driver updated";
                        }
                        else
                        {
                            _rm.Message = "Driver " + mDriver.FirstName + " " + mDriver.LastName + " not in your company!"; //_rm.Data = mDriver;
                        }
                    }
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel AcceptDeclineDispatch(int DispatchId, int DispatchStatusId, bool IsAccept)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {

                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == DispatchId).FirstOrDefault();
                    if (data != null)
                    {

                        data.DispatchStatusId = DispatchStatusId;
                        entities.SaveChanges();
                        _rm.Status = true;
                        if (IsAccept)
                        {
                            _rm.Message = "Dispatch accepted!";
                        }
                        else
                        {
                            _rm.Message = "Dispatch declined!";
                        }
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Dispatch not found!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel CloseTrip(string StartJobTime, string StartJobPosition, int? EnrouteDistance, string ClientOnBoardTime,
            string ClientOnBoardPosition, string DropOffTime, string DropOffPosition, int? DropOffMileage, int DispatchId)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    DateTime? starttime = string.IsNullOrEmpty(StartJobTime) ? (DateTime?)null : DateTime.Parse(StartJobTime);
                    DateTime? clientboard = string.IsNullOrEmpty(ClientOnBoardTime) ? (DateTime?)null : DateTime.Parse(ClientOnBoardTime);
                    DateTime? dropofftime = string.IsNullOrEmpty(DropOffTime) ? (DateTime?)null : DateTime.Parse(DropOffTime);

                    List<closeDriverDispatch_Result> tripdata = entities.closeDriverDispatch(starttime,
                        StartJobPosition, EnrouteDistance, clientboard, ClientOnBoardPosition,
                        dropofftime, DropOffPosition, DropOffMileage, DispatchId).
                        ToList<closeDriverDispatch_Result>();
                    _rm.Status = true;
                    _rm.Message = "Trip logged succesfully!";
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel StartTrip(string StartJobTime, string StartJobPosition, int DispatchId)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<closeDriverDispatch_Result> tripdata = entities.closeDriverDispatch(DateTime.Parse(StartJobTime),
                        StartJobPosition, null, null, null,null, null, null, DispatchId).
                        ToList<closeDriverDispatch_Result>();
                    _rm.Status = true;
                    _rm.Message = "Trip started succesfully!";
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel EnrouteTrip( int EnrouteDistance,int DispatchId)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<closeDriverDispatch_Result> tripdata = entities.closeDriverDispatch(null,
                        null, EnrouteDistance, null, null,null, null, null, DispatchId).
                        ToList<closeDriverDispatch_Result>();
                    _rm.Status = true;
                    _rm.Message = "Trip logged succesfully!";
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel ClientBoardTrip(string ClientOnBoardTime,string ClientOnBoardPosition,int DispatchId)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<closeDriverDispatch_Result> tripdata = entities.closeDriverDispatch(null,
                        null, null, DateTime.Parse(ClientOnBoardTime), ClientOnBoardPosition,
                        null, null, null, DispatchId).
                        ToList<closeDriverDispatch_Result>();
                    _rm.Status = true;
                    _rm.Message = "Trip logged succesfully!";
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel DropClientTrip(string DropOffTime, string DropOffPosition, int DropOffMileage, int DispatchId)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<closeDriverDispatch_Result> tripdata = entities.closeDriverDispatch(null,
                        null, null, null, null,DateTime.Parse(DropOffTime), DropOffPosition, DropOffMileage, DispatchId).
                        ToList<closeDriverDispatch_Result>();
                    _rm.Status = true;
                    _rm.Message = "Trip logged succesfully!";
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }


        public ResponseModel WaitCustomer(int DispatchId, string StartWaitTime, string EndWaitTime, string WaitingPosition)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<logWaitingTime_Result> tripdata = entities.logWaitingTime(DispatchId,DateTime.Parse(StartWaitTime),
                        DateTime.Parse(EndWaitTime),WaitingPosition).
                        ToList<logWaitingTime_Result>();
                    _rm.Status = true;
                    _rm.Message = "Waiting time logged succesfully!";
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

    }

    public class DriverDetailData
    {
        public string DriverUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone1 { get; set; }
        public string DriverNo { get; set; }
        public string IsMale { get; set; }
        public string Email1 { get; set; }
        public string IDNumber { get; set; }
        public int ClientId { get; set; }
    }
    public class TripDetails
    {
        public int DispatchId { get; set; }
        public string StartTime { get; set; }
        public string StartJobPosition { get; set; }
        public int EnrouteDisatnce { get; set; }
        public string ClientOnBoardTime { get; set; }
        public string ClientOnBoardPosition { get; set; }
        public string DropOffTime { get; set; }
        public string DropOffPosition { get; set; }
        public int DropOffMileage { get; set; }
    }
    public class WaitingDetails
    {
        public int DispatchId { get; set; }
        public string StartWaitTime { get; set; }
        public string EndWaitTime { get; set; }
        public string WaitingPosition { get; set; }
    }
}