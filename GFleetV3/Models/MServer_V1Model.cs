﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DataAccess.SQL;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Reflection;
using System.Transactions;
using System.Web.Security;
using System.Text;

namespace GFleetV3.Models
{
    public class FuelDetailsData
    {
        public int? ClientId { get; set; }
        public int? VehicleId { get; set; }
        public DateTime DateModified { get; set; }
        public Guid ModifiedBy { get; set; }
        public Guid DriverUserId { get; set; }
        public int? NoOfLitres { get; set; }
        public int? Difference { get; set; }
        public int? CurrentMileage { get; set; }
        public int? PreviousMileage { get; set; }
        public DateTime FuelDate { get; set; }
        public string AvgFuelCons { get; set; }
    }
    public class VehicleData
    {
        public string VehicleRegcode { get; set; }
        public int? VehicleId { get; set; }
        public string GroupName { get; set; }
        public string Town { get; set; }
        public string Street { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Date { get; set; }
        public string Timestamp { get; set; }
        public long? Speed { get; set; }
        public string VehicleStatus { get; set; }
    }
    public class ClientData
    {
        public int? ClientId { get; set; }
        public string ClientName { get; set; }
    }

    public class DriverData
    {
        public string DriverName { get; set; }
        public Guid DriverUserId { get; set; }
    }

    public class VehicleMileageData
    {
        public int? Mileage { get; set; }
        public double? Litres { get; set; }
    }

    public class MServer_V1ModelLatest
    {
        public bool isSuccess { get; set; }
        public string Message { get; set; }
        public List<VehicleData> Vehicles { get; set; }

        public MServer_V1ModelLatest()
        {
            isSuccess = true;
        }
    }

    public class MServer_V1ModelDriver
    {
        public bool isSuccess { get; set; }
        public string Message { get; set; }
        public List<DriverData> Drivers { get; set; }

        public MServer_V1ModelDriver()
        {
            isSuccess = true;
        }
    }

    public class MServer_V1Fuel
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public MServer_V1Fuel()
        {
            Drivers = new List<DriverData>();
            MileageData = new VehicleMileageData();
        }

        public string Message { get; set; }
        public VehicleMileageData MileageData { get; set; }
        public List<DriverData> Drivers { get; set; }
        

        public static List<DriverData> GetDriverData(int ClientId)
        {
            //public static int PersonId;
            //Guid DriverUserId = new Guid(PersonId, 0, 0, new byte[8]);
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                //return (from p in entities.People.OfType<Driver>().Where(v => v.ClientId == ClientId)
                //        select new DriverData
                //        {
                            
                //            DriverName = p.DriverNameNumber,
                //            DriverUserId = p.PersonId,
                //        }).ToList();

                string _ClientId = ClientId.ToString();

                return (from p in entities.vw_Driver.Where(v => v.ClientId == _ClientId)
                        select new DriverData
                        {

                            DriverName = p.DriverNameNumber,
                            DriverUserId = p.UserId,
                        }).ToList();
            }
        }
        public static VehicleMileageData GetVehicleMileageData(int vehicleId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return (from p in entities.VehicleFuelManagements.Where(v => v.VehicleId == vehicleId)
                     .OrderByDescending(v => v.VehicleFuelManagementId).Take(1)
                        select new VehicleMileageData
                        {
                            Mileage = p.CurrentMileage,
                            Litres = p.NoOfLitres
                        }).SingleOrDefault();
            }
        }
        public static MServer_V1Fuel GetVehicleFuelData(int ClientId, int VehicleId)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                MServer_V1Fuel fueldata;

                fueldata = new MServer_V1Fuel();
                fueldata.MileageData = GetVehicleMileageData(VehicleId);
                fueldata.Drivers = GetDriverData(ClientId);

                return fueldata;
            }

        }

        public ResponseModel saveFuelManagement(int VehicleId, int ClientId, double AvgFuelCons, DateTime FuelDate, Guid DriverUserId,
            int CurrentMileage, int PreviuosMileage, Guid UserId, double NoOfLitres, int Difference,double Cost)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                entities.FuelData_Insert(VehicleId, ClientId, UserId,DriverUserId, NoOfLitres, Difference, CurrentMileage, PreviuosMileage,
                    FuelDate, Cost, AvgFuelCons);
                _rm.Status = true;
                _rm.Message = "Fuel Details successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
    }


    public class MServer_V1Model
    {
        public MServer_V1Model()
        {
            Vehicles = new List<VehicleData>();
            IsVehicleControl = false;
            IsAuthenticated = false;
        }

        public string Username { get; set; }
        public Guid UserId { get; set; }
        public int ClientId { get; set; }
        public string CompanyName { get; set; }
        public bool IsAuthenticated { get; set; }
        public bool IsVehicleControl { get; set; }
        public string AuthMessage { get; set; }
        public List<VehicleData> Vehicles { get; set; }
        public List<ClientData> Clients { get; set; }

        public static MServer_V1Model getValidatedUser(Guid UserId, string Username)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                MServer_V1Model loggedIn;
                int? ClientId = Service.Utility.getClientId(Username);

                if (!ClientId.HasValue)
                {
                    loggedIn = new MServer_V1Model();
                    loggedIn.Username = Username;
                    loggedIn.UserId = UserId;
                    loggedIn.Clients = GetClientData();
                }
                else
                {

                    loggedIn = new MServer_V1Model();

                    string[] UserRoles = Roles.GetRolesForUser(Username);
                    if (UserRoles.Contains("Company Admin") || UserRoles.Contains("Company Supervisor"))
                    {
                        loggedIn.IsVehicleControl = true;
                    }
                    loggedIn.Username = Username;
                    loggedIn.UserId = UserId;
                    loggedIn.ClientId = ClientId.Value;
                    loggedIn.CompanyName = db.Clients.SingleOrDefault(v => v.ClientId == ClientId).ClientName;
                    loggedIn.Vehicles = GetVehicleData(ClientId.Value);
                    //loggedIn.Clients = GetClientData();

                }
                return loggedIn;
            }

        }

        public static MServer_V1ModelLatest getLatest(string VehicleIds, int ClientId, DateTime latestdate)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                MServer_V1ModelLatest latest;

                if (ClientId == null)
                {
                    latest = new MServer_V1ModelLatest();
                }
                else
                {

                    latest = new MServer_V1ModelLatest();
                    latest.Vehicles = GetLatestVehicleData(VehicleIds, ClientId, latestdate);
                }
                return latest;
            }

        }

        public static List<VehicleData> GetVehicleData()
        {
            int ClientId = Service.Utility.getClientId().Value;
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return (from p in entities.getWebTrackData_V3(ClientId)
                        select new VehicleData
                        {
                            VehicleRegcode = p.Reg + '(' + p.VehicleCode + ')',
                            VehicleId = p.VehicleId,
                            GroupName = p.GroupName,
                            Latitude = (float)p.Latitude,
                            Longitude = (float)p.Longitude,
                            Town = p.Town,
                            Street = p.Street,
                            Date = p.Date + ' ' + p.Time,
                            Speed = p.Speed,
                            VehicleStatus = p.Event,
                        }).ToList();
            }
        }

        public static List<VehicleData> GetVehicleData(int ClientId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return (from p in entities.getWebTrackData_V3(ClientId)
                        select new VehicleData
                        {
                            VehicleRegcode = p.Reg + '(' + p.VehicleCode + ')',
                            VehicleId = p.VehicleId,
                            GroupName = p.GroupName,
                            Latitude = (float)p.Latitude,
                            Longitude = (float)p.Longitude,
                            Town = p.Town,
                            Street = p.Street,
                            Date = p.timestamp != null ? DateTime.Parse(p.timestamp).ToString("yyyy-MM-dd HH:mm:ss") : null,
                            Timestamp = p.timestamp,
                            Speed = p.Speed,
                            VehicleStatus = p.Event,
                        }).ToList();
            }
        }

        public static List<VehicleData> GetLatestVehicleData(string VehicleIds, int ClientId, DateTime latestdate)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {

                return (from p in entities.getLatestOnVehicleIds(ClientId, VehicleIds, latestdate)
                        select new VehicleData
                        {
                            VehicleRegcode = p.Reg + '(' + p.VehicleCode + ')',
                            VehicleId = p.VehicleDetailId,
                            GroupName = p.GroupName,
                            Latitude = (float)p.Latitude,
                            Longitude = (float)p.Longitude,
                            Town = p.Town,
                            Street = p.Street,
                            Date = p.Date != null ? p.Date : null,
                            Speed = p.Speed,
                            Timestamp = p.timestamp,
                            VehicleStatus = p.Event,
                        }).ToList();
            }
        }

        public static List<ClientData> GetClientData()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return (from p in entities.Clients
                        select new ClientData
                        {
                            ClientId = p.ClientId,
                            ClientName = p.ClientName
                        }).ToList();
            }
        }


        public static string sendCommand(int vehiclId, string commandType, string IP)
        {
            HttpContext current = HttpContext.Current;

            return sendCommand(vehiclId, commandType, IP, current);
        }

        public static string sendCommand(int vehiclId, string commandType, string IP, HttpContext current)
        {
            string result = "";
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                try
                {
                    List<getCommandString_Result> data = entities.getCommandString(vehiclId, commandType).ToList<getCommandString_Result>();
                    if (data.Count > 0)
                    {
                        string cmdIp = data[0].cmdIp;
                        int cmdPort = data[0].cmdPort.Value;
                        //int cmdPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["X1CommandPort"]);
                        //string cmdIp = System.Configuration.ConfigurationManager.AppSettings["X1CommandIP"];

                        if (!string.IsNullOrEmpty(data[0].CommandString) && !string.IsNullOrEmpty(data[0].UnitId))
                        {
                            TcpClient tcpClient = new TcpClient();
                            try
                            {

                                tcpClient.Connect(cmdIp, cmdPort);
                                NetworkStream outStream = tcpClient.GetStream();
                                //Let's set up the command!
                                byte[] cmd = Encoding.ASCII.GetBytes("id:" + data[0].UnitId + ",cmd:" + data[0].CommandString);
                                outStream.Write(cmd, 0, cmd.Length);

                                Thread.Sleep(2000);
                                if (outStream.CanRead)
                                {
                                    byte[] myReadBuffer = new byte[1024];
                                    StringBuilder myCompleteMessage = new StringBuilder();
                                    int numberOfBytesRead = 0;

                                    // Incoming message may be larger than the buffer size.
                                    do
                                    {
                                        numberOfBytesRead = outStream.Read(myReadBuffer, 0, myReadBuffer.Length);

                                        myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));

                                    }
                                    while (outStream.DataAvailable);


                                    // Print out the received message to the console.
                                    result = myCompleteMessage.ToString();

                                    if (!result.ToLower().StartsWith("command") &&
                                        (commandType.ToLower().Contains("startengine") ||
                                        commandType.ToLower().Contains("stopengine")))
                                    {
                                        sendEngineControlSMS(vehiclId, commandType);
                                    }
                                }
                                else
                                {
                                    //Console.WriteLine("Sorry. You cannot read from this NetworkStream.");
                                    //result = "Error: Request for location of  received at server";
                                    result = "Error: Unknown whether device received request";
                                }
                            }
                            catch (Exception ex)
                            {
                                result = "Error: " + ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, current);
                            }
                            finally
                            {
                                entities.createSendCommandMobile(data[0].CommandString, IP, result, data[0].UnitId);
                                tcpClient.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = "Error: " + ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, current);
                    //throw new Exception(ex.Message.ToString());
                }
                return result;
            }
        }

        public static void sendEngineControlSMS(int vehicleId, string Command)
        {
            new Thread(() =>
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<getCommandSms_Result> data = entities.getCommandSms(vehicleId, Command).ToList<getCommandSms_Result>();

                    if (data.Count > 0)
                    {
                        string phoneNumber = data[0].PhoneNumber;
                        string sms = data[0].CommandString;

                        if (phoneNumber.Length > 0 && sms.Length > 0)
                        {
                            entities.SendSMS(phoneNumber, sms);
                        }
                    }
                }
            }).Start();
        }
    }


    public class Client_Phone
    {
        public string Phone_token { get; set; }

        public static void RegisterDevice(string Phone_token, Guid UserId)
        {
            new Thread(() =>
            {
                try
                {
                    using (GFleetModelContainer db = new GFleetModelContainer())
                    {
                        if (!db.ClientPhones.Any(v => v.UserId == UserId && v.Phone_token == Phone_token))
                        {
                            ClientPhone updated = new ClientPhone()
                            {
                                Phone_token = Phone_token,
                                UserId = UserId
                            };
                            db.ClientPhones.AddObject(updated);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }).Start();
        }

        private static void resetDevice(string Phone_token, GFleetModelContainer db)
        {
            using (db)
            {
                IQueryable<ClientPhone> contacts = db.ClientPhones.Where(u => u.Phone_token.Equals(Phone_token));
                foreach (ClientPhone _contact in contacts)
                {
                    _contact.Phone_token = null;
                }
                db.SaveChanges();
            }
        }
    }

}