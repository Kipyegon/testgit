﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class PagedMaintainanceComponentModel
    {
        public IEnumerable<MaintainanceComponent> MaintainanceComponent { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }


    public class ModelMaintainanceComponent
    {
        public IEnumerable<MaintainanceComponent> GetMaitainanceComponentPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.MaintainanceComponents
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountMaintainanceComponent()
        {
            return entities.MaintainanceComponents.Count();
        }

        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public static List<MaintainanceComponent> GetMaintainanceComponent()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<MaintainanceComponent> mc = new List<MaintainanceComponent>();

                mc = db.MaintainanceComponents
                    .ToList();
                return mc;
            }
        }
        public MaintainanceComponent GetMaintananceComponent(int mMaintananceComponentId)
        {
            return entities.MaintainanceComponents.Where(m => m.MaintainanceComponentId == mMaintananceComponentId).FirstOrDefault();
        }
        public static List<MaintainanceType> GetMaintainancetypes()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<MaintainanceType> mc = new List<MaintainanceType>();

                mc = db.MaintainanceTypes
                    .ToList();
                return mc;
            }
        }

        public ResponseModel CreateMaintananceComponent(MaintainanceComponent mMaintananceComponent)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.MaintainanceComponents.AddObject(mMaintananceComponent);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Maintanance part saved";// +mMaintananceComponent.ComponentName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateMaintananceComponent(MaintainanceComponent mMaintananceComponent)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    MaintainanceComponent data = entities.MaintainanceComponents.Where(m => m.MaintainanceComponentId == mMaintananceComponent.MaintainanceComponentId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mMaintananceComponent;

                        mMaintananceComponent = (MaintainanceComponent)entities.MaintainanceComponents.ApplyCurrentValues(mMaintananceComponent);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Component updated";// +mMaintananceComponent.ComponentName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Component " + mMaintananceComponent.ComponentName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteMaintananceComponent(int mMaintananceComponentID)
        {
            try
            {
                MaintainanceComponent data = entities.MaintainanceComponents.Where(m => m.MaintainanceComponentId == mMaintananceComponentID).FirstOrDefault();
                entities.MaintainanceComponents.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}