﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;


namespace GFleetV3.Models
{
    public class MaintainanceDetailView
    {
        public MaintainanceDetailView()
        {
            Main = new vw_Maintainance();
            Details = new List<vw_MaintainanceDetail>();
            MainTable = new Maintainance();
        }
        public vw_Maintainance Main { get; set; }
        public List<vw_MaintainanceDetail> Details { get; set; }
        public Maintainance MainTable { get; set; }

        public static MaintainanceDetailView MaintainanceDetailViewGet(int MaintainanceId)
        {
              using (GFleetModelContainer db = new GFleetModelContainer())
            {
                return new MaintainanceDetailView()
                {
                    Main = db.vw_Maintainance.SingleOrDefault(v => v.MaintainanceId == MaintainanceId),
                    Details = db.vw_MaintainanceDetail.Where(v => v.MaintainanceId == MaintainanceId).ToList<vw_MaintainanceDetail>(),
                    MainTable = db.Maintainances.SingleOrDefault(v => v.MaintainanceId == MaintainanceId)
                };
            }
        }
    }
    public class PagedMaintainanceModel
    {
        public IEnumerable<Maintainance> Maintainance { get; set; }
        public List<vw_MaintainanceDetail> Details { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class MaintainanceModel
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int DealerId = Service.Utility.getClientId().Value;

        public IEnumerable<Maintainance> GetMaitainancePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.Maintainances
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountMaintainance()
        {
            return entities.Maintainances.Count();
        }

        public static List<vw_Maintainance> GetMaintainance()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<vw_Maintainance> mc = new List<vw_Maintainance>();

                mc = db.vw_Maintainance
                    .ToList();
                return mc;
            }
        }
        public static List<vw_Maintainance> GetDealerMaintainances()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                int DealerId = Service.Utility.getClientId().Value;
                List<vw_Maintainance> mc = new List<vw_Maintainance>();

                mc = db.vw_Maintainance
                    .Where(u => u.DealerId == DealerId)
                    .ToList();
                return mc;
            }
        }
        public static List<vw_Maintainance> GetFilteredDealerMaintainance(string MaintainanceTypeid,
                         string MaintainanceStatusId, string VehicleId)
        {
            int MainTypeid = string.IsNullOrEmpty(MaintainanceTypeid) ? 0 : int.Parse(MaintainanceTypeid);
            int MainStatusId = string.IsNullOrEmpty(MaintainanceStatusId) ? 0 : int.Parse(MaintainanceStatusId);
            int VehId = string.IsNullOrEmpty(VehicleId) ? 0 : int.Parse(VehicleId);
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int dealerId = Service.Utility.getClientId().Value;
                var Report = from p in entities.vw_Maintainance.Where(v => v.DealerId == dealerId)
                             select p;
                if (MainTypeid > 0)
                {
                    Report = Report.Where(v => v.MaintainanceTypeId == MainTypeid);
                }
                if (MainStatusId > 0)
                {
                    Report = Report.Where(v => v.MaintainanceStatusId == MainStatusId);
                }
                if (VehId > 0)
                {
                    Report = Report.Where(v => v.VehicleId == VehId);
                }
            return Report.ToList<vw_Maintainance>(); 
            }
        }
        public Maintainance GetMaintainance(int mMaintainanceId)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                return db.Maintainances.Where(m => m.MaintainanceId == mMaintainanceId).FirstOrDefault();
            }

        }
        public static List<MaintainanceType> GetMaintainancetypes()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<MaintainanceType> mc = new List<MaintainanceType>();

                mc = db.MaintainanceTypes
                    .ToList();
                return mc;
            }
        }

        public ResponseModel CreateMaintainance(MaintainanceDetailView mMaintainance)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer db = new GFleetModelContainer())
                {
                    db.Maintainances.AddObject(mMaintainance.MainTable);
                    db.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    _rm.Status = true; _rm.Message = "Maintanance for vehicle created";// +mMaintainance.MainTable.Vehicle.RegistrationNo + " successfully created!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateMaintainance(MaintainanceDetailView mMaintainance)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Maintainance data = entities.Maintainances.Where(m => m.MaintainanceId == mMaintainance.MainTable.MaintainanceId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mMaintainance.MainTable;

                        mMaintainance.MainTable = (Maintainance)entities.Maintainances.ApplyCurrentValues(mMaintainance.MainTable);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Maintanance for vehicle" + mMaintainance.MainTable.Vehicle.RegistrationNo + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Vehicle" + mMaintainance.MainTable.Vehicle.RegistrationNo + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel AuthCompleteMaintainance(Maintainance mMaintainance)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Maintainance data = entities.Maintainances.Where(m => m.MaintainanceId == mMaintainance.MaintainanceId).FirstOrDefault();

                    if (data != null)
                    {
                            data.MaintainanceStatusId=mMaintainance.MaintainanceStatusId;
                        
                            bool IsComplete = false;
                            if (data.MaintainanceStatusId == 3)
                            {
                                IsComplete = true;
                            }
                            mMaintainance = (Maintainance)entities.Maintainances.ApplyCurrentValues(data);
                            entities.SaveChanges();
                            _rm.Status = true;
                            //_rm.Message = "Maintanance for vehicle " + mMaintainance.Vehicle.RegistrationNo + " successfully authrorized!";
                            _rm.Message = IsComplete ? "Maintanance for vehicle " + mMaintainance.Vehicle.RegistrationNo + " successfully completed!" : "Maintanance for vehicle " + mMaintainance.Vehicle.RegistrationNo + " successfully authrorized!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Vehicle" + mMaintainance.Vehicle.RegistrationNo + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteMaintainance(int mMaintainanceID)
        {
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Maintainance data = entities.Maintainances.Where(m => m.MaintainanceId == mMaintainanceID).FirstOrDefault();
                    entities.Maintainances.DeleteObject(data);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}