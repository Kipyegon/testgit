﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GFleetV3.Models
{
    public class MapModel
    {
        public Point StartAddress { get; set; }
        public Point EndAddress { get; set; }
    }

    public class Point
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}