﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Web.Security;
using DataAccess.SQL;
using System.Diagnostics.CodeAnalysis;
using System.Security.Principal;
using System.Web.Mvc.Properties;

namespace GFleetV3.Models
{
    public class Module
    {
        public string ModuleName { get; set; }
        public List<vw_ClientTypeController> Contoller { get; set; }

        public Module()
        {
            this.Contoller = new List<vw_ClientTypeController>();
        }

        //public static List<Module> getModules()
        //{
        //    object UserId = Service.Utility.getUserId();
        //    if (UserId != null)
        //    {
        //        List<Module> data = new List<Module>();

        //        IEnumerable<IGrouping<string, vw_ControllerView>> menus = MenuDa.getController(Roles.GetRolesForUser()).
        //                GroupBy(v => v.ModuleName);

        //        foreach (IGrouping<string, vw_ControllerView> _mainMenu in menus)
        //        {
        //            IEnumerable<IGrouping<string, vw_ControllerView>> _controllers = _mainMenu.Where(f => f.ParentId == null).
        //                GroupBy(v => v.ControllerName);

        //            Module mod = new Module()
        //            {
        //                ModuleName = _mainMenu.Key
        //                //,
        //                //Contoller = _mainMenu.Where(f => f.ParentId == null).GroupBy(v => v.ControllerName).
        //            };

        //            foreach (IGrouping<string, vw_ControllerView> _controller in _controllers)
        //            {
        //                mod.Contoller.Add(_controller.FirstOrDefault());
        //            }

        //            data.Add(mod);
        //        }
        //        return data;
        //    }
        //    return null;
        //}

        public static List<Module> getClientTypeModules()
        {
            string ClientTypeName = Service.Utility.getClientTypeName();
            if (ClientTypeName == null)
            {
                ClientTypeName = "Cab Company";
            }
            object UserId = Service.Utility.getUserId();
            if (UserId != null)
            {
                List<Module> data = new List<Module>();

                IEnumerable<IGrouping<string, vw_ClientTypeController>> menus = MenuDa.getClientTypeController(ClientTypeName).
                        GroupBy(v => v.ModuleName);

                foreach (IGrouping<string, vw_ClientTypeController> _mainMenu in menus)
                {
                    IEnumerable<IGrouping<string, vw_ClientTypeController>> _controllers = _mainMenu.Where(f => f.ParentId == null).
                        GroupBy(v => v.ControllerName);

                    Module mod = new Module()
                    {
                        ModuleName = _mainMenu.Key
                        //,
                        //Contoller = _mainMenu.Where(f => f.ParentId == null).GroupBy(v => v.ControllerName).
                    };

                    foreach (IGrouping<string, vw_ClientTypeController> _controller in _controllers)
                    {
                        mod.Contoller.Add(_controller.FirstOrDefault());
                    }

                    data.Add(mod);
                }
                return data;
            }
            return null;
        }
    }

    public class SideMenu
    {
        public static List<vw_ControllerView> getSubmenu(string controllerName)
        {
            return MenuDa.getSubController(controllerName);
        }
    }

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]

    public class AuthorizeEnumAttribute : AuthorizeAttribute
    {
        public AuthorizeEnumAttribute(string ControllerName, string ActionName)
        {
            //if (roles.Any(r => r.GetType().BaseType != typeof(Enum)))
            //    throw new ArgumentException("roles");

            //this.Roles = string.Join(",", roles.Select(r => Enum.GetName(r.GetType(), r)));
            this.Roles = getRoles(ControllerName, ActionName);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            IPrincipal user = httpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                return false;
            }
            string[] _rolesSplit = SplitString(Roles);
            if (!_rolesSplit.Any(user.IsInRole))
            {
                return false;
            }
            return base.AuthorizeCore(httpContext);
        }

        public static string getRoles(string ControllerName, string ActionName)
        {
            using (GFleetModelContainer gfleet = new GFleetModelContainer())
            {
                var query = from t in gfleet.vw_ControllerView
                            where t.ControllerName.Equals(ControllerName) && t.ActionName.Equals(ActionName) && t.IsEnabled.Equals(true)
                            group t by new
                            {
                                t.RoleName
                            } into g
                            orderby g.Key.RoleName
                            select new
                            {
                                RoleNames = (g.Key.RoleName)
                            };

                string result = string.Join(string.Empty, query.Select(r => r.RoleNames).ToArray());

                return string.IsNullOrEmpty(result) ? null : result;
            }
        }

        internal static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }
    }

    public class AuthorizationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var requestedController = filterContext.RouteData.GetRequiredString("controller");
            var requestedAction = filterContext.RouteData.GetRequiredString("action");

            string[] actionView = System.Configuration.ConfigurationManager.AppSettings["actionView"].ToString().Split(",".ToCharArray());

            if (actionView.Any(r => r.ToLower().Equals(requestedAction.ToString().ToLower())))
                requestedAction = "View";

            var operation = string.Format("/{0}/{1}", requestedController, requestedAction);

            string[] SystemRightsRoles = System.Configuration.ConfigurationManager.AppSettings["SystemRightsRoles"].ToString().Split(',');
            string SuperAdminPgs = System.Configuration.ConfigurationManager.AppSettings["SuperAdminPages"].ToString();

            string[] roles = Roles.GetRolesForUser();

            int? ClientId = Service.Utility.getClientId();
            foreach (string systemRightsRole in SystemRightsRoles)
            {
                //if (roles.Contains(systemRightsRole) && !ClientId.HasValue)
                //{
                //    string redirectUrl = System.Configuration.ConfigurationManager.AppSettings["unauthorisedsuperadmin"].ToString();

                //    string Message = string.Format("You are not authorized to perform operation: {0} kindly select the client before proceeding", operation);
                //    redirectUrl += "?Message=" + Message;
                //    filterContext.HttpContext.Response.Redirect(redirectUrl);
                //}
                //else
                    if (roles.Contains(systemRightsRole))
                {
                }
                else
                {
                    string roleNames = getRoles(requestedController, requestedAction);
                    string clientTypes = getClientTypes(requestedController, requestedAction);
                    string UserClientType = Service.Utility.getClientTypeName();

                    IPrincipal user = filterContext.HttpContext.User;

                    string[] _rolesSplit = SplitString(roleNames);
                    string[] _ClientTypeSplit = SplitString(clientTypes);

                    if (!_rolesSplit.Any(user.IsInRole) || !_ClientTypeSplit.Contains(UserClientType))
                    {
                        string redirectUrl = System.Configuration.ConfigurationManager.AppSettings["unauthorised"].ToString();

                        string Message = string.Format("You are not authorized to perform operation: {0}", operation);
                        redirectUrl += "?Message=" + Message;
                        filterContext.HttpContext.Response.Redirect(redirectUrl);
                    }
                }
            }


        }

        public static string getRoles(string ControllerName, string ActionName)
        {
            using (GFleetModelContainer gfleet = new GFleetModelContainer())
            {
                var query = from t in gfleet.vw_ControllerView
                            where t.ControllerName.Equals(ControllerName) && t.ActionName.Equals(ActionName) && t.IsEnabled.Equals(true)
                            group t by new
                            {
                                t.RoleName
                            } into g
                            orderby g.Key.RoleName
                            select new
                            {
                                RoleNames = (g.Key.RoleName)
                            };

                string result = string.Join(",", query.Select(r => r.RoleNames).ToArray());

                return string.IsNullOrEmpty(result) ? null : result;
            }
        }

        public static string getClientTypes(string ControllerName, string ActionName)
        {
            using (GFleetModelContainer gfleet = new GFleetModelContainer())
            {
                var query = from t in gfleet.vw_ClientTypeController
                            where t.ControllerName.Equals(ControllerName) && t.ActionName.Equals(ActionName) && t.IsEnabled.Equals(true)
                            group t by new
                            {
                                t.ClientTypeName
                            } into g
                            orderby g.Key.ClientTypeName
                            select new
                            {
                                ClientTypeName = (g.Key.ClientTypeName)
                            };

                string result = string.Join(",", query.Select(r => r.ClientTypeName).ToArray());

                return string.IsNullOrEmpty(result) ? null : result;
            }
        }

        internal static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }

    }
}