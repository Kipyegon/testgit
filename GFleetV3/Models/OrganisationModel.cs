﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Configuration;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Threading;

namespace GFleetV3.Models
{

    public class PagedOrganisationModel
    {
        public IEnumerable<Organisation> Organisation { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    //public class PagedOrganisationTypeModel
    //{
    //    public IEnumerable<OrganisationType> OrganisationType { get; set; }
    //    public int TotalRows { get; set; }
    //    public int PageSize { get; set; }
    //}

    public class ModelOrganisation : IDisposable
    {
        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public IEnumerable<Organisation> GetOrganisation()
        {
            return entities.Organisations.ToList();
        }

        //For Custom Paging

        public IEnumerable<Organisation> GetOrganisationPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;
            if (Roles.IsUserInRole("Super Admin"))
            {
                return entities.Organisations
                    //.OrderBy(orderCriteria)
              .OrderBy(v => v.OrganisationName)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            }
            else
                return entities.Organisations
                    //.OrderBy(orderCriteria)
                  .OrderBy(v => v.OrganisationName)
                  .Where(v => v.ClientId == ClientId)
                  .Skip((pageNumber - 1) * pageSize)
                  .Take(pageSize)
                  .ToList();
        }
        public int CountOrganisation()
        {
            return entities.Organisations.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Organisation GetOrganisation(int mOrganisationId)
        {
            return entities.Organisations.OrderBy(v => v.OrganisationName)
                .Where(m => m.OrganisationId == mOrganisationId).FirstOrDefault();
        }

        public ResponseModel CreateOrganisationAdminUser(OrganisationRegisterModel mOrganisationReg)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            SystemUser mSystemUser = mOrganisationReg.OrganisationAdminStaffModel;
            Organisation mOrganisation = mOrganisationReg.OrganisationModel;
            RegisterOrganisationAdminModel mRegister = mOrganisationReg.RegisterModel;
            try
            {

                if (!entities.Organisations.Any(v => v.OrganisationName.Equals(mOrganisation.OrganisationName) && v.OrganisationEmail.Equals(mOrganisation.OrganisationEmail)))
                {

                    //if (!string.IsNullOrEmpty(mRegister.UserRole.RoleName))
                    //{

                    pmp = Membership.Provider;
                    if (pmp.GetUser(mRegister.Email, false) == null)
                    {
                        MembershipCreateStatus createStatus = new MembershipCreateStatus();
                        MembershipUser pms = pmp.CreateUser(mRegister.Email, mRegister.Password, mRegister.Email, "Question",
                                            "Answer", true, null, out createStatus);
                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            Roles.AddUserToRole(mRegister.Email, "Organisation Admin");
                            if (mSystemUser == null)
                            {
                                mSystemUser = new SystemUser()
                                {
                                    ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                    DateModified = DateTime.Now,
                                    Organisation = null,
                                    UserId = Guid.Parse(pms.ProviderUserKey.ToString()),
                                    ClientId = Service.Utility.getClientId().Value
                                };
                            }
                            else
                            {
                                mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                mSystemUser.DateModified = DateTime.Now;
                                mSystemUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                                mSystemUser.ClientId = Service.Utility.getClientId().Value;
                            }
                            string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

                            string BrandName = Service.Utility.getClientBrandName();
                            string RepytoAddress = Service.Utility.getReplyToAddress();
                            RepytoAddress = RepytoAddress + "," + Membership.GetUser();

                            emailBody = emailBody.Replace("[newline]", "</br>");

                            string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
                            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                            string taksiaddress = BrandName + System.Configuration.ConfigurationManager.AppSettings["taksihostname"].ToString();

                            //string taksiaddress = System.Configuration.ConfigurationManager.AppSettings["taksiaddress"].ToString();

                            if (url.Contains(AccessUrls.ToString()))
                            {
                                emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, taksiaddress);
                            }
                            else
                            {
                                emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, taksiaddress);
                            }
                            string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();

                            //Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                            EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
                            if (ET != null)
                            {
                                if (url.Contains(AccessUrls.ToString()))
                                {
                                    emailBody = String.Format(ET.Body, mRegister.Email, mRegister.Password, taksiaddress);
                                }
                                else
                                {
                                    emailBody = String.Format(ET.Body, mRegister.Email, mRegister.Password, taksiaddress);
                                }
                                emailBody = ET.Header + emailBody + ET.Footer;
                            }

                            new Thread(() =>
                            {
                                try
                                {
                                    Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody, RepytoAddress);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    //throw new Exception(ex.Message.ToString());
                                }
                            }).Start();
                        }
                        else
                        {
                            _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                            return _rm;
                        }

                        mOrganisation.DateModified = DateTime.Now;
                        mOrganisation.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        mOrganisation.ClientId = Service.Utility.getClientId().Value;

                        mSystemUser.Organisation = mOrganisation;
                        entities.People.AddObject(mSystemUser);
                        entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        _rm.Status = true; _rm.Message = "Organisation saved";// +mOrganisation.OrganisationName + " successfully saved!";
                        return _rm;
                    }
                    else
                    {
                        _rm.Message = "user already exists"; _rm.Status = false;
                        return _rm;
                    }
                    //}
                    //else
                    //{
                    //    _rm.Message = "No Role name specified"; _rm.Status = false;
                    //    return _rm;
                    //}
                }
                else
                {
                    _rm.Message = "The Organisation already exists"; _rm.Status = false;
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel CreateOrganisationUser(OrganisationRegisterModel mOrganisationReg, HttpPostedFileBase File, bool sendSms, bool sendEmail)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            SystemUser mSystemUser = mOrganisationReg.OrganisationAdminStaffModel;
            Customer mCustEndUser = mOrganisationReg.OrganisationEndUserModel;
            RegisterEndUserPhoneModel mRegister = mOrganisationReg.RegisterEndUserModel;
            try
            {

                if (!entities.People.OfType<Customer>().Any(v => v.Telephone1.Equals(mCustEndUser.Telephone1) && v.Email1.Equals(mCustEndUser.Email1) && v.ClientId == ClientId && v.IsDelete == false))
                {

                    pmp = Membership.Providers["MembershipProviderOther"]; //.Provider;
                    if (pmp.GetUser(mRegister.Username, false) == null)
                    {
                        string username = mRegister.Username.TrimStart('+');
                        string password = Service.Utility.GeneratePass().ToString();

                        MembershipCreateStatus createStatus = new MembershipCreateStatus();
                        MembershipUser pms = pmp.CreateUser(username, password, mCustEndUser.Email1, "Question",
                                            "Answer", true, null, out createStatus);
                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            Roles.AddUserToRole(username, "End User");

                            if (mCustEndUser == null)
                            {
                                mCustEndUser = new Customer()
                                {
                                    ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                    DateModified = DateTime.Now,
                                    Organisation = null,
                                    UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                                };
                            }
                            else
                            {
                                mCustEndUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                mCustEndUser.DateModified = DateTime.Now;
                                mCustEndUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                                //mCustEndUser.OrganisationId = Service.Utility.getOrganisationId().Value;
                            }

                        }
                        else
                        {
                            _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                            return _rm;
                        }
                        string Filename = ""; string filepath = "";
                        try
                        {
                            if (File != null)
                            {
                                try
                                {
                                    string CustomerDir = WebConfigurationManager.AppSettings["CustomerDir"];
                                    filepath = System.Web.HttpContext.Current.Server.MapPath(CustomerDir);
                                    Filename = "Client_" + mCustEndUser.Telephone1 + "_map" + Path.GetExtension(File.FileName);

                                    mCustEndUser.CustomerDirUrl = Filename;

                                    File.SaveAs(Path.Combine(filepath, Filename));
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    mCustEndUser.CustomerDirUrl = null;
                                }
                            }
                            mCustEndUser.DateModified = DateTime.Now;
                            mCustEndUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                            mCustEndUser.ClientId = Service.Utility.getClientId().Value;

                            try
                            {
                                entities.People.AddObject(mCustEndUser);
                                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);

                                string BrandName = Service.Utility.getClientBrandName();
                                string RepytoAddress = Service.Utility.getReplyToAddress();
                                RepytoAddress = RepytoAddress + "," + Membership.GetUser();

                                string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

                                emailBody = emailBody.Replace("[newline]", "</br>");

                                string solnaddress = BrandName + System.Configuration.ConfigurationManager.AppSettings["taksihostname"].ToString();

                                emailBody = String.Format(emailBody, mRegister.Username.TrimStart('+'), password, solnaddress);

                                string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();

                                string smsBody = "Welcome to Taksi, Your Username is {0} and Password is {1}. Kindly Check your email for further Instructions";

                                smsBody = String.Format(smsBody, mRegister.Username.TrimStart('+'), password);

                                EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
                                if (ET != null)
                                {
                                    emailBody = String.Format(ET.Body, mRegister.Username.TrimStart('+'), password, solnaddress);
                                    emailBody = ET.Header + emailBody + ET.Footer;
                                }
                                if (sendEmail)
                                {
                                    new Thread(() =>
                                    {
                                        try
                                        {
                                            Service.SendEmail.SendUserMngtEmail(mCustEndUser.Email1, emailSubject, emailBody, RepytoAddress);
                                        }
                                        catch (Exception mex)
                                        {
                                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                            //throw new Exception(ex.Message.ToString());
                                        }
                                    }).Start();
                                }
                                //Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                                if (sendSms)
                                {
                                    {
                                        new Thread(() =>
                                        {
                                            try
                                            {
                                                GFleetV3SMSProcessor.SendSMS.SendMessage(mRegister.Username, smsBody, BrandName);
                                            }
                                            catch (Exception mex)
                                            {
                                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                                //throw new Exception(ex.Message.ToString());
                                            }
                                        }).Start();
                                    }
                                    ////string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();
                                    ////ModelCustomer.sendEnduserCreateSMSEmail(mCustEndUser.Email1, mRegister.Username, password, emailBody, "EndUserRegistration");
                                    _rm.Status = true; _rm.Message = "Customer saved";// +mCustEndUser.FullNames + " successfully saved!";
                                }
                            }
                            catch (Exception ex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                                if (createStatus == MembershipCreateStatus.Success)
                                {
                                    pmp.DeleteUser(username, true);
                                    _rm.Status = false; _rm.Message = "User not created.";

                                }
                            }
                            return _rm;
                        }
                        catch (Exception mex)
                        {
                            if (File != null)
                            {
                                try
                                {
                                    System.IO.File.Delete(filepath + Filename);
                                }
                                catch (Exception ex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);

                                }
                            }
                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                            _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                            return _rm;
                        }
                    }

                    else
                    {
                        _rm.Message = "user already exists"; _rm.Status = false;
                        return _rm;
                    }
                }
                else
                {
                    _rm.Message = "The User already exists"; _rm.Status = false;
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    pmp.DeleteUser(mRegister.Username, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateOrganisation(Organisation mOrganisation)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Organisation data = entities.Organisations.Where(m => m.OrganisationId == mOrganisation.OrganisationId).FirstOrDefault();
                    data = mOrganisation;

                    mOrganisation = (Organisation)entities.Organisations.ApplyCurrentValues(mOrganisation);
                    entities.SaveChanges();
                    _rm.Status = true; _rm.Message = "Organisation updated";// +mOrganisation.OrganisationName + " successfully updated!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateOrgAdminAccount(OrganisationRegisterModel mOrganisationReg)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            SystemUser mSystemUser = mOrganisationReg.OrganisationAdminStaffModel;
            Organisation mOrganisation = mOrganisationReg.OrganisationModel;
            if (mOrganisationReg.RegisterModel != null)
            {
                string username = mOrganisationReg.RegisterModel.Email;
                try
                {
                    pmp = Membership.Provider;
                    if (pmp.GetUser(username, false) == null)
                    {
                        string password = mOrganisationReg.RegisterModel.Password;

                        MembershipCreateStatus createStatus = new MembershipCreateStatus();
                        MembershipUser pms = pmp.CreateUser(username, password, username, "Question",
                                            "Answer", true, null, out createStatus);
                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            Roles.AddUserToRole(username, "Organisation Admin");

                            if (mSystemUser == null)
                            {
                                mSystemUser = new SystemUser()
                                {
                                    ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                    DateModified = DateTime.Now,
                                    Organisation = null,
                                    OrganisationId = mOrganisationReg.OrganisationModel.OrganisationId,
                                    UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                                };
                            }
                            else
                            {
                                mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                mSystemUser.DateModified = DateTime.Now;
                                mSystemUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                            }

                            entities.People.AddObject(mSystemUser);
                            entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);

                            string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

                            emailBody = emailBody.Replace("[newline]", "</br>");

                            string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
                            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                            string newportsolnaddress = System.Configuration.ConfigurationManager.AppSettings["taksiaddress"].ToString();
                            string taksiaddress = System.Configuration.ConfigurationManager.AppSettings["taksiaddress"].ToString();

                            if (url.Contains(AccessUrls.ToString()))
                            {
                                emailBody = String.Format(emailBody, mOrganisationReg.RegisterModel.Email, password, newportsolnaddress);
                            }
                            else
                            {
                                emailBody = String.Format(emailBody, mOrganisationReg.RegisterModel.Email, password, taksiaddress);
                            }
                            EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
                            if (ET != null)
                            {
                                if (url.Contains(AccessUrls.ToString()))
                                {
                                    emailBody = String.Format(ET.Body, mOrganisationReg.RegisterModel.Email, password, newportsolnaddress);
                                }
                                else
                                {
                                    emailBody = String.Format(ET.Body, mOrganisationReg.RegisterModel.Email, password, taksiaddress);
                                }
                                emailBody = ET.Header + emailBody + ET.Footer;
                            }
                            string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();

                            new Thread(() =>
                            {
                                try
                                {
                                    Service.SendEmail.SendUserMngtEmail(mOrganisationReg.RegisterModel.Email, emailSubject, emailBody);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                }
                            }).Start();
                        }
                        else
                        {
                            _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                            return _rm;
                        }
                        _rm = UpdateOrganisation(mOrganisation);
                        return _rm;
                    }
                    else
                    {
                        _rm.Message = "user already exists"; _rm.Status = false;
                        return _rm;
                    }

                }
                catch (Exception mex)
                {
                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    if (pmp != null)
                    {
                        pmp.DeleteUser(username, true);
                    }

                    _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                    return _rm;
                }
            }
            else
            {
                _rm = UpdateOrganisation(mOrganisation);
                return _rm;
            }
        }

        public ResponseModel DeleteOrganisation(int OrganisationId)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                int ClientId = Service.Utility.getClientId().Value;
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    List<Person> ps = entities.People.Where(v => v.OrganisationId == OrganisationId && v.ClientId == ClientId).ToList<Person>();
                    MembershipProvider pmp = null;
                    pmp = Membership.Provider;

                    foreach (Person p in ps)
                    {
                        try
                        {
                            string username = pmp.GetUser(p.UserId, false).UserName;
                            entities.DeleteObject(p);
                            entities.SaveChanges();
                            pmp.DeleteUser(username, true);
                        }
                        catch (Exception mex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                            _rm.Message = "User does not have an account kindly contact :support@geeckoltd.com for assistance"; _rm.Status = false;
                            return _rm;
                        }
                    }

                    Organisation or = entities.Organisations.SingleOrDefault(v => v.OrganisationId == OrganisationId && v.ClientId == ClientId);
                    if (or != null)
                    {
                        entities.DeleteObject(or);
                    }

                    entities.SaveChanges();
                    _rm.Message = "Organisation Successfully deleted"; _rm.Status = false;
                    return _rm;

                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Message = "Error encountered kindly contact :support@geeckoltd.com"; _rm.Status = false;
                return _rm;
            }
        }

        public static Boolean orgHasAdmin(int id)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return entities.People.OfType<SystemUser>().Any(p => p.OrganisationId == id);
            }
        }

        public static ResponseModel ResetPassword(string Username)
        {
            ResponseModel response = new ResponseModel();
            string loogedEmail = Membership.GetUser().Email;
            string BrandName = Service.Utility.getClientBrandName();
            string RepytoAddress = Service.Utility.getReplyToAddress();
            RepytoAddress = RepytoAddress + "," + Membership.GetUser();
            try
            {
                //MembershipProvider pmp = Membership.Provider.GetUser(Username.TrimStart('+'), true);
                MembershipUser user = ModelOrganisation.getUser(Username.TrimStart('+'));
                if (user != null)
                {
                    using (GFleetModelContainer db = new GFleetModelContainer())
                    {

                        user.UnlockUser();
                        Person contact = db.People.FirstOrDefault(u => u.Telephone1 == Username);
                        string reset = user.ResetPassword();
                        string newpass = Service.PasswordGenerator.RandomNumber().ToString();
                        user.ChangePassword(reset, newpass);
                        string emailBody = "Dear User! </br> </br>";
                        emailBody += "Your New Credentials are as below: </br>";
                        emailBody += "Username: {0} </br>";
                        emailBody += "Password: {1} </br> </br>";
                        emailBody += "Kindly logon in to the taksi phone application  </br>";
                        emailBody += "For any further queries please do not hesitate to make contact. </br> </br>";
                        emailBody += "Regards. </br>";
                        emailBody += "Taksi Team. </br>";
                        emailBody += "support@geeckoltd.com";


                        emailBody = String.Format(emailBody, Username.TrimStart('+'), newpass);

                        string emailSubject = "Taksi Password Reset.";


                        string smsBody = "Taksi: Your Password has been successfully reset to {0}. Check your email for more details";
                        smsBody = String.Format(smsBody, newpass);

                        new Thread(() =>
                        {
                            try
                            {
                                GFleetV3SMSProcessor.SendSMS.SendMessage(contact.Telephone1, smsBody, BrandName);
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();

                        new Thread(() =>
                        {
                            try
                            {
                                Service.SendEmail.SendUserMngtEmail(contact.Email1, emailSubject, emailBody, "", RepytoAddress);
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();

                        response.Status = true;
                        response.Message = "Password Reset Successful";
                    }
                }
                else
                {
                    response.Status = false;
                    response.Message = "Unknown User";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                throw new Exception(mex.InnerException == null ? mex.Message.ToString() : mex.InnerException.Message);
                //response.Status = false;
                //response.Message = "An Error Was Encountered";
            }
            return response;
        }
        public static ResponseModel ResetDriverPassword(string Username)
        {
            ResponseModel response = new ResponseModel();
            string loogedEmail = Membership.GetUser().Email;
            string BrandName = Service.Utility.getClientBrandName();
            string RepytoAddress = Service.Utility.getReplyToAddress();
            RepytoAddress = RepytoAddress + "," + Membership.GetUser();
            try
            {
                //MembershipProvider pmp = Membership.Provider.GetUser(Username.TrimStart('+'), true);
                MembershipUser user = ModelOrganisation.getUser(Username.TrimStart('+'));
                if (user != null)
                {
                    using (GFleetModelContainer db = new GFleetModelContainer())
                    {

                        user.UnlockUser();
                        var UserId = user.ProviderUserKey.ToString();
                        var Phone = user.UserName;
                        var contact = DriverRegisterModel.getDriver(UserId);
                        //Person contact = db.getUserProfile(People.SingleOrDefault(u => u.Telephone1 == Username);
                        string reset = user.ResetPassword();
                        string newpass = Service.PasswordGenerator.RandomNumber().ToString();
                        user.ChangePassword(reset, newpass);
                        string emailBody = "Dear User! </br> </br>";
                        emailBody += "Your New Credentials are as below: </br>";
                        emailBody += "Username: {0} </br>";
                        emailBody += "Password: {1} </br> </br>";
                        emailBody += "Kindly logon in to the taksi phone application  </br>";
                        emailBody += "For any further queries please do not hesitate to make contact. </br> </br>";
                        emailBody += "Regards. </br>";
                        emailBody += "Taksi Team. </br>";
                        emailBody += "support@geeckoltd.com";


                        emailBody = String.Format(emailBody, Username.TrimStart('+'), newpass);

                        string emailSubject = "Taksi Password Reset.";


                        string smsBody = "Taksi: Your Password has been successfully reset to {0}. Check your email for more details";
                        smsBody = String.Format(smsBody, newpass);

                        new Thread(() =>
                        {
                            try
                            {
                                GFleetV3SMSProcessor.SendSMS.SendMessage(Phone, smsBody, BrandName);
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();
                        if (contact.DriverModel.Email1 != null)
                        {
                            new Thread(() =>
                            {
                                try
                                {
                                    Service.SendEmail.SendUserMngtEmail(contact.DriverModel.Email1, emailSubject, emailBody, "", RepytoAddress);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    //throw new Exception(ex.Message.ToString());
                                }
                            }).Start();
                        }
                        response.Status = true;
                        response.Message = "Password Reset Successful";
                    }
                }
                else
                {
                    response.Status = false;
                    response.Message = "Unknown User";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                throw new Exception(mex.InnerException == null ? mex.Message.ToString() : mex.InnerException.Message);
                //response.Status = false;
                //response.Message = "An Error Was Encountered";
            }
            return response;
        }

        private static MembershipUser getUser(string emailName)
        {
            MembershipUser currentUser = Membership.Providers["MembershipProviderOther"].GetUser(emailName, false);

            if (currentUser == null)
            {
                string userName = Membership.Providers["MembershipProviderOther"].GetUserNameByEmail(emailName);
                if (userName != null)
                {
                    currentUser = Membership.Providers["MembershipProviderOther"].GetUser(userName, false);
                }
            }

            return currentUser;
        }

        public ResponseModel CreateUser(RegisterOrganisationAdminModel mRegister, Organisation Organisation)
        {
            MembershipProvider pmp = Membership.Provider;
            ResponseModel _rm = new ResponseModel();
            SystemUser mSystemUser;
            if (pmp.GetUser(mRegister.Email, false) == null)
            {
                MembershipCreateStatus createStatus = new MembershipCreateStatus();
                MembershipUser pms = pmp.CreateUser(mRegister.Email, mRegister.Password, mRegister.Email, "Question",
                                    "Answer", true, null, out createStatus);
                if (createStatus == MembershipCreateStatus.Success)
                {
                    Roles.AddUserToRole(mRegister.Email, "Organisation Admin");
                    mSystemUser = new SystemUser()
                   {
                       ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                       DateModified = DateTime.Now,
                       Organisation = null,
                       UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                   };

                    string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

                    emailBody = emailBody.Replace("[newline]", "</br>");

                    string[] AccessUrls = System.Configuration.ConfigurationManager.AppSettings["UrlAccessSys"].ToString().Split(',');
                    string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

                    string newportsolnaddress = System.Configuration.ConfigurationManager.AppSettings["taksiaddress"].ToString();
                    string taksiaddress = System.Configuration.ConfigurationManager.AppSettings["taksiaddress"].ToString();

                    if (url.Contains(AccessUrls.ToString()))
                    {
                        emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, newportsolnaddress);
                    }
                    else
                    {
                        emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, taksiaddress);
                    }
                    string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();

                    Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                    EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
                    if (ET != null)
                    {
                        if (url.Contains(AccessUrls.ToString()))
                        {
                            emailBody = String.Format(ET.Body, mRegister.Email, mRegister.Password, newportsolnaddress);
                        }
                        else
                        {
                            emailBody = String.Format(ET.Body, mRegister.Email, mRegister.Password, taksiaddress);
                        }
                        emailBody = ET.Header + emailBody + ET.Footer;
                    }

                    new Thread(() =>
                    {
                        try
                        {
                            Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                        }
                        catch (Exception mex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                            //throw new Exception(ex.Message.ToString());
                        }
                    }).Start();
                }
                else
                {
                    _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                    return _rm;
                }

                entities.People.AddObject(mSystemUser);

                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Organisation " + Organisation.OrganisationName + " successfully saved!";
                return _rm;
            }
            else
            {
                _rm.Message = "user already exists"; _rm.Status = false;
                return _rm;
            }
        }

    }
    public class OrganisationRegisterModel
    {
        public RegisterOrganisationAdminModel RegisterModel { get; set; }
        public RegisterEndUserPhoneModel RegisterEndUserModel { get; set; }
        public Organisation OrganisationModel { get; set; }
        public Customer OrganisationEndUserModel { get; set; }
        //public Driver OrganisationEndUserModel { get; set; }
        public SystemUser OrganisationAdminStaffModel { get; set; }

        public static OrganisationRegisterModel getOrganisationRegisterModel(int OrganisationId)
        {
            OrganisationRegisterModel crm = new OrganisationRegisterModel();
            crm.OrganisationModel = new ModelOrganisation().GetOrganisation(OrganisationId);

            return crm;
        }
        public static OrganisationRegisterModel getEndUser(int CustomerId)
        {
            OrganisationRegisterModel crm = new OrganisationRegisterModel();
            crm.OrganisationEndUserModel = new ModelCustomer().GetCustomer(CustomerId);

            return crm;
        }
    }
    //public class ModelOrganisationType : IDisposable
    //{
    //    private readonly GFleetModelContainer entities = new GFleetModelContainer();

    //    private readonly int ClientId = Service.Utility.getClientId().Value;
    //    private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

    //    public IEnumerable<OrganisationType> GetOrganisationType()
    //    {
    //        return entities.OrganisationTypes.Where(v => v.ClientId == ClientId).ToList();
    //    }

    //    //For Custom Paging

    //    public IEnumerable<OrganisationType> GetOrganisationTypePage(int pageNumber, int pageSize, string orderCriteria)
    //    {
    //        if (pageNumber < 1)
    //            pageNumber = 1;

    //        return entities.OrganisationTypes
    //          .OrderBy(orderCriteria)
    //          .Where(v => v.ClientId == ClientId)
    //          .Skip((pageNumber - 1) * pageSize)
    //          .Take(pageSize)
    //          .ToList();
    //    }
    //    public int CountOrganisationType()
    //    {
    //        return entities.OrganisationTypes.Where(v => v.ClientId == ClientId).Count();
    //    }

    //    public void Dispose()
    //    {
    //        entities.Dispose();
    //    }

    //    //For Edit 
    //    public OrganisationType GetOrganisationType(int mOrganisationTypeId)
    //    {
    //        return entities.OrganisationTypes.Where(m => m.OrganisationTypeId == mOrganisationTypeId && m.ClientId == ClientId).FirstOrDefault();
    //    }


    //    public ResponseModel CreateOrganisationType(OrganisationType mOrganisationType)
    //    {
    //        ResponseModel _rm = new ResponseModel();
    //        try
    //        {

    //            entities.OrganisationTypes.AddObject(mOrganisationType);
    //            entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
    //            _rm.Status = true;
    //            _rm.Message = "Organisation Type saved"; // +mOrganisationType.OrganisationTypeName + " successfully saved!";
    //            _rm.Data = new
    //            {
    //                OrganisationTypeName = mOrganisationType.OrganisationTypeName,
    //                Description = mOrganisationType.Description,
    //                Id = mOrganisationType.OrganisationTypeId
    //            };
    //            return _rm;
    //        }
    //        catch (Exception mex)
    //        {
    //            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
    //            _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
    //            return _rm;
    //        }
    //    }

    //    public ResponseModel UpdateOrganisationType(OrganisationType mOrganisationType)
    //    {
    //        ResponseModel _rm = new ResponseModel() { IsUpdate = true };
    //        try
    //        {
    //            using (GFleetModelContainer entities = new GFleetModelContainer())
    //            {
    //                OrganisationType data = entities.OrganisationTypes.Where(m => m.OrganisationTypeId == mOrganisationType.OrganisationTypeId && m.ClientId == ClientId).FirstOrDefault();

    //                if (data != null)
    //                {
    //                    data = mOrganisationType;

    //                    mOrganisationType = (OrganisationType)entities.OrganisationTypes.ApplyCurrentValues(mOrganisationType);
    //                    entities.SaveChanges();
    //                    _rm.Status = true; _rm.Message = "Organisation Type updated";// +mOrganisationType.OrganisationTypeName + " successfully updated!";
    //                }
    //                else
    //                {
    //                    _rm.Status = false; _rm.Message = "Organisation Type " + mOrganisationType.OrganisationTypeName + " is not in you company!";
    //                }
    //                return _rm;
    //            }
    //        }
    //        catch (Exception mex)
    //        {
    //            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
    //            _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
    //            return _rm;
    //        }
    //    }

    //    public bool DeleteOrganisationType(int mOrganisationTypeID)
    //    {
    //        try
    //        {
    //            OrganisationType data = entities.OrganisationTypes.Where(m => m.OrganisationTypeId == mOrganisationTypeID && m.ClientId == ClientId).FirstOrDefault();
    //            entities.OrganisationTypes.DeleteObject(data);
    //            entities.SaveChanges();
    //            return true;
    //        }
    //        catch (Exception mex)
    //        {
    //            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
    //            return false;
    //        }
    //    }
    //}

    //public class ModelOrganisation : IDisposable
    //{
    //    private readonly GFleetModelContainer entities = new GFleetModelContainer();

    //    private readonly int OrganisationId = Service.Utility.getClientId().Value;
    //    private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

    //    public IEnumerable<Organisation> GetOrganisation()
    //    {
    //        return entities.Organisations.Where(v => v.ClientId == ClientId).ToList();
    //    }

    //    public IEnumerable<object> GetOrganisationDetails()
    //    {
    //        ResponseModel _rm = new ResponseModel();
    //        return entities.Organisations.Where(b => b.ClientId == ClientId).Select(b => new
    //        {
    //            OrganisationId = b.OrganisationId,
    //            OrganisationName = b.OrganisationName
    //        }).ToList();
    //    }

    //    //For Custom Paging

    //    public IEnumerable<Organisation> GetOrganisationPage(int pageNumber, int pageSize, string orderCriteria)
    //    {
    //        if (pageNumber < 1)
    //            pageNumber = 1;

    //        return entities.Organisations
    //          .OrderBy(orderCriteria)
    //          .Where(v => v.ClientId == ClientId)
    //          .Skip((pageNumber - 1) * pageSize)
    //          .Take(pageSize)
    //          .ToList();
    //    }
    //    public int CountOrganisation()
    //    {
    //        return entities.Organisations.Where(v => v.ClientId == ClientId).Count();
    //    }

    //    public void Dispose()
    //    {
    //        entities.Dispose();
    //    }

    //    //For Edit 
    //    public Organisation GetOrganisation(int mOrganisationId)
    //    {
    //        return entities.Organisations.Where(m => m.OrganisationId == mOrganisationId && m.ClientId == ClientId).FirstOrDefault();
    //    }

    //    public ResponseModel CreateOrganisation(Organisation mOrganisation)
    //    {
    //        ResponseModel _rm = new ResponseModel();
    //        try
    //        {

    //            entities.Organisations.AddObject(mOrganisation);
    //            entities.SaveChanges(System.Data.Entity.Core.Objects.SaveOptions.DetectChangesBeforeSave);
    //            _rm.Status = true; _rm.Message = "Organisation " + mOrganisation.OrganisationName + " successfully saved!";
    //            return _rm;
    //        }
    //        catch (Exception mex)
    //        {
    //            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
    //            _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
    //            return _rm;
    //        }
    //    }

    //    public ResponseModel UpdateOrganisation(Organisation mOrganisation)
    //    {
    //        ResponseModel _rm = new ResponseModel() { IsUpdate = true };
    //        try
    //        {
    //            using (GFleetModelContainer entities = new GFleetModelContainer())
    //            {
    //                Organisation data = entities.Organisations.Where(m => m.OrganisationId == mOrganisation.OrganisationId && m.ClientId == ClientId).FirstOrDefault();

    //                if (data != null)
    //                {
    //                    data = mOrganisation;

    //                    mOrganisation = (Organisation)entities.Organisations.ApplyCurrentValues(mOrganisation);
    //                    entities.SaveChanges();
    //                    _rm.Status = true; _rm.Message = "Organisation " + mOrganisation.OrganisationName + " successfully updated!";
    //                }
    //                else
    //                {
    //                    _rm.Status = true; _rm.Message = "Organisation " + mOrganisation.OrganisationName + " not in your company!";
    //                }
    //                return _rm;
    //            }
    //        }
    //        catch (Exception mex)
    //        {
    //            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
    //            _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
    //            return _rm;
    //        }
    //    }

    //    public bool DeleteOrganisation(int mOrganisationID)
    //    {
    //        try
    //        {
    //            Organisation data = entities.Organisations.Where(m => m.OrganisationId == mOrganisationID && m.ClientId == ClientId).FirstOrDefault();
    //            entities.Organisations.DeleteObject(data);
    //            entities.SaveChanges();
    //            return true;
    //        }
    //        catch (Exception mex)
    //        {
    //            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
    //            return false;
    //        }
    //    }
    //}
}