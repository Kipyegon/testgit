﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class PagedOrganisationTypeModel
    {
        public IEnumerable<OrganisationType> OrganisationType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }


    public class ModelOrganisationType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<OrganisationType> GetOrganisationType()
        {
            return entities.OrganisationTypes.ToList();
        }

        //For Custom Paging

        public IEnumerable<OrganisationType> GetOrganisationTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
           // int ClientId = Service.Utility.getClientId().Value;
            if (pageNumber < 1)
                pageNumber = 1;


            return entities.OrganisationTypes
              .OrderBy(orderCriteria)
              //.Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountOrganisationType()
        {
            return entities.OrganisationTypes.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }
        //For Edit 
        public OrganisationType GetOrganisationType(int mOrganisationTypeId)
        {
            return entities.OrganisationTypes.Where(m => m.OrganisationTypeId == mOrganisationTypeId).FirstOrDefault();
        }


        public ResponseModel CreateOrganisationType(OrganisationType mOrganisationType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                mOrganisationType.OrganisationTypeId = mOrganisationType.OrganisationTypeId;

                entities.OrganisationTypes.AddObject(mOrganisationType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Organisation Type saved";//with SerialNo" + mDevice.SerialNo + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateOrganisationType(OrganisationType mOrganisationType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    OrganisationType data = entities.OrganisationTypes.Where(m => m.OrganisationTypeId == mOrganisationType.OrganisationTypeId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mOrganisationType;

                        mOrganisationType = (OrganisationType)entities.OrganisationTypes.ApplyCurrentValues(mOrganisationType);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Organisation Type Successfully Updated ";//with SerialNo" + mDevice.SerialNo + " successfully updated!";
                    }

                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteOrganisationType(int mOrganisationTypeID)
        {
            try
            {
                OrganisationType data = entities.OrganisationTypes.Where(m => m.OrganisationTypeId == mOrganisationTypeID).FirstOrDefault();
                entities.OrganisationTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}