﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class PagedPriceModel
    {
        public IEnumerable<Price> Price { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    public class ModelPrice : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Price> GetPrice()
        {
            return entities.Prices.Where(v => v.ClientId == ClientId).ToList();
        }

        public IEnumerable<object> GetPriceDetails()
        {
            ResponseModel _rm = new ResponseModel();
            return entities.Prices.Where(b => b.ClientId == ClientId).Select(b => new
            {
                PriceId = b.PriceId,
                PriceName = b.PriceName
            }).ToList();
        }

        //For Custom Paging

        public IEnumerable<Price> GetPricePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.Prices
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }

        public int CountPrice()
        {
            return entities.Prices.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Price GetPrice(int mPriceId)
        {
            return entities.Prices.Where(m => m.PriceId == mPriceId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreatePrice(Price mPrice)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.Prices.AddObject(mPrice);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Price saved";// +mPrice.PriceName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdatePrice(Price mPrice)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Price data = entities.Prices.Where(m => m.PriceId == mPrice.PriceId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mPrice;

                        mPrice = (Price)entities.Prices.ApplyCurrentValues(mPrice);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Price updated";// +mPrice.PriceName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Price " + mPrice.PriceName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeletePrice(int mPriceID)
        {
            try
            {
                Price data = entities.Prices.Where(m => m.PriceId == mPriceID && m.ClientId == ClientId).FirstOrDefault();
                entities.Prices.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}