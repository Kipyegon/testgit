﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GFleetV3.Models
{
    class ProfileCommon
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
