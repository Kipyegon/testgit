﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class RecoverModel
    {
        public static List<getWebRecoverData_V3_Result> getWebRecoverData(int recover, int lost)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                return entities.getWebRecoverData_V3(clientId, recover, lost).ToList<getWebRecoverData_V3_Result>();
            }
        }

        public static List<getLatestRecoverData_V3_Result> getLatestRecoverData(DateTime highestdate, int recover, int lost)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                return entities.getLatestRecoverData_V3(clientId, highestdate, recover, lost).ToList<getLatestRecoverData_V3_Result>();
            }
        }

        public static object getLostVehicle(int recover)
        {

            int clientId = Service.Utility.getClientId().Value;

            var vehicleQuery = from d in new GFleetModelContainer().vw_VehicleDetailsV3
                               where d.ClientId == clientId && d.VehicleId != recover
                               orderby d.VehicleRegCode ascending
                               select new { d.VehicleRegCode, d.VehicleId };
            return vehicleQuery;
        }
    }
}