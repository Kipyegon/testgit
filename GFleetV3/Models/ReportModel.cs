﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class DriverStatusReport
    {
        public IEnumerable<vw_DriverStatus> vw_DriverStatus { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class FuelData
    {  
        public double? AvgFuelCons { get; set; }
        public int ClientId { get; set; }
        public decimal? Cost { get; set; }
        public decimal? CumulativeFuel { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateModified { get; set; }
        public int? Difference { get; set; }
        public int? NoOfLitres { get; set; }
        public int? PersonId { get; set; }
        public int? VehicleId { get; set; }
        public string VehicleRegCode { get; set; }
    }
    public class ReportModel : IDisposable
    {
        public static IEnumerable<vw_DriverStatusRpt> GetDriverStatusRpt(int pageNumber, int pageSize, string orderCriteria,
            string DriverStatus)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int driverStatusId; Boolean IsDriverStatus = false;
                if (int.TryParse(DriverStatus, out driverStatusId))
                {
                    IsDriverStatus = true;
                }
                int clientId = Service.Utility.getClientId().Value;
                return entities.vw_DriverStatusRpt
                    .OrderBy(orderCriteria)
                     .Where(v =>
                      v.ClientId==(clientId)
                       && (IsDriverStatus ? v.DriverStatusId==(driverStatusId) : true)
                       )
                       .Skip((pageNumber - 1) * pageSize)
                  .Take(pageSize)
                  .ToList();
            }
        }

        public void Dispose()
        {
            //entities.Dispose();
        }

        public static IEnumerable<vw_DriverAssignmentStatus> getDriverAssignmentStatus(int pageNumber, int pageSize, string orderCriteria,
            string driver, string vehicle, string startdate, string enddate)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                if (pageNumber < 1)
                    pageNumber = 1;

                int vehicleId; int driverId;
                Boolean isVehicle = false; Boolean isDriver = false;

                if (int.TryParse(vehicle, out vehicleId))
                {
                    isVehicle = true;
                }

                if (int.TryParse(driver, out driverId))
                {
                    isDriver = true;
                }

                DateTime sdate; DateTime edate = DateTime.Now;
                Boolean goodDate = false;

                if (DateTime.TryParse(startdate, out sdate))
                {
                    if (DateTime.TryParse(enddate, out edate))
                    {
                        goodDate = true;
                    }
                }

                int clientId = Service.Utility.getClientId().Value;

                return entities.vw_DriverAssignmentStatus
                  .OrderBy(orderCriteria)
                  .Where(v =>
                      v.ClientId.Equals(clientId)
                      && (isVehicle ? v.VehicleId.Equals(vehicleId) : true)
                      && (isDriver ? v.PersonId.Equals(driverId) : true)
                      && (goodDate ? v.StartDate >= sdate && v.EndDate <= edate : true)
                   )
                  .Skip((pageNumber - 1) * pageSize)
                  .Take(pageSize)
                  .ToList();
            }
        }

        public static IEnumerable<vw_DriverLicenceTypeRpt> GetDriverLicenceRpt(int pageNumber, int pageSize, string orderCriteria,
         string DriverLicenceType)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int licenceTypeId; Boolean IsDriverLicence = false;
                if (int.TryParse(DriverLicenceType, out licenceTypeId))
                {
                    IsDriverLicence = true;
                }
                int clientId = Service.Utility.getClientId().Value;
                return entities.vw_DriverLicenceTypeRpt
                    .OrderBy(orderCriteria)
                     .Where(v =>
                      v.ClientId == (clientId)
                       && (IsDriverLicence ? v.LicenceTypeId == (licenceTypeId) : true)
                       )
                       .Skip((pageNumber - 1) * pageSize)
                  .Take(pageSize)
                  .ToList();
            }
        }

        public static IEnumerable<vw_VehicleFuelMngtRpt> GetVehicleFuelRpt(int pageNumber, int pageSize, string orderCriteria,
           string vehicle, string startdate, string enddate)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                if (pageNumber < 1)
                    pageNumber = 1;

                int vehicleId;
                Boolean isVehicle = false;

                if (int.TryParse(vehicle, out vehicleId))
                {
                    isVehicle = true;
                }

                DateTime sdate; DateTime edate;
                Boolean goodDate = false;

                enddate = HttpUtility.UrlDecode(enddate);     //endDate = endDate.Replace("+", ""); 
                startdate = HttpUtility.UrlDecode(startdate);   //startDate = startDate.Replace("+", "");

                enddate += " 23:59:59.999";

                if (DateTime.TryParse(startdate, out sdate))
                {
                    if (DateTime.TryParse(enddate, out edate))
                    {
                        goodDate = true;

                        int clientId = Service.Utility.getClientId().Value;
                        return entities.vw_VehicleFuelMngtRpt
                          .OrderBy(orderCriteria)
                          .Where(v =>
                              v.ClientId.Equals(clientId)
                              && (isVehicle ? v.VehicleId.Equals(vehicleId) : true)
                              && (goodDate ? v.Date >= sdate && v.DateModified <= edate : true)
                           )
                          .Skip((pageNumber - 1) * pageSize)
                          .Take(pageSize)
                          .ToList();
                    }
                }

                return new List<vw_VehicleFuelMngtRpt>();
            }
        }


        public static IEnumerable<vw_VehicleLicenceDetailRpt> GetVehicleLicenceDetails(int pageNumber, int pageSize, string orderCriteria,
            string VehicleLicenceType)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int vehicleLicenceTypeId; Boolean IsVehicleLicenceType = false;
                if (int.TryParse(VehicleLicenceType, out vehicleLicenceTypeId))
                {
                    IsVehicleLicenceType = true;
                }
                int clientId = Service.Utility.getClientId().Value;
                return entities.vw_VehicleLicenceDetailRpt
                    .OrderBy(orderCriteria)
                     .Where(v =>
                      v.ClientId==(clientId)
                       && (IsVehicleLicenceType ? v.LicenceTypeId == (vehicleLicenceTypeId) : true)
                       )
                       .Skip((pageNumber - 1) * pageSize)
                  .Take(pageSize)
                  .ToList();
            }
        }
    }
}