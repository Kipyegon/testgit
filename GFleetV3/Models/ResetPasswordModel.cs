﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;
using System.Linq;
using System.Web;
using GFleetV3.Service;

namespace GFleetV3.Models
{
    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "Please enter your email address")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserName (Email Address)")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Username { get; set; }

        public void ResetPassword(string username)
        {
            MembershipUser currentUser = Membership.GetUser(username);
            string password = currentUser.ResetPassword();
            //SendEmail.SendResetEmail(currentUser);
        }
    }
}