﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GFleetV3.Models
{
    public class ResponseModel
    {
        public ResponseModel()
        {
            Status = true;
            IsUpdate = false;
        }
        public Boolean Status { set; get; }
        public String Message { set; get; }
        public Boolean IsUpdate { get; set; }
        public int DriverDeviceId { set; get; }
        public int VehicleId { get; set; }
        public int DriverDeviceVehicleId { get; set; }
        public Object Data { get; set; }
    }
    public class MessageResponseModel
    {
        public MessageResponseModel()
        {
            Status = true;

        }

        public Boolean Status { set; get; }
        public String Error { set; get; }
        public String Message { set; get; }
        public string BatchNo { get; set; }
    }
}