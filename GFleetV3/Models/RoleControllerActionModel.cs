﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class GFleetAllControllers
    {
        public int ControllerId { get; set; }
        public string ControllerName { get; set; }
        public string ModuleName { get; set; }
        public string ModuleControllerName { get; set; }
    }
    public class GFleetControllerActions
    {
        public int ControllerActionId { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string ControllerActionName { get; set; }
    }
    public class GFleetAction
    {
        public int ActionId { get; set; }
        public string ActionName { get; set; }
    }
}
