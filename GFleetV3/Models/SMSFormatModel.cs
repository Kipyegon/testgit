﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{

    public class PagedSMSFormatModel
    {
        public IEnumerable<DispatchSMSFormat> SMSFormat { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class ModelSMSFormat : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<DispatchSMSFormat> GetSMSFormat()
        {
            return entities.DispatchSMSFormats.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<DispatchSMSFormat> GetSMSFormatPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.DispatchSMSFormats
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountSMSFormat()
        {
            return entities.DispatchSMSFormats.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public DispatchSMSFormat GetSMSFormat(int mSMSFormatId)
        {
            return entities.DispatchSMSFormats.Where(m => m.SMSFormatId == mSMSFormatId && m.ClientId == ClientId).FirstOrDefault();
        }


        public ResponseModel CreateSMSFormat(DispatchSMSFormat mSMSFormat)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                DispatchSMSFormat data = entities.DispatchSMSFormats.Where(m => m.ClientId == ClientId && m.SMSTypeId == mSMSFormat.SMSTypeId).FirstOrDefault();
                if (data != null)
                {
                    _rm.Status = false; _rm.Message = " SMS Type currently exists in the client.Kindly Edit the entry!";
                }
                else
                {
                    entities.DispatchSMSFormats.AddObject(mSMSFormat);
                    entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    _rm.Status = true;
                    _rm.Message = "Current SMS Format  successfully saved!";
                    _rm.Data = new
                    {
                        SMSFormat = mSMSFormat.SMSFormat,
                        Active = mSMSFormat.Active
                    };
                }
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateSMSFormat(DispatchSMSFormat mSMSFormat)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    DispatchSMSFormat data = entities.DispatchSMSFormats.Where(m => m.SMSFormatId == mSMSFormat.SMSFormatId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mSMSFormat;

                        mSMSFormat = (DispatchSMSFormat)entities.DispatchSMSFormats.ApplyCurrentValues(mSMSFormat);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Currentn SMS Format successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = " Error in updating!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

    }
}