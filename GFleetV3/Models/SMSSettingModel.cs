﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;

namespace GFleetV3.Models
{

    public class PagedSMSSettingModel
    {
        public IEnumerable<SMSSetting> SMSSetting { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class ModelSMSSetting : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<SMSSetting> GetSMSSetting()
        {
            return entities.SMSSettings.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<SMSSetting> GetSMSSettingPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.SMSSettings
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountSMSSetting()
        {
            return entities.SMSSettings.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public SMSSetting GetSMSSetting(int mSMSSettingId)
        {
            return entities.SMSSettings.Where(m => m.SMSSettingsId == mSMSSettingId && m.ClientId == ClientId).FirstOrDefault();
        }


        public ResponseModel CreateSMSSetting(SMSSetting mSMSSetting)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.SMSSettings.AddObject(mSMSSetting);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "Current SMS Settings  successfully saved!";
                _rm.Data = new
                {
                    SMSSetting = mSMSSetting.Password,
                    Active = mSMSSetting.Username,
                    SenderId = mSMSSetting.SenderId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateSMSSetting(SMSSetting mSMSSetting)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    SMSSetting data = entities.SMSSettings.Where(m => m.SMSSettingsId == mSMSSetting.SMSSettingsId && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mSMSSetting;

                        mSMSSetting = (SMSSetting)entities.SMSSettings.ApplyCurrentValues(mSMSSetting);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Current SMS Settings  successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = " Error in updating!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

    }
}