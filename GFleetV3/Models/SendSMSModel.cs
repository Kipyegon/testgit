﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class SendSMSModel
    {
        private static string PushSMS(string Phone, string Message)
        {
            try
            {
                Boolean isSMSSend = Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings["isSMSSend"].ToString());
                if (isSMSSend)
                {
                    string urlData = System.Configuration.ConfigurationManager.AppSettings["SMSurl"].ToString();
                    string[] urlValues = urlData.Split(',');

                    string url = urlValues[0].Replace("||", "&");

                    url = string.Format(url, urlValues[1], urlValues[2], urlValues[3] + Phone, urlValues[4], urlValues[5] + Message);

                    //Url = Url + "&password=1bli@012&destination=" + Phone + "&source=IBLI&message=" + Message;
                    WebRequest request = (WebRequest)WebRequest.Create(url);
                    //  execute the request
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    // read the response
                    StreamReader responseReader = new StreamReader(response.GetResponseStream());

                    String resultmsg = responseReader.ReadToEnd();
                    responseReader.Close();
                    response.Close();
                    return resultmsg.ToString();
                }
                else
                {
                    return "sms sending disabled!";
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                return ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
        }
        public static void SendSMS(string PhoneNo, string Message) //, Guid UserId, string Response)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());
                int clientId = Service.Utility.getClientId().Value;
                string Response = PushSMS(PhoneNo, Message);

                SentSMS sent = new SentSMS()
                {
                    aspnet_Users = null,
                    Date = DateTime.Now,
                    PhoneNo = PhoneNo,
                    Response = Response,
                    Sender = userId,
                    Text = Message,
                    ClientId = clientId
                };

                entities.SentSMS.AddObject(sent);
                entities.SaveChanges();
            }
        }
    }
}