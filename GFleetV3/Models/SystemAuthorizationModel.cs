﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;


namespace GFleetV3.Models
{
    public class SystemAuthorizationModel
    {
        public static ResponseModel createControllerActionClientTypes(string selectedControllerActions, string ClientTypeId, Boolean chkIsEnabled)
        {
            ResponseModel _rm = new ResponseModel();

            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    int clientTypeId = int.Parse(ClientTypeId);
                    string[] ControllerActionsIds = selectedControllerActions.Split(',');

                    HashSet<int> selected = new HashSet<int>();
                    foreach (string sel in ControllerActionsIds)
                    {
                        selected.Add(int.Parse(sel));
                    }
                    entities.ControllerActionClientTypes.Where(v => v.ClientTypeId == clientTypeId && !selected.Contains(v.ControllerActionId))
                                               .ToList()
                                               .ForEach(entities.ControllerActionClientTypes.DeleteObject);

                    HashSet<int> fromdb = new HashSet<int>(entities.ControllerActionClientTypes.Where(v => v.ClientTypeId == clientTypeId
                        && selected.Contains(v.ControllerActionId))
                        .Select(v => v.ControllerActionId));

                    List<int> toInsertList = selected.Except(fromdb.ToArray<int>()).ToList<int>();

                    foreach (int ControllerActionId in toInsertList)
                    {
                        entities.ControllerActionClientTypes.AddObject(new ControllerActionClientType()
                        {
                            ControllerActionId = ControllerActionId,
                            ClientType = null,
                            ControllerAction = null,
                            IsEnabled = chkIsEnabled,
                            ClientTypeId = clientTypeId
                        });
                    }

                    entities.SaveChanges();

                    _rm.Status = true;
                    _rm.Message = "Controller Action successfully assigned to Client Types";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }

            return _rm;
        }
        public static ResponseModel createControllerAction(int selectedControllers, string selectedAction)
        {
            ResponseModel _rm = new ResponseModel();

            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    string[] ActionIds = selectedAction.Split(',');

                    HashSet<int> selected = new HashSet<int>();
                    foreach (string sel in ActionIds)
                    {
                        selected.Add(int.Parse(sel));
                    }
                    entities.ControllerActions.Where(w => w.ControllerId == selectedControllers && !selected.Contains(w.ActionId))
                                               .ToList()
                                               .ForEach(entities.ControllerActions.DeleteObject);

                    HashSet<int> fromdb = new HashSet<int>(entities.ControllerActions.Where(v => v.ControllerId == selectedControllers
                        && selected.Contains(v.ActionId))
                        .Select(v => v.ActionId));

                    List<int> toInsertList = selected.Except(fromdb.ToArray<int>()).ToList<int>();

                    foreach (int ActionId in toInsertList)
                    {
                        entities.ControllerActions.AddObject(new ControllerAction()
                        {
                            ActionId = ActionId,
                            ControllerId = selectedControllers
                        });
                    }

                    entities.SaveChanges();

                    _rm.Status = true;
                    _rm.Message = "Controller Action successfully saved";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }

            return _rm;
        }
        public static ResponseModel createControllerActionRoles(string selectedControllerActions, string RoleId, Boolean chkIsEnabled)
        {
            ResponseModel _rm = new ResponseModel();

            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Guid roleId = Guid.Parse(RoleId);
                    string[] ControllerActionsIds = selectedControllerActions.Split(',');

                    HashSet<int> selected = new HashSet<int>();
                    foreach (string sel in ControllerActionsIds)
                    {
                        selected.Add(int.Parse(sel));
                    }
                    entities.ControllerActionRoles.Where(w => w.RoleId == roleId && !selected.Contains(w.ControllerActionId))
                                               .ToList()
                                               .ForEach(entities.ControllerActionRoles.DeleteObject);

                    HashSet<int> fromdb = new HashSet<int>(entities.ControllerActionRoles.Where(v => v.RoleId == roleId
                        && selected.Contains(v.ControllerActionId))
                        .Select(v => v.ControllerActionId));

                    List<int> toInsertList = selected.Except(fromdb.ToArray<int>()).ToList<int>();

                    foreach (int ControllerActionId in toInsertList)
                    {
                        entities.ControllerActionRoles.AddObject(new ControllerActionRole()
                        {
                            ControllerActionId = ControllerActionId,
                            aspnet_Roles = null,
                            ControllerAction = null,
                            IsEnabled = chkIsEnabled,
                            RoleId = roleId
                        });
                    }

                    entities.SaveChanges();

                    _rm.Status = true;
                    _rm.Message = "Controller Action successfully assigned to roles";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }

            return _rm;
        }
    }
}




