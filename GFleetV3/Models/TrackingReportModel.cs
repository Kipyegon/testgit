﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.SQL;

namespace GFleetV3.Models
{
    public class TrackingReportModel
    {

    }
    public class DetailedHistoryPayload
    {
        public int recNo { get; set; }
        public string DayDate { get; set; }
        public List<Report_History_V3_Result> Reports { get; set; }
    }
    public class DetailedHistory
    {
        public string time { get; set; }
        public string title { get; set; }
        public string VehicleRegCode { get; set; }
        public string header { get; set; }
        public List<DetailedHistoryPayload> data { get; set; }

        public static DetailedHistory getDetailedHistory(DateTime startDate, DateTime endDate, string registration)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                IEnumerable<IGrouping<string, Report_History_V3_Result>> availableData =
                   entities.Report_History_V3(clientId, startDate, endDate, registration).GroupBy(r => r.Date).ToList();
                return createHistoryReport(availableData, startDate,  endDate, registration);
            }
        }

        public static DetailedHistory getDetailedHistory(int DriverVehicleAssignmentId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                //int clientId = Service.Utility.getClientId().Value;
                //IEnumerable<IGrouping<string, Report_History_V3_Result>> availableData =
                //entities.Report_DetailedHistoryVehicleAssignment_V2(DriverVehicleAssignmentId, clientId).GroupBy(r => r.Date.ToString());
                //return createHistoryReport(availableData);
                return null;
            }
        }

        private static DetailedHistory createHistoryReport(IEnumerable<IGrouping<string, Report_History_V3_Result>> availableData, DateTime startDate, DateTime endDate, string registration)
        {
            List<DetailedHistoryPayload> data = new List<DetailedHistoryPayload>();
            List<Report_History_V3_Result> report = new List<Report_History_V3_Result>();
            int rec = 0;
            foreach (IGrouping<string, Report_History_V3_Result> rhr in availableData)
            {
                rec = rec + 1;
                string dayDate = rhr.Key.ToString() + " (" + DateTime.Parse(rhr.Key.ToString()).DayOfWeek + ")";
                data.Add(new DetailedHistoryPayload()
                {
                    recNo = rec,
                    DayDate = dayDate,
                    //Reportss = rhr.Where<Report_History_V3_Result>(r => r.ID > 0).ToList<Report_History_V3_Result>()
                    Reports = rhr.ToList<Report_History_V3_Result>()
                });
            }

            DetailedHistory retur = new DetailedHistory()
            {
                time = DateTime.Now.ToString(),
                title = "Detailed event report for vehicle " + registration,
                header = " -- Between " + startDate.ToString() + " and " + endDate.ToString(),
                data = data
            };
            return retur;
        }
    }

    public class DistanceReportPayload
    {
        public int recNo { get; set; }
        public string DayDate { get; set; }
        public string TotalDistance { get; set; }
        public List<getDistancetravelled_V3_Result> DistanceReports { get; set; }
    }
    public class DistanceReport
    {
        public string time { get; set; }
        public string title { get; set; }
        public string header { get; set; }
        public List<DistanceReportPayload> data { get; set; }

        public static DistanceReport getDistanceReport(string vehicleDetailId, DateTime startDate, DateTime endDate)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                List<getDistancetravelled_V3_Result> availableData =
                   entities.getDistancetravelled_V3(vehicleDetailId, startDate, endDate, clientId).ToList();
                return createDistanceReport(availableData);
            }
        }

        private static DistanceReport createDistanceReport(List<getDistancetravelled_V3_Result> availableData)
        {
            List<DistanceReportPayload> data = new List<DistanceReportPayload>();
            List<getDistancetravelled_V3_Result> report = new List<getDistancetravelled_V3_Result>();
            int rec = 0;
            //foreach (List<getDistancetravelled_V2_Result> rhr in availableData)
            //{
            //    rec = rec + 1;
            //    string dayDate = rhr.Key.ToString() + " (" + DateTime.Parse(rhr.Key.ToString()).DayOfWeek + ")";
            data.Add(new DistanceReportPayload()
            {
                recNo = rec,
                TotalDistance = availableData.Sum(v => v.Distance).Value.ToString(),
                DistanceReports = availableData
            });
            //}
            DistanceReport retur = new DistanceReport()
            {
                time = DateTime.Now.ToString(),
                title = "Distance Report Vehicle",
                //header = " -- Between " + startDate.ToString() + " and " + endDate.ToString(),
                data = data
            };
            return retur;
        }
    }

    public class JourneyPayload
    {
        public int recNo { get; set; }
        public string DayDate { get; set; }
        public string Registration { get; set; }
        public int journeyCount { get; set; }
        public decimal Distance { get; set; }
        public string ElapsedTime { get; set; }
        public long Time { get; set; }
        public decimal AveSpeed { get; set; }
        public List<Report_JourneyReport_V2_Result> JourneyReports { get; set; }
        public List<Report_JourneyReport_V2_Result> JourneyReportsDist { get; set; }
    }
    public class JourneyReport
    {
        public decimal TotalDistance { get; set; }
        public int TotaljourneyCount { get; set; }
        public decimal TotalAveSpeed { get; set; }
        public string TotalElapsedTime { get; set; }
        public string time { get; set; }
        public string title { get; set; }
        public string header { get; set; }
        public List<JourneyPayload> data { get; set; }
        private static string dateRep(long duration)
        {
            string result = "";
            long year = -1, month = -1, day = -1, hour = -1, minute = -1;

            minute = duration / 60;

            if (minute > 60)
            {
                hour = minute / 60;
                minute = minute % 60;
            }
            if (hour > 24)
            {
                day = hour / 60;
                hour = hour % 24;
            }
            if (day > 28)
            {
                month = day / 28;
                day = day % 28;
            }
            if (month > 12)
            {
                year = month / 12;
                month = month % 12;
            }

            if (year > 0)
                result = result + year + (year > 1 ? " Years " : " Year ");
            if (month > 0)
                result = result + month + (month > 1 ? " Months " : " Month ");
            if (day > 0)
                result = result + day + (day > 1 ? " Days " : " Day ");
            if (hour > 0)
                result = result + hour + (hour > 1 ? " Hours " : " Hour ");
            if (minute > 0)
                result = result + minute + (minute > 1 ? " Minutes " : " Minute");

            return result.Trim();
        }

        public static JourneyReport getJourneyReport(DateTime startDate, DateTime endDate, string regNo)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;

                IEnumerable<IGrouping<string, Report_JourneyReport_V2_Result>> availableData =
                    entities.Report_JourneyReport_V2(clientId, startDate, endDate, regNo).GroupBy(r => r.Date);
                return createJourneyReport(availableData, startDate, endDate, regNo);
            }
        }
        public static JourneyReport getJourneyReportDist(DateTime startDate, DateTime endDate, string regNo)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;

                IEnumerable<IGrouping<string, Report_JourneyReport_V2_Result>> availableData =
                    entities.Report_JourneyReport_V2(clientId, startDate, endDate, regNo).Where(r => r.Distance > 0).GroupBy(r => r.Date);
                return createJourneyDistanceReport(availableData, startDate,  endDate,  regNo);
            }
        }

        public static JourneyReport getJourneyReport(int DriverVehicleAssignmentId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                //int clientId = Service.Utility.getClientId().Value;

                //IEnumerable<IGrouping<string, Report_JourneyReport_V2_Result>> availableData =
                //    entities.Report_JourneyReport_X1DriverAssignment_V2(DriverVehicleAssignmentId, clientId).GroupBy(r => r.Date);
                //return createJourneyReport(availableData); ;
                return null;
            }
        }

        private static JourneyReport createJourneyReport(IEnumerable<IGrouping<string, Report_JourneyReport_V2_Result>> availableData, DateTime startDate, DateTime endDate, string regNo)
        {
            List<JourneyPayload> data = new List<JourneyPayload>();

            int rec = 0;
            foreach (IGrouping<string, Report_JourneyReport_V2_Result> rhr in availableData)
            {
                rec = rec + 1;
                string dayDate = rhr.Key.ToString() + " (" + DateTime.Parse(rhr.Key.ToString()).DayOfWeek + ")";
                decimal dist = decimal.Parse(rhr.Sum(v => v.Distance).ToString());
                //dist = dist / 1000; //conversion to Kms
                string Reg = rhr.First().Reg;
                long time = long.Parse(rhr.Sum(v => v.journeyDuration).ToString());
                decimal decTime = decimal.Parse(time.ToString());
                int count = rhr.Count();
                //time = time / 60 / 60; //conversion to hours
                decimal convtTime = decTime / 3600;
                decimal aveSpeed = dist / convtTime;
                //string aveSpeed = (dist / (time / 3600)).ToString("F");
                //string aveSpeed = speed.ToString("F2");


                data.Add(new JourneyPayload()
                {
                    journeyCount = count,
                    recNo = rec,
                    DayDate = dayDate,
                    Registration = Reg,
                    Distance = dist,
                    Time = time,
                    ElapsedTime = dateRep(time),
                    AveSpeed = aveSpeed,//aveSpeed.ToString() + "Km/h (average)",
                    JourneyReports = rhr.ToList<Report_JourneyReport_V2_Result>()
                });
            }

            JourneyReport retur = new JourneyReport()
            {
                TotalDistance = data.Sum(v => v.Distance),
                TotaljourneyCount = data.Sum(v => v.journeyCount),
                TotalElapsedTime = dateRep(data.Sum(v => v.Time)),
                time = DateTime.Now.ToString(),
                title = "Travel report for vehicle " + regNo,
                header = " -- Between " + startDate.ToString() + " and " + endDate.ToString(),
                data = data
            };
            return retur;
        }
        private static JourneyReport createJourneyDistanceReport(IEnumerable<IGrouping<string, Report_JourneyReport_V2_Result>> availableData, DateTime startDate, DateTime endDate, string regNo)
        {
            List<JourneyPayload> data = new List<JourneyPayload>();

            int rec = 0;
            foreach (IGrouping<string, Report_JourneyReport_V2_Result> rhr in availableData)
            {
                rec = rec + 1;
                string dayDate = rhr.Key.ToString() + " (" + DateTime.Parse(rhr.Key.ToString()).DayOfWeek + ")";
                decimal dist = decimal.Parse(rhr.Sum(v => v.Distance).ToString());
                //dist = dist / 1000; //conversion to Kms
                string Reg = rhr.First().Reg;
                long time = long.Parse(rhr.Sum(v => v.journeyDuration).ToString());
                decimal decTime = decimal.Parse(time.ToString());
                int count = rhr.Count();
                //time = time / 60 / 60; //conversion to hours
                decimal convtTime = decTime / 3600;
                decimal aveSpeed = dist / convtTime;
                //string aveSpeed = (dist / (time / 3600)).ToString("F");
                //string aveSpeed = speed.ToString("F2");


                data.Add(new JourneyPayload()
                {
                    journeyCount = count,
                    recNo = rec,
                    DayDate = dayDate,
                    Registration = Reg,
                    Distance = dist,
                    Time = time,
                    ElapsedTime = dateRep(time),
                    AveSpeed = aveSpeed,//aveSpeed.ToString() + "Km/h (average)",
                    JourneyReports = rhr.ToList<Report_JourneyReport_V2_Result>()
                });
            }

            JourneyReport retur = new JourneyReport()
            {
                TotalDistance = data.Sum(v => v.Distance),
                TotaljourneyCount = data.Sum(v => v.journeyCount),
                TotalElapsedTime = dateRep(data.Sum(v => v.Time)),
                time = DateTime.Now.ToString(),
                title = "Travel report for vehicle " + regNo,
                header = " -- Between " + startDate.ToString() + " and " + endDate.ToString(),
                data = data
            };
            return retur;
        }
    }

    public class StopPayload
    {
        public int recNo { get; set; }
        public string DayDate { get; set; }
        public List<Report_Stop_V2_Result> StopReports { get; set; }
    }
    public class StopReport
    {
        public string time { get; set; }
        public string title { get; set; }
        public string header { get; set; }
        public List<StopPayload> data { get; set; }
        public static StopReport getStopReport(DateTime startDate, DateTime endDate, string regNo)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;

                IEnumerable<IGrouping<string, Report_Stop_V2_Result>> availableData =
                    entities.Report_Stop_V2(clientId, startDate, endDate, regNo).GroupBy(r => r.From.Value.ToShortDateString());
                return createStopReport(availableData, startDate, endDate, regNo);
            }
        }
        public static StopReport getStopReport(int DriverVehicleAssignmentId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                //int clientId = Service.Utility.getClientId().Value;

                //IEnumerable<IGrouping<string, Report_Stop_V2_Result>> availableData =
                //    entities.Report_Stop_X1DriverAssignmnet_V2(DriverVehicleAssignmentId, clientId).GroupBy(r => r.From.Value.ToShortDateString());
                //return createStopReport(availableData);
                return null;
            }
        }

        private static StopReport createStopReport(IEnumerable<IGrouping<string, Report_Stop_V2_Result>> availableData, DateTime startDate, DateTime endDate, string regNo)
        {
            List<StopPayload> data = new List<StopPayload>();
            List<Report_Stop_V2_Result> report = new List<Report_Stop_V2_Result>();

            int rec = 0;
            foreach (IGrouping<string, Report_Stop_V2_Result> rhr in availableData)
            {
                rec = rec + 1;
                string dayDate = rhr.Key.ToString() + " (" + DateTime.Parse(rhr.Key.ToString()).DayOfWeek + ")";
                data.Add(new StopPayload()
                {
                    recNo = rec,
                    DayDate = dayDate,
                    StopReports = rhr.ToList<Report_Stop_V2_Result>()// rhr.Where<Report_Stop_Result>(r => r.ID > 0).ToList<Report_Stop_Result>()
                });
            }

            StopReport retur = new StopReport()
            {
                time = DateTime.Now.ToString(),
                title = "Stop report for vehicle " + regNo,
                header = " -- Between " + startDate.ToString() + " and " + endDate.ToString(),
                data = data
            };
            return retur;
        }
    }


    public class OverspeedPayload
    {
        public int recNo { get; set; }
        public string DayDate { get; set; }
        public List<Report_Overspeeding_V3_Result> OverspeedReport { get; set; }
    }
    public class Overspeed
    {
        public string time { get; set; }
        public string title { get; set; }
        public string header { get; set; }
        public List<OverspeedPayload> data { get; set; }

        public static Overspeed getOverspeed(string regNo, DateTime startDate, DateTime endDate, int Speed)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                IEnumerable<IGrouping<DateTime, Report_Overspeeding_V3_Result>> availableData =
                   entities.Report_Overspeeding_V3(regNo, startDate, endDate, Speed).GroupBy(r => r.Date.Value.Date);
                return createOverspeedReport(availableData,regNo,  startDate,  endDate);
            }
        }


        private static Overspeed createOverspeedReport(IEnumerable<IGrouping<DateTime, Report_Overspeeding_V3_Result>> availableData, string regNo, DateTime startDate, DateTime endDate)
        {
            List<OverspeedPayload> data = new List<OverspeedPayload>();
            List<Report_Overspeeding_V3_Result> report = new List<Report_Overspeeding_V3_Result>();
            int rec = 0;
            foreach (IGrouping<DateTime, Report_Overspeeding_V3_Result> rhr in availableData)
            {
                rec = rec + 1;
                string dayDate = rhr.Key.ToString() + " (" + DateTime.Parse(rhr.Key.ToString()).DayOfWeek + ")";
                data.Add(new OverspeedPayload()
                {
                    recNo = rec,
                    DayDate = dayDate,
                    OverspeedReport = rhr.ToList<Report_Overspeeding_V3_Result>()
                });
            }

            Overspeed retur = new Overspeed()
            {
                time = DateTime.Now.ToString(),
                title = "Overspeeding Report for vehicle " + regNo,
                header = " -- Between " + startDate.ToString() + " and " + endDate.ToString(),
                data = data
            };
            return retur;
        }
    }

    public class OverduePayload
    {
        public int recNo { get; set; }
        public string DayDate { get; set; }
        public List<getOverdueReport_V3_Result> OverdueReport { get; set; }
    }
    public class Overdue
    {
        public string time { get; set; }
        public string title { get; set; }
        public string header { get; set; }
        public List<OverduePayload> data { get; set; }

        public static Overdue getOverdue()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int clientId = Service.Utility.getClientId().Value;
                IEnumerable<IGrouping<string, getOverdueReport_V3_Result>> availableData =
                   entities.getOverdueReport_V3(clientId).GroupBy(r => r.date);
                return createOverdueReport(availableData);
            }
        }


        private static Overdue createOverdueReport(IEnumerable<IGrouping<string, getOverdueReport_V3_Result>> availableData)
        {
            List<OverduePayload> data = new List<OverduePayload>();
            List<getOverdueReport_V3_Result> report = new List<getOverdueReport_V3_Result>();
            int rec = 0;
            foreach (IGrouping<string, getOverdueReport_V3_Result> rhr in availableData)
            {
                rec = rec + 1;
                string dayDate = rhr.Key.ToString() + " (" + DateTime.Parse(rhr.Key.ToString()).DayOfWeek + ")";
                data.Add(new OverduePayload()
                {
                    recNo = rec,
                    DayDate = dayDate,
                    OverdueReport = rhr.ToList<getOverdueReport_V3_Result>()
                });
            }

            Overdue retur = new Overdue()
            {
                time = DateTime.Now.ToString(),
                title = "Overdue Report",
                //header = " -- Between " + startDate.ToString() + " and " + endDate.ToString(),
                data = data
            };
            return retur;
        }
    }
}