﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GFleetV3.Models
{
    public class TreeModel
    {
        public string id { get; set; }
        public string label { get; set; }
        public Boolean isFolder { get; set; }
        public Boolean open { get; set; }
        public string icon { get; set; }
        public TreeModel[] childs { get; set; }

        //public string icon { get; set; }
        //public string icon { get; set; }
    }
}