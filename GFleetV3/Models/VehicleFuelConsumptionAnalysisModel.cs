﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;


namespace GFleetV3.Models
{
    public class VehicleFuelConsumptionAnalysisModel

    {

        public class graphData
        {
            public string Name { get; set; }
            public int VehicleId { get; set; }
            public List<object> data { get; set; }
        }
        //public static IEnumerable<vw_DriverAssignmentStatus> getDriverAssignmentStatus(int pageNumber, int pageSize, string orderCriteria,
        //    string driver, string vehicle, string startdate, string enddate)
        //{
        //    using (GFleetModelContainer entities = new GFleetModelContainer())
        //    {
        //        if (pageNumber < 1)
        //            pageNumber = 1;

        //        int vehicleId; int driverId;
        //        Boolean isVehicle = false; Boolean isDriver = false;

        //        if (int.TryParse(vehicle, out vehicleId))
        //        {
        //            isVehicle = true;
        //        }

        //        if (int.TryParse(driver, out driverId))
        //        {
        //            isDriver = true;
        //        }

        //        DateTime sdate; DateTime edate = DateTime.Now;
        //        Boolean goodDate = false;

        //        if (DateTime.TryParse(startdate, out sdate))
        //        {
        //            if (DateTime.TryParse(enddate, out edate))
        //            {
        //                goodDate = true;
        //            }
        //        }

        //        int clientId = Service.Utility.getClientId().Value;

        //        return entities.vw_DriverAssignmentStatus
        //          .OrderBy(orderCriteria)
        //          .Where(v =>
        //              v.ClientId.Equals(clientId)
        //              && (isVehicle ? v.VehicleId.Equals(vehicleId) : true)
        //              && (isDriver ? v.PersonId.Equals(driverId) : true)
        //              && (goodDate ? v.StartDate >= sdate && v.EndDate <= edate : true)
        //           )
        //          .Skip((pageNumber - 1) * pageSize)
        //          .Take(pageSize)
        //          .ToList();
        //    }
        //}

        public static List<Object> getFuelConsumptionAnalysis(string VehicleIds, string startdate, string enddate)
        {
           
            try
            {
                using (GFleetModelContainer gFleet = new GFleetModelContainer())
                {
                    int ClientId = Service.Utility.getClientId().Value;
                    //int VehicleId;
                  
                   // string VehicleReg;
                    //DateTime sdate; DateTime edate;
                    //Boolean goodDate = false;

                    //enddate = HttpUtility.UrlDecode(enddate);     //endDate = endDate.Replace("+", ""); 
                    //startdate = HttpUtility.UrlDecode(startdate);   //startDate = startDate.Replace("+", "");

                    //enddate += " 23:59:59.999";




                    IEnumerable<IGrouping<string, VehicleFuelConsumptionAnalysis_Result>> data = gFleet.VehicleFuelConsumptionAnalysis(ClientId, DateTime.Parse(enddate), VehicleIds, DateTime.Parse(startdate))
                       .OrderBy(c => c.FuelConsumption).GroupBy(v => v.VehicleRegCode).AsEnumerable();

                        List<Object> returnData = new List<object>();

                        foreach (IGrouping<string, VehicleFuelConsumptionAnalysis_Result> _data in data)
                        {
                            graphData _vehicle = new graphData() { Name = _data.Key };
                            if (_vehicle.data == null)
                            {
                                _vehicle.data = new List<object>();
                            }

                            //data = data.OrderBy<VehicleFuelConsumptionAnalysis_Result>(c => c.FuelConsumption);

                            foreach (VehicleFuelConsumptionAnalysis_Result vehicle in _data)
                            {
                                // double[][] jk=new double[][];
                                var jk = new { vehicle.CurrentMileage, vehicle.FuelConsumption };

                                _vehicle.data.Add(jk);
                            }
                            returnData.Add(_vehicle);
                        }
                    

                    //if ((returnData != null) && (!returnData.Any()))
                    
                        return returnData ;

                    
                    //else
                    //{
                    //    return null;

                    //}
                     
                }
            }
            catch(Exception ex)
            {
                return null;
            }
        }


        double[] vehicleMileage; // X_Series

        //internal static object getFuelConsumptionAnalysisVehicleFuelConsumptionAnalysis(int ClientId, DateTime Now, DateTime p7DaysAgo)
        //{
        //    throw new NotImplementedException();
        //}

        //internal static List<object> getFuelConsumptionAnalysis(int ClientId, DateTime Now, DateTime p7DaysAgo)
        //{
        //    throw new NotImplementedException();
        //}



        //internal static List<object> getFuelConsumptionAnalysis(int ClientId, DateTime Now, DateTime p7DaysAgo)
        //{
        //    throw new NotImplementedException();
        //}
    }
}