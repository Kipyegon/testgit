﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Web.Security;
using System.Threading;

namespace GFleetV3.Models
{
    public class DispatchBookModel
    {
        public DispatchBookModel()
        {
            Dispatch = new Dispatch();
            PagedVehicleDispatchModel = new PagedVehicleDispatchModel();
        }
        public Dispatch Dispatch { get; set; }
        public PagedVehicleDispatchModel PagedVehicleDispatchModel { get; set; }

        public static DispatchBookModel getDispatchBookModel(int page = 1, string sort = "DispatchId", string sortDir = "DESC", string dataGrid = "false")
        {
            const int pageSize = 100;
            DispatchBookModel dbm = new DispatchBookModel();

            ModelVehicleDispatch vehicleDispatchModel = new ModelVehicleDispatch();
            var totalRows = vehicleDispatchModel.CountVehicleDispatch();

            dbm.PagedVehicleDispatchModel.VehicleDispatch = vehicleDispatchModel.GetVehicleDispatchPage(page, pageSize, "it." + sort + " " + sortDir);
            dbm.PagedVehicleDispatchModel.TotalRows = totalRows;
            dbm.PagedVehicleDispatchModel.PageSize = pageSize;

            return dbm;
        }

    }
    public class VehicleNumber
    {
        public string VehicleRegcode { get; set; }
        public string PhoneNo { get; set; }
        public string SerialNo { get; set; }
        public string DeviceMake { get; set; }
    }
    public class PagedVehicleModel
    {
        public IEnumerable<Vehicle> Vehicle { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedVehicleNumberModel
    {
        public IEnumerable<VehicleNumber> Vehicle { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedVehicleDispatchModel
    {
        public IEnumerable<getDispactBookDataClient_Result> VehicleDispatch { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedVehicleFuelManagementModel
    {
        public IEnumerable<VehicleFuelManagement> VehicleFuelManagement { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedVehicleLicenceTypeModel
    {
        public IEnumerable<VehicleLicenceType> VehicleLicenceType { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }
    public class PagedVehicleLicenceDetailModel
    {
        public IEnumerable<VehicleLicenceDetail> VehicleLicenceDetail { get; set; }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
    }

    #region new Dispatch grid

    public class DispatchGridModel
    {
        public IEnumerable<DispatchData> VehicleDispatch { get; set; }
        public Boolean isDispatch
        {
            get
            {
                return (Roles.GetRolesForUser(@HttpContext.Current.User.Identity.Name).ToArray<string>().Contains("Dispatch"));
            }
        }
        public Boolean isBooking
        {
            get
            {
                return (Roles.GetRolesForUser(@HttpContext.Current.User.Identity.Name).ToArray<string>().Contains("Booking"));
            }
        }
        public class DispatchData
        {
            public string PickUpPoint { get; set; }
            public string DropOffPoint { get; set; }
            public string PickUpDateTime { get; set; }
            public string PassengerNames { get; set; }
            public string PassengerOrganisation { get; set; }
            public string DriverName { get; set; }

            public string DispatchStatus { get; set; }
            public string PassengerTelephone { get; set; }
            public string VehicleDispatched { get; set; }


            public DateTime DateModified { get; set; }
            public string strDModified
            {
                get
                {
                    return DateModified.ToString("yyyy-MM-dd HH:mm:ss.fff");
                }
            }
            public string strBookedDate
            {
                get
                {
                    return BookedDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                }
            }
            public DateTime BookedDate { get; set; }
            public int? NoOfPassengers { get; set; }
            public int? PassengerOnBoardId { get; set; }
            public int? ClientId { get; set; }
            public int? CustomerId { get; set; }
            public int? DispatchId { get; set; }

            public int? Status { get; set; }
            public int? Rating { get; set; }
            public int? CallBackId { get; set; }
            public Guid? DispatchedBy { get; set; }
            public string PickUpdate { get; set; }
            public int? DispatchColorCode { get; set; }
            public bool IsAcknowledged { get; set; }
        }
        //public List<DispatchData> ClientDispatchData { get; set; }
        public int UserRole { get; set; }
        public static int getRoleId()
        {
            int RoleId = 0;

            if (Roles.IsUserInRole("Organisation Admin"))
            {
                RoleId = 0;
            }
            else
            {
                RoleId = 1;
            }
            return RoleId;
        }
        public DispatchGridModel()
        {
            //ClientDispatchData = new List<DispatchData>();
        }
        public static DispatchGridModel getDispatchData()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                DispatchGridModel dispatchdata = new DispatchGridModel();
                dispatchdata.VehicleDispatch = GetDispatchData();
                dispatchdata.UserRole = getRoleId();

                return dispatchdata;
            }

        }
        public static DispatchGridModel getLatestDispatchData(string highest)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                DispatchGridModel dispatchdata = new DispatchGridModel();
                dispatchdata.VehicleDispatch = GetLatestDispatchData(highest);
                dispatchdata.UserRole = getRoleId();

                if (dispatchdata.VehicleDispatch == null)
                {
                    dispatchdata.VehicleDispatch = new List<DispatchData>();
                }

                return dispatchdata;
            }

        }
        public static List<DispatchData> GetDispatchData()
        {
            try
            {
                int ClientId = Service.Utility.getClientId().Value;
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {

                    var data = (from p in entities.getDispactBookDataClient(ClientId)
                                select new DispatchData
                                {
                                    PickUpPoint = p.Pickuppointname,
                                    DropOffPoint = p.Dropoffpointname,
                                    PickUpDateTime = p.PickUpDateTime,
                                    PassengerOnBoardId = p.PassengerOnBoardId,
                                    CallBackId = p.CallBackId,
                                    DispatchedBy = p.DispatchedBy,
                                    NoOfPassengers = p.NoOfPassengers,
                                    CustomerId = p.CustomerId,
                                    PassengerNames = p.Customername,
                                    PassengerOrganisation = p.Organisationname,
                                    VehicleDispatched = p.VehicleRegCode,
                                    PassengerTelephone = p.CustomerTelephone,
                                    DispatchId = p.DispatchId,
                                    DriverName = p.Drivername,
                                    DispatchStatus = p.DispatchStatus,
                                    Status = p.DispatchStatusId,
                                    Rating = p.Rating,
                                    ClientId = p.ClientId,
                                    DateModified = p.DateModified.Value,
                                    DispatchColorCode = p.DispatchColor,
                                    IsAcknowledged = p.IsAcknowledged.Value,
                                }).Where(v => v.ClientId == ClientId && (v.Status.Value == 2 || v.Status.Value == 3))
                        //.OrderBy(v => v.PickUpDateTime)
                            .ToList();
                    return data;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }
        public static List<DispatchData> GetLatestDispatchData(string highest)
        {
            try
            {
                //int ClientId = Service.Utility.getClientId().Value;
                int? ClientId = Service.Utility.getClientId();
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    return (from p in entities.getLatestDispactBookDataClient(ClientId.Value, DateTime.Parse(highest))
                            select new DispatchData
                            {
                                PickUpPoint = p.Pickuppointname,
                                DropOffPoint = p.Dropoffpointname,
                                PickUpDateTime = p.PickUpDateTime,
                                PassengerOnBoardId = p.PassengerOnBoardId,
                                CallBackId = p.CallBackId,
                                DispatchedBy = p.DispatchedBy,
                                NoOfPassengers = p.NoOfPassengers,
                                CustomerId = p.CustomerId,
                                PassengerNames = p.Customername,
                                PassengerOrganisation = p.Organisationname,
                                PassengerTelephone = p.CustomerTelephone,
                                VehicleDispatched = p.VehicleRegCode,
                                DispatchStatus = p.DispatchStatus,
                                DispatchId = p.DispatchId,
                                Status = p.DispatchStatusId,
                                Rating = p.Rating,
                                DriverName = p.Drivername,
                                ClientId = p.ClientId,
                                DateModified = p.DateModified.Value,
                                DispatchColorCode = p.DispatchColor,
                                IsAcknowledged = p.IsAcknowledged.Value,
                            }).Where(v => v.ClientId == ClientId && (v.Status.Value == 2 || v.Status.Value == 3))
                        //.OrderBy(v => v.PickUpDateTime)
                            .ToList();
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }

    }


    #endregion


    public class ModelVehicle : IDisposable
    {
        //private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Vehicle> GetVehicle()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return entities.ClientVehicles.Where(v => v.ClientId == ClientId).Select(v => v.Vehicle).ToList();
            }
        }
        //public IEnumerable<DriverVehicleAssignment> GetDriverUserId(int VehicleId, string Pickupdatetime)
        //{
        //    using (GFleetModelContainer entities = new GFleetModelContainer())
        //    {
        //        return entities.getDriverVehicleAssignment(ClientId, VehicleId, Pickupdatetime).ToList();
        //    }
        //}
        public List<getDriverVehicleAssignment_Result> GetDriverUserId(int VehicleId, string Pickupdatetime)
        {
            int ClientId = Service.Utility.getClientId().Value;
            DateTime DateToParse = DateTime.ParseExact(Pickupdatetime, "dd-MM-yyyy HH:mm", null);
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                try
                {
                    return entities.getDriverVehicleAssignment(ClientId, VehicleId, DateToParse).ToList<getDriverVehicleAssignment_Result>();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
            }

        }
        //For Custom Paging

        public IEnumerable<Vehicle> GetVehiclePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return entities.ClientVehicles
                  .OrderBy(orderCriteria)
                  .Where(v => v.ClientId == ClientId).Select(v => v.Vehicle)
                  .Skip((pageNumber - 1) * pageSize)
                  .Take(pageSize)
                  .ToList();
            }
        }
        public List<VehicleNumber> GetVehicleNumber(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return (from p in entities.vw_VehicleDetailsV3
                       .Where(v => v.ClientId == ClientId)
                        select new VehicleNumber
                        {
                            VehicleRegcode = p.VehicleRegCode,
                            DeviceMake = p.DeviceMake,
                            SerialNo = p.SerialNo,
                            PhoneNo = p.PhoneNo
                        }).ToList();
            }
        }
        public int CountVehicle()
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return entities.ClientVehicles.Where(v => v.ClientId == ClientId).Select(v => v.Vehicle).Count();
            }
        }


        public void Dispose()
        {
            //using (GFleetModelContainer entities = new GFleetModelContainer())
            //{
            //    entities.Dispose();
            //}
        }

        //For Edit 
        public Vehicle GetVehicle(int mVehicleId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                return entities.Vehicles
                    .Include("VehicleDevices")
                    .Include("VehicleGroups")
                    .Include("VehicleStatu")
                    .Include("ClientVehicles")
                    //.Where(v => v.ClientId == ClientId).Select(v => v.Vehicle).FirstOrDefault();
                    .Where(m => m.VehicleId == mVehicleId).FirstOrDefault();
            }
        }

        public ResponseModel CreateVehicle(Vehicle mVehicle)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {

                    entities.Vehicles.AddObject(mVehicle);
                    entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    _rm.Status = true;
                    _rm.Message = "Vehicle saved";// +mVehicle.RegistrationNo + " successfully saved!";
                    _rm.Data = new
                    {
                        VehicleRegCode = mVehicle.VehicleRegCode,
                        Make = mVehicle.Make,
                        Model = mVehicle.Model,
                        Year = mVehicle.Year,
                        ChasisNo = mVehicle.ChasisNo,
                        EngineNo = mVehicle.EngineNo,
                        VehicleId = mVehicle.VehicleId
                    };
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateVehicle(Vehicle mVehicle)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            int ClientId = Service.Utility.getClientId().Value;
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Vehicle data = entities.Vehicles.Where(v => v.VehicleId == mVehicle.VehicleId).FirstOrDefault();

                    if (data != null)
                    {

                        data = (Vehicle)entities.Vehicles.ApplyCurrentValues(mVehicle);

                        HashSet<int> dltDevice = new HashSet<int>(mVehicle.VehicleDevices.Select(c => c.DeviceId));

                        entities.VehicleDevices.Where(w => w.VehicleId == mVehicle.VehicleId
                                                && !dltDevice.Contains(w.DeviceId))
                                            .ToList()
                                            .ForEach(entities.VehicleDevices.DeleteObject);

                        foreach (VehicleDevice vehdev in mVehicle.VehicleDevices)
                        {
                            if (!entities.VehicleDevices.Any(v => v.VehicleId == vehdev.VehicleId
                                        && v.DeviceId == vehdev.DeviceId))
                            {
                                entities.VehicleDevices.AddObject(new VehicleDevice()
                                {
                                    VehicleId = vehdev.VehicleId,
                                    DeviceId = vehdev.DeviceId,
                                    Status = true
                                });
                            }
                        }

                        HashSet<int> dltGroup = new HashSet<int>(mVehicle.VehicleGroups.Select(c => c.GroupId));

                        entities.VehicleGroups.Where(w => w.VehicleId == mVehicle.VehicleId
                                                && !dltGroup.Contains(w.GroupId)
                                                && w.Group.ClientId == ClientId)
                                            .ToList()
                                            .ForEach(entities.VehicleGroups.DeleteObject);

                        foreach (VehicleGroup vehgrp in mVehicle.VehicleGroups)
                        {
                            if (!entities.VehicleGroups.Any(v => v.VehicleId == vehgrp.VehicleId
                                && v.GroupId == vehgrp.GroupId
                                && v.Group.ClientId == ClientId))
                            {
                                entities.VehicleGroups.AddObject(new VehicleGroup()
                                {
                                    VehicleId = vehgrp.VehicleId,
                                    GroupId = vehgrp.GroupId,
                                    Status = true
                                });
                            }
                        }
                        HashSet<int> dltClientVehilce = new HashSet<int>(mVehicle.ClientVehicles.Select(c => c.ClientId));

                        entities.ClientVehicles.Where(w => w.VehicleId == mVehicle.VehicleId
                                                && !dltClientVehilce.Contains(w.ClientId)
                                                && w.Client.ClientId == ClientId)
                                            .ToList()
                                            .ForEach(entities.ClientVehicles.DeleteObject);

                        foreach (ClientVehicle clveh in mVehicle.ClientVehicles)
                        {
                            if (!entities.ClientVehicles.Any(v => v.VehicleId == clveh.VehicleId
                                && v.ClientId == clveh.ClientId
                                && v.Client.ClientId == ClientId))
                            {
                                entities.ClientVehicles.AddObject(new ClientVehicle()
                                {
                                    VehicleId = clveh.VehicleId,
                                    ClientId = clveh.ClientId
                                });
                            }
                        }
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Vehicle updated";// +mVehicle.RegistrationNo + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle " + mVehicle.RegistrationNo + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteVehicle(int mVehicleID)
        {
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Vehicle data = entities.ClientVehicles.Where(v => v.ClientId == ClientId).Select(v => v.Vehicle).FirstOrDefault();
                    entities.Vehicles.DeleteObject(data);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
    public class VehicleDispatch
    {
        public static List<VehiclesToDispatch_Result> getDispatchVehicle(string coordinates)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                int? clientId = Service.Utility.getClientId();
                if (coordinates != "undefined")
                {
                    return clientId.HasValue ? entities.VehiclesToDispatch(coordinates, clientId.Value).ToList<VehiclesToDispatch_Result>()
                        : new List<VehiclesToDispatch_Result>();
                }
                else
                {
                    return null;
                }
            }
        }
    }
    public class ModelVehicleDispatch : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Dispatch> GetVehicleDispatch()
        {
            return entities.Dispatches.ToList();
        }
        //For Custom Paging

        public IEnumerable<getDispactBookDataClient_Result> GetVehicleDispatchPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            var data = entities.getDispactBookDataClient(ClientId)
                        .Where(p => p.DispatchedBy == null || p.PassengerOnBoardId == -1
                        || p.CallBackId == -1)
                          .Skip((pageNumber - 1) * pageSize)
                          .Take(pageSize)
                          .ToList();

            return data;

            //var objectQuery = data as System.Data.Objects.ObjectQuery;

            //  entities.Dispatches
            //.OrderBy(orderCriteria)
            //.Skip((pageNumber - 1) * pageSize)
            //.Take(pageSize)
            //.Where(v => v.DispatchedBy == null || v.PassengerOnBoardId == -1 || v.CallBackId == -1 && v.ClientId == ClientId);



            //return entities.Dispatches
            //  .OrderBy(orderCriteria)
            //  .Skip((pageNumber - 1) * pageSize)
            //  .Take(pageSize)
            //  .Where(v => v.DispatchedBy == null || v.PassengerOnBoardId == -1 || v.CallBackId == -1 && v.ClientId == ClientId)
            //  .ToList();
            //return new List<Dispatch>();
        }

        public int CountVehicleDispatch()
        {
            return entities.Dispatches.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Dispatch GetVehicleDispatch(int mVehicleDispatchId)
        {
            return entities.Dispatches.Where(m => m.DispatchId == mVehicleDispatchId && m.ClientId == ClientId).FirstOrDefault();
        }

        public object GetVehicleDispatch(int mVehicleDispatchId, Boolean val)
        {
            return entities.Dispatches.Where(m => m.DispatchId == mVehicleDispatchId && m.ClientId == ClientId)
                .ToList()
                .Select(v => new
                {
                    v.Amount,
                    v.BookersPhone,
                    v.BookingRequestedBy,
                    PassengersName = v.Person.FirstName + " " + v.Person.LastName,
                    v.Person.Telephone1,
                    v.Person.Email1,
                    v.CustomerId,
                    DispatchAccessibilities = new HashSet<int>(v.DispatchAccessibilities.Select(c => c.AccessibilityId)),
                    v.DispatchedBy,
                    v.DispatchStatusId,
                    DispatchExtras = new HashSet<int>(v.DispatchExtras.Select(c => c.ExtrasId)),
                    v.DispatchId,
                    v.Distance,
                    v.DropOffPointName,
                    v.ExpectedCompletionTime,
                    v.From,
                    v.MoreDetails,
                    v.NoOfLuggage,
                    v.NoOfPassengers,
                    v.PaymentModeId,
                    PickUpDate = string.Format("{0:dd-MM-yyyy}", v.PickUpDateTime),
                    PickUpTime = string.Format("{0:HH:mm}", v.PickUpDateTime),
                    v.PickUpPointName,
                    v.SpecialInstructions,
                    v.To,
                    v.VehicleId,
                    v.Person.Organisation.OrganisationName,
                    v.Person.CustomerDirUrl
                })
                .FirstOrDefault();

        }

        public ResponseModel CreateVehicleDispatch(Dispatch mVehicleDispatch)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                //mVehicleDispatch.Vehicle = null;
                entities.Dispatches.AddObject(mVehicleDispatch);
                Person custdata = entities.People.OfType<Customer>().Where(v => v.PersonId == mVehicleDispatch.CustomerId).FirstOrDefault();
                if (mVehicleDispatch.VehicleId != null)
                {
                    UpdateVehicleAvailability(mVehicleDispatch.VehicleId.Value, false);
                }
                entities.SaveChanges();
                _rm.Status = true; _rm.Message = "Booking successfully Saved!";
                if (custdata != null)
                {
                    string emailBody;
                    string emailSubject;
                    string CustomerEmail = custdata.Email1;
                    string CustomerNames = mVehicleDispatch.PassengerNames;
                    string pickup = mVehicleDispatch.PickUpPointName;
                    string To = mVehicleDispatch.DropOffPointName;

                    emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectReceivedBooking"].ToString();
                    emailBody = CustomerNames + " your booking from " + pickup + " to " +
                    To + ",has been received. Regards Taksi Team";
                    EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("CustomerBooking"));
                    if (ET != null)
                    {
                        emailBody = String.Format(ET.Body, CustomerNames, pickup, To);
                        emailBody = ET.Header + emailBody + ET.Footer;
                    }
                    new Thread(() =>
                    {
                        try
                        {
                            Service.SendEmail.SendUserMngtEmail(CustomerEmail, emailSubject, emailBody);
                        }
                        catch (Exception mex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                            //throw new Exception(ex.Message.ToString());
                        }
                    }).Start();
                }
                _rm.Data = new
                {
                    PickUpDateTime = mVehicleDispatch.PickUpDateTime,
                    PickUpPointName = mVehicleDispatch.PickUpPointName,
                    DropOffPointName = mVehicleDispatch.DropOffPointName,
                    PassengerNames = mVehicleDispatch.PassengerNames,
                    Organisation = mVehicleDispatch.Person.Organisation.OrganisationName,
                    Telephone = mVehicleDispatch.Person.Telephone1,
                    SpecialInstructions = mVehicleDispatch.SpecialInstructions,
                    BookedDate = mVehicleDispatch.DateModified,
                    Cost = mVehicleDispatch.Amount,
                };

                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateVehicleDispatch(Dispatch mVehicleDispatch, bool? SendToDriver, bool? SendToCustomer)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {

                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == mVehicleDispatch.DispatchId && m.ClientId == ClientId).FirstOrDefault();
                    Person custdata = entities.People.OfType<Customer>().Where(v => v.PersonId == mVehicleDispatch.CustomerId).FirstOrDefault();
                    if (data != null)
                    {
                        mVehicleDispatch.BookedBy = data.BookedBy;
                        //mVehicleDispatch.PassengerOnBoardId = -1;
                        mVehicleDispatch.CallBackId = -1;
                        mVehicleDispatch.SMSToCustomer = SendToCustomer;
                        mVehicleDispatch.SMSToDriver = SendToDriver;
                        mVehicleDispatch.POBNumber = data.POBNumber;
                        data = mVehicleDispatch;

                        mVehicleDispatch = (Dispatch)entities.Dispatches.ApplyCurrentValues(mVehicleDispatch);
                        entities.SaveChanges();
                        _rm.Status = true;
                        _rm.Message = "Vehicle successfully dispatched!";

                        MessageResponseModel response = MServer_DriverV1Model.PushDriverDispatch(mVehicleDispatch.DispatchId);

                        if (custdata != null)
                        {

                            string emailBody;
                            string emailSubject;
                            string CustomerEmail = custdata.Email1;
                            string CustomerNames = mVehicleDispatch.PassengerNames;
                            string pickup = mVehicleDispatch.PickUpPointName;
                            string To = mVehicleDispatch.DropOffPointName;

                            emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectDispatchedBooking"].ToString();
                            emailBody = CustomerNames + " your booking from " + pickup + "to" +
                            To + ",has been Dispatched Successfully. Regards Taksi Team";
                            EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("CustomerBookingDispatched"));
                            if (ET != null)
                            {
                                emailBody = String.Format(ET.Body, CustomerNames, pickup, To);
                                emailBody = ET.Header + emailBody + ET.Footer;
                            }
                            new Thread(() =>
                            {
                                try
                                {
                                    Service.SendEmail.SendUserMngtEmail(CustomerEmail, emailSubject, emailBody);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    //throw new Exception(ex.Message.ToString());
                                }
                            }).Start();
                        }
                        UpdateVehicleAvailability(mVehicleDispatch.VehicleId.Value, false);
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle dispatch not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        private static void UpdateVehicleAvailability(int VehicleId, Boolean IsAvailable)
        {
            try
            {
                new Thread(() =>
                {
                    try
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            Vehicle vh = entities.Vehicles.SingleOrDefault(v => v.VehicleId == VehicleId);
                            if (vh != null)
                            {
                                vh.IsAvailable = IsAvailable;
                                entities.SaveChanges();
                            }
                        }
                    }
                    catch (Exception mex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                    }
                }).Start();
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
            }
        }

        public ResponseModel UpdatePOB(int idno, string PaggengerOnBoardId, string Comment)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == idno && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data.PassengerOnBoardId = int.Parse(PaggengerOnBoardId);
                        data.POBReason = Comment;
                        data.DispatchStatusId = 6;
                        data.POBBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        if (data.DispatchedBy == null)
                        {
                            data.DispatchedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        }
                        data.IsVehicleVisible = false;

                        data = (Dispatch)entities.Dispatches.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        _rm.Status = true;
                        _rm.Message = "Paggenger On Board successfully updated!";
                        if (data.VehicleId != null)
                        {
                            UpdateVehicleAvailability(data.VehicleId.Value, true);
                        }
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle dispatch not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel CanceBooking(int idno, string PassengerOnBoardId, string Comment, bool SendToCustomer)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == idno && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data.PassengerOnBoardId = int.Parse(PassengerOnBoardId);
                        data.POBReason = Comment;
                        data.DispatchStatusId = 5;
                        data.POBBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        if (data.DispatchedBy == null)
                        {
                            data.DispatchedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        }
                        data.IsVehicleVisible = false;

                        data = (Dispatch)entities.Dispatches.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        _rm.Status = true;
                        _rm.Message = "Booking successfully cancelled!";
                        if (SendToCustomer)
                        {
                            List<Dispatch_SendCustomerCancelSMS_Result> result = SendCancelledCustomer(idno).ToList<Dispatch_SendCustomerCancelSMS_Result>();
                        }
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle dispatch not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateCallBack(int idno, string CallBackId, string Comment)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == idno && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data.CallBackId = int.Parse(CallBackId);
                        data.CallBackBy = Guid.Parse(Service.Utility.getUserId().ToString());
                        data.CallBackReason = Comment;

                        data = (Dispatch)entities.Dispatches.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        _rm.Status = true;
                        _rm.Message = "Call back successfully updated!";

                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle dispatch not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel EditDispatch(int DispatchId, int VehicleId, Guid DriverUserId, bool? IsVehicleVisible, bool? SendToDriver, bool? SendToCustomer, bool? SendToDriverCancelled)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                Guid DriverCancelledUserId;
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == DispatchId && m.ClientId == ClientId).FirstOrDefault();
                    if (data != null)
                    {
                        DriverCancelledUserId = data.DriverUserId.Value;
                        data.VehicleId = VehicleId;
                        data.DriverUserId = DriverUserId;
                        data.IsVehicleVisible = IsVehicleVisible.Value;

                        data = (Dispatch)entities.Dispatches.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        _rm.Status = true;
                        _rm.Message = "Dispatch successfully updated!";
                        List<Dispatch_SendDispatchMessages1_Result> result = SendDispatchMessage(DispatchId, SendToDriver, SendToCustomer, SendToDriverCancelled, DriverCancelledUserId).ToList<Dispatch_SendDispatchMessages1_Result>();

                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle dispatch not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteVehicleDispatch(int mVehicleDispatchID)
        {
            try
            {
                Dispatch data = entities.Dispatches.Where(m => m.DispatchId == mVehicleDispatchID && m.ClientId == ClientId).FirstOrDefault();
                entities.Dispatches.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }

        public List<Dispatch_SendDispatchMessages1_Result> SendDispatchMessage(int DispatchId, bool? SendToDriver, bool? SendToCustomer, bool? SendToDriverCancelled, Guid? DriverUserId)
        {
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    int ClientId = Service.Utility.getClientId().Value;
                    return entities.Dispatch_SendDispatchMessages1(DispatchId, SendToDriver, SendToCustomer, SendToDriverCancelled, DriverUserId).ToList<Dispatch_SendDispatchMessages1_Result>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public ResponseModel AcknowledgeBooking(int DispatchId)
        {
             ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                        int ClientId = Service.Utility.getClientId().Value;
                        Dispatch data = entities.Dispatches.Where(m => m.DispatchId == DispatchId && m.ClientId == ClientId).FirstOrDefault();
                        if (data != null)
                        {
                            data.IsAcknowledged = true;
                            data = (Dispatch)entities.Dispatches.ApplyCurrentValues(data);
                            entities.SaveChanges();
                            entities.Dispatch_AcknowledgeBooking(DispatchId).ToList<Dispatch_AcknowledgeBooking_Result>();
                            _rm.Status = true;
                            _rm.Message = "Booking Acknowledged!";
                            return _rm;
                        }
                        else
                        {
                            _rm.Status = true;
                            _rm.Message = "Booking not acknowledged!Kindly contact your sytem admin";
                            return _rm;
                        }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<Dispatch_SendCustomerCancelSMS_Result> SendCancelledCustomer(int DispatchId)
        {
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    int ClientId = Service.Utility.getClientId().Value;
                    return entities.Dispatch_SendCustomerCancelSMS(DispatchId, ClientId).ToList<Dispatch_SendCustomerCancelSMS_Result>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
    public class ModelVehicleFuelManagement : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<VehicleFuelManagement> GetVehicleFuelManagement()
        {
            return entities.VehicleFuelManagements.Where(v => v.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<VehicleFuelManagement> GetVehicleFuelManagementPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.VehicleFuelManagements
              .OrderBy(orderCriteria)
              .Where(v => v.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .GroupBy(c => c.VehicleId)
              .Select(c => c.FirstOrDefault())
              .ToList();
        }
        public int CountVehicleFuelManagement()
        {
            return entities.VehicleFuelManagements.Where(v => v.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public VehicleFuelManagement GetVehicleFuelManagement(int mVehicleFuelManagementId)
        {
            return entities.VehicleFuelManagements.Where(m => m.VehicleFuelManagementId == mVehicleFuelManagementId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateVehicleFuelManagement(VehicleFuelManagement mVehicleFuelManagement)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.VehicleFuelManagements.AddObject(mVehicleFuelManagement);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Data = new
                {
                    VehicleRegCode = mVehicleFuelManagement.VehicleRegCode,
                    Date = mVehicleFuelManagement.Date,
                    NoOfLitres = mVehicleFuelManagement.NoOfLitres,
                    Difference = mVehicleFuelManagement.Difference,
                    AvgFuelCons = mVehicleFuelManagement.AvgFuelCons,
                    VehicleFuelManagementId = mVehicleFuelManagement.VehicleFuelManagementId
                };
                _rm.Message = "Fuel for Vehicle  " + mVehicleFuelManagement.Vehicle.RegistrationNo + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel saveFuelManagement(int VehicleId, int ClientId, double AvgFuelCons, DateTime FuelDate, Guid DriverUserId,
            int CurrentMileage, int PreviuosMileage, Guid UserId, int NoOfLitres, int Difference, double Cost)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.FuelData_Insert(VehicleId, ClientId, UserId, DriverUserId, NoOfLitres, Difference, CurrentMileage, PreviuosMileage,
                    FuelDate, Cost, AvgFuelCons);
                _rm.Status = true;
                _rm.Message = "Fuel Details successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel UpdateVehicleFuelManagement(VehicleFuelManagement mVehicleFuelManagement)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    VehicleFuelManagement data = entities.VehicleFuelManagements.Where(m =>
                        m.VehicleFuelManagementId == mVehicleFuelManagement.VehicleFuelManagementId
                        && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mVehicleFuelManagement;

                        mVehicleFuelManagement = (VehicleFuelManagement)entities.VehicleFuelManagements.ApplyCurrentValues(mVehicleFuelManagement);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Fuel For Vehicle " + mVehicleFuelManagement.Vehicle.RegistrationNo + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Fuel For Vehicle " + mVehicleFuelManagement.Vehicle.RegistrationNo + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteVehicleFuelManagement(int mVehicleFuelManagementID)
        {
            try
            {
                VehicleFuelManagement data = entities.VehicleFuelManagements.Where(m => m.VehicleFuelManagementId == mVehicleFuelManagementID
                    && m.ClientId == ClientId).FirstOrDefault();
                entities.VehicleFuelManagements.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }

    public class ModelVehicleLicenceType : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<VehicleLicenceType> GetVehicleLicenceType()
        {
            return entities.VehicleLicenceTypes.Where(c => c.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<VehicleLicenceType> GetVehicleLicenceTypePage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.VehicleLicenceTypes
              .OrderBy(orderCriteria)
              .Where(c => c.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountVehicleLicenceType()
        {
            return entities.VehicleLicenceTypes.Where(c => c.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public VehicleLicenceType GetVehicleLicenceType(int mVehicleLicenceTypeId)
        {
            return entities.VehicleLicenceTypes.Where(m => m.VehicleLicenceTypeId == mVehicleLicenceTypeId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateVehicleLicenceType(VehicleLicenceType mVehicleLicenceType)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.VehicleLicenceTypes.AddObject(mVehicleLicenceType);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Message = "Vehicle Licence Type " + mVehicleLicenceType.VehicleLicenceTypeName + " successfully saved!";
                _rm.Data = new
                {
                    VehicleLicenceTypeName = mVehicleLicenceType.VehicleLicenceTypeName,
                    VehicleLicenceTypeId = mVehicleLicenceType.VehicleLicenceTypeId
                };
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateVehicleLicenceType(VehicleLicenceType mVehicleLicenceType)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    VehicleLicenceType data = entities.VehicleLicenceTypes.Where(m =>
                        m.VehicleLicenceTypeId == mVehicleLicenceType.VehicleLicenceTypeId
                        && m.ClientId == ClientId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mVehicleLicenceType;

                        mVehicleLicenceType = (VehicleLicenceType)entities.VehicleLicenceTypes.ApplyCurrentValues(mVehicleLicenceType);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Vehicle Licence Type " + mVehicleLicenceType.VehicleLicenceTypeName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle Licence Type " + mVehicleLicenceType.VehicleLicenceTypeName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteVehicleLicenceType(int mVehicleLicenceTypeID)
        {
            try
            {
                VehicleLicenceType data = entities.VehicleLicenceTypes.Where(m => m.VehicleLicenceTypeId == mVehicleLicenceTypeID
                    && m.ClientId == ClientId).FirstOrDefault();
                entities.VehicleLicenceTypes.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }

    public class ModelVehicleLicenceDetail : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<VehicleLicenceDetail> GetVehicleLicenceDetail()
        {
            return entities.VehicleLicenceDetails.Where(c => c.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<VehicleLicenceDetail> GetVehicleLicenceDetailPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;
            DateTime nowdate = DateTime.Now;

            return entities.VehicleLicenceDetails
              .OrderBy(orderCriteria)
              .OrderByDescending(v => v.ExpireDate)
              .Where(c => c.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
                //.Where(v => v.ExpireDate.Year >= nowdate.Year && v.ExpireDate.Month >= nowdate.Month && v.ExpireDate.Day >= nowdate.Day)
              .ToList();
        }
        public int CountVehicleLicenceDetail()
        {
            return entities.VehicleLicenceDetails.Where(c => c.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public VehicleLicenceDetail GetVehicleLicenceDetail(int mVehicleLicenceDetailId)
        {
            return entities.VehicleLicenceDetails.Where(m => m.VehicleLicenceDetailId == mVehicleLicenceDetailId && m.ClientId == ClientId).FirstOrDefault();
        }

        public ResponseModel CreateVehicleLicenceDetail(VehicleLicenceDetail mVehicleLicenceDetail)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                entities.VehicleLicenceDetails.AddObject(mVehicleLicenceDetail);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true;
                _rm.Data = new
                {
                    VehicleRegCode = mVehicleLicenceDetail.VehicleRegCode,
                    VehicleLicenceType = mVehicleLicenceDetail.VehicleLicenceType.VehicleLicenceTypeName,
                    LicenceNo = mVehicleLicenceDetail.LicenceNo,
                    RenewalDate = mVehicleLicenceDetail.RenewalDate,
                    ExpireDate = mVehicleLicenceDetail.ExpireDate,
                    RenewalCost = mVehicleLicenceDetail.RenewalCost,
                    VehicleLicenceDetailId = mVehicleLicenceDetail.VehicleLicenceDetailId
                };
                _rm.Message = "Vehicle Licence Detail with Licence No." + mVehicleLicenceDetail.LicenceNo + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateVehicleLicenceDetail(VehicleLicenceDetail mVehicleLicenceDetail)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    VehicleLicenceDetail data = entities.VehicleLicenceDetails.Where(m =>
                        m.VehicleLicenceDetailId == mVehicleLicenceDetail.VehicleLicenceDetailId
                        && m.ClientId == ClientId).FirstOrDefault();

                    //mVehicleLicenceDetail.VehicleId = mVehicleLicenceDetail.Vehicle.VehicleId;
                    //mVehicleLicenceDetail.Vehicle = null;

                    //mVehicleLicenceDetail.Id = mVehicleLicenceDetail.VehicleLicenceType.Id;
                    //mVehicleLicenceDetail.VehicleLicenceType = null;

                    if (data != null)
                    {
                        data = mVehicleLicenceDetail;

                        mVehicleLicenceDetail = (VehicleLicenceDetail)entities.VehicleLicenceDetails.ApplyCurrentValues(mVehicleLicenceDetail);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Vehicle Licence Detail with Licence No." + mVehicleLicenceDetail.LicenceNo + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Vehicle Licence Detail with Licence No." + mVehicleLicenceDetail.LicenceNo + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteVehicleLicenceDetail(int mVehicleLicenceDetailID)
        {
            try
            {
                VehicleLicenceDetail data = entities.VehicleLicenceDetails.Where(m =>
                    m.VehicleLicenceDetailId == mVehicleLicenceDetailID
                    && m.ClientId == ClientId).FirstOrDefault();
                entities.VehicleLicenceDetails.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }

    public class ModelDispatchedVehiclesDaily : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public IEnumerable<Dispatch> GetVehicleDispatch()
        {
            return entities.Dispatches.Where(c => c.ClientId == ClientId).ToList();
        }

        //For Custom Paging

        public IEnumerable<Dispatch> GetDispatchedVehilcesDailyPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;
            DateTime nowdate = DateTime.Now.Date;
            return entities.Dispatches
              .OrderBy(orderCriteria)
              .Where(c => c.ClientId == ClientId)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .Where(v => v.DispatchedBy != null && v.PickUpDateTime < nowdate)
              .ToList();
        }
        public int CountDispatchedVehicles()
        {
            return entities.Dispatches.Where(c => c.ClientId == ClientId).Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public Dispatch GetDispatchedVehilce(int mVehicleDispatchId)
        {
            return entities.Dispatches.Where(m => m.DispatchId == mVehicleDispatchId && m.ClientId == ClientId).FirstOrDefault();
        }
    }
}