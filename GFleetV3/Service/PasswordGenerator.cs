﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GFleetV3.Service
{
    public class PasswordGenerator
    {
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber()
        {
            lock (syncLock)
            { // synchronize
                return random.Next(100000, 999999);

            }
        }

    }
}