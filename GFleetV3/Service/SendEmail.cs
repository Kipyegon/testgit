﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;

namespace GFleetV3.Service
{
    public class SendEmail
    {
        public static void SendUserMngtEmail(string email, string subject, string body, string replyto = null)
        {
            try
            {
                string mailServer = System.Configuration.ConfigurationManager.AppSettings["mailServer"].ToString();
                string emailFrom = System.Configuration.ConfigurationManager.AppSettings["emailFrom"].ToString();
                string emailFromPassword = System.Configuration.ConfigurationManager.AppSettings["emailFromPassword"].ToString();

                string[] Emails = null;
                /*int emaillen = email.Length;
                email = email.Substring(0, emaillen);*/

                Emails = email.Split(';');
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailFrom);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                if (replyto != null)
                {
                    string[] tos = replyto.Split(',');
                    foreach (string to in tos)
                    {
                        mail.ReplyToList.Add(new MailAddress(to));
                    }
                }
                if (email.Contains(";"))
                {
                    mail.To.Add(Emails.ElementAt(0).ToString());
                    for (int count = 0; count <= Emails.Count() - 1; count++)
                    {
                        if (Emails.ElementAt(count).ToString() != "" && Emails.ElementAt(count).ToString() != null)
                        {
                            mail.CC.Add(Emails.ElementAt(count).ToString());
                        }
                    }
                }
                else
                {
                    mail.To.Add(email);

                }
                SmtpClient smtp = new SmtpClient(mailServer);
                NetworkCredential credential = new NetworkCredential(emailFrom, emailFromPassword);
                smtp.Credentials = credential;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
            }
        }

        public static void SendUserMngtEmail(string email, string subject, string body, string emailFrom, string emailFromPassword)
        {
            try
            {
                string mailServer = System.Configuration.ConfigurationManager.AppSettings["mailServer"].ToString();

                email = string.IsNullOrEmpty(email) ? "" : email;

                string[] Emails = email.Contains('|') ? email.Split(Convert.ToChar("|")) : email.Split(',');

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailFrom);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                if (Emails.Length > 0)
                {
                    mail.To.Add(new MailAddress(Emails[0]));
                    for (int i = 1; i < Emails.Length; i++)
                        mail.CC.Add(new MailAddress(Emails[i]));

                    SmtpClient smtp = new SmtpClient(mailServer);
                    NetworkCredential credential = new NetworkCredential(emailFrom, emailFromPassword);
                    smtp.Credentials = credential;
                    smtp.Send(mail);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
            }
        }

        //Method to hash parameters to generate the Reset URL
        public static string HashResetParams(string username, string guid)
        {

            byte[] bytesofLink = System.Text.Encoding.UTF8.GetBytes(username + guid);
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            string HashParams = BitConverter.ToString(md5.ComputeHash(bytesofLink));

            return HashParams;
        }
    }
}