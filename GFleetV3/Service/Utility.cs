﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using DataAccess.SQL;

namespace GFleetV3.Service
{
    public class Utility
    {
        public static bool isAdmin()
        {
            string[] SystemRightsRoles = System.Configuration.ConfigurationManager.AppSettings["SystemRightsRoles"].ToString().Split(',');
            Boolean isSystemRightsRoles = false;
            foreach (string role in Roles.GetRolesForUser())
            {
                if (SystemRightsRoles.Contains(role))
                {
                    isSystemRightsRoles = true;
                    break;
                }
            }
            return isSystemRightsRoles;
        }
        public static bool isBook()
        {
            string[] BookRightsRoles = System.Configuration.ConfigurationManager.AppSettings["BookRightsRoles"].ToString().Split(',');
            Boolean isBookRightsRoles = false;
            foreach (string role in Roles.GetRolesForUser())
            {
                if (BookRightsRoles.Contains(role))
                {
                    isBookRightsRoles = true;
                    break;
                }
            }
            return isBookRightsRoles;
        }
        public static bool isDispatch()
        {
            string[] DispatchRightsRoles = System.Configuration.ConfigurationManager.AppSettings["DispatchRightsRoles"].ToString().Split(',');
            Boolean isDispatchRightsRoles = false;
            foreach (string role in Roles.GetRolesForUser())
            {
                if (DispatchRightsRoles.Contains(role))
                {
                    isDispatchRightsRoles = true;
                    break;
                }
            }
            return isDispatchRightsRoles;
        }

        public static object getUserId(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {

                object UserId = Membership.GetUser(Username).ProviderUserKey;
                return UserId;
            }
            else
                return null;
        }
        public static int? getOrganisationId()
        {
            if (HttpContext.Current.Session["OrganisationId"] == null)
            {
                if (Membership.GetUser() != null)
                {
                    //MembershipProvider pmp = Membership.Provider;
                    object UserId = getUserId();
                    if (UserId != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            Guid userId = Guid.Parse(UserId.ToString());
                            Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                            if (per != null)
                            {
                                int? OrganisationId = per.OrganisationId;
                                if (OrganisationId.HasValue)
                                    HttpContext.Current.Session["OrganisationId"] = OrganisationId.Value;
                                return OrganisationId;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
            else
                return int.Parse(HttpContext.Current.Session["OrganisationId"].ToString());
        }
        public static int GeneratePass()
        {
            return PasswordGenerator.RandomNumber();
        }

        public static object getUserId()
        {
            if (Membership.GetUser() != null)
            {

                object UserId = Membership.GetUser().ProviderUserKey;
                return UserId;
            }
            else
                return null;
        }
        public static int? getClientId()
        {
            if (HttpContext.Current.Session["ClientId"] == null)
            {
              
                    if (Membership.GetUser() != null)
                    {
                        //MembershipProvider pmp = Membership.Provider;
                        object UserId = getUserId();
                        if (UserId != null)
                        {
                            using (GFleetModelContainer entities = new GFleetModelContainer())
                            {
                                Guid userId = Guid.Parse(UserId.ToString());
                                Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                                if (per != null)
                                {
                                    int? ClientId = per.ClientId;
                                    if (ClientId.HasValue)
                                        HttpContext.Current.Session["ClientId"] = ClientId.Value;
                                    return ClientId;
                                }
                                else
                                {
                                    return null;
                                }
                            }
                        }
                   
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
            else
                return int.Parse(HttpContext.Current.Session["ClientId"].ToString());
        }

        public static int? getClientId(string Username)
        {
                if (Membership.GetUser(Username) != null)
                {
                    //MembershipProvider pmp = Membership.Provider;
                    object UserId = getUserId(Username);
                    if (UserId != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            Guid userId = Guid.Parse(UserId.ToString());
                            Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                            if (per != null)
                            {
                                int? ClientId = per.ClientId;
                                return ClientId;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
        }
        public static int? getDriverClientId(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {
                //MembershipProvider pmp = Membership.Provider;
                object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        var per = entities.getSingleUserProfile(UserId.ToString(), 3).SingleOrDefault();
                        if (per != null)
                        {
                            int? ClientId = int.Parse(per.ClientId);
                            return ClientId;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public static string getClientBrandName()
        {
            int? clientId = getClientId();

            
            if (clientId.HasValue)
            {
                GFleetModelContainer entities =new GFleetModelContainer();

                Client _cl=entities.Clients.SingleOrDefault(r=>r.ClientId==clientId.Value);

                return _cl != null ? _cl.BrandName : null;
            }
            return null;
        }

        public static string getClientTypeName()
        {
            int? clientId = getClientId();

            if (clientId.HasValue)
            {
                GFleetModelContainer entities = new GFleetModelContainer();

                vw_ClientTypeDetails _cl = entities.vw_ClientTypeDetails.SingleOrDefault(v => v.ClientId == clientId.Value);

                return _cl != null ? _cl.ClientTypeName : null;
            }
            return null;
        }

        public static string getReplyToAddress()
        {
            int? clientId = getClientId();

            if (clientId.HasValue)
            {
                GFleetModelContainer entities = new GFleetModelContainer();

                CompanySetting _cl = entities.CompanySettings.SingleOrDefault(r => r.ClientId == clientId.Value);

                return _cl != null ? _cl.ReplyToEmail : null;
            }
            return null;
        }

        public static string getCustomerName(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {
                //MembershipProvider pmp = Membership.Provider;
                object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            string FullNames = (per.Title != null ? per.Title : " ") + " " + (per.FirstName != null ? per.FirstName : " ") + " " + (per.LastName != null ? per.LastName : " ");

                            return FullNames;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
    }

    public class Helper
    {
        public static byte[] GetBytesFromFile(string fullFilePath)
        {
            // this method is limited to 2^32 byte files (4.2 GB)

            FileStream fs = null;
            try
            {
                fs = File.OpenRead(fullFilePath);
                byte[] bytes = new byte[fs.Length];
                fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                return bytes;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
            }

        }
    }
}