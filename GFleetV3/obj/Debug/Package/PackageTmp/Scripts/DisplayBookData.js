﻿
var update_interval = 15000;
var update_pending = false;
var docMap;
var gm = google.maps;
var map = null;
var pickup_xy = "";
var dropoff_xy = "";
var vehicles = [];
var book_edit_url = "/VehicleDispatch/Edit";
var dispatch_edit_url = "/VehicleDispatch/EditDispatch";
var dispatch_UpdatePOB_url = "/VehicleDispatch/UpdatePOB";
var dispatch_UpdateCallBack_url = "/VehicleDispatch/UpdateCallBack";
var the_dataTable;
var $map = $('#map');
var NotificationBar = $('#notsBar');


$(document).ready(function () {
    loadDataTable();
    first_load();

    setInterval('update_vehicles()', update_interval);
   $('html').niceScroll();
});


function initial_load(data) {
    update_pending = true;

    create_vehicles(data);

    loadDataTable();
    update_pending = false;
}

function loadDataTable() {
    //if (vehicles.length > 0) {
    if (the_dataTable == null) {
        the_dataTable = $('#vehicles').DataTable({
            "oTableTools": {
                "sSwfPath": "../../Scripts/dataTable/copy_csv_xls_pdf.swf",
                "aButtons": [
                    {
                        "sExtends": "copy",
                        "mColumns": [0, 1, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "csv",
                        "mColumns": [0, 1, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "print",
                        "mColumns": [0, 1, 3, 4, 5, 6]
                    },
                ]
            },
            "scrollCollapse": true,
            "paging": false,
            "ordering": true,
            "order": [[1, "desc"]],
            "info": false,
            "ScrollY": "350px",
            "bPaginate": false,
            //"sDom": 'Tlfrtip'
            "sDom": '<"top_left"l><"top_right"Tf>'
        });
    }
}


function create_vehicles(vs) {
    vehicles = [];

    $.each(vs.VehicleDispatch, function () {
        var role = vs.isDispatch && vs.isBooking ? 1 : (vs.isDispatch ? 0 : vs.isBooking ? -1 : -2);
        vehicles.push(new vehicledispatch(this, role));
    });
}


// Vehicle class
function vehicledispatch(values, role, start) {

    this.update = function (new_values) {

        for (var v in new_values) {
            this[v] = new_values[v];
        }

        this.update_row();
    };

    this.update_row = function () {
        var self = this;
        var result_div = "<ul class='booking_options'><li data-tooltip='Accept'><i class='fa fa-paper-plane-o'></i></li> <li data-tooltip='Start'><i class='fa fa-taxi'></i></li> <li data-tooltip='Wait'><i class='fa fa-circle'></i></li> <li data-tooltip='onBoard'><i class='fa fa-user-plus'></i></li> <li data-tooltip='Stop Over(s)'><i class='fa fa-dot-circle-o'></i></li></div>";

        var row = {
            DispatchId: this.DispatchId,
            PickUpDateTime: this.PickUpDateTime,
            PickUpPoint: this.PickUpPoint,
            DropOffPoint: this.DropOffPoint,
            PassengerNames: this.PassengerNames,
            //VehicleDispatched: this.VehicleDispatched,
            PassengerOrganisation: this.PassengerOrganisation,
            PassengerTelephone: this.PassengerTelephone,
            NoOfPassengers: this.NoOfPassengers,
            DateModified: this.strDModified,
            DispatchStatus: result_div

        };

        this.visible = true;

        this.row.html(make_tds(row, self))
                .unbind('click')
                .unbind('dblclick');

        this.row.find('.editDialog').click(function (event) {
            event.preventDefault();
            var params = $.param({
                id: self.DispatchId
            });
            $.ajax(
             {
                 url: book_edit_url,
                 type: "POST",
                 data: params,
                 success: function (data, textStatus, jqXHR) {
                     //console.log(data);
                     loadBookData(data);
                 },
                 error: function (jqXHR, textStatus, errorThrown) {
                     //if fails     
                     console.log(jqXHR);
                     console.log(textStatus);
                     console.log(errorThrown);
                 },
                 beforeSend: function () {
                     showWaiting();
                 },
                 complete: function () {
                     closeWaiting();
                 }
             });
            event.preventDefault();
        });

        this.row.find('.editBookDialog').click(function (event) {
            event.preventDefault();
            var params = $.param({
                id: self.DispatchId
            });
            $.ajax(
            {
                url: book_edit_url,
                type: "POST",
                data: params,
                success: function (data, textStatus, jqXHR) {
                    //console.log(data);
                    loadEditBookData(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails     
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                },
                beforeSend: function () {
                    showWaiting();
                },
                complete: function () {
                    closeWaiting();
                }
            });
            event.preventDefault();
        });

        this.row.find('.editDispatchBookDialog').click(function (event) {
            event.preventDefault();
            var params = $.param({
                id: self.DispatchId
            });
            $.ajax(
            {
                url: book_edit_url,
                type: "POST",
                data: params,
                success: function (data, textStatus, jqXHR) {
                    //console.log(data);
                    loadEditDispatchBookData(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails     
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                },
                beforeSend: function () {
                    showWaiting();
                },
                complete: function () {
                    closeWaiting();
                }
            });
        });

        this.row.find('.confirmDialog').click(function (e) {
            // e.preventDefault(); use this or return false
            $("#TxtComment").val("");
            $("#DivPOB").val([]);
            var dataobj = $(this);
            var val = $("button[type=submit][clicked=true]").val();
            var url = $(this).attr('href');
            //document.getElementById('TxtComment').style.display = "none";
            $("#TxtComment").hide();
            $("#dialog-pob").dialog({
                autoOpen: false,
                resizable: false,
                minHeight: "auto",
                width: 350,
                show: { effect: 'drop', direction: "up" },
                modal: true,
                draggable: true,
                buttons: {
                    "Save": function () {
                        var e = document.getElementById("DivPOB");
                        var strPOB = e.options[e.selectedIndex].value;
                        var Comment = $("#TxtComment").val();
                        var params = $.param({
                            idno: self.DispatchId,//getParameterByName(dataobj.attr('href'), 'idno'),
                            PaggengerOnBoardId: strPOB,
                            Comment: Comment
                        });

                        // var url = dataobj.attr('href').substring(0, dataobj.attr('href').indexOf("?"));

                        $(this).dialog("close");
                        $.ajax({
                            type: "POST",
                            data: params,
                            url: dispatch_UpdatePOB_url,
                            success: function () {
                                location.reload(true);
                            },
                            beforeSend: function () {
                                showWaiting();
                            },
                            complete: function () {
                                closeWaiting();
                            }
                        });
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $("#dialog-pob").dialog('open');
            return false;
        });

        this.row.find('.cancelBookDialog').click(function (e) {
            // e.preventDefault(); use this or return false
            $("#TxtComment").val("");
            var dataobj = $(this);
            var val = $("button[type=submit][clicked=true]").val();
            var url = $(this).attr('href');
            $("#dialog-cancel-book").dialog({
                autoOpen: false,
                resizable: false,
                minHeight: "auto",
                width: 350,
                show: { effect: 'drop', direction: "up" },
                modal: true,
                draggable: true,
                buttons: {
                    "Save": function () {
                        var Comment = $("#TxtCommentcancel").val();
                        var params = $.param({
                            idno: self.DispatchId,//getParameterByName(dataobj.attr('href'), 'idno'),
                            PaggengerOnBoardId: 2,
                            Comment: Comment
                        });

                        // var url = dataobj.attr('href').substring(0, dataobj.attr('href').indexOf("?"));

                        $(this).dialog("close");
                        $.ajax({
                            type: "POST",
                            data: params,
                            url: dispatch_UpdatePOB_url,
                            success: function () {
                                location.reload(true);
                            },
                            beforeSend: function () {
                                showWaiting();
                            },
                            complete: function () {
                                closeWaiting();
                            }
                        });
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $("#dialog-cancel-book").dialog('open');
            return false;
        });

        this.row.find('.editDispatchDialog').click(function (e) {
            // e.preventDefault(); use this or return false
            var params = $.param({
                id: self.DispatchId
            });
            $.ajax(
            {
                url: book_edit_url,
                type: "POST",
                data: params,
                success: function (data, textStatus, jqXHR) {
                    //console.log(data);
                    loadDispatchData(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails     
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                },
                beforeSend: function () {
                    showWaiting();
                },
                complete: function () {
                    closeWaiting();
                }
            });
            var dataobj = $(this);
            var val = $("button[type=submit][clicked=true]").val();
            var url = $(this).attr('href');
            $("#dialog-editDispatch").dialog({
                autoOpen: false,
                resizable: false,
                minHeight: "auto",
                width: 350,
                show: { effect: 'drop', direction: "up" },
                modal: true,
                draggable: true,
                buttons: {
                    "Save": function () {
                        var v = document.getElementById("cmbVehicles");
                        var Vehicle = v.options[v.selectedIndex].value;
                        var d = document.getElementById("cmbDrivers");
                        var Driver = d.options[d.selectedIndex].value;
                        var params = $.param({
                            DispatchId: self.DispatchId,
                            DriverUserId: Driver,
                            VehicleId: Vehicle,
                            SendToDriver: $('#ChkEditDriver').is(":checked"),
                            SendToCustomer: $('#ChkEditCustomer').is(":checked"),
                            IsVehicleVisible: $('#ChkIsVehicleVisibleEdit').is(":checked"),
                        });

                        // var url = dataobj.attr('href').substring(0, dataobj.attr('href').indexOf("?"));

                        $(this).dialog("close");
                        $.ajax({
                            type: "POST",
                            data: params,
                            url: dispatch_edit_url,
                            success: function () {
                                location.reload(true);
                            },
                            beforeSend: function () {
                                showWaiting();
                            },
                            complete: function () {
                                closeWaiting();
                            }
                        });
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $("#dialog-editDispatch").dialog('open');
            return false;
        });

        if (values.DispatchColorCode == 0)
            this.row.addClass('dispatched');
        else if (values.DispatchColorCode == 1)
            this.row.addClass('NotDispatched');
        else if (values.DispatchColorCode == 2)
            this.row.addClass('dispatched');
        else if (values.DispatchColorCode == 3)
            this.row.addClass('');
        else if (values.DispatchColorCode == 4)
            this.row.addClass('dispatched');
        else if (values.DispatchColorCode == 5)
            this.row.addClass('');
        else if (values.DispatchColorCode == 6)
            this.row.addClass('vip');
        else if (values.DispatchColorCode == 7)
            this.row.addClass('dispatched');

        if (the_dataTable.row(this.row).length > 0) {
            var row_data = [];
            this.row.find("td").each(function () {
                row_data.push($(this).text());
            });

            the_dataTable.row(this.row)
            .data(row_data)
            .draw();
        }
    };

    this.submit_edit = function () {
        var dID = this.DispatchId;
        var params = $.param({
            DispatchId: dID,
            DispatchStatusId: status ? 2 : 4
        });

        $.ajax(
            {
                url: book_edit_url,
                type: "POST",
                success: function (data, textStatus, jqXHR) {
                    //console.log(data);
                    loadBookData(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails     
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                },
                beforeSend: function () {
                    showWaiting();
                },
                complete: function () {
                    closeWaiting();
                }
            });
        //send_to_backend(dispatch_edit_url, params)
    };

    this.submit_callback = function () {
        var dID = this.DispatchId;
        var params = $.param({
            DispatchId: dID,
            DispatchStatusId: status ? 2 : 4
        });

        send_to_backend(dispatch_UpdateCallBack_url, params);
    };

    this.select = function () {
        the_dataTable.find('tr').removeClass('sel').find('td').removeClass('sel');
        this.row.addClass('sel').find('td').addClass('sel');
        selected_vehicle = this;
    };

    this.setVisible = function (x) {
        this.visible = x;

        if (x) {
            this.row.show();
        } else {
            this.row.hide();
        }
    };

    function make_tds(vals, self) {
        var result = '';
        for (var v in vals) {
            if (vals[v] == null) {
                result += '<td class="' + v + '">&nbsp;</td>';
            } else {
                var dat = (vals[v].toString() == '' ? '&nbsp;' : vals[v].toString());
                result += '<td alt="' + dat + '" title="' + dat + '" class="' + v + '">' + dat + '</td>';
            }

        }
        return result
    }


    this.row = $('<tr />');
    this.marker = null;
    this.update(values);

    the_dataTable.row.add(this.row).draw();
}

function comit_and_remove_nots(url, params, elemnt) {

}

function update_vehicles() {
    if (update_pending) {
        return;
    }
    if (vehicles.length < 1) {
        first_load();
        return;
    }

    update_pending = true;

    var highest = vehicles[0].strDModified;
    $.each(vehicles, function () {
        highest = highest == null ? this.strDModified : this.strDModified > highest && this.strDModified != null ? this.strDModified : highest;
    });

    var params = $.param({
        highest: highest
    });

    $.ajax({
        url: '/VehicleDispatch/getLatestDispatchData',
        type: 'POST',
        dataType: 'json',
        data: params,
        success: function (data) {
            update_pending = false;
            var result_new = [];
            $.each(data.VehicleDispatch, function () {
                var new_data = this;
                $.each(vehicles, function () {
                    var cur_vehicle = this;
                    if (this.DispatchId == new_data.DispatchId) {
                        this.update(new_data);
                    }
                    //result_new.push($.grep(data.VehicleDispatch, function (e) { return e.DispatchId == cur_vehicle.DispatchId; }));
                });
                var exists = $.grep(vehicles, function (e) { return e.DispatchId == new_data.DispatchId; });
                if (exists.length == 0)
                    result_new.push(new_data);
            });
            $.each(result_new, function () {
                var role = data.isDispatch && data.isBooking ? 1 : (data.isDispatch ? 0 : data.isBooking ? -1 : -2);
                vehicles.push(new vehicledispatch(this, role));
            });
            update_pending = false;
            the_dataTable
                .order([[1, 'asc']])
                .draw(false);
            if (data.VehicleDispatch.length > 0) {
                playAlertSound();
                $.each(data.VehicleDispatch, function () {
                    var Dialog = "";
                    var role = data.isDispatch && data.isBooking ? 1 : (data.isDispatch ? 0 : data.isBooking ? -1 : -2);
                    if (role == -1) {
                        Dialog = "<tr><td><div class='dialog_not'><p class='h'>New Booking</p>" +
                            "<p>Passenger: " + this.PassengerNames + "<br/>" +
                            "Pick-Up Point: " + this.PickUpPoint + "<br/>" +
                            "Pick-Up Date Time: " + this.PickUpDateTime + "</p>" +
                            //"<div><button class='warning small editDialog_not' id='editDialog_" + this.DispatchId + "'>Dispatch</button>" +
                            "<button style='padding: 0!important; border: 0!important;' class='success small editDispatchBookDialog_not' id='editDispatchDialog_" + this.DispatchId + "'>Edit</button></div>" +
                            "</div></td></tr>";
                    }
                    else if (role == 0) {
                        Dialog = "<tr><td><div class='dialog_not'><p class='h'>New Booking</p>" +
                           "<p>Passenger: " + this.PassengerNames + "<br/>" +
                           "Pick-Up Point: " + this.PickUpPoint + "<br/>" +
                           "Pick-Up Date Time: " + this.PickUpDateTime + "</p>" +
                           "<div><button style='padding: 0!important; border: 0!important;' class='warning small editDialog_not' id='editDialog_" + this.DispatchId + "'>Dispatch</button>" +
                           //"<button class='success small editDispatchBookDialog_not' id='editDispatchDialog_" + this.DispatchId + "'>Edit</button></div>" +
                           "</div></td></tr>";
                    }
                    else if (role == 1) {
                        Dialog = "<tr><td><div class='dialog_not'><p class='h'>New Booking</p>" +
                           "<p>Passenger: " + this.PassengerNames + "<br/>" +
                           "Pick-Up Point: " + this.PickUpPoint + "<br/>" +
                           "Pick-Up Date Time: " + this.PickUpDateTime + "</p>" +
                           "<div><button style='padding: 0!important; border: 0!important;' class='warning small editDialog' id='editDialog_" + this.DispatchId + "'>Dispatch</button>" +
                           "<button style='padding: 0!important; border: 0!important;' class='success small editDispatchBookDialog_not' id='editDispatchDialog_" + this.DispatchId + "'>Edit</button></div>" +
                           "</div></td></tr>";
                    }
                    if (Dialog != "") {
                        //NotificationBar.on('click', $(Dialog).find('editDialog_" + self.DispatchId+'),)
                        //$(Dialog).find('.editDialog_not').click(
                        // $(Dialog).find('.editDispatchBookDialog_not').click(
                        $(Dialog).prependTo(NotificationBar);
                        //NotificationBar.on('click', $(Dialog).find('editDialog_'+ self.DispatchId),
                        NotificationBar.find('#editDialog_' + this.DispatchId).on('click', function (event) {
                            event.preventDefault();
                            var parent_row = $('#editDialog_' + parseInt(this.id.replace('editDialog_', ''))).closest('tr');

                            var params = $.param({
                                id: parseInt(this.id.replace('editDialog_', ''))
                            });
                            $.ajax(
                             {
                                 url: book_edit_url,
                                 type: "POST",
                                 data: params,
                                 success: function (data, textStatus, jqXHR) {
                                     loadBookData(data);
                                     parent_row.remove();
                                 },
                                 error: function (jqXHR, textStatus, errorThrown) {
                                     //if fails     
                                     console.log(jqXHR);
                                     console.log(textStatus);
                                     console.log(errorThrown);
                                 },
                                 beforeSend: function () {
                                     showWaiting();
                                 },
                                 complete: function () {
                                     closeWaiting();
                                 }
                             });
                        });
                        NotificationBar.find('#editDispatchDialog_' + this.DispatchId).on('click', function (event) {
                            event.preventDefault();
                            var parent_row = $('#editDispatchDialog_' + parseInt(this.id.replace('editDispatchDialog_', '')));
                            var params = $.param({
                                id: parseInt(this.id.replace('editDispatchDialog_', ''))
                            });
                            $.ajax(
                            {
                                url: book_edit_url,
                                type: "POST",
                                data: params,
                                success: function (data, textStatus, jqXHR) {
                                    //console.log(data);
                                    loadEditDispatchBookData(data);
                                    parent_row.remove();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    //if fails     
                                    console.log(jqXHR);
                                    console.log(textStatus);
                                    console.log(errorThrown);
                                },
                                beforeSend: function () {
                                    showWaiting();
                                },
                                complete: function () {
                                    closeWaiting();
                                }
                            });
                        });
                    }
                    $('.dialog_not').niceScroll();
                });
            }
        },
        error: ajax_error
    });
}
function playAlertSound() {
    //if ($('#opt1_1').attr('checked')) {
    $('#off-screen').html('<embed src="' + soundpath + '" autostart="true" loop="false" hidden="true"></embed>');
    setTimeout(function () { $('#off-screen').html(''); }, 6000);
    //}
}
function send_to_backend(url, params) {
    $.ajax({
        url: url,
        type: 'POST',
        data: params,
        success: function (data) {
            if (data.Status) {
                show_notification(data.Message, "success");
            }
            else {
                show_notification(data.message, "error");
            }
            console.log(data);
        },
        error: ajax_error
    });
}

function ajax_error(request, text, error) {
    update_pending = false;
    //close_progress();
    console.log('request: ' + request.responseText);
    console.log('text: ' + text);
    console.log('error: ' + error);
}

function first_load() {
    update_pending = true;
    $.ajax({
        url: '/VehicleDispatch/GetDispatchData',
        type: 'POST',
        dataType: 'json',
        error: ajax_error,
        success: function (data) {
            if (data != null) {
                initial_load(data);
                update_pending = false;
                the_dataTable
                    .order([[1, 'asc']])
                    .draw(false);
            }
        },
        error: ajax_error
    });
}