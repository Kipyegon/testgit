﻿/**
All rights are reserved. Reproduction or transmission in whole or in part, in
any form or by any means, electronic, mechanical or otherwise, is prohibited
without the prior written consent of the copyright owner.

Filename        : MapDirections.js
Purpose         : MapDirections Funtionalities
Creation Date   : July 14 2010
Author          : Greepty.Gopal
**/

var map;
var geocoder;
var marker;
var directionRenderer;
var directionService = new google.maps.DirectionsService();
var arrayAlternativePaths = new Array();
var arrayRoutePathHTML = new Array();
var lat = null;
var lon = null;
var infowindow = null;
var zoonLevel = 5;
var panorama;
var sv = new google.maps.StreetViewService();
var arrayWayPoints = [];
var imageToReturn = '';
var textToReturn = '';
var destinationIcon = '';
var myTableId = "tbl";
var totalJourneyDistance = 0;
var totalJourneyDuration = 0;

function fnInitializeMap() {
    console.log('fnInitializeMap');
    if (map == null) {
        console.log('map == null');
        var centerLatLng = new google.maps.LatLng(51.5007, -0.1264)
        var position;

        //        if (lat != null && lon != null) {
        //            position = new google.maps.LatLng(lat, lon);
        //        }

        //        if (position != null) {
        //            centerLatLng = position;
        //            zoonLevel = $("#hdnSelectedZoom").val();
        //        }

        var myOptions = {
            zoom: parseInt(zoonLevel),
            center: centerLatLng,
            streetViewControl: false,
            navigationControl: true,
            navigationControlOptions: { style: google.maps.NavigationControlStyle.LARGE },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        console.log(document.getElementById('map_canvas'))
        map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
        console.log(document.getElementById('map_canvas'))

        //GEOCODER
        geocoder = new google.maps.Geocoder();

        //        var height = (map.getDiv().clientHeight) + 'px';
        //        $("#dvLeftContent").height(height);

        // Create new control to display latlng and coordinates under mouse.
        //var latLngControl = new LatLngControl(map);

        marker = new google.maps.Marker({
            map: map,
            draggable: true
        });
        // Register event listeners
        google.maps.event.addListener(map, 'mouseover', function (mEvent) {
            //latLngControl.set('visible', true);
        });
        google.maps.event.addListener(map, 'mouseout', function (mEvent) {
            //latLngControl.set('visible', false);
        });
        google.maps.event.addListener(map, 'mousemove', function (mEvent) {
            //latLngControl.updatePosition(mEvent.latLng);
        });

        google.maps.event.addListener(map, 'zoom_changed', function () {
            $("#hdnSelectedZoom").val(map.getZoom());
        });

        google.maps.event.addListener(marker, 'drag', function () {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                    }
                }
            });

        });

        directionRenderer = new google.maps.DirectionsRenderer();
        directionRenderer.setMap(map);

        initializePanorama();
    }
    else {
        console.log('map != null');
    }
//    if (map != null) {
//        if ($("#hdnSetRouteIndexFlag").val() == 1) {
//            if ($("#hdnAllDestinations").val() != "" && $("#hdnAllDestinations").val() != null) {
//                var arrDestPoints = $("#hdnAllDestinations").val().split(";");
//                $("#txtStartAddress").val(arrDestPoints[0]);
//                $("#txtDestinationAddress").val(arrDestPoints[1]);

//                for (var wayCnt = 2; wayCnt < arrDestPoints.length; wayCnt++) {
//                    fnAddDestination(arrDestPoints[wayCnt]);
//                }
//            }

//            PlotRoutesOnMap();
//        }
//    }
}

function initializePanorama() {
    panorama = map.getStreetView();
    var centerLatLng = map.getCenter();
    panorama.setPosition(centerLatLng);
    panorama.setPov({
        heading: 265,
        zoom: 1,
        pitch: 0
    });

    google.maps.event.addListener(panorama, 'closeclick', function() {
        var toggle = panorama.getVisible();
        panorama.setVisible(false);
    });
}

function fnHideStreetPanorma() {
    if (panorama != null) {
        panorama.setVisible(false);
    }

    if (infowindow != null) {
        infowindow.close();
    }    
}

function PlotRoutesOnMap() {
    if (($("#txtStartAddress").val() != '') && ($("#txtDestinationAddress").val() != '')) {        

        var startDestination = $("#txtStartAddress").val();
        var endDestination = $("#txtDestinationAddress").val();
        var selectedUnit = $('input[name=rdoUnit]:checked').val();
        var txtEntries = [];
        var wayPoints = [];

        $("#dvDirections").find(":text").each(function() {
            txtEntries.push($(this).val());
        });

        for (var wpCnt = 1; wpCnt < txtEntries.length - 1; wpCnt++) {
            wayPoints.push({ location: txtEntries[wpCnt], stopover: true });
        }

        if (txtEntries.length > 2) {
            startDestination = txtEntries[0];
            endDestination = txtEntries[txtEntries.length - 1];
        }

        $("#dir_title").html("Driving Directions to " + endDestination);

        if (startDestination != "" && endDestination != "") {
            var request = {
                origin: startDestination,
                destination: endDestination,
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
                unitSystem: (selectedUnit == 'M') ? google.maps.DirectionsUnitSystem.METRIC :
                google.maps.DirectionsUnitSystem.IMPERIAL,
                provideRouteAlternatives: true,
                waypoints: wayPoints,
                avoidHighways: $('#chkHighways').is(':checked'),
                avoidTolls: $('#chkTolls').is(":checked")
            };

            directionService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {

                    map.setZoom(parseInt(zoonLevel));
                    fnHideStreetPanorma();

                    $("#dvSuggestedRoutes").hide();
                    $("#map_canvas").height("95%");

                    directionRenderer.setDirections(response);
                    $("#hdnSelectedRouteIndex").val('0');
                    directionRenderer.setRouteIndex(parseInt($("#hdnSelectedRouteIndex").val()));
                    ShowJSonRoutes(response);
                }
                else {
                    var startDestination = $("#dvDirections input:first").val();
                    var endDestination = $("#dvDirections input:last").val();

                    var message = "<img src='" + siteURL + "images/MapDirection/error.png' /><span class='errorMessage'> We could not calculate directions between " + startDestination
                        + " and " + endDestination + ".</span>";
                    $("#dvSuggestedRoutes").hide();
                    $("#dir_title").hide();
                    $("#dvDetails").html(message);
                    $("#dvLegal").hide();
                    $("#dvCopyRight").hide();
                }
            });
        }
    }
    else {
        if (parseInt(fnReturnTextboxCount()) == 2) {
            alert("Please enter destination A and B to plot driving directions.");
        }
        return false;
    }
}

function fnShowDirectionsOnMap(routeIndex) {

    fnHideStreetPanorma();
    
    directionRenderer.setRouteIndex(routeIndex);
    $("#hdnSelectedRouteIndex").val(routeIndex);
    ShowJSonSteps(routeIndex);
}

function ShowJSonRoutes(results) {
    var htmlRouteContent = "";

    if (results.routes.length > 1) {
        htmlRouteContent = "<table border='0' cellspacing='1' cellpadding='1' width='100%' id='" + myTableId + "'>";

        $.each(results.routes, function(routeIndex, route) {
            var text = route.summary;
            var bgColor = (routeIndex == 0) ? "trclick" : "";

            htmlRouteContent += "<tr id='trRoutes' style='height:2em;padding-bottom:2px;cursor:pointer;' class='" + bgColor + "' onclick='fnSetRowStyle(this," + routeIndex + ");'>"
                                + "<td><span><b>" + text + "</b><span><div>" + route.legs[0].distance.text + "<div style='float:right;'>" + route.legs[0].duration.text + "</div></td>"
                                + "</tr>";
            arrayAlternativePaths[routeIndex] = route;
        });

        htmlRouteContent += "</table>";

        if (htmlRouteContent != "") {
            $("#dvSuggestedRoutes").show();
            $("#dvRoutes").html(htmlRouteContent);
        }
    }
    else {
        $("#dvSuggestedRoutes").hide();
        arrayAlternativePaths[0] = results.routes[0];
    }

    if (htmlRouteContent != "") {
        $("#dvLegal").show();
        $("#dvCopyRight").html(results.routes[0].copyrights);
    }

    ShowJSonSteps(0);
}

function fnSetRowStyle(tr, routeIndex) {
    $("#" + myTableId).find('tr').each(function() {
        $(this).removeClass("trclick");
    })

    $(tr).addClass("trclick");
    fnShowDirectionsOnMap(routeIndex);
}

function ShowJSonSteps(routeIndex) {
    var route = arrayAlternativePaths[routeIndex];
    $("#dvDetails").html('');
    if (route != null) {
        htmlStep = "<table cellspacing='0' cellspadding='0' border='0' class='mapdirdetails'>";
        var legs = route.legs;
        var htmlContentString = "";

        htmlContentString += fnGetAllDestinations() + "||";

        totalJourneyDistance = 0;
        totalJourneyDuration = 0;
        
        $.each(legs, function(i, leg) {

            var steps = leg.steps;
            var imgLegStart = "imgLegSV_" + (i + 1);
            var spStartLeg = "spLeg_" + (i + 1);
            var legStartLat = leg.start_location.lat();
            var legStartLng = leg.start_location.lng();

            htmlContentString += leg.start_address + "##";
            htmlContentString += leg.distance.text + "##";
            htmlContentString += leg.duration.text;

            //Journey Distance and duration calcualtion
            totalJourneyDistance += leg.distance.value;
            totalJourneyDuration += leg.duration.value;
            //End calcualtion

            destinationIcon = fnReturnImageUrlOrDisplayText((i + 1), 2);
            fnCheckStreetViewExists(legStartLat, legStartLng, imgLegStart);

            htmlStep += "<tr class='mapdirdetailsDiv' style='cursor:pointer;' onclick='ShowPointOnMap(" + legStartLat + ", " + legStartLng + ", \"" + imgLegStart + "\", \"" + spStartLeg + "\");'>"
            htmlStep += "<td class='mapdirdetailsTD'><span style='float:left;padding:5px 0px 0px 0px;'><img src='" + destinationIcon + "' alt='" +
               leg.start_address + "' title='" + leg.start_address + "'/></span></td>"
            htmlStep += "<td class='mapdirdetailsTD'><span id='" + spStartLeg + "' style='float:left;padding:15px 0px 0px 5px;'>" + leg.start_address + "</span></td>"
            htmlStep += "<td class='mapdirdetailsTD' style='width:10%;'><span style='float:left;padding-top:15px;padding-left:2px;'><img id='" + imgLegStart + "' src='images/MapDirection/camera.png' alt='Street View' title='Street View' style='cursor:pointer'/></span></td>"
            htmlStep += "<td class='mapdirdetailsTD'>&nbsp;</td></tr>";

            $.each(steps, function(j, step) {
                var lat = step.start_point.lat();
                var lng = step.start_point.lng();
                var svId = "imgSV_" + i + "_" + j;
                var spnId = "sp_" + i + "_" + j;
                htmlStep += "<tr  onmouseover='fnFocusRow(this);' style='cursor:pointer;' onclick='ShowPointOnMap(" + lat + ", " + lng + ", \"" + svId + "\", \"" + spnId + "\");' >";
                htmlStep += "<td  style='width:10%;vertical-align:text-top;' class='mapdirdetailsTD'>" + (j + 1) + "." + "</td>";
                htmlStep += "<td  style='width:60%;' class='mapdirdetailsTD'><span id='" + spnId + "'>" + step.instructions + "</span></td>";
                htmlStep += "<td class='mapdirdetailsTD' style='width:10%;'><img id='" + svId + "' src='images/MapDirection/camera.png' alt='Street View' title='Street View' style='cursor:pointer'  /></td>";
                htmlStep += "<td class='dirdistance mapdirdetailsTD'><span id='" + spnId + "'>" + step.distance.text + "</span></td>";
                htmlStep += "</tr>";

                htmlContentString += "##" + [j + 1] + "."
                    + "::" + fnRemoveHTMLTags(step.instructions)
                    + "::" + step.distance.text;

                fnCheckStreetViewExists(lat, lng, svId);
            });

            htmlStep += "<tr><td  class='distanceMeasureText' colspan='4'>";
            htmlStep += leg.distance.text + "- about " + leg.duration.text;
            htmlStep += "</td></tr>";

            htmlContentString += "^^";
        });

        arrayRoutePathHTML[routeIndex] = htmlContentString;

        var legEndLat = legs[legs.length - 1].end_location.lat();
        var legEndLng = legs[legs.length - 1].end_location.lng();
        var imgLegEnd = "imgLegEnd";
        var spEndLeg = "spEndLeg";
        fnCheckStreetViewExists(legEndLat, legEndLng, imgLegEnd);
        destinationIcon = fnReturnImageUrlOrDisplayText(legs.length + 1, 2);

        htmlStep += "<tr class='mapdirdetailsDiv' style='cursor:pointer;' onclick='ShowPointOnMap(" + legEndLat + ", " + legEndLng + ", \"" + imgLegEnd + "\", \"" + spEndLeg + "\");'>"
        htmlStep += "<td class='mapdirdetailsTD'><span style='float:left;padding:5px 0px 0px 0px;'><img src='" + destinationIcon + "' alt='" +
               legs[legs.length - 1].end_address + "' title='" + legs[legs.length - 1].end_address + "'/></span></td>"
        htmlStep += "<td class='mapdirdetailsTD'><span id='" + spEndLeg + "' style='float:left;padding:15px 0px 0px 5px;'>" + legs[legs.length - 1].end_address + "</span></td>"
        htmlStep += "<td class='mapdirdetailsTD' style='width:10%;'><span style='float:left;padding-top:15px;padding-left:2px;'><img id='" + imgLegEnd + "' src='images/MapDirection/camera.png' alt='Street View' title='Street View' style='cursor:pointer'/></span></td>"
        htmlStep += "<td class='mapdirdetailsTD'>&nbsp;</td></tr>";
        
        htmlStep += "</table>";
        $("#dvDetails").html(htmlStep);

        if (legs.length > 1) {
            var selectedUnit = $('input[name=rdoUnit]:checked').val();

            if (totalJourneyDistance > 0 && totalJourneyDuration > 0) {

                var distanceInKM = Math.floor(totalJourneyDistance / 1000);
                
                var calculatedDistance = 0;

                if (selectedUnit == "M") {
                    calculatedDistance = Math.ceil(distanceInKM) + " km";
                }
                else {
                    calculatedDistance = Math.ceil(distanceInKM / 1.609344) + " mi";
                }               
                
                var days = Math.floor(totalJourneyDuration / 86400);
                var hours = Math.floor((totalJourneyDuration - (days * 86400)) / 3600);
                var minutes = Math.floor((totalJourneyDuration - (days * 86400) - (hours * 3600)) / 60)
                var textToDisplay = "<b>" + fnFormatNumberWithCommas(calculatedDistance) + "</b> - about <b>" + days + " Day(s) " + hours + " hours " + minutes + " mins</b>";
                $("#dir_title").append("<br/><span style='padding-top:2px;font-size: 11px;font-family: Arial, Helvetica, sans-serif;color:#737373;'" + textToDisplay + "</span>");
            }
        }
    }
}

function fnReturnTextboxCount() {
    var textBoxCount = 0;
    $("#dvDirections").find(':text').each(function() {
        textBoxCount++;
    });

    return textBoxCount;
}

function fnAddDestination(val) {
    var textBoxCount = 0;

    textBoxCount = fnReturnTextboxCount();
    $("#dvReverse").hide();

    var imageUrl = fnReturnImageUrlOrDisplayText(parseInt(textBoxCount + 1), 0);
    var displayText = fnReturnImageUrlOrDisplayText(parseInt(textBoxCount + 1), 1);

    if (displayText != "" && displayText != null && imageUrl != "" && imageUrl != null) {
        var htmlObject =
        "<div  id='dv" + (textBoxCount + 1) + "' style='padding-top:2px;'>"
            + "<img src='" + imageUrl + "' alt='" + displayText + "' title='" + displayText + "' style='padding-right: 3px;cursor:pointer;' />"
            + "<input type='text' name='address' id='txt" + (textBoxCount + 1) + "' value='" + val + "'  class='maptxbx'  onblur='fnAssignValues(\"txt" + (textBoxCount + 1) + "\");'/>"
            + "<img src='" + siteURL + "images/MapDirection/cross.gif' style='cursor:pointer;padding-left:2px;' alt='Remove' title='Remove' onClick='fnRemoveDestination(\"dv" + (textBoxCount + 1) + "\",\"txt" + (textBoxCount + 1) + "\"); return false;'/>"
        + "</div>";

        $("#dvMoreDestinations").append(htmlObject);

        var valueToPush = ("txt" + (textBoxCount + 1));
        arrayWayPoints[valueToPush] = $("#txt" + (textBoxCount + 1)).val();
        SetAutoComplete("txt" + (textBoxCount + 1));

        $("#dvLeftPane").height(($("#dvLeftPane").height() + 25) + "px");
    }
    else {
        alert("You can add a maximum of 8 destinations.");
    }
    return false;
}

function fnReturnImageUrlOrDisplayText(id, returnType) {

    if (id != "" && id != null) {
        switch (id) {
            case 1:
                imageToReturn = siteURL + "images/MapDirection/a.gif";
                destinationIcon = siteURL + "images/MapDirection/iconA.png";
                textToReturn = "A";
                break;
            case 2:
                imageToReturn = siteURL + "images/MapDirection/b.gif";
                destinationIcon = siteURL + "images/MapDirection/iconB.png";
                textToReturn = "B";
                break;
            case 3:
                imageToReturn = siteURL + "images/MapDirection/c.gif";
                destinationIcon = siteURL + "images/MapDirection/iconC.png";
                textToReturn = "C";
                break;
            case 4:
                imageToReturn = siteURL + "images/MapDirection/d.gif";
                destinationIcon = siteURL + "images/MapDirection/iconD.png";
                textToReturn = "D";
                break;
            case 5:
                imageToReturn = siteURL + "images/MapDirection/e.gif";
                destinationIcon = siteURL + "images/MapDirection/iconE.png";
                textToReturn = "E";
                break;
            case 6:
                imageToReturn = siteURL + "images/MapDirection/f.gif";
                destinationIcon = siteURL + "images/MapDirection/iconF.png";
                textToReturn = "F";
                break;
            case 7:
                imageToReturn = siteURL + "images/MapDirection/g.gif";
                destinationIcon = siteURL + "images/MapDirection/iconG.png";
                textToReturn = "G";
                break;
            case 8:
                imageToReturn = siteURL + "images/MapDirection/h.gif";
                destinationIcon = siteURL + "images/MapDirection/iconH.png";
                textToReturn = "H";
                break;
            default:
                imageToReturn = "";
                textToReturn = "";
                destinationicon = siteURL + "images/MapDirection/defaultDest.png";
                break;
        }
    }

    if (returnType == 0) {  //ImageUrl
        return imageToReturn;
    } else if (returnType == 1) { //ImageText
        return textToReturn;
    } else if (returnType == 2) { //DestinationIcon
        return destinationIcon;
    }
}

function fnRemoveDestination(dvDestToRemove, txt) {
    $("#" + dvDestToRemove).remove();

    var newId = 2;
    var newArray = new Array();
    var id = (newId + 1);

    $("#dvMoreDestinations").html('');

    for (var key in arrayWayPoints) {
        //alert("key is :" + key + " and value is " + arrayWayPoints[key]);

        var value = arrayWayPoints[key];

        if (typeof value == 'undefined') {
            value = "";
        }

        if (key == txt) {
            delete arrayWayPoints[key];
        }
        else {
            var imageUrl = fnReturnImageUrlOrDisplayText(parseInt(id), 0);
            var displayText = fnReturnImageUrlOrDisplayText(parseInt(id), 1);

            if (displayText != "" && displayText != null) {
                var htmlObject =
                   "<div  id='dv" + id + "' style='padding-top:2px;'>"
                        + "<img src='" + imageUrl + "' alt='" + displayText + "' title='" + displayText + "' style='padding-right: 3px;cursor:pointer;' />"
                        + "<input type='text' name='address' id='txt" + id + "' class='maptxbx'  onblur='fnAssignValues(\"txt" + id + "\");' value='" + value + "'/>"
                        + "<img src='" + siteURL + "images/MapDirection/cross.gif' "
                        + "style='cursor:pointer;padding-left:2px;' alt='Remove' title='Remove' "
                        + " onClick='fnRemoveDestination(\"dv" + id + "\",\"txt" + id + "\"); return false;'/>"
                    + "</div>";

                $("#dvMoreDestinations").append(htmlObject);

                var valueToPush = ("txt" + id);
                newArray[valueToPush] = value;
                SetAutoComplete(id);
                id++;
            }
        }
    }

    $("#dvLeftPane").height(($("#dvLeftPane").height() - 25) + "px");

    arrayWayPoints = null;
    arrayWayPoints = newArray;

    var textBoxCount = 0;
    textBoxCount = fnReturnTextboxCount();

    if (textBoxCount > 2) {
        $("#dvReverse").hide();
    }
    else {
        $("#dvReverse").show();
    }

    if (textBoxCount == 2) {
        if (($("#txtStartAddress").val() != '') && ($("#txtDestinationAddress").val() != '')) {
            PlotRoutesOnMap();
        }
    }
    else {
        PlotRoutesOnMap();
    }

    return false;
}

function fnAssignValues(txtId) {
    arrayWayPoints[txtId] = $("#" + txtId).val();
}

function fnGetAllDestinations() {
    var txtEntries = [];
    var destinationHTML = '';

    $("#dvDirections").find(":text").each(function() {
        txtEntries.push($(this).val());
    });

    destinationHTML = txtEntries.join(";");

    return destinationHTML
}

function ShowPointOnMap(lat, lng, svId, spnId) {
    var point = new google.maps.LatLng(lat, lng);
    map.setCenter(point);
    map.setZoom(15);

    if (infowindow != null) {
        infowindow.close();
    }
    
    infowindow = new google.maps.InfoWindow(
    {
        content: $("#" + spnId).html(),
        size: new google.maps.Size(50, 50),
        position: point
    });

    if ($("#" + svId).is(":hidden")) {
        panorama.setVisible(false);
    }
    else {
        panorama = map.getStreetView();
        panorama.setPosition(point);
        panorama.setPov({
            heading: 265,
            zoom: 1,
            pitch: 0
        });
        panorama.setVisible(true);
    }

    infowindow.open(map);
}

function fnCheckStreetViewExists(lat, lng, svId) {

    var point = new google.maps.LatLng(lat, lng);
    sv.getPanoramaByLocation(point, 50, function(data, status) {
        if (status == google.maps.StreetViewStatus.OK) {
            $("#" + svId).show();
        }
        else {
            $("#" + svId).hide();
        }
    });
}

function fnRemoveHTMLTags(htmlString) {
    if (htmlString) {
        var mydiv = document.createElement("div");
        mydiv.innerHTML = htmlString;

        if (document.all) // IE Stuff
        {
            return mydiv.innerText;
        }
        else // Mozilla does not work with innerText
        {
            return mydiv.textContent;
        }
    }
}

function fnPrintMap() {
    if (directionRenderer != null) {
        $("#hdnRoadViewData").val(arrayRoutePathHTML[directionRenderer.getRouteIndex()]);
        $("#hdnSelectedZoom").val(map.getZoom());
        $("#hdnSelectedRouteIndex").val(directionRenderer.getRouteIndex());

        var height = (screen.availHeight);
        var width = (screen.availWidth);

        if (($("#hdnRoadViewData").val() != null) && ($("#hdnRoadViewData").val() != '')) {
            var features = "height=" + height + ",width=" + width + ",scrollTo,resizable=1,menubar=1,scrollbars=1,location=0";
            window.open("PrintMapDirections.aspx", 'Popup', features);
        }
    }

    return false;
}

function fnSendMail() {
    if (directionRenderer != null) {
        $("#hdnRoadViewData").val(arrayRoutePathHTML[directionRenderer.getRouteIndex()]);
        $("#hdnSelectedZoom").val(map.getZoom());
        $("#hdnSelectedRouteIndex").val(directionRenderer.getRouteIndex());

        if (($("#hdnRoadViewData").val() != null) && ($("#hdnRoadViewData").val() != '')) {
            ShowPopup();
        }
    }

    return false;
}

function ShowDirectionPanel() {
    $("#dvLeftPane").show();
    return false;
}

function HideDirectionPanel() {
    $("#dvLeftPane").hide();
    return false;
}

function fnToggleImages(flgHide) {
    if (parseInt(flgHide) == 0) {
        $("#imgExpand").show();
        $("#imgCollapse").hide();
        $("#dvRoutes").hide();
    }
    else if (parseInt(flgHide) == 1) {
        $("#imgExpand").hide();
        $("#imgCollapse").show();
        $("#dvRoutes").show();
    }
}

function fnManageOptionsVisibility() {
    if ($("#dvOptions").is(':hidden') == true) {
        $("#dvOptions").show();
        $("#spanOptions").html('Hide Options');
    }
    else if ($("#dvOptions").is(':hidden') == false) {
        $("#dvOptions").hide();
        $("#spanOptions").html('Show Options');
    }
    return false;
}

function fnReverseDirections() {
    var startDestination = $("#txtStartAddress").val();
    var endDestination = $("#txtDestinationAddress").val();

    $("#txtStartAddress").val(endDestination);
    $("#txtDestinationAddress").val(startDestination);    

    if (panorama != null) {
        panorama.setVisible(false);
    }

    var content = $.trim($("#dvDirections").html());

    if (content != '' && content != null) {
        $("#hdnRoadViewData").val('');
        PlotRoutesOnMap();
    }
}

function fnHandleErrors() {
    if (status == google.maps.DirectionsStatus.INVALID_REQUEST)
        alert("This request was invalid");
    else if (status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT)
        alert("The webpage has gone over the requests limit in too short a period of time.");
    else if (status == google.maps.DirectionsStatus.REQUEST_DENIED)
        alert("The webpage is not allowed to use the elevation service for some reason.");
    else if (status == google.maps.DirectionsStatus.UNKNOWN_ERROR)
        alert("A geocoding, directions or elevation request could not be successfully processed, yet the exact reason for the failure is not known.");
    else alert("An unknown error occurred.");
}

function SetAutoComplete(txtId) {
    $(function() {
        $("#" + txtId).autocomplete({
            //This bit uses the geocoder to fetch address values
            minLength: 2,
            autofill: true,
            source: function(request, response) {

                geocoder.geocode({ 'address': request.term, 'region': 'GB' }, function(results, status) {
                    response($.map(results, function(item) {
                        return {
                            label: item.formatted_address,
                            value: item.formatted_address,
                            latitude: item.geometry.location.lat(),
                            longitude: item.geometry.location.lng()
                        }
                    }));
                })
            },
            //This bit is executed upon selection of an address
            select: function(event, ui) {
                $("#latitude").val(ui.item.latitude);
                $("#longitude").val(ui.item.longitude);
                /*  var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                marker.setPosition(location);
                map.setCenter(location);*/
            }
        });
    });
}


function fnFocusRow(tr) {
    $(tr).hover(
        function() {
            //$(this).css({ 'background-color': '#e4e5e8' }); //mouseover
            $(this).css({ 'background-color': '#eeeeee' }); //mouseover
        },
        function() {
            $(this).css({ 'background-color': 'white' }); // mouseout
        });
}

function fnFormatNumberWithCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
