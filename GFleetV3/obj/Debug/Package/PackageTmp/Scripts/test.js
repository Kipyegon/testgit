﻿// Copyright (c) 2013 - NMA developments
var locateInterval = 10000,
    updateInterval = 10000,
    streetZoom = 18,
    townZoom = 15,
    canLocate = true,
    updatePending = false,
    map = null,
    vehicles = [],
    selectedVehicle = null;

function div() {
    return $(document.createElement('div'));
}

function log(x) {
    if (typeof console != 'undefined') {
        console.log(x);
    }
}

function metresToLatLngApprox(metres) {
    return metres / 111300;
}

function openProgressIndicator(description) {
    $.blockUI({
        message: description + '<br /><br /><img src="images/loading.gif" alt="[loading]" />',
        css: {
            border: 'none',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            padding: '15px',
            backgroundColor: '#fff',
            color: '#000',
            opacity: '1'
        }
    });
}

function closeProgress() {
    $.unblockUI();
}

function daysBetween(from, to) {
    return (to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24);
}

function post(url, description, params, success, userError) {
    var error = typeof userError != 'undefined'
        ? userError
        : function (x) {
            updatePending = false;
            alert(x);
        };

    if (description != '') {
        openProgressIndicator(description);
    }

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: params,
        timeout: 60000,
        error: ajaxError,
        success: function (data) {
            updatePending = false;
            closeProgress();
            data.success ? success(data.result) : error(data.error);
        }
    });
}

function ajaxError(request, text, error) {
    updatePending = false;
    closeProgress();
    log('request: ' + request.responseText);
    log('text: ' + text);
    log('error: ' + error);
}

function latLng(lat, lng) {
    return new google.maps.LatLng(lat, lng);
}

function createMarker(iconPath, lat, lng, text, size, tooltip) {
    var label = text !== '',
        halfSize = size / 2 | 0,
        base = {
            position: latLng(lat, lng),
            map: map,
            icon: {
                url: iconPath,
                size: new google.maps.Size(size, size),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(halfSize, halfSize)
            }
        };

    if (label) {
        base.labelContent = text;
        base.labelClass = 'maplabel';
        base.labelAnchor = new google.maps.Point(-halfSize, 8);
    }

    if (tooltip) {
        base.title = tooltip;
    }

    return new (label ? MarkerWithLabel : google.maps.Marker)(base);
}

// ----------------------------------------------------------------------------
function Vehicle(values, $tbody, $map) {
    this.update = function (newValues) {
        if (this.alert_timestamp && newValues.alert_timestamp && this.alert_timestamp < newValues.alert_timestamp) {
            alarms.systemAlert(this.reg, newValues.alert_data);
        }

        if (this.warning_timestamp && newValues.warning_timestamp && this.warning_timestamp < newValues.warning_timestamp) {
            alarms.systemWarning(this.reg, newValues.warning_data);
        }

        alarms.checkForCrossings(this.point_id, newValues.point_id, newValues);

        for (var v in newValues) {
            this[v] = newValues[v];
        }

        this.updateRow();
        this.updateIcon();
    };

    this.updateRow = function () {
        var self = this,
            street = this.point == '' ? this.map1 : '\'' + this.point + '\'',
            row = {
                reg: this.reg,
                driver: this.driver,
                group: this.vehicle_group,
                street: street,
                town: this.map2,
                lga: this.map3,
                state: this.map4,
                country: this.map5,
                speed: this.speed,
                dir: this.heading,
                date: this.date,
                time: this.time,
                event: this.event
            };

        if (!show_lga) {
            delete row.lga;
        }

        this.row.html(makeTds(row))
                .unbind('click')
                .unbind('dblclick')
                .click(function () { self.select(); })
                .dblclick(function () { self.moveTo(streetZoom); })
                .disableTextSelect();

        if (this.connected == '0') {
            this.row.addClass('discon');
        } else {
            this.row.removeClass('discon');
        }
    }

    this.updateIcon = function () {
        var type = this.vehicle_type,
            heading = headingToAngleIndex(this.heading),
            path = iconPath + 'vehicles/',
            self = this;

        if (type === 'Motorbike') {
            path += 'MotorbikeBlue' + heading + '.png';
        } else {
            path += 'CarBlue' + heading + '.gif';
        }

        if (this.marker !== null) {
            this.marker.setMap(null);
        }

        this.marker = createMarker(path, this.lat, this.lon, this.reg + '<br>(' + this.driver + ')', 32);

        google.maps.event.addListener(this.marker, 'click', function () {
            self.select();
            $('#grid')[0].scrollTo(self.row.position().top - 14);
        });
    }

    this.select = function () {
        $tbody.find('tr').removeClass('sel').find('td').removeClass('sel');
        this.row.addClass('sel').find('td').addClass('sel');
        selectedVehicle = this;
    };

    this.moveTo = function (zoom) {
        map.setCenter(latLng(this.lat, this.lon));
        map.setZoom(zoom);
    };

    this.locate = function () {
        var reg = this.reg;

        if (this.connected == '0') {
            return alert('Cannot request position of this vehicle because it is disconnected.');
        }

        if (!canLocate) {
            return;
        }

        setStatus('Sending request for location of ' + reg);
        post(
            'locate.php',
            '',
            $.param({
                tab_id: tab_id,
                vehicle_id: this.vehicle_id,
                server_id: this.server_id
            }),
            function () {
                setStatus('Request for location of ' + reg + ' received at GVT');
            }
        );

        canLocate = false;
        setTimeout('canLocate = true;', locateInterval);
    };

    this.setVisible = function (x) {
        this.visible = x;

        if (x) {
            this.row.show();
        } else {
            this.row.hide();
        }
    };

    function makeTds(vals) {
        var result = '';
        for (var v in vals) {
            result += '<td class="' + v + '">' + (vals[v] == '' ? '&nbsp;' : vals[v]) + '</td>';
        }
        return result;
    }

    function headingToAngleIndex(heading) {
        return heading == 'NNE' ? 1 :
               heading == 'NE' ? 2 :
               heading == 'ENE' ? 3 :
               heading == 'E' ? 4 :
               heading == 'ESE' ? 5 :
               heading == 'SE' ? 6 :
               heading == 'SSE' ? 7 :
               heading == 'S' ? 8 :
               heading == 'SSW' ? 9 :
               heading == 'SW' ? 10 :
               heading == 'WSW' ? 11 :
               heading == 'W' ? 12 :
               heading == 'WNW' ? 13 :
               heading == 'NW' ? 14 :
               heading == 'NNW' ? 15 : 0;
    }

    this.visible = true;
    this.row = $('<tr />').appendTo($tbody);
    this.marker = null;
    this.update(values);
}

function createVehicles(vs) {
    var $tbody = $('#vehicles > tbody'),
        $map = $('#map');

    $tbody.empty();
    $.each(vs, function () {
        vehicles.push(new Vehicle(this, $tbody, $map));
    });

    $('#vehicles').tablesorter({
        widgets: ['zebra'],
        fixed_headers: true,
        header_fixpoint: '#footer',
        sortList: [[2, 0]]
    });

    initialiseGroups();
    setupScrolling();
    selectFirstVisibleGridRow();
}

function greaterOf(a, b) {
    return a > b ? a : b;
}

function updateVehicles() {
    var latest = vehicles[0].timestamp || '',
        latestWarning = vehicles[0].warning_timestamp || '',
        latestAlert = vehicles[0].alert_timestamp || '';

    if (updatePending) {
        return;
    }

    updatePending = true;
    setStatus('Checking for new data...');

    $.each(vehicles, function () {
        latest = greaterOf(this.timestamp || '', latest);
        latestWarning = greaterOf(this.warning_timestamp || '', latestWarning);
        latestAlert = greaterOf(this.alert_timestamp || '', latestAlert);
    });

    function onVehicleData(data) {
        var regs = [];
        updatePending = false;

        $.each(data, function () {
            var newData = this;

            regs.push(this.reg);
            $.each(vehicles, function () {
                if (this.vehicle_id == newData.vehicle_id) {
                    this.update(newData);
                }
            });
        });

        setStatus(data.length > 0 ? 'New data for: ' + regs.join(', ') : 'No new data');
    }

    post('get_vehicles.php',
         '',
         $.param({
             tab_id: tab_id,
             time: latest,
             warning: latestWarning,
             alert: latestAlert
         }),
         onVehicleData);
}

function selectFirstVisibleGridRow() {
    $('#vehicles > tbody > tr :visible').each(function () {
        $(this).click();
        return false;
    });
}

function setupScrolling() {
    var vehiclesHeight = $('#vehicles').height(),
        footerHeight = $('#footer').height(),
        less = vehiclesHeight < footerHeight;

    $('#grid div').css('margin-top', (less ? footerHeight - vehiclesHeight : 0) + 'px');
    $('#grid').jScrollPane({
        showArrows: true,
        scrollbarWidth: 15,
        arrowSize: 16
    });
}

function setSizes() {
    var gridHeight = Math.min($('#vehicles').height(), 98),
        mainHeight = $('#main').height(),
        routesHeight = Math.min(mainHeight - 45, 350),
        pointsHeight = $('#points').height();

    $('#footer').css('height', gridHeight + 'px');
    $('#main').css('bottom', gridHeight + 'px');
    $('#map').height(mainHeight);
    $('#routes').height(routesHeight);
    $('#routes-tree').height(routesHeight - $('#routes-tree').position().top);
    $('#points-tree').height(pointsHeight - 310);
    //$('#alerts-div').height($('#alerts').height() - 45); MDA

    if (typeof map != 'undefined') {
        google.maps.event.trigger(map, 'resize');
    }

    $('.jScrollPaneContainer').width($(window).width());
    $('#vehicles, #vehicles_').width($(window).width()/* - 16*/);
    setupScrolling();
}

function padTwoDigits(value) {
    return value < 10 ? '0' + value : value;
}

function setStatus(msg) {
    var hours = padTwoDigits(new Date().getHours()),
        mins = padTwoDigits(new Date().getMinutes()),
        secs = padTwoDigits(new Date().getSeconds());

    $('#status').html(hours + ':' + mins + ':' + secs + ' - ' + msg);
}

// ----------------------------------------------------------------------------
function zoomToAll() {
    var bounds = new google.maps.LatLngBounds();

    $.each(vehicles, function () {
        if (this.visible) {
            bounds.extend(latLng(this.lat, this.lon));
        }
    });

    map.fitBounds(bounds);
}

function zoomToLevel(zoom) {
    if (selectedVehicle === null) {
        map.setZoom(zoom);
    } else {
        map.setCenter(latLng(selectedVehicle.lat, selectedVehicle.lon));
        map.setZoom(zoom);
    }
}

function zoomStreet() {
    zoomToLevel(streetZoom);
}

function zoomTown() {
    zoomToLevel(townZoom);
}

function logout() {
    window.location = 'logout.php?tab_id=' + tab_id;
}

function closeAll() {
    routesRemoveRoute();
    pointsRemovePoints();
    $('#reports, #routes, #points').hide();
    zones.close();
}

// ----------------------------------------------------------------------------
function reportOpen() {
    closeAll();
    $('#reports').show();
    $('#report-from-hour').val('00');
    $('#report-from-min').val('00');
    $('#report-to-hour').val('23');
    $('#report-to-min').val('59');
}

function reportInitialise() {
    $('#reports .close').click(closeAll);
    $('#report-create').click(reportCreate);
    $('#report-from').datepicker({ defaultDate: -1 }).datepicker('setDate', -1);
    $('#report-to').datepicker().datepicker('setDate', new Date());
}

function reportCreate() {
    if (typeof selectedVehicle == 'undefined' || selectedVehicle == null) {
        return alert('No vehicle selected');
    }

    var type = $('#report-type').val(),
        format = $('#report-format').val(),
        from = $('#report-from').datepicker('getDate'),
        to = $('#report-to').datepicker('getDate'),
        fromHour = $('#report-from-hour').val(),
        fromMin = $('#report-from-min').val(),
        toHour = $('#report-to-hour').val(),
        toMin = $('#report-to-min').val(),
        period = daysBetween(from, to),
        max = type == 'detail' ? detail_max_period :
              type == 'travel' ? travel_max_period : stop_max_period;

    if (period > max) {
        return alert('The maximum period for this report type is ' + max + ' days');
    }

    var params = $.param({
        tab_id: tab_id,
        type: type,
        format: format,
        from: $('#report-from').val(),
        from_hour: fromHour,
        from_min: fromMin,
        to: $('#report-to').val(),
        to_hour: toHour,
        to_min: toMin,
        vehicle: selectedVehicle.vehicle_id
    });

    window.open('get_report.php?' + params,
                'win',
                'toolbar=1,scrollbars=1,location=0,statusbar=1,' +
                'menubar=1,resizable=1,left=20,top=20,' +
                'width=' + ($(window).width() - 40) + ',' +
                'height=' + ($(window).height() - 20));
}

// ----------------------------------------------------------------------------
function routesOpen() {
    closeAll();
    var routesHeight = Math.min($('#main').height() - 45, 350);
    $('#routes').height(routesHeight).show();
    $('#routes-tree').height(routesHeight - $('#routes-tree').position().top).empty();
}

function routesInitialise() {
    $('#routes .close').click(closeAll);
    $('#routes-retrieve').click(routesRetrieve);
    $('#routes-from').datepicker({ defaultDate: -7 }).datepicker('setDate', -7);
    $('#routes-to').datepicker().datepicker('setDate', new Date());
}

function routesRetrieve() {
    if (typeof selectedVehicle == 'undefined' || selectedVehicle == null) {
        return alert('No vehicle selected');
    }

    var from = $('#routes-from').datepicker('getDate'),
        to = $('#routes-to').datepicker('getDate'),
        period = daysBetween(from, to),
        max = 31;

    if (period > max) {
        alert('The maximum period for this route type is ' + max + ' days');
        return;
    }

    post(
        'get_routes_list.php',
        'Retrieving route list',
        $.param({
            tab_id: tab_id,
            from: $('#routes-from').val(),
            to: $('#routes-to').val(),
            from_hour: $('#routes-from-hour').val(),
            from_min: $('#routes-from-min').val(),
            to_hour: $('#routes-to-hour').val(),
            to_min: $('#routes-to-min').val(),
            vehicle_id: selectedVehicle.vehicle_id
        }),
        function (data) {
            routesCreateTree(data);
        }
    );
}

function routesCreateTree(days) {
    function div() { return $(document.createElement('div')); }

    var $tree = $('#routes-tree').empty();

    if (days.length == 0) {
        $tree.html('No data over this period');
        return;
    }

    $.each(days, function () {
        var routes = this.data,
            date = this.date,
            id = selectedVehicle.vehicle_id;

        if (routes.length == 0) {
            return true; // continue
        }

        var $element = div().addClass('element');

        div()
            .addClass('header')
            .html(date + ' (' + this.dow + ')')
            .appendTo($element)
            .click(function () {
                $tree.find('.subelement').hide();
                $element.find('.subelement').show().end();
            })
            .dblclick(function () {
                var d = date.substring(6) + date.substring(3, 5) + date.substring(0, 2);

                $('.selected').removeClass('selected');
                $(this).addClass('selected');
                routesShowRoute(d + '000000', d + '235959', id);
            })
            .disableTextSelect();

        $.each(routes, function () {
            var route = this,
                hint = this.start_town + ' to ' +
                       this.end_town + ' (' +
                       'Click to plot the route on the map, approx. ' +
                       (this.end_id - this.start_id) + ' events)',
                text = this.start_time.substr(0, 5) + ' - ' +
                       this.end_time.substr(0, 5);

            div().addClass('subelement')
                 .hide()
                 .html(text)
                 .attr('title', hint)
                 .disableTextSelect()
                 .appendTo($element)
                 .hover(function () { $(this).addClass('route_hover'); },
                        function () { $(this).removeClass('route_hover'); })
                 .click(function () {
                     $('.selected').removeClass('selected');
                     $(this).addClass('selected');
                     routesShowRoute(route.start_datetime, route.end_datetime, id);
                 });
        });

        $tree.append($element);
    });

    $tree.find('> .element:first > .subelement').show();
}

function routesShowRoute(start, end, vehicleId) {
    post(
        'get_route.php',
        'Retrieving route data',
        $.param({
            tab_id: tab_id,
            vehicle_id: vehicleId,
            start: start,
            end: end
        }),
        routesPlotRoute
    );
}

var routeIcons = [],
    routePolyline = null;

function routesPlotRoute(route) {
    var plotLines = $('#routes-plot-lines').attr('checked') != '',
        polyPoints = [];

    if (route.length == 0) {
        return;
    }

    routesRemoveRoute();

    $.each(route, function (i) {
        var angle = Math.floor((parseInt(this.Dir, 10) + 12) / 22.5) % 16,
            text = this.GPSTime + ' - ' + this.speed;
        iconName = 'images/arrows/';

        if (this.is_start === '1' || i == 0) {
            iconName += '0a0_start.gif';
        } else if (this.is_stop === '1' || i == route.length - 1) {
            iconName += 'f00_stop.gif';
        } else {
            iconName += 'ff0_' + angle + '.gif';
        }

        routeIcons.push(routesCreateIcon(this.X, this.Y, text, iconName));

        if (plotLines) {
            polyPoints.push(latLng(this.X, this.Y));
        }
    });

    if (plotLines) {
        routePolyline = new google.maps.Polyline({
            path: polyPoints,
            strokeColor: '#ff0',
            strokeWeight: 3,
            strokeOpacity: 1,
            map: map
        });
    }

    routesCentreRoute();
}

function routesRemoveRoute() {
    $.each(routeIcons, function () { this.setMap(null); });
    routeIcons = [];

    if (routePolyline) {
        routePolyline.setMap(null);
        routePolyline = null;
    }
}

function routesCentreRoute() {
    var bounds = new google.maps.LatLngBounds();

    $.each(routeIcons, function () {
        bounds.extend(this.getPosition());
    });

    map.fitBounds(bounds);
}

function routesCreateIcon(lat, lng, text, iconName) {
    return createMarker(iconName, lat, lng, '', 17, text);
}

// ----------------------------------------------------------------------------
var selectedPointId = -1,
    selectedPointGroup = '',
    pointPolygon = null,
    points = [],
    visiblePoints = {};

function pointsOpen() {
    closeAll();
    $('#points').show();
    $('#points-tree').empty();

    post(
        'get_points.php',
        'Retrieving custom points',
        $.param({ tab_id: tab_id }),
        pointsCreateTree
    );
}

function pointsInitialise() {
    $('#points .close').click(closeAll);
    $('#points-save').click(pointsSave);
    $('#points-del').click(pointsDelete);
    $('#points-show').click(pointsShow);
}

function pointsCreateTree(data) {
    points = [];

    if (data.length > 0 && (selectedPointId == -1 || selectedPointGroup == '')) {
        pointsSelect(data[0]);
    }

    var grouped = {};

    $.each(data, function () {
        var group = this.point_group;

        if (typeof grouped[group] === 'undefined') {
            grouped[group] = [];
        }

        grouped[group].push(this);
    });

    var $tree = $('#points-tree').empty();

    $.each(grouped, function () {
        var $element = div().addClass('element');

        div().addClass('header')
             .html(this[0].point_group)
             .appendTo($element)
             .click(function () {
                 $tree.find('.subelement').hide();
                 $element.find('.subelement').show().end();
             });

        $.each(this, function () {
            var point = this,
                $x = div();

            $x.addClass('subelement')
              .html(this.point_name)
              .disableTextSelect()
              .appendTo($element)
              .click(function () {
                  $('.selected').removeClass('selected');
                  $(this).addClass('selected');
                  pointsSelect(point);
              }).dblclick(function () {
                  pointsShow();
              });

            if (this.id == selectedPointId) {
                $x.addClass('selected');
            }

            if (this.point_group != selectedPointGroup) {
                $x.hide();
            }

            points.push(this);
        });

        $tree.append($element);
    });

    if ($tree.html() === '') {
        $tree.html('Currently there are no custom points. To add a custom point left-click on the map.');
    }

    pointsUpdateVisiblePoints();
}

function pointsAdd(lat, lon) {
    var point = {
        id: -1,
        point_name: 'New point',
        point_group: 'Points',
        lat: lat,
        lon: lon,
        radius: 50,
        enter_address: '',
        exit_address: ''
    };

    post(
        'add_point.php',
        'Adding custom point',
        $.param({
            tab_id: tab_id,
            lat: lat,
            lon: lon,
            name: point.point_name,
            group: point.point_group,
            radius: point.radius
        }),
        function (data) {
            point.id = data.id;
            pointsSelect(point);
            $('#points-name').focus();
            pointsCreateTree(data.points);
        }
    );
}

function allDigits(x) {
    for (var i = 0; i != x.length; ++i) {
        if (x.charAt(i) < '0' || x.charAt(i) > '9') {
            return false;
        }
    }

    return true;
}

function pointsSave() {
    var $group = $('#points-group'),
        $radius = $('#points-radius'),
        radius = parseInt($radius.val(), 10);

    if ($group.val() == '') {
        alert('Please enter a group.');
        $group.focus();
        return;
    }

    if (!allDigits($radius.val())) {
        alert('Invalid radius.');
        $radius.focus();
        return;
    }

    if (radius < 5) {
        alert('Radius cannot be less than 5 metres.');
        $radius.focus();
        return;
    }

    if (radius > 100000) {
        alert('Radius cannot be greater than 100 kilometres.');
        $radius.focus();
        return;
    }

    post(
        'save_point.php',
        'Saving custom point',
        $.param({
            tab_id: tab_id,
            point_id: selectedPointId,
            lat: $('#points-lat').val(),
            lon: $('#points-lon').val(),
            name: $('#points-name').val(),
            group: $('#points-group').val(),
            radius: $('#points-radius').val(),
            enter: $('#points-alert-enter').attr('checked') ? 1 : 0,
            exit: $('#points-alert-exit').attr('checked') ? 1 : 0,
            all_users: $('#points-all-users').attr('checked') ? 1 : 0,
            enter_address: $('#points-email-enter').val(),
            exit_address: $('#points-email-exit').val()
        }),
        function (data) {
            selectedPointGroup = $('#points-group').val();
            pointsCreateTree(data);
            pointsUpdatePoint(selectedPointId);
        }
    );
}

function pointsDelete() {
    if (selectedPointId === -1) {
        return;
    }

    if (!confirm('Are you sure you wish to delete ' + $('#points-name').val() + '?')) {
        return;
    }

    post(
        'delete_point.php',
        'Deleting custom point',
        $.param({
            tab_id: tab_id,
            point_id: selectedPointId
        }),
        function (data) {
            pointsRemovePoint(selectedPointId);

            if (data.length > 0) {
                pointsSelect(data[0]);
            }

            pointsCreateTree(data);

            if (data.length === 0) {
                selectedPointId = -1;
                selectedPointGroup = '';
                $('#points input').val('').attr('checked', '');
            }
        }
    );
}

function pointsShow() {
    var lat = parseFloat($('#points-lat').val()),
        lon = parseFloat($('#points-lon').val()),
        radius = parseInt($('#points-radius').val());

    if (selectedPointId == -1) {
        return;
    }

    pointsRemovePointArea();

    pointPolygon = createPointArea(lat, lon, radius);
    map.setCenter(latLng(lat, lon));
    map.setZoom(17);
}

function pointsSelect(point) {
    selectedPointId = point.id;
    selectedPointGroup = point.point_group;
    $('#points-name').val(point.point_name);
    $('#points-group').val(point.point_group);
    $('#points-lat').val(parseFloat(point.lat).toFixed(5));
    $('#points-lon').val(parseFloat(point.lon).toFixed(5));
    $('#points-radius').val(point.radius);
    $('#points-alert-enter').attr('checked', point.alert_enter == 1 ? 'checked' : '');
    $('#points-alert-exit').attr('checked', point.alert_exit == 1 ? 'checked' : '');
    $('#points-all-users').attr('checked', point.used_by_all == 1 ? 'checked' : '');
    $('#points-email-enter').val(point.enter_address);
    $('#points-email-exit').val(point.exit_address);
}

function createPointArea(lat, lon, radiusInMetres) {
    var radius = metresToLatLngApprox(radiusInMetres),
        points = [],
        angleRad;

    for (var i = 0; i != 180; ++i) {
        angleRad = i * 2 * (Math.PI / 180);
        points.push(latLng(lat + Math.sin(angleRad) * radius, lon + Math.cos(angleRad) * radius));
    }

    return new google.maps.Polygon({
        paths: points,
        strokeColor: '#00f',
        strokeWeight: 1,
        strokeOpacity: 1,
        fillColor: '#00f',
        fillOpacity: 0.3,
        map: map
    });
}

function pointsRemovePointArea() {
    if (pointPolygon !== null) {
        pointPolygon.setMap(null);
        pointPolygon = null;
    }
}

function pointsRemovePoints() {
    pointsRemovePointArea();
    $.each(visiblePoints, function () { this.setMap(null); });
    visiblePoints = {};
}

function pointsRemovePoint(pointId) {
    var id = 'l' + pointId;
    visiblePoints[id].setMap(null);
    delete visiblePoints[id];
}

function pointsUpdatePoint(pointId) {
    var id = 'l' + pointId,
        point = null;

    if (typeof visiblePoints[id] != 'undefined') {
        pointsRemovePoint(pointId);
    }

    $.each(points, function () {
        if (this.id == pointId) {
            point = this;
        }
    });

    if (point !== null) {
        visiblePoints[id] = createMarker(iconPath + 'misc/House.gif', point.lat, point.lon, point.point_name, 16);
    }
}

function pointsUpdateVisiblePoints() {
    var bounds = map.getBounds();

    $.each(points, function () {
        var id = 'l' + this.id,
            visible = bounds.contains(latLng(this.lat, this.lon)),
            onMap = typeof visiblePoints[id] != 'undefined';

        if (visible && !onMap) {
            pointsUpdatePoint(this.id);
        }

        if (!visible && onMap) {
            pointsRemovePoint(this.id);
        }
    });
}

// ----------------------------------------------------------------------------
var zones = (function () {
    var maximumPoints = 16,
        polygon = null,
        creating = false,
        drawingText = 'Click on the map to define a polyzone. A polyzone may have at most sixteen points. Click on the first point to automatically close the zone. Once created you will have the option to edit the zone.',
        editingText = 'You may now edit the polygon. Click \'Finish\' when satisified.',
        drawingManager,
        selectedPoint = null;

    function initialise() {
        $('#zones .close').click(close);
        $('#zones-create').click(create);
        $('#zones-save').click(save);
        $('#zones-del').click(del);
        $('#zones-show').click(show);
        initialiseCreator();
    }

    function open() {
        closeAll();
        $('#zones').show();
        loadZones();
    }

    function close() {
        removePolygon();
        closeCreator();
        $('#zones').hide();
    }

    function create() {
        openCreator();
        $('#zones').hide();
    }

    function enterType(point) {
        return point.enter_alert === '1' ? 'alert' :
               point.enter_warning === '1' ? 'warn' : '';
    }

    function exitType(point) {
        return point.exit_alert === '1' ? 'alert' :
               point.exit_warning === '1' ? 'warn' : '';
    }

    function select(point) {
        selectedPoint = point;
        $('#zones-name').val(point.name);
        $('#zones-type-enter').val(enterType(point));
        $('#zones-type-exit').val(exitType(point));
        $('#zones-email-enter').val(point.enter_email);
        $('#zones-email-exit').val(point.exit_email);
    }

    function show() {
        var point = selectedPoint,
            points = [],
            bounds = new google.maps.LatLngBounds();
        i = 0;

        if (point === null) {
            return;
        }

        for (; i != point.points; ++i) {
            points.push(latLng(point['lat_' + i], point['lng_' + i]));
        }

        createPolygon(points);

        for (i = 0; i < points.length; i++) {
            bounds.extend(points[i]);
        }

        map.fitBounds(bounds);
    }

    function save() {
        if (selectedPoint === null) {
            return;
        }

        post(
            'save_zone.php',
            'Saving polyzone',
            $.param({
                tab_id: tab_id,
                id: selectedPoint.id,
                name: $('#zones-name').val(),
                enter_email: $('#zones-email-enter').val(),
                enter_type: $('#zones-type-enter').val(),
                exit_email: $('#zones-email-exit').val(),
                exit_type: $('#zones-type-exit').val()
            }),
            createTree
        );
    }

    function del() {
        if (selectedPoint === null || !confirm('Are you sure you want to delete \'' + selectedPoint.name + '\'?')) {
            return;
        }

        removePolygon();
        post(
            'delete_zone.php',
            'Deleting polyzone',
            $.param({
                tab_id: tab_id,
                id: selectedPoint.id
            }),
            createTree
        );
    }

    function loadZones() {
        post(
            'get_zones.php',
            'Retrieving polyzones',
            $.param({ tab_id: tab_id }),
            createTree
        );
    }

    function createTree(zones) {
        var $list = $('#zones-list').empty();

        $.each(zones, function () {
            var $elem = $('<div/>'),
                point = this;

            function onClick() {
                $('.selected').removeClass('selected');
                $(this).addClass('selected');
                select(point);
            }

            $elem.text(this.name)
                .click(onClick)
                .dblclick(show)
                .disableTextSelect()
                .appendTo($list);
        });
    }

    // Creator
    function initialiseCreator() {
        $('#zone-creator .close').click(closeCreator);
        $('#zone-creator-finish').click(finish);
    }

    function openCreator() {
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingControl: false,
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            polygonOptions: {
                strokeColor: '#00f',
                strokeWeight: 1,
                strokeOpacity: 1,
                fillColor: '#00f',
                fillOpacity: 0.3,
                clickable: false,
                editable: true
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'polygoncomplete', onDrawingFinish);

        creating = true;
        $('#zone-creator p').text(drawingText);
        $('#zone-creator-finish').hide();
        $('#zone-creator').show();
    }

    function stopCreation() {
        if (drawingManager) {
            drawingManager.setMap(null);
            drawingManager = null;
        }
        creating = false;
        removePolygon();
        $('#zone-creator').hide();
        $('#zones').show();
    }

    function closeCreator() {
        if (creating && polygon !== null && polygon.getPath().getLength() > 2) {
            if (confirm('Are you sure you want to cancel?')) {
                stopCreation();
            }
        }
    }

    function finish() {
        var name = prompt('Please enter the name of this polyzone', ''),
            path = polygon.getPath(),
            points = path.getLength(),
            data = {
                tab_id: tab_id,
                name: name === '' ? 'New polyzone' : name,
                points: points
            };

        if (points >= maximumPoints) {
            alert('Sorry, a polygon cannot have more than 16 points.');
            stopCreation();
            return;
        }

        for (i = 0; i != points; ++i) {
            data['lat_' + i] = path.getAt(i).lat();
            data['lng_' + i] = path.getAt(i).lng();
        }

        post(
            'add_zone.php',
            'Adding polyzone.',
            $.param(data),
            function () {
                loadZones();
            }
        );

        stopCreation();
    }

    function onDrawingFinish(poly) {
        removePolygon();
        polygon = poly;

        if (polygon.getPath().getLength() >= maximumPoints) {
            alert('Sorry, a polygon cannot have more than 16 points.');
            stopCreation();
            return;
        }

        drawingManager.setMap(null);
        drawingManager = null;

        $('#zone-creator p').text(editingText);
        $('#zone-creator-finish').show();
    }

    function createPolygon(points) {
        removePolygon();

        polygon = new google.maps.Polygon({
            paths: points,
            strokeColor: '#00f',
            strokeWeight: 3,
            strokeOpacity: 1,
            fillColor: '#00f',
            fillOpacity: 0.5,
            map: map
        });
    }

    function removePolygon() {
        if (polygon !== null) {
            polygon.setMap(null);
            polygon = null;
        }
    }

    return {
        initialise: initialise,
        open: open,
        close: close
    };
} ());

// ----------------------------------------------------------------------------
var alarms = (function () {
    var warningsCloseTimer = null,
        warningsCloseTimeMs = 30000;

    function initialise() {
        $('#alarms-close').click(closeAlerts);
        $('#warnings-close').click(closeWarnings);
    }

    function systemWarning(reg, data) {
        var values = data.split('@#@'),
            eventNumber = parseInt(values[0], 10),
            eventText = values[1],
            date = '<span class="date">' + values[2] + '</span>',
            time = '<span class="time">' + values[3] + '</span>',
            street = values[4],
            town = values[5],
            region = values[6],
            lat = values[7],
            lng = values[8],
            text =
                date + ' ' + time + ': ' +
                '<span class="event">' + eventText + '</span>' +
                ' for <span class="reg">' + reg + '</span> at ' +
                street + ', ' + town + ', ' + region + '.';

        showWarningMessage(text);
    }

    function systemAlert(reg, data) {
        var values = data.split('@#@'),
            eventNumber = parseInt(values[0], 10),
            eventText = values[1],
            date = '<span class="date">' + values[2] + '</span>',
            time = '<span class="time">' + values[3] + '</span>',
            street = values[4],
            town = values[5],
            region = values[6],
            lat = values[7],
            lng = values[8],
            text =
                date + ' ' + time + ': ' +
                '<span class="event">' + eventText + '</span>' +
                ' for <span class="reg">' + reg + '</span> at ' +
                street + ', ' + town + ', ' + region + '.';

        showAlertMessage(text);
    }

    function checkForCrossings(oldPointId, currentPointId, vehicle) {
        if (typeof oldPointId === 'undefined' || oldPointId === currentPointId) {
            return;
        }

        if (currentPointId != -1) {
            entered(currentPointId, vehicle);
        }

        if (oldPointId != -1) {
            exited(oldPointId, vehicle);
        }
    }

    function entered(id, vehicle) {
        crossed(id, vehicle, 'alert_enter', 'entered');
    }

    function exited(id, vehicle) {
        crossed(id, vehicle, 'alert_exit', 'exited');
    }

    function crossed(id, vehicle, field, verb) {
        var point = lookupPointById(id),
            text =
                '<span class="date">' + vehicle.date + '</span>' +
                '<span class="time">' + vehicle.time + '</span>: ' +
                '<span class="reg">' + vehicle.reg + '</span> ' +
                verb;

        if (point !== null && point[field] == 1) {
            showAlertMessage(text + ' \'' + point.point_name + '\'');
        }
    }

    function showWarningMessage(text) {
        $('#warnings').show();
        $('<li />').html(text).appendTo($('#warnings-list'));
        startWarningsCloseTimer();
    }

    function showAlertMessage(text) {
        playAlertSound();
        $('#alarms-wrap').show();
        $('<li />').html(text).appendTo($('#alarms-list'));
    }

    function playAlertSound() {
        $('#off-screen').html('<embed src="audio/alert.wav" autostart="true" loop="false" hidden="true"></embed>');
        setTimeout(function () { $('#off-screen').html(''); }, 6000);
    }

    function closeAlerts() {
        $('#alarms-wrap').hide();
        $('#alarms-list').empty();
    }

    function closeWarnings() {
        $('#warnings').hide();
        $('#warnings-list').empty();
        warningsCloseTimer = null;
    }

    function startWarningsCloseTimer() {
        if (warningsCloseTimer !== null) {
            clearTimeout(warningsCloseTimer);
        }

        warningsCloseTimer = setTimeout(closeWarnings, warningsCloseTimeMs);
    }

    function lookupPointById(id) {
        var result = null;

        $.each(points, function () {
            if (this.id == id) {
                result = this;
            }
        });

        return result;
    }

    return {
        initialise: initialise,
        systemWarning: systemWarning,
        systemAlert: systemAlert,
        checkForCrossings: checkForCrossings
    };
} ());

// ----------------------------------------------------------------------------
function initialiseGroups() {
    var $filter = $('#group-filter').empty(),
        groups = { All: '' },
        groupCount = -1;

    $.each(vehicles, function () { groups[this.vehicle_group] = ''; });

    for (var x in groups) {
        ++groupCount;
    }

    if (groupCount === 1) {
        return;
    }

    for (var group in groups) {
        $('<option />').appendTo($filter).html(group).val(group);
    }

    $('#grid-options').css('width', '484px');
    $('#group-filtering').css('display', 'inline');
}

function filterVehicleGrid() {
    var groupFilter = $('#group-filter').val();

    $.each(vehicles, function () {
        if (this.vehicle_group == groupFilter || groupFilter == 'All') {
            this.setVisible(true);
            this.updateIcon();
        } else {
            this.setVisible(false);
            if (this.marker !== null) {
                this.marker.setMap(null);
            }
            this.marker = null;
        }
    });

    setupScrolling();
    selectFirstVisibleGridRow();
}

function searchFor($element, field) {
    var text = $element.val().toLowerCase(),
        textLength = text.length,
        found = false;

    if (textLength === 0) {
        $element.removeClass('not-found');
        return;
    }

    $.each(vehicles, function () {
        var x = this[field].substring(0, textLength).toLowerCase();

        if (text === x) {
            this.select();
            $('#grid')[0].scrollTo(this.row.position().top - 14);
            found = true;
            return false;
        }
    });

    if (!found) {
        $element.addClass('not-found');
    } else {
        $element.removeClass('not-found');
    }
}

function searchReg() {
    $('#search-driver').removeClass('not-found').val('');
    searchFor($('#search-reg'), 'reg');
}

function searchDriver() {
    $('#search-reg').removeClass('not-found').val('');
    searchFor($('#search-driver'), 'driver');
}

// ----------------------------------------------------------------------------
function initialLoad(data) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: latLng(0, 10),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.HYBRID
    });

    google.maps.event.addListener(map, 'click', function (event) {
        var loc = event.latLng;

        if ($('#points').css('display') === 'block') {
            pointsAdd(loc.lat(), loc.lng());
        }
    });

    google.maps.event.addListener(map, 'bounds_changed', function () {
        if ($('#points').css('display') === 'block') {
            pointsUpdateVisiblePoints();
        }
    });

    points = data.points;
    createVehicles(data.vehicles);
    setSizes();
    zoomToAll();
}

$(document).ready(function () {
    $('.button, th, h1').disableTextSelect();
    $('#logout').click(function () { logout(); });
    $('#zoom-all-btn').click(function () { zoomToAll(); });
    $('#street-btn').click(function () { zoomStreet(); });
    $('#town-btn').click(function () { zoomTown(); });
    $('#zoom-in-btn').click(function () { map.setZoom(map.getZoom() + 1); });
    $('#zoom-out-btn').click(function () { map.setZoom(map.getZoom() - 1); });
    $('#reports-btn').click(function () { reportOpen(); });
    $('#routes-btn').click(function () { routesOpen(); });
    $('#points-btn').click(function () { pointsOpen(); });
    $('#zones-btn').click(function () { zones.open(); });
    $('#locate-btn').click(function () { selectedVehicle.locate(); });
    $('#search-reg').keyup(searchReg);
    $('#search-driver').keyup(searchDriver);

    $.datepicker.setDefaults({
        maxDate: new Date(),
        dateFormat: 'dd-mm-yy'
    });

    reportInitialise();
    routesInitialise();
    pointsInitialise();
    zones.initialise();
    alarms.initialise();

    // Workaround: Chrome 22 displayed the calendar at login at the top left.
    $('#ui-datepicker-div').css('position', 'absolute').css('left', '-9999px').hide();

    $('#group-filter').change(function () { filterVehicleGrid(); });
    setInterval('updateVehicles()', updateInterval);

    post(
        'initial_load.php',
        '',
        $.param({ tab_id: tab_id }),
        initialLoad,
        function (error) { alert(error); logout(); }
    );

    $('#routes label').disableTextSelect();
});

$(window).bind('resize', setSizes);
