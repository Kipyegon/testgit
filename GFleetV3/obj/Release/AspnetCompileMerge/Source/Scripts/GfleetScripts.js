﻿function loadDialog(url, divElementId, title) { 
    showLoading();
    divElementId = "#" + divElementId;
    $(divElementId).load(url, function (responseText, textStatus, XMLHttpRequest) {
        ////console.log(responseText);
        $(divElementId).dialog({
            title: title,
            autoOpen: false,
            resizable: false,
            width: 'auto',
            autoResize: true,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: true,
            open: function (event, ui) {
                $(this).html(textStatus == 'success' ? responseText : 'An error occured while loading dialog. </br> Kindly try again');
                closeLoading(); 
                $("div.ui-dialog-content").bind('DOMNodeInserted', function (e) {
                    //alert('element now contains: ' + $(e.target).html());
                    $(this).dialog('option', 'position', 'center');
                    return;
                });
            },
            resizeStop: function(event, ui) {
                //                if(mapmapcanvas!=null){
                //                google.maps.event.trigger(map, 'resize');
                //                  }
            }
            
        });
        $(divElementId).dialog('open');
    });
}


function dialogStartUp(callback) {
    $.validator.unobtrusive.parse($('form'));
    $('form').submit(function () {
        var val = $("button[type=submit][clicked=true]").val();
        if ($(this).valid()) {
            var frm = $(this);
            $('#from_result').html('');
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize() + '&Command=' + val,
                type: 'POST',
                success: function (responseText, textStatus, XMLHttpRequest) {
                    closeLoading();
                    closeWaiting();
                    console.log(responseText);

                    if(responseText.Status==null){
                        console.log(1);
                        $('#from_result').html(responseText);
                        return;
                    }
                    if (!responseText.Status) {
                        $('#from_result').html(responseText.Message);
                        //showWaiting();
                        //location.reload(true);
                        return;
                    } else if (responseText.Status && responseText.IsUpdate) {
                        $('#from_result').html(responseText.Message);
                        location.reload(true);
                        return;
                    } else if (responseText.Status && !responseText.IsUpdate) {                
                        frm[0].reset();
                        $('#from_result').html(responseText.Message);
                        if( callback !=null && typeof callback === 'function'){
                            callback(responseText);
                        }
                        $.validator.unobtrusive.parse($('form'));
                    } else{
                        console.log(responseText);console.log(textStatus);console.log(XMLHttpRequest);
                    }                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    $('#from_result').html("An error occurred while trying to access the server, Kindly retry again.</br>" +
                        "If problem persists, kindly check your network and contact support@geeckoltd.com");
                },
                beforeSend: function () {
                    showWaiting();
                    //location.reload(true);
                },
                complete: function () {
                    closeWaiting();
                    //location.reload(true);
                }
            });
        }
        return false;
    });

    $("form button[type=submit]").click(function () {
        $("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });
}
function showLoading() {
    if ($('#loading').length < 1) {
        var $div = $('<div />').appendTo('body,html');
        $div.attr('id', 'loading');
    }
    $('#loading').css('visibility', 'visible');
}
function closeLoading() {
    $('#loading').remove();
    $('#loading').css('visibility', 'hidden');
}

function showWaiting() {
    $.blockUI({
        message: $('#wait')
    });
}

function closeWaiting() {
    $.unblockUI();
}

function getItems(query, targetControl, actionName, controllerName, targetUL, targerDiv, hdnFieldValue, field, custControls) {
    if (query.length < 1) {
        $("#" + targetUL).find("li").remove();
        $("#" + targetUL).remove();
        return;
    }

    //Here we are using ajax get method to fetch data from the list based on the user entered value in the textbox.
    //We are sending query i.e textbox as data. @Url.Action(actionName, controllerName)
    $.ajax({
        url: "/" + controllerName + "/" + actionName,
        data: { "query": query, "field": field },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            console.log(response);
            if (response.Data != null && response.Data.length != null && response.Data.length > 0) {
                if ($("#" + targetUL) != undefined) {
                    //If the UL element is not null or undefined we are clearing it, so that the result is appended in new UL every next time.
                    $("#" + targetUL).remove();
                }
                //assigning json response data to <span id="IL_AD1" class="IL_AD">local variable</span>. It is basically list of values.
                data = response.Data;
                //appending an UL element to show the values.
                $("#" + targerDiv).append($("<ul id='" + targetUL + "' class='targetUL'></ul>"));
                //Removing previously added li elements to the list.
                $("#" + targetUL).find("li").remove();
                //We are iterating over the list returned by the json and for each element we are creating a li element and appending 
                //the li element to ul element.
                $("#" + targetControl).focusout(function () {
                    //$("#" + targetUL).find("li").remove();
                    //$("#" + targetUL).remove();
                    if ($("#" + hdnFieldValue).val() == null || $("#" + hdnFieldValue).val().length < 1) {
                        $("#" + custControls.Email).val(null);
                        $("#" + custControls.Telephone).val(null);
                        $("#" + custControls.PersonId).val(null);
                        $("#" + custControls.Name).val(null);
                        $("#" + custControls.Organisation).val(null);
                    }
                });

                $.each(data, function (i, value) {
                    var appenddata = {
                        value: value,
                        targetControl: targetControl,
                        hdnFieldValue: hdnFieldValue,
                        targetUL: targetUL,
                        custControls: custControls
                    };

                    var spanval = field == 'name' ? value.Name : field == 'email' ? value.Email : value.Telephone;

                    $("#" + targetUL).append($("<li class='targetLI' " +
                            "onclick='javascript:appendTextToTextBox(this," + JSON.stringify(appenddata) + ")'>" +
                            "<span class='text'>" + spanval + "</span></li>"));
                });
            }
            else {
                //If data is null the we are removing the li and ul elements.
                $("#" + targetUL).find("li").remove();
                $("#" + targetUL).remove();
            }
        },
        error: function (xhr, status, error) {
        }
    });
}

//This method appends the text oc clicked li element to textbox.
function appendTextToTextBox(e, data) {
    //Getting the text of selected li element.
    var textToappend = e.innerText;
    //setting the value attribute of textbox with selected li element.
    $("#" + data.custControls.Email).val(data.value.Email);
    $("#" + data.custControls.Telephone).val(data.value.Telephone);
    $("#" + data.custControls.PersonId).val(data.value.PersonId);
    $("#" + data.custControls.Name).val(data.value.Name);
    $("#" + data.custControls.Organisation).val(data.value.Organisation);
    //Removing the ul element once selected element is set to textbox.
    $("#" + data.targetUL).remove();
}

function loadMapDialog(url, divElementId, title,) {
    showLoading();
    divElementId = "#" + divElementId;
    $(divElementId).load(url, function (responseText, textStatus, XMLHttpRequest) {
        responseText = responseText.replace(/mapcanvas/gi, "mapMapDialog");
        $(divElementId).dialog({
            title: title,
            autoOpen: false,
            resizable: false,
            width: 'auto',
            autoResize: true,
            show: { effect: 'drop', direction: "up" },
            draggable: true,
            open: function (event, ui) {
                $(this).html(textStatus == 'success' ? responseText : 'An error occured while loading dialog. </br> Kindly try again');
                closeLoading();
                $("div.ui-dialog-content").bind('DOMNodeInserted', function (e) {
                    $(this).dialog('option', 'position', 'center');
                    return;
                });
            }
        });
        $(divElementId).dialog('open');
    });
}
function InitGrid(gridData){
    var $tbody = $('#'+gridData +' > tbody');
    $tbody.addClass('data_container');

    var $thhead = $('#'+gridData +' > thead > tr > th');
    $thhead.addClass('subheader_cell');
}

//                        $("div.ui-dialog-content").dialog('close');
//                        $('#dataGrid').load(window.location.href + window.location.href.indexOf('?') > -1 ? '&dataGrid=true' : "?dataGrid=true", function () {
//                            $('tbody > tr:first').effect("highlight", {}, 2000);
//                        });

String.prototype.format = String.prototype.f = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};