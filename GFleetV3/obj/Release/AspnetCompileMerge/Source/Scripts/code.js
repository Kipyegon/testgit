﻿/*jquery 1.3.2*/(function () {
    var l = this, g, y = l.jQuery, p = l.$, o = l.jQuery = l.$ = function (E, F) { return new o.fn.init(E, F) }, D = /^[^<]*(<(.|\s)+>)[^>]*$|^#([\w-]+)$/, f = /^.[^:#\[\.,]*$/; o.fn = o.prototype = { init: function (E, H) { E = E || document; if (E.nodeType) { this[0] = E; this.length = 1; this.context = E; return this } if (typeof E === "string") { var G = D.exec(E); if (G && (G[1] || !H)) { if (G[1]) { E = o.clean([G[1]], H) } else { var I = document.getElementById(G[3]); if (I && I.id != G[3]) { return o().find(E) } var F = o(I || []); F.context = document; F.selector = E; return F } } else { return o(H).find(E) } } else { if (o.isFunction(E)) { return o(document).ready(E) } } if (E.selector && E.context) { this.selector = E.selector; this.context = E.context } return this.setArray(o.isArray(E) ? E : o.makeArray(E)) }, selector: "", jquery: "1.3.2", size: function () { return this.length }, get: function (E) { return E === g ? Array.prototype.slice.call(this) : this[E] }, pushStack: function (F, H, E) { var G = o(F); G.prevObject = this; G.context = this.context; if (H === "find") { G.selector = this.selector + (this.selector ? " " : "") + E } else { if (H) { G.selector = this.selector + "." + H + "(" + E + ")" } } return G }, setArray: function (E) { this.length = 0; Array.prototype.push.apply(this, E); return this }, each: function (F, E) { return o.each(this, F, E) }, index: function (E) { return o.inArray(E && E.jquery ? E[0] : E, this) }, attr: function (F, H, G) { var E = F; if (typeof F === "string") { if (H === g) { return this[0] && o[G || "attr"](this[0], F) } else { E = {}; E[F] = H } } return this.each(function (I) { for (F in E) { o.attr(G ? this.style : this, F, o.prop(this, E[F], G, I, F)) } }) }, css: function (E, F) { if ((E == "width" || E == "height") && parseFloat(F) < 0) { F = g } return this.attr(E, F, "curCSS") }, text: function (F) { if (typeof F !== "object" && F != null) { return this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(F)) } var E = ""; o.each(F || this, function () { o.each(this.childNodes, function () { if (this.nodeType != 8) { E += this.nodeType != 1 ? this.nodeValue : o.fn.text([this]) } }) }); return E }, wrapAll: function (E) { if (this[0]) { var F = o(E, this[0].ownerDocument).clone(); if (this[0].parentNode) { F.insertBefore(this[0]) } F.map(function () { var G = this; while (G.firstChild) { G = G.firstChild } return G }).append(this) } return this }, wrapInner: function (E) { return this.each(function () { o(this).contents().wrapAll(E) }) }, wrap: function (E) { return this.each(function () { o(this).wrapAll(E) }) }, append: function () { return this.domManip(arguments, true, function (E) { if (this.nodeType == 1) { this.appendChild(E) } }) }, prepend: function () { return this.domManip(arguments, true, function (E) { if (this.nodeType == 1) { this.insertBefore(E, this.firstChild) } }) }, before: function () { return this.domManip(arguments, false, function (E) { this.parentNode.insertBefore(E, this) }) }, after: function () { return this.domManip(arguments, false, function (E) { this.parentNode.insertBefore(E, this.nextSibling) }) }, end: function () { return this.prevObject || o([]) }, push: [].push, sort: [].sort, splice: [].splice, find: function (E) { if (this.length === 1) { var F = this.pushStack([], "find", E); F.length = 0; o.find(E, this[0], F); return F } else { return this.pushStack(o.unique(o.map(this, function (G) { return o.find(E, G) })), "find", E) } }, clone: function (G) { var E = this.map(function () { if (!o.support.noCloneEvent && !o.isXMLDoc(this)) { var I = this.outerHTML; if (!I) { var J = this.ownerDocument.createElement("div"); J.appendChild(this.cloneNode(true)); I = J.innerHTML } return o.clean([I.replace(/ jQuery\d+="(?:\d+|null)"/g, "").replace(/^\s*/, "")])[0] } else { return this.cloneNode(true) } }); if (G === true) { var H = this.find("*").andSelf(), F = 0; E.find("*").andSelf().each(function () { if (this.nodeName !== H[F].nodeName) { return } var I = o.data(H[F], "events"); for (var K in I) { for (var J in I[K]) { o.event.add(this, K, I[K][J], I[K][J].data) } } F++ }) } return E }, filter: function (E) { return this.pushStack(o.isFunction(E) && o.grep(this, function (G, F) { return E.call(G, F) }) || o.multiFilter(E, o.grep(this, function (F) { return F.nodeType === 1 })), "filter", E) }, closest: function (E) { var G = o.expr.match.POS.test(E) ? o(E) : null, F = 0; return this.map(function () { var H = this; while (H && H.ownerDocument) { if (G ? G.index(H) > -1 : o(H).is(E)) { o.data(H, "closest", F); return H } H = H.parentNode; F++ } }) }, not: function (E) { if (typeof E === "string") { if (f.test(E)) { return this.pushStack(o.multiFilter(E, this, true), "not", E) } else { E = o.multiFilter(E, this) } } var F = E.length && E[E.length - 1] !== g && !E.nodeType; return this.filter(function () { return F ? o.inArray(this, E) < 0 : this != E }) }, add: function (E) { return this.pushStack(o.unique(o.merge(this.get(), typeof E === "string" ? o(E) : o.makeArray(E)))) }, is: function (E) { return !!E && o.multiFilter(E, this).length > 0 }, hasClass: function (E) { return !!E && this.is("." + E) }, val: function (K) { if (K === g) { var E = this[0]; if (E) { if (o.nodeName(E, "option")) { return (E.attributes.value || {}).specified ? E.value : E.text } if (o.nodeName(E, "select")) { var I = E.selectedIndex, L = [], M = E.options, H = E.type == "select-one"; if (I < 0) { return null } for (var F = H ? I : 0, J = H ? I + 1 : M.length; F < J; F++) { var G = M[F]; if (G.selected) { K = o(G).val(); if (H) { return K } L.push(K) } } return L } return (E.value || "").replace(/\r/g, "") } return g } if (typeof K === "number") { K += "" } return this.each(function () { if (this.nodeType != 1) { return } if (o.isArray(K) && /radio|checkbox/.test(this.type)) { this.checked = (o.inArray(this.value, K) >= 0 || o.inArray(this.name, K) >= 0) } else { if (o.nodeName(this, "select")) { var N = o.makeArray(K); o("option", this).each(function () { this.selected = (o.inArray(this.value, N) >= 0 || o.inArray(this.text, N) >= 0) }); if (!N.length) { this.selectedIndex = -1 } } else { this.value = K } } }) }, html: function (E) { return E === g ? (this[0] ? this[0].innerHTML.replace(/ jQuery\d+="(?:\d+|null)"/g, "") : null) : this.empty().append(E) }, replaceWith: function (E) { return this.after(E).remove() }, eq: function (E) { return this.slice(E, +E + 1) }, slice: function () { return this.pushStack(Array.prototype.slice.apply(this, arguments), "slice", Array.prototype.slice.call(arguments).join(",")) }, map: function (E) { return this.pushStack(o.map(this, function (G, F) { return E.call(G, F, G) })) }, andSelf: function () { return this.add(this.prevObject) }, domManip: function (J, M, L) { if (this[0]) { var I = (this[0].ownerDocument || this[0]).createDocumentFragment(), F = o.clean(J, (this[0].ownerDocument || this[0]), I), H = I.firstChild; if (H) { for (var G = 0, E = this.length; G < E; G++) { L.call(K(this[G], H), this.length > 1 || G > 0 ? I.cloneNode(true) : I) } } if (F) { o.each(F, z) } } return this; function K(N, O) { return M && o.nodeName(N, "table") && o.nodeName(O, "tr") ? (N.getElementsByTagName("tbody")[0] || N.appendChild(N.ownerDocument.createElement("tbody"))) : N } } }; o.fn.init.prototype = o.fn; function z(E, F) { if (F.src) { o.ajax({ url: F.src, async: false, dataType: "script" }) } else { o.globalEval(F.text || F.textContent || F.innerHTML || "") } if (F.parentNode) { F.parentNode.removeChild(F) } } function e() { return +new Date } o.extend = o.fn.extend = function () { var J = arguments[0] || {}, H = 1, I = arguments.length, E = false, G; if (typeof J === "boolean") { E = J; J = arguments[1] || {}; H = 2 } if (typeof J !== "object" && !o.isFunction(J)) { J = {} } if (I == H) { J = this; --H } for (; H < I; H++) { if ((G = arguments[H]) != null) { for (var F in G) { var K = J[F], L = G[F]; if (J === L) { continue } if (E && L && typeof L === "object" && !L.nodeType) { J[F] = o.extend(E, K || (L.length != null ? [] : {}), L) } else { if (L !== g) { J[F] = L } } } } } return J }; var b = /z-?index|font-?weight|opacity|zoom|line-?height/i, q = document.defaultView || {}, s = Object.prototype.toString; o.extend({ noConflict: function (E) { l.$ = p; if (E) { l.jQuery = y } return o }, isFunction: function (E) { return s.call(E) === "[object Function]" }, isArray: function (E) { return s.call(E) === "[object Array]" }, isXMLDoc: function (E) { return E.nodeType === 9 && E.documentElement.nodeName !== "HTML" || !!E.ownerDocument && o.isXMLDoc(E.ownerDocument) }, globalEval: function (G) { if (G && /\S/.test(G)) { var F = document.getElementsByTagName("head")[0] || document.documentElement, E = document.createElement("script"); E.type = "text/javascript"; if (o.support.scriptEval) { E.appendChild(document.createTextNode(G)) } else { E.text = G } F.insertBefore(E, F.firstChild); F.removeChild(E) } }, nodeName: function (F, E) { return F.nodeName && F.nodeName.toUpperCase() == E.toUpperCase() }, each: function (G, K, F) { var E, H = 0, I = G.length; if (F) { if (I === g) { for (E in G) { if (K.apply(G[E], F) === false) { break } } } else { for (; H < I;) { if (K.apply(G[H++], F) === false) { break } } } } else { if (I === g) { for (E in G) { if (K.call(G[E], E, G[E]) === false) { break } } } else { for (var J = G[0]; H < I && K.call(J, H, J) !== false; J = G[++H]) { } } } return G }, prop: function (H, I, G, F, E) { if (o.isFunction(I)) { I = I.call(H, F) } return typeof I === "number" && G == "curCSS" && !b.test(E) ? I + "px" : I }, className: { add: function (E, F) { o.each((F || "").split(/\s+/), function (G, H) { if (E.nodeType == 1 && !o.className.has(E.className, H)) { E.className += (E.className ? " " : "") + H } }) }, remove: function (E, F) { if (E.nodeType == 1) { E.className = F !== g ? o.grep(E.className.split(/\s+/), function (G) { return !o.className.has(F, G) }).join(" ") : "" } }, has: function (F, E) { return F && o.inArray(E, (F.className || F).toString().split(/\s+/)) > -1 } }, swap: function (H, G, I) { var E = {}; for (var F in G) { E[F] = H.style[F]; H.style[F] = G[F] } I.call(H); for (var F in G) { H.style[F] = E[F] } }, css: function (H, F, J, E) { if (F == "width" || F == "height") { var L, G = { position: "absolute", visibility: "hidden", display: "block" }, K = F == "width" ? ["Left", "Right"] : ["Top", "Bottom"]; function I() { L = F == "width" ? H.offsetWidth : H.offsetHeight; if (E === "border") { return } o.each(K, function () { if (!E) { L -= parseFloat(o.curCSS(H, "padding" + this, true)) || 0 } if (E === "margin") { L += parseFloat(o.curCSS(H, "margin" + this, true)) || 0 } else { L -= parseFloat(o.curCSS(H, "border" + this + "Width", true)) || 0 } }) } if (H.offsetWidth !== 0) { I() } else { o.swap(H, G, I) } return Math.max(0, Math.round(L)) } return o.curCSS(H, F, J) }, curCSS: function (I, F, G) { var L, E = I.style; if (F == "opacity" && !o.support.opacity) { L = o.attr(E, "opacity"); return L == "" ? "1" : L } if (F.match(/float/i)) { F = w } if (!G && E && E[F]) { L = E[F] } else { if (q.getComputedStyle) { if (F.match(/float/i)) { F = "float" } F = F.replace(/([A-Z])/g, "-$1").toLowerCase(); var M = q.getComputedStyle(I, null); if (M) { L = M.getPropertyValue(F) } if (F == "opacity" && L == "") { L = "1" } } else { if (I.currentStyle) { var J = F.replace(/\-(\w)/g, function (N, O) { return O.toUpperCase() }); L = I.currentStyle[F] || I.currentStyle[J]; if (!/^\d+(px)?$/i.test(L) && /^\d/.test(L)) { var H = E.left, K = I.runtimeStyle.left; I.runtimeStyle.left = I.currentStyle.left; E.left = L || 0; L = E.pixelLeft + "px"; E.left = H; I.runtimeStyle.left = K } } } } return L }, clean: function (F, K, I) { K = K || document; if (typeof K.createElement === "undefined") { K = K.ownerDocument || K[0] && K[0].ownerDocument || document } if (!I && F.length === 1 && typeof F[0] === "string") { var H = /^<(\w+)\s*\/?>$/.exec(F[0]); if (H) { return [K.createElement(H[1])] } } var G = [], E = [], L = K.createElement("div"); o.each(F, function (P, S) { if (typeof S === "number") { S += "" } if (!S) { return } if (typeof S === "string") { S = S.replace(/(<(\w+)[^>]*?)\/>/g, function (U, V, T) { return T.match(/^(abbr|br|col|img|input|link|meta|param|hr|area|embed)$/i) ? U : V + "></" + T + ">" }); var O = S.replace(/^\s+/, "").substring(0, 10).toLowerCase(); var Q = !O.indexOf("<opt") && [1, "<select multiple='multiple'>", "</select>"] || !O.indexOf("<leg") && [1, "<fieldset>", "</fieldset>"] || O.match(/^<(thead|tbody|tfoot|colg|cap)/) && [1, "<table>", "</table>"] || !O.indexOf("<tr") && [2, "<table><tbody>", "</tbody></table>"] || (!O.indexOf("<td") || !O.indexOf("<th")) && [3, "<table><tbody><tr>", "</tr></tbody></table>"] || !O.indexOf("<col") && [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"] || !o.support.htmlSerialize && [1, "div<div>", "</div>"] || [0, "", ""]; L.innerHTML = Q[1] + S + Q[2]; while (Q[0]--) { L = L.lastChild } if (!o.support.tbody) { var R = /<tbody/i.test(S), N = !O.indexOf("<table") && !R ? L.firstChild && L.firstChild.childNodes : Q[1] == "<table>" && !R ? L.childNodes : []; for (var M = N.length - 1; M >= 0; --M) { if (o.nodeName(N[M], "tbody") && !N[M].childNodes.length) { N[M].parentNode.removeChild(N[M]) } } } if (!o.support.leadingWhitespace && /^\s/.test(S)) { L.insertBefore(K.createTextNode(S.match(/^\s*/)[0]), L.firstChild) } S = o.makeArray(L.childNodes) } if (S.nodeType) { G.push(S) } else { G = o.merge(G, S) } }); if (I) { for (var J = 0; G[J]; J++) { if (o.nodeName(G[J], "script") && (!G[J].type || G[J].type.toLowerCase() === "text/javascript")) { E.push(G[J].parentNode ? G[J].parentNode.removeChild(G[J]) : G[J]) } else { if (G[J].nodeType === 1) { G.splice.apply(G, [J + 1, 0].concat(o.makeArray(G[J].getElementsByTagName("script")))) } I.appendChild(G[J]) } } return E } return G }, attr: function (J, G, K) { if (!J || J.nodeType == 3 || J.nodeType == 8) { return g } var H = !o.isXMLDoc(J), L = K !== g; G = H && o.props[G] || G; if (J.tagName) { var F = /href|src|style/.test(G); if (G == "selected" && J.parentNode) { J.parentNode.selectedIndex } if (G in J && H && !F) { if (L) { if (G == "type" && o.nodeName(J, "input") && J.parentNode) { throw "type property can't be changed" } J[G] = K } if (o.nodeName(J, "form") && J.getAttributeNode(G)) { return J.getAttributeNode(G).nodeValue } if (G == "tabIndex") { var I = J.getAttributeNode("tabIndex"); return I && I.specified ? I.value : J.nodeName.match(/(button|input|object|select|textarea)/i) ? 0 : J.nodeName.match(/^(a|area)$/i) && J.href ? 0 : g } return J[G] } if (!o.support.style && H && G == "style") { return o.attr(J.style, "cssText", K) } if (L) { J.setAttribute(G, "" + K) } var E = !o.support.hrefNormalized && H && F ? J.getAttribute(G, 2) : J.getAttribute(G); return E === null ? g : E } if (!o.support.opacity && G == "opacity") { if (L) { J.zoom = 1; J.filter = (J.filter || "").replace(/alpha\([^)]*\)/, "") + (parseInt(K) + "" == "NaN" ? "" : "alpha(opacity=" + K * 100 + ")") } return J.filter && J.filter.indexOf("opacity=") >= 0 ? (parseFloat(J.filter.match(/opacity=([^)]*)/)[1]) / 100) + "" : "" } G = G.replace(/-([a-z])/ig, function (M, N) { return N.toUpperCase() }); if (L) { J[G] = K } return J[G] }, trim: function (E) { return (E || "").replace(/^\s+|\s+$/g, "") }, makeArray: function (G) { var E = []; if (G != null) { var F = G.length; if (F == null || typeof G === "string" || o.isFunction(G) || G.setInterval) { E[0] = G } else { while (F) { E[--F] = G[F] } } } return E }, inArray: function (G, H) { for (var E = 0, F = H.length; E < F; E++) { if (H[E] === G) { return E } } return -1 }, merge: function (H, E) { var F = 0, G, I = H.length; if (!o.support.getAll) { while ((G = E[F++]) != null) { if (G.nodeType != 8) { H[I++] = G } } } else { while ((G = E[F++]) != null) { H[I++] = G } } return H }, unique: function (K) { var F = [], E = {}; try { for (var G = 0, H = K.length; G < H; G++) { var J = o.data(K[G]); if (!E[J]) { E[J] = true; F.push(K[G]) } } } catch (I) { F = K } return F }, grep: function (F, J, E) { var G = []; for (var H = 0, I = F.length; H < I; H++) { if (!E != !J(F[H], H)) { G.push(F[H]) } } return G }, map: function (E, J) { var F = []; for (var G = 0, H = E.length; G < H; G++) { var I = J(E[G], G); if (I != null) { F[F.length] = I } } return F.concat.apply([], F) } }); var C = navigator.userAgent.toLowerCase(); o.browser = { version: (C.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [0, "0"])[1], safari: /webkit/.test(C), opera: /opera/.test(C), msie: /msie/.test(C) && !/opera/.test(C), mozilla: /mozilla/.test(C) && !/(compatible|webkit)/.test(C) }; o.each({ parent: function (E) { return E.parentNode }, parents: function (E) { return o.dir(E, "parentNode") }, next: function (E) { return o.nth(E, 2, "nextSibling") }, prev: function (E) { return o.nth(E, 2, "previousSibling") }, nextAll: function (E) { return o.dir(E, "nextSibling") }, prevAll: function (E) { return o.dir(E, "previousSibling") }, siblings: function (E) { return o.sibling(E.parentNode.firstChild, E) }, children: function (E) { return o.sibling(E.firstChild) }, contents: function (E) { return o.nodeName(E, "iframe") ? E.contentDocument || E.contentWindow.document : o.makeArray(E.childNodes) } }, function (E, F) { o.fn[E] = function (G) { var H = o.map(this, F); if (G && typeof G == "string") { H = o.multiFilter(G, H) } return this.pushStack(o.unique(H), E, G) } }); o.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (E, F) { o.fn[E] = function (G) { var J = [], L = o(G); for (var K = 0, H = L.length; K < H; K++) { var I = (K > 0 ? this.clone(true) : this).get(); o.fn[F].apply(o(L[K]), I); J = J.concat(I) } return this.pushStack(J, E, G) } }); o.each({ removeAttr: function (E) { o.attr(this, E, ""); if (this.nodeType == 1) { this.removeAttribute(E) } }, addClass: function (E) { o.className.add(this, E) }, removeClass: function (E) { o.className.remove(this, E) }, toggleClass: function (F, E) { if (typeof E !== "boolean") { E = !o.className.has(this, F) } o.className[E ? "add" : "remove"](this, F) }, remove: function (E) { if (!E || o.filter(E, [this]).length) { o("*", this).add([this]).each(function () { o.event.remove(this); o.removeData(this) }); if (this.parentNode) { this.parentNode.removeChild(this) } } }, empty: function () { o(this).children().remove(); while (this.firstChild) { this.removeChild(this.firstChild) } } }, function (E, F) { o.fn[E] = function () { return this.each(F, arguments) } }); function j(E, F) { return E[0] && parseInt(o.curCSS(E[0], F, true), 10) || 0 } var h = "jQuery" + e(), v = 0, A = {}; o.extend({ cache: {}, data: function (F, E, G) { F = F == l ? A : F; var H = F[h]; if (!H) { H = F[h] = ++v } if (E && !o.cache[H]) { o.cache[H] = {} } if (G !== g) { o.cache[H][E] = G } return E ? o.cache[H][E] : H }, removeData: function (F, E) { F = F == l ? A : F; var H = F[h]; if (E) { if (o.cache[H]) { delete o.cache[H][E]; E = ""; for (E in o.cache[H]) { break } if (!E) { o.removeData(F) } } } else { try { delete F[h] } catch (G) { if (F.removeAttribute) { F.removeAttribute(h) } } delete o.cache[H] } }, queue: function (F, E, H) { if (F) { E = (E || "fx") + "queue"; var G = o.data(F, E); if (!G || o.isArray(H)) { G = o.data(F, E, o.makeArray(H)) } else { if (H) { G.push(H) } } } return G }, dequeue: function (H, G) { var E = o.queue(H, G), F = E.shift(); if (!G || G === "fx") { F = E[0] } if (F !== g) { F.call(H) } } }); o.fn.extend({ data: function (E, G) { var H = E.split("."); H[1] = H[1] ? "." + H[1] : ""; if (G === g) { var F = this.triggerHandler("getData" + H[1] + "!", [H[0]]); if (F === g && this.length) { F = o.data(this[0], E) } return F === g && H[1] ? this.data(H[0]) : F } else { return this.trigger("setData" + H[1] + "!", [H[0], G]).each(function () { o.data(this, E, G) }) } }, removeData: function (E) { return this.each(function () { o.removeData(this, E) }) }, queue: function (E, F) { if (typeof E !== "string") { F = E; E = "fx" } if (F === g) { return o.queue(this[0], E) } return this.each(function () { var G = o.queue(this, E, F); if (E == "fx" && G.length == 1) { G[0].call(this) } }) }, dequeue: function (E) { return this.each(function () { o.dequeue(this, E) }) } });
    /*sizzle 0.9.3*/(function () { var R = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?/g, L = 0, H = Object.prototype.toString; var F = function (Y, U, ab, ac) { ab = ab || []; U = U || document; if (U.nodeType !== 1 && U.nodeType !== 9) { return [] } if (!Y || typeof Y !== "string") { return ab } var Z = [], W, af, ai, T, ad, V, X = true; R.lastIndex = 0; while ((W = R.exec(Y)) !== null) { Z.push(W[1]); if (W[2]) { V = RegExp.rightContext; break } } if (Z.length > 1 && M.exec(Y)) { if (Z.length === 2 && I.relative[Z[0]]) { af = J(Z[0] + Z[1], U) } else { af = I.relative[Z[0]] ? [U] : F(Z.shift(), U); while (Z.length) { Y = Z.shift(); if (I.relative[Y]) { Y += Z.shift() } af = J(Y, af) } } } else { var ae = ac ? { expr: Z.pop(), set: E(ac) } : F.find(Z.pop(), Z.length === 1 && U.parentNode ? U.parentNode : U, Q(U)); af = F.filter(ae.expr, ae.set); if (Z.length > 0) { ai = E(af) } else { X = false } while (Z.length) { var ah = Z.pop(), ag = ah; if (!I.relative[ah]) { ah = "" } else { ag = Z.pop() } if (ag == null) { ag = U } I.relative[ah](ai, ag, Q(U)) } } if (!ai) { ai = af } if (!ai) { throw "Syntax error, unrecognized expression: " + (ah || Y) } if (H.call(ai) === "[object Array]") { if (!X) { ab.push.apply(ab, ai) } else { if (U.nodeType === 1) { for (var aa = 0; ai[aa] != null; aa++) { if (ai[aa] && (ai[aa] === true || ai[aa].nodeType === 1 && K(U, ai[aa]))) { ab.push(af[aa]) } } } else { for (var aa = 0; ai[aa] != null; aa++) { if (ai[aa] && ai[aa].nodeType === 1) { ab.push(af[aa]) } } } } } else { E(ai, ab) } if (V) { F(V, U, ab, ac); if (G) { hasDuplicate = false; ab.sort(G); if (hasDuplicate) { for (var aa = 1; aa < ab.length; aa++) { if (ab[aa] === ab[aa - 1]) { ab.splice(aa--, 1) } } } } } return ab }; F.matches = function (T, U) { return F(T, null, null, U) }; F.find = function (aa, T, ab) { var Z, X; if (!aa) { return [] } for (var W = 0, V = I.order.length; W < V; W++) { var Y = I.order[W], X; if ((X = I.match[Y].exec(aa))) { var U = RegExp.leftContext; if (U.substr(U.length - 1) !== "\\") { X[1] = (X[1] || "").replace(/\\/g, ""); Z = I.find[Y](X, T, ab); if (Z != null) { aa = aa.replace(I.match[Y], ""); break } } } } if (!Z) { Z = T.getElementsByTagName("*") } return { set: Z, expr: aa } }; F.filter = function (ad, ac, ag, W) { var V = ad, ai = [], aa = ac, Y, T, Z = ac && ac[0] && Q(ac[0]); while (ad && ac.length) { for (var ab in I.filter) { if ((Y = I.match[ab].exec(ad)) != null) { var U = I.filter[ab], ah, af; T = false; if (aa == ai) { ai = [] } if (I.preFilter[ab]) { Y = I.preFilter[ab](Y, aa, ag, ai, W, Z); if (!Y) { T = ah = true } else { if (Y === true) { continue } } } if (Y) { for (var X = 0; (af = aa[X]) != null; X++) { if (af) { ah = U(af, Y, X, aa); var ae = W ^ !!ah; if (ag && ah != null) { if (ae) { T = true } else { aa[X] = false } } else { if (ae) { ai.push(af); T = true } } } } } if (ah !== g) { if (!ag) { aa = ai } ad = ad.replace(I.match[ab], ""); if (!T) { return [] } break } } } if (ad == V) { if (T == null) { throw "Syntax error, unrecognized expression: " + ad } else { break } } V = ad } return aa }; var I = F.selectors = { order: ["ID", "NAME", "TAG"], match: { ID: /#((?:[\w\u00c0-\uFFFF_-]|\\.)+)/, CLASS: /\.((?:[\w\u00c0-\uFFFF_-]|\\.)+)/, NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF_-]|\\.)+)['"]*\]/, ATTR: /\[\s*((?:[\w\u00c0-\uFFFF_-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/, TAG: /^((?:[\w\u00c0-\uFFFF\*_-]|\\.)+)/, CHILD: /:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/, POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/, PSEUDO: /:((?:[\w\u00c0-\uFFFF_-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/ }, attrMap: { "class": "className", "for": "htmlFor" }, attrHandle: { href: function (T) { return T.getAttribute("href") } }, relative: { "+": function (aa, T, Z) { var X = typeof T === "string", ab = X && !/\W/.test(T), Y = X && !ab; if (ab && !Z) { T = T.toUpperCase() } for (var W = 0, V = aa.length, U; W < V; W++) { if ((U = aa[W])) { while ((U = U.previousSibling) && U.nodeType !== 1) { } aa[W] = Y || U && U.nodeName === T ? U || false : U === T } } if (Y) { F.filter(T, aa, true) } }, ">": function (Z, U, aa) { var X = typeof U === "string"; if (X && !/\W/.test(U)) { U = aa ? U : U.toUpperCase(); for (var V = 0, T = Z.length; V < T; V++) { var Y = Z[V]; if (Y) { var W = Y.parentNode; Z[V] = W.nodeName === U ? W : false } } } else { for (var V = 0, T = Z.length; V < T; V++) { var Y = Z[V]; if (Y) { Z[V] = X ? Y.parentNode : Y.parentNode === U } } if (X) { F.filter(U, Z, true) } } }, "": function (W, U, Y) { var V = L++, T = S; if (!U.match(/\W/)) { var X = U = Y ? U : U.toUpperCase(); T = P } T("parentNode", U, V, W, X, Y) }, "~": function (W, U, Y) { var V = L++, T = S; if (typeof U === "string" && !U.match(/\W/)) { var X = U = Y ? U : U.toUpperCase(); T = P } T("previousSibling", U, V, W, X, Y) } }, find: { ID: function (U, V, W) { if (typeof V.getElementById !== "undefined" && !W) { var T = V.getElementById(U[1]); return T ? [T] : [] } }, NAME: function (V, Y, Z) { if (typeof Y.getElementsByName !== "undefined") { var U = [], X = Y.getElementsByName(V[1]); for (var W = 0, T = X.length; W < T; W++) { if (X[W].getAttribute("name") === V[1]) { U.push(X[W]) } } return U.length === 0 ? null : U } }, TAG: function (T, U) { return U.getElementsByTagName(T[1]) } }, preFilter: { CLASS: function (W, U, V, T, Z, aa) { W = " " + W[1].replace(/\\/g, "") + " "; if (aa) { return W } for (var X = 0, Y; (Y = U[X]) != null; X++) { if (Y) { if (Z ^ (Y.className && (" " + Y.className + " ").indexOf(W) >= 0)) { if (!V) { T.push(Y) } } else { if (V) { U[X] = false } } } } return false }, ID: function (T) { return T[1].replace(/\\/g, "") }, TAG: function (U, T) { for (var V = 0; T[V] === false; V++) { } return T[V] && Q(T[V]) ? U[1] : U[1].toUpperCase() }, CHILD: function (T) { if (T[1] == "nth") { var U = /(-?)(\d*)n((?:\+|-)?\d*)/.exec(T[2] == "even" && "2n" || T[2] == "odd" && "2n+1" || !/\D/.test(T[2]) && "0n+" + T[2] || T[2]); T[2] = (U[1] + (U[2] || 1)) - 0; T[3] = U[3] - 0 } T[0] = L++; return T }, ATTR: function (X, U, V, T, Y, Z) { var W = X[1].replace(/\\/g, ""); if (!Z && I.attrMap[W]) { X[1] = I.attrMap[W] } if (X[2] === "~=") { X[4] = " " + X[4] + " " } return X }, PSEUDO: function (X, U, V, T, Y) { if (X[1] === "not") { if (X[3].match(R).length > 1 || /^\w/.test(X[3])) { X[3] = F(X[3], null, null, U) } else { var W = F.filter(X[3], U, V, true ^ Y); if (!V) { T.push.apply(T, W) } return false } } else { if (I.match.POS.test(X[0]) || I.match.CHILD.test(X[0])) { return true } } return X }, POS: function (T) { T.unshift(true); return T } }, filters: { enabled: function (T) { return T.disabled === false && T.type !== "hidden" }, disabled: function (T) { return T.disabled === true }, checked: function (T) { return T.checked === true }, selected: function (T) { T.parentNode.selectedIndex; return T.selected === true }, parent: function (T) { return !!T.firstChild }, empty: function (T) { return !T.firstChild }, has: function (V, U, T) { return !!F(T[3], V).length }, header: function (T) { return /h\d/i.test(T.nodeName) }, text: function (T) { return "text" === T.type }, radio: function (T) { return "radio" === T.type }, checkbox: function (T) { return "checkbox" === T.type }, file: function (T) { return "file" === T.type }, password: function (T) { return "password" === T.type }, submit: function (T) { return "submit" === T.type }, image: function (T) { return "image" === T.type }, reset: function (T) { return "reset" === T.type }, button: function (T) { return "button" === T.type || T.nodeName.toUpperCase() === "BUTTON" }, input: function (T) { return /input|select|textarea|button/i.test(T.nodeName) } }, setFilters: { first: function (U, T) { return T === 0 }, last: function (V, U, T, W) { return U === W.length - 1 }, even: function (U, T) { return T % 2 === 0 }, odd: function (U, T) { return T % 2 === 1 }, lt: function (V, U, T) { return U < T[3] - 0 }, gt: function (V, U, T) { return U > T[3] - 0 }, nth: function (V, U, T) { return T[3] - 0 == U }, eq: function (V, U, T) { return T[3] - 0 == U } }, filter: { PSEUDO: function (Z, V, W, aa) { var U = V[1], X = I.filters[U]; if (X) { return X(Z, W, V, aa) } else { if (U === "contains") { return (Z.textContent || Z.innerText || "").indexOf(V[3]) >= 0 } else { if (U === "not") { var Y = V[3]; for (var W = 0, T = Y.length; W < T; W++) { if (Y[W] === Z) { return false } } return true } } } }, CHILD: function (T, W) { var Z = W[1], U = T; switch (Z) { case "only": case "first": while (U = U.previousSibling) { if (U.nodeType === 1) { return false } } if (Z == "first") { return true } U = T; case "last": while (U = U.nextSibling) { if (U.nodeType === 1) { return false } } return true; case "nth": var V = W[2], ac = W[3]; if (V == 1 && ac == 0) { return true } var Y = W[0], ab = T.parentNode; if (ab && (ab.sizcache !== Y || !T.nodeIndex)) { var X = 0; for (U = ab.firstChild; U; U = U.nextSibling) { if (U.nodeType === 1) { U.nodeIndex = ++X } } ab.sizcache = Y } var aa = T.nodeIndex - ac; if (V == 0) { return aa == 0 } else { return (aa % V == 0 && aa / V >= 0) } } }, ID: function (U, T) { return U.nodeType === 1 && U.getAttribute("id") === T }, TAG: function (U, T) { return (T === "*" && U.nodeType === 1) || U.nodeName === T }, CLASS: function (U, T) { return (" " + (U.className || U.getAttribute("class")) + " ").indexOf(T) > -1 }, ATTR: function (Y, W) { var V = W[1], T = I.attrHandle[V] ? I.attrHandle[V](Y) : Y[V] != null ? Y[V] : Y.getAttribute(V), Z = T + "", X = W[2], U = W[4]; return T == null ? X === "!=" : X === "=" ? Z === U : X === "*=" ? Z.indexOf(U) >= 0 : X === "~=" ? (" " + Z + " ").indexOf(U) >= 0 : !U ? Z && T !== false : X === "!=" ? Z != U : X === "^=" ? Z.indexOf(U) === 0 : X === "$=" ? Z.substr(Z.length - U.length) === U : X === "|=" ? Z === U || Z.substr(0, U.length + 1) === U + "-" : false }, POS: function (X, U, V, Y) { var T = U[2], W = I.setFilters[T]; if (W) { return W(X, V, U, Y) } } } }; var M = I.match.POS; for (var O in I.match) { I.match[O] = RegExp(I.match[O].source + /(?![^\[]*\])(?![^\(]*\))/.source) } var E = function (U, T) { U = Array.prototype.slice.call(U); if (T) { T.push.apply(T, U); return T } return U }; try { Array.prototype.slice.call(document.documentElement.childNodes) } catch (N) { E = function (X, W) { var U = W || []; if (H.call(X) === "[object Array]") { Array.prototype.push.apply(U, X) } else { if (typeof X.length === "number") { for (var V = 0, T = X.length; V < T; V++) { U.push(X[V]) } } else { for (var V = 0; X[V]; V++) { U.push(X[V]) } } } return U } } var G; if (document.documentElement.compareDocumentPosition) { G = function (U, T) { var V = U.compareDocumentPosition(T) & 4 ? -1 : U === T ? 0 : 1; if (V === 0) { hasDuplicate = true } return V } } else { if ("sourceIndex" in document.documentElement) { G = function (U, T) { var V = U.sourceIndex - T.sourceIndex; if (V === 0) { hasDuplicate = true } return V } } else { if (document.createRange) { G = function (W, U) { var V = W.ownerDocument.createRange(), T = U.ownerDocument.createRange(); V.selectNode(W); V.collapse(true); T.selectNode(U); T.collapse(true); var X = V.compareBoundaryPoints(Range.START_TO_END, T); if (X === 0) { hasDuplicate = true } return X } } } } (function () { var U = document.createElement("form"), V = "script" + (new Date).getTime(); U.innerHTML = "<input name='" + V + "'/>"; var T = document.documentElement; T.insertBefore(U, T.firstChild); if (!!document.getElementById(V)) { I.find.ID = function (X, Y, Z) { if (typeof Y.getElementById !== "undefined" && !Z) { var W = Y.getElementById(X[1]); return W ? W.id === X[1] || typeof W.getAttributeNode !== "undefined" && W.getAttributeNode("id").nodeValue === X[1] ? [W] : g : [] } }; I.filter.ID = function (Y, W) { var X = typeof Y.getAttributeNode !== "undefined" && Y.getAttributeNode("id"); return Y.nodeType === 1 && X && X.nodeValue === W } } T.removeChild(U) })(); (function () { var T = document.createElement("div"); T.appendChild(document.createComment("")); if (T.getElementsByTagName("*").length > 0) { I.find.TAG = function (U, Y) { var X = Y.getElementsByTagName(U[1]); if (U[1] === "*") { var W = []; for (var V = 0; X[V]; V++) { if (X[V].nodeType === 1) { W.push(X[V]) } } X = W } return X } } T.innerHTML = "<a href='#'></a>"; if (T.firstChild && typeof T.firstChild.getAttribute !== "undefined" && T.firstChild.getAttribute("href") !== "#") { I.attrHandle.href = function (U) { return U.getAttribute("href", 2) } } })(); if (document.querySelectorAll) { (function () { var T = F, U = document.createElement("div"); U.innerHTML = "<p class='TEST'></p>"; if (U.querySelectorAll && U.querySelectorAll(".TEST").length === 0) { return } F = function (Y, X, V, W) { X = X || document; if (!W && X.nodeType === 9 && !Q(X)) { try { return E(X.querySelectorAll(Y), V) } catch (Z) { } } return T(Y, X, V, W) }; F.find = T.find; F.filter = T.filter; F.selectors = T.selectors; F.matches = T.matches })() } if (document.getElementsByClassName && document.documentElement.getElementsByClassName) { (function () { var T = document.createElement("div"); T.innerHTML = "<div class='test e'></div><div class='test'></div>"; if (T.getElementsByClassName("e").length === 0) { return } T.lastChild.className = "e"; if (T.getElementsByClassName("e").length === 1) { return } I.order.splice(1, 0, "CLASS"); I.find.CLASS = function (U, V, W) { if (typeof V.getElementsByClassName !== "undefined" && !W) { return V.getElementsByClassName(U[1]) } } })() } function P(U, Z, Y, ad, aa, ac) { var ab = U == "previousSibling" && !ac; for (var W = 0, V = ad.length; W < V; W++) { var T = ad[W]; if (T) { if (ab && T.nodeType === 1) { T.sizcache = Y; T.sizset = W } T = T[U]; var X = false; while (T) { if (T.sizcache === Y) { X = ad[T.sizset]; break } if (T.nodeType === 1 && !ac) { T.sizcache = Y; T.sizset = W } if (T.nodeName === Z) { X = T; break } T = T[U] } ad[W] = X } } } function S(U, Z, Y, ad, aa, ac) { var ab = U == "previousSibling" && !ac; for (var W = 0, V = ad.length; W < V; W++) { var T = ad[W]; if (T) { if (ab && T.nodeType === 1) { T.sizcache = Y; T.sizset = W } T = T[U]; var X = false; while (T) { if (T.sizcache === Y) { X = ad[T.sizset]; break } if (T.nodeType === 1) { if (!ac) { T.sizcache = Y; T.sizset = W } if (typeof Z !== "string") { if (T === Z) { X = true; break } } else { if (F.filter(Z, [T]).length > 0) { X = T; break } } } T = T[U] } ad[W] = X } } } var K = document.compareDocumentPosition ? function (U, T) { return U.compareDocumentPosition(T) & 16 } : function (U, T) { return U !== T && (U.contains ? U.contains(T) : true) }; var Q = function (T) { return T.nodeType === 9 && T.documentElement.nodeName !== "HTML" || !!T.ownerDocument && Q(T.ownerDocument) }; var J = function (T, aa) { var W = [], X = "", Y, V = aa.nodeType ? [aa] : aa; while ((Y = I.match.PSEUDO.exec(T))) { X += Y[0]; T = T.replace(I.match.PSEUDO, "") } T = I.relative[T] ? T + "*" : T; for (var Z = 0, U = V.length; Z < U; Z++) { F(T, V[Z], W) } return F.filter(X, W) }; o.find = F; o.filter = F.filter; o.expr = F.selectors; o.expr[":"] = o.expr.filters; F.selectors.filters.hidden = function (T) { return T.offsetWidth === 0 || T.offsetHeight === 0 }; F.selectors.filters.visible = function (T) { return T.offsetWidth > 0 || T.offsetHeight > 0 }; F.selectors.filters.animated = function (T) { return o.grep(o.timers, function (U) { return T === U.elem }).length }; o.multiFilter = function (V, T, U) { if (U) { V = ":not(" + V + ")" } return F.matches(V, T) }; o.dir = function (V, U) { var T = [], W = V[U]; while (W && W != document) { if (W.nodeType == 1) { T.push(W) } W = W[U] } return T }; o.nth = function (X, T, V, W) { T = T || 1; var U = 0; for (; X; X = X[V]) { if (X.nodeType == 1 && ++U == T) { break } } return X }; o.sibling = function (V, U) { var T = []; for (; V; V = V.nextSibling) { if (V.nodeType == 1 && V != U) { T.push(V) } } return T }; return; l.Sizzle = F })(); o.event = { add: function (I, F, H, K) { if (I.nodeType == 3 || I.nodeType == 8) { return } if (I.setInterval && I != l) { I = l } if (!H.guid) { H.guid = this.guid++ } if (K !== g) { var G = H; H = this.proxy(G); H.data = K } var E = o.data(I, "events") || o.data(I, "events", {}), J = o.data(I, "handle") || o.data(I, "handle", function () { return typeof o !== "undefined" && !o.event.triggered ? o.event.handle.apply(arguments.callee.elem, arguments) : g }); J.elem = I; o.each(F.split(/\s+/), function (M, N) { var O = N.split("."); N = O.shift(); H.type = O.slice().sort().join("."); var L = E[N]; if (o.event.specialAll[N]) { o.event.specialAll[N].setup.call(I, K, O) } if (!L) { L = E[N] = {}; if (!o.event.special[N] || o.event.special[N].setup.call(I, K, O) === false) { if (I.addEventListener) { I.addEventListener(N, J, false) } else { if (I.attachEvent) { I.attachEvent("on" + N, J) } } } } L[H.guid] = H; o.event.global[N] = true }); I = null }, guid: 1, global: {}, remove: function (K, H, J) { if (K.nodeType == 3 || K.nodeType == 8) { return } var G = o.data(K, "events"), F, E; if (G) { if (H === g || (typeof H === "string" && H.charAt(0) == ".")) { for (var I in G) { this.remove(K, I + (H || "")) } } else { if (H.type) { J = H.handler; H = H.type } o.each(H.split(/\s+/), function (M, O) { var Q = O.split("."); O = Q.shift(); var N = RegExp("(^|\\.)" + Q.slice().sort().join(".*\\.") + "(\\.|$)"); if (G[O]) { if (J) { delete G[O][J.guid] } else { for (var P in G[O]) { if (N.test(G[O][P].type)) { delete G[O][P] } } } if (o.event.specialAll[O]) { o.event.specialAll[O].teardown.call(K, Q) } for (F in G[O]) { break } if (!F) { if (!o.event.special[O] || o.event.special[O].teardown.call(K, Q) === false) { if (K.removeEventListener) { K.removeEventListener(O, o.data(K, "handle"), false) } else { if (K.detachEvent) { K.detachEvent("on" + O, o.data(K, "handle")) } } } F = null; delete G[O] } } }) } for (F in G) { break } if (!F) { var L = o.data(K, "handle"); if (L) { L.elem = null } o.removeData(K, "events"); o.removeData(K, "handle") } } }, trigger: function (I, K, H, E) { var G = I.type || I; if (!E) { I = typeof I === "object" ? I[h] ? I : o.extend(o.Event(G), I) : o.Event(G); if (G.indexOf("!") >= 0) { I.type = G = G.slice(0, -1); I.exclusive = true } if (!H) { I.stopPropagation(); if (this.global[G]) { o.each(o.cache, function () { if (this.events && this.events[G]) { o.event.trigger(I, K, this.handle.elem) } }) } } if (!H || H.nodeType == 3 || H.nodeType == 8) { return g } I.result = g; I.target = H; K = o.makeArray(K); K.unshift(I) } I.currentTarget = H; var J = o.data(H, "handle"); if (J) { J.apply(H, K) } if ((!H[G] || (o.nodeName(H, "a") && G == "click")) && H["on" + G] && H["on" + G].apply(H, K) === false) { I.result = false } if (!E && H[G] && !I.isDefaultPrevented() && !(o.nodeName(H, "a") && G == "click")) { this.triggered = true; try { H[G]() } catch (L) { } } this.triggered = false; if (!I.isPropagationStopped()) { var F = H.parentNode || H.ownerDocument; if (F) { o.event.trigger(I, K, F, true) } } }, handle: function (K) { var J, E; K = arguments[0] = o.event.fix(K || l.event); K.currentTarget = this; var L = K.type.split("."); K.type = L.shift(); J = !L.length && !K.exclusive; var I = RegExp("(^|\\.)" + L.slice().sort().join(".*\\.") + "(\\.|$)"); E = (o.data(this, "events") || {})[K.type]; for (var G in E) { var H = E[G]; if (J || I.test(H.type)) { K.handler = H; K.data = H.data; var F = H.apply(this, arguments); if (F !== g) { K.result = F; if (F === false) { K.preventDefault(); K.stopPropagation() } } if (K.isImmediatePropagationStopped()) { break } } } }, props: "altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode metaKey newValue originalTarget pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "), fix: function (H) { if (H[h]) { return H } var F = H; H = o.Event(F); for (var G = this.props.length, J; G;) { J = this.props[--G]; H[J] = F[J] } if (!H.target) { H.target = H.srcElement || document } if (H.target.nodeType == 3) { H.target = H.target.parentNode } if (!H.relatedTarget && H.fromElement) { H.relatedTarget = H.fromElement == H.target ? H.toElement : H.fromElement } if (H.pageX == null && H.clientX != null) { var I = document.documentElement, E = document.body; H.pageX = H.clientX + (I && I.scrollLeft || E && E.scrollLeft || 0) - (I.clientLeft || 0); H.pageY = H.clientY + (I && I.scrollTop || E && E.scrollTop || 0) - (I.clientTop || 0) } if (!H.which && ((H.charCode || H.charCode === 0) ? H.charCode : H.keyCode)) { H.which = H.charCode || H.keyCode } if (!H.metaKey && H.ctrlKey) { H.metaKey = H.ctrlKey } if (!H.which && H.button) { H.which = (H.button & 1 ? 1 : (H.button & 2 ? 3 : (H.button & 4 ? 2 : 0))) } return H }, proxy: function (F, E) { E = E || function () { return F.apply(this, arguments) }; E.guid = F.guid = F.guid || E.guid || this.guid++; return E }, special: { ready: { setup: B, teardown: function () { } } }, specialAll: { live: { setup: function (E, F) { o.event.add(this, F[0], c) }, teardown: function (G) { if (G.length) { var E = 0, F = RegExp("(^|\\.)" + G[0] + "(\\.|$)"); o.each((o.data(this, "events").live || {}), function () { if (F.test(this.type)) { E++ } }); if (E < 1) { o.event.remove(this, G[0], c) } } } } } }; o.Event = function (E) { if (!this.preventDefault) { return new o.Event(E) } if (E && E.type) { this.originalEvent = E; this.type = E.type } else { this.type = E } this.timeStamp = e(); this[h] = true }; function k() { return false } function u() { return true } o.Event.prototype = { preventDefault: function () { this.isDefaultPrevented = u; var E = this.originalEvent; if (!E) { return } if (E.preventDefault) { E.preventDefault() } E.returnValue = false }, stopPropagation: function () { this.isPropagationStopped = u; var E = this.originalEvent; if (!E) { return } if (E.stopPropagation) { E.stopPropagation() } E.cancelBubble = true }, stopImmediatePropagation: function () { this.isImmediatePropagationStopped = u; this.stopPropagation() }, isDefaultPrevented: k, isPropagationStopped: k, isImmediatePropagationStopped: k }; var a = function (F) { var E = F.relatedTarget; while (E && E != this) { try { E = E.parentNode } catch (G) { E = this } } if (E != this) { F.type = F.data; o.event.handle.apply(this, arguments) } }; o.each({ mouseover: "mouseenter", mouseout: "mouseleave" }, function (F, E) { o.event.special[E] = { setup: function () { o.event.add(this, F, a, E) }, teardown: function () { o.event.remove(this, F, a) } } }); o.fn.extend({ bind: function (F, G, E) { return F == "unload" ? this.one(F, G, E) : this.each(function () { o.event.add(this, F, E || G, E && G) }) }, one: function (G, H, F) { var E = o.event.proxy(F || H, function (I) { o(this).unbind(I, E); return (F || H).apply(this, arguments) }); return this.each(function () { o.event.add(this, G, E, F && H) }) }, unbind: function (F, E) { return this.each(function () { o.event.remove(this, F, E) }) }, trigger: function (E, F) { return this.each(function () { o.event.trigger(E, F, this) }) }, triggerHandler: function (E, G) { if (this[0]) { var F = o.Event(E); F.preventDefault(); F.stopPropagation(); o.event.trigger(F, G, this[0]); return F.result } }, toggle: function (G) { var E = arguments, F = 1; while (F < E.length) { o.event.proxy(G, E[F++]) } return this.click(o.event.proxy(G, function (H) { this.lastToggle = (this.lastToggle || 0) % F; H.preventDefault(); return E[this.lastToggle++].apply(this, arguments) || false })) }, hover: function (E, F) { return this.mouseenter(E).mouseleave(F) }, ready: function (E) { B(); if (o.isReady) { E.call(document, o) } else { o.readyList.push(E) } return this }, live: function (G, F) { var E = o.event.proxy(F); E.guid += this.selector + G; o(document).bind(i(G, this.selector), this.selector, E); return this }, die: function (F, E) { o(document).unbind(i(F, this.selector), E ? { guid: E.guid + this.selector + F } : null); return this } }); function c(H) { var E = RegExp("(^|\\.)" + H.type + "(\\.|$)"), G = true, F = []; o.each(o.data(this, "events").live || [], function (I, J) { if (E.test(J.type)) { var K = o(H.target).closest(J.data)[0]; if (K) { F.push({ elem: K, fn: J }) } } }); F.sort(function (J, I) { return o.data(J.elem, "closest") - o.data(I.elem, "closest") }); o.each(F, function () { if (this.fn.call(this.elem, H, this.fn.data) === false) { return (G = false) } }); return G } function i(F, E) { return ["live", F, E.replace(/\./g, "`").replace(/ /g, "|")].join(".") } o.extend({ isReady: false, readyList: [], ready: function () { if (!o.isReady) { o.isReady = true; if (o.readyList) { o.each(o.readyList, function () { this.call(document, o) }); o.readyList = null } o(document).triggerHandler("ready") } } }); var x = false; function B() { if (x) { return } x = true; if (document.addEventListener) { document.addEventListener("DOMContentLoaded", function () { document.removeEventListener("DOMContentLoaded", arguments.callee, false); o.ready() }, false) } else { if (document.attachEvent) { document.attachEvent("onreadystatechange", function () { if (document.readyState === "complete") { document.detachEvent("onreadystatechange", arguments.callee); o.ready() } }); if (document.documentElement.doScroll && l == l.top) { (function () { if (o.isReady) { return } try { document.documentElement.doScroll("left") } catch (E) { setTimeout(arguments.callee, 0); return } o.ready() })() } } } o.event.add(l, "load", o.ready) } o.each(("blur,focus,load,resize,scroll,unload,click,dblclick,mousedown,mouseup,mousemove,mouseover,mouseout,mouseenter,mouseleave,change,select,submit,keydown,keypress,keyup,error").split(","), function (F, E) { o.fn[E] = function (G) { return G ? this.bind(E, G) : this.trigger(E) } }); o(l).bind("unload", function () { for (var E in o.cache) { if (E != 1 && o.cache[E].handle) { o.event.remove(o.cache[E].handle.elem) } } }); (function () { o.support = {}; var F = document.documentElement, G = document.createElement("script"), K = document.createElement("div"), J = "script" + (new Date).getTime(); K.style.display = "none"; K.innerHTML = '   <link/><table></table><a href="/a" style="color:red;float:left;opacity:.5;">a</a><select><option>text</option></select><object><param/></object>'; var H = K.getElementsByTagName("*"), E = K.getElementsByTagName("a")[0]; if (!H || !H.length || !E) { return } o.support = { leadingWhitespace: K.firstChild.nodeType == 3, tbody: !K.getElementsByTagName("tbody").length, objectAll: !!K.getElementsByTagName("object")[0].getElementsByTagName("*").length, htmlSerialize: !!K.getElementsByTagName("link").length, style: /red/.test(E.getAttribute("style")), hrefNormalized: E.getAttribute("href") === "/a", opacity: E.style.opacity === "0.5", cssFloat: !!E.style.cssFloat, scriptEval: false, noCloneEvent: true, boxModel: null }; G.type = "text/javascript"; try { G.appendChild(document.createTextNode("window." + J + "=1;")) } catch (I) { } F.insertBefore(G, F.firstChild); if (l[J]) { o.support.scriptEval = true; delete l[J] } F.removeChild(G); if (K.attachEvent && K.fireEvent) { K.attachEvent("onclick", function () { o.support.noCloneEvent = false; K.detachEvent("onclick", arguments.callee) }); K.cloneNode(true).fireEvent("onclick") } o(function () { var L = document.createElement("div"); L.style.width = L.style.paddingLeft = "1px"; document.body.appendChild(L); o.boxModel = o.support.boxModel = L.offsetWidth === 2; document.body.removeChild(L).style.display = "none" }) })(); var w = o.support.cssFloat ? "cssFloat" : "styleFloat"; o.props = { "for": "htmlFor", "class": "className", "float": w, cssFloat: w, styleFloat: w, readonly: "readOnly", maxlength: "maxLength", cellspacing: "cellSpacing", rowspan: "rowSpan", tabindex: "tabIndex" }; o.fn.extend({ _load: o.fn.load, load: function (G, J, K) { if (typeof G !== "string") { return this._load(G) } var I = G.indexOf(" "); if (I >= 0) { var E = G.slice(I, G.length); G = G.slice(0, I) } var H = "GET"; if (J) { if (o.isFunction(J)) { K = J; J = null } else { if (typeof J === "object") { J = o.param(J); H = "POST" } } } var F = this; o.ajax({ url: G, type: H, dataType: "html", data: J, complete: function (M, L) { if (L == "success" || L == "notmodified") { F.html(E ? o("<div/>").append(M.responseText.replace(/<script(.|\s)*?\/script>/g, "")).find(E) : M.responseText) } if (K) { F.each(K, [M.responseText, L, M]) } } }); return this }, serialize: function () { return o.param(this.serializeArray()) }, serializeArray: function () { return this.map(function () { return this.elements ? o.makeArray(this.elements) : this }).filter(function () { return this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|hidden|password|search/i.test(this.type)) }).map(function (E, F) { var G = o(this).val(); return G == null ? null : o.isArray(G) ? o.map(G, function (I, H) { return { name: F.name, value: I } }) : { name: F.name, value: G } }).get() } }); o.each("ajaxStart,ajaxStop,ajaxComplete,ajaxError,ajaxSuccess,ajaxSend".split(","), function (E, F) { o.fn[F] = function (G) { return this.bind(F, G) } }); var r = e(); o.extend({ get: function (E, G, H, F) { if (o.isFunction(G)) { H = G; G = null } return o.ajax({ type: "GET", url: E, data: G, success: H, dataType: F }) }, getScript: function (E, F) { return o.get(E, null, F, "script") }, getJSON: function (E, F, G) { return o.get(E, F, G, "json") }, post: function (E, G, H, F) { if (o.isFunction(G)) { H = G; G = {} } return o.ajax({ type: "POST", url: E, data: G, success: H, dataType: F }) }, ajaxSetup: function (E) { o.extend(o.ajaxSettings, E) }, ajaxSettings: { url: location.href, global: true, type: "GET", contentType: "application/x-www-form-urlencoded", processData: true, async: true, xhr: function () { return l.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest() }, accepts: { xml: "application/xml, text/xml", html: "text/html", script: "text/javascript, application/javascript", json: "application/json, text/javascript", text: "text/plain", _default: "*/*" } }, lastModified: {}, ajax: function (M) { M = o.extend(true, M, o.extend(true, {}, o.ajaxSettings, M)); var W, F = /=\?(&|$)/g, R, V, G = M.type.toUpperCase(); if (M.data && M.processData && typeof M.data !== "string") { M.data = o.param(M.data) } if (M.dataType == "jsonp") { if (G == "GET") { if (!M.url.match(F)) { M.url += (M.url.match(/\?/) ? "&" : "?") + (M.jsonp || "callback") + "=?" } } else { if (!M.data || !M.data.match(F)) { M.data = (M.data ? M.data + "&" : "") + (M.jsonp || "callback") + "=?" } } M.dataType = "json" } if (M.dataType == "json" && (M.data && M.data.match(F) || M.url.match(F))) { W = "jsonp" + r++; if (M.data) { M.data = (M.data + "").replace(F, "=" + W + "$1") } M.url = M.url.replace(F, "=" + W + "$1"); M.dataType = "script"; l[W] = function (X) { V = X; I(); L(); l[W] = g; try { delete l[W] } catch (Y) { } if (H) { H.removeChild(T) } } } if (M.dataType == "script" && M.cache == null) { M.cache = false } if (M.cache === false && G == "GET") { var E = e(); var U = M.url.replace(/(\?|&)_=.*?(&|$)/, "$1_=" + E + "$2"); M.url = U + ((U == M.url) ? (M.url.match(/\?/) ? "&" : "?") + "_=" + E : "") } if (M.data && G == "GET") { M.url += (M.url.match(/\?/) ? "&" : "?") + M.data; M.data = null } if (M.global && !o.active++) { o.event.trigger("ajaxStart") } var Q = /^(\w+:)?\/\/([^\/?#]+)/.exec(M.url); if (M.dataType == "script" && G == "GET" && Q && (Q[1] && Q[1] != location.protocol || Q[2] != location.host)) { var H = document.getElementsByTagName("head")[0]; var T = document.createElement("script"); T.src = M.url; if (M.scriptCharset) { T.charset = M.scriptCharset } if (!W) { var O = false; T.onload = T.onreadystatechange = function () { if (!O && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) { O = true; I(); L(); T.onload = T.onreadystatechange = null; H.removeChild(T) } } } H.appendChild(T); return g } var K = false; var J = M.xhr(); if (M.username) { J.open(G, M.url, M.async, M.username, M.password) } else { J.open(G, M.url, M.async) } try { if (M.data) { J.setRequestHeader("Content-Type", M.contentType) } if (M.ifModified) { J.setRequestHeader("If-Modified-Since", o.lastModified[M.url] || "Thu, 01 Jan 1970 00:00:00 GMT") } J.setRequestHeader("X-Requested-With", "XMLHttpRequest"); J.setRequestHeader("Accept", M.dataType && M.accepts[M.dataType] ? M.accepts[M.dataType] + ", */*" : M.accepts._default) } catch (S) { } if (M.beforeSend && M.beforeSend(J, M) === false) { if (M.global && ! --o.active) { o.event.trigger("ajaxStop") } J.abort(); return false } if (M.global) { o.event.trigger("ajaxSend", [J, M]) } var N = function (X) { if (J.readyState == 0) { if (P) { clearInterval(P); P = null; if (M.global && ! --o.active) { o.event.trigger("ajaxStop") } } } else { if (!K && J && (J.readyState == 4 || X == "timeout")) { K = true; if (P) { clearInterval(P); P = null } R = X == "timeout" ? "timeout" : !o.httpSuccess(J) ? "error" : M.ifModified && o.httpNotModified(J, M.url) ? "notmodified" : "success"; if (R == "success") { try { V = o.httpData(J, M.dataType, M) } catch (Z) { R = "parsererror" } } if (R == "success") { var Y; try { Y = J.getResponseHeader("Last-Modified") } catch (Z) { } if (M.ifModified && Y) { o.lastModified[M.url] = Y } if (!W) { I() } } else { o.handleError(M, J, R) } L(); if (X) { J.abort() } if (M.async) { J = null } } } }; if (M.async) { var P = setInterval(N, 13); if (M.timeout > 0) { setTimeout(function () { if (J && !K) { N("timeout") } }, M.timeout) } } try { J.send(M.data) } catch (S) { o.handleError(M, J, null, S) } if (!M.async) { N() } function I() { if (M.success) { M.success(V, R) } if (M.global) { o.event.trigger("ajaxSuccess", [J, M]) } } function L() { if (M.complete) { M.complete(J, R) } if (M.global) { o.event.trigger("ajaxComplete", [J, M]) } if (M.global && ! --o.active) { o.event.trigger("ajaxStop") } } return J }, handleError: function (F, H, E, G) { if (F.error) { F.error(H, E, G) } if (F.global) { o.event.trigger("ajaxError", [H, F, G]) } }, active: 0, httpSuccess: function (F) { try { return !F.status && location.protocol == "file:" || (F.status >= 200 && F.status < 300) || F.status == 304 || F.status == 1223 } catch (E) { } return false }, httpNotModified: function (G, E) { try { var H = G.getResponseHeader("Last-Modified"); return G.status == 304 || H == o.lastModified[E] } catch (F) { } return false }, httpData: function (J, H, G) { var F = J.getResponseHeader("content-type"), E = H == "xml" || !H && F && F.indexOf("xml") >= 0, I = E ? J.responseXML : J.responseText; if (E && I.documentElement.tagName == "parsererror") { throw "parsererror" } if (G && G.dataFilter) { I = G.dataFilter(I, H) } if (typeof I === "string") { if (H == "script") { o.globalEval(I) } if (H == "json") { I = l["eval"]("(" + I + ")") } } return I }, param: function (E) { var G = []; function H(I, J) { G[G.length] = encodeURIComponent(I) + "=" + encodeURIComponent(J) } if (o.isArray(E) || E.jquery) { o.each(E, function () { H(this.name, this.value) }) } else { for (var F in E) { if (o.isArray(E[F])) { o.each(E[F], function () { H(F, this) }) } else { H(F, o.isFunction(E[F]) ? E[F]() : E[F]) } } } return G.join("&").replace(/%20/g, "+") } }); var m = {}, n, d = [["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"], ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"], ["opacity"]]; function t(F, E) { var G = {}; o.each(d.concat.apply([], d.slice(0, E)), function () { G[this] = F }); return G } o.fn.extend({ show: function (J, L) { if (J) { return this.animate(t("show", 3), J, L) } else { for (var H = 0, F = this.length; H < F; H++) { var E = o.data(this[H], "olddisplay"); this[H].style.display = E || ""; if (o.css(this[H], "display") === "none") { var G = this[H].tagName, K; if (m[G]) { K = m[G] } else { var I = o("<" + G + " />").appendTo("body"); K = I.css("display"); if (K === "none") { K = "block" } I.remove(); m[G] = K } o.data(this[H], "olddisplay", K) } } for (var H = 0, F = this.length; H < F; H++) { this[H].style.display = o.data(this[H], "olddisplay") || "" } return this } }, hide: function (H, I) { if (H) { return this.animate(t("hide", 3), H, I) } else { for (var G = 0, F = this.length; G < F; G++) { var E = o.data(this[G], "olddisplay"); if (!E && E !== "none") { o.data(this[G], "olddisplay", o.css(this[G], "display")) } } for (var G = 0, F = this.length; G < F; G++) { this[G].style.display = "none" } return this } }, _toggle: o.fn.toggle, toggle: function (G, F) { var E = typeof G === "boolean"; return o.isFunction(G) && o.isFunction(F) ? this._toggle.apply(this, arguments) : G == null || E ? this.each(function () { var H = E ? G : o(this).is(":hidden"); o(this)[H ? "show" : "hide"]() }) : this.animate(t("toggle", 3), G, F) }, fadeTo: function (E, G, F) { return this.animate({ opacity: G }, E, F) }, animate: function (I, F, H, G) { var E = o.speed(F, H, G); return this[E.queue === false ? "each" : "queue"](function () { var K = o.extend({}, E), M, L = this.nodeType == 1 && o(this).is(":hidden"), J = this; for (M in I) { if (I[M] == "hide" && L || I[M] == "show" && !L) { return K.complete.call(this) } if ((M == "height" || M == "width") && this.style) { K.display = o.css(this, "display"); K.overflow = this.style.overflow } } if (K.overflow != null) { this.style.overflow = "hidden" } K.curAnim = o.extend({}, I); o.each(I, function (O, S) { var R = new o.fx(J, K, O); if (/toggle|show|hide/.test(S)) { R[S == "toggle" ? L ? "show" : "hide" : S](I) } else { var Q = S.toString().match(/^([+-]=)?([\d+-.]+)(.*)$/), T = R.cur(true) || 0; if (Q) { var N = parseFloat(Q[2]), P = Q[3] || "px"; if (P != "px") { J.style[O] = (N || 1) + P; T = ((N || 1) / R.cur(true)) * T; J.style[O] = T + P } if (Q[1]) { N = ((Q[1] == "-=" ? -1 : 1) * N) + T } R.custom(T, N, P) } else { R.custom(T, S, "") } } }); return true }) }, stop: function (F, E) { var G = o.timers; if (F) { this.queue([]) } this.each(function () { for (var H = G.length - 1; H >= 0; H--) { if (G[H].elem == this) { if (E) { G[H](true) } G.splice(H, 1) } } }); if (!E) { this.dequeue() } return this } }); o.each({ slideDown: t("show", 1), slideUp: t("hide", 1), slideToggle: t("toggle", 1), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" } }, function (E, F) { o.fn[E] = function (G, H) { return this.animate(F, G, H) } }); o.extend({ speed: function (G, H, F) { var E = typeof G === "object" ? G : { complete: F || !F && H || o.isFunction(G) && G, duration: G, easing: F && H || H && !o.isFunction(H) && H }; E.duration = o.fx.off ? 0 : typeof E.duration === "number" ? E.duration : o.fx.speeds[E.duration] || o.fx.speeds._default; E.old = E.complete; E.complete = function () { if (E.queue !== false) { o(this).dequeue() } if (o.isFunction(E.old)) { E.old.call(this) } }; return E }, easing: { linear: function (G, H, E, F) { return E + F * G }, swing: function (G, H, E, F) { return ((-Math.cos(G * Math.PI) / 2) + 0.5) * F + E } }, timers: [], fx: function (F, E, G) { this.options = E; this.elem = F; this.prop = G; if (!E.orig) { E.orig = {} } } }); o.fx.prototype = { update: function () { if (this.options.step) { this.options.step.call(this.elem, this.now, this) } (o.fx.step[this.prop] || o.fx.step._default)(this); if ((this.prop == "height" || this.prop == "width") && this.elem.style) { this.elem.style.display = "block" } }, cur: function (F) { if (this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null)) { return this.elem[this.prop] } var E = parseFloat(o.css(this.elem, this.prop, F)); return E && E > -10000 ? E : parseFloat(o.curCSS(this.elem, this.prop)) || 0 }, custom: function (I, H, G) { this.startTime = e(); this.start = I; this.end = H; this.unit = G || this.unit || "px"; this.now = this.start; this.pos = this.state = 0; var E = this; function F(J) { return E.step(J) } F.elem = this.elem; if (F() && o.timers.push(F) && !n) { n = setInterval(function () { var K = o.timers; for (var J = 0; J < K.length; J++) { if (!K[J]()) { K.splice(J--, 1) } } if (!K.length) { clearInterval(n); n = g } }, 13) } }, show: function () { this.options.orig[this.prop] = o.attr(this.elem.style, this.prop); this.options.show = true; this.custom(this.prop == "width" || this.prop == "height" ? 1 : 0, this.cur()); o(this.elem).show() }, hide: function () { this.options.orig[this.prop] = o.attr(this.elem.style, this.prop); this.options.hide = true; this.custom(this.cur(), 0) }, step: function (H) { var G = e(); if (H || G >= this.options.duration + this.startTime) { this.now = this.end; this.pos = this.state = 1; this.update(); this.options.curAnim[this.prop] = true; var E = true; for (var F in this.options.curAnim) { if (this.options.curAnim[F] !== true) { E = false } } if (E) { if (this.options.display != null) { this.elem.style.overflow = this.options.overflow; this.elem.style.display = this.options.display; if (o.css(this.elem, "display") == "none") { this.elem.style.display = "block" } } if (this.options.hide) { o(this.elem).hide() } if (this.options.hide || this.options.show) { for (var I in this.options.curAnim) { o.attr(this.elem.style, I, this.options.orig[I]) } } this.options.complete.call(this.elem) } return false } else { var J = G - this.startTime; this.state = J / this.options.duration; this.pos = o.easing[this.options.easing || (o.easing.swing ? "swing" : "linear")](this.state, J, 0, 1, this.options.duration); this.now = this.start + ((this.end - this.start) * this.pos); this.update() } return true } }; o.extend(o.fx, { speeds: { slow: 600, fast: 200, _default: 400 }, step: { opacity: function (E) { o.attr(E.elem.style, "opacity", E.now) }, _default: function (E) { if (E.elem.style && E.elem.style[E.prop] != null) { E.elem.style[E.prop] = E.now + E.unit } else { E.elem[E.prop] = E.now } } } }); if (document.documentElement.getBoundingClientRect) { o.fn.offset = function () { if (!this[0]) { return { top: 0, left: 0 } } if (this[0] === this[0].ownerDocument.body) { return o.offset.bodyOffset(this[0]) } var G = this[0].getBoundingClientRect(), J = this[0].ownerDocument, F = J.body, E = J.documentElement, L = E.clientTop || F.clientTop || 0, K = E.clientLeft || F.clientLeft || 0, I = G.top + (self.pageYOffset || o.boxModel && E.scrollTop || F.scrollTop) - L, H = G.left + (self.pageXOffset || o.boxModel && E.scrollLeft || F.scrollLeft) - K; return { top: I, left: H } } } else { o.fn.offset = function () { if (!this[0]) { return { top: 0, left: 0 } } if (this[0] === this[0].ownerDocument.body) { return o.offset.bodyOffset(this[0]) } o.offset.initialized || o.offset.initialize(); var J = this[0], G = J.offsetParent, F = J, O = J.ownerDocument, M, H = O.documentElement, K = O.body, L = O.defaultView, E = L.getComputedStyle(J, null), N = J.offsetTop, I = J.offsetLeft; while ((J = J.parentNode) && J !== K && J !== H) { M = L.getComputedStyle(J, null); N -= J.scrollTop, I -= J.scrollLeft; if (J === G) { N += J.offsetTop, I += J.offsetLeft; if (o.offset.doesNotAddBorder && !(o.offset.doesAddBorderForTableAndCells && /^t(able|d|h)$/i.test(J.tagName))) { N += parseInt(M.borderTopWidth, 10) || 0, I += parseInt(M.borderLeftWidth, 10) || 0 } F = G, G = J.offsetParent } if (o.offset.subtractsBorderForOverflowNotVisible && M.overflow !== "visible") { N += parseInt(M.borderTopWidth, 10) || 0, I += parseInt(M.borderLeftWidth, 10) || 0 } E = M } if (E.position === "relative" || E.position === "static") { N += K.offsetTop, I += K.offsetLeft } if (E.position === "fixed") { N += Math.max(H.scrollTop, K.scrollTop), I += Math.max(H.scrollLeft, K.scrollLeft) } return { top: N, left: I } } } o.offset = { initialize: function () { if (this.initialized) { return } var L = document.body, F = document.createElement("div"), H, G, N, I, M, E, J = L.style.marginTop, K = '<div style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;"><div></div></div><table style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;" cellpadding="0" cellspacing="0"><tr><td></td></tr></table>'; M = { position: "absolute", top: 0, left: 0, margin: 0, border: 0, width: "1px", height: "1px", visibility: "hidden" }; for (E in M) { F.style[E] = M[E] } F.innerHTML = K; L.insertBefore(F, L.firstChild); H = F.firstChild, G = H.firstChild, I = H.nextSibling.firstChild.firstChild; this.doesNotAddBorder = (G.offsetTop !== 5); this.doesAddBorderForTableAndCells = (I.offsetTop === 5); H.style.overflow = "hidden", H.style.position = "relative"; this.subtractsBorderForOverflowNotVisible = (G.offsetTop === -5); L.style.marginTop = "1px"; this.doesNotIncludeMarginInBodyOffset = (L.offsetTop === 0); L.style.marginTop = J; L.removeChild(F); this.initialized = true }, bodyOffset: function (E) { o.offset.initialized || o.offset.initialize(); var G = E.offsetTop, F = E.offsetLeft; if (o.offset.doesNotIncludeMarginInBodyOffset) { G += parseInt(o.curCSS(E, "marginTop", true), 10) || 0, F += parseInt(o.curCSS(E, "marginLeft", true), 10) || 0 } return { top: G, left: F } } }; o.fn.extend({ position: function () { var I = 0, H = 0, F; if (this[0]) { var G = this.offsetParent(), J = this.offset(), E = /^body|html$/i.test(G[0].tagName) ? { top: 0, left: 0 } : G.offset(); J.top -= j(this, "marginTop"); J.left -= j(this, "marginLeft"); E.top += j(G, "borderTopWidth"); E.left += j(G, "borderLeftWidth"); F = { top: J.top - E.top, left: J.left - E.left } } return F }, offsetParent: function () { var E = this[0].offsetParent || document.body; while (E && (!/^body|html$/i.test(E.tagName) && o.css(E, "position") == "static")) { E = E.offsetParent } return o(E) } }); o.each(["Left", "Top"], function (F, E) { var G = "scroll" + E; o.fn[G] = function (H) { if (!this[0]) { return null } return H !== g ? this.each(function () { this == l || this == document ? l.scrollTo(!F ? H : o(l).scrollLeft(), F ? H : o(l).scrollTop()) : this[G] = H }) : this[0] == l || this[0] == document ? self[F ? "pageYOffset" : "pageXOffset"] || o.boxModel && document.documentElement[G] || document.body[G] : this[0][G] } }); o.each(["Height", "Width"], function (I, G) { var E = I ? "Left" : "Top", H = I ? "Right" : "Bottom", F = G.toLowerCase(); o.fn["inner" + G] = function () { return this[0] ? o.css(this[0], F, false, "padding") : null }; o.fn["outer" + G] = function (K) { return this[0] ? o.css(this[0], F, false, K ? "margin" : "border") : null }; var J = G.toLowerCase(); o.fn[J] = function (K) { return this[0] == l ? document.compatMode == "CSS1Compat" && document.documentElement["client" + G] || document.body["client" + G] : this[0] == document ? Math.max(document.documentElement["client" + G], document.body["scroll" + G], document.documentElement["scroll" + G], document.body["offset" + G], document.documentElement["offset" + G]) : K === g ? (this.length ? o.css(this[0], J) : null) : this.css(J, typeof K === "string" ? K : K + "px") } })
})();
/*ui 1.7.1*/jQuery.ui || (function (c) { var i = c.fn.remove, d = c.browser.mozilla && (parseFloat(c.browser.version) < 1.9); c.ui = { version: "1.7.1", plugin: { add: function (k, l, n) { var m = c.ui[k].prototype; for (var j in n) { m.plugins[j] = m.plugins[j] || []; m.plugins[j].push([l, n[j]]) } }, call: function (j, l, k) { var n = j.plugins[l]; if (!n || !j.element[0].parentNode) { return } for (var m = 0; m < n.length; m++) { if (j.options[n[m][0]]) { n[m][1].apply(j.element, k) } } } }, contains: function (k, j) { return document.compareDocumentPosition ? k.compareDocumentPosition(j) & 16 : k !== j && k.contains(j) }, hasScroll: function (m, k) { if (c(m).css("overflow") == "hidden") { return false } var j = (k && k == "left") ? "scrollLeft" : "scrollTop", l = false; if (m[j] > 0) { return true } m[j] = 1; l = (m[j] > 0); m[j] = 0; return l }, isOverAxis: function (k, j, l) { return (k > j) && (k < (j + l)) }, isOver: function (o, k, n, m, j, l) { return c.ui.isOverAxis(o, n, j) && c.ui.isOverAxis(k, m, l) }, keyCode: { BACKSPACE: 8, CAPS_LOCK: 20, COMMA: 188, CONTROL: 17, DELETE: 46, DOWN: 40, END: 35, ENTER: 13, ESCAPE: 27, HOME: 36, INSERT: 45, LEFT: 37, NUMPAD_ADD: 107, NUMPAD_DECIMAL: 110, NUMPAD_DIVIDE: 111, NUMPAD_ENTER: 108, NUMPAD_MULTIPLY: 106, NUMPAD_SUBTRACT: 109, PAGE_DOWN: 34, PAGE_UP: 33, PERIOD: 190, RIGHT: 39, SHIFT: 16, SPACE: 32, TAB: 9, UP: 38 } }; if (d) { var f = c.attr, e = c.fn.removeAttr, h = "http://www.w3.org/2005/07/aaa", a = /^aria-/, b = /^wairole:/; c.attr = function (k, j, l) { var m = l !== undefined; return (j == "role" ? (m ? f.call(this, k, j, "wairole:" + l) : (f.apply(this, arguments) || "").replace(b, "")) : (a.test(j) ? (m ? k.setAttributeNS(h, j.replace(a, "aaa:"), l) : f.call(this, k, j.replace(a, "aaa:"))) : f.apply(this, arguments))) }; c.fn.removeAttr = function (j) { return (a.test(j) ? this.each(function () { this.removeAttributeNS(h, j.replace(a, "")) }) : e.call(this, j)) } } c.fn.extend({ remove: function () { c("*", this).add(this).each(function () { c(this).triggerHandler("remove") }); return i.apply(this, arguments) }, enableSelection: function () { return this.attr("unselectable", "off").css("MozUserSelect", "").unbind("selectstart.ui") }, disableSelection: function () { return this.attr("unselectable", "on").css("MozUserSelect", "none").bind("selectstart.ui", function () { return false }) }, scrollParent: function () { var j; if ((c.browser.msie && (/(static|relative)/).test(this.css("position"))) || (/absolute/).test(this.css("position"))) { j = this.parents().filter(function () { return (/(relative|absolute|fixed)/).test(c.curCSS(this, "position", 1)) && (/(auto|scroll)/).test(c.curCSS(this, "overflow", 1) + c.curCSS(this, "overflow-y", 1) + c.curCSS(this, "overflow-x", 1)) }).eq(0) } else { j = this.parents().filter(function () { return (/(auto|scroll)/).test(c.curCSS(this, "overflow", 1) + c.curCSS(this, "overflow-y", 1) + c.curCSS(this, "overflow-x", 1)) }).eq(0) } return (/fixed/).test(this.css("position")) || !j.length ? c(document) : j } }); c.extend(c.expr[":"], { data: function (l, k, j) { return !!c.data(l, j[3]) }, focusable: function (k) { var l = k.nodeName.toLowerCase(), j = c.attr(k, "tabindex"); return (/input|select|textarea|button|object/.test(l) ? !k.disabled : "a" == l || "area" == l ? k.href || !isNaN(j) : !isNaN(j)) && !c(k)["area" == l ? "parents" : "closest"](":hidden").length }, tabbable: function (k) { var j = c.attr(k, "tabindex"); return (isNaN(j) || j >= 0) && c(k).is(":focusable") } }); function g(m, n, o, l) { function k(q) { var p = c[m][n][q] || []; return (typeof p == "string" ? p.split(/,?\s+/) : p) } var j = k("getter"); if (l.length == 1 && typeof l[0] == "string") { j = j.concat(k("getterSetter")) } return (c.inArray(o, j) != -1) } c.widget = function (k, j) { var l = k.split(".")[0]; k = k.split(".")[1]; c.fn[k] = function (p) { var n = (typeof p == "string"), o = Array.prototype.slice.call(arguments, 1); if (n && p.substring(0, 1) == "_") { return this } if (n && g(l, k, p, o)) { var m = c.data(this[0], k); return (m ? m[p].apply(m, o) : undefined) } return this.each(function () { var q = c.data(this, k); (!q && !n && c.data(this, k, new c[l][k](this, p))._init()); (q && n && c.isFunction(q[p]) && q[p].apply(q, o)) }) }; c[l] = c[l] || {}; c[l][k] = function (o, n) { var m = this; this.namespace = l; this.widgetName = k; this.widgetEventPrefix = c[l][k].eventPrefix || k; this.widgetBaseClass = l + "-" + k; this.options = c.extend({}, c.widget.defaults, c[l][k].defaults, c.metadata && c.metadata.get(o)[k], n); this.element = c(o).bind("setData." + k, function (q, p, r) { if (q.target == o) { return m._setData(p, r) } }).bind("getData." + k, function (q, p) { if (q.target == o) { return m._getData(p) } }).bind("remove", function () { return m.destroy() }) }; c[l][k].prototype = c.extend({}, c.widget.prototype, j); c[l][k].getterSetter = "option" }; c.widget.prototype = { _init: function () { }, destroy: function () { this.element.removeData(this.widgetName).removeClass(this.widgetBaseClass + "-disabled " + this.namespace + "-state-disabled").removeAttr("aria-disabled") }, option: function (l, m) { var k = l, j = this; if (typeof l == "string") { if (m === undefined) { return this._getData(l) } k = {}; k[l] = m } c.each(k, function (n, o) { j._setData(n, o) }) }, _getData: function (j) { return this.options[j] }, _setData: function (j, k) { this.options[j] = k; if (j == "disabled") { this.element[k ? "addClass" : "removeClass"](this.widgetBaseClass + "-disabled " + this.namespace + "-state-disabled").attr("aria-disabled", k) } }, enable: function () { this._setData("disabled", false) }, disable: function () { this._setData("disabled", true) }, _trigger: function (l, m, n) { var p = this.options[l], j = (l == this.widgetEventPrefix ? l : this.widgetEventPrefix + l); m = c.Event(m); m.type = j; if (m.originalEvent) { for (var k = c.event.props.length, o; k;) { o = c.event.props[--k]; m[o] = m.originalEvent[o] } } this.element.trigger(m, n); return !(c.isFunction(p) && p.call(this.element[0], m, n) === false || m.isDefaultPrevented()) } }; c.widget.defaults = { disabled: false }; c.ui.mouse = { _mouseInit: function () { var j = this; this.element.bind("mousedown." + this.widgetName, function (k) { return j._mouseDown(k) }).bind("click." + this.widgetName, function (k) { if (j._preventClickEvent) { j._preventClickEvent = false; k.stopImmediatePropagation(); return false } }); if (c.browser.msie) { this._mouseUnselectable = this.element.attr("unselectable"); this.element.attr("unselectable", "on") } this.started = false }, _mouseDestroy: function () { this.element.unbind("." + this.widgetName); (c.browser.msie && this.element.attr("unselectable", this._mouseUnselectable)) }, _mouseDown: function (l) { l.originalEvent = l.originalEvent || {}; if (l.originalEvent.mouseHandled) { return } (this._mouseStarted && this._mouseUp(l)); this._mouseDownEvent = l; var k = this, m = (l.which == 1), j = (typeof this.options.cancel == "string" ? c(l.target).parents().add(l.target).filter(this.options.cancel).length : false); if (!m || j || !this._mouseCapture(l)) { return true } this.mouseDelayMet = !this.options.delay; if (!this.mouseDelayMet) { this._mouseDelayTimer = setTimeout(function () { k.mouseDelayMet = true }, this.options.delay) } if (this._mouseDistanceMet(l) && this._mouseDelayMet(l)) { this._mouseStarted = (this._mouseStart(l) !== false); if (!this._mouseStarted) { l.preventDefault(); return true } } this._mouseMoveDelegate = function (n) { return k._mouseMove(n) }; this._mouseUpDelegate = function (n) { return k._mouseUp(n) }; c(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate); (c.browser.safari || l.preventDefault()); l.originalEvent.mouseHandled = true; return true }, _mouseMove: function (j) { if (c.browser.msie && !j.button) { return this._mouseUp(j) } if (this._mouseStarted) { this._mouseDrag(j); return j.preventDefault() } if (this._mouseDistanceMet(j) && this._mouseDelayMet(j)) { this._mouseStarted = (this._mouseStart(this._mouseDownEvent, j) !== false); (this._mouseStarted ? this._mouseDrag(j) : this._mouseUp(j)) } return !this._mouseStarted }, _mouseUp: function (j) { c(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate); if (this._mouseStarted) { this._mouseStarted = false; this._preventClickEvent = (j.target == this._mouseDownEvent.target); this._mouseStop(j) } return false }, _mouseDistanceMet: function (j) { return (Math.max(Math.abs(this._mouseDownEvent.pageX - j.pageX), Math.abs(this._mouseDownEvent.pageY - j.pageY)) >= this.options.distance) }, _mouseDelayMet: function (j) { return this.mouseDelayMet }, _mouseStart: function (j) { }, _mouseDrag: function (j) { }, _mouseStop: function (j) { }, _mouseCapture: function (j) { return true } }; c.ui.mouse.defaults = { cancel: null, distance: 1, delay: 0 } })(jQuery); (function ($) { $.extend($.ui, { datepicker: { version: "1.7.1" } }); var PROP_NAME = "datepicker"; function Datepicker() { this.debug = false; this._curInst = null; this._keyEvent = false; this._disabledInputs = []; this._datepickerShowing = false; this._inDialog = false; this._mainDivId = "ui-datepicker-div"; this._inlineClass = "ui-datepicker-inline"; this._appendClass = "ui-datepicker-append"; this._triggerClass = "ui-datepicker-trigger"; this._dialogClass = "ui-datepicker-dialog"; this._disableClass = "ui-datepicker-disabled"; this._unselectableClass = "ui-datepicker-unselectable"; this._currentClass = "ui-datepicker-current-day"; this._dayOverClass = "ui-datepicker-days-cell-over"; this.regional = []; this.regional[""] = { closeText: "Done", prevText: "Prev", nextText: "Next", currentText: "Today", monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], dateFormat: "mm/dd/yy", firstDay: 0, isRTL: false }; this._defaults = { showOn: "focus", showAnim: "show", showOptions: {}, defaultDate: null, appendText: "", buttonText: "...", buttonImage: "", buttonImageOnly: false, hideIfNoPrevNext: false, navigationAsDateFormat: false, gotoCurrent: false, changeMonth: false, changeYear: false, showMonthAfterYear: false, yearRange: "-10:+10", showOtherMonths: false, calculateWeek: this.iso8601Week, shortYearCutoff: "+10", minDate: null, maxDate: null, duration: "normal", beforeShowDay: null, beforeShow: null, onSelect: null, onChangeMonthYear: null, onClose: null, numberOfMonths: 1, showCurrentAtPos: 0, stepMonths: 1, stepBigMonths: 12, altField: "", altFormat: "", constrainInput: true, showButtonPanel: false }; $.extend(this._defaults, this.regional[""]); this.dpDiv = $('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ui-helper-hidden-accessible"></div>') } $.extend(Datepicker.prototype, { markerClassName: "hasDatepicker", log: function () { if (this.debug) { console.log.apply("", arguments) } }, setDefaults: function (settings) { extendRemove(this._defaults, settings || {}); return this }, _attachDatepicker: function (target, settings) { var inlineSettings = null; for (var attrName in this._defaults) { var attrValue = target.getAttribute("date:" + attrName); if (attrValue) { inlineSettings = inlineSettings || {}; try { inlineSettings[attrName] = eval(attrValue) } catch (err) { inlineSettings[attrName] = attrValue } } } var nodeName = target.nodeName.toLowerCase(); var inline = (nodeName == "div" || nodeName == "span"); if (!target.id) { target.id = "dp" + (++this.uuid) } var inst = this._newInst($(target), inline); inst.settings = $.extend({}, settings || {}, inlineSettings || {}); if (nodeName == "input") { this._connectDatepicker(target, inst) } else { if (inline) { this._inlineDatepicker(target, inst) } } }, _newInst: function (target, inline) { var id = target[0].id.replace(/([:\[\]\.])/g, "\\\\$1"); return { id: id, input: target, selectedDay: 0, selectedMonth: 0, selectedYear: 0, drawMonth: 0, drawYear: 0, inline: inline, dpDiv: (!inline ? this.dpDiv : $('<div class="' + this._inlineClass + ' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>')) } }, _connectDatepicker: function (target, inst) { var input = $(target); inst.trigger = $([]); if (input.hasClass(this.markerClassName)) { return } var appendText = this._get(inst, "appendText"); var isRTL = this._get(inst, "isRTL"); if (appendText) { input[isRTL ? "before" : "after"]('<span class="' + this._appendClass + '">' + appendText + "</span>") } var showOn = this._get(inst, "showOn"); if (showOn == "focus" || showOn == "both") { input.focus(this._showDatepicker) } if (showOn == "button" || showOn == "both") { var buttonText = this._get(inst, "buttonText"); var buttonImage = this._get(inst, "buttonImage"); inst.trigger = $(this._get(inst, "buttonImageOnly") ? $("<img/>").addClass(this._triggerClass).attr({ src: buttonImage, alt: buttonText, title: buttonText }) : $('<button type="button"></button>').addClass(this._triggerClass).html(buttonImage == "" ? buttonText : $("<img/>").attr({ src: buttonImage, alt: buttonText, title: buttonText }))); input[isRTL ? "before" : "after"](inst.trigger); inst.trigger.click(function () { if ($.datepicker._datepickerShowing && $.datepicker._lastInput == target) { $.datepicker._hideDatepicker() } else { $.datepicker._showDatepicker(target) } return false }) } input.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).bind("setData.datepicker", function (event, key, value) { inst.settings[key] = value }).bind("getData.datepicker", function (event, key) { return this._get(inst, key) }); $.data(target, PROP_NAME, inst) }, _inlineDatepicker: function (target, inst) { var divSpan = $(target); if (divSpan.hasClass(this.markerClassName)) { return } divSpan.addClass(this.markerClassName).append(inst.dpDiv).bind("setData.datepicker", function (event, key, value) { inst.settings[key] = value }).bind("getData.datepicker", function (event, key) { return this._get(inst, key) }); $.data(target, PROP_NAME, inst); this._setDate(inst, this._getDefaultDate(inst)); this._updateDatepicker(inst); this._updateAlternate(inst) }, _dialogDatepicker: function (input, dateText, onSelect, settings, pos) { var inst = this._dialogInst; if (!inst) { var id = "dp" + (++this.uuid); this._dialogInput = $('<input type="text" id="' + id + '" size="1" style="position: absolute; top: -100px;"/>'); this._dialogInput.keydown(this._doKeyDown); $("body").append(this._dialogInput); inst = this._dialogInst = this._newInst(this._dialogInput, false); inst.settings = {}; $.data(this._dialogInput[0], PROP_NAME, inst) } extendRemove(inst.settings, settings || {}); this._dialogInput.val(dateText); this._pos = (pos ? (pos.length ? pos : [pos.pageX, pos.pageY]) : null); if (!this._pos) { var browserWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth; var browserHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight; var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft; var scrollY = document.documentElement.scrollTop || document.body.scrollTop; this._pos = [(browserWidth / 2) - 100 + scrollX, (browserHeight / 2) - 150 + scrollY] } this._dialogInput.css("left", this._pos[0] + "px").css("top", this._pos[1] + "px"); inst.settings.onSelect = onSelect; this._inDialog = true; this.dpDiv.addClass(this._dialogClass); this._showDatepicker(this._dialogInput[0]); if ($.blockUI) { $.blockUI(this.dpDiv) } $.data(this._dialogInput[0], PROP_NAME, inst); return this }, _destroyDatepicker: function (target) { var $target = $(target); var inst = $.data(target, PROP_NAME); if (!$target.hasClass(this.markerClassName)) { return } var nodeName = target.nodeName.toLowerCase(); $.removeData(target, PROP_NAME); if (nodeName == "input") { inst.trigger.remove(); $target.siblings("." + this._appendClass).remove().end().removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress) } else { if (nodeName == "div" || nodeName == "span") { $target.removeClass(this.markerClassName).empty() } } }, _enableDatepicker: function (target) { var $target = $(target); var inst = $.data(target, PROP_NAME); if (!$target.hasClass(this.markerClassName)) { return } var nodeName = target.nodeName.toLowerCase(); if (nodeName == "input") { target.disabled = false; inst.trigger.filter("button").each(function () { this.disabled = false }).end().filter("img").css({ opacity: "1.0", cursor: "" }) } else { if (nodeName == "div" || nodeName == "span") { var inline = $target.children("." + this._inlineClass); inline.children().removeClass("ui-state-disabled") } } this._disabledInputs = $.map(this._disabledInputs, function (value) { return (value == target ? null : value) }) }, _disableDatepicker: function (target) { var $target = $(target); var inst = $.data(target, PROP_NAME); if (!$target.hasClass(this.markerClassName)) { return } var nodeName = target.nodeName.toLowerCase(); if (nodeName == "input") { target.disabled = true; inst.trigger.filter("button").each(function () { this.disabled = true }).end().filter("img").css({ opacity: "0.5", cursor: "default" }) } else { if (nodeName == "div" || nodeName == "span") { var inline = $target.children("." + this._inlineClass); inline.children().addClass("ui-state-disabled") } } this._disabledInputs = $.map(this._disabledInputs, function (value) { return (value == target ? null : value) }); this._disabledInputs[this._disabledInputs.length] = target }, _isDisabledDatepicker: function (target) { if (!target) { return false } for (var i = 0; i < this._disabledInputs.length; i++) { if (this._disabledInputs[i] == target) { return true } } return false }, _getInst: function (target) { try { return $.data(target, PROP_NAME) } catch (err) { throw "Missing instance data for this datepicker" } }, _optionDatepicker: function (target, name, value) { var settings = name || {}; if (typeof name == "string") { settings = {}; settings[name] = value } var inst = this._getInst(target); if (inst) { if (this._curInst == inst) { this._hideDatepicker(null) } extendRemove(inst.settings, settings); var date = new Date(); extendRemove(inst, { rangeStart: null, endDay: null, endMonth: null, endYear: null, selectedDay: date.getDate(), selectedMonth: date.getMonth(), selectedYear: date.getFullYear(), currentDay: date.getDate(), currentMonth: date.getMonth(), currentYear: date.getFullYear(), drawMonth: date.getMonth(), drawYear: date.getFullYear() }); this._updateDatepicker(inst) } }, _changeDatepicker: function (target, name, value) { this._optionDatepicker(target, name, value) }, _refreshDatepicker: function (target) { var inst = this._getInst(target); if (inst) { this._updateDatepicker(inst) } }, _setDateDatepicker: function (target, date, endDate) { var inst = this._getInst(target); if (inst) { this._setDate(inst, date, endDate); this._updateDatepicker(inst); this._updateAlternate(inst) } }, _getDateDatepicker: function (target) { var inst = this._getInst(target); if (inst && !inst.inline) { this._setDateFromField(inst) } return (inst ? this._getDate(inst) : null) }, _doKeyDown: function (event) { var inst = $.datepicker._getInst(event.target); var handled = true; var isRTL = inst.dpDiv.is(".ui-datepicker-rtl"); inst._keyEvent = true; if ($.datepicker._datepickerShowing) { switch (event.keyCode) { case 9: $.datepicker._hideDatepicker(null, ""); break; case 13: var sel = $("td." + $.datepicker._dayOverClass + ", td." + $.datepicker._currentClass, inst.dpDiv); if (sel[0]) { $.datepicker._selectDay(event.target, inst.selectedMonth, inst.selectedYear, sel[0]) } else { $.datepicker._hideDatepicker(null, $.datepicker._get(inst, "duration")) } return false; break; case 27: $.datepicker._hideDatepicker(null, $.datepicker._get(inst, "duration")); break; case 33: $.datepicker._adjustDate(event.target, (event.ctrlKey ? -$.datepicker._get(inst, "stepBigMonths") : -$.datepicker._get(inst, "stepMonths")), "M"); break; case 34: $.datepicker._adjustDate(event.target, (event.ctrlKey ? +$.datepicker._get(inst, "stepBigMonths") : +$.datepicker._get(inst, "stepMonths")), "M"); break; case 35: if (event.ctrlKey || event.metaKey) { $.datepicker._clearDate(event.target) } handled = event.ctrlKey || event.metaKey; break; case 36: if (event.ctrlKey || event.metaKey) { $.datepicker._gotoToday(event.target) } handled = event.ctrlKey || event.metaKey; break; case 37: if (event.ctrlKey || event.metaKey) { $.datepicker._adjustDate(event.target, (isRTL ? +1 : -1), "D") } handled = event.ctrlKey || event.metaKey; if (event.originalEvent.altKey) { $.datepicker._adjustDate(event.target, (event.ctrlKey ? -$.datepicker._get(inst, "stepBigMonths") : -$.datepicker._get(inst, "stepMonths")), "M") } break; case 38: if (event.ctrlKey || event.metaKey) { $.datepicker._adjustDate(event.target, -7, "D") } handled = event.ctrlKey || event.metaKey; break; case 39: if (event.ctrlKey || event.metaKey) { $.datepicker._adjustDate(event.target, (isRTL ? -1 : +1), "D") } handled = event.ctrlKey || event.metaKey; if (event.originalEvent.altKey) { $.datepicker._adjustDate(event.target, (event.ctrlKey ? +$.datepicker._get(inst, "stepBigMonths") : +$.datepicker._get(inst, "stepMonths")), "M") } break; case 40: if (event.ctrlKey || event.metaKey) { $.datepicker._adjustDate(event.target, +7, "D") } handled = event.ctrlKey || event.metaKey; break; default: handled = false } } else { if (event.keyCode == 36 && event.ctrlKey) { $.datepicker._showDatepicker(this) } else { handled = false } } if (handled) { event.preventDefault(); event.stopPropagation() } }, _doKeyPress: function (event) { var inst = $.datepicker._getInst(event.target); if ($.datepicker._get(inst, "constrainInput")) { var chars = $.datepicker._possibleChars($.datepicker._get(inst, "dateFormat")); var chr = String.fromCharCode(event.charCode == undefined ? event.keyCode : event.charCode); return event.ctrlKey || (chr < " " || !chars || chars.indexOf(chr) > -1) } }, _showDatepicker: function (input) { input = input.target || input; if (input.nodeName.toLowerCase() != "input") { input = $("input", input.parentNode)[0] } if ($.datepicker._isDisabledDatepicker(input) || $.datepicker._lastInput == input) { return } var inst = $.datepicker._getInst(input); var beforeShow = $.datepicker._get(inst, "beforeShow"); extendRemove(inst.settings, (beforeShow ? beforeShow.apply(input, [input, inst]) : {})); $.datepicker._hideDatepicker(null, ""); $.datepicker._lastInput = input; $.datepicker._setDateFromField(inst); if ($.datepicker._inDialog) { input.value = "" } if (!$.datepicker._pos) { $.datepicker._pos = $.datepicker._findPos(input); $.datepicker._pos[1] += input.offsetHeight } var isFixed = false; $(input).parents().each(function () { isFixed |= $(this).css("position") == "fixed"; return !isFixed }); if (isFixed && $.browser.opera) { $.datepicker._pos[0] -= document.documentElement.scrollLeft; $.datepicker._pos[1] -= document.documentElement.scrollTop } var offset = { left: $.datepicker._pos[0], top: $.datepicker._pos[1] }; $.datepicker._pos = null; inst.rangeStart = null; inst.dpDiv.css({ position: "absolute", display: "block", top: "-1000px" }); $.datepicker._updateDatepicker(inst); offset = $.datepicker._checkOffset(inst, offset, isFixed); inst.dpDiv.css({ position: ($.datepicker._inDialog && $.blockUI ? "static" : (isFixed ? "fixed" : "absolute")), display: "none", left: offset.left + "px", top: offset.top + "px" }); if (!inst.inline) { var showAnim = $.datepicker._get(inst, "showAnim") || "show"; var duration = $.datepicker._get(inst, "duration"); var postProcess = function () { $.datepicker._datepickerShowing = true; if ($.browser.msie && parseInt($.browser.version, 10) < 7) { $("iframe.ui-datepicker-cover").css({ width: inst.dpDiv.width() + 4, height: inst.dpDiv.height() + 4 }) } }; if ($.effects && $.effects[showAnim]) { inst.dpDiv.show(showAnim, $.datepicker._get(inst, "showOptions"), duration, postProcess) } else { inst.dpDiv[showAnim](duration, postProcess) } if (duration == "") { postProcess() } if (inst.input[0].type != "hidden") { inst.input[0].focus() } $.datepicker._curInst = inst } }, _updateDatepicker: function (inst) { var dims = { width: inst.dpDiv.width() + 4, height: inst.dpDiv.height() + 4 }; var self = this; inst.dpDiv.empty().append(this._generateHTML(inst)).find("iframe.ui-datepicker-cover").css({ width: dims.width, height: dims.height }).end().find("button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a").bind("mouseout", function () { $(this).removeClass("ui-state-hover"); if (this.className.indexOf("ui-datepicker-prev") != -1) { $(this).removeClass("ui-datepicker-prev-hover") } if (this.className.indexOf("ui-datepicker-next") != -1) { $(this).removeClass("ui-datepicker-next-hover") } }).bind("mouseover", function () { if (!self._isDisabledDatepicker(inst.inline ? inst.dpDiv.parent()[0] : inst.input[0])) { $(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"); $(this).addClass("ui-state-hover"); if (this.className.indexOf("ui-datepicker-prev") != -1) { $(this).addClass("ui-datepicker-prev-hover") } if (this.className.indexOf("ui-datepicker-next") != -1) { $(this).addClass("ui-datepicker-next-hover") } } }).end().find("." + this._dayOverClass + " a").trigger("mouseover").end(); var numMonths = this._getNumberOfMonths(inst); var cols = numMonths[1]; var width = 17; if (cols > 1) { inst.dpDiv.addClass("ui-datepicker-multi-" + cols).css("width", (width * cols) + "em") } else { inst.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("") } inst.dpDiv[(numMonths[0] != 1 || numMonths[1] != 1 ? "add" : "remove") + "Class"]("ui-datepicker-multi"); inst.dpDiv[(this._get(inst, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"); if (inst.input && inst.input[0].type != "hidden" && inst == $.datepicker._curInst) { $(inst.input[0]).focus() } }, _checkOffset: function (inst, offset, isFixed) { var dpWidth = inst.dpDiv.outerWidth(); var dpHeight = inst.dpDiv.outerHeight(); var inputWidth = inst.input ? inst.input.outerWidth() : 0; var inputHeight = inst.input ? inst.input.outerHeight() : 0; var viewWidth = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) + $(document).scrollLeft(); var viewHeight = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) + $(document).scrollTop(); offset.left -= (this._get(inst, "isRTL") ? (dpWidth - inputWidth) : 0); offset.left -= (isFixed && offset.left == inst.input.offset().left) ? $(document).scrollLeft() : 0; offset.top -= (isFixed && offset.top == (inst.input.offset().top + inputHeight)) ? $(document).scrollTop() : 0; offset.left -= (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ? Math.abs(offset.left + dpWidth - viewWidth) : 0; offset.top -= (offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ? Math.abs(offset.top + dpHeight + inputHeight * 2 - viewHeight) : 0; return offset }, _findPos: function (obj) { while (obj && (obj.type == "hidden" || obj.nodeType != 1)) { obj = obj.nextSibling } var position = $(obj).offset(); return [position.left, position.top] }, _hideDatepicker: function (input, duration) { var inst = this._curInst; if (!inst || (input && inst != $.data(input, PROP_NAME))) { return } if (inst.stayOpen) { this._selectDate("#" + inst.id, this._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear)) } inst.stayOpen = false; if (this._datepickerShowing) { duration = (duration != null ? duration : this._get(inst, "duration")); var showAnim = this._get(inst, "showAnim"); var postProcess = function () { $.datepicker._tidyDialog(inst) }; if (duration != "" && $.effects && $.effects[showAnim]) { inst.dpDiv.hide(showAnim, $.datepicker._get(inst, "showOptions"), duration, postProcess) } else { inst.dpDiv[(duration == "" ? "hide" : (showAnim == "slideDown" ? "slideUp" : (showAnim == "fadeIn" ? "fadeOut" : "hide")))](duration, postProcess) } if (duration == "") { this._tidyDialog(inst) } var onClose = this._get(inst, "onClose"); if (onClose) { onClose.apply((inst.input ? inst.input[0] : null), [(inst.input ? inst.input.val() : ""), inst]) } this._datepickerShowing = false; this._lastInput = null; if (this._inDialog) { this._dialogInput.css({ position: "absolute", left: "0", top: "-100px" }); if ($.blockUI) { $.unblockUI(); $("body").append(this.dpDiv) } } this._inDialog = false } this._curInst = null }, _tidyDialog: function (inst) { inst.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar") }, _checkExternalClick: function (event) { if (!$.datepicker._curInst) { return } var $target = $(event.target); if (($target.parents("#" + $.datepicker._mainDivId).length == 0) && !$target.hasClass($.datepicker.markerClassName) && !$target.hasClass($.datepicker._triggerClass) && $.datepicker._datepickerShowing && !($.datepicker._inDialog && $.blockUI)) { $.datepicker._hideDatepicker(null, "") } }, _adjustDate: function (id, offset, period) { var target = $(id); var inst = this._getInst(target[0]); if (this._isDisabledDatepicker(target[0])) { return } this._adjustInstDate(inst, offset + (period == "M" ? this._get(inst, "showCurrentAtPos") : 0), period); this._updateDatepicker(inst) }, _gotoToday: function (id) { var target = $(id); var inst = this._getInst(target[0]); if (this._get(inst, "gotoCurrent") && inst.currentDay) { inst.selectedDay = inst.currentDay; inst.drawMonth = inst.selectedMonth = inst.currentMonth; inst.drawYear = inst.selectedYear = inst.currentYear } else { var date = new Date(); inst.selectedDay = date.getDate(); inst.drawMonth = inst.selectedMonth = date.getMonth(); inst.drawYear = inst.selectedYear = date.getFullYear() } this._notifyChange(inst); this._adjustDate(target) }, _selectMonthYear: function (id, select, period) { var target = $(id); var inst = this._getInst(target[0]); inst._selectingMonthYear = false; inst["selected" + (period == "M" ? "Month" : "Year")] = inst["draw" + (period == "M" ? "Month" : "Year")] = parseInt(select.options[select.selectedIndex].value, 10); this._notifyChange(inst); this._adjustDate(target) }, _clickMonthYear: function (id) { var target = $(id); var inst = this._getInst(target[0]); if (inst.input && inst._selectingMonthYear && !$.browser.msie) { inst.input[0].focus() } inst._selectingMonthYear = !inst._selectingMonthYear }, _selectDay: function (id, month, year, td) { var target = $(id); if ($(td).hasClass(this._unselectableClass) || this._isDisabledDatepicker(target[0])) { return } var inst = this._getInst(target[0]); inst.selectedDay = inst.currentDay = $("a", td).html(); inst.selectedMonth = inst.currentMonth = month; inst.selectedYear = inst.currentYear = year; if (inst.stayOpen) { inst.endDay = inst.endMonth = inst.endYear = null } this._selectDate(id, this._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear)); if (inst.stayOpen) { inst.rangeStart = this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay)); this._updateDatepicker(inst) } }, _clearDate: function (id) { var target = $(id); var inst = this._getInst(target[0]); inst.stayOpen = false; inst.endDay = inst.endMonth = inst.endYear = inst.rangeStart = null; this._selectDate(target, "") }, _selectDate: function (id, dateStr) { var target = $(id); var inst = this._getInst(target[0]); dateStr = (dateStr != null ? dateStr : this._formatDate(inst)); if (inst.input) { inst.input.val(dateStr) } this._updateAlternate(inst); var onSelect = this._get(inst, "onSelect"); if (onSelect) { onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]) } else { if (inst.input) { inst.input.trigger("change") } } if (inst.inline) { this._updateDatepicker(inst) } else { if (!inst.stayOpen) { this._hideDatepicker(null, this._get(inst, "duration")); this._lastInput = inst.input[0]; if (typeof (inst.input[0]) != "object") { inst.input[0].focus() } this._lastInput = null } } }, _updateAlternate: function (inst) { var altField = this._get(inst, "altField"); if (altField) { var altFormat = this._get(inst, "altFormat") || this._get(inst, "dateFormat"); var date = this._getDate(inst); dateStr = this.formatDate(altFormat, date, this._getFormatConfig(inst)); $(altField).each(function () { $(this).val(dateStr) }) } }, noWeekends: function (date) { var day = date.getDay(); return [(day > 0 && day < 6), ""] }, iso8601Week: function (date) { var checkDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()); var firstMon = new Date(checkDate.getFullYear(), 1 - 1, 4); var firstDay = firstMon.getDay() || 7; firstMon.setDate(firstMon.getDate() + 1 - firstDay); if (firstDay < 4 && checkDate < firstMon) { checkDate.setDate(checkDate.getDate() - 3); return $.datepicker.iso8601Week(checkDate) } else { if (checkDate > new Date(checkDate.getFullYear(), 12 - 1, 28)) { firstDay = new Date(checkDate.getFullYear() + 1, 1 - 1, 4).getDay() || 7; if (firstDay > 4 && (checkDate.getDay() || 7) < firstDay - 3) { return 1 } } } return Math.floor(((checkDate - firstMon) / 86400000) / 7) + 1 }, parseDate: function (format, value, settings) { if (format == null || value == null) { throw "Invalid arguments" } value = (typeof value == "object" ? value.toString() : value + ""); if (value == "") { return null } var shortYearCutoff = (settings ? settings.shortYearCutoff : null) || this._defaults.shortYearCutoff; var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort; var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames; var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort; var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames; var year = -1; var month = -1; var day = -1; var doy = -1; var literal = false; var lookAhead = function (match) { var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match); if (matches) { iFormat++ } return matches }; var getNumber = function (match) { lookAhead(match); var origSize = (match == "@" ? 14 : (match == "y" ? 4 : (match == "o" ? 3 : 2))); var size = origSize; var num = 0; while (size > 0 && iValue < value.length && value.charAt(iValue) >= "0" && value.charAt(iValue) <= "9") { num = num * 10 + parseInt(value.charAt(iValue++), 10); size-- } if (size == origSize) { throw "Missing number at position " + iValue } return num }; var getName = function (match, shortNames, longNames) { var names = (lookAhead(match) ? longNames : shortNames); var size = 0; for (var j = 0; j < names.length; j++) { size = Math.max(size, names[j].length) } var name = ""; var iInit = iValue; while (size > 0 && iValue < value.length) { name += value.charAt(iValue++); for (var i = 0; i < names.length; i++) { if (name == names[i]) { return i + 1 } } size-- } throw "Unknown name at position " + iInit }; var checkLiteral = function () { if (value.charAt(iValue) != format.charAt(iFormat)) { throw "Unexpected literal at position " + iValue } iValue++ }; var iValue = 0; for (var iFormat = 0; iFormat < format.length; iFormat++) { if (literal) { if (format.charAt(iFormat) == "'" && !lookAhead("'")) { literal = false } else { checkLiteral() } } else { switch (format.charAt(iFormat)) { case "d": day = getNumber("d"); break; case "D": getName("D", dayNamesShort, dayNames); break; case "o": doy = getNumber("o"); break; case "m": month = getNumber("m"); break; case "M": month = getName("M", monthNamesShort, monthNames); break; case "y": year = getNumber("y"); break; case "@": var date = new Date(getNumber("@")); year = date.getFullYear(); month = date.getMonth() + 1; day = date.getDate(); break; case "'": if (lookAhead("'")) { checkLiteral() } else { literal = true } break; default: checkLiteral() } } } if (year == -1) { year = new Date().getFullYear() } else { if (year < 100) { year += new Date().getFullYear() - new Date().getFullYear() % 100 + (year <= shortYearCutoff ? 0 : -100) } } if (doy > -1) { month = 1; day = doy; do { var dim = this._getDaysInMonth(year, month - 1); if (day <= dim) { break } month++; day -= dim } while (true) } var date = this._daylightSavingAdjust(new Date(year, month - 1, day)); if (date.getFullYear() != year || date.getMonth() + 1 != month || date.getDate() != day) { throw "Invalid date" } return date }, ATOM: "yy-mm-dd", COOKIE: "D, dd M yy", ISO_8601: "yy-mm-dd", RFC_822: "D, d M y", RFC_850: "DD, dd-M-y", RFC_1036: "D, d M y", RFC_1123: "D, d M yy", RFC_2822: "D, d M yy", RSS: "D, d M y", TIMESTAMP: "@", W3C: "yy-mm-dd", formatDate: function (format, date, settings) { if (!date) { return "" } var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort; var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames; var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort; var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames; var lookAhead = function (match) { var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match); if (matches) { iFormat++ } return matches }; var formatNumber = function (match, value, len) { var num = "" + value; if (lookAhead(match)) { while (num.length < len) { num = "0" + num } } return num }; var formatName = function (match, value, shortNames, longNames) { return (lookAhead(match) ? longNames[value] : shortNames[value]) }; var output = ""; var literal = false; if (date) { for (var iFormat = 0; iFormat < format.length; iFormat++) { if (literal) { if (format.charAt(iFormat) == "'" && !lookAhead("'")) { literal = false } else { output += format.charAt(iFormat) } } else { switch (format.charAt(iFormat)) { case "d": output += formatNumber("d", date.getDate(), 2); break; case "D": output += formatName("D", date.getDay(), dayNamesShort, dayNames); break; case "o": var doy = date.getDate(); for (var m = date.getMonth() - 1; m >= 0; m--) { doy += this._getDaysInMonth(date.getFullYear(), m) } output += formatNumber("o", doy, 3); break; case "m": output += formatNumber("m", date.getMonth() + 1, 2); break; case "M": output += formatName("M", date.getMonth(), monthNamesShort, monthNames); break; case "y": output += (lookAhead("y") ? date.getFullYear() : (date.getYear() % 100 < 10 ? "0" : "") + date.getYear() % 100); break; case "@": output += date.getTime(); break; case "'": if (lookAhead("'")) { output += "'" } else { literal = true } break; default: output += format.charAt(iFormat) } } } } return output }, _possibleChars: function (format) { var chars = ""; var literal = false; for (var iFormat = 0; iFormat < format.length; iFormat++) { if (literal) { if (format.charAt(iFormat) == "'" && !lookAhead("'")) { literal = false } else { chars += format.charAt(iFormat) } } else { switch (format.charAt(iFormat)) { case "d": case "m": case "y": case "@": chars += "0123456789"; break; case "D": case "M": return null; case "'": if (lookAhead("'")) { chars += "'" } else { literal = true } break; default: chars += format.charAt(iFormat) } } } return chars }, _get: function (inst, name) { return inst.settings[name] !== undefined ? inst.settings[name] : this._defaults[name] }, _setDateFromField: function (inst) { var dateFormat = this._get(inst, "dateFormat"); var dates = inst.input ? inst.input.val() : null; inst.endDay = inst.endMonth = inst.endYear = null; var date = defaultDate = this._getDefaultDate(inst); var settings = this._getFormatConfig(inst); try { date = this.parseDate(dateFormat, dates, settings) || defaultDate } catch (event) { this.log(event); date = defaultDate } inst.selectedDay = date.getDate(); inst.drawMonth = inst.selectedMonth = date.getMonth(); inst.drawYear = inst.selectedYear = date.getFullYear(); inst.currentDay = (dates ? date.getDate() : 0); inst.currentMonth = (dates ? date.getMonth() : 0); inst.currentYear = (dates ? date.getFullYear() : 0); this._adjustInstDate(inst) }, _getDefaultDate: function (inst) { var date = this._determineDate(this._get(inst, "defaultDate"), new Date()); var minDate = this._getMinMaxDate(inst, "min", true); var maxDate = this._getMinMaxDate(inst, "max"); date = (minDate && date < minDate ? minDate : date); date = (maxDate && date > maxDate ? maxDate : date); return date }, _determineDate: function (date, defaultDate) { var offsetNumeric = function (offset) { var date = new Date(); date.setDate(date.getDate() + offset); return date }; var offsetString = function (offset, getDaysInMonth) { var date = new Date(); var year = date.getFullYear(); var month = date.getMonth(); var day = date.getDate(); var pattern = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g; var matches = pattern.exec(offset); while (matches) { switch (matches[2] || "d") { case "d": case "D": day += parseInt(matches[1], 10); break; case "w": case "W": day += parseInt(matches[1], 10) * 7; break; case "m": case "M": month += parseInt(matches[1], 10); day = Math.min(day, getDaysInMonth(year, month)); break; case "y": case "Y": year += parseInt(matches[1], 10); day = Math.min(day, getDaysInMonth(year, month)); break } matches = pattern.exec(offset) } return new Date(year, month, day) }; date = (date == null ? defaultDate : (typeof date == "string" ? offsetString(date, this._getDaysInMonth) : (typeof date == "number" ? (isNaN(date) ? defaultDate : offsetNumeric(date)) : date))); date = (date && date.toString() == "Invalid Date" ? defaultDate : date); if (date) { date.setHours(0); date.setMinutes(0); date.setSeconds(0); date.setMilliseconds(0) } return this._daylightSavingAdjust(date) }, _daylightSavingAdjust: function (date) { if (!date) { return null } date.setHours(date.getHours() > 12 ? date.getHours() + 2 : 0); return date }, _setDate: function (inst, date, endDate) { var clear = !(date); var origMonth = inst.selectedMonth; var origYear = inst.selectedYear; date = this._determineDate(date, new Date()); inst.selectedDay = inst.currentDay = date.getDate(); inst.drawMonth = inst.selectedMonth = inst.currentMonth = date.getMonth(); inst.drawYear = inst.selectedYear = inst.currentYear = date.getFullYear(); if (origMonth != inst.selectedMonth || origYear != inst.selectedYear) { this._notifyChange(inst) } this._adjustInstDate(inst); if (inst.input) { inst.input.val(clear ? "" : this._formatDate(inst)) } }, _getDate: function (inst) { var startDate = (!inst.currentYear || (inst.input && inst.input.val() == "") ? null : this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay))); return startDate }, _generateHTML: function (inst) { var today = new Date(); today = this._daylightSavingAdjust(new Date(today.getFullYear(), today.getMonth(), today.getDate())); var isRTL = this._get(inst, "isRTL"); var showButtonPanel = this._get(inst, "showButtonPanel"); var hideIfNoPrevNext = this._get(inst, "hideIfNoPrevNext"); var navigationAsDateFormat = this._get(inst, "navigationAsDateFormat"); var numMonths = this._getNumberOfMonths(inst); var showCurrentAtPos = this._get(inst, "showCurrentAtPos"); var stepMonths = this._get(inst, "stepMonths"); var stepBigMonths = this._get(inst, "stepBigMonths"); var isMultiMonth = (numMonths[0] != 1 || numMonths[1] != 1); var currentDate = this._daylightSavingAdjust((!inst.currentDay ? new Date(9999, 9, 9) : new Date(inst.currentYear, inst.currentMonth, inst.currentDay))); var minDate = this._getMinMaxDate(inst, "min", true); var maxDate = this._getMinMaxDate(inst, "max"); var drawMonth = inst.drawMonth - showCurrentAtPos; var drawYear = inst.drawYear; if (drawMonth < 0) { drawMonth += 12; drawYear-- } if (maxDate) { var maxDraw = this._daylightSavingAdjust(new Date(maxDate.getFullYear(), maxDate.getMonth() - numMonths[1] + 1, maxDate.getDate())); maxDraw = (minDate && maxDraw < minDate ? minDate : maxDraw); while (this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1)) > maxDraw) { drawMonth--; if (drawMonth < 0) { drawMonth = 11; drawYear-- } } } inst.drawMonth = drawMonth; inst.drawYear = drawYear; var prevText = this._get(inst, "prevText"); prevText = (!navigationAsDateFormat ? prevText : this.formatDate(prevText, this._daylightSavingAdjust(new Date(drawYear, drawMonth - stepMonths, 1)), this._getFormatConfig(inst))); var prev = (this._canAdjustMonth(inst, -1, drawYear, drawMonth) ? '<a class="ui-datepicker-prev ui-corner-all" onclick="DP_jQuery.datepicker._adjustDate(\'#' + inst.id + "', -" + stepMonths + ", 'M');\" title=\"" + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? "e" : "w") + '">' + prevText + "</span></a>" : (hideIfNoPrevNext ? "" : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? "e" : "w") + '">' + prevText + "</span></a>")); var nextText = this._get(inst, "nextText"); nextText = (!navigationAsDateFormat ? nextText : this.formatDate(nextText, this._daylightSavingAdjust(new Date(drawYear, drawMonth + stepMonths, 1)), this._getFormatConfig(inst))); var next = (this._canAdjustMonth(inst, +1, drawYear, drawMonth) ? '<a class="ui-datepicker-next ui-corner-all" onclick="DP_jQuery.datepicker._adjustDate(\'#' + inst.id + "', +" + stepMonths + ", 'M');\" title=\"" + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? "w" : "e") + '">' + nextText + "</span></a>" : (hideIfNoPrevNext ? "" : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? "w" : "e") + '">' + nextText + "</span></a>")); var currentText = this._get(inst, "currentText"); var gotoDate = (this._get(inst, "gotoCurrent") && inst.currentDay ? currentDate : today); currentText = (!navigationAsDateFormat ? currentText : this.formatDate(currentText, gotoDate, this._getFormatConfig(inst))); var controls = (!inst.inline ? '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery.datepicker._hideDatepicker();">' + this._get(inst, "closeText") + "</button>" : ""); var buttonPanel = (showButtonPanel) ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (isRTL ? controls : "") + (this._isInRange(inst, gotoDate) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery.datepicker._gotoToday(\'#' + inst.id + "');\">" + currentText + "</button>" : "") + (isRTL ? "" : controls) + "</div>" : ""; var firstDay = parseInt(this._get(inst, "firstDay"), 10); firstDay = (isNaN(firstDay) ? 0 : firstDay); var dayNames = this._get(inst, "dayNames"); var dayNamesShort = this._get(inst, "dayNamesShort"); var dayNamesMin = this._get(inst, "dayNamesMin"); var monthNames = this._get(inst, "monthNames"); var monthNamesShort = this._get(inst, "monthNamesShort"); var beforeShowDay = this._get(inst, "beforeShowDay"); var showOtherMonths = this._get(inst, "showOtherMonths"); var calculateWeek = this._get(inst, "calculateWeek") || this.iso8601Week; var endDate = inst.endDay ? this._daylightSavingAdjust(new Date(inst.endYear, inst.endMonth, inst.endDay)) : currentDate; var defaultDate = this._getDefaultDate(inst); var html = ""; for (var row = 0; row < numMonths[0]; row++) { var group = ""; for (var col = 0; col < numMonths[1]; col++) { var selectedDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, inst.selectedDay)); var cornerClass = " ui-corner-all"; var calender = ""; if (isMultiMonth) { calender += '<div class="ui-datepicker-group ui-datepicker-group-'; switch (col) { case 0: calender += "first"; cornerClass = " ui-corner-" + (isRTL ? "right" : "left"); break; case numMonths[1] - 1: calender += "last"; cornerClass = " ui-corner-" + (isRTL ? "left" : "right"); break; default: calender += "middle"; cornerClass = ""; break } calender += '">' } calender += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + cornerClass + '">' + (/all|left/.test(cornerClass) && row == 0 ? (isRTL ? next : prev) : "") + (/all|right/.test(cornerClass) && row == 0 ? (isRTL ? prev : next) : "") + this._generateMonthYearHeader(inst, drawMonth, drawYear, minDate, maxDate, selectedDate, row > 0 || col > 0, monthNames, monthNamesShort) + '</div><table class="ui-datepicker-calendar"><thead><tr>'; var thead = ""; for (var dow = 0; dow < 7; dow++) { var day = (dow + firstDay) % 7; thead += "<th" + ((dow + firstDay + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : "") + '><span title="' + dayNames[day] + '">' + dayNamesMin[day] + "</span></th>" } calender += thead + "</tr></thead><tbody>"; var daysInMonth = this._getDaysInMonth(drawYear, drawMonth); if (drawYear == inst.selectedYear && drawMonth == inst.selectedMonth) { inst.selectedDay = Math.min(inst.selectedDay, daysInMonth) } var leadDays = (this._getFirstDayOfMonth(drawYear, drawMonth) - firstDay + 7) % 7; var numRows = (isMultiMonth ? 6 : Math.ceil((leadDays + daysInMonth) / 7)); var printDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1 - leadDays)); for (var dRow = 0; dRow < numRows; dRow++) { calender += "<tr>"; var tbody = ""; for (var dow = 0; dow < 7; dow++) { var daySettings = (beforeShowDay ? beforeShowDay.apply((inst.input ? inst.input[0] : null), [printDate]) : [true, ""]); var otherMonth = (printDate.getMonth() != drawMonth); var unselectable = otherMonth || !daySettings[0] || (minDate && printDate < minDate) || (maxDate && printDate > maxDate); tbody += '<td class="' + ((dow + firstDay + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (otherMonth ? " ui-datepicker-other-month" : "") + ((printDate.getTime() == selectedDate.getTime() && drawMonth == inst.selectedMonth && inst._keyEvent) || (defaultDate.getTime() == printDate.getTime() && defaultDate.getTime() == selectedDate.getTime()) ? " " + this._dayOverClass : "") + (unselectable ? " " + this._unselectableClass + " ui-state-disabled" : "") + (otherMonth && !showOtherMonths ? "" : " " + daySettings[1] + (printDate.getTime() >= currentDate.getTime() && printDate.getTime() <= endDate.getTime() ? " " + this._currentClass : "") + (printDate.getTime() == today.getTime() ? " ui-datepicker-today" : "")) + '"' + ((!otherMonth || showOtherMonths) && daySettings[2] ? ' title="' + daySettings[2] + '"' : "") + (unselectable ? "" : " onclick=\"DP_jQuery.datepicker._selectDay('#" + inst.id + "'," + drawMonth + "," + drawYear + ', this);return false;"') + ">" + (otherMonth ? (showOtherMonths ? printDate.getDate() : "&#xa0;") : (unselectable ? '<span class="ui-state-default">' + printDate.getDate() + "</span>" : '<a class="ui-state-default' + (printDate.getTime() == today.getTime() ? " ui-state-highlight" : "") + (printDate.getTime() >= currentDate.getTime() && printDate.getTime() <= endDate.getTime() ? " ui-state-active" : "") + '" href="#">' + printDate.getDate() + "</a>")) + "</td>"; printDate.setDate(printDate.getDate() + 1); printDate = this._daylightSavingAdjust(printDate) } calender += tbody + "</tr>" } drawMonth++; if (drawMonth > 11) { drawMonth = 0; drawYear++ } calender += "</tbody></table>" + (isMultiMonth ? "</div>" + ((numMonths[0] > 0 && col == numMonths[1] - 1) ? '<div class="ui-datepicker-row-break"></div>' : "") : ""); group += calender } html += group } html += buttonPanel + ($.browser.msie && parseInt($.browser.version, 10) < 7 && !inst.inline ? '<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : ""); inst._keyEvent = false; return html }, _generateMonthYearHeader: function (inst, drawMonth, drawYear, minDate, maxDate, selectedDate, secondary, monthNames, monthNamesShort) { minDate = (inst.rangeStart && minDate && selectedDate < minDate ? selectedDate : minDate); var changeMonth = this._get(inst, "changeMonth"); var changeYear = this._get(inst, "changeYear"); var showMonthAfterYear = this._get(inst, "showMonthAfterYear"); var html = '<div class="ui-datepicker-title">'; var monthHtml = ""; if (secondary || !changeMonth) { monthHtml += '<span class="ui-datepicker-month">' + monthNames[drawMonth] + "</span> " } else { var inMinYear = (minDate && minDate.getFullYear() == drawYear); var inMaxYear = (maxDate && maxDate.getFullYear() == drawYear); monthHtml += '<select class="ui-datepicker-month" onchange="DP_jQuery.datepicker._selectMonthYear(\'#' + inst.id + "', this, 'M');\" onclick=\"DP_jQuery.datepicker._clickMonthYear('#" + inst.id + "');\">"; for (var month = 0; month < 12; month++) { if ((!inMinYear || month >= minDate.getMonth()) && (!inMaxYear || month <= maxDate.getMonth())) { monthHtml += '<option value="' + month + '"' + (month == drawMonth ? ' selected="selected"' : "") + ">" + monthNamesShort[month] + "</option>" } } monthHtml += "</select>" } if (!showMonthAfterYear) { html += monthHtml + ((secondary || changeMonth || changeYear) && (!(changeMonth && changeYear)) ? "&#xa0;" : "") } if (secondary || !changeYear) { html += '<span class="ui-datepicker-year">' + drawYear + "</span>" } else { var years = this._get(inst, "yearRange").split(":"); var year = 0; var endYear = 0; if (years.length != 2) { year = drawYear - 10; endYear = drawYear + 10 } else { if (years[0].charAt(0) == "+" || years[0].charAt(0) == "-") { year = drawYear + parseInt(years[0], 10); endYear = drawYear + parseInt(years[1], 10) } else { year = parseInt(years[0], 10); endYear = parseInt(years[1], 10) } } year = (minDate ? Math.max(year, minDate.getFullYear()) : year); endYear = (maxDate ? Math.min(endYear, maxDate.getFullYear()) : endYear); html += '<select class="ui-datepicker-year" onchange="DP_jQuery.datepicker._selectMonthYear(\'#' + inst.id + "', this, 'Y');\" onclick=\"DP_jQuery.datepicker._clickMonthYear('#" + inst.id + "');\">"; for (; year <= endYear; year++) { html += '<option value="' + year + '"' + (year == drawYear ? ' selected="selected"' : "") + ">" + year + "</option>" } html += "</select>" } if (showMonthAfterYear) { html += (secondary || changeMonth || changeYear ? "&#xa0;" : "") + monthHtml } html += "</div>"; return html }, _adjustInstDate: function (inst, offset, period) { var year = inst.drawYear + (period == "Y" ? offset : 0); var month = inst.drawMonth + (period == "M" ? offset : 0); var day = Math.min(inst.selectedDay, this._getDaysInMonth(year, month)) + (period == "D" ? offset : 0); var date = this._daylightSavingAdjust(new Date(year, month, day)); var minDate = this._getMinMaxDate(inst, "min", true); var maxDate = this._getMinMaxDate(inst, "max"); date = (minDate && date < minDate ? minDate : date); date = (maxDate && date > maxDate ? maxDate : date); inst.selectedDay = date.getDate(); inst.drawMonth = inst.selectedMonth = date.getMonth(); inst.drawYear = inst.selectedYear = date.getFullYear(); if (period == "M" || period == "Y") { this._notifyChange(inst) } }, _notifyChange: function (inst) { var onChange = this._get(inst, "onChangeMonthYear"); if (onChange) { onChange.apply((inst.input ? inst.input[0] : null), [inst.selectedYear, inst.selectedMonth + 1, inst]) } }, _getNumberOfMonths: function (inst) { var numMonths = this._get(inst, "numberOfMonths"); return (numMonths == null ? [1, 1] : (typeof numMonths == "number" ? [1, numMonths] : numMonths)) }, _getMinMaxDate: function (inst, minMax, checkRange) { var date = this._determineDate(this._get(inst, minMax + "Date"), null); return (!checkRange || !inst.rangeStart ? date : (!date || inst.rangeStart > date ? inst.rangeStart : date)) }, _getDaysInMonth: function (year, month) { return 32 - new Date(year, month, 32).getDate() }, _getFirstDayOfMonth: function (year, month) { return new Date(year, month, 1).getDay() }, _canAdjustMonth: function (inst, offset, curYear, curMonth) { var numMonths = this._getNumberOfMonths(inst); var date = this._daylightSavingAdjust(new Date(curYear, curMonth + (offset < 0 ? offset : numMonths[1]), 1)); if (offset < 0) { date.setDate(this._getDaysInMonth(date.getFullYear(), date.getMonth())) } return this._isInRange(inst, date) }, _isInRange: function (inst, date) { var newMinDate = (!inst.rangeStart ? null : this._daylightSavingAdjust(new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay))); newMinDate = (newMinDate && inst.rangeStart < newMinDate ? inst.rangeStart : newMinDate); var minDate = newMinDate || this._getMinMaxDate(inst, "min"); var maxDate = this._getMinMaxDate(inst, "max"); return ((!minDate || date >= minDate) && (!maxDate || date <= maxDate)) }, _getFormatConfig: function (inst) { var shortYearCutoff = this._get(inst, "shortYearCutoff"); shortYearCutoff = (typeof shortYearCutoff != "string" ? shortYearCutoff : new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10)); return { shortYearCutoff: shortYearCutoff, dayNamesShort: this._get(inst, "dayNamesShort"), dayNames: this._get(inst, "dayNames"), monthNamesShort: this._get(inst, "monthNamesShort"), monthNames: this._get(inst, "monthNames") } }, _formatDate: function (inst, day, month, year) { if (!day) { inst.currentDay = inst.selectedDay; inst.currentMonth = inst.selectedMonth; inst.currentYear = inst.selectedYear } var date = (day ? (typeof day == "object" ? day : this._daylightSavingAdjust(new Date(year, month, day))) : this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay))); return this.formatDate(this._get(inst, "dateFormat"), date, this._getFormatConfig(inst)) } }); function extendRemove(target, props) { $.extend(target, props); for (var name in props) { if (props[name] == null || props[name] == undefined) { target[name] = props[name] } } return target } function isArray(a) { return (a && (($.browser.safari && typeof a == "object" && a.length) || (a.constructor && a.constructor.toString().match(/\Array\(\)/)))) } $.fn.datepicker = function (options) { if (!$.datepicker.initialized) { $(document).mousedown($.datepicker._checkExternalClick).find("body").append($.datepicker.dpDiv); $.datepicker.initialized = true } var otherArgs = Array.prototype.slice.call(arguments, 1); if (typeof options == "string" && (options == "isDisabled" || options == "getDate")) { return $.datepicker["_" + options + "Datepicker"].apply($.datepicker, [this[0]].concat(otherArgs)) } return this.each(function () { typeof options == "string" ? $.datepicker["_" + options + "Datepicker"].apply($.datepicker, [this].concat(otherArgs)) : $.datepicker._attachDatepicker(this, options) }) }; $.datepicker = new Datepicker(); $.datepicker.initialized = false; $.datepicker.uuid = new Date().getTime(); $.datepicker.version = "1.7.1"; window.DP_jQuery = $ })(jQuery);
/*blockUI 2.33*/; (function ($) { if (/1\.(0|1|2)\.(0|1|2)/.test($.fn.jquery) || /^1.1/.test($.fn.jquery)) { alert('blockUI requires jQuery v1.2.3 or later! You are using v' + $.fn.jquery); return; } $.fn._fadeIn = $.fn.fadeIn; var noOp = function () { }; var mode = document.documentMode || 0; var setExpr = $.browser.msie && (($.browser.version < 8 && !mode) || mode < 8); var ie6 = $.browser.msie && /MSIE 6.0/.test(navigator.userAgent) && !mode; $.blockUI = function (opts) { install(window, opts); }; $.unblockUI = function (opts) { remove(window, opts); }; $.growlUI = function (title, message, timeout, onClose) { var $m = $('<div class="growlUI"></div>'); if (title) $m.append('<h1>' + title + '</h1>'); if (message) $m.append('<h2>' + message + '</h2>'); if (timeout == undefined) timeout = 3000; $.blockUI({ message: $m, fadeIn: 700, fadeOut: 1000, centerY: false, timeout: timeout, showOverlay: false, onUnblock: onClose, css: $.blockUI.defaults.growlCSS }); }; $.fn.block = function (opts) { return this.unblock({ fadeOut: 0 }).each(function () { if ($.css(this, 'position') == 'static') this.style.position = 'relative'; if ($.browser.msie) this.style.zoom = 1; install(this, opts); }); }; $.fn.unblock = function (opts) { return this.each(function () { remove(this, opts); }); }; $.blockUI.version = 2.33; $.blockUI.defaults = { message: '<h1>Please wait...</h1>', title: null, draggable: true, theme: false, css: { padding: 0, margin: 0, width: '30%', top: '40%', left: '35%', textAlign: 'center', color: '#000', border: '3px solid #aaa', backgroundColor: '#fff', cursor: 'wait' }, themedCSS: { width: '30%', top: '40%', left: '35%' }, overlayCSS: { backgroundColor: '#000', opacity: 0.6, cursor: 'wait' }, growlCSS: { width: '350px', top: '10px', left: '', right: '10px', border: 'none', padding: '5px', opacity: 0.6, cursor: 'default', color: '#fff', backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', 'border-radius': '10px' }, iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank', forceIframe: false, baseZ: 1000, centerX: true, centerY: true, allowBodyStretch: true, bindEvents: true, constrainTabKey: true, fadeIn: 200, fadeOut: 400, timeout: 0, showOverlay: true, focusInput: true, applyPlatformOpacityRules: true, onBlock: null, onUnblock: null, quirksmodeOffsetHack: 4 }; var pageBlock = null; var pageBlockEls = []; function install(el, opts) { var full = (el == window); var msg = opts && opts.message !== undefined ? opts.message : undefined; opts = $.extend({}, $.blockUI.defaults, opts || {}); opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {}); var css = $.extend({}, $.blockUI.defaults.css, opts.css || {}); var themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {}); msg = msg === undefined ? opts.message : msg; if (full && pageBlock) remove(window, { fadeOut: 0 }); if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) { var node = msg.jquery ? msg[0] : msg; var data = {}; $(el).data('blockUI.history', data); data.el = node; data.parent = node.parentNode; data.display = node.style.display; data.position = node.style.position; if (data.parent) data.parent.removeChild(node); } var z = opts.baseZ; var lyr1 = ($.browser.msie || opts.forceIframe) ? $('<iframe class="blockUI" style="z-index:' + (z++) + ';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="' + opts.iframeSrc + '"></iframe>') : $('<div class="blockUI" style="display:none"></div>'); var lyr2 = $('<div class="blockUI blockOverlay" style="z-index:' + (z++) + ';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>'); var lyr3, s; if (opts.theme && full) { s = '<div class="blockUI blockMsg blockPage ui-dialog ui-widget ui-corner-all" style="z-index:' + z + ';display:none;position:fixed">' + '<div class="ui-widget-header ui-dialog-titlebar blockTitle">' + (opts.title || '&nbsp;') + '</div>' + '<div class="ui-widget-content ui-dialog-content"></div>' + '</div>'; } else if (opts.theme) { s = '<div class="blockUI blockMsg blockElement ui-dialog ui-widget ui-corner-all" style="z-index:' + z + ';display:none;position:absolute">' + '<div class="ui-widget-header ui-dialog-titlebar blockTitle">' + (opts.title || '&nbsp;') + '</div>' + '<div class="ui-widget-content ui-dialog-content"></div>' + '</div>'; } else if (full) { s = '<div class="blockUI blockMsg blockPage" style="z-index:' + z + ';display:none;position:fixed"></div>'; } else { s = '<div class="blockUI blockMsg blockElement" style="z-index:' + z + ';display:none;position:absolute"></div>'; } lyr3 = $(s); if (msg) { if (opts.theme) { lyr3.css(themedCSS); lyr3.addClass('ui-widget-content'); } else lyr3.css(css); } if (!opts.applyPlatformOpacityRules || !($.browser.mozilla && /Linux/.test(navigator.platform))) lyr2.css(opts.overlayCSS); lyr2.css('position', full ? 'fixed' : 'absolute'); if ($.browser.msie || opts.forceIframe) lyr1.css('opacity', 0.0); var layers = [lyr1, lyr2, lyr3], $par = full ? $('body') : $(el); $.each(layers, function () { this.appendTo($par); }); if (opts.theme && opts.draggable && $.fn.draggable) { lyr3.draggable({ handle: '.ui-dialog-titlebar', cancel: 'li' }); } var expr = setExpr && (!$.boxModel || $('object,embed', full ? null : el).length > 0); if (ie6 || expr) { if (full && opts.allowBodyStretch && $.boxModel) $('html,body').css('height', '100%'); if ((ie6 || !$.boxModel) && !full) { var t = sz(el, 'borderTopWidth'), l = sz(el, 'borderLeftWidth'); var fixT = t ? '(0 - ' + t + ')' : 0; var fixL = l ? '(0 - ' + l + ')' : 0; } $.each([lyr1, lyr2, lyr3], function (i, o) { var s = o[0].style; s.position = 'absolute'; if (i < 2) { full ? s.setExpression('height', 'Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.boxModel?0:' + opts.quirksmodeOffsetHack + ') + "px"') : s.setExpression('height', 'this.parentNode.offsetHeight + "px"'); full ? s.setExpression('width', 'jQuery.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"') : s.setExpression('width', 'this.parentNode.offsetWidth + "px"'); if (fixL) s.setExpression('left', fixL); if (fixT) s.setExpression('top', fixT); } else if (opts.centerY) { if (full) s.setExpression('top', '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"'); s.marginTop = 0; } else if (!opts.centerY && full) { var top = (opts.css && opts.css.top) ? parseInt(opts.css.top) : 0; var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + ' + top + ') + "px"'; s.setExpression('top', expression); } }); } if (msg) { if (opts.theme) lyr3.find('.ui-widget-content').append(msg); else lyr3.append(msg); if (msg.jquery || msg.nodeType) $(msg).show(); } if (($.browser.msie || opts.forceIframe) && opts.showOverlay) lyr1.show(); if (opts.fadeIn) { var cb = opts.onBlock ? opts.onBlock : noOp; var cb1 = (opts.showOverlay && !msg) ? cb : noOp; var cb2 = msg ? cb : noOp; if (opts.showOverlay) lyr2._fadeIn(opts.fadeIn, cb1); if (msg) lyr3._fadeIn(opts.fadeIn, cb2); } else { if (opts.showOverlay) lyr2.show(); if (msg) lyr3.show(); if (opts.onBlock) opts.onBlock(); } bind(1, el, opts); if (full) { pageBlock = lyr3[0]; pageBlockEls = $(':input:enabled:visible', pageBlock); if (opts.focusInput) setTimeout(focus, 20); } else center(lyr3[0], opts.centerX, opts.centerY); if (opts.timeout) { var to = setTimeout(function () { full ? $.unblockUI(opts) : $(el).unblock(opts); }, opts.timeout); $(el).data('blockUI.timeout', to); } }; function remove(el, opts) { var full = (el == window); var $el = $(el); var data = $el.data('blockUI.history'); var to = $el.data('blockUI.timeout'); if (to) { clearTimeout(to); $el.removeData('blockUI.timeout'); } opts = $.extend({}, $.blockUI.defaults, opts || {}); bind(0, el, opts); var els; if (full) els = $('body').children().filter('.blockUI').add('body > .blockUI'); else els = $('.blockUI', el); if (full) pageBlock = pageBlockEls = null; if (opts.fadeOut) { els.fadeOut(opts.fadeOut); setTimeout(function () { reset(els, data, opts, el); }, opts.fadeOut); } else reset(els, data, opts, el); }; function reset(els, data, opts, el) { els.each(function (i, o) { if (this.parentNode) this.parentNode.removeChild(this); }); if (data && data.el) { data.el.style.display = data.display; data.el.style.position = data.position; if (data.parent) data.parent.appendChild(data.el); $(el).removeData('blockUI.history'); } if (typeof opts.onUnblock == 'function') opts.onUnblock(el, opts); }; function bind(b, el, opts) { var full = el == window, $el = $(el); if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked'))) return; if (!full) $el.data('blockUI.isBlocked', b); if (!opts.bindEvents || (b && !opts.showOverlay)) return; var events = 'mousedown mouseup keydown keypress'; b ? $(document).bind(events, opts, handler) : $(document).unbind(events, handler); }; function handler(e) { if (e.keyCode && e.keyCode == 9) { if (pageBlock && e.data.constrainTabKey) { var els = pageBlockEls; var fwd = !e.shiftKey && e.target == els[els.length - 1]; var back = e.shiftKey && e.target == els[0]; if (fwd || back) { setTimeout(function () { focus(back) }, 10); return false; } } } if ($(e.target).parents('div.blockMsg').length > 0) return true; return $(e.target).parents().children().filter('div.blockUI').length == 0; }; function focus(back) { if (!pageBlockEls) return; var e = pageBlockEls[back === true ? pageBlockEls.length - 1 : 0]; if (e) e.focus(); }; function center(el, x, y) { var p = el.parentNode, s = el.style; var l = ((p.offsetWidth - el.offsetWidth) / 2) - sz(p, 'borderLeftWidth'); var t = ((p.offsetHeight - el.offsetHeight) / 2) - sz(p, 'borderTopWidth'); if (x) s.left = l > 0 ? (l + 'px') : '0'; if (y) s.top = t > 0 ? (t + 'px') : '0'; }; function sz(el, p) { return parseInt($.css(el, p)) || 0; }; })(jQuery);
/*mousewheel 3.0.2*/(function (c) { var a = ["DOMMouseScroll", "mousewheel"]; c.event.special.mousewheel = { setup: function () { if (this.addEventListener) { for (var d = a.length; d;) { this.addEventListener(a[--d], b, false) } } else { this.onmousewheel = b } }, teardown: function () { if (this.removeEventListener) { for (var d = a.length; d;) { this.removeEventListener(a[--d], b, false) } } else { this.onmousewheel = null } } }; c.fn.extend({ mousewheel: function (d) { return d ? this.bind("mousewheel", d) : this.trigger("mousewheel") }, unmousewheel: function (d) { return this.unbind("mousewheel", d) } }); function b(f) { var d = [].slice.call(arguments, 1), g = 0, e = true; f = c.event.fix(f || window.event); f.type = "mousewheel"; if (f.wheelDelta) { g = f.wheelDelta / 120 } if (f.detail) { g = -f.detail / 3 } d.unshift(f, g); return c.event.handle.apply(this, d) } })(jQuery);
/*textselection*/$(function () { $.extend($.fn.disableTextSelect = function () { return this.each(function () { if ($.browser.mozilla) { $(this).css('MozUserSelect', 'none'); } else if ($.browser.msie) { $(this).bind('selectstart', function () { return false; }); } else { $(this).mousedown(function () { return false; }); } }); }); });
/*tablesorter 2.0.3*/(function ($) { $.extend({ tablesorter: new function () { var parsers = [], widgets = []; this.defaults = { cssHeader: "header", cssAsc: "asc", cssDesc: "desc", sortInitialOrder: "asc", sortMultiSortKey: "shiftKey", sortForce: null, sortAppend: null, textExtraction: "simple", parsers: {}, widgets: [], widgetZebra: { css: ["even", "odd"] }, headers: {}, widthFixed: false, cancelSelection: true, sortList: [], headerList: [], dateFormat: "us", decimal: '.', fixed_headers: false, header_fixpoint: '', debug: false }; function benchmark(s, d) { log(s + "," + (new Date().getTime() - d.getTime()) + "ms"); } this.benchmark = benchmark; function log(s) { if (typeof console != "undefined" && typeof console.debug != "undefined") { console.log(s); } else { alert(s); } } function buildParserCache(table, $headers) { if (table.config.debug) { var parsersDebug = ""; } var rows = table.tBodies[0].rows; if (table.tBodies[0].rows[0]) { var list = [], cells = rows[0].cells, l = cells.length; for (var i = 0; i < l; i++) { var p = false; if ($.metadata && ($($headers[i]).metadata() && $($headers[i]).metadata().sorter)) { p = getParserById($($headers[i]).metadata().sorter); } else if ((table.config.headers[i] && table.config.headers[i].sorter)) { p = getParserById(table.config.headers[i].sorter); } if (!p) { p = detectParserForColumn(table, cells[i]); } if (table.config.debug) { parsersDebug += "column:" + i + " parser:" + p.id + "\n"; } list.push(p); } } if (table.config.debug) { log(parsersDebug); } return list; }; function detectParserForColumn(table, node) { var l = parsers.length; for (var i = 1; i < l; i++) { if (parsers[i].is($.trim(getElementText(table.config, node)), table, node)) { return parsers[i]; } } return parsers[0]; } function getParserById(name) { var l = parsers.length; for (var i = 0; i < l; i++) { if (parsers[i].id.toLowerCase() == name.toLowerCase()) { return parsers[i]; } } return false; } function buildCache(table) { if (table.config.debug) { var cacheTime = new Date(); } var totalRows = (table.tBodies[0] && table.tBodies[0].rows.length) || 0, totalCells = (table.tBodies[0].rows[0] && table.tBodies[0].rows[0].cells.length) || 0, parsers = table.config.parsers, cache = { row: [], normalized: [] }; for (var i = 0; i < totalRows; ++i) { var c = table.tBodies[0].rows[i], cols = []; cache.row.push($(c)); for (var j = 0; j < totalCells; ++j) { cols.push(parsers[j].format(getElementText(table.config, c.cells[j]), table, c.cells[j])); } cols.push(i); cache.normalized.push(cols); cols = null; }; if (table.config.debug) { benchmark("Building cache for " + totalRows + " rows:", cacheTime); } return cache; }; function getElementText(config, node) { if (!node) return ""; var t = ""; if (config.textExtraction == "simple") { if (node.childNodes[0] && node.childNodes[0].hasChildNodes()) { t = node.childNodes[0].innerHTML; } else { t = node.innerHTML; } } else { if (typeof (config.textExtraction) == "function") { t = config.textExtraction(node); } else { t = $(node).text(); } } return t; } function appendToTable(table, cache) { if (table.config.debug) { var appendTime = new Date() } var c = cache, r = c.row, n = c.normalized, totalRows = n.length, checkCell = (n[0].length - 1), tableBody = $(table.tBodies[0]), rows = []; for (var i = 0; i < totalRows; i++) { rows.push(r[n[i][checkCell]]); if (!table.config.appender) { var o = r[n[i][checkCell]]; var l = o.length; for (var j = 0; j < l; j++) { tableBody[0].appendChild(o[j]); } } } if (table.config.appender) { table.config.appender(table, rows); } rows = null; if (table.config.debug) { benchmark("Rebuilt table:", appendTime); } applyWidget(table); setTimeout(function () { $(table).trigger("sortEnd"); }, 0); }; function buildHeaders(table) { if (table.config.debug) { var time = new Date(); } var meta = ($.metadata) ? true : false, tableHeadersRows = []; for (var i = 0; i < table.tHead.rows.length; i++) { tableHeadersRows[i] = 0; }; $tableHeaders = $("thead th", table); if (table.config.fixed_headers) { $tableHeaders = $(table).clone(true).attr('id', 'vehicles_').find('tbody').hide().end().css({ position: 'absolute', left: 0, top: 0, 'z-index': 5, width: $(table).width() + 1 }).appendTo($(table.config.header_fixpoint)).find('thead th'); } $tableHeaders.each(function (index) { this.count = 0; this.column = index; this.order = formatSortingOrder(table.config.sortInitialOrder); if (checkHeaderMetadata(this) || checkHeaderOptions(table, index)) this.sortDisabled = true; if (!this.sortDisabled) { $(this).addClass(table.config.cssHeader); } table.config.headerList[index] = this; }); if (table.config.debug) { benchmark("Built headers:", time); log($tableHeaders); } return $tableHeaders; }; function checkCellColSpan(table, rows, row) { var arr = [], r = table.tHead.rows, c = r[row].cells; for (var i = 0; i < c.length; i++) { var cell = c[i]; if (cell.colSpan > 1) { arr = arr.concat(checkCellColSpan(table, headerArr, row++)); } else { if (table.tHead.length == 1 || (cell.rowSpan > 1 || !r[row + 1])) { arr.push(cell); } } } return arr; }; function checkHeaderMetadata(cell) { if (($.metadata) && ($(cell).metadata().sorter === false)) { return true; }; return false; } function checkHeaderOptions(table, i) { if ((table.config.headers[i]) && (table.config.headers[i].sorter === false)) { return true; }; return false; } function applyWidget(table) { var c = table.config.widgets; var l = c.length; for (var i = 0; i < l; i++) { getWidgetById(c[i]).format(table); } } function getWidgetById(name) { var l = widgets.length; for (var i = 0; i < l; i++) { if (widgets[i].id.toLowerCase() == name.toLowerCase()) { return widgets[i]; } } }; function formatSortingOrder(v) { if (typeof (v) != "Number") { i = (v.toLowerCase() == "desc") ? 1 : 0; } else { i = (v == (0 || 1)) ? v : 0; } return i; } function isValueInArray(v, a) { var l = a.length; for (var i = 0; i < l; i++) { if (a[i][0] == v) { return true; } } return false; } function setHeadersCss(table, $headers, list, css) { $headers.removeClass(css[0]).removeClass(css[1]); var h = []; $headers.each(function (offset) { if (!this.sortDisabled) { h[this.column] = $(this); } }); var l = list.length; for (var i = 0; i < l; i++) { h[list[i][0]].addClass(css[list[i][1]]); } } function fixColumnWidth(table, $headers) { var c = table.config; if (c.widthFixed) { var colgroup = $('<colgroup>'); $("tr:first td", table.tBodies[0]).each(function () { colgroup.append($('<col>').css('width', $(this).width())); }); $(table).prepend(colgroup); }; } function updateHeaderSortCount(table, sortList) { var c = table.config, l = sortList.length; for (var i = 0; i < l; i++) { var s = sortList[i], o = c.headerList[s[0]]; o.count = s[1]; o.count++; } } function multisort(table, sortList, cache) { if (table.config.debug) { var sortTime = new Date(); } var dynamicExp = "var sortWrapper = function(a,b) {", l = sortList.length; for (var i = 0; i < l; i++) { var c = sortList[i][0]; var order = sortList[i][1]; var s = (getCachedSortType(table.config.parsers, c) == "text") ? ((order == 0) ? "sortText" : "sortTextDesc") : ((order == 0) ? "sortNumeric" : "sortNumericDesc"); var e = "e" + i; dynamicExp += "var " + e + " = " + s + "(a[" + c + "],b[" + c + "]); "; dynamicExp += "if(" + e + ") { return " + e + "; } "; dynamicExp += "else { "; } var orgOrderCol = cache.normalized[0].length - 1; dynamicExp += "return a[" + orgOrderCol + "]-b[" + orgOrderCol + "];"; for (var i = 0; i < l; i++) { dynamicExp += "}; "; } dynamicExp += "return 0; "; dynamicExp += "}; "; eval(dynamicExp); cache.normalized.sort(sortWrapper); if (table.config.debug) { benchmark("Sorting on " + sortList.toString() + " and dir " + order + " time:", sortTime); } return cache; }; function sortText(a, b) { return ((a < b) ? -1 : ((a > b) ? 1 : 0)); }; function sortTextDesc(a, b) { return ((b < a) ? -1 : ((b > a) ? 1 : 0)); }; function sortNumeric(a, b) { return a - b; }; function sortNumericDesc(a, b) { return b - a; }; function getCachedSortType(parsers, i) { return parsers[i].type; }; this.construct = function (settings) { return this.each(function () { if (!this.tHead || !this.tBodies) return; var $this, $document, $headers, cache, config, shiftDown = 0, sortOrder; this.config = {}; config = $.extend(this.config, $.tablesorter.defaults, settings); $this = $(this); $headers = buildHeaders(this); this.config.parsers = buildParserCache(this, $headers); cache = buildCache(this); var sortCSS = [config.cssDesc, config.cssAsc]; fixColumnWidth(this); $headers.click(function (e) { $this.trigger("sortStart"); var totalRows = ($this[0].tBodies[0] && $this[0].tBodies[0].rows.length) || 0; if (!this.sortDisabled && totalRows > 0) { var $cell = $(this); var i = this.column; this.order = this.count++ % 2; if (!e[config.sortMultiSortKey]) { config.sortList = []; if (config.sortForce != null) { var a = config.sortForce; for (var j = 0; j < a.length; j++) { if (a[j][0] != i) { config.sortList.push(a[j]); } } } config.sortList.push([i, this.order]); } else { if (isValueInArray(i, config.sortList)) { for (var j = 0; j < config.sortList.length; j++) { var s = config.sortList[j], o = config.headerList[s[0]]; if (s[0] == i) { o.count = s[1]; o.count++; s[1] = o.count % 2; } } } else { config.sortList.push([i, this.order]); } }; setTimeout(function () { setHeadersCss($this[0], $headers, config.sortList, sortCSS); appendToTable($this[0], multisort($this[0], config.sortList, cache)); }, 1); return false; } }).mousedown(function () { if (config.cancelSelection) { this.onselectstart = function () { return false }; return false; } }); $this.bind("update", function () { this.config.parsers = buildParserCache(this, $headers); cache = buildCache(this); }).bind("sorton", function (e, list) { $(this).trigger("sortStart"); config.sortList = list; var sortList = config.sortList; updateHeaderSortCount(this, sortList); setHeadersCss(this, $headers, sortList, sortCSS); appendToTable(this, multisort(this, sortList, cache)); }).bind("appendCache", function () { appendToTable(this, cache); }).bind("applyWidgetId", function (e, id) { getWidgetById(id).format(this); }).bind("applyWidgets", function () { applyWidget(this); }); if ($.metadata && ($(this).metadata() && $(this).metadata().sortlist)) { config.sortList = $(this).metadata().sortlist; } if (config.sortList.length > 0) { $this.trigger("sorton", [config.sortList]); } applyWidget(this); }); }; this.addParser = function (parser) { var l = parsers.length, a = true; for (var i = 0; i < l; i++) { if (parsers[i].id.toLowerCase() == parser.id.toLowerCase()) { a = false; } } if (a) { parsers.push(parser); }; }; this.addWidget = function (widget) { widgets.push(widget); }; this.formatFloat = function (s) { var i = parseFloat(s); return (isNaN(i)) ? 0 : i; }; this.formatInt = function (s) { var i = parseInt(s); return (isNaN(i)) ? 0 : i; }; this.isDigit = function (s, config) { var DECIMAL = '\\' + config.decimal; var exp = '/(^[+]?0(' + DECIMAL + '0+)?$)|(^([-+]?[1-9][0-9]*)$)|(^([-+]?((0?|[1-9][0-9]*)' + DECIMAL + '(0*[1-9][0-9]*)))$)|(^[-+]?[1-9]+[0-9]*' + DECIMAL + '0+$)/'; return RegExp(exp).test($.trim(s)); }; this.clearTableBody = function (table) { if ($.browser.msie) { function empty() { while (this.firstChild) this.removeChild(this.firstChild); } empty.apply(table.tBodies[0]); } else { table.tBodies[0].innerHTML = ""; } }; } }); $.fn.extend({ tablesorter: $.tablesorter.construct }); var ts = $.tablesorter; ts.addParser({ id: "text", is: function (s) { return true; }, format: function (s) { return $.trim(s.toLowerCase()); }, type: "text" }); ts.addParser({ id: "digit", is: function (s, table) { var c = table.config; return $.tablesorter.isDigit(s, c); }, format: function (s) { return $.tablesorter.formatFloat(s); }, type: "numeric" }); ts.addParser({ id: "currency", is: function (s) { return /^[�$�?.]/.test(s); }, format: function (s) { return $.tablesorter.formatFloat(s.replace(new RegExp(/[^0-9.]/g), "")); }, type: "numeric" }); ts.addParser({ id: "ipAddress", is: function (s) { return /^\d{2,3}[\.]\d{2,3}[\.]\d{2,3}[\.]\d{2,3}$/.test(s); }, format: function (s) { var a = s.split("."), r = "", l = a.length; for (var i = 0; i < l; i++) { var item = a[i]; if (item.length == 2) { r += "0" + item; } else { r += item; } } return $.tablesorter.formatFloat(r); }, type: "numeric" }); ts.addParser({ id: "url", is: function (s) { return /^(https?|ftp|file):\/\/$/.test(s); }, format: function (s) { return jQuery.trim(s.replace(new RegExp(/(https?|ftp|file):\/\//), '')); }, type: "text" }); ts.addParser({ id: "isoDate", is: function (s) { return /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(s); }, format: function (s) { return $.tablesorter.formatFloat((s != "") ? new Date(s.replace(new RegExp(/-/g), "/")).getTime() : "0"); }, type: "numeric" }); ts.addParser({ id: "percent", is: function (s) { return /\%$/.test($.trim(s)); }, format: function (s) { return $.tablesorter.formatFloat(s.replace(new RegExp(/%/g), "")); }, type: "numeric" }); ts.addParser({ id: "usLongDate", is: function (s) { return s.match(new RegExp(/^[A-Za-z]{3,10}\.? [0-9]{1,2}, ([0-9]{4}|'?[0-9]{2}) (([0-2]?[0-9]:[0-5][0-9])|([0-1]?[0-9]:[0-5][0-9]\s(AM|PM)))$/)); }, format: function (s) { return $.tablesorter.formatFloat(new Date(s).getTime()); }, type: "numeric" }); ts.addParser({ id: "shortDate", is: function (s) { return /\d{1,2}[\/\-]\d{1,2}[\/\-]\d{2,4}/.test(s); }, format: function (s, table) { var c = table.config; s = s.replace(/\-/g, "/"); if (c.dateFormat == "us") { s = s.replace(/(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4})/, "$3/$1/$2"); } else if (c.dateFormat == "uk") { s = s.replace(/(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4})/, "$3/$2/$1"); } else if (c.dateFormat == "dd/mm/yy" || c.dateFormat == "dd-mm-yy") { s = s.replace(/(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{2})/, "$1/$2/$3"); } return $.tablesorter.formatFloat(new Date(s).getTime()); }, type: "numeric" }); ts.addParser({ id: "time", is: function (s) { return /^(([0-2]?[0-9]:[0-5][0-9])|([0-1]?[0-9]:[0-5][0-9]\s(am|pm)))$/.test(s); }, format: function (s) { return $.tablesorter.formatFloat(new Date("2000/01/01 " + s).getTime()); }, type: "numeric" }); ts.addParser({ id: "metadata", is: function (s) { return false; }, format: function (s, table, cell) { var c = table.config, p = (!c.parserMetadataName) ? 'sortValue' : c.parserMetadataName; return $(cell).metadata()[p]; }, type: "numeric" }); ts.addWidget({ id: "zebra", format: function (table) { if (table.config.debug) { var time = new Date(); } $("tr:visible", table.tBodies[0]).filter(':even').removeClass(table.config.widgetZebra.css[1]).addClass(table.config.widgetZebra.css[0]).end().filter(':odd').removeClass(table.config.widgetZebra.css[0]).addClass(table.config.widgetZebra.css[1]); if (table.config.debug) { $.tablesorter.benchmark("Applying Zebra widget", time); } } }); })(jQuery);

/*blockUI 2.61.0-2013.06.06*/(function () { "use strict"; function e(e) { function a(i, a) { var l, h; var m = i == window; var g = a && a.message !== undefined ? a.message : undefined; a = e.extend({}, e.blockUI.defaults, a || {}); if (a.ignoreIfBlocked && e(i).data("blockUI.isBlocked")) return; a.overlayCSS = e.extend({}, e.blockUI.defaults.overlayCSS, a.overlayCSS || {}); l = e.extend({}, e.blockUI.defaults.css, a.css || {}); if (a.onOverlayClick) a.overlayCSS.cursor = "pointer"; h = e.extend({}, e.blockUI.defaults.themedCSS, a.themedCSS || {}); g = g === undefined ? a.message : g; if (m && o) f(window, { fadeOut: 0 }); if (g && typeof g != "string" && (g.parentNode || g.jquery)) { var y = g.jquery ? g[0] : g; var b = {}; e(i).data("blockUI.history", b); b.el = y; b.parent = y.parentNode; b.display = y.style.display; b.position = y.style.position; if (b.parent) b.parent.removeChild(y) } e(i).data("blockUI.onUnblock", a.onUnblock); var w = a.baseZ; var E, S, x, T; if (n || a.forceIframe) E = e('<iframe class="blockUI" style="z-index:' + w++ + ';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="' + a.iframeSrc + '"></iframe>'); else E = e('<div class="blockUI" style="display:none"></div>'); if (a.theme) S = e('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:' + w++ + ';display:none"></div>'); else S = e('<div class="blockUI blockOverlay" style="z-index:' + w++ + ';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>'); if (a.theme && m) { T = '<div class="blockUI ' + a.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:' + (w + 10) + ';display:none;position:fixed">'; if (a.title) { T += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (a.title || " ") + "</div>" } T += '<div class="ui-widget-content ui-dialog-content"></div>'; T += "</div>" } else if (a.theme) { T = '<div class="blockUI ' + a.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:' + (w + 10) + ';display:none;position:absolute">'; if (a.title) { T += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (a.title || " ") + "</div>" } T += '<div class="ui-widget-content ui-dialog-content"></div>'; T += "</div>" } else if (m) { T = '<div class="blockUI ' + a.blockMsgClass + ' blockPage" style="z-index:' + (w + 10) + ';display:none;position:fixed"></div>' } else { T = '<div class="blockUI ' + a.blockMsgClass + ' blockElement" style="z-index:' + (w + 10) + ';display:none;position:absolute"></div>' } x = e(T); if (g) { if (a.theme) { x.css(h); x.addClass("ui-widget-content") } else x.css(l) } if (!a.theme) S.css(a.overlayCSS); S.css("position", m ? "fixed" : "absolute"); if (n || a.forceIframe) E.css("opacity", 0); var N = [E, S, x], C = m ? e("body") : e(i); e.each(N, function () { this.appendTo(C) }); if (a.theme && a.draggable && e.fn.draggable) { x.draggable({ handle: ".ui-dialog-titlebar", cancel: "li" }) } var k = s && (!e.support.boxModel || e("object,embed", m ? null : i).length > 0); if (r || k) { if (m && a.allowBodyStretch && e.support.boxModel) e("html,body").css("height", "100%"); if ((r || !e.support.boxModel) && !m) { var L = v(i, "borderTopWidth"), A = v(i, "borderLeftWidth"); var O = L ? "(0 - " + L + ")" : 0; var M = A ? "(0 - " + A + ")" : 0 } e.each(N, function (e, t) { var n = t[0].style; n.position = "absolute"; if (e < 2) { if (m) n.setExpression("height", "Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:" + a.quirksmodeOffsetHack + ') + "px"'); else n.setExpression("height", 'this.parentNode.offsetHeight + "px"'); if (m) n.setExpression("width", 'jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"'); else n.setExpression("width", 'this.parentNode.offsetWidth + "px"'); if (M) n.setExpression("left", M); if (O) n.setExpression("top", O) } else if (a.centerY) { if (m) n.setExpression("top", '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"'); n.marginTop = 0 } else if (!a.centerY && m) { var r = a.css && a.css.top ? parseInt(a.css.top, 10) : 0; var i = "((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + " + r + ') + "px"'; n.setExpression("top", i) } }) } if (g) { if (a.theme) x.find(".ui-widget-content").append(g); else x.append(g); if (g.jquery || g.nodeType) e(g).show() } if ((n || a.forceIframe) && a.showOverlay) E.show(); if (a.fadeIn) { var _ = a.onBlock ? a.onBlock : t; var D = a.showOverlay && !g ? _ : t; var P = g ? _ : t; if (a.showOverlay) S._fadeIn(a.fadeIn, D); if (g) x._fadeIn(a.fadeIn, P) } else { if (a.showOverlay) S.show(); if (g) x.show(); if (a.onBlock) a.onBlock() } c(1, i, a); if (m) { o = x[0]; u = e(a.focusableElements, o); if (a.focusInput) setTimeout(p, 20) } else d(x[0], a.centerX, a.centerY); if (a.timeout) { var H = setTimeout(function () { if (m) e.unblockUI(a); else e(i).unblock(a) }, a.timeout); e(i).data("blockUI.timeout", H) } } function f(t, n) { var r; var i = t == window; var s = e(t); var a = s.data("blockUI.history"); var f = s.data("blockUI.timeout"); if (f) { clearTimeout(f); s.removeData("blockUI.timeout") } n = e.extend({}, e.blockUI.defaults, n || {}); c(0, t, n); if (n.onUnblock === null) { n.onUnblock = s.data("blockUI.onUnblock"); s.removeData("blockUI.onUnblock") } var h; if (i) h = e("body").children().filter(".blockUI").add("body > .blockUI"); else h = s.find(">.blockUI"); if (n.cursorReset) { if (h.length > 1) h[1].style.cursor = n.cursorReset; if (h.length > 2) h[2].style.cursor = n.cursorReset } if (i) o = u = null; if (n.fadeOut) { r = h.length; h.fadeOut(n.fadeOut, function () { if (--r === 0) l(h, a, n, t) }) } else l(h, a, n, t) } function l(t, n, r, i) { var s = e(i); t.each(function (e, t) { if (this.parentNode) this.parentNode.removeChild(this) }); if (n && n.el) { n.el.style.display = n.display; n.el.style.position = n.position; if (n.parent) n.parent.appendChild(n.el); s.removeData("blockUI.history") } if (s.data("blockUI.static")) { s.css("position", "static") } if (typeof r.onUnblock == "function") r.onUnblock(i, r); var o = e(document.body), u = o.width(), a = o[0].style.width; o.width(u - 1).width(u); o[0].style.width = a } function c(t, n, r) { var i = n == window, s = e(n); if (!t && (i && !o || !i && !s.data("blockUI.isBlocked"))) return; s.data("blockUI.isBlocked", t); if (!i || !r.bindEvents || t && !r.showOverlay) return; var u = "mousedown mouseup keydown keypress keyup touchstart touchend touchmove"; if (t) e(document).bind(u, r, h); else e(document).unbind(u, h) } function h(t) { if (t.keyCode && t.keyCode == 9) { if (o && t.data.constrainTabKey) { var n = u; var r = !t.shiftKey && t.target === n[n.length - 1]; var i = t.shiftKey && t.target === n[0]; if (r || i) { setTimeout(function () { p(i) }, 10); return false } } } var s = t.data; var a = e(t.target); if (a.hasClass("blockOverlay") && s.onOverlayClick) s.onOverlayClick(); if (a.parents("div." + s.blockMsgClass).length > 0) return true; return a.parents().children().filter("div.blockUI").length === 0 } function p(e) { if (!u) return; var t = u[e === true ? u.length - 1 : 0]; if (t) t.focus() } function d(e, t, n) { var r = e.parentNode, i = e.style; var s = (r.offsetWidth - e.offsetWidth) / 2 - v(r, "borderLeftWidth"); var o = (r.offsetHeight - e.offsetHeight) / 2 - v(r, "borderTopWidth"); if (t) i.left = s > 0 ? s + "px" : "0"; if (n) i.top = o > 0 ? o + "px" : "0" } function v(t, n) { return parseInt(e.css(t, n), 10) || 0 } e.fn._fadeIn = e.fn.fadeIn; var t = e.noop || function () { }; var n = /MSIE/.test(navigator.userAgent); var r = /MSIE 6.0/.test(navigator.userAgent) && !/MSIE 8.0/.test(navigator.userAgent); var i = document.documentMode || 0; var s = e.isFunction(document.createElement("div").style.setExpression); e.blockUI = function (e) { a(window, e) }; e.unblockUI = function (e) { f(window, e) }; e.growlUI = function (t, n, r, i) { var s = e('<div class="growlUI"></div>'); if (t) s.append("<h1>" + t + "</h1>"); if (n) s.append("<h2>" + n + "</h2>"); if (r === undefined) r = 3e3; var o = function (t) { t = t || {}; e.blockUI({ message: s, fadeIn: typeof t.fadeIn !== "undefined" ? t.fadeIn : 700, fadeOut: typeof t.fadeOut !== "undefined" ? t.fadeOut : 1e3, timeout: typeof t.timeout !== "undefined" ? t.timeout : r, centerY: false, showOverlay: false, onUnblock: i, css: e.blockUI.defaults.growlCSS }) }; o(); var u = s.css("opacity"); s.mouseover(function () { o({ fadeIn: 0, timeout: 3e4 }); var t = e(".blockMsg"); t.stop(); t.fadeTo(300, 1) }).mouseout(function () { e(".blockMsg").fadeOut(1e3) }) }; e.fn.block = function (t) { if (this[0] === window) { e.blockUI(t); return this } var n = e.extend({}, e.blockUI.defaults, t || {}); this.each(function () { var t = e(this); if (n.ignoreIfBlocked && t.data("blockUI.isBlocked")) return; t.unblock({ fadeOut: 0 }) }); return this.each(function () { if (e.css(this, "position") == "static") { this.style.position = "relative"; e(this).data("blockUI.static", true) } this.style.zoom = 1; a(this, t) }) }; e.fn.unblock = function (t) { if (this[0] === window) { e.unblockUI(t); return this } return this.each(function () { f(this, t) }) }; e.blockUI.version = 2.6; e.blockUI.defaults = { message: "<h1>Please wait...</h1>", title: null, draggable: true, theme: false, css: { padding: 0, margin: 0, width: "30%", top: "40%", left: "35%", textAlign: "center", color: "#000", border: "3px solid #aaa", backgroundColor: "#fff", cursor: "wait" }, themedCSS: { width: "30%", top: "40%", left: "35%" }, overlayCSS: { backgroundColor: "#000", opacity: .6, cursor: "wait" }, cursorReset: "default", growlCSS: { width: "350px", top: "10px", left: "", right: "10px", border: "none", padding: "5px", opacity: .6, cursor: "default", color: "#fff", backgroundColor: "#000", "-webkit-border-radius": "10px", "-moz-border-radius": "10px", "border-radius": "10px" }, iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank", forceIframe: false, baseZ: 1e3, centerX: true, centerY: true, allowBodyStretch: true, bindEvents: true, constrainTabKey: true, fadeIn: 200, fadeOut: 400, timeout: 0, showOverlay: true, focusInput: true, focusableElements: ":input:enabled:visible", onBlock: null, onUnblock: null, onOverlayClick: null, quirksmodeOffsetHack: 4, blockMsgClass: "blockMsg", ignoreIfBlocked: false }; var o = null; var u = [] } if (typeof define === "function" && define.amd && define.amd.jQuery) { define(["jquery"], e) } else { e(jQuery) } })()

// Copyright (c) 2012 - Geecko Group Holdings Ltd
var locate_interval = 10000;
var update_interval = 15000;
var street_zoom = 18;
var town_zoom = 15;
var icon_path = 'images/';
var can_locate = true;
var update_pending = false;
var logging_out = false;
var map = null;
var vehicles = [];
var selected_vehicle = null;
var vehicle_groups = { All: '' };

google.maps.visualRefresh = true;

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}

var pickerOpts = {
    dateFormat: "mm - dd - yy"
};

function PlaySound() {
    $('#off-screen').html('<embed src="' + soundpath + '" autostart="true" loop="false" hidden="true"></embed>');
    setTimeout(function () { $('#off-screen').html(''); }, 6000);
}

function log(x) {
    if (typeof console != 'undefined') {
        console.log(x);
    }
}

function open_progress(description) {
    $.blockUI({
        message: description + '<br /><br /><img src="../content/images/loadinglive.gif" alt="[loading]" />',
        css: {
            border: 'none',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            padding: '15px',
            backgroundColor: '#fff',
            color: '#000',
            opacity: '1'
        }
    });
}

function close_progress() {
    $.unblockUI();
}

function days_between(from, to) {
    return (to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24);
}

function postplain(url, description, params, success, user_error) {
    var error = typeof user_error != 'undefined'
        ? user_error
        : function (x) {
            update_pending = false;
            alert(x);
        };

    if (description != '') {
        open_progress(description);
    }

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: params,
        timeout: 60000,
        error: ajax_error,
        success: function (data) {
            update_pending = false;
            close_progress();
            success(data);
        }
    });
}

function post(url, description, params, success, user_error) {
    var error = typeof user_error != 'undefined'
        ? user_error
        : function (x) {
            update_pending = false;
            alert(x);
        };

    if (description != '') {
        open_progress(description);
    }

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: params,
        timeout: 60000,
        error: ajax_error,
        success: function (data) {
            update_pending = false;
            close_progress();
            data.Status ? success(data.Data) : error(data.Message);
            //success(data);
        }
    });
}

function ajax_error(request, text, error) {
    update_pending = false;
    close_progress();
    log('request: ' + request.responseText);
    log('text: ' + text);
    log('error: ' + error);
}

function latLng(lat, lng) {
    return new google.maps.LatLng(lat, lng);
}

function createMarker(iconPath, lat, lng, text, size, tooltip) {
    var label = text !== '',
        halfSize = size / 2 | 0,
        base = {
            position: latLng(lat, lng),
            map: map,
            icon: {
                url: iconPath,
                size: new google.maps.Size(size, size),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(halfSize, halfSize)
            }
        };

    if (label) {
        base.labelContent = text;
        base.labelClass = 'maplabel';
        base.labelAnchor = new google.maps.Point(-halfSize, 8);
    }

    if (tooltip) {
        base.title = tooltip;
    }

    return new (label ? MarkerWithLabel : google.maps.Marker)(base);
}

// Vehicle class
function vehicle(values, $tbody, $map) {
    this.update = function (new_values) {

        if (this.Speed > alertSpeedLimit && alertSpeedLimit > 0) {
            var title = this.Reg; //+' overspeeding!';
            var stub = this.Reg + ' overspeeding at ' + this.Speed + 'Km/h on ' + this.Street + ' </br> Time:' + this.timestamp;
            alarms.systemAlert(title, stub);
        }

        for (var v in new_values) {
            this[v] = new_values[v];
        }

        this.update_row();
        this.update_icon();
    };

    this.update_row = function () {
        var self = this;

        var row = {
            group: this.GroupName,
            vehicleCode: this.VehicleCode,
            reg: this.Reg,
            driver: this.Driver,
            street: this.Street,
            town: this.Town,
            lga: this.map3,
            distanceKm: this.DistanceKm,
            speed: this.Speed,
            dir: this.Dir,
            date: this.Date,
            time: this.Time,
            SatellitesUsed: this.SatellitesUsed,
            event: this.Event
        };

        this.visible = true;

        if (!show_lga) {
            delete row.lga;
        }

        if (!show_street) {
            delete row.street;
        }

        this.row.html(make_tds(row))
                .unbind('click')
                .unbind('dblclick')
                .click(function () { self.select(); })
                .dblclick(function () { self.move_to(street_zoom); })
                .disableTextSelect();

        if (this.Event == 'Disconnected') {
            this.row.addClass('discon');
        } else {
            this.row.removeClass('discon');
        }
    }

    this.update_icon = function () {
        var devicetype = this.DeviceType;
        if (devicetype.indexOf("Human Tracker") >= 0) {
            var icon_name = "../Content/Images/humanicon.jpg";
        }
        else {
            var icon_name = imagePath + this.ImageUrl;
        }
        var self = this;

        if (this.marker !== null) {
            this.marker.setMap(null);
        }

        this.marker = createMarker(icon_name, this.Latitude, this.Longitude, this.Reg + '<br>(' + this.VehicleCode + ')', 32);
        var attr = this;
        google.maps.event.addListener(this.marker, 'click', function () {
            onCarClickFunc(attr);
            self.select();
            $('#grid')[0].scrollTo(self.row.position().top - 14);
        });
    }
    this.select = function () {
        $tbody.find('tr').removeClass('sel').find('td').removeClass('sel');
        this.row.addClass('sel').find('td').addClass('sel');
        selected_vehicle = this;
    };

    this.move_to = function (zoom) {
        map.setCenter(latLng(this.Latitude, this.Longitude));
        map.setZoom(zoom);
    };

    this.locate = function () {
        if (this.connected == '0') {
            return alert('Cannot request position of this vehicle because it is disconnected');
        }

        if (!this.can_locate) {
            return;
        }

        var reg = this.Reg;
        set_status('Sending request for location of ' + reg);

        var params = $.param({
            VehicleId: this.VehicleId
        });

        $.ajax({
            url: '/Live/requestLocation',
            type: 'POST',
            dataType: 'json',
            data: params,
            error: ajax_error,
            success: function (data) {
                set_status('Request for location of ' + reg + ' received at server');
            }
        });

        this.can_locate = false;

        setTimeout(
            function (param1) {
                param1.can_locate = true;
            },
            locate_interval, this
        );
    };

    this.startEngine = function () {
        if (this.connected == '0') {
            return alert('Cannot switch Engine on this vehicle because it is disconnected');
        }

        var reg = this.Reg;
        set_status('Sending request for switch Engine on of ' + reg);

        var params = $.param({
            VehicleId: this.VehicleId,
            isSwitchOn: true
        });

        $.ajax({
            url: '/Live/controlEngine',
            type: 'POST',
            dataType: 'json',
            data: params,
            error: ajax_error,
            success: function (data) {
                set_status('Request for switch Engine on of ' + reg + ' received at server');
            }
        });
    };

    this.stopEngine = function () {
        if (this.connected == '0') {
            return alert('Cannot switch Engine off this vehicle because it is disconnected');
        }

        var reg = this.Reg;
        set_status('Sending request for switch Engine off of ' + reg);

        var params = $.param({
            VehicleId: this.VehicleId,
            isSwitchOn: false
        });

        $.ajax({
            url: '/Live/controlEngine',
            type: 'POST',
            dataType: 'json',
            data: params,
            error: ajax_error,
            success: function (data) {
                set_status('Request for switch Engine off of ' + reg + ' received at server');
            }
        });
    };

    this.getLog = function () {
        if (this.connected == '0') {
            return alert('Cannot get Log for this vehicle because it is disconnected');
        }

        var reg = this.Reg;
        set_status('Sending request for switch Engine off of ' + reg);

        var params = $.param({
            VehicleId: this.VehicleId
        });

        $.ajax({
            url: '/Live/requestGetLog',
            type: 'POST',
            dataType: 'json',
            data: params,
            error: ajax_error,
            success: function (data) {
                set_status('Request for get Log of ' + reg + ' received at server');
            }
        });
    };

    this.setVisible = function (x) {
        this.visible = x;

        if (x) {
            this.row.show();
        } else {
            this.row.hide();
        }
    };

    function make_tds(vals) {
        var result = '';
        for (var v in vals) {
            if (vals[v] == null) {
                result += '<td class="' + v + '">&nbsp;</td>';
            } else {
                var dat = (vals[v].toString() == '' ? '&nbsp;' : vals[v].toString());
                result += '<td alt="' + dat + '" title="' + dat + '" class="' + v + '">' + dat + '</td>';
            }
        }
        return result;
    }

    function heading_to_angle_index(heading) {
        return heading == 'NNE' ? 1 :
               heading == 'NE' ? 2 :
               heading == 'ENE' ? 3 :
               heading == 'E' ? 4 :
               heading == 'ESE' ? 5 :
               heading == 'SE' ? 6 :
               heading == 'SSE' ? 7 :
               heading == 'S' ? 8 :
               heading == 'SSW' ? 9 :
               heading == 'SW' ? 10 :
               heading == 'WSW' ? 11 :
               heading == 'W' ? 12 :
               heading == 'WNW' ? 13 :
               heading == 'NW' ? 14 :
               heading == 'NNW' ? 15 : 0;
    }

    this.row = $('<tr />').appendTo($tbody);
    this.marker = null;
    this.can_locate = true;
    this.can_monitor = false;
    this.update(values);

    function onCarClickFunc(attr) {
        var gMarker = this;
        var marker = new google.maps.Marker({
            position: latLng(attr.Latitude, attr.Longitude),
        });
        var infowindow = new google.maps.InfoWindow({
            content: "<p>" + attr.Reg + "<br />Driver: " + attr.Driver +
                        "<br />Speed: " + attr.Speed + " Km/h<br />Town: " + attr.Town +
                        "<br />Street: " + attr.Street + "<br /> Pos: Lon - " + attr.Longitude +
                        ",<br/><space/>Lat - " + attr.Latitude + "</p>"
        });
        infowindow.open(map, marker);
    }
    function infoTemplateContent(attr) {
        var content = "<p>" + attr.Reg + "<br />Driver: " + attr.Driver +
                        "<br />Speed: " + attr.Speed + " Km/h<br />Town: " + attr.Town +
                        "<br />Street: " + attr.Street + "<br /> Pos: Lon - " + attr.Longitude +
                        ",<br/><space/>Lat - " + attr.Latitude + "</p>";
        var smsValue = "Reg: " + attr.Reg + " Driver: " + attr.Driver + " Speed: " + attr.Speed + " Km/h Town: " + attr.Town +
                        " Street: " + attr.Street + " Pos: Lon - " + attr.Longitude + ",Lat - " + attr.Latitude;
        var result = "<div id='classic_infowindow'><div id='carpos_info'>" + content +
                    "</div><div><div  id='divEnterNo'>Enter No:<input type='text' id='send_to_phone' class='iMask' value='2547' />" +
                      "<input id='hiddenSMS' type='hidden' value='" + smsValue + "' /><button type='submit'  onClick='sendSMS()'>Send</button></div></div></div>";
        return result;
    }

}

function create_vehicles(vs) {
    var $tbody = $('#vehicles > tbody');
    var $map = $('#map');

    $tbody.empty();
    $.each(vs, function () {
        vehicles.push(new vehicle(this, $tbody, $map));
        if (isRecovery()) {
            vehiclesMonitoring();
        }
        checkboxgroup()
    });

    $('#vehicles').tablesorter({
        widgets: ['zebra'],
        fixed_headers: true,
        header_fixpoint: '#footer',
        sortList: [[2, 0]]
    });
    initialise_groups();

    distancerptvehicles(vehicles);
    setup_scrolling();
    //console.log('sdssd');
    select_first_visible_grid_row();
}

function select_first_visible_grid_row() {
    $('#vehicles > tbody > tr :visible').each(function () {
        $(this).click();
        return false;
    });
}

function checkboxgroup() {
    $("input:checkbox").click(function () {
        if ($(this).is(":checked")) {
            var group = "input:checkbox[name='" + $(this).attr("name") + "']";
            $(group).attr("checked", false);
            $(this).attr("checked", true);
        } else {
            $(this).attr("checked", false);
        }
    });
}



function setup_scrolling() {
    var vehicles_height = $('#vehicles').height();
    var footer_height = $('#footer').height();

    var less = vehicles_height < footer_height;
    $('#grid div').css('margin-top', (less ? footer_height - vehicles_height : 0) + 'px');

    $('#grid').jScrollPane({
        showArrows: true,
        scrollbarWidth: 15,
        arrowSize: 16
    });
}
function set_sizes() {
    var grid_height = Math.min($('#vehicles').height(), gridtableheight);

    $('#footer').css('height', grid_height + 'px');
    $('#main').css('bottom', grid_height + 'px');

    var main_height = $('#main').height();

    $('#map').height(main_height);
    $('#tree').height(main_height - 20);

    var routes_height = Math.min(main_height - 45, 350);
    $('#routes').height(routes_height);
    $('#routes-tree').height(routes_height - $('#routes-tree').position().top);

    var monitor_height = Math.min($('#main').height() - 45, 350);
    $('#monitor-vehicles').height(monitor_height);
    if (isRecovery()) {
        $('#vehicles-to-monitor').height(monitor_height - $('#vehicles-to-monitor').position().top);
    }

    if (typeof map != 'undefined') {
        google.maps.event.trigger(map, 'resize');
    }

    $('.jScrollPaneContainer').width($(window).width());
    $('#vehicles, #vehicles_').width($(window).width()/* - 16*/);
    $('#vehicles, #vehicles_, .jScrollPaneContainer').height(grid_height/* - 16*/);
    setup_scrolling();
}

function pad2digits(value) {
    return value < 10 ? '0' + value : value;
}

function set_status(msg) {
    var hours = pad2digits(new Date().getHours());
    var mins = pad2digits(new Date().getMinutes());
    var secs = pad2digits(new Date().getSeconds());
    $('#status').html(hours + ':' + mins + ':' + secs + ' - ' + msg);
}

// Buttons
function zoom_to_all() {
    var bounds = new google.maps.LatLngBounds();

    $.each(vehicles, function () {
        if (this.visible) {
            bounds.extend(latLng(this.Latitude, this.Longitude));
        }
    });

    map.fitBounds(bounds);
}

function zoom_To_Level(zoom) {
    if (selected_vehicle === null) {
        map.setZoom(zoom);
    } else {
        map.setCenter(latLng(selected_vehicle.Latitude, selected_vehicle.Longitude));
        map.setZoom(zoom);
    }
}

function zoom_street() {
    zoom_To_Level(street_zoom);
}

function zoom_town() {
    zoom_To_Level(town_zoom);
}

function closeAll() {
    routesRemoveRoute();
    pointsRemovePoints();
    $.each(vehicles, function () {
        this.can_monitor = false;
    });
    $('#reports, #routes, #points, #monitor-vehicles,#distancetravelled-vehicles').hide();
    zones.close();
}

// Reports
function report_open() {
    closeAll();
    $('#reports').show();
}

function report_initialise() {
    $('#reports .close').click(closeAll);
    $('#report-create').click(report_create);
    $('#report-from').datepicker({ defaultDate: -7 }).datepicker('setDate', -7);
    $('#report-to').datepicker().datepicker('setDate', new Date());
    $('#report-from-time').timePicker();
    $('#report-to-time').timePicker();
    $('#report-from-time').val('00:00');
    $('#report-to-time').val('23:59');
}

function report_create() {
    if (typeof selected_vehicle == 'undefined' || selected_vehicle == null) {
        return alert('No vehicle selected');
    }

    var type = $('#report-type').val();
    var format = $('#report-format').val();
    var from = $('#report-from').datepicker('getDate');
    var to = $('#report-to').datepicker('getDate');

    var period = days_between(from, to);
    var max = type == 'detail' ? detail_max_period :
              type == 'travel' ? travel_max_period : stop_max_period;

    if (period > max) {
        return alert('The maximum period for this report type is ' + max + ' days');
    }

    from = $('#report-from').datepicker('getDate');
    to = $('#report-to').datepicker('getDate');
    $.datepicker.formatDate('yyyy-mm-dd', from);
    $.datepicker.formatDate('yyyy-mm-dd', to);

    var fdate = from.getFullYear() + ' - ' + (from.getMonth() + 1) + ' - ' + from.getDate();
    var tdate = to.getFullYear() + ' - ' + (to.getMonth() + 1) + ' - ' + to.getDate();
    fdate = fdate + ' ' + $('#report-from-time').val();
    tdate = tdate + ' ' + $('#report-to-time').val();

    //console.log(fdate);
    //console.log(tdate);

    var params = $.param({
        type: $('#report-type').val(),
        //format: $('#report-format').val().replace(/ /g, ""),
        startDate: fdate,
        endDate: tdate,
        regNo: selected_vehicle.Reg
    });

    window.open('TrackingReports/?' + params,
                'win',
                'toolbar=1,scrollbars=1,location=0,statusbar=1,' +
                'menubar=1,resizable=1,left=20,top=20,' +
                'width=' + ($(window).width() - 40) + ',' +
                'height=' + ($(window).height() - 20));
}
function report_page(report) {
    //var dir = '/g-track/Reports/';
    var dir = '/Reports/';
    var page = report == 'detail' ? 'DetailedReport.aspx?' :
                   report == 'travel' ? 'JourneyReport.aspx?' :
                   report == 'travel_Move' ? 'JourneyReport.aspx?' :
                   report == 'stop' ? 'StopReport.aspx?' : 'Error.aspx';
    return dir + page;
}
//Monitor
function monitor_open() {
    closeAll();
    var monitor_height = Math.min($('#main').height() - 45, 350);
    $('#monitor-vehicles').height(monitor_height).show();
    if (isRecovery()) {
        $('#vehicles-to-monitor').height(monitor_height - $('#vehicles-to-monitor').position().top);
    }
}
function distancetravelledrpt_open() {
    closeAll();
    var distancetravelled_height = Math.min($('#main').height() - 45, 350);
    $('#distancetravelled-vehicles').height(distancetravelled_height).show();
    if (isRecovery()) {
        $('#vehicles-to-monitor').height(distancetravelled_height - $('#report-vehicles-distancetravelled').position().top);
    }
}
// Routes
function routes_open() {
    closeAll();
    var routes_height = Math.min($('#main').height() - 45, 350);
    $('#routes').height(routes_height).show();
    $('#routes-tree').height(routes_height - $('#routes-tree').position().top).empty();
}

function routes_initialise() {
    $('#routes .close').click(closeAll);
    $('#routes-retrieve').click(routes_retrieve);
    $('#routes-from').datepicker({ defaultDate: -7 }).datepicker('setDate', -7);
    $('#routes-to').datepicker().datepicker('setDate', new Date());
    $('#routes-from-time').timePicker();
    $('#routes-to-time').timePicker();
    $('#routes-from-time').val('00:00');
    $('#routes-to-time').val('23:59');
}
function monitor_initialise() {
    $('#monitor-vehicles .close').click(closeAll);
}
function distancerpt_initialise() {
    $('#distancetravelled-vehicles .close').click(closeAll);
    $('#generate-distrpt').click(report_distance);
    $('#distrpt-from').datepicker({ defaultDate: -7 }).datepicker('setDate', -7);
    $('#distrpt-to').datepicker().datepicker('setDate', new Date());
    $('#distrpt-from-time').timePicker();
    $('#distrpt-to-time').timePicker();
    $('#distrpt-from-time').val('00:00');
    $('#distrpt-to-time').val('23:59');
}
function submitFn(event) {
    var boxes = document.getElementsByClassName('chkReg');
    var checked = [];
    for (var i = 0; boxes[i]; ++i) {
        if (boxes[i].checked) {
            checked.push(boxes[i].name);
        }
    }

    var checkedStr = checked.join();

    document.getElementById('checkbox_str').value = checkedStr;
    form.submit();

    return false;
}
function report_distance() {
    //if (typeof selected_vehicle == 'undefined' || selected_vehicle == null) {
    //    return alert('No vehicle selected');
    //}
    var boxes = document.getElementsByClassName('chkReg');
    var checked = [];
    for (var i = 0; boxes[i]; ++i) {
        if (boxes[i].checked) {
            checked.push(boxes[i].value);
        }
    }

    var vehicleDetailId = checked.join();

    var from = $('#distrpt-from').datepicker('getDate');
    var to = $('#distrpt-to').datepicker('getDate');


    from = $('#distrpt-from').datepicker('getDate');
    to = $('#distrpt-to').datepicker('getDate');
    $.datepicker.formatDate('yyyy-mm-dd', from);
    $.datepicker.formatDate('yyyy-mm-dd', to);

    var fdate = from.getFullYear() + ' - ' + (from.getMonth() + 1) + ' - ' + from.getDate();
    var tdate = to.getFullYear() + ' - ' + (to.getMonth() + 1) + ' - ' + to.getDate();
    fdate = fdate + ' ' + $('#distrpt-from-time').val();
    tdate = tdate + ' ' + $('#distrpt-to-time').val();

    //console.log(fdate);
    //console.log(tdate);

    var params = $.param({
        type: 'distance',
        startDate: fdate,
        endDate: tdate,
        vehicleDetailId: vehicleDetailId
    });

    window.open('TrackingReports/?' + params,
                'win',
                'toolbar=1,scrollbars=1,location=0,statusbar=1,' +
                'menubar=1,resizable=1,left=20,top=20,' +
                'width=' + ($(window).width() - 40) + ',' +
                'height=' + ($(window).height() - 20));
}
function routes_retrieve() {
    if (typeof selected_vehicle == 'undefined' || selected_vehicle == null) {
        return alert('No vehicle selected');
    }

    $('#routes-tree').empty();
    routesRemoveRoute();

    var from = $('#routes-from').datepicker('getDate');
    var to = $('#routes-to').datepicker('getDate');
    var period = days_between(from, to);
    var max = 31;

    if (period > max) {
        alert('The maximum period for this report type is ' + max + ' days');
        return;
    }

    from = $('#routes-from').datepicker(pickerOpts).val().replace(/ /g, "");
    to = $('#routes-to').datepicker(pickerOpts).val().replace(/ /g, "");

    from = $('#routes-from').datepicker('getDate');
    to = $('#routes-to').datepicker('getDate');
    $.datepicker.formatDate('yyyy-mm-dd', from);
    $.datepicker.formatDate('yyyy-mm-dd', to);

    var fdate = from.getFullYear() + ' - ' + (from.getMonth() + 1) + ' - ' + from.getDate();
    var tdate = to.getFullYear() + ' - ' + (to.getMonth() + 1) + ' - ' + to.getDate();
    fdate = fdate + ' ' + $('#routes-from-time').val();
    tdate = tdate + ' ' + $('#routes-to-time').val();

    open_progress('Retrieving route list');

    var params = $.param({
        Registration: selected_vehicle.Reg,
        startDate: fdate,
        endDate: tdate
    });

    $.ajax({
        url: '/Live/getRouteList', //'@Url.Action("getRouteList", "Live")',
        type: 'POST',
        dataType: 'json',
        data: params,
        error: ajax_error,
        success: function (data) {
            if (data.search != null && data.search(/Error/i) > -1) {
                update_pending = true;
                close_progress();
            }
            else {
                if (data != null) {
                    update_pending = false;
                    close_progress();
                    routes_create_tree(data);
                }
            }
        }
    });
}

function routes_create_tree(days) {
    function div() { return $(document.createElement('div')); }

    var $tree = $('#routes-tree').empty();

    if (days.length == 0) {
        $tree.html('No data over this period');
        return;
    }

    $.each(days, function () {
        var routes = this.data,
			date = this.date,
		    Registration = selected_vehicle.Reg;

        if (routes.length == 0) {
            return true; // continue
        }
        var startDate = routes[0].start_datetime,
            endDate = routes[routes.length - 1].end_datetime;

        var $element = div().addClass('element');

        div()
			.addClass('header')
            .html(date + ' (' + this.dow + ')')
            .appendTo($element)
            .click(function () {
                $tree.find('.subelement').hide();
                $element.find('.subelement').show().end();
            })
            .dblclick(function () {
                var d = date.substring(6) + date.substring(3, 5) + date.substring(0, 2);

                $('.route_selected').removeClass('route_selected');
                $(this).addClass('route_selected');
                routes_show_route(endDate, endDate, Registration);
            })
			.disableTextSelect();

        $.each(routes, function () {
            var route = this;
            var hint = this.start_town + ' to ' +
                       this.end_town + ' (' +
                       'Click to plot the route on the map)';
            //                       , approx. ' +
            //                       (this.end_id - this.start_id) + ' events)';
            var text = this.start_time.substr(0, 5) + ' - ' +
                       this.end_time.substr(0, 5);

            div().addClass('subelement')
                 .hide()
                 .html(text)
                 .attr('title', hint)
                 .disableTextSelect()
                 .appendTo($element)
                 .hover(function () { $(this).addClass('route_hover'); },
                        function () { $(this).removeClass('route_hover'); })
                 .click(function () {
                     $('.route_selected').removeClass('route_selected');
                     $(this).addClass('route_selected');
                     routes_show_route(route.start_datetime, route.end_datetime, Registration);
                 });
        });

        $tree.append($element);
    });

    $tree.find('> .element:first > .subelement').show();
}

function routes_show_route(startDate, endDate, Registration) {
    var params = $.param({
        Registration: Registration,
        startDate: startDate,
        endDate: endDate
    });

    $.ajax({
        url: '/Live/getPlotRouteData', //'@Url.Action("getRouteList", "Live")',
        type: 'POST',
        dataType: 'json',
        data: params,
        error: ajax_error,
        success: function (data) {
            if (data.search != null && data.search(/Error/i) > -1) {
                console.log(data);
            }
            else {
                if (data != null) {
                    routes_plot_route(data);
                }
            }
        }
    });
}

var routeIcons = [],
    routePolyline = null;

function routes_plot_route(route) {
    var plotLines = $('#routes-plot-lines').attr('checked') != '',
        polyPoints = [];
    if (route.length == 0) {
        return;
    }

    routesRemoveRoute();

    $.each(route, function (i) {
        var angle = Math.floor((parseInt(this.Dir, 10) + 12) / 22.5) % 16,
            text = this.GPSTime + ' - ' + this.Speed + ' Kph';
        icon_name = imagePath;

        if (this.is_start === '1' || i == 0) {
            icon_name += '0a0_start.gif';
        } else if (this.is_stop === '1' || i == route.length - 1) {
            icon_name += 'f00_stop.gif';
        } else {
            icon_name += 'ff0_' + angle + '.gif';
        }


        routeIcons.push(routes_create_icon(this.Latitude, this.Longitude, text, icon_name));

        if (plotLines) {
            polyPoints.push(latLng(this.Latitude, this.Longitude));
        }
    });

    if (plotLines) {
        routePolyline = new google.maps.Polyline({
            path: polyPoints,
            strokeColor: '#ff0',
            strokeWeight: 7,
            strokeOpacity: 1,
            map: map
        });
    }

    routes_centre_route();
}

function routesRemoveRoute() {
    $.each(routeIcons, function () { this.setMap(null); });
    routeIcons = [];

    if (routePolyline) {
        routePolyline.setMap(null);
        routePolyline = null;
    }
}

function routes_centre_route() {
    var bounds = new google.maps.LatLngBounds();

    $.each(routeIcons, function () {
        bounds.extend(this.getPosition());
    });

    map.fitBounds(bounds);
}

function routes_create_icon(lat, lon, text, icon_name) {
    return createMarker(icon_name, lat, lon, '', 17, text);
}

// Filter
function initialise_groups() {
    var $filter = $('#group-filter').empty(),
        //groups = { All: '' },
        groupCount = -1;

    $.each(vehicles, function () {
        if (this.GroupName != null) {
            var _group = this.GroupName.split(',');
            $.each(_group, function () {
                vehicle_groups[this != null ? this : ''] = '';
            });
        }
    });

    for (var x in vehicle_groups) {
        ++groupCount;
    }

    //if (groupCount === 1) {
    //    return;
    //}

    for (var group in vehicle_groups) {
        $('<option />').appendTo($filter).html(group).val(group);
    }

    $('#group-selection').show();
}
function sendSMS() {
    var Message = $('#hiddenSMS').val();
    var Contacts = $('#send_to_phone').val();

    if (Contacts == '') {
        return alert('no phone number to send to');
    }
    set_status('Sending vehicle message: ');

    var params = $.param({
        Contacts: Contacts,
        Message: Message
    });

    $.ajax({
        url: '/Live/sendSMS', //'@Url.Action("getRouteList", "Live")',
        type: 'POST',
        dataType: 'json',
        data: params,
        error: ajax_error,
        success: function (data) {
            set_status('Request for location received at server ' + data);
        }
    });
    $('#send_to_phone').val('');
}

function filter_vehicle_grid() {
    var group_filter = $('#group-filter').val();
    group_filter = group_filter == '' ? null : group_filter;

    $.each(vehicles, function () {
        var _groupname = this.GroupName.split(',');
        var self = this;
        $.each(_groupname, function () {
            if (this == group_filter || group_filter == 'All') {
                self.setVisible(true);
                self.update_icon();
                return false;
            } else {
                self.setVisible(false);
                if (self.marker !== null) {
                    self.marker.setMap(null);
                }
                self.marker = null;
            }
        });
    });

    setup_scrolling();
    select_first_visible_grid_row();
}

//function filter_vehicle_grid_reg() {
//    var reg_filter = $('#reg_filter').val();
//    reg_filter = reg_filter == '' ? null : reg_filter.toUpperCase().split(',');

//    $.each(vehicles, function () {
//        var veh = this;
//        $.each(reg_filter, function (index, item) {
//            item = item == '' ? null : item;
//            if (veh.Reg.toUpperCase().indexOf(item) > -1 ||
//                veh.VehicleCode.toUpperCase().indexOf(item) > -1 ||
//                item == null) {
//                veh.setVisible(true);
//                veh.update_icon();
//            } else {
//                veh.row.hide();
//                if (veh.marker !== null)
//                    veh.marker.setMap(null);
//                veh.marker = null;
//            }
//        })
//    });

//    setup_scrolling();
//    select_first_visible_grid_row();
//}

function filter_vehicle_grid_reg() {
    var reg_filter = $('#reg_filter').val();
    reg_filter = reg_filter == '' ? null : reg_filter.toUpperCase();

    $.each(vehicles, function () {
        if (this.Reg.toUpperCase().indexOf(reg_filter) > -1 ||
            this.VehicleCode.toUpperCase().indexOf(reg_filter) > -1 ||
            reg_filter == null) {
            this.setVisible(true);
            this.update_icon();
        } else {
            this.row.hide();
            if (this.marker !== null)
                this.marker.setMap(null);
            this.marker = null;
        }
    });

    setup_scrolling();
    select_first_visible_grid_row();
}

// Miscellaneous
function initial_load(data) {
    update_pending = true;
    map = new google.maps.Map(document.getElementById('map'), {
        center: latLng(0, 10),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.HYBRID
    });

    create_vehicles(data);
    set_sizes();
    zoom_to_all();
    update_pending = false;

    // initialize widget on a container, passing in all the defaults.
    // the defaults will apply to any notification created within this
    // container, but can be overwritten on notification-by-notification
    // basis.
    //$container = $("#container").notify();
}


// ----------------------------------------------------------------------------
var selectedPointId = -1,
    selectedPointGroup = '',
    pointPolygon = null,
    points = [],
    visiblePoints = {};

function pointsOpen() {
    closeAll();
    $('#points').show();
    $('#points-tree').empty();

    post(
        'get_points.php',
        'Retrieving custom points',
        $.param({ tab_id: tab_id }),
        pointsCreateTree
    );
}

function pointsInitialise() {
    $('#points .close').click(closeAll);
    $('#points-save').click(pointsSave);
    $('#points-del').click(pointsDelete);
    $('#points-show').click(pointsShow);
}

function pointsCreateTree(data) {
    points = [];

    if (data.length > 0 && (selectedPointId == -1 || selectedPointGroup == '')) {
        pointsSelect(data[0]);
    }

    var grouped = {};

    $.each(data, function () {
        var group = this.point_group;

        if (typeof grouped[group] === 'undefined') {
            grouped[group] = [];
        }

        grouped[group].push(this);
    });

    var $tree = $('#points-tree').empty();

    $.each(grouped, function () {
        var $element = div().addClass('element');

        div().addClass('header')
             .html(this[0].point_group)
             .appendTo($element)
             .click(function () {
                 $tree.find('.subelement').hide();
                 $element.find('.subelement').show().end();
             });

        $.each(this, function () {
            var point = this,
                $x = div();

            $x.addClass('subelement')
              .html(this.point_name)
              .disableTextSelect()
              .appendTo($element)
              .click(function () {
                  $('.selected').removeClass('selected');
                  $(this).addClass('selected');
                  pointsSelect(point);
              }).dblclick(function () {
                  pointsShow();
              });

            if (this.id == selectedPointId) {
                $x.addClass('selected');
            }

            if (this.point_group != selectedPointGroup) {
                $x.hide();
            }

            points.push(this);
        });

        $tree.append($element);
    });

    if ($tree.html() === '') {
        $tree.html('Currently there are no custom points. To add a custom point left-click on the map.');
    }

    pointsUpdateVisiblePoints();
}

function pointsAdd(lat, lon) {
    var point = {
        id: -1,
        point_name: 'New point',
        point_group: 'Points',
        lat: lat,
        lon: lon,
        radius: 50,
        enter_address: '',
        exit_address: ''
    };

    post(
        'add_point.php',
        'Adding custom point',
        $.param({
            tab_id: tab_id,
            lat: lat,
            lon: lon,
            name: point.point_name,
            group: point.point_group,
            radius: point.radius
        }),
        function (data) {
            point.id = data.id;
            pointsSelect(point);
            $('#points-name').focus();
            pointsCreateTree(data.points);
        }
    );
}

function allDigits(x) {
    for (var i = 0; i != x.length; ++i) {
        if (x.charAt(i) < '0' || x.charAt(i) > '9') {
            return false;
        }
    }

    return true;
}

function pointsSave() {
    var $group = $('#points-group'),
        $radius = $('#points-radius'),
        radius = parseInt($radius.val(), 10);

    if ($group.val() == '') {
        alert('Please enter a group.');
        $group.focus();
        return;
    }

    if (!allDigits($radius.val())) {
        alert('Invalid radius.');
        $radius.focus();
        return;
    }

    if (radius < 5) {
        alert('Radius cannot be less than 5 metres.');
        $radius.focus();
        return;
    }

    if (radius > 100000) {
        alert('Radius cannot be greater than 100 kilometres.');
        $radius.focus();
        return;
    }

    post(
        'save_point.php',
        'Saving custom point',
        $.param({
            tab_id: tab_id,
            point_id: selectedPointId,
            lat: $('#points-lat').val(),
            lon: $('#points-lon').val(),
            name: $('#points-name').val(),
            group: $('#points-group').val(),
            radius: $('#points-radius').val(),
            enter: $('#points-alert-enter').attr('checked') ? 1 : 0,
            exit: $('#points-alert-exit').attr('checked') ? 1 : 0,
            all_users: $('#points-all-users').attr('checked') ? 1 : 0,
            enter_address: $('#points-email-enter').val(),
            exit_address: $('#points-email-exit').val()
        }),
        function (data) {
            selectedPointGroup = $('#points-group').val();
            pointsCreateTree(data);
            pointsUpdatePoint(selectedPointId);
        }
    );
}

function pointsDelete() {
    if (selectedPointId === -1) {
        return;
    }

    if (!confirm('Are you sure you wish to delete ' + $('#points-name').val() + '?')) {
        return;
    }

    post(
        'delete_point.php',
        'Deleting custom point',
        $.param({
            tab_id: tab_id,
            point_id: selectedPointId
        }),
        function (data) {
            pointsRemovePoint(selectedPointId);

            if (data.length > 0) {
                pointsSelect(data[0]);
            }

            pointsCreateTree(data);

            if (data.length === 0) {
                selectedPointId = -1;
                selectedPointGroup = '';
                $('#points input').val('').attr('checked', '');
            }
        }
    );
}

function pointsShow() {
    var lat = parseFloat($('#points-lat').val()),
        lon = parseFloat($('#points-lon').val()),
        radius = parseInt($('#points-radius').val());

    if (selectedPointId == -1) {
        return;
    }

    pointsRemovePointArea();

    pointPolygon = createPointArea(lat, lon, radius);
    map.addOverlay(pointPolygon);
    map.setCenter(new GLatLng(lat, lon), 17);
}

function pointsSelect(point) {
    selectedPointId = point.id;
    selectedPointGroup = point.point_group;
    $('#points-name').val(point.point_name);
    $('#points-group').val(point.point_group);
    $('#points-lat').val(parseFloat(point.lat).toFixed(5));
    $('#points-lon').val(parseFloat(point.lon).toFixed(5));
    $('#points-radius').val(point.radius);
    $('#points-alert-enter').attr('checked', point.alert_enter == 1 ? 'checked' : '');
    $('#points-alert-exit').attr('checked', point.alert_exit == 1 ? 'checked' : '');
    $('#points-all-users').attr('checked', point.used_by_all == 1 ? 'checked' : '');
    $('#points-email-enter').val(point.enter_address);
    $('#points-email-exit').val(point.exit_address);
}

function createPointArea(lat, lon, radiusInMetres) {
    var radius = metresToLatLngApprox(radiusInMetres),
        points = [],
        angleRad;

    for (var i = 0; i != 180; ++i) {
        angleRad = i * 2 * (Math.PI / 180);
        points.push(new GLatLng(lat + Math.sin(angleRad) * radius, lon + Math.cos(angleRad) * radius));
    }
    points.push(points[0]);

    return new GPolygon(points, '#0000ff', 1, 1, '#0000ff', 0.3);
}

function pointsRemovePointArea() {
    if (pointPolygon !== null) {
        pointPolygon.remove();
        pointPolygon = null;
    }
}

function pointsRemovePoints() {
    pointsRemovePointArea();
    $.each(visiblePoints, function () { this.remove(); });
    visiblePoints = {};
}

function pointsRemovePoint(pointId) {
    var id = 'l' + pointId;
    visiblePoints[id].remove();
    delete visiblePoints[id];
}

function pointsUpdatePoint(pointId) {
    var id = 'l' + pointId,
        point = null;

    if (typeof visiblePoints[id] != 'undefined') {
        pointsRemovePoint(pointId);
    }

    $.each(points, function () {
        if (this.id == pointId) {
            point = this;
        }
    });

    if (point !== null) {
        visiblePoints[id] = createMarker(iconPath + 'misc/House.gif', point.lat, point.lon, point.point_name, 16);
    }
}

function pointsUpdateVisiblePoints() {
    var bounds = map.getBounds();

    $.each(points, function () {
        var id = 'l' + this.id,
            visible = bounds.containsLatLng(new GLatLng(this.lat, this.lon)),
            onMap = typeof visiblePoints[id] != 'undefined';

        if (visible && !onMap) {
            pointsUpdatePoint(this.id);
        }

        if (!visible && onMap) {
            pointsRemovePoint(this.id);
        }
    });
}

// ----------------------------------------------------------------------------
var zones = (function () {
    var maximumPoints = 16,
        polygon = null,
        creating = false,
        drawingText = 'Click on the map to define a polyzone. A polyzone may have at most sixteen points. Click on the first point to automatically close the zone. Once created you will have the option to edit the zone.',
        editingText = 'You may now edit the polygon. Click \'Finish\' when satisified.',
        selectedPoint = null;

    function initialise() {
        $('#zones .close').click(close);
        $('#zones-create').click(create);
        $('#zones-save').click(save);
        $('#zones-del').click(del);
        $('#zones-show').click(show);
        initialiseCreator();
    }

    function open() {
        closeAll();
        $('#zones').show();
        loadZones();
    }

    function close() {
        closeCreator();
        $('#zones').hide();
    }

    function create() {
        openCreator();
        $('#zones').hide();
    }

    function enterType(point) {
        return point.enter_alert === '1' ? 'alert' :
               point.enter_warning === '1' ? 'warn' : '';
    }

    function exitType(point) {
        return point.exit_alert === '1' ? 'alert' :
               point.exit_warning === '1' ? 'warn' : '';
    }

    function clearzone() {
        $('.zones-groups').removeAttr('checked');

        $('#zones-name').val(null);
        $('#zones-geofenceactive').attr('checked', null);
        $('#zones-Isgeofencespeed').attr('checked', null);

        $('#zones-reportonexitfreq').val(null);
        $('#zones-reportonexit').attr('checked', null);

        $('#zones-Speed').val(null);
        $('#zones-reportonexitnoofalerts').val(null);

        $('#zones-reportonentry').attr('checked', null);
        $('#zones-reportonentryfreq').val(null);
        $('#zones-reportonentrynoofalerts').val(null);

        $('#zones-email').val(null);
        $('#zones-phone').val(null);
    }

    function select(point) {
        selectedPoint = point;
        $('.zones-groups').removeAttr('checked');

        $('#zones-name').val(point.GeoFenceName);
        $('#zones-geofenceactive').attr('checked', point.Active);
        $('#zones-Isgeofencespeed').attr('checked', point.IsSpeedGeoFence);

        $('#zones-reportonexit').attr('checked', point.ReportOnExit);
        $('#zones-reportonexitfreq').val(point.FreqInMin_Exit);
        $('#zones-reportonexitnoofalerts').val(point.NoOfAlerts_Exit);

        $('#zones-reportonentry').attr('checked', point.ReportOnEntry);
        $('#zones-reportonentryfreq').val(point.FreqInMin_Entry);
        $('#zones-reportonentrynoofalerts').val(point.NoOfAlerts_Entry);

        $('#zones-email').val(point.Email);
        $('#zones-Speed').val(point.Speed);
        $('#zones-phone').val(point.PhoneNumber);

        if ($('#zones-Isgeofencespeed').is(':checked')) {
            document.getElementById('zone-frequency').style.display = 'none';
            document.getElementById('zone-speed').style.display = 'block';
        }
        else {
            document.getElementById('zone-frequency').style.display = 'block';
            document.getElementById('zone-speed').style.display = 'none';
        }

        var grouIds = point.GroupIds != null ? point.GroupIds.split(',') : [];
        $.each(grouIds, function () {
            $('.zones-groups[value=' + this.valueOf() + ']').attr('checked', true);
        });
    }

    function show() {
        var point = selectedPoint,
            points = [],
            bounds = new google.maps.LatLngBounds();
        i = 0;

        if (point === null) {
            return;
        }
        var pointArray = point.Geom.split('],[');

        for (; i != pointArray.length; ++i) {
            var unitPoint = pointArray[i].split(',');
            points.push(new latLng(unitPoint[0], unitPoint[1]));
        }

        createPolygon(points);

        for (i = 0; i < points.length; i++) {
            bounds.extend(points[i]);
        }

        map.fitBounds(bounds);
    }

    function save() {
        if (selectedPoint === null) {
            return;
        }

        post(
            '/Live/updateGeofence',
            'Saving polyzone',
            $.param({
                id: selectedPoint.GeoFenceId,
                groupIds: $('#zones-groups:checked').map(function (_, el) { return $(el).val() }).get().toString(),
                name: $('#zones-name').val(),
                speed: $('#zones-Speed').val(),
                geofenceactive: $('#zones-geofenceactive').is(":checked"),
                reportonexit: $('#zones-reportonexit').is(":checked"),
                Isgeofencespeed: $('#zones-Isgeofencespeed').is(":checked"),
                reportonexitfreq: $('#zones-reportonexitfreq').val(),
                reportonexitnoofalerts: $('#zones-reportonexitnoofalerts').val(),
                reportonentry: $('#zones-reportonentry').is(":checked"),
                reportonentryfreq: $('#zones-reportonentryfreq').val(),
                reportonentrynoofalerts: $('#zones-reportonentrynoofalerts').val(),
                email: $('#zones-email').val(),
                phone: $('#zones-phone').val()
            }),
            function (results) {
                createTree(results.Data);
            }
        );
    }

    function del() {
        if (selectedPoint === null || !confirm('Are you sure you want to delete \'' + selectedPoint.GeoFenceName + '\'?')) {
            return;
        }

        removePolygon();
        post(
            '/Live/deleteGeofence',
            'Deleting polyzone',
            $.param({
                id: selectedPoint.GeoFenceId
            }),
            function (results) {
                createTree(results.Data);
            }
        );
    }

    function loadZones() {
        post(
            '/Live/getGeofences',
            'Retrieving polyzones',
            null,
            createTree
        );
    }

    function createTree(zones) {
        var $list = $('#zones-list').empty();
        clearzone();

        post(
            '/Live/getAllgroups',
            'Retrieving Groups',
            null,
            createGroups
        );

        $.each(zones, function () {
            var $elem = $('<div/>'),
                point = this;

            function onClick() {
                $('.selected').removeClass('selected');
                $(this).addClass('selected');
                select(point);
            }

            $elem.text(this.GeoFenceName)
                .attr("title", "Double Click to view Geofence " + this.GeoFenceName)
                .click(onClick)
                .dblclick(show)
                .disableTextSelect()
                .appendTo($list);
        });
    }

    function createGroups(groups) {
        var $list = $('#zones-group').empty();

        var htmldata = "<table><tr>", count = 1;
        $.each(groups, function () {
            count += 1;
            htmldata += (count % 2 == 0 ? "</tr> <tr>" : "");
            htmldata += "<td><input type='checkbox' class='zones-groups' id='zones-groups' value='" + this.GroupId + "'/> " + this.GroupName +
             "</td>";
        });
        htmldata += "</tr></table>";
        $list.html(htmldata);
    }

    // Creator
    function initialiseCreator() {
        $('#zone-creator .close').click(closeCreator);
        $('#zone-creator-finish').click(finish);
    }

    function openCreator() {
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingControl: false,
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            polygonOptions: {
                strokeColor: '#00f',
                strokeWeight: 1,
                strokeOpacity: 1,
                fillColor: '#00f',
                fillOpacity: 0.3,
                clickable: false,
                editable: true
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'polygoncomplete', onDrawingFinish);

        creating = true;
        $('#zone-creator p').text(drawingText);
        $('#zone-creator-finish').hide();
        $('#zone-creator').show();
    }

    function closeCreator() {
        if (creating && polygon !== null && polygon.getVertexCount() > 2) {
            if (!confirm('Are you sure you want to cancel?')) {
                return;
            }
        }

        creating = false;
        removePolygon();
        $('#zone-creator').hide();
        $('#zones').show();
    }

    function finish() {
        var name = prompt('Please enter the name of this polyzone', ''),
            path = polygon.getPath(),
            points = path.getLength(),
            data = {
                tab_id: tab_id,
                name: name === '' ? 'New polyzone' : name,
                points: points
            };

        if (points >= maximumPoints) {
            alert('Sorry, a polygon cannot have more than 16 points.');
            stopCreation();
            return;
        }


        var geoValues = "";

        for (i = 0; i != points; ++i) {
            if (geoValues != "") {
                geoValues = geoValues + ",";
            }
            geoValues = geoValues + path.getAt(i).lat() + ' ' + path.getAt(i).lng();
        }

        data['points'] = geoValues + "," + path.getAt(0).lat() + ' ' + path.getAt(0).lng();

        post(
            '/Live/addGeofence',
            'Adding polyzone.',
            $.param(data),
            function () {
                loadZones();
            }
        );

        creating = false;
        removePolygon();
        $('#zone-creator').hide();
        $('#zones').show();
    }

    function onDrawingFinish(poly) {
        removePolygon();
        polygon = poly;

        if (polygon.getPath().getLength() >= maximumPoints) {
            alert('Sorry, a polygon cannot have more than 16 points.');
            stopCreation();
            return;
        }

        drawingManager.setMap(null);
        drawingManager = null;

        $('#zone-creator p').text(editingText);
        $('#zone-creator-finish').show();
    }

    function createPolygon(points) {
        removePolygon();

        polygon = new google.maps.Polygon({
            paths: points,
            strokeColor: '#00f',
            strokeWeight: 3,
            strokeOpacity: 1,
            fillColor: '#00f',
            fillOpacity: 0.5,
            map: map
        });
    }

    function removePolygon() {
        if (polygon !== null) {
            polygon.setMap(null);
            polygon = null;
        }
    }

    return {
        initialise: initialise,
        open: open,
        close: close
    };
}());


var alarms = (function () {
    var warningsCloseTimer = null,
        warningsCloseTimeMs = 30000;

    function initialise() {
        $('#alarms-close').click(closeAlerts);
        $('#warnings-close').click(closeWarnings);
    }

    function systemWarning(reg, data) {
        var values = data.split('@#@'),
            eventNumber = parseInt(values[0], 10),
            eventText = values[1],
            date = '<span class="date">' + values[2] + '</span>',
            time = '<span class="time">' + values[3] + '</span>',
            street = values[4],
            town = values[5],
            region = values[6],
            lat = values[7],
            lng = values[8],
            text =
                date + ' ' + time + ': ' +
                '<span class="event">' + eventText + '</span>' +
                ' for <span class="reg">' + reg + '</span> at ' +
                street + ', ' + town + ', ' + region + '.';

        showWarningMessage(text);
    }

    function systemAlert(reg, data) {
        var text = data;
        showAlertMessage(reg, text);
    }

    function checkForCrossings(oldPointId, currentPointId, vehicle) {
        if (typeof oldPointId === 'undefined' || oldPointId === currentPointId) {
            return;
        }

        if (currentPointId != -1) {
            entered(currentPointId, vehicle);
        }

        if (oldPointId != -1) {
            exited(oldPointId, vehicle);
        }
    }

    function entered(id, vehicle) {
        crossed(id, vehicle, 'alert_enter', 'entered');
    }

    function exited(id, vehicle) {
        crossed(id, vehicle, 'alert_exit', 'exited');
    }

    function crossed(id, vehicle, field, verb) {
        var point = lookupPointById(id),
            text =
                '<span class="date">' + vehicle.date + '</span>' +
                '<span class="time">' + vehicle.time + '</span>: ' +
                '<span class="reg">' + vehicle.reg + '</span> ' +
                verb;

        if (point !== null && point[field] == 1) {
            showAlertMessage(text + ' \'' + point.point_name + '\'');
        }
    }

    function showWarningMessage(text) {
        $('#warnings').show();
        $('<li />').html(text).appendTo($('#warnings-list'));
        startWarningsCloseTimer();
    }

    function showAlertMessage(reg, text) {
        playAlertSound();
        if ($('#opt1_2').attr('checked')) {
            //createnotify("sticky", { title: reg + ' Overspeed Notification', text: text }, { expires: false });
            $.jnotify('<div class="jnotify_header">' + reg + ' Overspeed Notification</div>' + text, 'error', { timeout: 0 });
        }
    }

    function createnotify(template, vars, opts) {
        return $container.notify("create", template, vars, opts);
    }

    function playAlertSound() {
        if ($('#opt1_1').attr('checked')) {
            $('#off-screen').html('<embed src="' + soundpath + '" autostart="true" loop="false" hidden="true"></embed>');
            setTimeout(function () { $('#off-screen').html(''); }, 6000);
        }
    }

    function closeAlerts() {
        $('#alarms-wrap').hide();
        $('#alarms-list').empty();
    }

    function closeWarnings() {
        $('#warnings').hide();
        $('#warnings-list').empty();
        warningsCloseTimer = null;
    }

    function startWarningsCloseTimer() {
        if (warningsCloseTimer !== null) {
            clearTimeout(warningsCloseTimer);
        }

        warningsCloseTimer = setTimeout(closeWarnings, warningsCloseTimeMs);
    }

    function lookupPointById(id) {
        var result = null;

        $.each(points, function () {
            if (this.id == id) {
                result = this;
            }
        });

        return result;
    }

    return {
        initialise: initialise,
        systemWarning: systemWarning,
        systemAlert: systemAlert,
        checkForCrossings: checkForCrossings
    };
}());

$(window).bind('resize', set_sizes);
function compare(a, b) {
    if (a.Reg < b.Reg)
        return -1;
    if (a.Reg > b.Reg)
        return 1;
    return 0;
}

function vehiclesMonitoring() {
    var results = "";
    var vehiclesArray = new Array();
    var vehiclesArray = vehicles;
    vehiclesArray.sort(compare);
    var accesscnt = 0;
    results = "<table>";
    for (var i = 0; i < vehiclesArray.length; i++) {
        results += "<tr><td>" + '<input type="checkbox" name="chkReg" value="' + vehiclesArray[i].VehicleId + '">' +
            vehiclesArray[i].Reg + '</input>'; +"</td></tr>";
    }
    results += "<table><br /> <br />";
    var div = document.getElementById("vehicles-to-monitor");
    div.innerHTML = results;

    $('#chkReg').click($("input:checkbox").click(function () {
        var VehicleId = this.value;
        var monitorresult = $.grep(vehicles, function (e) { return e.VehicleId == VehicleId; });
        if (monitorresult.length > 0) {
            for (var i = 0; i < monitorresult.length; i++) {
                monitorresult[i].can_monitor = $(this).is(":checked");
            }
        }
    }));
}
function distancerptvehicles(vehicles) {
    var $list = $('#report-vehicles-distancetravelled').empty();
    var htmldata = "<table>";
    var colmn = 2; var groupCount = 0; var x_count = 0;

    for (var x in vehicle_groups) {
        ++groupCount;
    }

    for (var group in vehicle_groups) {
        htmldata += '<tr><td colspan=' + colmn + '><input type="radio" class=" chkRegid ' + group.replace(/ /g, "_") +
                '" name="chkGroup" value="' + group + '">' + group + '</input>' + "</td></tr>";
    }
    //htmldata += '<td colspan='+colmn+ '><input type="checkbox" id="chkRegid" name="chkRegAll" value="">All</input>'+"</td>";id="chkGroup"

    htmldata += "<tr><td colspan='" + colmn + "'><hr /></td></tr><tr>";
    var count = 1;
    $.each(vehicles, function () {
        count += 1;
        htmldata += (count % colmn == 0 ? "</tr> <tr>" : "");
        var classes = this.GroupName.length > 0 ? this.GroupName : "";
        classes = classes.replace(/ /g, "_").replace(/,/g, " ");
        htmldata += "<td><input type='checkbox' class='chkReg all All " + classes + "' name='chkReg' value='" + this.VehicleId + "'> "
            + this.Reg + "</input></td>";
    });
    htmldata += "</tr></table>";
    $list.html(htmldata);

    $('.chkRegid').click(function (event) {  //on click 
        $(".all").each(function () { //loop through each checkbox
            this.checked = false;  //select all checkboxes with class "checkbox1"               
        });

        var value_sel = '.' + this.value.toString().replace(/ /g, "_");
        if (this.checked) { // check select status
            $(value_sel).each(function () { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        } else {
            $(value_sel).each(function () { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });
        }
    });
}
function zoom_to_monitoring() {
    var bounds = new google.maps.LatLngBounds();

    $.each(vehicles, function () {
        if (this.can_monitor) {
            bounds.extend(latLng(this.Latitude, this.Longitude));
        }
    });
    if (!bounds.isEmpty()) {
        map.fitBounds(bounds);
    }
}

function isRecovery() {
    return (typeof lost === "undefined");
}