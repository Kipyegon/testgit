﻿function loadDialog(url, divElementId, title) {
    showLoading();
    divElementId = "#" + divElementId;
    $(divElementId).load(url, function (responseText, textStatus, XMLHttpRequest) {
        $(divElementId).dialog({
            title: title,
            autoOpen: false,
            resizable: false,
            width: 'auto',
            autoResize: true,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: true,
            open: function (event, ui) {
                $(this).html(textStatus == 'success' ? responseText : 'An error occured while loading dialog. </br> Kindly try again');
                closeLoading();
                $("div.ui-dialog-content").bind('DOMNodeInserted', function (e) {
                    //alert('element now contains: ' + $(e.target).html());
                    $(this).dialog('option', 'position', 'center');
                    return;
                });
            }
        });
        $(divElementId).dialog('open');
    });
}
function dialogStartUp() {
    $.validator.unobtrusive.parse($('form'));
    $('form').submit(function () {
        var val = $("button[type=submit][clicked=true]").val();
        if ($(this).valid()) {
            var frm = $(this);
            $('#from_result').html('');
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize() + '&Command=' + val,
                type: 'POST',
                success: function (responseText, textStatus, XMLHttpRequest) {
                    console.log(responseText);
                    if (responseText.Status == null && responseText.IsUpdate == null) {
                        $('#from_result').html(responseText);
                        return;
                    }
                    else if (responseText.Status && responseText.IsUpdate) {
                        //location.reload(true);
                        closeLoading();
                        closeWaiting();
                        $("div.ui-dialog-content").dialog('close');
                        $('#dataGrid').load(window.location.href + window.location.href.indexOf('?') > -1 ? '&dataGrid=true' : "?dataGrid=true", function () {
                            $('tbody > tr:first').effect("highlight", {}, 2000);
                        });
                        return;
                    } else if (responseText.Status && !responseText.IsUpdate) {
                        frm[0].reset();
                    }
                    $('#from_result').html(responseText.Message);
                    $.validator.unobtrusive.parse($('form'));
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $('#from_result').html("An error occurred while trying to access the server, Kindly retry again.</br>" +
                            "If problem persists, kindly check your network and contact support@geeckoltd.com");
                    // console.log(XMLHttpRequest);
                    // console.log(textStatus);
                    // console.log(errorThrown);
                },
                beforeSend: function () {
                    showWaiting();
                },
                complete: function () {
                    closeWaiting();
                }
            });
        }
        return false;
    });

    $("form button[type=submit]").click(function () {
        $("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });
}
function showLoading() {
    if ($('#loading').length < 1) {
        var $div = $('<div />').appendTo('body,html');
        $div.attr('id', 'loading');
    }
    $('#loading').css('visibility', 'visible');
}
function closeLoading() {
    $('#loading').remove();
    $('#loading').css('visibility', 'hidden');
}

function showWaiting() {
    $.blockUI({
        message: $('#wait')
    });
}

function closeWaiting() {
    $.unblockUI();
}