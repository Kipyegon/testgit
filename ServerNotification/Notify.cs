﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using DataAccess.SQL;
using PushSharp;
using PushSharp.Apple;
using PushSharp.WindowsPhone;
using PushSharp.Android;
using PushSharp.Blackberry;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Reflection;

namespace ServerNotification
{
    public static class GlobalNotify
    {
        static Notify _notify;
        //private readonly GFleetModelContainer entities = new GFleetModelContainer();

        public static Notify notify
        {
            get
            {
                if (_notify == null)
                {
                    _notify = new Notify();
                }
                return _notify;
            }

        }
    }

    public class Notify
    {
        PushService pushApple;
        PushService pushWindowPhone;
        PushService pushAndroid;
        PushService pushblackBerry;

        string Sent = "01", Failed = "02", Chanel_Exception = "03", //Service_Exception = "04",
            Subscription_Expired = "05", Subscription_Changed = "06", Chanel_Destroyed = "07", Chanel_Created = "08";

        public void sendAppleNotifications(string deviceToken, string Message, Guid ModifiedBy, Guid UserId, int NotificationTypeId)
        {
            try
            {
                //Create our push services broker

                deviceToken = deviceToken.Replace("<", "").Replace(" ", "").Replace(">", "");

                if (pushApple == null)
                {
                    try
                    {
                        //deviceids.Add("235423565475675676");
                        pushApple = new PushService();


                        //Wire up the events for all the services that the broker registers
                        /*
                            pushApple.Events.OnNotificationSent += new PushSharp.Common.ChannelEvents.NotificationSentDelegate(Events_OnNotificationSent);
                            pushApple.Events.OnChannelException += ChannelException;
                            pushApple.Events.OnServiceException += ServiceException;
                            pushApple.Events.OnNotificationFailed += NotificationFailed;
                            pushApple.Events.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                            pushApple.Events.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
                            pushApple.Events.OnChannelCreated += ChannelCreated;
                            pushApple.Events.OnChannelDestroyed += ChannelDestroyed;
                        */

                        pushApple.Events.OnDeviceSubscriptionExpired += new PushSharp.Common.ChannelEvents.DeviceSubscriptionExpired(Events_OnDeviceSubscriptionExpired);
                        pushApple.Events.OnDeviceSubscriptionIdChanged += new PushSharp.Common.ChannelEvents.DeviceSubscriptionIdChanged(Events_OnDeviceSubscriptionIdChanged);
                        pushApple.Events.OnChannelException += new PushSharp.Common.ChannelEvents.ChannelExceptionDelegate(Events_OnChannelException);
                        pushApple.Events.OnNotificationSendFailure += new PushSharp.Common.ChannelEvents.NotificationSendFailureDelegate(Events_OnNotificationSendFailure);
                        pushApple.Events.OnNotificationSent += new PushSharp.Common.ChannelEvents.NotificationSentDelegate(Events_OnNotificationSent);
                        pushApple.Events.OnChannelCreated += new PushSharp.Common.ChannelEvents.ChannelCreatedDelegate(Events_OnChannelCreated);
                        pushApple.Events.OnChannelDestroyed += new PushSharp.Common.ChannelEvents.ChannelDestroyedDelegate(Events_OnChannelDestroyed);


                        //-------------------------
                        // APPLE NOTIFICATIONS
                        //-------------------------
                        //Configure and start Apple APNS
                        // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
                        //   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
                        //   app with!
                        Boolean isProduction;
                        byte[] appleCert;

                        Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["AppleProduction"], out isProduction);
                        if (isProduction)
                        {
                            appleCert = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "aps_production.p12"));
                        }
                        else
                        {
                            appleCert = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "aps_development.p12"));
                        }
                        //IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
                        //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
                        //  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
                        //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')

                        string CertificateFilePwd = System.Configuration.ConfigurationManager.AppSettings["CertificateFilePwd"];

                        pushApple.StartApplePushService(new ApplePushChannelSettings(isProduction, appleCert, CertificateFilePwd)); //Extension method
                        //pushApple.StartApplePushService(new ApplePushChannelSettings(appleCert, "pushsharp"));

                        logToFile("pushApple Created!", getDeviceType("pushApple"));
                    }
                    catch (Exception ex)
                    {
                        pushApple = null;
                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);

                        logToFile("pushApple not Created!", getDeviceType("pushApple"));
                        //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                    }
                }

                if (pushApple != null)
                {
                    logToFile("pushApple Pushing to device: " + deviceToken, getDeviceType("pushApple"));
                    new Thread(() =>
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            try
                            {
                                DataAccess.SQL.Notification not = new DataAccess.SQL.Notification()
                                {
                                    DeviceToken = deviceToken,
                                    UserId = UserId,
                                    NotificationTypeId = NotificationTypeId,
                                    NotificationMsg = Message,
                                    ModifiedBy = ModifiedBy,
                                    ModifiedDate = DateTime.Parse(DateTime.Now.ToString())
                                };

                                entities.Notifications.AddObject(not);
                                entities.SaveChanges();

                                //Fluent construction of an iOS notification
                                //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
                                //  for registered for remote notifications is called, and the device token is passed back to you
                                //pushApple.QueueNotification(new AppleNotification()
                                //    .ForDeviceToken(deviceToken)
                                //    .WithContentAvailable(1)
                                //    .WithCustomItem("Id", not.NotificationId)
                                //    .WithCustomItem("typeId", NotificationTypeId)
                                //    .WithAlert(Message)
                                //    .WithBadge(1)
                                //    .WithSound("default")
                                //);

                                pushApple.QueueNotification(NotificationFactory.Apple()
                                   .ForDeviceToken(deviceToken)
                                   .WithContentAvailable(1)
                                   .WithCustomItem("Id", not.NotificationId)
                                   .WithCustomItem("typeId", NotificationTypeId)
                                   .WithAlert(Message)
                                   .WithBadge(1)
                                   .WithSound("default")
                                   );

                                //Stop and wait for the queues to drains
                                //pushApple.StopAllServices();
                                //push = null;
                            }
                            catch (Exception ex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                                //logToFile(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                            }
                        }
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                //logToFile(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public void sendWinPhoneNotifications(string deviceToken, string ChannelUri, string Message, Guid ModifiedBy, Guid UserId, int NotificationTypeId, int ClientId)
        {

            Message = Message.Substring(0, Message.IndexOf('.'));
            try
            {
                //Create our push services broker
                string _WinPhoneNavigatePath = System.Configuration.ConfigurationManager.AppSettings["WinPhoneNavigatePath"];

                //deviceToken = deviceToken.Replace("<", "").Replace(" ", "").Replace(">", "");

                if (pushWindowPhone == null)
                {
                    try
                    {
                        //deviceids.Add("235423565475675676");
                        pushWindowPhone = new PushService();


                        //Wire up the events for all the services that the broker registers
                        /*
                            pushWindow.OnNotificationSent += new Common.ChannelEvents.DeviceSubscriptionExpired(Events_OnDeviceSubscriptionExpired);
                            pushWindow.OnChannelException += ChannelException;
                            pushWindow.OnServiceException += ServiceException;
                            pushWindow.OnNotificationFailed += NotificationFailed;
                            pushWindow.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                            pushWindow.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
                            pushWindow.OnChannelCreated += ChannelCreated;
                            pushWindow.OnChannelDestroyed += ChannelDestroyed;
                        */

                        pushWindowPhone.Events.OnDeviceSubscriptionExpired += new PushSharp.Common.ChannelEvents.DeviceSubscriptionExpired(Events_OnDeviceSubscriptionExpired);
                        pushWindowPhone.Events.OnDeviceSubscriptionIdChanged += new PushSharp.Common.ChannelEvents.DeviceSubscriptionIdChanged(Events_OnDeviceSubscriptionIdChanged);
                        pushWindowPhone.Events.OnChannelException += new PushSharp.Common.ChannelEvents.ChannelExceptionDelegate(Events_OnChannelException);
                        pushWindowPhone.Events.OnNotificationSendFailure += new PushSharp.Common.ChannelEvents.NotificationSendFailureDelegate(Events_OnNotificationSendFailure);
                        pushWindowPhone.Events.OnNotificationSent += new PushSharp.Common.ChannelEvents.NotificationSentDelegate(Events_OnNotificationSent);
                        pushWindowPhone.Events.OnChannelCreated += new PushSharp.Common.ChannelEvents.ChannelCreatedDelegate(Events_OnChannelCreated);
                        pushWindowPhone.Events.OnChannelDestroyed += new PushSharp.Common.ChannelEvents.ChannelDestroyedDelegate(Events_OnChannelDestroyed);

                        //winMobileApple.RegisterWindowsPhoneService();
                        //pushWindow.RegisterService<WindowsPhoneToastNotification>(
                        //                                    new WindowsPhonePushService(new WindowsPhonePushChannelSettings()));

                        pushWindowPhone.StartWindowsPhonePushService(new WindowsPhonePushChannelSettings());
                        logToFile("pushWindow Created!", getDeviceType("pushWindow"));
                    }
                    catch (Exception ex)
                    {
                        pushWindowPhone = null;

                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);

                        logToFile("pushWindow not Created!", getDeviceType("pushWindow"));
                        //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                    }
                }

                if (pushWindowPhone != null)
                {
                    logToFile("pushWindow Pushing to device: " + deviceToken, getDeviceType("pushWindow"));
                    new Thread(() =>
                    {

                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            try
                            {
                                DataAccess.SQL.Notification not = new DataAccess.SQL.Notification()
                                {
                                    DeviceToken = deviceToken,
                                    UserId = UserId,
                                    NotificationTypeId = NotificationTypeId,
                                    NotificationMsg = Message,
                                    ModifiedBy = ModifiedBy,
                                    ModifiedDate = DateTime.Parse(DateTime.Now.ToString()),
                                    ChannelUri = ChannelUri
                                };

                                entities.Notifications.AddObject(not);
                                entities.SaveChanges();

                                //-----------------------------
                                // WINDOWS PHONE NOTIFICATIONS
                                //-----------------------------
                                //Configure and start Windows Phone Notifications
                                //winMobileApple.RegisterWindowsPhoneService();
                                //Fluent construction of a Windows Phone Toast notification
                                //IMPORTANT: For Windows Phone you MUST use your own Endpoint Uri here that gets generated within your Windows Phone app itself!

                                if (!string.IsNullOrEmpty(ChannelUri))
                                {
                                    //winMobileApple.QueueNotification(new WindowsPhoneTileNotification()
                                    //    .ForEndpointUri(new Uri(ChannelUri))
                                    //    .ForOSVersion(WindowsPhoneDeviceOSVersion.Eight)
                                    //    .WithBatchingInterval(BatchingInterval.Immediate)
                                    //    .WithTitle("TFix")
                                    //    .WithBackContent(Message)

                                    //);

                                    pushWindowPhone.QueueNotification(new WindowsPhoneToastNotification()
                                        .ForEndpointUri(new Uri(ChannelUri))
                                        .ForOSVersion(WindowsPhoneDeviceOSVersion.Eight)
                                        .WithBatchingInterval(BatchingInterval.Immediate)
                                        .WithNavigatePath(_WinPhoneNavigatePath)
                                        .WithText1("GFLEET")
                                        .WithText2(Message)
                                        .WithParameter("Id", not.NotificationId.ToString())
                                        .WithParameter("typeId", NotificationTypeId.ToString())
                                        );
                                }
                                getUserProfile_Result p = entities.getUserProfile(ClientId, 3).Where(r => r.UserId == UserId).FirstOrDefault();

                                string sms = System.Configuration.ConfigurationManager.AppSettings["WinSMS"];

                                sms = string.Format(sms, p == null ? null : p.FullNames);
                                new Thread(() =>
                                {
                                    try
                                    {
                                        GFleetV3SMSProcessor.SendSMS.SendMessage(p.Telephone1, sms, "");
                                    }
                                    catch (Exception ex)
                                    {
                                        //throw new Exception(ex.Message.ToString());
                                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                                    }
                                }).Start();


                            }
                            catch (Exception ex)
                            {
                                //logToFile(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                            }
                        }
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                //logToFile(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
            }
        }

        public void sendAndroidNotifications(string deviceToken, int DispatchId, Guid ModifiedBy, int NotificationTypeId)
        {
            if (pushAndroid == null)
            {

                try
                {
                    pushAndroid = new PushService();

                    //Wire up the events for all the services that the broker registers
                    /*
                        pushAndroid.OnNotificationSent += new Common.ChannelEvents.DeviceSubscriptionExpired(Events_OnDeviceSubscriptionExpired);
                        pushAndroid.OnChannelException += ChannelException;
                        pushAndroid.OnServiceException += ServiceException;
                        pushAndroid.OnNotificationFailed += NotificationFailed;
                        pushAndroid.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                        pushAndroid.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
                        pushAndroid.OnChannelCreated += ChannelCreated;
                        pushAndroid.OnChannelDestroyed += ChannelDestroyed;
                    */

                    pushAndroid.Events.OnDeviceSubscriptionExpired += new PushSharp.Common.ChannelEvents.DeviceSubscriptionExpired(Events_OnDeviceSubscriptionExpired);
                    pushAndroid.Events.OnDeviceSubscriptionIdChanged += new PushSharp.Common.ChannelEvents.DeviceSubscriptionIdChanged(Events_OnDeviceSubscriptionIdChanged);
                    pushAndroid.Events.OnChannelException += new PushSharp.Common.ChannelEvents.ChannelExceptionDelegate(Events_OnChannelException);
                    pushAndroid.Events.OnNotificationSendFailure += new PushSharp.Common.ChannelEvents.NotificationSendFailureDelegate(Events_OnNotificationSendFailure);
                    pushAndroid.Events.OnNotificationSent += new PushSharp.Common.ChannelEvents.NotificationSentDelegate(Events_OnNotificationSent);
                    pushAndroid.Events.OnChannelCreated += new PushSharp.Common.ChannelEvents.ChannelCreatedDelegate(Events_OnChannelCreated);
                    pushAndroid.Events.OnChannelDestroyed += new PushSharp.Common.ChannelEvents.ChannelDestroyedDelegate(Events_OnChannelDestroyed);


                    //---------------------------
                    // ANDROID GCM NOTIFICATIONS
                    //---------------------------
                    //Configure and start Android GCM
                    //IMPORTANT: The API KEY comes from your Google APIs Console App, under the API Access section, 
                    //  by choosing 'Create new Server key...'
                    //  You must ensure the 'Google Cloud Messaging for Android' service is enabled in your APIs Console
                    //pushAndroid.RegisterGcmService(new GcmPushChannelSettings("AIzaSyC_rxrEIhZSf0GYE-kB9y0Ztp5MYjoEZjM"));
                    //Fluent construction of an Android GCM Notification
                    //IMPORTANT: For Android you MUST use your own RegistrationId here that gets generated within your Android app itself!

                    string GoogleProjectID = System.Configuration.ConfigurationManager.AppSettings["GoogleProjectID"];
                    string GoogleAuthToken = System.Configuration.ConfigurationManager.AppSettings["GoogleAuthToken"];
                    string GooglePackageName = System.Configuration.ConfigurationManager.AppSettings["GooglePackageName"];

                    //Configure and start Android GCM
                    //IMPORTANT: The SENDER_ID is your Google API Console App Project ID.
                    //  Be sure to get the right Project ID from your Google APIs Console.  It's not the named project ID that appears in the Overview,
                    //  but instead the numeric project id in the url: eg: https://code.google.com/apis/console/?pli=1#project:785671162406:overview
                    //  where 785671162406 is the project id, which is the SENDER_ID to use!
                    //pushAndroid.StartGoogleCloudMessagingPushService(new GcmPushChannelSettings("785671162406", "AIzaSyC_rxrEIhZSf0GYE-kB9y0Ztp5MYjoEZjM", "com.pushsharp.test"));
                    pushAndroid.StartGoogleCloudMessagingPushService(new GcmPushChannelSettings(GoogleProjectID, GoogleAuthToken, GooglePackageName));


                    logToFile("pushAndroid Created!", getDeviceType("pushAndroid"));
                }
                catch (Exception ex)
                {
                    pushAndroid = null;
                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);

                    logToFile("pushAndroid not Created!", getDeviceType("pushAndroid"));
                }
            }

            if (pushAndroid != null)
            {

                logToFile("pushAndroid Pushing to device: " + deviceToken, getDeviceType("pushAndroid"));
                new Thread(() =>
                {

                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        try
                        {
                            DataAccess.SQL.Notification not = new DataAccess.SQL.Notification()
                            {
                                DeviceToken = deviceToken,
                                UserId = ModifiedBy,
                                NotificationTypeId = NotificationTypeId,
                                NotificationMsg = "Dispatch has been made Kindly Tap here to view details.",
                                ModifiedBy = ModifiedBy,
                                ModifiedDate = DateTime.Parse(DateTime.Now.ToString())
                            };

                            entities.Notifications.AddObject(not);
                            entities.SaveChanges();
                            PushNots _not = new PushNots()
                            {
                                alert = "Dispatch has been made Kindly Tap here to view details.",
                                badge = 7,
                                Id = DispatchId,
                                sound = "sound.caf",
                                typeId = NotificationTypeId
                            };

                            pushAndroid.QueueNotification(NotificationFactory.AndroidGcm()
                                .ForDeviceRegistrationId(deviceToken)
                                .WithCollapseKey("NONE")
                                .WithJson(_not.ToJSON()));

                            //string msg = "{\"alert\":\"" + Message + "\",\"badge\":7,\"sound\":\"sound.caf\",\"Id\":" + not.NotificationId.ToString() + ",\"typeId\":"+NotificationTypeId+"}";
                            //pushAndroid.QueueNotification(new GcmNotification()
                            //    .ForDeviceRegistrationId(deviceToken)
                            //        .WithJson(_not.ToJSON()));

                        }
                        catch (Exception ex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                        }
                    }

                }).Start();
            }
        }

        public void sendBlackBerryNotifications(string deviceToken, string Message, Guid ModifiedBy, Guid UserId, int NotificationTypeId)
        {
            if (pushblackBerry == null)
            {

                try
                {
                    pushblackBerry = new PushService();


                    //Wire up the events for all the services that the broker registers
                    pushblackBerry.Events.OnDeviceSubscriptionExpired += new PushSharp.Common.ChannelEvents.DeviceSubscriptionExpired(Events_OnDeviceSubscriptionExpired);
                    pushblackBerry.Events.OnDeviceSubscriptionIdChanged += new PushSharp.Common.ChannelEvents.DeviceSubscriptionIdChanged(Events_OnDeviceSubscriptionIdChanged);
                    pushblackBerry.Events.OnChannelException += new PushSharp.Common.ChannelEvents.ChannelExceptionDelegate(Events_OnChannelException);
                    pushblackBerry.Events.OnNotificationSendFailure += new PushSharp.Common.ChannelEvents.NotificationSendFailureDelegate(Events_OnNotificationSendFailure);
                    pushblackBerry.Events.OnNotificationSent += new PushSharp.Common.ChannelEvents.NotificationSentDelegate(Events_OnNotificationSent);
                    pushblackBerry.Events.OnChannelCreated += new PushSharp.Common.ChannelEvents.ChannelCreatedDelegate(Events_OnChannelCreated);
                    pushblackBerry.Events.OnChannelDestroyed += new PushSharp.Common.ChannelEvents.ChannelDestroyedDelegate(Events_OnChannelDestroyed);

                    string blackBerryBaseUrl = System.Configuration.ConfigurationManager.AppSettings["blackBerryBaseUrl"];
                    string blackBerryAppId = System.Configuration.ConfigurationManager.AppSettings["blackBerryAppId"];
                    string blackBerryPassword = System.Configuration.ConfigurationManager.AppSettings["blackBerryPassword"];

                    //Configure and start Blackberry Push
                    //pushblackBerry.StartBlackberryPushService(new BlackberryPushChannelSettings(
                    //  "https://cpXXXX.pushapi.eval.blackberry.com", "xxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "xxxxxxx"));

                    pushblackBerry.StartBlackberryPushService(new BlackberryPushChannelSettings(blackBerryBaseUrl, blackBerryAppId, blackBerryPassword));

                    logToFile("pushblackBerry Created!", getDeviceType("pushblackBerry"));
                }
                catch (Exception ex)
                {
                    pushblackBerry = null;
                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);

                    logToFile("pushblackBerry not Created!", getDeviceType("pushblackBerry"));
                }
            }

            if (pushblackBerry != null)
            {

                logToFile("pushblackBerry Pushing to device: " + deviceToken, getDeviceType("pushblackBerry"));
                new Thread(() =>
                {

                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        try
                        {
                            DataAccess.SQL.Notification not = new DataAccess.SQL.Notification()
                            {
                                DeviceToken = deviceToken,
                                UserId = UserId,
                                NotificationTypeId = NotificationTypeId,
                                NotificationMsg = Message,
                                ModifiedBy = ModifiedBy,
                                ModifiedDate = DateTime.Parse(DateTime.Now.ToString())
                            };

                            entities.Notifications.AddObject(not);
                            entities.SaveChanges();

                            PushNots _not = new PushNots()
                            {
                                Id = not.NotificationId,
                                typeId = NotificationTypeId
                            };

                            //Object _not = new Object()
                            //{
                            //    Id = not.NotificationId,
                            //    typeId = NotificationTypeId
                            //};


                            //Fluent construction of an Blackberry Notification
                            pushblackBerry.QueueNotification(NotificationFactory.Blackberry()

                                .ForDeviceRegistrationId(deviceToken)
                                //.WithData(_not.ToJSON()));
                                .WithTag(_not.ToJSON())
                                .WithData(Message));

                        }
                        catch (Exception ex)
                        {
                            ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
                        }
                    }

                }).Start();
            }
        }

        public string getExeptionMessage(Exception ex)
        {

            return ((NotificationFailureException)ex).ErrorStatusDescription +
                ":: " + ex.InnerException != null ? ex.Message + " >>> " + ex.InnerException.Message +
                (ex.InnerException.InnerException != null ? " >> " + ex.InnerException.InnerException.Message : "") : ex.Message;
        }

        public void logToFile(string text, int deviceType)
        {
            Thread df = new Thread(() => writefile(text, deviceType));
            df.Start();
        }
        public void logToFile(string text, string logType, int deviceType)
        {
            Thread df = new Thread(() => writefile(text, logType, deviceType));
            df.Start();

        }
        public void writefile(string text, int deviceType)
        {
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    string data = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss tt") + " DATA: " + text;
                    PushLog pl = new PushLog()
                    {
                        PushLogCharacter = data,
                        DateModified = DateTime.Now,
                        LogType = "10",
                        DeviceType = deviceType
                    };
                    entities.PushLogs.AddObject(pl);
                    entities.SaveChanges();
                }
                ////string path = System.Web.HttpContext.Current.Server.MapPath("~/applogs.txt");
                //string path = Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).LocalPath) + "\\TFixLogs.txt";
                ////Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                //{
                //    file.WriteLine(data);
                //}

                ////string data = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss tt") + " DATA: " + text;
                ////using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"applogs.txt", true))
                ////{
                ////    file.WriteLine(data);
                ////}
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
            }
        }
        public void writefile(string text, string logType, int deviceType)
        {
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    string data = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss tt") + " DATA: " + text;
                    PushLog pl = new PushLog()
                    {
                        PushLogCharacter = data,
                        DateModified = DateTime.Now,
                        LogType = logType,
                        DeviceType = deviceType
                    };
                    entities.PushLogs.AddObject(pl);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);
            }
        }

        int getDeviceType(string data)
        {
            if (data.ToLower().Contains("android"))
                return 1;
            else if (data.ToLower().Contains("apple"))
                return 2;
            else if (data.ToLower().Contains("window"))
                return 3;
            else if (data.ToLower().Contains("blackberry"))
                return 4;
            else
                return -1;
        }

        void StopPush(string data)
        {
            if (data.ToLower().Contains("android"))
            {
                if (this.pushAndroid != null)
                {
                    this.pushAndroid.StopAllServices(true);
                    this.pushAndroid = null;
                }
            }
            else if (data.ToLower().Contains("apple"))
            {
                if (this.pushApple != null)
                {
                    this.pushApple.StopAllServices(true);
                    this.pushApple = null;
                }
            }
            else if (data.ToLower().Contains("window"))
            {
                if (this.pushWindowPhone != null)
                {
                    this.pushWindowPhone.StopAllServices(true);
                    this.pushWindowPhone = null;
                }
            }
            else if (data.ToLower().Contains("blackberry"))
            {
                if (this.pushblackBerry != null)
                {
                    this.pushblackBerry.StopAllServices(true);
                    this.pushblackBerry = null;
                }
            }
            else
            { }
        }


        /*
               void NotificationSent(object sender, INotification notification)
               {
                   logToFile("Sent: " + sender + " -> " + notification.ToJSON(), Sent, getDeviceType(sender.ToString()));
               }

               void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
               {
                   logToFile("Failure: " + getExeptionMessage(notificationFailureException) + " -> " +
                       notification.ToJSON(), Failed, getDeviceType(sender.ToString()));
               }

               void ChannelException(object sender, IPushChannel channel, Exception exception)
               {
                   logToFile("Channel Exception: " + getExeptionMessage(exception) + " -> ", Chanel_Exception, getDeviceType(sender.ToString()));
               }

               void ServiceException(object sender, Exception exception)
               {
                   logToFile("Service Exception: " + getExeptionMessage(exception) + " -> " + exception, Service_Exception, getDeviceType(sender.ToString()));
               }

               void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
               {
                   logToFile("Device Subscription Expired: " + expiredDeviceSubscriptionId + " -> " + notification.ToJSON(), Subscription_Expired
                       , getDeviceType(sender.ToString()));
               }

               void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
               {
                   //Currently this event will only ever happen for Android GCM
                   logToFile("Device Registration Changed:  Old-> " + oldSubscriptionId + "  New-> " + newSubscriptionId + " -> " + notification.ToJSON(),
                       Subscription_Changed, getDeviceType(sender.ToString()));
               }

               void ChannelDestroyed(object sender)
               {
                   logToFile("Channel Destroyed for: " + sender.ToString(), Chanel_Destroyed, getDeviceType(sender.ToString()));
               }

               void ChannelCreated(object sender, IPushChannel pushChannel)
               {
                   logToFile("Channel Created for: " + sender.ToString(), Chanel_Created, getDeviceType(sender.ToString()));
               }

               */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="oldDeviceInfo"></param>
        /// <param name="newDeviceInfo"></param>
        /// <param name="notification"></param>

        void Events_OnDeviceSubscriptionIdChanged(PushSharp.Common.PlatformType platform, string oldDeviceInfo, string newDeviceInfo, PushSharp.Common.Notification notification)
        {
            //Currently this event will only ever happen for Android GCM
            //Console.WriteLine("Device Registration Changed:  Old-> " + oldDeviceInfo + "  New-> " + newDeviceInfo);
            logToFile("Device Registration Changed:  Old-> " + oldDeviceInfo + "  New-> " + newDeviceInfo + " -> " + notification.ToJSON(),
                Subscription_Changed, getDeviceType(platform.ToString().ToString()));
        }

        void Events_OnNotificationSent(PushSharp.Common.Notification notification)
        {
            //Console.WriteLine("Sent: " + notification.Platform.ToString() + " -> " + notification.ToString());
            logToFile("Sent: " + notification.Platform.ToString() + " -> " + notification.ToJSON(), Sent, getDeviceType(notification.Platform.ToString()));
        }

        void Events_OnNotificationSendFailure(PushSharp.Common.Notification notification, Exception notificationFailureException)
        {
            //Console.WriteLine("Failure: " + notification.Platform.ToString() + " -> " + notificationFailureException.Message + " -> " + notification.ToString());
            logToFile("Failure: " + getExeptionMessage(notificationFailureException) + " -> " +
                notification.ToJSON(), Failed, getDeviceType(notification.Platform.ToString()));
            //logToFile("Failure: " + ((NotificationFailureException)notificationFailureException).ErrorStatusDescription + " -> " +
            //    notification.ToJSON(), Failed, getDeviceType(notification.Platform.ToString()));
        }

        void Events_OnChannelException(Exception exception, PushSharp.Common.PlatformType platformType, PushSharp.Common.Notification notification)
        {
            //Console.WriteLine("Channel Exception: " + platformType.ToString() + " -> " + exception.ToString());
            StopPush(platformType.ToString());

            logToFile("Channel Exception: " + getExeptionMessage(exception) + " -> ", Chanel_Exception, getDeviceType(platformType.ToString()));
        }

        void Events_OnDeviceSubscriptionExpired(PushSharp.Common.PlatformType platform, string deviceInfo, PushSharp.Common.Notification notification)
        {
            //Console.WriteLine("Device Subscription Expired: " + platform.ToString() + " -> " + deviceInfo);
            logToFile("Device Subscription Expired: " + deviceInfo + " -> " + notification.ToJSON(), Subscription_Expired
                , getDeviceType(platform.ToString()));
        }

        void Events_OnChannelDestroyed(PushSharp.Common.PlatformType platformType, int newChannelCount)
        {
            //Console.WriteLine("Channel Destroyed for: " + platformType.ToString() + " Channel Count: " + newChannelCount);
            StopPush(platformType.ToString());

            logToFile("Channel Destroyed for: " + platformType.ToString() + ". newChannelCount: " + newChannelCount, Chanel_Destroyed, getDeviceType(platformType.ToString()));
        }

        void Events_OnChannelCreated(PushSharp.Common.PlatformType platformType, int newChannelCount)
        {
            //Console.WriteLine("Channel Created for: " + platformType.ToString() + " Channel Count: " + newChannelCount);
            logToFile("Channel Created for: " + platformType.ToString() + ". newChannelCount: " + newChannelCount, Chanel_Created, getDeviceType(platformType.ToString()));
        }
    }


    public static class JSONHelper
    {
        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Serialize(obj);
        }
    }

    public class PushNots
    {
        public string alert { get; set; }
        public int badge { get; set; }
        public string sound { get; set; }
        public int Id { get; set; }
        public int typeId { get; set; }
    }
}
