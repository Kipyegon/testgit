﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Taksi.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include("~/css/*.css"));
            bundles.Add(new StyleBundle("~/Content/Pretty").Include("~/Content/Prettify/*.css"));
            bundles.Add(new StyleBundle("~/Content/DataTables").Include("~/Content/DataTables-1.10.4/css/*.css"));

           // Bundle canvasScripts =
           //new ScriptBundle("~/bundles/scripts/canvas")
           //    .Include("~/Scripts/modernizr-*")
           //    .Include("~/Scripts/Shared/achievements.js")
           //    .Include("~/Scripts/Shared/canvas.js");
           // bundles.Add(canvasScripts); 

            Bundle canvasScripts = new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-2.1.3.js")
                        .Include("~/Scripts/jquery-ui-1.11.3.min.js")
                        .Include("~/Scripts/jquery.validate.min.js")
                        .Include("~/Scripts/jquery.blockUI.js")
                        .Include("~/Scripts/jquery.validate.unobtrusive.min.js");
            bundles.Add(canvasScripts); 

            Bundle canvasScript = new ScriptBundle("~/bundles/taksi").Include("~/js/metro/metro-loader.js")
                       .Include("~/js/metro.min.js")
                       .Include("~/js/taksi.js");
            bundles.Add(canvasScript); 

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                        "~/Scripts/DataTables-1.10.4/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/Prettify").Include(
                         "~/Scripts/Prettify/prettify.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/metrojs").Include(
                        "~/js/metro/metro-global.js",
                        "~/js/metro/metro-core.js",
                        "~/js/metro/metro-locale.js",
                        "~/js/metro/metro-touch-handler.js",

                        "~/js/metro/metro-accordion.js",
                        "~/js/metro/metro-button-set.js",
                //"~/js/metro/metro-date-format.js",
                //"~/js/metro/metro-calendar.js",
                // "~/js/metro/metro-datepicker.js",
                        "~/js/metro/metro-carousel.js",
                        "~/js/metro/metro-countdown.js",
                        "~/js/metro/metro-dropdown.js",
                        "~/js/metro/metro-input-control.js",
                        "~/js/metro/metro-live-tile.js",

                        "~/js/metro/metro-progressbar.js",
                        "~/js/metro/metro-rating.js",
                        "~/js/metro/metro-slider.js",
                        "~/js/metro/metro-tab-control.js",
                        "~/js/metro/metro-table.js",
                        "~/js/metro/metro-times.js",
                        "~/js/metro/metro-dialog.js",
                        "~/js/metro/metro-notify.js",
                        "~/js/metro/metro-listview.js",
                        "~/js/metro/metro-treeview.js",
                        "~/js/metro/metro-fluentmenu.js",
                        "~/js/metro/metro-hint.js",
                        "~/js/metro/metro-streamer.js",
                        "~/js/metro/metro-stepper.js",
                        "~/js/metro/metro-drag-tile.js",
                        "~/js/metro/metro-scroll.js",
                        "~/js/metro/metro-pull.js",
                        "~/js/metro/metro-wizard.js",
                        "~/js/metro/metro-panel.js",
                        "~/js/metro/metro-tile-transform.js",
                        "~/js/metro/metro-plugin-template.js",
                        "~/js/metro/metro-initiator.js"));


            bundles.Add(new ScriptBundle("~/bundles/jPlot").Include(
                //"~/Scripts/jqPlot/excanvas.js",
                "~/Scripts/jqPlot/jquery.jqplot.min.js",
                "~/Scripts/jqPlot/plugins/jqplot.barRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.BezierCurveRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.blockRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.bubbleRenderer.js",
                //"~/Scripts/jqPlot/plugins/jqplot.canvasAxisLabelRenderer.js",
                //"~/Scripts/jqPlot/plugins/jqplot.canvasAxisTickRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.canvasOverlay.js",
                "~/Scripts/jqPlot/plugins/jqplot.TextRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.AxisRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.ciParser.js",
                "~/Scripts/jqPlot/plugins/jqplot.cursor.js",
                "~/Scripts/jqPlot/plugins/jqplot.dateAxisRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.donutRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.dragale.js",
                "~/Scripts/jqPlot/plugins/jqplot.enhancedLegendRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.funnelRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.highlighter.js",
                "~/Scripts/jqPlot/plugins/jqplot.json2.js",
                "~/Scripts/jqPlot/plugins/jqplot.logAxisRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.mekkoAxisRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.mekkoRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.metterGuageRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.mobile.js",
                "~/Scripts/jqPlot/plugins/jqplot.ohclRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.pieRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.pointLabels.js",
                "~/Scripts/jqPlot/plugins/jqplot.pyramidAxisRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.pyramidGridRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.pyramidRenderer.js",
                "~/Scripts/jqPlot/plugins/jqplot.trendline.js"));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                "~/Scripts/dataTables-1.10.4/jquery.dataTables.js",
                "~/Scripts/dataTables-1.10.4/dataTables.autoFill.js",
                "~/Scripts/dataTables-1.10.4/dataTables.bootstrap.js",
                "~/Scripts/dataTables-1.10.4/dataTables.colReorder.js",
                "~/Scripts/dataTables-1.10.4/dataTables.colVis.js",
                "~/Scripts/dataTables-1.10.4/dataTables.fixedColumns.js",
                "~/Scripts/dataTables-1.10.4/dataTables.fixedHeaders.js",
                "~/Scripts/dataTables-1.10.4/dataTables.foundation.js",
                "~/Scripts/dataTables-1.10.4/dataTables.jqueryui.js",
                "~/Scripts/dataTables-1.10.4/dataTables.keyTable.js",
                "~/Scripts/dataTables-1.10.4/dataTables.responsive.js",
                "~/Scripts/dataTables-1.10.4/dataTables.scroller.js",
                "~/Scripts/dataTables-1.10.4/dataTables.tableTools.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/metrocss").Include(
                "~/css/iconFont.min.css",
                "~/css/metro-*"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/markerlabel").Include(
              "~/Scripts/markerlabel.js"
              ));
        }
    }
}