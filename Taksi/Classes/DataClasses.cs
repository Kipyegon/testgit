﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Taksi.Classes
{
    public class DataClasses
    {
    }

    public class DispatchPaymentMode
    {
        public int PaymentModeId { get; set; }
        public string PaymentMode { get; set; }
        public bool Assigned { get; set; }
    }
}