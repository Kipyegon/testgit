﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Taksi.Models;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Threading;
using System.Xml.Serialization;
using System.IO;
using System.Web.Configuration;
using DataAccess.SQL;
using Taksi.Service;
using Newtonsoft.Json;

namespace Taksi.Controllers
{
    public class AccountController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        //[CaptchaMvc.Attributes.CaptchaVerify("Captcha is not valid")]
        [HttpPost]
        public ActionResult Login(string Username, string Password, string Remember, string returnUrl)
        {
            var response = Request["g-recaptcha-response"];
            //secret that was generated in key value pair
            const string secret = "6LecJwUTAAAAALne594n_HSNPbqoIvzNA-06OvFE";

            var client = new System.Net.WebClient();
            var reply =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0) return View();

                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        ModelState.AddModelError("", "The captcha secret parameter is missing.");
                        break;
                    case ("invalid-input-secret"):
                        ModelState.AddModelError("", "The captcha secret parameter is invalid or malformed.");
                        break;

                    case ("missing-input-response"):
                        ModelState.AddModelError("", "The captcha response parameter is missing.");
                        break;
                    case ("invalid-input-response"):
                        ModelState.AddModelError("", "The captcha response parameter is invalid or malformed.");
                        break;

                    default:
                        ModelState.AddModelError("", "Error occured. Please try again");
                        break;
                }
            }
            else
            {
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
                FormsAuthentication.SignOut();
                Session.Abandon();
                Session.Clear();
                Username = Username.Replace("+", "");
                //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Boolean uservalid = Membership.ValidateUser(Username, Password);
                bool RememberMe = bool.Parse(Remember);

                if (uservalid == false)
                {
                    bool successfulUnlock = AutoUnlockUser(Username);
                    if (successfulUnlock)
                    {
                        FormsService.SignIn(Username, RememberMe);
                        return Json(new { ok = true, newurl = Url.Action("Index", "Dashboard") });
                    }
                }
                else //if (uservalid)
                {
                    string[] validLoginRoles = WebConfigurationManager.AppSettings["validLoginRoles"].Split(',');
                    string[] roles = Roles.GetRolesForUser(Username);
                    bool valid = false;
                    foreach (string role in validLoginRoles)
                    {
                        if (roles.Contains(role))
                        {
                            valid = true;
                        }
                    }
                    if (!valid)
                    {
                        return Json(new { ok = false, message = "User not authorized on this platform." });
                    }

                    FormsService.SignIn(Username, RememberMe);
                    return Json(new { ok = true, newurl = Url.Action("Index", "Dashboard") });
                }
            }
            return Json(new { ok = false, message = "Login Failed.Check credentials and retry" });
        }

        [CaptchaMvc.Attributes.CaptchaVerify("Captcha Is Not valid")]
        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                Boolean uservalid = Membership.ValidateUser(model.UserName, model.Password);
                if (uservalid == false)
                {
                    bool successfulUnlock = AutoUnlockUser(model.UserName);
                    if (successfulUnlock)
                    {
                        FormsService.SignIn(model.UserName, model.RememberMe);
                        if (Url.IsLocalUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Dashboard");
                        }
                    }
                }
                else //if (uservalid)
                {
                    string[] validLoginRoles = WebConfigurationManager.AppSettings["validLoginRoles"].Split(',');
                    string[] roles = Roles.GetRolesForUser(model.UserName);
                    bool valid = false;
                    foreach (string role in validLoginRoles)
                    {
                        if (roles.Contains(role))
                        {
                            valid = true;
                        }
                    }
                    if (!valid)
                    {
                        ModelState.AddModelError("", "User not authorized on this platform.");
                        return View(model);
                    }

                    FormsService.SignIn(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Dashboard");
                    }
                }

                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private bool AutoUnlockUser(string Username)
        {
            string _passwordLockoutMinutes = System.Configuration.ConfigurationManager.AppSettings["PasswordLockoutMinutes"];
            MembershipUser mu = Membership.GetUser(Username, false);
            if ((mu != null) && (mu.IsLockedOut) &&
                (mu.LastLockoutDate.ToUniversalTime().AddMinutes(int.Parse(_passwordLockoutMinutes)) < DateTime.UtcNow))
            {
                bool retval = mu.UnlockUser();
                if (retval)
                    return true;
                else
                    return false;    //something went wrong with the unlock
            }
            else
                return false;       //not locked out in the first place
            //or still in lockout period
        }

        public ActionResult LogOff()
        {
            FormsService.SignOut();
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            return RedirectToAction("Index", "Home");

        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        //public ActionResult ResetPassword()
        //{
        //    return View();
        //}
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string UserName)
        {
            //check user existence
            var user = this.getUser(UserName);

            if (user == null)
            {
                TempData["Message"] = "The username does not exist....";
            }
            else
            {

                //generate password token
                var token = SendEmail.HashResetParams(UserName, user.ProviderUserKey.ToString());

                //create url with above token
                var resetLink = "<a href='" + Url.Action("ResetPassword", "Account", new { username = UserName, reset = token }, "http") + "'>Reset Password</a>";

                var emailid = user.Email;


                //send mail
                string subject = "Password Reset Token";
                string body = "<b>Please click the link below to find your Password Reset Token</b><br/>" + resetLink; //edit it
                EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("PasswordResetToken"));
                if (ET != null)
                {
                    body = String.Format(ET.Body, resetLink);
                    body = ET.Header + body + ET.Footer;
                }

                new Thread(() =>
                {
                    try
                    {
                        Service.SendEmail.SendUserMngtEmail(emailid, subject, body,null);
                    }
                    catch (Exception mex)
                    {
                        //ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        throw new Exception(mex.Message.ToString());
                    }
                }).Start();
                //try
                //{
                //    SendEmail.SendUserMngtEmail(emailid, subject, body);
                //    TempData["Message"] = "Your Password reset token has been sent to your email. Check your mail for more instructions.";
                //}
                //catch (Exception ex)
                //{
                //    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
                //    TempData["Message"] = "Error occured while sending email." + ex.Message;
                //    return View();
                //}

            }

            return RedirectToAction("Login", "Home");
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string reset, string username)
        {
            if ((reset != null) && (username != null))
            {
                MembershipUser currentUser = this.getUser(username);


                currentUser.UnlockUser();

                string newpass = currentUser.ResetPassword();
                ViewBag.newPass = newpass;
                ViewBag.userName = username;

                string emailid = currentUser.Email;
                string subject = "Password reset";
                var resetLink = "<a href='" + Url.Action("logOn", "Home") + "'>Login</a>";
                string body = "Your Password has been changed to: <b>" + newpass + "</b><br/>";
                body += "Please login " + resetLink;
                //SendEmail.SendUserMngtEmail(emailid, subject, body);

                EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("PasswordReset"));
                if (ET != null)
                {
                    body = String.Format(ET.Body, username, newpass, "http://taksi.co.ke");
                    //String.Format(ET.Body, mRegister.Username.TrimStart('+'), password, solnaddress);
                    body = ET.Header + body + ET.Footer;
                }

                new Thread(() =>
                {
                    try
                    {
                        Service.SendEmail.SendUserMngtEmail(currentUser.Email, subject, body,null);
                    }
                    catch (Exception mex)
                    {
                        //ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                        throw new Exception(mex.Message.ToString());
                    }
                }).Start();

                TempData["Message"] = "Your Password was reset and sent to your email. Check new password and login";

                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        private MembershipUser getUser(string emailName)
        {
            MembershipUser currentUser = Membership.Providers["MembershipProviderOther"].GetUser(emailName, false);

            if (currentUser == null)
            {
                string userName = Membership.Providers["MembershipProviderOther"].GetUserNameByEmail(emailName);
                if (userName != null)
                {
                    currentUser = Membership.Providers["MembershipProviderOther"].GetUser(userName, false);
                }
            }

            return currentUser;
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {//model.NewPassword
                    string emailid = Membership.GetUser(User.Identity.Name).Email;
                    string subject = "Password reset Successful";
                    var resetLink = "<a href='" + Url.Action("logOn", "Account") + "'>Login</a>";
                    string body = "Your Password has been changed to: <b>" + model.NewPassword + "</b><br/>";
                    body += "Please login " + resetLink;
                    try
                    {
                        SendEmail.SendUserMngtEmail(emailid, subject, body,null);
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
                    }

                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = MembershipService.MinPasswordLength;
            return View(model);
        }
        [Authorize]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

    }
}