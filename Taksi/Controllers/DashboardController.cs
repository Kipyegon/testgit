﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taksi.Models;
using DataAccess.SQL;
using Newtonsoft.Json;
using Taksi.Classes;

namespace Taksi.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard

        ResponseModel _rm = new ResponseModel();
        [Authorize]
        public ActionResult Index()
        {
            ViewData["payment_mode"] = PopulatePaymentModeData();
            List<DispatchPaymentMode> dipatch = new List<DispatchPaymentMode>();
            return View();
        }

        public List<DispatchPaymentMode> PopulatePaymentModeData(Dispatch dispatch = null)
       {
            var allPaymentMode = new GFleetModelContainer().PaymentModes;
            var dispatchPaymentMode = dispatch != null ? new HashSet<int>(dispatch.PaymentMode.Dispatches.Select(c => c.PaymentModeId)) : new HashSet<int>();
            var viewModel = new List<DispatchPaymentMode>();
            foreach (var paymentMode in allPaymentMode)
            {
                viewModel.Add(new DispatchPaymentMode
                {
                    PaymentModeId = paymentMode.PaymentModeId,
                    PaymentMode = paymentMode.PaymentMode1,
                    Assigned = dispatchPaymentMode.Contains(paymentMode.PaymentModeId)
                });
            }
            return viewModel;
        }

        [HttpPost]
        [Authorize]
        public ActionResult Book(string pickup, string pickupxy, string dropoff, string dropoffxy, int passengers,
           string Date, int paymentModeId, string Instruction, bool? BookingImmediate, string DeviceType, string OSVersion, string AppVersion, string BuildVersion)
        {
            ResponseModel rm;
            try
            {
                DashBoardModel saveBooking = new DashBoardModel();
                Guid bookedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                DateTime DateModified = DateTime.Parse(DateTime.Now.ToString());
                int ClientId = Service.Utility.getClientId().Value;
                int OrganisationId = Service.Utility.getOrganisationId().Value;
                int CustomerId = Service.Utility.getCustomerId().Value;
                string OrgName = Service.Utility.getOrganisationName(OrganisationId);
                string BookingRequestedBy = Service.Utility.getCustomerName(User.Identity.Name);


                 rm = saveBooking.saveBooking(pickup, pickupxy, dropoff, dropoffxy, passengers, Date, bookedBy, DateModified,
                    CustomerId, ClientId, paymentModeId, Instruction, BookingRequestedBy,BookingImmediate.Value,DeviceType,OSVersion,AppVersion,BuildVersion);
            }
            catch (Exception ex)
            {
                rm = new ResponseModel() { Message = "Could not submit Data, Contact System Admin", Status = false };
            }

            return this.Json(rm);
        }

        

        [HttpPost]
        [Authorize]
        public ActionResult GetBookData()
        {
            DashBoardModel mbookdata = new DashBoardModel();
            try
            {
                mbookdata = DashBoardModel.getBookData();
                //ViewBag.BookingCount = mbookdata.OrgBookData.Count;
                var Response = Json(mbookdata);
                return Response;

            }
            catch (Exception ex)
            {
                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

                return Json(mbookdata);
            }

        }
        [Authorize]
        public ActionResult GetApprovalData()
        {
            return View("GetApproval");
        }
        public ActionResult GetAdminData()
        {
            DashBoardModel mAdmindata = new DashBoardModel();
            try
            {
                mAdmindata = DashBoardModel.getAdminData();
                var Response = Json(mAdmindata);
                return Response;

            }
            catch (Exception ex)
            {
                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

                return Json(mAdmindata);
            }

        }
        [Authorize]
        public JsonResult getLatestBookData(string highest)
        {
            DateTime date;
            if (DateTime.TryParse(highest, out date))
            {
                return Json(DashBoardModel.GetLatestBookData(date));
            }
            else
            {
                return Json("Wrong Date: " + highest);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateDispatchStatus(int DispatchId, int DispatchStatusId)
        {
            DashBoardModel dispatchStatus = new DashBoardModel();
            var data = dispatchStatus.UpdateDispatchStatus(DispatchId, DispatchStatusId);

            return Json(data);
        }
        
        public ActionResult UpdateRating(int DispatchId, int Rating)
        {
            DashBoardModel dispatchRating = new DashBoardModel();
            var data = dispatchRating.UpdateRating(DispatchId, Rating);

            return Json(data);
        }
        [HttpPost]
        public ActionResult Update_Booking() 
        {
            ResponseModel rm;
            rm = new ResponseModel() { Message="Data Received and is being processed"};
            return Json(rm);
        }
    }
}