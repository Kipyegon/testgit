﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Taksi.Models;
using System.Net.Mail;
using System.Web.Configuration;
using Newtonsoft.Json;
using DataAccess.SQL;

namespace Taksi.Controllers
{
    public class DepartmentAdminController : Controller
    {
        [Authorize]
        //[Authorization]
        public ActionResult Index()
        {
            var deptadmins = ModelOrganisation.getDepartmentAdmins();
            ViewBag.Label = "DepartmentAdmin";
            return View(deptadmins.ToList());
        }

        public ActionResult CreateEditUser(bool? edit)
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
            }
            PopulatePersonTitle();
            PopulateDepartmentDropDownList();
            var data = 0;
            if (edit.HasValue && edit.Value)
            {
                data = 1;
                ViewData["heading"] = "Edit Deparment Admin";
            }
            else
            {
                ViewData["heading"] = "New Deparment Admin";
            }

            return View("_DepartmentAdmin");
        }
        [HttpPost]
        [Authorize]
        public ActionResult CreateEditDepartmentAdmin(OrganisationRegisterModel mDepartmentAdmin, string Command)
        {
            DepartmentAdminModel DeparmentAdminModel = new DepartmentAdminModel();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mDepartmentAdmin.DepartmentAdminPersonModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDepartmentAdmin.DepartmentAdminPersonModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                //mDepartmentAdmin.DepartmentAdminPersonModel.ClientId = Service.Utility.getClientId().Value;
                mDepartmentAdmin.DepartmentAdminPersonModel.OrganisationId = Service.Utility.getOrganisationId().Value;
                //int OrganisationId = Service.Utility.getOrganisationId().Value;

                return this.Json(DeparmentAdminModel.CreateDepartmentAdmin(mDepartmentAdmin));
                //return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mDepartmentAdmin.DepartmentAdminPersonModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDepartmentAdmin.DepartmentAdminPersonModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mDepartmentAdmin.DepartmentAdminPersonModel.ClientId = Service.Utility.getClientId().Value;
                //return this.Json(customerModel.UpdateCustomer(mDepartmentAdmin.OrganisationEndUserModel, File));
                return this.Json(null);

            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        private void PopulateCustomerTypeDropDownList(object CustomerType = null)
        {
            var CustomerTypeQuery = from d in new GFleetModelContainer().CustomerTypes
                                    orderby d.CustomerTypeName
                                    select d;
            ViewBag.CustomerType = new SelectList(CustomerTypeQuery, "CustomerTypeId", "CustomerTypeName", CustomerType);
        }

        private void PopulateOrganisationDropDownList(object Organisation = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var OrganisationQuery = from d in new GFleetModelContainer().Organisations.Where(v => v.ClientId == ClientId)
                                    orderby d.OrganisationName
                                    select d;
            ViewBag.Organisation = new SelectList(OrganisationQuery, "OrganisationId", "OrganisationName", Organisation);
        }

        private void PopulatePersonTitle(object title = null)
        {
            List<string> PersonTitle = System.Configuration.ConfigurationManager.AppSettings["personTitle"].ToString().Split(',').ToList<string>();

            List<SelectListItem> sli = new List<SelectListItem>();

            foreach (string item in PersonTitle)
            {
                sli.Add(new SelectListItem()
                {
                    Value = item,
                    Text = item
                });
            }

            ViewBag.PersonTitle = new SelectList(sli, "Value", "Text", title);
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] OrganisationRoles = System.Configuration.ConfigurationManager.AppSettings["OrganisationRole"].ToString().Split(',');
            var RolesQuery = from d in new GFleetModelContainer().aspnet_Roles.Where(m => OrganisationRoles.Contains(m.RoleName))
                             orderby d.RoleName
                             select d;
            ViewBag.RoleName = new SelectList(RolesQuery, "RoleName", "RoleName", RoleName);
        }

        private void PopulateDepartmentDropDownList(object OrganisationDepartment = null)
        {
            var DepartmentQuery = from d in new GFleetModelContainer().OrganisationDepartments
                                  orderby d.DepartmentName
                                  select d;
            ViewBag.Department = new SelectList(DepartmentQuery, "OrganisationDeptId", "DepartmentName", OrganisationDepartment);
        }
        [Authorize]
        //[Authorization]
        public ActionResult DoResetPassword(string Username)
        {
            DepartmentAdminModel modeldeptadmin = new DepartmentAdminModel();
            ResponseModel response = modeldeptadmin.ResetPassword(Username);

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult DeleteUser(int PersonId, string Username)
        {
            ModelOrganisation modelorg = new ModelOrganisation();
            ResponseModel response = modelorg.DeleteEndUser(PersonId, Username);

            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}