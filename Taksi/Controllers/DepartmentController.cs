﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taksi.Models;
using DataAccess.SQL;
using System.Threading;

namespace Taksi.Controllers
{
    public class DepartmentController : Controller
    {
        [Authorize]
        //[Authorization]
        public ActionResult Index()
        {
            var edepts = ModelOrganisation.GetOrganisationDepartment();
            ViewBag.Label = "Department";
            return View(edepts.ToList());
        }

        //[Authorize]
        //[Authorization]
        public ActionResult Create(bool? edit)
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
            }
            var data = 0;
            if (edit.HasValue && edit.Value)
            {
                data = 1;
                ViewData["heading"] = "Edit Deparment";
            }
            else
            {
                ViewData["heading"] = "New Deparment";
            }

            return View("_Department");
        }

        //
        // POST: /OrganisationDepartment/Create

        [HttpPost]

        //[Authorize]
        //[Authorization]
        public ActionResult createEditOrganisationDepartment(OrganisationDepartment mDepartment, string Command)
        {
            DepartmentModel departmentModel = new DepartmentModel();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mDepartment.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDepartment.DateModified = DateTime.Parse(DateTime.Now.ToString());
                ResponseModel rm = departmentModel.CreateOrganisationDepartment(mDepartment);
                return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mDepartment.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mDepartment.DateModified = DateTime.Parse(DateTime.Now.ToString());
                return this.Json(departmentModel.UpdateOrganisationDepartment(mDepartment));
            }

            ErrorModel errModX = new ErrorModel();
            errModX.Errors.Add("Unrecognized operation, kindly refresh browser and retry.");
            return View("_Error", errModX);
        }
        //
        // GET: /OrganisationDepartment/Edit/5

        //[Authorize]
        //[Authorization]
        public ActionResult Edit(int id)
        {
            DepartmentModel departmentModel = new DepartmentModel();
            var data = departmentModel.GetOrganisationDepartment(id);
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = true;
            }
            return View("_Department", data);
        }

        //
        // GET: /OrganisationDepartment/Delete/5

        //[Authorize]
        [Authorization]
        public ActionResult Delete(int id)
        {
            return View();
        }
    }
}