﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Taksi.Models;

namespace Taksi.Controllers
{
    public class Mserver_V1Controller : Controller
    {
        // GET: Mserver_V1
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Login(string Username, string Password)
        {
            MServer_V1Model mlogin = new MServer_V1Model();

            try
            {
                Username = Username.Replace("+", "");
                Boolean uservalid = Membership.ValidateUser(Username, Password);
                MembershipUser user = Membership.GetUser(Username);

                if (!uservalid)
                {
                    mlogin.IsAuthenticated = false;

                    if (user != null)
                    {
                        if (!user.IsApproved)
                        {
                            mlogin.AuthMessage = "Your Account is not Approved";
                        }
                        else if (user.IsLockedOut)
                        {
                            mlogin.AuthMessage = "Your Account is locked";
                        }
                        else
                        {
                            mlogin.AuthMessage = "Invalid Username or Password";
                        }
                    }
                    else
                    {
                        mlogin.AuthMessage = "Invalid Username or Password";
                    }
                    return Json(mlogin);
                }
                else
                {
                    string[] InvalidLoginRoles = WebConfigurationManager.AppSettings["validLoginRoles"].Split(',');

                    foreach (string role in Roles.GetRolesForUser(Username))
                    {
                        if (!InvalidLoginRoles.Contains(role))
                        {
                            mlogin.IsAuthenticated = false;
                            mlogin.AuthMessage = "User not authorized on this platform.";
                            return Json(mlogin);
                        }
                    }


                    //Client_Phone.RegisterDevice(Phone_token, Guid.Parse(user.ProviderUserKey.ToString()));
                    mlogin = MServer_V1Model.getValidatedUser(Guid.Parse(user.ProviderUserKey.ToString()), Username);

                    if (mlogin == null)
                    {
                        mlogin = new MServer_V1Model();
                        mlogin.IsAuthenticated = false;
                        mlogin.AuthMessage = "Credentials used is not a contact type. Kindly contact the administrator";

                        return Json(mlogin);
                    }
                    else
                    {

                        mlogin.IsAuthenticated = true;
                        mlogin.AuthMessage = "Login Successfull";

                        var Response = Json(mlogin);
                        return Response;
                    }

                }
            }
            catch (Exception ex)
            {
                mlogin.AuthMessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                mlogin.IsAuthenticated = false;

                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

                return Json(mlogin);
            }
        }

        [HttpPost]
        public ActionResult Book(BookingData bookdata, string Date, string Username)
        {
            ResponseModel rm;
            try
            {
                DashBoardModel saveBooking = new DashBoardModel(bookdata.OrganisationId);
                //Guid bookedBy = Guid.Parse(bookdata.UserId);
                DateTime DateModified = DateTime.Parse(DateTime.Now.ToString());



                rm = saveBooking.saveBooking(bookdata.pickup, bookdata.pickupxy, bookdata.dropoff, bookdata.dropoffxy, bookdata.passengers, bookdata.Date, Guid.Parse(bookdata.UserId), DateModified,
                   bookdata.CustomerId, bookdata.ClientId, bookdata.paymentModeId, bookdata.Instruction, bookdata.Username, true, bookdata.DeviceType, bookdata.OSVersion, bookdata.AppVersion, bookdata.BuildVersion);
            }
            catch (Exception ex)
            {
                rm = new ResponseModel()
                {
                    Message = "Could not submit Data, Contact System Admin",
                    Status = false

                };
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);
            }

            return this.Json(rm);
        }

        [HttpPost]
        public ActionResult GetBookData(int ClientID, string username, int organizationId, int CustId)
        {
            DashBoardModel mbookdata = new DashBoardModel(organizationId);
            try
            {
                mbookdata = DashBoardModel.getBookData(ClientID, username, organizationId, CustId);
                var Response = Json(mbookdata);
                return Response;

            }
            catch (Exception ex)
            {
                //logToFile(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, System.Web.HttpContext.Current);

                return Json(mbookdata);
            }

        }

        [HttpPost]
        public JsonResult getLatestBookData(string highest, int ClientID, string username, int organizationId, int CustId)
        {
            ResponseModel rm;
            DateTime date;
            if (DateTime.TryParse(highest, out date))
            {
                return Json(DashBoardModel.GetLatestBookData(date, ClientID, username, organizationId, CustId));
            }
            else
            {
                rm = new ResponseModel()
                {
                    Message = "Wrong Date: " + highest,
                    Status = false

                };
                return this.Json(rm);
                //return Json("Wrong Date: " + highest);
            }
        }

        [HttpPost]
        public ActionResult UpdateDispatchStatus(int DispatchId, int DispatchStatusId)
        {
            DashBoardModel dispatchStatus = new DashBoardModel();
            var data = dispatchStatus.UpdateDispatchStatus(DispatchId, DispatchStatusId);

            return Json(data);
        }

        [HttpPost]
        public ActionResult UpdateRating(int DispatchId, int Rating)
        {
            DashBoardModel dispatchRating = new DashBoardModel();
            var data = dispatchRating.UpdateRating(DispatchId, Rating);

            return Json(data);
        }

        [HttpPost]
        public ActionResult Update_Booking()
        {
            ResponseModel rm;
            rm = new ResponseModel() { Message = "Data Received and is being processed" };
            return Json(rm);
        }

        [HttpPost]
        public ActionResult checkUserExists(string UserId)
        {
            MembershipProvider pmp = Membership.Provider;
            MServer_V1Model mversion = new MServer_V1Model();
            Guid UserIdGuid;
            ResponseModel rm;

            if (Guid.TryParse(UserId, out UserIdGuid))
            {

                rm = new ResponseModel()
                {

                    Status = pmp.GetUser(UserIdGuid, true) != null
                };
            }
            else
            {
                rm = new ResponseModel()
                {
                    Status = false
                };
            }

            //rm = new ResponseModel() { Message = "Data Received and is being processed" };
            return Json(rm);
        }
        [HttpPost]
        public ActionResult checkUserExistsV1(string UserId, string MobileType)
        {
            MembershipProvider pmp = Membership.Provider;
            MServer_V1Model mversion = new MServer_V1Model();
            Guid UserIdGuid;
            MobileType = MobileType.Trim().ToLower();
            mversion.MobileBuildVersion = MServer_V1Model.getMobileBuildVersion(MobileType);
            if (Guid.TryParse(UserId, out UserIdGuid))
            {
                mversion.UserStatus = pmp.GetUser(UserIdGuid, true) != null;
            }
            else
            {
                mversion.UserStatus = false;
            }
            return Json(mversion);
        }
    }




}