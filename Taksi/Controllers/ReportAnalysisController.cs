﻿using DataAccess.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taksi.Models;

namespace Taksi.Controllers
{
    public class ReportAnalysisController : Controller 
    {
            // GET: ReportAnalysis/Create
            [Authorize]
            //[Authorization]
            public ActionResult Create()
            {
                PopulateVehicleDropDownList();
                PopulateOrganisationDropDownList();
                PopulateCustomerDropDownList();
                PopulateDriverDropDownList();
                PopulateSMSStatusDropDownList();
                PopulatePassengerOnBoardDropDownList();
                PopulateCallBackDropDownList();
                return PartialView("_ReportAnalysis");
            }
            [Authorize]
            //[Authorization]
            public ActionResult GenerateReport(DispatchRptParam mReportParam)
            {
                return View("ReportControls", ReportAnalysis.GetDispatchRpt(mReportParam));

            }

            [Authorize]
            //[Authorization]
            public ActionResult export(string reportType, string StartDate, string EndDate, string OrganisationId,
                string CustomerId, string VehicleId, bool IsDispactchedChk, bool IsNotDispactchedChk, string SmsStatusId, string PassengerOnBoardId, string CallBackId)
            {
                DateTime sdate;
                DateTime edate;
                if (!validateReportInput(StartDate, EndDate, out  sdate, out  edate))
                {
                    return View();
                }
                string spacer = " " + Environment.NewLine + " ";

                List<object> data = new List<object>();
                List<vw_DispatchReport> values = ReportAnalysis.GetDispatchRptDownload(sdate, edate, OrganisationId,CustomerId, VehicleId, IsDispactchedChk, IsNotDispactchedChk, SmsStatusId, PassengerOnBoardId, CallBackId);
                data.AddRange(values.Select(m => new
                {
                    Date = m.DateModified,
                    Organization = m.OrganisationName,
                    Customer = m.CustomerName,
                    Vehicle = m.VehicleRegCode,
                    From = m.PickUpPointName,
                    To = m.DropOffPointName,
                    Driver = m.DriverName,
                    DispatchStatus = m.DispatchStatus,
                    POBStatus = m.POBStatus,
                    CallBackName = m.CallBackName,
                    MsgStatus = m.Status,
                    SMS = m.CustomerSMS
                }));

                return new ExportResult<object>
                (
                    ControllerContext,
                    "Dispatch report (" + sdate.ToString() + ") - (" + edate.ToString() + ")",
                    data,
                    reportType
                );
            }

            private Boolean validateReportInput(string StartDate, string EndDate, out DateTime sdate, out DateTime edate)
            {
                sdate = DateTime.Now;
                edate = DateTime.Now;


                EndDate = HttpUtility.UrlDecode(EndDate);     //endDate = endDate.Replace("+", ""); 
                StartDate = HttpUtility.UrlDecode(StartDate);   //startDate = startDate.Replace("+", "");

                //validate start date
                if (string.IsNullOrEmpty(StartDate))
                {
                    ViewBag.message = "A start date must be selected!!";
                    return false;
                }
                if (!DateTime.TryParse(StartDate, out sdate))
                {
                    string fdate = StartDate;
                    string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                        + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : "");
                    if (!DateTime.TryParse(otherDate, out sdate))
                    {
                        ViewBag.message = "Invalid start date specified!!";
                        return false;
                    }
                }
                StartDate = sdate.ToString("dd/MM/yyyy HH:mm:ss");

                //validate end date
                if (string.IsNullOrEmpty(EndDate))
                {
                    ViewBag.message = "An end date must be selected!!";
                    return false;
                }
                if (!DateTime.TryParse(EndDate, out edate))
                {
                    string fdate = EndDate;
                    string otherDate = fdate.Substring(3, 3) + fdate.Substring(0, 3) + fdate.Substring(6, 4)
                        + (fdate.Length > 10 ? fdate.Substring(11, fdate.Length - 11) : " 23:59:59.999");
                    if (!DateTime.TryParse(otherDate, out edate))
                    {
                        ViewBag.message = "Invalid start date specified!!";
                        return false;
                    }
                }
                //edate = edate.Add(TimeSpan.FromDays(1)).AddMilliseconds(-1);

                return true;
            }

            private void PopulateVehicleDropDownList(object Vehicle = null)
            {
                int ClientId = Service.Utility.getClientId().Value;
                var VehicleQuery = from d in new GFleetModelContainer().ClientVehicles.Where(v => v.ClientId == ClientId)
                                   orderby d.Vehicle.RegistrationNo
                                   select new { VehicleId = d.VehicleId, RegistrationNo = (d.Vehicle.RegistrationNo + " (" + d.Vehicle.NickName + ")") };
                ViewBag.Vehicle = new SelectList(VehicleQuery, "VehicleId", "RegistrationNo", Vehicle);
            }

            private void PopulateOrganisationDropDownList(object Organisation = null)
            {
                int ClientId = Service.Utility.getClientId().Value;
                var OrganisationQuery = from d in new GFleetModelContainer().Organisations.Where(v => v.ClientId == ClientId)
                                        orderby d.OrganisationName
                                        select d;
                ViewBag.Organisation = new SelectList(OrganisationQuery, "OrganisationId", "OrganisationName", Organisation);
            }

            private void PopulateCustomerDropDownList(object Customer = null)
            {
                int ClientId = Service.Utility.getClientId().Value;
                var CustomerQuery = from d in new GFleetModelContainer().People.OfType<Customer>().Where(v => v.ClientId == ClientId)
                                    orderby d.FirstName
                                    select new { PersonId = d.PersonId, Name = d.FirstName + d.LastName };
                ViewBag.Customer = new SelectList(CustomerQuery, "PersonId", "Name", Customer);
            }

            private void PopulateDriverDropDownList(object Driver = null)
            {
                int ClientId = Service.Utility.getClientId().Value;
                var DriverQuery = from d in new GFleetModelContainer().People.OfType<Driver>().Where(v => v.ClientId == ClientId)
                                  orderby d.FirstName
                                  select new { PersonId = d.PersonId, Name = d.DriverNameNumber };
                ViewBag.Driver = new SelectList(DriverQuery, "PersonId", "Name", Driver);
            }

            private void PopulatePassengerOnBoardDropDownList(object POBStatus = null)
            {
                var PassengerOnBoardQuery = from d in new GFleetModelContainer().PassengerOnBoards
                                            orderby d.POBStatus
                                            select new { PassengerOnBoardId = d.PassengerOnBoardId, POBStatus = d.POBStatus };
                ViewBag.PassengerOnBoard = new SelectList(PassengerOnBoardQuery, "PassengerOnBoardId", "POBStatus", POBStatus);
            }

            private void PopulateCallBackDropDownList(object CallBack = null)
            {
                var CallBackQuery = from d in new GFleetModelContainer().CallBacks
                                    orderby d.CallBackName
                                    select new { CallBackId = d.CallBackId, CallBack = d.CallBackName };
                ViewBag.CallBack = new SelectList(CallBackQuery, "CallBackId", "CallBack", CallBack);
            }

            private void PopulateSMSStatusDropDownList(object SmsStatus = null)
            {

                var SmsStatusQuery = from d in new GFleetModelContainer().vw_SmsStatus
                                     orderby d.Status
                                     select d;
                ViewBag.SmsStatus = new SelectList(SmsStatusQuery, "StatusId", "Status", SmsStatus);
            }
    }
}
