﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Taksi.Models;
using DataAccess.SQL;
using System.Web.Configuration;
using System.Web.Routing;
using System.Web.Security;

namespace Taksi.Controllers
{
    public class UsersController : Controller
    {

        [Authorize]
        //[Authorization]
        public ActionResult Index()
        {
            var econtacts = ModelOrganisation.getContacts();
            ViewBag.Label = "Users";
            return View(econtacts.ToList());
        }

        public ActionResult CreateEditUser(bool? edit)
        {
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsUpdate = false;
            }
            PopulateCustomerTypeDropDownList();
            PopulateDepartmentDropDownList();
            PopulatePersonTitle();
            PopulateRoleDropDownList();
            var data = 0;
            if (edit.HasValue && edit.Value)
            {
                data = 1;
                ViewData["heading"] = "Edit User";
            }
            else
            {
                ViewData["heading"] = "New User";
            }

            return View("CreateEditUser");
        }
        [HttpPost]
        [Authorize]
        public ActionResult CreateEditCustomer(OrganisationRegisterModel mCustomerReg, string Command)
        {
            HttpPostedFileBase File = Request.Files["CustomerDirUrl"];//mCustomer.CustomerDirUrl.ToString(); 
            ModelOrganisation OrganisationModel = new ModelOrganisation();
            if (!ModelState.IsValid)
            {
                ErrorModel errMod = new ErrorModel();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errMod.Errors.Add(error.ErrorMessage);
                    }
                }
                return View("_Error", errMod);
            }
            else if (Command == "Save")
            {
                mCustomerReg.OrganisationEndUserModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mCustomerReg.OrganisationEndUserModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mCustomerReg.OrganisationEndUserModel.ClientId = Service.Utility.getClientId().Value;
                //mCustomerReg.OrganisationEndUserModel.OrganisationDeptId = Service.Utility.getDepartmentId().Value;

                if (File != null)
                {
                    if (File.ContentLength > 10240000)
                    {
                        ModelState.AddModelError("file", "The size of the file should not exceed 10 MB");
                        return View();
                    }
                }

                //ResponseModel rm = customerModel.customerModel(mCustomer,File);
                return View("CreateEditUser");
                //return this.Json(OrganisationModel.CreateOrganisationUser(mCustomerReg, File));
                //return this.Json(rm);
            }

            else if (Command == "Update")
            {
                mCustomerReg.OrganisationEndUserModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                mCustomerReg.OrganisationEndUserModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
                mCustomerReg.OrganisationEndUserModel.ClientId = Service.Utility.getClientId().Value;
                //return this.Json(customerModel.UpdateCustomer(mCustomerReg.OrganisationEndUserModel, File));
                return this.Json(null);

            }

            return Content("Unrecognized operation, kindly refresh browser and retry.");
        }

        //CreateDeptAdmin
        //[HttpPost]
        //[Authorize]
        public ActionResult CreateEditDepartmentAdmin(string Username)
        {
            ResponseModel rm = new ResponseModel();
            DepartmentAdminModel DeptAdminModel = new DepartmentAdminModel();
            ResponseModel mDepartAdmin = DeptAdminModel.CreateExistingDepartmentAdmin(Username);
            //return Json(mDepartAdmin, JsonRequestBehavior.AllowGet);
            return View("Index");
        }
           // MServer_DriverV1Model mregistertoken = new MServer_DriverV1Model();
            //ResponseModel mregistered = mregistertoken.RegisterDriverDevice(Device_token, RegistrationNo);
            //return Json(mregistered);
        //    ResponseModel rm;
        //    try
        //    {
        //        //if (!ModelState.IsValid)
        //        //{
        //        //    ErrorModel errMod = new ErrorModel();
        //        //    foreach (ModelState modelState in ViewData.ModelState.Values)
        //        //    {
        //        //        foreach (ModelError error in modelState.Errors)
        //        //        {
        //        //            errMod.Errors.Add(error.ErrorMessage);
        //        //        }
        //        //    }
        //        //    return View("_Error", errMod);
        //        //}
        //        //else 
        //        //{
        //        mDepartmentAdmin.DepartmentAdminPersonModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
        //        mDepartmentAdmin.DepartmentAdminPersonModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
        //        mDepartmentAdmin.DepartmentAdminPersonModel.ClientId = Service.Utility.getClientId().Value;
        //        mDepartmentAdmin.DepartmentAdminPersonModel.OrganisationId = Service.Utility.getOrganisationId().Value;
        //        int OrganisationId = Service.Utility.getOrganisationId().Value;

        //        return this.Json(DeparmentAdminModel.CreateDepartmentAdmin(mDepartmentAdmin,Username));
        //            //return this.Json(rm);
        //        //}

        //        //else if (Command == "Update")
        //        //{
        //        //    mDepartmentAdmin.DepartmentAdminPersonModel.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
        //        //    mDepartmentAdmin.DepartmentAdminPersonModel.DateModified = DateTime.Parse(DateTime.Now.ToString());
        //        //    mDepartmentAdmin.DepartmentAdminPersonModel.ClientId = Service.Utility.getClientId().Value;
        //        //    //return this.Json(customerModel.UpdateCustomer(mDepartmentAdmin.OrganisationEndUserModel, File));
        //        //    return this.Json(null);

        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        rm = new ResponseModel() { Message = "User cannot be made the department admin.", Status = false };
        //    }
        //    return Content("Unrecognized operation, kindly refresh browser and retry.");
        //}

        [Authorize]
        //[Authorization]
        public ActionResult DoResetPassword(string Username)
        {
            ModelOrganisation modelorg = new ModelOrganisation();
            ResponseModel response = modelorg.ResetPassword(Username);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DeleteUser(int PersonId,string Username)
        {
            ModelOrganisation modelorg = new ModelOrganisation();
            ResponseModel response = modelorg.DeleteEndUser(PersonId,Username);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private void PopulateCustomerTypeDropDownList(object CustomerType = null)
        {
            var CustomerTypeQuery = from d in new GFleetModelContainer().CustomerTypes
                                    orderby d.CustomerTypeName
                                    select d;
            ViewBag.CustomerType = new SelectList(CustomerTypeQuery, "CustomerTypeId", "CustomerTypeName", CustomerType);
        }

        private void PopulateDepartmentDropDownList(object OrganisationDepartment = null)
        {
            var DepartmentQuery = from d in new GFleetModelContainer().OrganisationDepartments
                                  orderby d.DepartmentName
                                  select d;
            ViewBag.Department = new SelectList(DepartmentQuery, "OrganisationDeptId", "DepartmentName", OrganisationDepartment);
        }

        private void PopulateOrganisationDropDownList(object Organisation = null)
        {
            int ClientId = Service.Utility.getClientId().Value;
            var OrganisationQuery = from d in new GFleetModelContainer().Organisations.Where(v => v.ClientId == ClientId)
                                    orderby d.OrganisationName
                                    select d;
            ViewBag.Organisation = new SelectList(OrganisationQuery, "OrganisationId", "OrganisationName", Organisation);
        }

        private void PopulatePersonTitle(object title = null)
        {
            List<string> PersonTitle = System.Configuration.ConfigurationManager.AppSettings["personTitle"].ToString().Split(',').ToList<string>();

            List<SelectListItem> sli = new List<SelectListItem>();

            foreach (string item in PersonTitle)
            {
                sli.Add(new SelectListItem()
                {
                    Value = item,
                    Text = item
                });
            }

            ViewBag.PersonTitle = new SelectList(sli, "Value", "Text", title);
        }

        private void PopulateRoleDropDownList(object RoleName = null)
        {
            string[] OrganisationRoles = System.Configuration.ConfigurationManager.AppSettings["OrganisationRole"].ToString().Split(',');
            var RolesQuery = from d in new GFleetModelContainer().aspnet_Roles.Where(m => OrganisationRoles.Contains(m.RoleName))
                             orderby d.RoleName
                             select d;
            ViewBag.RoleName = new SelectList(RolesQuery, "RoleName", "RoleName", RoleName);
        }

    }
}