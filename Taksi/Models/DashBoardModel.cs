﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DataAccess.SQL;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Reflection;
using System.Web.Security;
using System.Text;

namespace Taksi.Models
{
    public class DispatchLatestDataParams
    {
        public DispatchLatestDataParams(DateTime dispatchDateTime, int dispatchId)
        {
            this.DispatchDateTime = dispatchDateTime;
            this.DispatchId = dispatchId;
        }
        public DateTime DispatchDateTime { get; set; }
        public int DispatchId { get; set; }
    }
    public class DashBoardModel
    {

        //private readonly GFleetModelContainer entities = new GFleetModelContainer();
        public int OrganisationId { get; set; }

        public class BookData
        {
            public string PickUpPoint { get; set; }
            public string DropOffPoint { get; set; }
            public string PickUpDateTime { get; set; }
            public DateTime DateModified { get; set; }
            public string strDModified
            {
                get
                {
                    return DateModified.ToString("yyyy-MM-dd HH:mm:ss.fff");
                }
            }
            public string BookedDate { get; set; }
            public int? NoOfPassengers { get; set; }
            public int? CustomerId { get; set; }

            public string Customername { get; set; }
            public string Drivernumber { get; set; }
            public int? OrganisationId { get; set; }
            public int? DispatchId { get; set; }
            public string DispatchStatus { get; set; }
            public int? Status { get; set; }
            public int? Rating { get; set; }
            public string PickUpdate { get; set; }
            public string RegCode { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public string ImgUrl { get; set; }
            public string Drivername { get; set; }
            public string Company { get; set; }
            public bool? isVehicleVisible { get; set; }
            public string BrandName { get; set; }
        }

        public class OrganisationAdmin
        {
            public Guid UserId { get; set; }
            public int? OrganisationId { get; set; }
            public string Phones { get; set; }
        }

        public static List<OrganisationAdmin> GetOrgAdminData(int OrganisationId)
        {
            try
            {
                string picktoday = DateTime.Now.ToString("yyyy.MM.dd");
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    var data = (from p in entities.People.OfType<SystemUser>()
                                select new OrganisationAdmin
                                {
                                    UserId = p.UserId.Value,
                                    OrganisationId = p.OrganisationId,
                                    Phones = p.Telephone1,


                                }).Where(v => v.OrganisationId == OrganisationId).ToList();
                    return data;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }

        public static OrganisationAdmin GetOrgAdminPhone(int OrganisationId)
        {
            try
            {
                string picktoday = DateTime.Now.ToString("yyyy.MM.dd");
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    var data = (from p in entities.Organisations
                                select new OrganisationAdmin
                                {
                                    Phones = p.OrganisationTelephone


                                }).Where(v => v.OrganisationId == OrganisationId).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }

        public static List<OrganisationAdmin> GetOrgAdminData()
        {
            int OrganisationId = Service.Utility.getOrganisationId().Value;
            return GetOrgAdminData(OrganisationId);
            //try
            //{
            //    int OrganisationId = Service.Utility.getOrganisationId().Value;
            //    string picktoday = DateTime.Now.ToString("yyyy.MM.dd");
            //    using (GFleetModelContainer entities = new GFleetModelContainer())
            //    {
            //        var data = (from p in entities.People.OfType<SystemUser>()
            //                    select new OrganisationAdmin
            //                    {
            //                        UserId = p.UserId.Value,
            //                        OrganisationId = p.OrganisationId

            //                    }).Where(v => v.OrganisationId == OrganisationId).ToList();
            //        return data;
            //    }
            //}
            //catch (Exception mex)
            //{
            //    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
            //    return null;
            //}
        }

        public static string getOrgAdminsEmails()
        {
            List<OrganisationAdmin> data = GetOrgAdminData();
            List<string> userids = new List<string>();
            foreach (OrganisationAdmin oa in data)
            {
                userids.Add(oa.UserId.ToString());
            }

            if (userids.Count > 0)
            {
                string ids = String.Join(",", userids);
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    return String.Join(",", entities.GetUsermailsByUserId(ids));
                }
            }
            else
            {
                return null;
            }

        }

        public static string getOrgAdminsEmails(int OrganisationId)
        {
            List<OrganisationAdmin> data = GetOrgAdminData(OrganisationId);
            List<string> userids = new List<string>();
            foreach (OrganisationAdmin oa in data)
            {
                userids.Add(oa.UserId.ToString());
            }

            if (userids.Count > 0)
            {
                string ids = String.Join(",", userids);
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    return String.Join(",", entities.GetUsermailsByUserId(ids));
                }
            }
            else
            {
                return null;
            }

        }
        public static string getOrgAdminsPhones(int OrganisationId)
        {
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Organisation orddata = entities.Organisations.Where(s => s.OrganisationId == OrganisationId).FirstOrDefault();
                    if (orddata != null)
                    {
                        string OrgPhone = orddata.OrganisationTelephone;
                        return OrgPhone;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }

        }
        public static List<BookData> GetBookData()
        {
            try
            {
                int OrganisationId = Service.Utility.getOrganisationId().Value;
                int CustomerId = Service.Utility.getCustomerId().Value;
                int? DepartmentId = Service.Utility.getDepartmentId();
                string picktoday = DateTime.Now.ToString("yyyy.MM.dd");
                string brand = Service.Utility.getClientBrandName();
                if (Roles.IsUserInRole("Organisation Admin"))
                {

                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        var data = (from p in entities.getDispactBookDataOrg(OrganisationId)
                                    select new BookData
                                    {
                                        PickUpPoint = p.Pickuppointname,
                                        DropOffPoint = p.Dropoffpointname,
                                        PickUpDateTime = p.PickUpDateTime,
                                        BookedDate = p.BookedDate,
                                        NoOfPassengers = p.NoOfPassengers,
                                        DispatchStatus = p.DispatchStatus,
                                        CustomerId = p.CustomerId,
                                        Customername = p.Customername,
                                        OrganisationId = p.OrganisationId,
                                        DispatchId = p.DispatchId,
                                        Status = p.DispatchStatusId,
                                        //Rating = p.Rating,
                                        Rating = p.Rating != null ? 0 : p.Rating,
                                        PickUpdate = p.PickUpDate,
                                        DateModified = p.DateModified.Value,
                                        isVehicleVisible = p.IsVehicleVisible,
                                        RegCode = p.VehicleRegCode,
                                        Latitude = p.Latitude,
                                        Longitude = p.Longitude,
                                        ImgUrl = p.ImageUrl
                                    }).ToList();
                        return data;
                    }

                }
                else if (Roles.IsUserInRole("OrganisationDepartment Admin"))
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        var data = (from p in entities.getDispactBookDataDeptAdmin(DepartmentId, OrganisationId)
                                    select new BookData
                                    {
                                        PickUpPoint = p.Pickuppointname,
                                        DropOffPoint = p.Dropoffpointname,
                                        PickUpDateTime = p.PickUpDateTime,
                                        BookedDate = p.BookedDate,
                                        NoOfPassengers = p.NoOfPassengers,
                                        DispatchStatus = p.DispatchStatus,
                                        CustomerId = p.CustomerId,
                                        Customername = p.Customername,
                                        OrganisationId = p.OrganisationId,
                                        DispatchId = p.DispatchId,
                                        Status = p.DispatchStatusId,
                                        Rating = p.Rating,
                                        PickUpdate = p.PickUpDate,
                                        DateModified = p.DateModified.Value,
                                        RegCode = p.VehicleRegCode,
                                        Latitude = p.Latitude,
                                        Longitude = p.Longitude,
                                        ImgUrl = p.ImageUrl
                                    }).ToList();
                        return data;
                    }
                }
                else
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        var data = (from p in entities.getDispactBookDataCustomer(CustomerId, OrganisationId)
                                    select new BookData
                                    {
                                        PickUpPoint = p.Pickuppointname,
                                        DropOffPoint = p.Dropoffpointname,
                                        PickUpDateTime = p.PickUpDateTime,
                                        BookedDate = p.BookedDate,
                                        NoOfPassengers = p.NoOfPassengers,
                                        DispatchStatus = p.DispatchStatus,
                                        CustomerId = p.CustomerId,
                                        Customername = p.Customername,
                                        OrganisationId = p.OrganisationId,
                                        DispatchId = p.DispatchId,
                                        Status = p.DispatchStatusId,
                                        Rating = p.Rating,
                                        PickUpdate = p.PickUpDate,
                                        DateModified = p.DateModified.Value,
                                        RegCode = p.VehicleRegCode,
                                        Latitude = p.Latitude,
                                        Longitude = p.Longitude,
                                        ImgUrl = p.ImageUrl
                                    }).ToList();
                        return data;
                    }
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }

        public static List<BookData> GetBookData(int ClientId, string username, int orgId, int customerid)
        {
            try
            {
                string picktoday = DateTime.Now.ToString("yyyy.MM.dd");
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    var data = (from p in entities.getDispactBookDataCustomer(customerid, orgId)
                                where p.ClientId == ClientId
                                orderby p.DateModified descending
                                select new BookData
                                {
                                    PickUpPoint = p.Pickuppointname,
                                    DropOffPoint = p.Dropoffpointname,
                                    PickUpDateTime = p.PickUpDateTime,
                                    BookedDate = p.BookedDate,
                                    NoOfPassengers = p.NoOfPassengers,
                                    DispatchStatus = p.DispatchStatus,
                                    CustomerId = p.CustomerId,
                                    Customername = p.Customername,
                                    OrganisationId = p.OrganisationId,
                                    DispatchId = p.DispatchId,
                                    Status = p.DispatchStatusId,
                                    Rating = p.Rating,
                                    PickUpdate = p.PickUpDate,
                                    DateModified = p.DateModified.Value,
                                    RegCode = p.VehicleRegCode,
                                    Latitude = p.Latitude,
                                    Longitude = p.Longitude,
                                    ImgUrl = p.ImageUrl,
                                    Drivername = p.Drivername,
                                    Company = p.Organisationname,
                                    isVehicleVisible = p.IsVehicleVisible,
                                    BrandName = p.BrandName,
                                    Drivernumber = p.Drivernumber
                                }).Take(5).ToList();
                    return data;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }

        public static DashBoardModel GetLatestBookData(DateTime highest)
        {
            try
            {
                DashBoardModel bookdata = new DashBoardModel();

                int OrganisationId = Service.Utility.getOrganisationId().Value;
                int CustomerId = Service.Utility.getCustomerId().Value;
                int? DepartmentId = Service.Utility.getDepartmentId();
                string picktoday = DateTime.Now.ToString("yyyy-MM-dd");

                if (Roles.IsUserInRole("Organisation Admin"))
                {

                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        bookdata.OrgBookData = (from p in entities.getLatestDispactBookDataOrg(OrganisationId, highest)
                                                select new BookData
                                                {
                                                    PickUpPoint = p.Pickuppointname,
                                                    DropOffPoint = p.Dropoffpointname,
                                                    PickUpDateTime = p.PickUpDateTime,
                                                    BookedDate = p.BookedDate,
                                                    NoOfPassengers = p.NoOfPassengers,
                                                    DispatchStatus = p.DispatchStatus,
                                                    CustomerId = p.CustomerId,
                                                    Customername = p.Customername,
                                                    OrganisationId = p.OrganisationId,
                                                    DispatchId = p.DispatchId,
                                                    Status = p.DispatchStatusId,
                                                    Rating = p.Rating,
                                                    PickUpdate = p.PickUpDate,
                                                    DateModified = p.DateModified.Value,
                                                    RegCode = p.VehicleRegCode,
                                                    Latitude = p.Latitude,
                                                    Longitude = p.Longitude,
                                                    ImgUrl = p.ImageUrl
                                                }).ToList();
                    }

                }
                else if (Roles.IsUserInRole("OrganisationDepartment Admin"))
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        bookdata.OrgBookData = (from p in entities.getLatestDispactBookDataDeptAdmin(DepartmentId, OrganisationId, highest)
                                                select new BookData
                                                {
                                                    PickUpPoint = p.Pickuppointname,
                                                    DropOffPoint = p.Dropoffpointname,
                                                    PickUpDateTime = p.PickUpDateTime,
                                                    BookedDate = p.BookedDate,
                                                    NoOfPassengers = p.NoOfPassengers,
                                                    DispatchStatus = p.DispatchStatus,
                                                    CustomerId = p.CustomerId,
                                                    Customername = p.Customername,
                                                    OrganisationId = p.OrganisationId,
                                                    DispatchId = p.DispatchId,
                                                    Status = p.DispatchStatusId,
                                                    Rating = p.Rating,
                                                    DateModified = p.DateModified.Value,
                                                    RegCode = p.VehicleRegCode,
                                                    Latitude = p.Latitude,
                                                    Longitude = p.Longitude,
                                                    ImgUrl = p.ImageUrl
                                                }).ToList();
                    }
                }
                else
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        bookdata.OrgBookData = (from p in entities.getLatestDispactBookDataCustomer(CustomerId, OrganisationId, highest)
                                                select new BookData
                                                {
                                                    PickUpPoint = p.Pickuppointname,
                                                    DropOffPoint = p.Dropoffpointname,
                                                    PickUpDateTime = p.PickUpDateTime,
                                                    BookedDate = p.BookedDate,
                                                    NoOfPassengers = p.NoOfPassengers,
                                                    DispatchStatus = p.DispatchStatus,
                                                    CustomerId = p.CustomerId,
                                                    Customername = p.Customername,
                                                    OrganisationId = p.OrganisationId,
                                                    DispatchId = p.DispatchId,
                                                    Status = p.DispatchStatusId,
                                                    Rating = p.Rating,
                                                    DateModified = p.DateModified.Value,
                                                    RegCode = p.VehicleRegCode,
                                                    Latitude = p.Latitude,
                                                    Longitude = p.Longitude,
                                                    ImgUrl = p.ImageUrl,
                                                    Drivernumber = p.Drivernumber
                                                }).ToList();
                    }
                }
                if (bookdata.OrgBookData == null)
                {
                    bookdata.OrgBookData = new List<BookData>();
                }
                return bookdata;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }
        public async static Task<List<BookData>> ProcessDispatchDataAsync(int organizationId, int CustId, int ClientId, int? DispatchId, DateTime? highestdate, string DispatchIds)
        {
            List<BookData> returndata = new List<BookData>();
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    returndata.AddRange((from p in entities.getMobileTaksiLatestData(ClientId, CustId, organizationId, DispatchId, highestdate, DispatchIds)
                                         orderby p.DateModified descending
                                         select new BookData
                                         {
                                             PickUpPoint = p.Pickuppointname,
                                             DropOffPoint = p.Dropoffpointname,
                                             PickUpDateTime = p.PickUpDateTime,
                                             BookedDate = p.BookedDate,
                                             NoOfPassengers = p.NoOfPassengers,
                                             DispatchStatus = p.DispatchStatus,
                                             CustomerId = p.CustomerId,
                                             Customername = p.Customername,
                                             OrganisationId = p.OrganisationId,
                                             DispatchId = p.DispatchId,
                                             Status = p.DispatchStatusId,
                                             Rating = p.Rating,
                                             PickUpdate = p.PickUpDate,
                                             DateModified = p.DateModified.Value,
                                             RegCode = p.VehicleRegCode,
                                             Latitude = p.Latitude,
                                             Longitude = p.Longitude,
                                             ImgUrl = p.ImageUrl,
                                             Drivername = p.Drivername,
                                             Company = p.Organisationname,
                                             isVehicleVisible = p.IsVehicleVisible,
                                             BrandName = p.BrandName,
                                             Drivernumber=p.Drivernumber
                                         }).ToList());
                    //return returndata;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                //return null;
            }
            return returndata;
        }
        public static DashBoardModel GetLatestBookData(DateTime date, int ClientID, string username, int organizationId, int CustId)
        {
            try
            {
                //string picktoday = DateTime.Now.ToString("yyyy.MM.dd");
                DashBoardModel bookdata = new DashBoardModel(organizationId);
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    var data = (from p in entities.getLatestDispactBookDataCustomer(CustId, organizationId, date)
                                where p.ClientId == ClientID && p.IsVehicleVisible == true
                                orderby p.DateModified descending
                                select new BookData
                                {
                                    PickUpPoint = p.Pickuppointname,
                                    DropOffPoint = p.Dropoffpointname,
                                    PickUpDateTime = p.PickUpDateTime,
                                    BookedDate = p.BookedDate,
                                    NoOfPassengers = p.NoOfPassengers,
                                    DispatchStatus = p.DispatchStatus,
                                    CustomerId = p.CustomerId,
                                    Customername = p.Customername,
                                    OrganisationId = p.OrganisationId,
                                    DispatchId = p.DispatchId,
                                    Status = p.DispatchStatusId,
                                    Rating = p.Rating,
                                    PickUpdate = p.PickUpDate,
                                    DateModified = p.DateModified.Value,
                                    RegCode = p.VehicleRegCode,
                                    Latitude = p.Latitude,
                                    Longitude = p.Longitude,
                                    ImgUrl = p.ImageUrl,
                                    Drivername = p.Drivername,
                                    Company = p.Organisationname,
                                    isVehicleVisible = p.IsVehicleVisible,
                                    BrandName = p.BrandName
                                }).Take(5).ToList();

                    bookdata.OrgBookData = data;
                    return bookdata;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }
        public async static Task<DashBoardModel> GetDispatchLatestBookDataAsync(int OrganizationId, int CustId, int ClientId, string username, List<DispatchLatestDataParams> latestDataParams)
        {
            try
            {
                DashBoardModel bookdata = new DashBoardModel(OrganizationId);
                List<BookData> returndata = new List<BookData>();

                if (latestDataParams == null || latestDataParams.Count < 1 || latestDataParams[0] == null)
                {
                    var data = ProcessDispatchDataAsync(OrganizationId, CustId, ClientId, null, null, null);

                    await data;

                    bookdata.OrgBookData = data.Result;
                }
                else //if (latestDataParams.Value.Count > 0)
                {
                    var xyz = new List<Task<List<BookData>>>();
                    foreach (DispatchLatestDataParams param in latestDataParams)
                    {
                        xyz.Add(ProcessDispatchDataAsync(OrganizationId, CustId, ClientId, param.DispatchId, param.DispatchDateTime, null));
                    }
                    //Task xyz = latestDataParams.Select(c => ProcessDispatchDataAsync(OrganizationId, CustId, ClientId, c.DispatchId, c.DispatchDateTime, null));
                    xyz.Add(ProcessDispatchDataAsync(OrganizationId, CustId, ClientId, null, latestDataParams.OrderBy(c => c.DispatchDateTime).FirstOrDefault().DispatchDateTime, String.Join(",", latestDataParams.Select(v => v.DispatchId))));

                    //await Task.WhenAll(xyz);
                    foreach (var xyztask in await Task.WhenAll(xyz))
                    {
                        returndata.AddRange(xyztask);
                    }
                    bookdata.OrgBookData = returndata;
                }


                //string picktoday = DateTime.Now.ToString("yyyy.MM.dd");

                return bookdata;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }
        public DashBoardModel(int OrganisationId)
        {
            OrgBookData = new List<BookData>();
            this.OrganisationId = OrganisationId;
        }

        public DashBoardModel()
        {
            OrgBookData = new List<BookData>();
            this.OrganisationId = Service.Utility.getOrganisationId().Value;
        }

        public List<BookData> OrgBookData { get; set; }

        public List<OrganisationAdmin> OrganisationAdminData { get; set; }

        public int UserRole
        {
            get
            {
                if (Roles.IsUserInRole("Organisation Admin"))
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        }

        public static DashBoardModel getAdminData()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                MembershipProvider pmp = null;
                DashBoardModel admindata = new DashBoardModel();
                admindata.OrganisationAdminData = GetOrgAdminData();
                pmp = Membership.Provider;
                return admindata;
            }

        }

        public static DashBoardModel getBookData()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                DashBoardModel bookdata = new DashBoardModel();
                bookdata.OrgBookData = GetBookData();
                //bookdata.UserRole = getRoleId();
                if (bookdata.OrgBookData == null)
                {
                    bookdata.OrgBookData = new List<BookData>();
                }
                return bookdata;
            }

        }

        public static DashBoardModel getBookData(int ClientId, string username, int orgId, int custId)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                DashBoardModel bookdata = new DashBoardModel(orgId);
                bookdata.OrgBookData = GetBookData(ClientId, username, orgId, custId);
                if (bookdata.OrgBookData == null)
                {
                    bookdata.OrgBookData = new List<BookData>();
                }
                return bookdata;
            }
        }

        public ResponseModel saveBooking(string pickup, string pickupxy, string dropoff, string dropoffxy, int passengers,
           string Date, Guid bookedBy, DateTime DateModified, int customerId, int ClientId, int paymentModeId, string Instruction,
            string BookingRequestedBy, bool? BookingImmediate, string DeviceType, string OSVersion, string AppVersion, string BuildVersion)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    bool IsOrganisationAdmin = false;

                    if (Roles.IsUserInRole("Organisation Admin"))
                    {
                        IsOrganisationAdmin = true;
                    }

                    List<Booking_InsertV1_Result> dataBook = entities.Booking_InsertV1(pickup, pickupxy, dropoff, dropoffxy,
                        passengers, DateTime.Parse(Date), bookedBy, DateModified, customerId, ClientId, Instruction,
                        paymentModeId, BookingRequestedBy, BookingImmediate, IsOrganisationAdmin, DeviceType, OSVersion,
                        AppVersion, BuildVersion).ToList<Booking_InsertV1_Result>();
                    if (dataBook[0].ApproveEmail == true)
                    {
                        OrganisationId = Service.Utility.getMobileOrganisationId(bookedBy.ToString()).Value;
                        string BrandName = Service.Utility.getClientBrandName(ClientId);
                        string To = dropoff != null ? dropoff : " ";
                        string CustomerNames = Service.Utility.getCustomerNames(bookedBy.ToString()).Trim();
                        string emailBody = CustomerNames + "has made a Taksi booking from " + pickup + " to " +
                        To + ".Kindly login to www.taksi.co.ke to approve the booking. Regards Taksi Team";
                        string CustomerWaitEmailBody = CustomerNames + " your booking from" + pickup + " to " +
                       To + ",is awaiting approval from your Administrator.We will update you once its approved. Regards Taksi Team";
                        //string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyApprove"].ToString();
                        string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectApprove"].ToString();
                        string CustWaitingEmailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectCustWaitingApproal"].ToString();

                        string orgAdminEmails = getOrgAdminsEmails(OrganisationId);
                        string CustomerEmail = Service.Utility.getCustomerEmail(customerId);
                        string smsBody = CustomerNames + " has made a Taksi booking from " + pickup + " to " +
                        To + ".Kindly login to www.taksi.co.ke to approve the booking";
                        Organisation phone = entities.Organisations.Where(v => v.OrganisationId == OrganisationId).FirstOrDefault();
                        //string orgAdminPhones = phone.OrganisationTelephone;
                        string orgAdminPhones = getOrgAdminsPhones(OrganisationId);
                        EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("ApproveBooking"));
                        EmailTemplate ECustWait = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("CustomerWaitingApproval"));
                        if (ET != null)
                        {
                            emailBody = String.Format(ET.Body, CustomerNames, pickup, To);
                            emailBody = ET.Header + emailBody + ET.Footer;
                        }
                        if (ECustWait != null)
                        {
                            CustomerWaitEmailBody = String.Format(ECustWait.Body, CustomerNames, pickup, To);
                            CustomerWaitEmailBody = ECustWait.Header + CustomerWaitEmailBody + ECustWait.Footer;
                        }
                        if (orgAdminEmails == null)
                        {
                            string OrgName = Service.Utility.getOrganisationName(OrganisationId);
                            Exception kex = new Exception(String.Format("Booking succefully made for {0} of organisation {1} but no email for approval", BookingRequestedBy, OrgName));
                            //kex.Message = String.Format("Booking succefully made for {0} of organisation {1} but no email for approval", BookingRequestedBy, OrgName);
                            ExceptionHandler.dbErrorLogging.LogError(kex, HttpContext.Current);
                        }
                        else
                        {

                            new Thread(() =>
                            {
                                try
                                {
                                    Service.SendEmail.SendUserMngtEmail(orgAdminEmails, emailSubject, emailBody, ClientId);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    //throw new Exception(ex.Message.ToString());
                                }
                            }).Start();
                            if (CustomerEmail == null)
                            {
                                string OrgName = Service.Utility.getOrganisationName(OrganisationId);
                                Exception kex = new Exception(String.Format("Booking succefully made for {0} of organisation {1} but no Customer Email", BookingRequestedBy, OrgName));
                                //kex.Message = String.Format("Booking succefully made for {0} of organisation {1} but no email for approval", BookingRequestedBy, OrgName);
                                ExceptionHandler.dbErrorLogging.LogError(kex, HttpContext.Current);
                            }
                            else
                            {

                                new Thread(() =>
                                {
                                    try
                                    {
                                        Service.SendEmail.SendUserMngtEmail(CustomerEmail, CustWaitingEmailSubject, CustomerWaitEmailBody, ClientId);
                                    }
                                    catch (Exception mex)
                                    {
                                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                        //throw new Exception(ex.Message.ToString());
                                    }
                                }).Start();
                            }
                            if (orgAdminPhones != null)
                            {
                                new Thread(() =>
                                {
                                    try
                                    {
                                        //GFleetV3SMSProcessor.SendSMS.SendMessage(orgAdminPhones, smsBody, BrandName);
                                    }
                                    catch (Exception mex)
                                    {
                                        ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                        //throw new Exception(ex.Message.ToString());
                                    }
                                }).Start();
                            }
                        }
                    }
                    _rm.Status = true;
                    _rm.Message = "Booking successfully made!";
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateDispatchStatus(int DispatchId, int DispatchStatusId)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            string emailSubject;
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == DispatchId).FirstOrDefault();
                    Person custdata = entities.People.OfType<Customer>().Where(v => v.PersonId == data.CustomerId).FirstOrDefault();
                    int ClientId = custdata.ClientId.Value;
                    if (data != null)
                    {
                        string emailBody;
                        string CustomerEmail = custdata.Email1;
                        string CustomerNames = data.PassengerNames;
                        string pickup = data.PickUpPointName;
                        string To = data.DropOffPointName;
                        data.DispatchStatusId = DispatchStatusId;
                        data = (Dispatch)entities.Dispatches.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        _rm.Status = true;
                        _rm.Message = "Dispatch status successfully updated!";
                        if (DispatchStatusId == 2)
                        {
                            emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectCustApproved"].ToString();
                            emailBody = CustomerNames + " your booking from " + pickup + "to" +
                            To + ",has been approved. Regards Taksi Team";
                            EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("CustomerApproved"));
                            if (ET != null)
                            {
                                emailBody = String.Format(ET.Body, CustomerNames, pickup, To);
                                emailBody = ET.Header + emailBody + ET.Footer;
                            }
                        }
                        else
                        {
                            emailBody = CustomerNames + " your booking from " + pickup + "to" +
                            To + ",has been rejected. Regards Taksi Team";
                            emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectCustRejected"].ToString();
                            EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("CustomerRejected"));
                            if (ET != null)
                            {
                                emailBody = String.Format(ET.Body, CustomerNames, pickup, To);
                                emailBody = ET.Header + emailBody + ET.Footer;
                            }
                        }

                        if (CustomerEmail == null)
                        {
                            string OrgName = Service.Utility.getOrganisationName(OrganisationId);
                            Exception kex = new Exception(String.Format("Booking succefully updated for {0} of organisation {1} but no Customer Email", OrgName));
                            //kex.Message = String.Format("Booking succefully made for {0} of organisation {1} but no email for approval", BookingRequestedBy, OrgName);
                            ExceptionHandler.dbErrorLogging.LogError(kex, HttpContext.Current);
                        }

                        else
                        {

                            new Thread(() =>
                            {
                                try
                                {
                                    Service.SendEmail.SendUserMngtEmail(CustomerEmail, emailSubject, emailBody, ClientId);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    //throw new Exception(ex.Message.ToString());
                                }
                            }).Start();
                        }
                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Dispatch status not updated!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateRating(int DispatchId, int Rating)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Dispatch data = entities.Dispatches.Where(m => m.DispatchId == DispatchId).FirstOrDefault();

                    if (data != null)
                    {
                        data.Rating = Rating;
                        data = (Dispatch)entities.Dispatches.ApplyCurrentValues(data);
                        entities.SaveChanges();
                        _rm.Status = true;
                        _rm.Message = "Rating successfully submitted!";

                    }
                    else
                    {
                        _rm.Status = false; _rm.Message = "Rating not submitted!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }


    }
}