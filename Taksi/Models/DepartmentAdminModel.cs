﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Configuration;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Threading;

namespace Taksi.Models
{
    public class DepartmentAdminModel
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();
        private readonly int ClientId = Service.Utility.getClientId().Value;

        public ResponseModel CreateDepartmentAdmin(OrganisationRegisterModel mOrganisationReg)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            SystemUser mSystemUser = mOrganisationReg.DepartmentAdminPersonModel;
            RegisterDeparmentAdminModel mRegister = mOrganisationReg.RegisterDeparmentAdminModel;
            try
            {

                pmp = Membership.Provider;
                if (pmp.GetUser(mRegister.Email, false) == null)
                {
                    MembershipCreateStatus createStatus = new MembershipCreateStatus();
                    MembershipUser pms = pmp.CreateUser(mRegister.Email, mRegister.Password, mRegister.Email, "Question",
                                        "Answer", true, null, out createStatus);
                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        Roles.AddUserToRole(mRegister.Email, "OrganisationDepartment Admin");
                        if (mSystemUser == null)
                        {
                            mSystemUser = new SystemUser()
                            {
                                ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                DateModified = DateTime.Now,
                                Organisation = null,
                                UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                            };
                        }
                        else
                        {
                            mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                            mSystemUser.DateModified = DateTime.Now;
                            mSystemUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                        }
                        string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

                        string BrandName = Service.Utility.getClientBrandName();

                        emailBody = emailBody.Replace("[newline]", "</br>");

                        string taksiaddress = BrandName + System.Configuration.ConfigurationManager.AppSettings["taksihostname"].ToString();


                        emailBody = String.Format(emailBody, mRegister.Email, mRegister.Password, taksiaddress);
                        string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();

                        //Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                        EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
                        if (ET != null)
                        {
                            emailBody = String.Format(ET.Body, mRegister.Email, mRegister.Password, taksiaddress);
                            emailBody = ET.Header + emailBody + ET.Footer;
                        }

                        new Thread(() =>
                        {
                            try
                            {
                                Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody, ClientId);
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();
                    }
                    else
                    {
                        _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                        return _rm;
                    }

                    mSystemUser.DateModified = DateTime.Now;
                    mSystemUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                    mSystemUser.ClientId = Service.Utility.getClientId().Value;

                    //mSystemUser.Organisation = mSystemUser;
                    entities.People.AddObject(mSystemUser);
                    entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    _rm.Status = true; _rm.Message = "Department Admin " + mSystemUser.FullNames + " successfully saved!";
                    return _rm;
                }
                else
                {
                    _rm.Message = "user already exists"; _rm.Status = false;
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    pmp.DeleteUser(mRegister.Email, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }
        public ResponseModel CreateExistingDepartmentAdmin(string Username)
        {
            ResponseModel _rm = new ResponseModel();
            //MembershipUser user = DepartmentAdminModel.getUser(Username);
            try
            {
                // var roles = new List<string> { "End User", "OrganisationDepartment Admin" };
                var userRoles = Roles.GetRolesForUser(Username);

                if (userRoles.Contains("End User"))
                {
                    Roles.RemoveUserFromRole(Username, "End User");
                    Roles.AddUserToRole(Username, "OrganisationDepartment Admin");
                }
                else
                {
                    _rm.Status = false;
                    _rm.Message = "Department Admin was not successfully created!";
                    return _rm;
                }
                //Roles.RemoveUserFromRole(Username, "End User");
                //Roles.AddUserToRole(Username, "OrganisationDepartment Admin");

                _rm.Status = true;
                _rm.Message = "Department Admin successfully created!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false;
                _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel ResetPassword(string Username)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                MembershipUser user = DepartmentAdminModel.getUser(Username);
                if (user != null)
                {
                    using (GFleetModelContainer db = new GFleetModelContainer())
                    {
                        user.UnlockUser();
                        vw_DepartmentAdmin contact = db.vw_DepartmentAdmin.Where(u => u.UserName == Username).FirstOrDefault();
                        string reset = user.ResetPassword();
                        string newpass = Service.PasswordGenerator.RandomNumber().ToString();
                        user.ChangePassword(reset, newpass);

                        string BrandName = Service.Utility.getClientBrandName();

                        string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailresetPassTaksi"].ToString();

                        emailBody = emailBody.Replace("[newline]", "</br>");

                        string solnaddress = BrandName + System.Configuration.ConfigurationManager.AppSettings["taksihostname"].ToString();

                        emailBody = String.Format(emailBody, Username, newpass, solnaddress);

                        string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectresetPassTaksi"].ToString();

                        emailBody = String.Format(emailBody, Username.TrimStart('+'), newpass);

                        string smsBody = "Taksi: Your Password has been successfully reset to {0}. Check your email for more details";
                        smsBody = String.Format(smsBody, newpass);

                        EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("PasswordReset"));
                        if (ET != null)
                        {
                            emailBody = String.Format(ET.Body, Username, newpass, solnaddress);
                            emailBody = ET.Header + emailBody + ET.Footer;
                        }
                        new Thread(() =>
                        {
                            try
                            {
                                Service.SendEmail.SendUserMngtEmail(contact.Email1, emailSubject, emailBody, ClientId);
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();

                        response.Status = true;
                        response.Message = "Password Reset Successful";
                    }
                }
                else
                {
                    response.Status = false;
                    response.Message = "Unknown User";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                throw new Exception(mex.InnerException == null ? mex.Message.ToString() : mex.InnerException.Message);
                //response.Status = false;
                //response.Message = "An Error Was Encountered";
            }
            return response;
        }

        private static MembershipUser getUser(string emailName)
        {
            MembershipUser currentUser = Membership.Providers["MembershipProviderOther"].GetUser(emailName, false);

            if (currentUser == null)
            {
                string userName = Membership.Providers["MembershipProviderOther"].GetUserNameByEmail(emailName);
                if (userName != null)
                {
                    currentUser = Membership.Providers["MembershipProviderOther"].GetUser(userName, false);
                }
            }

            return currentUser;
        }
    }

}