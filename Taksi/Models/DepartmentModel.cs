﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;


namespace Taksi.Models
{
    public class DepartmentModel : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();

        private readonly int ClientId = Service.Utility.getClientId().Value;
        private readonly Guid userId = Guid.Parse(Service.Utility.getUserId().ToString());

        public static List<OrganisationDepartment> GetOrganisationDepartment()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<OrganisationDepartment> dpl = new List<OrganisationDepartment>();

                dpl = db.OrganisationDepartments
                    .ToList();
                return dpl;
            }
        }
        public IEnumerable<object> GetOrganisationDepartmentDetails()
        {
            ResponseModel _rm = new ResponseModel();
            return entities.OrganisationDepartments.Select(b => new
            {
                OrganisationDeptId = b.OrganisationDeptId,
                DepartmentName = b.DepartmentName
            }).ToList();
        }

        //For Custom Paging

        public IEnumerable<OrganisationDepartment> GetOrganisationDepartmentPage(int pageNumber, int pageSize, string orderCriteria)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            return entities.OrganisationDepartments
              .OrderBy(orderCriteria)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
        }
        public int CountOrganisationDepartment()
        {
            return entities.OrganisationDepartments.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

        //For Edit 
        public OrganisationDepartment GetOrganisationDepartment(int mOrganisationDepartmentId)
        {
            return entities.OrganisationDepartments.Where(m => m.OrganisationDeptId == mOrganisationDepartmentId).FirstOrDefault();
        }

        public ResponseModel CreateOrganisationDepartment(OrganisationDepartment mOrganisationDepartment)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {

                entities.OrganisationDepartments.AddObject(mOrganisationDepartment);
                entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                _rm.Status = true; _rm.Message = "Department " + mOrganisationDepartment.DepartmentName + " successfully saved!";
                return _rm;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel UpdateOrganisationDepartment(OrganisationDepartment mOrganisationDepartment)
        {
            ResponseModel _rm = new ResponseModel() { IsUpdate = true };
            try
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    OrganisationDepartment data = entities.OrganisationDepartments.Where(m => m.OrganisationDeptId == mOrganisationDepartment.OrganisationDeptId).FirstOrDefault();

                    if (data != null)
                    {
                        data = mOrganisationDepartment;

                        mOrganisationDepartment = (OrganisationDepartment)entities.OrganisationDepartments.ApplyCurrentValues(mOrganisationDepartment);
                        entities.SaveChanges();
                        _rm.Status = true; _rm.Message = "Department " + mOrganisationDepartment.DepartmentName + " successfully updated!";
                    }
                    else
                    {
                        _rm.Status = true; _rm.Message = "Department " + mOrganisationDepartment.DepartmentName + " not in your company!";
                    }
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public bool DeleteOrganisationDepartment(int mOrganisationDepartmentID)
        {
            try
            {
                OrganisationDepartment data = entities.OrganisationDepartments.Where(m => m.OrganisationDeptId == mOrganisationDepartmentID).FirstOrDefault();
                entities.OrganisationDepartments.DeleteObject(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return false;
            }
        }
    }
}