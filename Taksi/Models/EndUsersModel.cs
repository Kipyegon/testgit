﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Configuration;
using System.ComponentModel.DataAnnotations;
using DataAccess.SQL;
using System.Threading;

namespace Taksi.Models
{

    public class ModelOrganisation : IDisposable
    {
        private readonly GFleetModelContainer entities = new GFleetModelContainer();
        private readonly int OrganisationId = Service.Utility.getOrganisationId().Value;
        private readonly int ClientId = Service.Utility.getClientId().Value;
        public static List<vw_PersonEndUser> getContacts()
        {
            int OrganisationId = Service.Utility.getOrganisationId().Value;
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<vw_PersonEndUser> pe = new List<vw_PersonEndUser>();
                if (Roles.IsUserInRole("Organisation Admin"))
                {
                    pe = db.vw_PersonEndUser
                        .Where(u => u.OrganisationId == OrganisationId)
                        .ToList();
                    return pe;
                }
                else
                {
                    int DepartmentId = Service.Utility.getDepartmentId().Value;
                    pe = db.vw_PersonEndUser
                        .Where(u => u.OrganisationDeptId == DepartmentId)
                        .ToList();
                    return pe;
                }
            }
        }

        public static List<vw_DepartmentAdmin> getDepartmentAdmins()
        {
            int OrganisationId = Service.Utility.getOrganisationId().Value;
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<vw_DepartmentAdmin> dpa = new List<vw_DepartmentAdmin>();

                dpa = db.vw_DepartmentAdmin
                    .Where(u => u.OrganisationId == OrganisationId)
                    .ToList();
                return dpa;
            }
        }

        public static List<OrganisationDepartment> GetOrganisationDepartment()
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                List<OrganisationDepartment> dpl = new List<OrganisationDepartment>();

                dpl = db.OrganisationDepartments
                    .ToList();
                return dpl;
            }
        }

        public void Dispose()
        {
            entities.Dispose();
        }
        public Organisation GetOrganisation(int mOrganisationId)
        {
            return entities.Organisations.Where(m => m.OrganisationId == mOrganisationId).FirstOrDefault();
        }
        public ResponseModel CreateOrganisationUser(OrganisationRegisterModel mOrganisationReg, HttpPostedFileBase File)
        {
            ResponseModel _rm = new ResponseModel();
            MembershipProvider pmp = null;
            SystemUser mSystemUser = mOrganisationReg.DepartmentAdminPersonModel;
            Customer mCustEndUser = mOrganisationReg.OrganisationEndUserModel;
            RegisterEndUserModel mRegister = mOrganisationReg.RegisterEndUserModel;
            try
            {

                if (!entities.People.OfType<Customer>().Any(v => v.Telephone1.Equals(mCustEndUser.Telephone1) && v.Email1.Equals(mCustEndUser.Email1) && v.OrganisationId == OrganisationId))
                {

                    //if (!string.IsNullOrEmpty(mRegister.UserRole.RoleName))
                    //{

                    //pmp = Membership.Provider;
                    pmp = Membership.Providers["MembershipProviderOther"];
                    if (pmp.GetUser(mRegister.Username, false) == null)
                    {
                        string username = mRegister.Username.TrimStart('+');
                        string password = Service.Utility.GeneratePass().ToString();

                        MembershipCreateStatus createStatus = new MembershipCreateStatus();
                        MembershipUser pms = pmp.CreateUser(username, password, mCustEndUser.Email1, "Question",
                                            "Answer", true, null, out createStatus);
                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            Roles.AddUserToRole(username, "End User");

                            if (mCustEndUser == null)
                            {
                                mCustEndUser = new Customer()
                                {
                                    ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString()),
                                    DateModified = DateTime.Now,
                                    Organisation = null,
                                    UserId = Guid.Parse(pms.ProviderUserKey.ToString())
                                };
                            }
                            else
                            {
                                mCustEndUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                                mCustEndUser.DateModified = DateTime.Now;
                                mCustEndUser.UserId = Guid.Parse(pms.ProviderUserKey.ToString());
                                mCustEndUser.OrganisationId = Service.Utility.getOrganisationId().Value;
                            }

                            string BrandName = Service.Utility.getClientBrandName();

                            string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailBodyTaksi"].ToString();

                            emailBody = emailBody.Replace("[newline]", "</br>");

                            string solnaddress = BrandName + System.Configuration.ConfigurationManager.AppSettings["taksihostname"].ToString();

                            emailBody = String.Format(emailBody, mRegister.Username.TrimStart('+'), password, solnaddress);

                            string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectTaksi"].ToString();

                            string smsBody = "Welcome to Taksi, Your Username is {0} and Password is {1}. Kindly Check your email for further Instructions";

                            smsBody = String.Format(smsBody, mRegister.Username.TrimStart('+'), password);

                            EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("EndUserRegistration"));
                            if (ET != null)
                            {
                                emailBody = String.Format(ET.Body, mRegister.Username.TrimStart('+'), password, solnaddress);
                                emailBody = ET.Header + emailBody + ET.Footer;
                            }

                            new Thread(() =>
                            {
                                try
                                {
                                    Service.SendEmail.SendUserMngtEmail(mCustEndUser.Email1, emailSubject, emailBody,ClientId);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    //throw new Exception(ex.Message.ToString());
                                }
                            }).Start();

                            //Service.SendEmail.SendUserMngtEmail(mRegister.Email, emailSubject, emailBody);
                            new Thread(() =>
                            {
                                try
                                {
                                    GFleetV3SMSProcessor.SendSMS.SendMessage(mRegister.Username, smsBody, BrandName);
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    //throw new Exception(ex.Message.ToString());
                                }
                            }).Start();

                        }
                        else
                        {
                            _rm.Message = AccountValidation.ErrorCodeToString(createStatus); _rm.Status = false;
                            return _rm;
                        }
                        string Filename = ""; string filepath = "";
                        try
                        {
                            if (File != null)
                            {
                                try
                                {
                                    string CustomerDir = WebConfigurationManager.AppSettings["CustomerDir"];
                                    filepath = System.Web.HttpContext.Current.Server.MapPath(CustomerDir);
                                    Filename = "Client_" + mCustEndUser.Telephone1 + "_map" + Path.GetExtension(File.FileName);

                                    mCustEndUser.CustomerDirUrl = Filename;

                                    File.SaveAs(Path.Combine(filepath, Filename));
                                }
                                catch (Exception mex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                    mCustEndUser.CustomerDirUrl = null;
                                }
                            }
                            mCustEndUser.DateModified = DateTime.Now;
                            mCustEndUser.ModifiedBy = Guid.Parse(Service.Utility.getUserId().ToString());
                            mCustEndUser.ClientId = Service.Utility.getClientId().Value;
                            //mCustEndUser. = mCustEndUser;
                            entities.People.AddObject(mCustEndUser);
                            entities.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                            _rm.Status = true; _rm.Message = "Customer " + mCustEndUser.FullNames + " successfully saved!";
                            return _rm;
                        }
                        catch (Exception mex)
                        {
                            if (File != null)
                            {
                                try
                                {
                                    System.IO.File.Delete(filepath + Filename);
                                }
                                catch (Exception ex)
                                {
                                    ExceptionHandler.dbErrorLogging.LogError(ex.InnerException != null ? ex.InnerException : ex, HttpContext.Current);

                                }
                            }
                            ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                            _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                            return _rm;
                        }
                    }

                    else
                    {
                        _rm.Message = "user already exists"; _rm.Status = false;
                        return _rm;
                    }
                    //}
                    //else
                    //{
                    //    _rm.Message = "No Role name specified"; _rm.Status = false;
                    //    return _rm;
                    //}
                }
                else
                {
                    _rm.Message = "The Organisation already exists"; _rm.Status = false;
                    return _rm;
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                if (pmp != null)
                {
                    pmp.DeleteUser(mRegister.Username, true);
                }

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
        }

        public ResponseModel ResetPassword(string Username)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                MembershipUser user = ModelOrganisation.getUser(Username.TrimStart('+'));
                if (user != null)
                {
                    using (GFleetModelContainer db = new GFleetModelContainer())
                    {
                        user.UnlockUser();
                        vw_PersonEndUser contact = db.vw_PersonEndUser.Where(u => u.UserName == Username).FirstOrDefault();
                        string reset = user.ResetPassword();
                        string newpass = Service.PasswordGenerator.RandomNumber().ToString();
                        user.ChangePassword(reset, newpass);

                        string BrandName = Service.Utility.getClientBrandName();

                        string emailBody = System.Configuration.ConfigurationManager.AppSettings["emailresetPassTaksi"].ToString();

                        emailBody = emailBody.Replace("[newline]", "</br>");

                        string solnaddress = BrandName + System.Configuration.ConfigurationManager.AppSettings["taksihostname"].ToString();

                        emailBody = String.Format(emailBody, Username, newpass, solnaddress);

                        string emailSubject = System.Configuration.ConfigurationManager.AppSettings["emailSubjectresetPassTaksi"].ToString();

                        emailBody = String.Format(emailBody, Username.TrimStart('+'), newpass);

                        string smsBody = "Taksi: Your Password has been successfully reset to {0}. Check your email for more details";
                        smsBody = String.Format(smsBody, newpass);

                        EmailTemplate ET = entities.EmailTemplates.SingleOrDefault(v => v.EmailTemplateName.Equals("PasswordReset"));
                        if (ET != null)
                        {
                            emailBody = String.Format(ET.Body, Username, newpass, solnaddress);
                            emailBody = ET.Header + emailBody + ET.Footer;
                        }

                        new Thread(() =>
                        {
                            try
                            {
                                GFleetV3SMSProcessor.SendSMS.SendMessage(contact.UserName, smsBody, BrandName);
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();

                        new Thread(() =>
                        {
                            try
                            {
                                Service.SendEmail.SendUserMngtEmail(contact.Email1, emailSubject, emailBody,ClientId);
                            }
                            catch (Exception mex)
                            {
                                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                                //throw new Exception(ex.Message.ToString());
                            }
                        }).Start();

                        response.Status = true;
                        response.Message = "Password Reset Successful";
                    }
                }
                else
                {
                    response.Status = false;
                    response.Message = "Unknown User";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                throw new Exception(mex.InnerException == null ? mex.Message.ToString() : mex.InnerException.Message);
                //response.Status = false;
                //response.Message = "An Error Was Encountered";
            }
            return response;
        }
        private static MembershipUser getUser(string emailName)
        {
            MembershipUser currentUser = Membership.Providers["MembershipProviderOther"].GetUser(emailName, false);

            if (currentUser == null)
            {
                string userName = Membership.Providers["MembershipProviderOther"].GetUserNameByEmail(emailName);
                if (userName != null)
                {
                    currentUser = Membership.Providers["MembershipProviderOther"].GetUser(userName, false);
                }
            }

            return currentUser;
        }

        public ResponseModel DeleteEndUser(int PersonId, string Username)
        {
            ResponseModel _rm = new ResponseModel();
            try
            {
                int OrganisationId = Service.Utility.getOrganisationId().Value;
                List<Delete_EndUser_Result> data = entities.Delete_EndUser(PersonId, OrganisationId).ToList<Delete_EndUser_Result>();
                if (data[0].Error == "User Deleted....")
                {
                    MembershipProvider pmp = null;
                    pmp = Membership.Provider;
                    pmp.DeleteUser(Username, true);
                    _rm.Message = "User Successfully deleted";
                }
                else
                {
                    _rm.Message = "Problem occured durig deletion.Kindly refresh browser and try again.If persists contact support@geeckoltd.com ";
                }
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);

                _rm.Status = false; _rm.Message = mex.InnerException != null ? mex.InnerException.Message : mex.Message;
                return _rm;
            }
            return null;
        }
    }
    public class OrganisationRegisterModel
    {
        public RegisterDeparmentAdminModel RegisterDeparmentAdminModel { get; set; }
        public RegisterEndUserModel RegisterEndUserModel { get; set; }
        public Organisation OrganisationModel { get; set; }
        public Customer OrganisationEndUserModel { get; set; }
        public SystemUser DepartmentAdminPersonModel { get; set; }

        public static OrganisationRegisterModel getOrganisationRegisterModel(int OrganisationId)
        {
            OrganisationRegisterModel crm = new OrganisationRegisterModel();
            crm.OrganisationModel = new ModelOrganisation().GetOrganisation(OrganisationId);

            return crm;
        }
    }
}