﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Taksi.Models
{
    public class ErrorModel
    {
        public Boolean IsErrors;
        public List<string> Errors;
        public ErrorModel()
        {
            Errors = new List<string>();
            IsErrors = true;
        }
    }
}