﻿using DataAccess.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Taksi.Models
{
    public class MServer_V1Model
    {
        public bool IsAuthenticated { get; set; }
        public string AuthMessage { get; set; }
        public string UserId { get; set; }
        public int ClientId { get; set; }
        public int OrganisationId { get; set; }
        public int CustomerId { get; set; }
        public string OrgName { get; set; }
        public string Username { get; set; }
        public string Clientname { get; set; }
        public string Customername { get; set; }
        public bool IsOrganisationAdmin { get; set; }
        public string MobileBuildVersion { get; set; }
        public bool UserStatus { get; set; }


        public static MServer_V1Model getValidatedUser(Guid UserId, string Username)
        {
            using (GFleetModelContainer db = new GFleetModelContainer())
            {
                MServer_V1Model loggedIn;
                int CustomerId = Service.Utility.getCustomerId(Username).Value;
                int? ClientId = Service.Utility.getClientId(Username);
                 MembershipProvider pmp = Membership.Provider;
                string Customername = "";
                var CustomerDetails = db.vw_DispatchBookData.Where(v => v.CustomerId == CustomerId).FirstOrDefault();
                if (CustomerDetails != null)
                {
                    Customername = CustomerDetails.CustomerName.Trim();
                }
                if (!ClientId.HasValue)
                {
                    loggedIn = new MServer_V1Model();
                    loggedIn.Username = Username;
                    loggedIn.UserId = UserId.ToString();
                }
                else
                {

                    loggedIn = new MServer_V1Model();

                    string[] UserRoles = Roles.GetRolesForUser(Username);
                    if (UserRoles.Contains("Organisation Admin"))
                    {
                        loggedIn.IsOrganisationAdmin = true;
                    }
                    loggedIn.Username = Username;
                    loggedIn.UserId = UserId.ToString();
                    loggedIn.ClientId = ClientId.Value;
                    loggedIn.OrganisationId = getOrgId(UserId.ToString()).Value;
                    loggedIn.OrgName = Service.Utility.getOrganisationName(loggedIn.OrganisationId);
                    loggedIn.CustomerId = CustomerId;
                    loggedIn.Clientname = Service.Utility.getClientName(ClientId);
                    loggedIn.Customername = Customername;
                    loggedIn.UserStatus = pmp.GetUser(UserId, true) != null;
                    loggedIn.MobileBuildVersion = "-1";

                }
                return loggedIn;
            }

        }
        public static string getMobileBuildVersion(string MobileType)
        {
            var MobileBuildVersion="";
            try
            {
                //MServer_V1Model loggedIn;
                //loggedIn = new MServer_V1Model();
                MobileBuildVersion = System.Configuration.ConfigurationManager.AppSettings[MobileType].ToString();
                return MobileBuildVersion;
            }
            catch (Exception mex)
            {
                MServer_V1Model loggedIn;
                loggedIn = new MServer_V1Model();
                //ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                MobileBuildVersion = "-1";
                return MobileBuildVersion;
            }

        }

        public static int? getOrgId(string UserId)
        {
            using (GFleetModelContainer entities = new GFleetModelContainer())
            {
                Guid userId = Guid.Parse(UserId.ToString());
                Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                if (per != null)
                {
                    int? OrganisationId = per.OrganisationId;
                    if (OrganisationId.HasValue)
                        HttpContext.Current.Session["OrganisationId"] = OrganisationId.Value;
                    return OrganisationId.Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public static DashBoardModel GetLatestBookData(DateTime highest, string Username, int CustomerId)
        {
            try
            {
                DashBoardModel bookdata = new DashBoardModel();

                int OrganisationId = Service.Utility.getOrganisationId().Value;
                string picktoday = DateTime.Now.ToString("yyyy-MM-dd");
                string[] UserRoles = Roles.GetRolesForUser(Username);
                if (UserRoles.Contains("Organisation Admin"))
                {

                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        bookdata.OrgBookData = (from p in entities.getLatestDispactBookDataOrg(OrganisationId, highest)
                                                select new Taksi.Models.DashBoardModel.BookData
                                                {
                                                    PickUpPoint = p.Pickuppointname,
                                                    DropOffPoint = p.Dropoffpointname,
                                                    PickUpDateTime = p.PickUpDateTime,
                                                    BookedDate = p.BookedDate,
                                                    NoOfPassengers = p.NoOfPassengers,
                                                    DispatchStatus = p.DispatchStatus,
                                                    CustomerId = p.CustomerId,
                                                    OrganisationId = p.OrganisationId,
                                                    DispatchId = p.DispatchId,
                                                    Status = p.DispatchStatusId,
                                                    Rating = p.Rating,
                                                    PickUpdate = p.PickUpDate,
                                                    DateModified = p.DateModified.Value,
                                                    RegCode = p.VehicleRegCode,
                                                    Latitude = p.Latitude,
                                                    Longitude = p.Longitude,
                                                    ImgUrl = p.ImageUrl
                                                }).ToList();
                    }

                }
                else
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        bookdata.OrgBookData = (from p in entities.getLatestDispactBookDataCustomer(CustomerId, OrganisationId, highest)
                                                select new Taksi.Models.DashBoardModel.BookData
                                                {
                                                    PickUpPoint = p.Pickuppointname,
                                                    DropOffPoint = p.Dropoffpointname,
                                                    PickUpDateTime = p.PickUpDateTime,
                                                    BookedDate = p.BookedDate,
                                                    NoOfPassengers = p.NoOfPassengers,
                                                    DispatchStatus = p.DispatchStatus,
                                                    CustomerId = p.CustomerId,
                                                    OrganisationId = p.OrganisationId,
                                                    DispatchId = p.DispatchId,
                                                    Status = p.DispatchStatusId,
                                                    Rating = p.Rating,
                                                    DateModified = p.DateModified.Value,
                                                    RegCode = p.VehicleRegCode,
                                                    Latitude = p.Latitude,
                                                    Longitude = p.Longitude,
                                                    ImgUrl = p.ImageUrl
                                                }).ToList();
                    }
                }
                if (bookdata.OrgBookData == null)
                {
                    bookdata.OrgBookData = new List<Taksi.Models.DashBoardModel.BookData>();
                }
                return bookdata;
            }
            catch (Exception mex)
            {
                ExceptionHandler.dbErrorLogging.LogError(mex.InnerException != null ? mex.InnerException : mex, HttpContext.Current);
                return null;
            }
        }

    }

    public class BookingData
    {
        public string pickup { get; set; }
        public string pickupxy { get; set; }
        public string dropoff { get; set; }
        public string dropoffxy { get; set; }
        public int passengers { get; set; }
        public string Date { get; set; }
        public int paymentModeId { get; set; }
        public string Instruction { get; set; }
        public bool BookingImmediate { get; set; }
        public string UserId { get; set; }
        public int ClientId { get; set; }
        public int OrganisationId { get; set; }
        public string Username { get; set; }
        public int CustomerId { get; set; }
        public string OrgName { get; set; }
        public string DeviceType { get; set; }
        public string OSVersion { get; set; }
        public string AppVersion { get; set; }
        public string BuildVersion { get; set; }
    }
}