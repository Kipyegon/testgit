﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Taksi.Models
{
    public class ResponseModel
    {
        public ResponseModel()
        {
            Status = true;
            IsUpdate = false;
        }
        public Boolean Status { set; get; }
        public String Message { set; get; }
        public Boolean IsUpdate { get; set; }
        public Object Data { get; set; }
    }
}