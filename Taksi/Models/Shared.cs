﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Web.Security;
using DataAccess.SQL;
using System.Diagnostics.CodeAnalysis;
using System.Security.Principal;
using System.Web.Mvc.Properties;

namespace Taksi.Models
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]

    public class AuthorizeEnumAttribute : AuthorizeAttribute
    {
        public AuthorizeEnumAttribute(string ControllerName, string ActionName)
        {
            //if (roles.Any(r => r.GetType().BaseType != typeof(Enum)))
            //    throw new ArgumentException("roles");

            //this.Roles = string.Join(",", roles.Select(r => Enum.GetName(r.GetType(), r)));
            this.Roles = getRoles(ControllerName, ActionName);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            IPrincipal user = httpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                return false;
            }
            string[] _rolesSplit = SplitString(Roles);
            if (!_rolesSplit.Any(user.IsInRole))
            {
                return false;
            }
            return base.AuthorizeCore(httpContext);
        }

        public static string getRoles(string ControllerName, string ActionName)
        {
            using (GFleetModelContainer gfleet = new GFleetModelContainer())
            {
                var query = from t in gfleet.vw_ControllerView
                            where t.ControllerName.Equals(ControllerName) && t.ActionName.Equals(ActionName) && t.IsEnabled.Equals(true)
                            group t by new
                            {
                                t.RoleName
                            } into g
                            orderby g.Key.RoleName
                            select new
                            {
                                RoleNames = (g.Key.RoleName)
                            };

                string result = string.Join(string.Empty, query.Select(r => r.RoleNames).ToArray());

                return string.IsNullOrEmpty(result) ? null : result;
            }
        }

        internal static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }
    }

    public class AuthorizationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var requestedController = filterContext.RouteData.GetRequiredString("controller");
            var requestedAction = filterContext.RouteData.GetRequiredString("action");

            string[] actionView = System.Configuration.ConfigurationManager.AppSettings["actionView"].ToString().Split(",".ToCharArray());

            if (actionView.Any(r => r.ToLower().Equals(requestedAction.ToString().ToLower())))
                requestedAction = "View";

            var operation = string.Format("/{0}/{1}", requestedController, requestedAction);

            string[] SystemRightsRoles = System.Configuration.ConfigurationManager.AppSettings["SystemRightsRoles"].ToString().Split(',');
            string SuperAdminPgs = System.Configuration.ConfigurationManager.AppSettings["SuperAdminPages"].ToString();

            string[] roles = Roles.GetRolesForUser();

            int? OrganisationId = Service.Utility.getOrganisationId();
            foreach (string systemRightsRole in SystemRightsRoles)
            {
                if (SuperAdminPgs.Contains(requestedController))
                {
                    //string redirectUrl = System.Configuration.ConfigurationManager.AppSettings["SuperAdminPages"].ToString();
                    //filterContext.HttpContext.Response.Redirect(redirectUrl);
                }
                else
                    if (roles.Contains(systemRightsRole) && !OrganisationId.HasValue)
                    {
                        string redirectUrl = System.Configuration.ConfigurationManager.AppSettings["unauthorisedsuperadmin"].ToString();

                        string Message = string.Format("You are not authorized to perform operation: {0} kindly select the client before proceeding", operation);
                        redirectUrl += "?Message=" + Message;
                        filterContext.HttpContext.Response.Redirect(redirectUrl);
                    }
            }
            string roleNames = getRoles(requestedController, requestedAction);

            IPrincipal user = filterContext.HttpContext.User;

            string[] _rolesSplit = SplitString(roleNames);

            if (!_rolesSplit.Any(user.IsInRole))
            {
                string redirectUrl = System.Configuration.ConfigurationManager.AppSettings["unauthorised"].ToString();

                string Message = string.Format("You are not authorized to perform operation: {0}", operation);
                redirectUrl += "?Message=" + Message;
                filterContext.HttpContext.Response.Redirect(redirectUrl);
            }

        }

        public static string getRoles(string ControllerName, string ActionName)
        {
            using (GFleetModelContainer gfleet = new GFleetModelContainer())
            {
                var query = from t in gfleet.vw_ControllerView
                            where t.ControllerName.Equals(ControllerName) && t.ActionName.Equals(ActionName) && t.IsEnabled.Equals(true)
                            group t by new
                            {
                                t.RoleName
                            } into g
                            orderby g.Key.RoleName
                            select new
                            {
                                RoleNames = (g.Key.RoleName)
                            };

                string result = string.Join(",", query.Select(r => r.RoleNames).ToArray());

                return string.IsNullOrEmpty(result) ? null : result;
            }
        }

        internal static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }

    }
}