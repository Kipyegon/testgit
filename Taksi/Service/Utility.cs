﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using DataAccess.SQL;

namespace Taksi.Service
{
    public class Utility
    {

        public static object getUserId(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {

                object UserId = Membership.GetUser(Username).ProviderUserKey;
                return UserId;
            }
            else
                return null;
        }
        public static int GeneratePass()
        {
            return PasswordGenerator.RandomNumber();
        }

        public static object getUserId()
        {
            if (Membership.GetUser() != null)
            {

                object UserId = Membership.GetUser().ProviderUserKey;
                return UserId;
            }
            else
                return null;
        }

        public static int? getOrganisationId()
        {
            if (HttpContext.Current.Session["OrganisationId"] == null)
            {
                if (Membership.GetUser() != null)
                {
                    //MembershipProvider pmp = Membership.Provider;
                    object UserId = getUserId();
                    if (UserId != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            Guid userId = Guid.Parse(UserId.ToString());
                            Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                            if (per != null)
                            {
                                int? OrganisationId = per.OrganisationId;
                                if (OrganisationId.HasValue)
                                    HttpContext.Current.Session["OrganisationId"] = OrganisationId.Value;
                                return OrganisationId;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
            else
                return int.Parse(HttpContext.Current.Session["OrganisationId"].ToString());
        }

        public static int? getOrganisationId(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {
                //MembershipProvider pmp = Membership.Provider;
                object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            int? OrganisationId = per.OrganisationId;
                            return OrganisationId;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public static int? getMobileOrganisationId(string UserId)
        {
            //if (Membership.GetUser() != null)
            //{
                //MembershipProvider pmp = Membership.Provider;
                //object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            int? OrganisationId = per.OrganisationId;
                            return OrganisationId;
                        }
                        else
                        {
                            return null;
                        }
                    }
                //}
                //else
                //{
                //    return null;
                //}
            }
            return null;
        }

        public static int? getDepartmentId()
        {
            if (HttpContext.Current.Session["DepartmentId"] == null)
            {
                if (Membership.GetUser() != null)
                {
                    //MembershipProvider pmp = Membership.Provider;
                    object UserId = getUserId();
                    if (UserId != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            Guid userId = Guid.Parse(UserId.ToString());
                            Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                            if (per != null)
                            {
                                int? DepartmentId = per.OrganisationDeptId;
                                if (DepartmentId.HasValue)
                                    HttpContext.Current.Session["DepartmentId"] = DepartmentId.Value;
                                return DepartmentId;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
            else
                return int.Parse(HttpContext.Current.Session["DepartmentId"].ToString());
        }

        public static int? getDepartmentId(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {
                //MembershipProvider pmp = Membership.Provider;
                object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            int? DepartmentId = per.OrganisationDeptId;
                            return DepartmentId;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public static string getClientBrandName()
        {
            int? clientId = getClientId();

            if (clientId.HasValue)
            {
                GFleetModelContainer entities = new GFleetModelContainer();

                Client _cl = entities.Clients.SingleOrDefault(r => r.ClientId == clientId.Value);

                return _cl != null ? _cl.BrandName : null;
            }
            return null;
        }

        public static string getClientBrandName(int? clientId)
        {

            if (clientId.HasValue)
            {
                GFleetModelContainer entities = new GFleetModelContainer();

                Client _cl = entities.Clients.SingleOrDefault(r => r.ClientId == clientId.Value);

                return _cl != null ? _cl.BrandName : null;
            }
            return null;
        }

        public static string getClientName(int? ClientId)
        {
            //int? clientId = getClientId();

            //if (clientId.HasValue)
            //{
            GFleetModelContainer entities = new GFleetModelContainer();

            Client _cl = entities.Clients.SingleOrDefault(r => r.ClientId == ClientId.Value);

            return _cl != null ? _cl.BrandName : null;
            //}
            //return null;
        }

        public static int? getClientId()
        {
            if (HttpContext.Current.Session["ClientId"] == null)
            {
                if (Membership.GetUser() != null)
                {
                    //MembershipProvider pmp = Membership.Provider;
                    object UserId = getUserId();
                    if (UserId != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            Guid userId = Guid.Parse(UserId.ToString());
                            Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                            if (per != null)
                            {
                                int? ClientId = per.ClientId;
                                if (ClientId.HasValue)
                                    HttpContext.Current.Session["ClientId"] = ClientId.Value;
                                return ClientId;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
            else
                return int.Parse(HttpContext.Current.Session["ClientId"].ToString());
        }

        public static int? getClientId(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {
                //MembershipProvider pmp = Membership.Provider;
                object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            int? ClientId = per.ClientId;
                            return ClientId;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public static int? getCustomerId()
        {
            if (HttpContext.Current.Session["CustomerId"] == null)
            {
                if (Membership.GetUser() != null)
                {
                    //MembershipProvider pmp = Membership.Provider;
                    object UserId = getUserId();
                    if (UserId != null)
                    {
                        using (GFleetModelContainer entities = new GFleetModelContainer())
                        {
                            Guid userId = Guid.Parse(UserId.ToString());
                            Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                            if (per != null)
                            {
                                int? CustomerId = per.PersonId;
                                if (CustomerId.HasValue)
                                    HttpContext.Current.Session["CustomerId"] = CustomerId.Value;
                                return CustomerId;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
            else
                return int.Parse(HttpContext.Current.Session["CustomerId"].ToString());
        }

        public static int? getCustomerId(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {
                //MembershipProvider pmp = Membership.Provider;
                object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            int? CustomerId = per.PersonId;
                            return CustomerId;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public static string getCustomerName(string Username)
        {
            if (Membership.GetUser(Username) != null)
            {
                //MembershipProvider pmp = Membership.Provider;
                object UserId = getUserId(Username);
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            string FullNames = (per.Title != null ? per.Title : " ") + " " + (per.FirstName != null ? per.FirstName : " ") + " " + (per.LastName != null ? per.LastName : " ");

                            return FullNames;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
        public static string getCustomerNames(string UserId)
        { //MembershipProvider pmp = Membership.Provider;
                if (UserId != null)
                {
                    using (GFleetModelContainer entities = new GFleetModelContainer())
                    {
                        Guid userId = Guid.Parse(UserId.ToString());
                        Person per = entities.People.SingleOrDefault(s => s.UserId == userId);
                        if (per != null)
                        {
                            string FullNames = (per.Title != null ? per.Title : " ") + " " + (per.FirstName != null ? per.FirstName : " ") + " " + (per.LastName != null ? per.LastName : " ");

                            return FullNames;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
        }
        public static string getOrganisationName(int OrganisationId)
        {
            if (OrganisationId != null)
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Organisation per = entities.Organisations.SingleOrDefault(s => s.OrganisationId == OrganisationId);
                    if (per != null)
                    {
                        string OrganisationName = (per.OrganisationName != null ? per.OrganisationName : " ");

                        return OrganisationName;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public static string getCustomerEmail(int CustomerId)
        {
            if (CustomerId != null)
            {
                using (GFleetModelContainer entities = new GFleetModelContainer())
                {
                    Person per = entities.People.OfType<Customer>().SingleOrDefault(s => s.PersonId == CustomerId);
                    if (per != null)
                    {
                        string CustomerEmail = (per.Email1 != null ? per.Email1 : " ");

                        return CustomerEmail;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public class Helper
        {
            public static byte[] GetBytesFromFile(string fullFilePath)
            {
                // this method is limited to 2^32 byte files (4.2 GB)

                FileStream fs = null;
                try
                {
                    fs = File.OpenRead(fullFilePath);
                    byte[] bytes = new byte[fs.Length];
                    fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                    return bytes;
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                        fs.Dispose();
                    }
                }

            }
        }
    }
}