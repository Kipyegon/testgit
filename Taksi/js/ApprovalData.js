﻿var update_interval = 1000;
var update_pending;
var the_dataTable;
var vehicles = [];
$(document).ready(function () {

    loadDataTable();
    first_load();

    setInterval('update_vehicles()', update_interval);
});
function loadDataTable() {
    if (the_dataTable == null) {
        the_dataTable = $('#vehicles').DataTable({
            "sScrollY": "590px",
            "sScrollX": "100%",
            "sScrollXInner": "100%",
            "bScrollCollapse": true,
            "bPaginate": false,
            "sDom": 'tS',
            "bSort": false,
            "bSortClasses": false
        });

    }
    the_dataTable.on("draw", function () {
        $(".dataTables_scrollBody").niceScroll();
    });
}
function update_vehicles() {
    if (update_pending) {
        return;
    }
    if (vehicles.length < 1) {
        first_load();
        return;
    }

    update_pending = false;

    var highest = vehicles[0].strDModified;
    $.each(vehicles, function () {
        highest = highest == null ? this.strDModified : this.strDModified > highest && this.strDModified != null ? this.strDModified : highest;
    });

    var params = $.param({
        highest: highest
    });

    $.ajax({
        url: '/Dashboard/getLatestBookData',
        type: 'POST',
        dataType: 'json',
        data: params,
        success: function (data) {
            update_pending = false;
            var result_new = [];
            $.each(data.OrgBookData, function () {
                var new_data = this;

                $.each(vehicles, function () {
                    if (this.DispatchId == new_data.DispatchId) {
                        this.update(new_data);
                    }
                });

                var exists = $.grep(vehicles, function (e) { return e.DispatchId == new_data.DispatchId; });
                if (exists.length == 0)
                    result_new.push(new_data);

            });
            $.each(result_new, function (vs) {
                var role = vs.UserRole;
                vehicles.push(new vehicle(this, role));
            });

            update_pending = false;
            the_dataTable
              .order([[3, 'desc']])
              .draw();
        },
        error: ajax_error
    });
}
function vehicle(values, role) {

    this.update = function (new_values) {


        for (var v in new_values) {
            this[v] = new_values[v];
        }

        this.update_row();
        this.update_icon();
    };

    this.update_row = function () {
        var self = this;

        var row = {
            RefNo: this.DispatchId,
            Customername: this.Customername,
            PickUpPoint: this.PickUpPoint,
            DropOffPoint: this.DropOffPoint,
            PickUpDateTime: this.PickUpDateTime,
            BookedDate: this.BookedDate,
            NoOfPassengers: this.NoOfPassengers,
            Status: this.Status,
            DispatchStatus: this.DispatchStatus,
            Rating: this.Rating
        };

        this.visible = true;

        this.row.html(make_tds(row))
                .unbind('click')
                .unbind('dblclick');

        this.row.find('.btn_approve').click(function () {
            self.aproove_booking(true);
        });

        this.row.find('.btn_reject').click(function () {
            self.aproove_booking(false);
        });
        if (row.Rating == null) {
            this.row.find('.rating').rating({
                static: false,
                score: row.Rating == null ? 3 : row.Rating,
                stars: 5,
                showHint: true,
                hints: ['bad', 'poor', 'average', 'good', 'excellent'],
                showScore: false,
                scoreHint: "Current score: ",
                click: function (value, rating) {
                    rating.rate(value);
                    self.submit_rating(value);
                }
            });
        }
        else {
            this.row.find('.rating').rating({
                static: true,
                score: row.Rating,
                stars: 5,
                showHint: true,
                hints: ['bad', 'poor', 'average', 'good', 'excellent'],
                showScore: false,
                scoreHint: "Current score: ",
            });
        }
        if (the_dataTable.row(this.row).length > 0) {
            var row_data = [];
            this.row.find("td").each(function () {
                row_data.push($(this).text());
            });
            the_dataTable.row(this.row)
            .data(row_data)
            .draw();
        }
    };

    this.submit_rating = function (value, comment) {
        var dID = this.DispatchId;
        var params = $.param({
            DispatchId: dID,
            Rating: value,
            Comments: comment
        });
        $.ajax({
            url: "/Dashboard/UpdateRating",
            type: 'POST',
            data: params,
            success: function (data) {
                if (data.Status) {
                    show_notification(data.Message, "success");
                }
                else {
                    show_notification(data.message, "error");
                }
                console.log(data);
            }
        });
    }
    this.aproove_booking = function (status) {
        var dID = this.DispatchId;
        var params = $.param({
            DispatchId: dID,
            DispatchStatusId: status ? 2 : 4
        });
        $.ajax({
            url: "/Dashboard/UpdateDispatchStatus",
            type: 'POST',
            data: params,
            success: function (data) {
                if (data.Status) {
                    show_notification(data.Message, "success");
                }
                else {
                    show_notification(data.message, "error");
                }
                console.log(data);
            }
        });
    }

    this.update_icon = function () {

        var self = this;

        if (this.marker != null) {
            this.marker.setMap(null);
        }

        this.marker = getMarkerIcon(this);

        var dispatchurl = 'http://gfleet.geeckoltd.com/VehicleDispatch/BookDispatch';

        var openDataParam = '{"Vid":"' + this.VehicleId + '"}';


        this.marker = this.marker;
    };


    function getMarkerIcon(sVehicle) {
        if (sVehicle.Latitude != null && sVehicle.Longitude != null) {
            var icon_name = imagePath + sVehicle.ImgUrl;
            return createMarker(icon_name, sVehicle.Latitude, sVehicle.Longitude, sVehicle.RegCode, 32);
        } else {
            return null;
        }
    };

    this.setVisible = function (x) {
        this.visible = x;

        if (x) {
            this.row.show();
        } else {
            this.row.hide();
        }
    };

    function make_tds(vals) {
        var result = '';
        for (var v in vals) {
            if (v == "Status") {
                var dat = (vals[v] == '' || vals[v] == null ? 1 : vals[v]);
                if (role == 0) {
                    result += "";
                    if (dat == 1) {
                        result += '<td alt="Action" title="' + vals['DispatchStatus'] + '" class="' + v + " absorbing-column" + '">';
                        result += '<button class="button small success btn_approve">Approve</button><button class="button small inverse btn_reject">Reject</button>';
                        result += '</td>';
                    }
                    else if (dat == 6) {
                        result += '<td alt="Action" title="' + vals['DispatchStatus'] + '" class="' + v + " absorbing-column" + '">';
                        result += '<div class="rating small"></div>';
                        result += '</td>';
                    }
                    else {
                        result += '<td alt="' + dat + '" class="' + v + " absorbing-column" + '">' + vals['DispatchStatus'] + '</td>';
                    }
                }
                else {
                    if (dat == 6) {
                        result += '<td alt="Action" title="click on star to rate" class="' + v + '">';
                        result += '<div class="rating small"></div>';
                        result += '</td>';
                    }
                    else {
                        result += '<td alt="' + dat + '" class="' + v + " absorbing-column" + '">' + vals['DispatchStatus'] + '</td>';
                    }
                }
            }
            else if (v == "DispatchStatus" || v == "Rating") {
                //do nothing
            }
            else {
                if (vals[v] == null) {
                    result += '<td class="' + v + '">&nbsp;</td>';
                } else {
                    var dat = (vals[v].toString() == '' ? '&nbsp;' : vals[v].toString());
                    result += '<td alt="' + dat + '" title="' + dat + '" class="' + v + '">' + dat + '</td>';
                }
            }

        }
        return result;
    }


    this.row = $('<tr />');
    this.marker = null;
    this.update(values);

    the_dataTable.row.add(this.row).draw();

}
function ajax_error(request, text, error) {
    update_pending = false;
    console.log('request: ' + request.responseText);
    console.log('text: ' + text);
    console.log('error: ' + error);
}
function initial_load(data) {
    //update_pending = false;

    create_vehicles(data);

    update_pending = true;
}
function create_vehicles(vs) {

    $.each(vs.OrgBookData, function () {
        var role = vs.UserRole;
        vehicles.push(new vehicle(this, role));
    });

}
