﻿var sidebarstate = false;
var bookingstate = false;
var notificationstate = false;
var passengers = 0;
var mMap = null;
var pinned = false;
function check_empty() {
    if (document.getElementById('uname').value == "" || document.getElementById('pass').value == "") {
        alert("Fill All Fields !");
        return false;
    } else {
        var form = $('#login_form');
        var params = $.param({
            Username: $("#uname").val(),
            Password: $("#pass").val(),
            Remember: true,
            returnUrl: 'val'
        });
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: params,
            success: function (data) {
                if (data.ok) {
                    window.location.href = data.newurl;
                }
                else {
                    $('#div_errormsg').html(data.message);
                }
                console.log(data);
            }
        });
        return true;
    }
}
function submit_form(formid,params) {
    var form = $(formid);

    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: params,
        success: function (data) {
            if (data.Status) {
                $(form)[0].reset();
                //window.location.reload();
                show_notification(data.Message, "info");
            }
            else {
                show_notification(data.Message, "error");
            }
        }
    });
    return true;
}

function show_notification(message, type)
{
    var background = "";
    var icon = "";
    if (type == "warn") {
        background = "#20B2AA";
        icon = "<i class='icon-warning'></i>&nbsp;";
    }
    else if (type == "error")
    {
        background = "#FF4500";
        icon = "<i class='icon-cancel-2'></i>&nbsp;";
    }
    else if (type == "info") {
        background = "#DA70D6";
        icon = "<i class='icon-info-2'></i>&nbsp;";
    }
    else if (type == "success")
    {
        background = "#33CC33";
        icon = "<i class='icon-checkmark'></i>&nbsp;";
    }

    var not = $.Notify({
        content: message,
        timeout:1000*3,
        style: { background: background, color: 'white' }
    });
    $(".notify .content").prepend(icon);

}
//Function To Display Popup
function div_show() {
    document.getElementById('abc').style.display = "block";
    console.log('show login');
}
//Function to Hide Popup
function div_hide() {
    document.getElementById('abc').style.display = "none";
}

function toggle_tools(element)
{
    var divSize = $("#Btnusername");
    var slidingDiv = $("#DivSigOut");
    var top = element.offsetHeight + element.offsetTop;
    var left = element.offsetLeft;
    slidingDiv.css({ top: top, left: left, position: 'absolute', minHeight: 50, padding: 10 });
    slidingDiv.width(divSize.width()+45+ 'px');
    slidingDiv.addClass("bg-lightTeal fg-darkCrimson");
    slidingDiv.zIndex(1010);
    slidingDiv.slideToggle("slow");
}
function toggle_sidebar() {
    sidebarstate = !sidebarstate;
    var sidebar = $('#sidebar');
    if (sidebarstate == true) {
        sidebar.removeClass('hide');
        sidebar.addClass('show');
    }
    else {
        sidebar.removeClass('show');
        sidebar.addClass('hide');
    }
}



function toggle_newbooking() {

    bookingstate = !bookingstate;
    var url = window.location.href;
    var host = window.location.host;
    if (url.indexOf('http://' + host + '/Dashboard') != -1) {
        //match
        var newbooking = $('#nbooking');
        if (bookingstate == true) {
            newbooking.removeClass('hide');
            newbooking.addClass('show');
            $('#sidebar').addClass('hide');
            $('#sidebar').slideUp();
        }
        else {
            newbooking.removeClass('show');
            newbooking.addClass('hide');
        }
    }
    else {
        window.location = 'http://' + host + '/Dashboard?b=1';
        //newbooking.removeClass('show');
        //newbooking.addClass('hide');
    }

}

function load_booking() {
        var host = window.location.host;
        window.location = 'http://' + host + '/dashboard?b=1';
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function set_LogoUrl() {
    $.ajax({
        url: '@Url.Action("getClientTypeLogoUrl", "Live")',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            var image = baseUrl() + data;
            document.getElementById('LiveImg').src = image;
        }
    });
}

function baseUrl() {
    var thisurl = document.location.href.toString();
    var hav = thisurl.substring(thisurl.indexOf("//") + 2, thisurl.length);
    var thisbase = hav.substring(0, hav.indexOf("/"));
    return thisurl.substring(0, thisurl.indexOf("//") + 2) + thisbase + '/';
}

function loadPaymentModes()
{
    $.get('@Url.Action("PopulatePaymentModeData", "Dashboard")',function (data) {
            console.log(data);
        });
}

$(document).ready(function () {
    var booking = getUrlParameter('b');
    if (booking == 1) {
        loadPaymentModes();
        toggle_newbooking();

    }

    $('.login_submit').click(function (event) {
        console.log("clicked");
        check_empty()
        event.preventDefault();
    });

    $('.login_cancel').click(function (event) {
        div_hide();
        event.preventDefault();
    });

    if (!pinned) {
        $(".pin-icon").attr("src", "../../img/pin.png");
        $(".pin-icon").attr("title", "Pin to page");
    }
    else
    {
        $(".pin-icon").attr("src", "../../img/unpin.png");
        $(".pin-icon").attr("title", "Unpin from page");
    }
    
});

$(document).on('init.dt', function (e, settings) {
    var api = new $.fn.dataTable.Api(settings);

    //var newSearch = '<input type="search" class="" placeholder="Search" aria-controls="vehicles">';
    $('#vehicles_filter').addClass("bg-white");
    $('#vehicles_filter').css({ padding: "0 0px" });

});

function load_notifications() {
        notificationstate = !notificationstate;
        var notifications = $('#nots');
        var btn = $('#btnNots');
        var pos = btn.position();
        var offset = $(btn).offset();
        var height = $(btn).height();
        var top = offset.top + height * 3 + "px";
        notifications.css({ 'top': top, left: pos.left, position: 'absolute' });
        notifications.slideToggle("slow");
        notificationstate ? btn.addClass("bg-active-blue") : btn.removeClass("bg-active-blue");
}

function dialogStartUp(callback) {
    $.validator.unobtrusive.parse($('form'));
    $('form').submit(function () {
        var val = $("button[type=submit][clicked=true]").val();
        if ($(this).valid()) {
            var frm = $(this);
            $('#from_result').html('');
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize() + '&Command=' + val,
                type: 'POST',
                success: function (responseText, textStatus, XMLHttpRequest) {
                    closeLoading();
                    closeWaiting();
                    console.log(responseText);

                    if (responseText.Status == null) {
                        console.log(1);
                        show_notification(responseText, 'info');
                        return;
                    }
                    if (!responseText.Status) {
                        show_notification(responseText.Message, 'info');
                        return;
                    } else if (responseText.Status && responseText.IsUpdate) {
                        show_notification(responseText.Message, 'info');
                        location.reload(true);
                        return;
                    } else if (responseText.Status && !responseText.IsUpdate) {
                        frm[0].reset();
                        show_notification(responseText.Message, 'info');
                        if (callback != null && typeof callback === 'function') {
                            callback(responseText);
                        }
                        $.validator.unobtrusive.parse($('form'));
                    } else {
                        console.log(responseText); console.log(textStatus); console.log(XMLHttpRequest);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    show_notification("An error occurred while trying to access the server, Kindly retry again.</br>" +
                        "If problem persists, kindly check your network and contact support@geeckoltd.com", 'error');
                },
                beforeSend: function () {
                    showWaiting();
                    //location.reload(true);
                },
                complete: function () {
                    closeWaiting();
                    //location.reload(true);
                }
            });
        }
        return false;
    });

    $("form button[type=submit]").click(function () {
        $("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });
}


function showLoading() {
    if ($('#loading').length < 1) {
        var $div = $('<div />').appendTo('body,html');
        $div.attr('id', 'loading');
    }
    $('#loading').css('visibility', 'visible');
}

function closeLoading() {
    $('#loading').remove();
    $('#loading').css('visibility', 'hidden');
}

function showWaiting() {
    $.blockUI({
        message: $('#wait')
    });
}

function closeWaiting() {
    $.unblockUI();
}


