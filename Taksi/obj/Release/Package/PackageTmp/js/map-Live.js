﻿var update_interval = 15000;
var update_pending = false;
var docMap;
var gm = google.maps;
var map = null;
var pickup_xy = "";
var dropoff_xy = "";
var vehicles = [];
var book_table = null;
var focused_element = null;
var the_dataTable;
var $map = $('#map');
var imagePath = "../content/images/carsymb/";
var oms;

$(document).ready(function () {

    docMap = new mapobject("map");
    docMap.initialize();
    docMap.getCurrentLocation();

    docMap.SetAutoComplete("pickuppoint", "pickup");
    docMap.SetAutoComplete("dropoffpoint", "dropoff");

    loadDataTable();
    first_load();

    setInterval('update_vehicles()', update_interval);
});

function mapobject(elemCanvas) {
    var poly;
    var geocoder;
    var directionsDisplay;
    var directionsService;
    var stepDisplay;
    var markerArray = [];
    var markersArray = [];
    var markers = [];
    var path = new google.maps.MVCArray;
    var elemCanvas = elemCanvas;
    var self = this;
    var startstoppoints = { start: null, end: null };

    this.initialize = function () {
        directionsService = new google.maps.DirectionsService();

        //var manhattan = new google.maps.LatLng(-1.2833333, 36.8166667);
        //var mapOptions = {
        //    zoom: 8,
        //    mapTypeId: google.maps.MapTypeId.ROADMAP,
        //    center: manhattan
        //}
        //map = new google.maps.Map(document.getElementById("map"), mapOptions);

        map = new google.maps.Map(document.getElementById('map'), {
            center: latLng(-1.2833333, 36.8166667),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        oms = new OverlappingMarkerSpiderfier(map, { markersWontMove: true, markersWontHide: true });

        oms.addListener('click', function (marker) {
        });
        oms.addListener('spiderfy', function (markers) {
        });
        oms.addListener('unspiderfy', function (markers) {
        });

        // Create a renderer for directions and bind it to the map.
        var rendererOptions = {
            map: map
        }
        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

        //GEOCODER
        geocoder = new google.maps.Geocoder();

        //// Instantiate an info window to hold step text.
        stepDisplay = new google.maps.InfoWindow();

        google.maps.event.addListener(map, 'click', self.addPointEvent);
        google.maps.event.trigger(map, 'resize');
    }

    this.clearmapArray = function () {
        // First, remove any existing markers from the map.
        for (var i = 0; i < markerArray.length; i++) {
            markerArray[i].setMap(null);
        }
        // Now, clear the array itself.
        markerArray = [];
    }

    this.drawPoint = function (point) {
        var pnt = new google.maps.LatLng(point.split(",")[0], point.split(",")[1]);
        self.addMarker(pnt, "Start", 0);
    }

    this.calcRoute = function (start, end) {
        self.clearmapArray();
        // Retrieve the start and end locations and create
        // a DirectionsRequest using WALKING directions.
        var request = {
            origin: start,
            destination: end,
            travelMode: google.maps.TravelMode.DRIVING
        };

        // Route the directions and pass the response to a
        // function to create markers for each step.
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                //                var warnings = document.getElementById('warnings_panel');
                //                warnings.innerHTML = '<b>' + response.routes[0].warnings + '</b>';
                directionsDisplay.setDirections(response);
                self.showSteps(response);
            }
        });
    }

    this.showSteps = function (directionResult) {
        // For each step, place a marker, and add the text to the marker's
        // info window. Also attach the marker to an array so we
        // can keep track of it and remove it when calculating new
        // routes.
        //console.log('this.showSteps');
        var myRoute = directionResult.routes[0].legs[0];

        for (var i = 0; i < myRoute.steps.length; i++) {
            var marker = new google.maps.Marker({
                position: myRoute.steps[i].start_point,
                map: map
            });
            self.attachInstructionText(marker, myRoute.steps[i].instructions);
            markerArray[i] = marker;
        }

        //self.showroutedetails(myRoute);
    }

    this.attachInstructionText = function (marker, text) {
        google.maps.event.addListener(marker, 'click', function () {
            // Open an info window when the marker is clicked on,
            // containing the text of the step.
            stepDisplay.setContent(text);
            stepDisplay.open(map, marker);
        });
    }

    this.locSuccess = function (position) {
        var currentLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        geocoder.geocode({ 'location': currentLocation, 'region': 'GB' }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                //console.log(latitude, longitude);
                //console.log(results[0]);
                $('#pickuppoint').val(results[0].formatted_address);
                pickup_xy = latitude + ',' + longitude;
                startstoppoints.start = new google.maps.LatLng(latitude, longitude);
                //console.log(startstoppoints);

                if (path.length > 0) {
                    path.setAt(0, startstoppoints.start);
                } else {
                    path.insertAt(0, startstoppoints.start);
                }

                self.addMarker(startstoppoints.start, "Start", 0);
            }
        })


    }

    this.locError = function () {
        alert("Could not get current Location");
    }
    this.getCurrentLocation = function () {
        navigator.geolocation.getCurrentPosition(self.locSuccess, self.locError);
    }


    this.addPointEvent = function (event) {
        if (path.length > 1) {
            path.removeAt(1);
            //markers.pop().setMap(null);
        }
        path.insertAt(path.length, event.latLng);
        self.addMarker(event.latLng, path.length > 1 ? "End" : "Start");
    }

    this.addMarker = function (latLng, markerTitle, markerPos) {
        //console.log(path);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: true,
            visible: true,
            title: markerTitle
        });

        google.maps.event.addListener(marker, 'click', function () {
            marker.setMap(null);
            //                for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
            //                markers.splice(i, 1);
            path.removeAt(marker.getTitle().indexOf('end') > -1 ? 1 : 0);
        });

        if (markerTitle == "Start") {
            marker.setAnimation(google.maps.Animation.BOUNCE);
            google.maps.event.addListener(marker, 'dragend', function () {
                var position = marker.getPosition()
                var currentLocation = new google.maps.LatLng(position.lat(), position.lng());
                geocoder.geocode({ 'location': currentLocation, 'region': 'GB' }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        $('#pickuppoint').val(results[0].formatted_address);
                        pickup_xy = latitude + ',' + longitude;
                        startstoppoints.start = new google.maps.LatLng(latitude, longitude);
                        if (path.length > 0) {
                            path.setAt(0, startstoppoints.start);
                        } else {
                            path.insertAt(0, startstoppoints.start);
                        }

                        if (path.getAt(0) != null && path.getAt(1) != null) {
                            self.calcRoute(path.getAt(0), path.getAt(1));
                        }

                    }
                })
            });
        }
        else if (markerTitle == "End") {

            marker.setAnimation(google.maps.Animation.BOUNCE);
            google.maps.event.addListener(marker, 'dragend', function () {
                var position = marker.getPosition()
                var currentLocation = new google.maps.LatLng(position.lat(), position.lng());
                geocoder.geocode({ 'location': currentLocation, 'region': 'GB' }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        $('#dropoffpoint').val(results[0].formatted_address);
                        pickup_xy = latitude + ',' + longitude;
                        startstoppoints.end = new google.maps.LatLng(latitude, longitude);
                        if (path.length > 0) {
                            path.setAt(1, startstoppoints.end);
                        } else {
                            path.insertAt(1, startstoppoints.end);
                        }

                        if (path.getAt(0) != null && path.getAt(1) != null) {
                            self.calcRoute(path.getAt(0), path.getAt(1));
                        }

                    }
                })
            });
        }

        if (path.length > 1) {
            if (path.getAt(0) != null && path.getAt(1) != null) {
                self.calcRoute(path.getAt(0), path.getAt(1));
            }
        }
    }


    this.SetAutoComplete = function (txtId, hdnPoint) {
        $(function () {
            $("#" + txtId).autocomplete({
                //This bit uses the geocoder to fetch address values
                minLength: 2,
                autofill: true,
                source: function (request, response) {
                    geocoder.geocode({ 'address': request.term, 'region': 'GB' }, function (results, status) {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                latitude: item.geometry.location.lat(),
                                longitude: item.geometry.location.lng()
                            }
                        }));
                    })
                },
                //This bit is executed upon selection of an address
                select: function (event, ui) {
                    $("#" + hdnPoint).val(ui.item.latitude + "," + ui.item.longitude);
                    if (hdnPoint.indexOf('pickup') > -1) {

                        var coordinates = ui.item.latitude + " " + ui.item.longitude;
                        var params = $.param({
                            coordinates: coordinates
                        });

                        startstoppoints.start = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                        pickup_xy = ui.item.latitude + ',' + ui.item.longitude;
                        console.log(startstoppoints);

                        if (path.length > 0) {
                            path.setAt(0, startstoppoints.start);
                        } else {
                            path.insertAt(0, startstoppoints.start);
                        }

                        self.addMarker(startstoppoints.start, "Start", 0);
                    }
                    else {

                        startstoppoints.end = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                        console.log(startstoppoints);
                        dropoff_xy = ui.item.latitude + ',' + ui.item.longitude;
                        if (path.length > 1) {
                            path.setAt(1, startstoppoints.end);
                        } else if (path.length == 1) {
                            path.setAt(0, startstoppoints.start);
                            path.insertAt(1, startstoppoints.end);
                        } else {
                            path.insertAt(0, startstoppoints.start);
                            path.insertAt(1, startstoppoints.end);
                        }
                        self.addMarker(startstoppoints.end, "End", 1);
                    }
                }
            });
        });
    }
}

function initial_load(data) {
    update_pending = true;

    create_vehicles(data);

    update_pending = false;
}

function loadDataTable() {
    if (the_dataTable == null) {
        the_dataTable = $('#vehicles').DataTable({
            //"scrollCollapse": true,
            //"paging": true,
            ////"ordering": true,
            //"order": [[3, "desc"]],
            //"info": false,
            //"sScrollY": "200px",
            //"bPaginate": false,
            //"sDom": 'lfrtip<"clear spacer">T',
            //"bLengthChange": false
            //"paging": true,
            //"ordering": true,
            "order": [[3, "desc"]],
            "sScrollY": "200px",
            "sScrollX": "100%",
            "sScrollXInner": "100%",
            "bScrollCollapse": true,
            "bPaginate": false,
            "sDom": 'tS',
            "bSort": false,
            "bSortClasses": false
        });

    }
    the_dataTable.on("draw", function () {
        $(".dataTables_scrollBody").niceScroll();
    });
}


function create_vehicles(vs) {

    $.each(vs.OrgBookData, function () {
        var role = vs.UserRole;
        vehicles.push(new vehicle(this, role));
    });

}


// Vehicle class
function vehicle(values, role) {

    this.update = function (new_values) {


        for (var v in new_values) {
            this[v] = new_values[v];
        }

        this.update_row();
        this.update_icon();
    };

    this.update_row = function () {
        var self = this;

        var row = {
            RefNo: this.DispatchId,
            Customername: this.Customername,
            PickUpPoint: this.PickUpPoint,
            DropOffPoint: this.DropOffPoint,
            PickUpDateTime: this.PickUpDateTime,
            BookedDate: this.BookedDate,
            NoOfPassengers: this.NoOfPassengers,
            Status: this.Status,
            DispatchStatus: this.DispatchStatus,
            Rating: this.Rating
        };

        this.visible = true;

        this.row.html(make_tds(row))
                .unbind('click')
                .unbind('dblclick');

        this.row.find('.btn_approve').click(function () {
            self.aproove_booking(true);
        });

        this.row.find('.btn_reject').click(function () {
            self.aproove_booking(false);
        });
        if (row.Rating == null) {
            this.row.find('.rating').rating({
                static: false,
                score: row.Rating == null ? 0 : row.Rating,
                stars: 5,
                showHint: true,
                hints: ['bad', 'poor', 'average', 'good', 'excellent'],
                showScore: false,
                scoreHint: "Current score: ",
                click: function (value, rating) {
                    rating.rate(value);
                    self.submit_rating(value);
                }
            });
        }
        else {
            this.row.find('.rating').rating({
                static: true,
                score: row.Rating,
                stars: 5,
                showHint: true,
                hints: ['bad', 'poor', 'average', 'good', 'excellent'],
                showScore: false,
                scoreHint: "Current score: ",
            });
        }
        if (the_dataTable.row(this.row).length > 0) {
            var row_data = [];
            this.row.find("td").each(function () {
                row_data.push($(this).text());
            });
            the_dataTable.row(this.row)
            .data(row_data)
            .draw();
        }
    };

    this.submit_rating = function (value, comment) {
        var dID = this.DispatchId;
        var params = $.param({
            DispatchId: dID,
            Rating: value,
            Comments: comment
        });
        $.ajax({
            url: "/Dashboard/UpdateRating",
            type: 'POST',
            data: params,
            success: function (data) {
                if (data.Status) {
                    show_notification(data.Message, "success");
                }
                else {
                    show_notification(data.message, "error");
                }
                console.log(data);
            }
        });
    }
    this.aproove_booking = function (status) {
        var dID = this.DispatchId;
        var params = $.param({
            DispatchId: dID,
            DispatchStatusId: status ? 2 : 4
        });
        $.ajax({
            url: "/Dashboard/UpdateDispatchStatus",
            type: 'POST',
            data: params,
            success: function (data) {
                if (data.Status) {
                    show_notification(data.Message, "success");
                }
                else {
                    show_notification(data.message, "error");
                }
                console.log(data);
            }
        });
    }

    this.update_icon = function () {

        var self = this;

        if (this.marker != null) {
            this.marker.setMap(null);
        }

        this.marker = getMarkerIcon(this);

        var dispatchurl = 'http://gfleet.geeckoltd.com/VehicleDispatch/BookDispatch';

        var openDataParam = '{"Vid":"' + this.VehicleId + '"}';

        if (this.marker != null) {

            oms.addMarker(this.marker);

            this.marker = this.marker;
        }
    };


    function getMarkerIcon(sVehicle) {
        if (sVehicle.Latitude != null && sVehicle.Longitude != null && sVehicle.isVehicleVisible == true) {
            var icon_name = imagePath + sVehicle.ImgUrl;
            return createMarker(icon_name, sVehicle.Latitude, sVehicle.Longitude, sVehicle.RegCode, 32);
        } else {
            return null;
        }
    };

    this.setVisible = function (x) {
        this.visible = x;

        if (x) {
            this.row.show();
        } else {
            this.row.hide();
        }
    };

    function make_tds(vals) {
        var result = '';
        for (var v in vals) {
            if (v == "Status") {
                var dat = (vals[v] == '' || vals[v] == null ? 1 : vals[v]);
                if (role == 0) {
                    result += "";
                    if (dat == 1) {
                        result += '<td alt="Action" title="' + vals['DispatchStatus'] + '" class="' + v + " absorbing-column" + '">';
                        result += '<button class="button small success btn_approve">Approve</button><button class="button small inverse btn_reject">Reject</button>';
                        result += '</td>';
                    }
                    else if (dat == 6) {
                        result += '<td alt="Action" title="' + vals['DispatchStatus'] + '" class="' + v + " absorbing-column" + '">';
                        result += '<div class="rating small"></div>';
                        result += '</td>';
                    }
                    else {
                        result += '<td alt="' + dat + '" class="' + v + " absorbing-column" + '">' + vals['DispatchStatus'] + '</td>';
                    }
                }
                else {
                    if (dat == 6) {
                        result += '<td alt="Action" title="click on star to rate" class="' + v + '">';
                        result += '<div class="rating small"></div>';
                        result += '</td>';
                    }
                    else {
                        result += '<td alt="' + dat + '" class="' + v + " absorbing-column" + '">' + vals['DispatchStatus'] + '</td>';
                    }
                }
            }
            else if (v == "DispatchStatus" || v == "Rating") {
                //do nothing
            }
            else {
                if (vals[v] == null) {
                    result += '<td class="' + v + '">&nbsp;</td>';
                } else {
                    var dat = (vals[v].toString() == '' ? '&nbsp;' : vals[v].toString());
                    result += '<td alt="' + dat + '" title="' + dat + '" class="' + v + '">' + dat + '</td>';
                }
            }

        }
        return result;
    }


    this.row = $('<tr />');
    this.marker = null;
    this.update(values);

    the_dataTable.row.add(this.row).draw();

}

function update_vehicles() {
    if (update_pending) {
        return;
    }
    if (vehicles.length < 1) {
        first_load();
        return;
    }

    update_pending = true;

    var highest = vehicles[0].strDModified;
    $.each(vehicles, function () {
        highest = highest == null ? this.strDModified : this.strDModified > highest && this.strDModified != null ? this.strDModified : highest;
    });

    var params = $.param({
        highest: highest
    });

    $.ajax({
        url: '/Dashboard/getLatestBookData',
        type: 'POST',
        dataType: 'json',
        data: params,
        success: function (data) {
            update_pending = false;
            var result_new = [];
            $.each(data.OrgBookData, function () {
                var new_data = this;

                $.each(vehicles, function () {
                    if (this.DispatchId == new_data.DispatchId) {
                        this.update(new_data);
                    }
                });

                var exists = $.grep(vehicles, function (e) { return e.DispatchId == new_data.DispatchId; });
                if (exists.length == 0)
                    result_new.push(new_data);

            });
            $.each(result_new, function (vs) {
                var role = vs.UserRole;
                vehicles.push(new vehicle(this, role));
            });

            update_pending = false;
            the_dataTable
              .order([[3, 'desc']])
              .draw();
        },
        error: ajax_error
    });
}

function ajax_error(request, text, error) {
    update_pending = false;
    console.log('request: ' + request.responseText);
    console.log('text: ' + text);
    console.log('error: ' + error);
}
function createMarker(iconPath, lat, lng, text, size, tooltip) {
    var label = text !== '',
        halfSize = size / 2 | 0,
        base = {
            position: latLng(lat, lng),
            map: map,
            icon: {
                url: iconPath,
                size: new google.maps.Size(size, size),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(halfSize, halfSize)
            }
        };

    if (label) {
        base.labelContent = text;
        base.labelClass = 'maplabel';
        base.labelAnchor = new google.maps.Point(-halfSize, 8);
    }

    if (tooltip) {
        base.title = tooltip;
    }

    return new (label ? MarkerWithLabel : google.maps.Marker)(base);
}


function latLng(lat, lng) {
    return new google.maps.LatLng(lat, lng);
}